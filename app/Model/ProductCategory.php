<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public function product() {
    	return $this->hasMany('LBV\Model\Product','product_category_id');
    }
}
