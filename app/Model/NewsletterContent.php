<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class NewsletterContent extends Model
{
    public function newsletter() {
        return $this->belongsTo('LBV\Model\Newsletter'); 
    }
}
