<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
     
    public function accomodation() { 
    	return $this->belongsTo('LBV\Model\Accomodation');
    }

  

}
