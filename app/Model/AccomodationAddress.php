<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class AccomodationAddress extends Model
{
    public function accomodation() {
        return $this->hasOne('LBV\Model\Accomodation');
    }
}
