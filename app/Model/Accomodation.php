<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
     protected $fillable = ['*'];
    public function accomodation_prices() { 
    	return $this->hasMany('LBV\Model\AccomodationPrice','accom_id');
    }
    public function accomodation_type() {
    	return $this->belongsTo('LBV\Model\AccomodationType');  
    }

    public function gallery_images() {
    	return $this->hasMany('LBV\Model\GalleryImage','accom_id')->orderBy('sort_order'); 
    }

    public function room() { 
    	return $this->hasMany('LBV\Model\Room','accom_id'); 
    }

    public function user() {
        return $this->belongsTo('LBV\User');
    }
    public function accomodation_address() {
        return $this->hasOne('LBV\Model\AccomodationAddress','accom_id');
    }
    public function searchtag() {
        return $this->hasMany('LBV\Model\AccomodationSearchtag','accom_id');
    } 
    public function extra_price() {
        return $this->hasMany('LBV\Model\AccomodationPriceExtraCharge','accom_id');
    } 
}
