<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class SearchtagSearchingGroup extends Model
{
 	
 	public function search_tag() {
 		return $this->belongsTo('LBV\Model\SearchTag');
 	}
 	public function search_group() {
 		return $this->belongsTo('LBV\Model\SearchGroup');
 	}
}
