<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model; 

class Product extends Model
{
    public function product_category() { 
    	return $this->belongsTo('LBV\Model\ProductCategory');
    }
}
