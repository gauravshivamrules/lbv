<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class RoomPrice extends Model 
{
    public function room() {
    	return $this->belongsTo('LBV\Model\Room');
    }
}
