<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class AccomodationSearchtag extends Model 
{
    public function accomodation() { 
    	return $this->belongsTo('LBV\Model\Accomodation'); 
    }
}
