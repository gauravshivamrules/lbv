<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public function newsletter_content() { 
       	return $this->hasOne('LBV\Model\NewsletterContent');
    }
}
