<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;
 
class AccomodationPrice extends Model
{
    
    public function accomodation() {
    	return $this->belongsTo('LBV\Model\Accomodation');
    } 
}
