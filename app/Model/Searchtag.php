<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class Searchtag extends Model 
{
	public function search_search_group() {
		return $this->hasMany('LBV\Model\SearchSearchGroup');
	}

}
