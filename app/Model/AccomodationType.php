<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class AccomodationType extends Model
{
    
    public function accomodation() {  
    	return $this->hasMany('LBV\Model\Accomodation');  
    }

}
