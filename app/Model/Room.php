<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{ 
    public function accomodation() {
    	return $this->belongsTo('LBV\Model\Accomodation');
    }

    public function room_price() {
    	return $this->hasMany('LBV\Model\RoomPrice');
    }


}
