<?php

namespace LBV\Model;

use Illuminate\Database\Eloquent\Model;

class AccomodationNews extends Model 
{
    public function accomodation() {
        return $this->hasOne('LBV\Model\Accomodation');
    }
}
