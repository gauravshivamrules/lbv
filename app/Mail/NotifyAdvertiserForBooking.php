<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable; 
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdvertiserForBooking extends Mailable  
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public $url;
    public function __construct($subject,$msg,$url)  
    {
        $this->subject=$subject;
        $this->msg=$msg;
        $this->url=$url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.bookings.notify_adv')->with('msg',$this->msg)->with('url',$this->url); 
    } 
}
