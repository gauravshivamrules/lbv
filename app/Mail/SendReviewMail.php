<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReviewMail extends Mailable  
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public $link;
    public function __construct($subject,$msg,$link)  
    {
        $this->subject=$subject;
        $this->msg=$msg;
        $this->link=$link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.bookings.review_link')->with('msg',$this->msg)->with('link',$this->link);  
    } 
}
