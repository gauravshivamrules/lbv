<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendAdminPaymentNotification extends Mailable 
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user; 
    public $subject;
    public $msg;
    public $accom;
    public function __construct(User $user,$accom,$subject,$msg) 
    {
        $this->user=$user;
        $this->subject=$subject;
        $this->msg=$msg;
        $this->accom=$accom;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.subscriptions.new')->with('user',$this->user)->with('msg',$this->msg)->with('accom',$this->accom); 
        //return $this->subject(__('New Bank Transfer Request Received'))->markdown('emails.subscriptions.new')->with('accom',$this->accom);    
    } 
}
