<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewsletter extends Mailable  
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $finalArr; 
    public $newsletter;
    public function __construct($subject,$finalArr,$newsletter)  
    {
        $this->subject=$subject;
        $this->finalArr=$finalArr;
        $this->newsletter=$newsletter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.newsletters.newsletter')->with('finalArr',$this->finalArr)->with('newsletter',$this->newsletter);   
    } 
}
