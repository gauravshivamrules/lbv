<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendPaymentInvoicePaypal extends Mailable
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct(User $user)
    {
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__('Thanks for Choosing Paypal'))->markdown('emails.subscriptions.paid_invoice_paypal')->with('user',$this->user)->attach(public_path('files/pdf/invoice.pdf'));   
    } 
}
