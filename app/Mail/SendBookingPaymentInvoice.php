<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendBookingPaymentInvoice extends Mailable
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $msg; 
    public $link; 
    public function __construct(User $user,$msg,$link)
    {
        $this->user=$user;
        $this->msg=$msg;
        $this->link=$link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->subject(__('10% booking commission invoice'))->markdown('emails.bookings.notify_user')->with('user',$this->user)->with('msg',$this->msg)->with('url',$this->link)->attach(public_path('files/pdf/products/invoice.pdf'));     
    } 
}
