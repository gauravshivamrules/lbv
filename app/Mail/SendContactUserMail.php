<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendContactUserMail extends Mailable
{
    use Queueable, SerializesModels;  
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public function __construct($subject,$msg)
    {
        $this->msg=$msg;
        $this->subject=$subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->subject(__($this->subject))->markdown('emails.subscriptions.contact_user')->with('msg',$this->msg);    
    } 
}
