<?php
namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVerificationLink extends Mailable  
{
    use Queueable, SerializesModels;  
    public $subject; 
    public $msg; 
    public $link;
    public function __construct($subject,$msg,$link)  
    {
        $this->subject=$subject;
        $this->msg=$msg;
        $this->link=$link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */ 
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.users.verification')->with('msg',$this->msg)->with('link',$this->link);  
    } 
}
