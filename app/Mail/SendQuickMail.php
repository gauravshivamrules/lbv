<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendQuickMail extends Mailable
{
    use Queueable, SerializesModels;  
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public $advSubject; 
    public $user; 
    
    public function __construct($subject,$msg,$advSubject,$user)
    {
        $this->msg=$msg;
        $this->subject=$subject;
        $this->advSubject=$advSubject;
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->subject(__($this->subject))
            ->markdown('emails.quick_mail')->with('msg',$this->msg) 
            ->with('advSubject',$this->advSubject)         
            ->with('user',$this->user); 
    } 
}
