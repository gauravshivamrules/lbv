<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendContactRequest extends Mailable
{
    use Queueable, SerializesModels;  
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public $name; 
    public $email; 
    public $txt; 
    public function __construct($subject,$msg,$name,$email,$txt)
    {
        $this->msg=$msg;
        $this->subject=$subject;
        $this->name=$name;
        $this->email=$email;
        $this->txt=$txt;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->subject(__($this->subject))
            ->markdown('emails.contact_us')->with('msg',$this->msg) 
            ->with('name',$this->name)    
            ->with('email',$this->email)    
            ->with('txt',$this->txt);    
    } 
}
