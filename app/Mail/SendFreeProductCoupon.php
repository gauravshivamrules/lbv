<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue; 
use LBV\User;

class SendFreeProductCoupon extends Mailable
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance. 
     *
     * @return void
     */
    public $user;
    public $coupon;
    public $accom;
    public $product;
    public function __construct(User $user,$coupon,$accom,$product)
    {
        $this->user=$user;
        $this->coupon=$coupon;
        $this->accom=$accom;
        $this->product=$product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
       return $this->subject(__('Congratulations,you have got a free promotion coupon'))->markdown('emails.products.free_coupon')
                        ->with('user',$this->user)->with('coupon',$this->coupon)->with('accom',$this->accom)->with('product',$this->product);   
    } 
}
