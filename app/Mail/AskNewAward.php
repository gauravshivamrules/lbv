<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class AskNewAward extends Mailable
{
    use Queueable, SerializesModels; 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user; 
    public $msg; 
    public function __construct($msg,$subject,$image)
    {
        $this->msg=$msg;
        $this->subject=$subject;
        $this->image=$image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->subject(__($this->subject))->markdown('emails.awards.notify_admin')->with('msg',$this->msg)->attach(public_path('/images/awards/tmp_awards/'.$this->image));    
    } 
}
