<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendProductPaymentInvoiceAdmin extends Mailable
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $msg; 
    public $subject;
    public function __construct(User $user,$msg,$subject)
    {
        $this->user=$user;
        $this->msg=$msg;
        $this->subject=$subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    { 
    	return $this->subject(__($this->subject))->markdown('emails.products.notify_user')->with('user',$this->user)
    				->with('msg',$this->msg)->attach(public_path('files/pdf/products/invoice.pdf'));    
    	
        
    } 
}
