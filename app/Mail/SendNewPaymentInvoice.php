<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use LBV\User;

class SendNewPaymentInvoice extends Mailable 
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */ 
    public $user; 
    public $subject;
    public $msg;
    public function __construct(User $user,$subject,$msg) 
    {
        $this->user=$user;
        $this->subject=$subject;
        $this->msg=$msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.subscriptions.new_paid_invoice')->with('user',$this->user)->with('msg',$this->msg)->attach(public_path('files/pdf/invoice.pdf'));    
    } 
}
