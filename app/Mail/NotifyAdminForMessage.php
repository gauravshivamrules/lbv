<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminForMessage extends Mailable   
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg;
    public $userMsg; 
    public function __construct($subject,$msg,$userMsg)  
    {
        $this->subject=$subject;
        $this->msg=$msg;
        $this->userMsg=$userMsg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.messages.notify_admin')->with('msg',$this->msg)->with('userMsg',$this->userMsg);  
    } 
}
