<?php

namespace LBV\Mail;
use LBV\Model\Accomodation;
use LBV\Model\Country;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class NewBankTransferRequestAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $accom; 
    public function __construct(Accomodation $accom)
    {
       $this->accom=$accom;
    }

    /**
     * Build the message. 
     *
     * @return $this
     */
    public function build()
    {
        $country=Country::select(['name'])->whereId($this->accom->accomodation_address->country_id)->first();
        if($country) {
           $this->accom->accomodation_address->country_id=$country->name; 
        } 
        return $this->subject(__('New Bank Transfer Request Received'))->markdown('emails.subscriptions.new')->with('accom',$this->accom); 
    }
}
