<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use LBV\Model\Accomodation;
use LBV\Model\Country;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAccomodationRegistered extends Mailable 
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $accom; 
    public function __construct(Accomodation $accom)
    {
        $this->accom=$accom;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        $country=Country::select(['name'])->whereId($this->accom->accomodation_address->country_id)->first();
        if($country) {
           $this->accom->accomodation_address->country_id=$country->name; 
        }
        $msg='';
        if($this->accom->payment_type==1) {
            $LABEL='';
            if($this->accom->label_id==3) {
                $LABEL='RENT';
            } else {
                $LABEL='SALE';
            }
            $msg='New paid '.$LABEL.' accomodation has been registered'; 
        } else {
            $msg='New free accomodation has been registered'; 
        }
        return $this->markdown('emails.accomodations.new')->with('accom',$this->accom)->with('msg',$msg); 
    }
}
