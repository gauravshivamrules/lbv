<?php
namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminsForNewLocationRequest extends Mailable  
{
    use Queueable, SerializesModels;  

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject; 
    public $msg; 
    public function __construct($subject,$msg)  
    {
        $this->subject=$subject;
        $this->msg=$msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */ 
    public function build()
    {
        return $this->subject(__($this->subject))->markdown('emails.accomodations.notify_admin')->with('msg',$this->msg);  
    } 
}
