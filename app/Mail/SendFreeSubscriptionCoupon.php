<?php

namespace LBV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue; 
use LBV\User;

class SendFreeSubscriptionCoupon extends Mailable
{
    use Queueable, SerializesModels; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $coupon;
    public $accom;
    public function __construct(User $user,$coupon,$accom)
    {
        $this->user=$user;
        $this->coupon=$coupon;
        $this->accom=$accom;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->subject(__('Congratulations,you have got a free subscription coupon'))->markdown('emails.subscriptions.free_coupon')
                        ->with('user',$this->user)->with('coupon',$this->coupon)->with('accom',$this->accom);   
    } 
}
