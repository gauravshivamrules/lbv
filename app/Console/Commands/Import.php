<?php

namespace LBV\Console\Commands;
use DB;
use Illuminate\Console\Command;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:Import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports old lbv DB to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command. 
     *
     * @return mixed
     */
    public function handle()
    {
       $totalUsers=$totalAccoms=0;
       $oldUsers=DB::table('old_lbv.users')->get(); 
       $totalUsers=count($oldUsers);
       // dump($oldUsers); 
       $this->info(count($oldUsers)." Users Found!");
       if($oldUsers){ 
           foreach($oldUsers as $u){
                $this->info("Importing Users...");
               $userAddress=DB::table('old_lbv.user_addresses')->whereId($u->user_address_id)->first();
               $newUser=DB::table('users')->insertGetId([
                   'id'=>$u->id,
                   'role_id'=>$u->role_id,
                   'created_at'=>$u->created,
                   'updated_at'=>date('Y-m-d H:i:s'),
                   'first_name'=>$u->first_name,
                   'last_name'=>$u->sur_name,
                   'email'=>$u->email,
                   'btw_number'=>$u->uservat,
                   'user_company'=>$u->user_company,
                   'phone_number'=>$u->phone,
                   'country_id'=>$userAddress?$userAddress->country_id:null,
                   'city'=>$userAddress?$userAddress->city:'',
                   'street'=>$userAddress?$userAddress->street:'',
                   'street_number'=>$userAddress?$userAddress->street_number:null,
                   'post_code'=>$userAddress?$userAddress->zip:null,
                   'complete_profile'=>$u->complete_profile,
                   'is_verified'=>1
               ]);
                $this->info($u->email." Imported"); 
               if($u->role_id==3){
                 $this->info("Found Advertiser");
                 $userAccoms=DB::table('old_lbv.accoms')->whereUserId($u->id)->get();
                 $totalAccoms=$totalAccoms+count($userAccoms);
                 $this->info(count($userAccoms)." Accomodations Found For User ".$u->email);
                 if($userAccoms){
                    foreach($userAccoms as $a){
                        $this->info("Importing Accomodations...");
                       $accomLable=DB::table('old_lbv.acc_labels')->whereAccomId($a->id)->first();
                       $accom=DB::table('accomodations')->insertGetId([
                           'id'=>$a->id,
                           'user_id'=>$a->user_id,
                           'name'=>$a->accom_name,
                           'created_at'=>$a->created,
                           'updated_at'=>date('Y-m-d H:i:s'),
                           'approved'=>$a->approved,
                           'completed'=>1,
                           'label_id'=>$accomLable?$accomLable->label_id:0,
                           'accomodation_type_id'=>$a->acc_type_id,
                           'payment_type'=>$a->accom_payment_type,
                           'subs_valid_license'=>$a->subs_valid_license,
                           'website'=>$a->accom_website,
                           'email'=>$a->accom_email,
                           'description'=>$a->accom_description,
                           'number_of_persons'=>$a->accom_number_persons,
                           'deletion_request'=>$a->deletion_request,
                           'featured_image'=>$a->featured_image,
                           'editable'=>$a->editable,
                           'is_slider'=>$a->is_slider,
                           'reservation_active'=>$a->reservation_active,
                           'slug'=>$a->accom_slug
                       ]);
                        $accomLocation=DB::table('old_lbv.acc_addresses')->whereId($a->acc_address_id)->first();
                        if($accomLocation){
                          $this->info("Importing Address...");
                          $location=DB::table('accomodation_addresses')->insertGetId([ 
                            'accom_id'=>$a->id,
                            'country_id'=>$accomLocation->country_id,
                            'region_id'=>$accomLocation->region_id,
                            'department_id'=>$accomLocation->department_id,
                            'street'=>$accomLocation->addr_street,
                            'street_number'=>$accomLocation->addr_street_number,
                            'city'=>$accomLocation->addr_city,
                            'postal_code'=>$accomLocation->addr_zip,
                            'lattitude'=>$accomLocation->addr_lattitude,   
                            'longitude'=>$accomLocation->addr_longitude,
                            'created_at'=>$accomLocation->created,
                            'updated_at'=>date('Y-m-d H:i:s')
                          ]);
                        }

                       $accomPrices=DB::table('old_lbv.acc_prices')->whereAccomId($a->id)->get();
                       if(!$accomPrices->isEmpty()){
                            $this->info("Importing Prices...");
                           foreach ($accomPrices as $p) {
                             $unit='perweek';
                             if($p->price_unit_id==1){
                               $unit='perweek';
                             }else if($p->price_unit_id==2){
                               $unit='perroom';
                             }else {
                               $unit='perperson';
                             }
                             $price=DB::table('accomodation_prices')->insertGetId([
                                 'accom_id'=>$a->id,
                                 'created_at'=>$p->created,
                                 'updated_at'=>date('Y-m-d H:i:s'),
                                 'price_unit_id'=>$p->price_unit_id,
                                 'price_unit'=>$unit,
                                 'amount'=>$p->price_amount,
                                 'max_persons'=>$p->min_persons,
                                 'min_days'=>$p->min_days,
                                 'start_date'=>$p->price_period_start,
                                 'end_date'=>$p->price_period_ends,
                                 'changeday'=>$p->change_day,
                                 'extra_price_per_person'=>$p->extra_price
                             ]);
                           }
                           $rooms=DB::table('old_lbv.rooms')->whereAccomId($a->id)->get();
                           if($rooms){
                                $this->info("Importing Rooms...");
                               foreach ($rooms as $r) {
                                 $room=DB::table('rooms')->insertGetId([
                                     'accom_id'=>$a->id,
                                     //'accom_price_id'=>
                                     'room_type'=>$r->room_type,
                                     'description'=>$r->description,
                                     'adults'=>$r->adults,
                                     'childs'=>$r->childs,
                                     'max_child_age'=>$r->child_age,
                                     'no_of_rooms'=>$r->room,
                                     'total_adults'=>$r->total_adults,
                                     'total_childs'=>$r->total_childs,
                                     'min_days'=>$accomPrices?$accomPrices->first()->min_days:0,
                                     'max_adults_per_room'=>$r->max_adults,
                                     'max_persons_per_room'=>$r->max_persons,
                                     'created_at'=>$r->created,
                                     'updated_at'=>date('Y-m-d H:i:s')
                                 ]);
                                 $roomPrice=DB::table('old_lbv.room_prices')->whereRoomId($r->id)->get();
                                 if($roomPrice){
                                    $this->info("Importing Room Prices...");
                                   foreach ($roomPrice as $rP) {
                                     $rPrice=DB::table('room_prices')->insertGetId([
                                       'room_id'=>$room,
                                       'accom_id'=>$a->id,
                                       'start_date'=>$rP->start_date,
                                       'end_date'=>$rP->end_date,
                                       'price'=>$rP->price,
                                       'price1'=>$rP->price1,
                                       'price2'=>$rP->price2,
                                       'price3'=>$rP->price3,
                                       'extra_price_adult'=>$rP->extra_price_adult,
                                       'extra_price_child'=>$rP->extra_price_child,
                                       'created_at'=>$rP->created,
                                       'updated_at'=>date('Y-m-d H:i:s')
                                     ]);
                                   }
                                 }
                               }
                           }
                       }
                       $extraPrices=DB::table('old_lbv.acc_price_extra_charges')->whereAccomId($a->id)->get();
                       if($extraPrices){
                            $this->info("Importing Extra Prices...");
                           foreach ($extraPrices as $e) {
                             $extra=DB::table('accomodation_price_extra_charges')->insertGetId([
                                 'accom_id'=>$a->id,
                                 'amount'=>$e->amount,
                                 'description'=>$e->description,
                                 'is_optional'=>$e->is_optional,
                                 'charge_type'=>$e->optional_charge_type, 
                                 'is_per_person'=>$e->is_per_person,
                                 'created_at'=>$e->created,
                                 'updated_at'=>date('Y-m-d H:i:s')
                             ]);
                           }
                       }
                       $accomAmnities=DB::table('old_lbv.accom_searchtags')->whereAccomId($a->id)->get();
                       if($accomAmnities){
                            $this->info("Importing Amnities...");
                           foreach ($accomAmnities as $am) {
                             $searchTag=DB::table('accomodation_searchtags')->insertGetId([
                                 'accom_id'=>$a->id,
                                 'searchtag_id'=>$am->searchtag_id,
                                 'created_at'=>date('Y-m-d H:i:s'),
                                 'updated_at'=>date('Y-m-d H:i:s')
                             ]);
                           }
                       }
                       $accomExtraTabs=DB::table('old_lbv.extra_tabs')->whereAccomId($a->id)->get();
                       if($accomExtraTabs){
                         $this->info("Importing Extra Tabs...");
                         foreach ($accomExtraTabs as $et) {
                           $extra=DB::table('extra_tabs')->insertGetId([
                               'accom_id'=>$a->id,
                               'page_name'=>$et->page_name,
                               'tab_content'=>$et->tab_content,
                               'status'=>$et->status,
                               'created_at'=>date('Y-m-d H:i:s'),
                               'updated_at'=>date('Y-m-d H:i:s')
                           ]);
                         }
                       }
                       $accomAwards=DB::table('old_lbv.acc_awards')->whereAccomId($a->id)->get();
                       if($accomAwards){
                        $this->info("Importing Awards...");
                        foreach ($accomAwards as $aw) {
                          $award=DB::table('accomodation_awards')->insertGetId([
                              'accom_id'=>$a->id,
                              'award_id'=>$aw->award_id,
                              'approved'=>$aw->approved,
                              'deletion_request'=>$aw->deletion_request,
                              'created_at'=>$aw->created,
                              'updated_at'=>date('Y-m-d H:i:s')
                          ]);
                        }
                       }
                       $galImages=DB::table('old_lbv.gallery_images')->whereAccomId($a->id)->get();
                       if($galImages){
                        $this->info("Importing Images...");
                         foreach ($galImages as $g) {
                           $gal=DB::table('gallery_images')->insertGetId([
                               'accom_id'=>$a->id,
                               'image'=>$g->main_img,
                               'thumb_image'=>$g->search_img,
                               'slider_image'=>$g->slider_img,
                               'sliderbig_image'=>$g->slider_big_image,
                               'created_at'=>$g->created,
                               'updated_at'=>date('Y-m-d H:i:s') 
                           ]);
                         }
                       }
                    } 
                 }
               }
           }
           $this->info('STEP 1 HAS BEEN COMPLETED');
           $this->info('Total Users Imported: '.$totalUsers);
           $this->info('Total Accomomdations Imported: '.$totalAccoms);
          }

           $this->info('Preparing Subscription Licenses');
           $subscriptions=DB::table('old_lbv.sale_rent_subscriptions')->get();
           if($subscriptions){
             foreach ($subscriptions as $s) {
                 DB::table('subscriptions')->insert([
                     'id'=>$s->id,
                     'name'=>$s->subscription_name,
                     'description'=>$s->subscription_desc,
                     'amount'=>$s->amount,
                     'type_id'=>$s->subscription_type_id,
                     'duration'=>$s->duration,
                     'duration_type'=>$s->duration_type,
                     'created_at'=>$s->created,
                     'updated_at'=>date('Y-m-d H:i:s') 
                 ]); 
             }
           } 
            $subInstances=DB::table('old_lbv.subscription_instances')->get();
            if($subInstances){
                 foreach ($subInstances as $sI) {
                     $startP=DB::table('old_lbv.subscription_payments')->select('id')->whereSubscriptionInstanceId($sI->id)->first();
                     if($startP){
                       $startEnd=DB::table('old_lbv.subscription_licenses')->whereSubscriptionPaymentId($startP->id)->first();
                     }
                     // dump($startEnd);
                     $instance=DB::table('subscription_instances')->insertGetId([
                         'id'=>$sI->id,
                         'subscription_id'=>$sI->sale_rent_subscription_id,
                         'accom_id'=>$startEnd?$startEnd->accom_id:null,
                         'start_from'=>$startEnd?$startEnd->created:null,
                         'valid_upto'=>$startEnd?$startEnd->valid_upto:null,
                         'amount'=>$sI->amount,
                         'created_at'=>$sI->created,
                         'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                 }
            }
           // die;

            $subPayments=DB::table('old_lbv.subscription_payments')->get();
            if($subPayments){
                 foreach ($subPayments as $sP) {
                     $payment=DB::table('subscription_payments')->insertGetId([
                         'id'=>$sP->id,
                         'subscription_instance_id'=>$sP->subscription_instance_id,
                         'user_id'=>$sP->user_id,
                         'amount_paid'=>$sP->amount_paid,
                         'cancelled_amount'=>$sP->cancelled_amount,
                         'discount'=>$sP->discount,
                         'pay_method_id'=>$sP->pay_method_id,
                         'payment_complete'=>$sP->payment_complete,
                         'sub_transaction_id'=>$sP->sub_transaction_id,
                         'pay_json'=>$sP->pay_json,
                         'paypal_ipn_response'=>$sP->paypal_ipn_response,
                         'paypal_ipn_response_id'=>$sP->paypal_ipn_response_id,
                         'ipn_verified'=>$sP->ipn_verified,
                         'created_at'=>$sP->created,
                         'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                 }
            }
            $subLicense=DB::table('old_lbv.subscription_licenses')->get();
            if($subLicense){
                 foreach ($subLicense as $sL) {
                     $license=DB::table('subscription_licenses')->insertGetId([
                         'id'=>$sL->id,
                         'subscription_payment_id'=>$sL->subscription_payment_id,
                         'start_from'=>$sL->created,
                         'valid_upto'=>$sL->valid_upto,
                         'accom_id'=>$sL->accom_id,
                         'subscription_key'=>$sL->subscription_key,
                         'invoice_number'=>$sL->invoice_number,
                         'order_number'=>$sL->order_number,
                         'user_id'=>$sL->user_id,
                         'license_renewed'=>$sL->license_renewed,
                         'four_notified'=>$sL->four_notified,
                         'two_notified'=>$sL->two_notified,
                         'one_notified'=>$sL->one_notified,
                         'after_expire'=>$sL->after_expire,
                         'token'=>$sL->token,
                         'tmp_valid_upto'=>$sL->tmp_valid_upto,
                         'created_at'=>$sL->created,
                         'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                 }
            }
            $this->info('Subscription Licenses Imported');
            $this->info('Preparing Promotion Licenses');
            $products=DB::table('old_lbv.products')->get();
            if($products){
                 foreach ($products as $p) {
                      DB::table('products')->insert([
                         'id'=>$p->id,
                         'product_category_id'=>$p->product_category_id,
                         'product_name'=>$p->product_name,
                         'product_description'=>$p->product_description,
                         'product_price'=>$p->product_price,
                         'product_month'=>$p->product_month,
                         'product_year'=>$p->product_year,
                         'created_at'=>$p->created,
                         'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                 }
             }
             $proInstances=DB::table('old_lbv.product_instances')->get();
             foreach ($proInstances as $p) {
                 $instanceAccomID=DB::table('old_lbv.product_licenses')->select('accom_id')->whereProductInstanceId($p->id)->first();
                 DB::table('product_instances')->insert([
                     'id'=>$p->id,
                     'product_id'=>$p->product_id,
                     'product_category_id'=>$p->product_category_id,
                     'accom_id'=>$instanceAccomID?$instanceAccomID->accom_id:null,
                     'product_payment_id'=>$p->product_payment_id,
                     'product_price'=>$p->product_price,
                     'cancelled_amount'=>$p->cancelled_amount,
                     'created_at'=>$p->created,
                     'updated_at'=>date('Y-m-d H:i:s')
                 ]);
             }
            
             $proLicense=DB::table('old_lbv.product_licenses')->get();
              foreach ($proLicense as $p) {
                 $instancePaymentID=DB::table('old_lbv.product_instances')->select('product_payment_id')->whereId($p->product_instance_id)->first();
                 $proUserId=DB::table('old_lbv.product_payments')->select('user_id')->whereId($instancePaymentID->product_payment_id)->first();
                 DB::table('product_licenses')->insert([
                      'id'=>$p->id,
                      'user_id'=>$proUserId?$proUserId->user_id:null,
                      'product_payment_id'=>$instancePaymentID->product_payment_id,
                      'product_instance_id'=>$p->product_instance_id,
                      'accom_id'=>$p->accom_id,
                      'valid_from'=>$p->valid_from,
                      'valid_upto'=>$p->valid_upto,
                      'discarded'=>$p->discarded,
                      'product_month'=>$p->product_month,
                      'product_year'=>$p->product_year,
                      'created_at'=>$p->created,
                      'updated_at'=>date('Y-m-d H:i:s')

                  ]);
              }
              $proPayments=DB::table('old_lbv.product_payments')->get();
                foreach ($proPayments as $p) {
                    DB::table('product_payments')->insert([
                        'id'=>$p->id,
                        'invoice_number'=>$p->invoice_num,
                        'transaction_id'=>$p->transaction_id,
                        'transaction_json'=>$p->transaction_json,
                        'user_id'=>$p->user_id,
                        'payment_complete'=>$p->payment_status,
                        'total_amount'=>$p->total_amount,
                        'cancelled_amount'=>$p->cancelled_amount,
                        'discount'=>$p->discount,
                        'pay_method_id'=>$p->pay_method_id,
                        'invoice_key'=>$p->invoice_key,
                        'paypal_ipn_response'=>$p->paypal_ipn_response,
                        'paypal_ipn_response_id'=>$p->paypal_ipn_response_id,
                        'ipn_verified'=>$p->ipn_verified,
                        'created_at'=>$p->created,
                        'updated_at'=>date('Y-m-d H:i:s') 
                    ]);
                }
            $this->info('Promotion Licenses Imported');


            $this->info('Preparing Lastminutes,news messages for import');
            $promos=DB::table('old_lbv.promos')->get();
            foreach ($promos as $p) {
              $content=DB::table('old_lbv.lastminutes')->wherePromoId($p->id)->first();
              DB::table('accomodation_lastminutes')->insert([
                  'id'=>$p->id,
                  'accom_id'=>$p->accom_id,
                  //'title'=>
                  'description'=>$content?$content->promo_content:'',
                  'start_from'=>$p->promo_start,
                  'valid_upto'=>$p->promo_end,
                  'product_license_id'=>$p->product_license_id,
                  'created_at'=>$p->created,
                  'updated_at'=>date('Y-m-d H:i:s') 
              ]);
            }
            $news=DB::table('old_lbv.acc_news')->get();
            foreach ($news as $n) {
                DB::table('accomodation_news')->insert([
                    'id'=>$n->id,
                    'accom_id'=>$n->accom_id,
                    'title'=>$n->news_title,
                    'description'=>$n->news_description,
                    'start_from'=>$n->news_startdate,
                    'valid_upto'=>$n->news_enddate,
                    'product_license_id'=>$n->product_license_id,
                    'created_at'=>$n->created,
                    'updated_at'=>date('Y-m-d H:i:s') 
                ]);
            }

            $messages=DB::table('old_lbv.contact_tickets')->get();
            if($messages){
                foreach ($messages as $m) {
                      DB::table('messages')->insert([
                          'id'=>$m->id,
                          'message'=>$m->message,
                          'to_user'=>$m->to_user,
                          'from_user'=>$m->from_user,
                          'sender_name'=>$m->sender_name,
                          'accom_id'=>$m->accom_id,
                          'approved'=>$m->approved,
                          'seen_by_adv'=>$m->seen_by_adv,
                          'seen_by_cust'=>$m->seen_by_cust,
                          'created_at'=>$m->created,
                          'updated_at'=>date('Y-m-d H:i:s') 
                      ]);
                      $replies=DB::table('old_lbv.contact_ticket_replies')->whereContactTicketId($m->id)->get();
                      if($replies){
                        foreach ($replies as $r) {
                            DB::table('message_replies')->insert([
                              'id'=>$r->id,
                              'message_id'=>$m->id,
                              'accom_id'=>$m->accom_id,
                              'reply'=>$r->reply_message,
                              //'sender_name'=>
                              'approved'=>$r->approved,
                              'mail_to'=>$r->mail_to,
                              'user_id'=>$r->user_id,
                              'seen_by_adv'=>$r->seen_by_adv,
                              'seen_by_cust'=>$r->seen_by_cust,
                              'sent_by'=>$r->sent_by,
                              'sent_to'=>$r->sent_to,
                              'created_at'=>$r->created,
                              'updated_at'=>date('Y-m-d H:i:s') 
                            ]);
                        }
                      }
                }
            }
            $this->info('Lastminutes,news messages Imported');
            dd("ALL Data Imported");



       } 
}

