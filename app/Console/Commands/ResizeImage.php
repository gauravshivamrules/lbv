<?php

namespace LBV\Console\Commands;
use DB;
use Image;
use LBV\Model\GalleryImage;
use Illuminate\Console\Command;

class ResizeImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ResizeImage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resizes all accommodations images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $allImages=DB::table('old_gallery_images')->limit(50)->get();
       $destinationPath=public_path('images/gallery/');
       $originPath=public_path('images/old_gallery/');
       foreach ($allImages as $img) { 
           if ($dh = opendir($originPath.$img->accom_id)){
               while (($file = readdir($dh)) !== false){
                 if($file==$img->image){
                   if (!file_exists($destinationPath.$img->accom_id)) {
                       mkdir($destinationPath.$img->accom_id,0777,true);
                   }
                   $photo=$originPath.$img->accom_id.'/'.$file;
                   $picName=pathinfo($img->image);
                   $imagename = uniqid().'.'.$picName['extension']; 
                   $thumb='thumb_'.$imagename;
                   $slider='slider_'.$imagename;
                   $thumb_img=Image::make($photo)->resize(500,316);
                   $slider_img=Image::make($photo)->resize(830, null,function ($constraint){
                       $constraint->aspectRatio();
                   });
                   $featuredImg=DB::table('accomodations')->select('featured_image')->whereId($img->accom_id)->first();
                   if($featuredImg){
                     if($featuredImg->featured_image==$img->image){
                        DB::table('accomodations')->whereId($img->accom_id)->update(['featured_image'=>$imagename]);       
                     }  
                   } 
                   $image=new GalleryImage();
                   $image->accom_id=$img->accom_id;
                   $image->image=$imagename;
                   $image->thumb_image=$thumb;
                   $image->slider_image=$slider; 
                   if($image->save()) {
                       $thumb_img->save($destinationPath.$img->accom_id.'/'.$thumb,80);
                       $slider_img->save($destinationPath.$img->accom_id.'/'.$slider,80);
                       copy($originPath.$img->accom_id.'/'.$file,$destinationPath.$img->accom_id.'/'.$file);
                       rename($destinationPath.$img->accom_id.'/'.$file, $destinationPath.$img->accom_id.'/'.$imagename); 
                       $this->info('Image saved for Accommodation ID:'.$img->accom_id); 
                   }else{
                      $this->error('Image not saved for Accommodation ID:'.$img->accom_id); 
                   }
                 }
               }
               closedir($dh);
           }    
       }
       $this->info('All Images resized and moved');
    }
}
