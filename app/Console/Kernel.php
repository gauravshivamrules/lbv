<?php

namespace LBV\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [ 
        '\LBV\Console\Commands\Import',
        '\LBV\Console\Commands\ResizeImage'
    ]; 

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')  
        //          ->hourly();    
       // $schedule->command('CronJob.cronjob')->everyFiveMinutes();
    } 

    /**
     * Register the commands for the application.
     *
     * @return void
     */  
    protected function commands()  
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
