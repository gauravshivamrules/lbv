<?php
namespace LBV\Custom;
use LBV\Model\AdminNotification;
use LBV\Model\UserNotification;


class Notifications {
	
	static function notifyAdmin($args=array()) {
		if($args) {
			$notify=new AdminNotification();
			$notify->accom_id=$args['accom_id'];
			$notify->from_user=$args['from_user'];
			$notify->message=$args['message'];
			$notify->request_type_url=$args['request_type_url'];
			if($notify->save()) {
				return true;
			}
			return false;
		}
	}
	static function notifyUser($args=array()) {  
		if($args) {
			$notify=new UserNotification();
			$notify->accom_id=$args['accom_id'];
			$notify->to_user=$args['to_user'];
			$notify->message=$args['message'];
			$notify->request_type_url=$args['request_type_url'];
			if($notify->save()) {
				return true;
			}
			return false;
		}
	}
}