<?php

namespace LBV;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','email','phone_number','password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 
        'password', 'remember_token'
    ];


    public function setPasswordAttribute($password) {
        $this->attributes['password']=Hash::make($password);
    }

    public function accomodation() {
        return $this->hasMany('LBV\Model\Accomodation'); 
    }
    public function country() {
        return $this->hasOne('LBV\Model\Country'); 
    }




}
