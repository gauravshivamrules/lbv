<?php

namespace LBV\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Advertiser
{
    /** 
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
           if(Auth::user()->role_id !=3) {
             return redirect()->back()->with('error',__('Sorry,You are not authorized to access that location.'));   
           }   
        } 
        return $next($request);
    }
}
