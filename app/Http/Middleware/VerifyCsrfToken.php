<?php

namespace LBV\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /** 
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/accomodations/add_accom_images',
        '/accomodations/sort-images',
        '/tickets/contact-advertiser',
        '/tickets/contact-customer',
        '/tickets/contact-owner',
        '/tickets/contact-admin',
        '/tickets/update-message',
        '/bookings/add-noavability',
        '/subscription-licenses/mollie-webhook/{order}',
        '/search/ajax-search', 
        '/search/ajax-search-map'
    ];
}
