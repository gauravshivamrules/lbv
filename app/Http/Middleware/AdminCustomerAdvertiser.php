<?php

namespace LBV\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminCustomerAdvertiser   
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $roles=[1,2,3];
            if (!in_array(Auth::user()->role_id, $roles)) { 
                return redirect()->back()->with('error',__('Sorry,You are not authorized to access that location.'));  
            } 
        }   
        return $next($request); 
    }
}
