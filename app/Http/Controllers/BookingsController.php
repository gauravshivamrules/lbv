<?php

namespace LBV\Http\Controllers;
use DB;
use PDF;
use Mail;
use Mollie;
use LBV\Model\Room;
use LBV\Model\Booking;
use LBV\Model\BookingPayment;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables; 
use Illuminate\Support\Facades\Auth;
use LBV\Mail\SendReviewMail;
use LBV\Mail\SendBookingPaymentInvoice;
use LBV\Mail\NotifyAdvertiserForBooking;
use LBV\Mail\NotifyCustomerForBooking;
use LBV\Mail\NotifyAdminForBooking;
use LBV\Custom\Notifications;
use Validator;
 
class BookingsController extends Controller
{
   public function getNoAvaibility(){
		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id');  
		return view('bookings.no_avaibility',compact('accoms')); 
   }
   public function addNoAvaibility(Request $r){
   		if($r->accom){
   			if($r->start && $r->end){
               $roomID=$nor=null;
               if($r->roomid && $r->nor){
                  $roomID=$r->roomid;
                  $nor=$r->nor;
               }
   				$date=DB::table('booked_dates')->insertGetId([
   					'accom_id'=>$r->accom,
   					'start_date'=>date('Y-m-d',strtotime($r->start)).' 00:00:00',
   					'end_date'=>date('Y-m-d',strtotime($r->end)).' 23:59:59',
                  'room_id'=>$roomID,
                  'no_of_rooms'=>$nor,
   					'created_at'=>date('Y-m-d H:i:s'),
   					'updated_at'=>date('Y-m-d H:i:s')
   				]);
   				if($date){
   					return "true";
   				}else{
   					return "false";
   				}
   			}
   		}
   }
   public function getAccomNoAvaibility($accom){
   		if($accom){
   			$noAvabilityArr=$returnArr=[];
            $priceUnit=DB::table('accomodation_prices')->select('price_unit')->whereAccomId($accom)->first();
            $returnArr['priceUnit']=$priceUnit->price_unit;
   			$noAvability=DB::table('booked_dates')->whereAccomId($accom)->get();
            if($priceUnit->price_unit!='perweek'){
               $rooms=DB::table('rooms')->select('id','room_type','no_of_rooms')->whereAccomId($accom)->get()->toArray();
               $returnArr['rooms']=$rooms;
            } 
   			if($noAvability){
   				$i=0;
   				foreach ($noAvability as $nA) {
   					$noAvabilityArr[$i]['id']=$nA->id;
   					$noAvabilityArr[$i]['start']=$nA->start_date;
   					$noAvabilityArr[$i]['end']=$nA->end_date;
   					$noAvabilityArr[$i]['start_date']=date('d-m-Y',strtotime($nA->start_date));
   					$noAvabilityArr[$i]['end_date']=date('d-m-Y',strtotime($nA->end_date));
   					$noAvabilityArr[$i]['accom']=$accom;
                  if($nA->room_id){
                     $roomDetail=DB::table('rooms')->select('room_type')->whereId($nA->room_id)->first();
                     if($roomDetail){
                        $noAvabilityArr[$i]['title']=$nA->no_of_rooms.' '.$roomDetail->room_type.' '.__('NOT AVAILABLE');       
                        $noAvabilityArr[$i]['hover']=$nA->no_of_rooms.' '.$roomDetail->room_type.' '.__('NOT AVAILABLE');       
                     }
                  }else{
                     $noAvabilityArr[$i]['title']=__('NOT AVAILABLE');    
                     $noAvabilityArr[$i]['hover']=__('NOT AVAILABLE');
                  } 
   					
   					$noAvabilityArr[$i]['allDay']=false;
   					$i++;
   				}
   			}
            $returnArr['noAvability']=$noAvabilityArr;
   			echo json_encode($returnArr);die; 
   		}
   }
   public function getNoOfRooms($room){ 
      $r=DB::table('rooms')->select('no_of_rooms')->whereId($room)->first();
      return $r->no_of_rooms;
   }
   public function updateNoAvaibility(Request $r){
   		if($r->id){
   			$update=DB::table('booked_dates')->whereId($r->id)->update([
   				'accom_id'=>$r->accom_id,
   				'start_date'=>date('Y-m-d',strtotime($r->start_date)).' 00:00:00',
   				'end_date'=>date('Y-m-d',strtotime($r->end_date)).' 23:59:59',
   				'updated_at'=>date('Y-m-d H:i:s')
   			]);
   			if($update){
   				return redirect()->back()->with('success',__('Updated successfully'));
   			}
   			return redirect()->back()->with('error',__('Internal error,please try again!'));
   		}
   }
   public function deleteNoAvaibility($id){
   		if($id){
   			$delete=DB::table('booked_dates')->whereId($id)->delete();
   			if($delete){
				return redirect()->back()->with('success',__('Deleted successfully'));
   			}
   			return redirect()->back()->with('error',__('Internal error,please try again!'));
   		}
   }
   public function getAvabilityDates(Request $r){
      $reqSDate=$r->year.'-'.$r->month.'-01';
      $reqEDate=$r->year.'-'.$r->month.'-31';
      $priceType=DB::table('accomodation_prices')->select('price_unit')->whereAccomId($r->accom_id)->first();
      if($priceType){
         if($priceType->price_unit=='perweek'){
               $avability=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday')
                     ->whereAccomId($r->accom_id)
                     /*->where('start_date','<=',$reqSDate)
                     ->where('end_date','>=',$reqEDate)*/
                     ->whereMonth('start_date','=',$r->month)
                     ->whereYear('start_date','=',$r->year)
                     ->get();
               if($avability->isEmpty()){
                  $avability=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday')
                        ->whereAccomId($r->accom_id)
                        /*->where('start_date','<=',$reqSDate)
                        ->where('end_date','>=',$reqEDate)*/
                        ->whereMonth('end_date','=',$r->month)
                        ->whereYear('end_date','=',$r->year)
                        ->get();
                     if($avability->isEmpty()){
                        $avability=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday')
                              ->whereAccomId($r->accom_id)
                              ->where('end_date','>=',$reqSDate)
                              /*->where( DB::raw('MONTH(end_date)'), '=', date('m',strtotime($reqEDate)) )
                              ->where( DB::raw('YEAR(end_date)'), '=', date('Y',strtotime($reqEDate)) )*/
                              ->get();      
                     }
                  
               }
         }else{ 
            $avability=DB::table('room_prices')->select('id','accom_id','start_date','end_date','price','price1','price2','price3')
                  ->whereAccomId($r->accom_id)
                  /*->where('start_date','<=',$reqSDate)
                  ->where('end_date','>=',$reqEDate)*/
                  ->whereMonth('start_date','=',$r->month)
                  ->whereYear('start_date','=',$r->year)
                  ->get();
            if($avability->isEmpty()){
               $avability=DB::table('room_prices')->select('id','accom_id','start_date','end_date','price','price1','price2','price3')
                     ->whereAccomId($r->accom_id)
                     /*->where('start_date','<=',$reqSDate)
                     ->where('end_date','>=',$reqEDate)*/
                     ->whereMonth('end_date','=',$r->month)
                     ->whereYear('end_date','=',$r->year)
                     ->get();
                  if($avability->isEmpty()){
                     $avability=DB::table('room_prices')->select('id','accom_id','start_date','end_date','price','price1','price2','price3')
                           ->whereAccomId($r->accom_id)
                           ->where('end_date','>=',$reqSDate)
                           /*->where( DB::raw('MONTH(end_date)'), '=', date('m',strtotime($reqEDate)) )
                           ->where( DB::raw('YEAR(end_date)'), '=', date('Y',strtotime($reqEDate)) )*/
                           ->get();      
                  }
               
            }
         }
      } 
      $changeDay=DB::table('accomodation_prices')->select('changeday')->whereAccomId($r->accom_id)->first();
      $booked=DB::table('booked_dates')->whereAccomId($r->accom_id)->get();
      $returnArr=[];

      if($avability){
         $i=0;
         $range=[];
         foreach ($avability as $a){
            $range['date'][]=$this->dateRange($a->start_date,$a->end_date);
            $i++;
         }
         //dump($range);  
         $newDateRange=[];
         if($range){
            foreach($range['date'] as $d){
               foreach($d as $date){
                  $newDateRange[]=$date;    
               }
               $j=0;
               foreach($newDateRange as $nDate){
                  $returnArr[$j]['date']=$nDate;    
                  $returnArr[$j]['classname']='label label-success';
                  $title="<span class='label label-success'></span>";
                  //$returnArr[$j]['title']=$title;
                  $j++;
               }
            }
         }
      }
      if($priceType=='perweek'){
            if($booked){
             $returnArrB=$bRange=[];
             $k=$z=0;
             foreach ($booked as $a){
              $bRange['date'][]=$this->dateRange($a->start_date,$a->end_date);
              $k++;
            }
            if($bRange){
              $newBRange=[];
              foreach($bRange['date'] as $nDate){
               foreach($nDate as $date){
                $newBRange[]=$date;    
              }
              $jz=0;
              foreach ($newBRange as $bDate) {
                $returnArrB[$jz]['date']=$bDate;    
                $returnArrB[$jz]['classname']='label label-danger';
                $returnArrB[$jz]['title']='Not Available';
                $jz++;
              }
              $z++;
            }
          }
        }
      }else{
        if($booked){
               $returnArrB=$bRange=[];
               $k=$z=$ab=0;
               $allBookedRooms=[];
               $allBookedDates=[];
               foreach ($booked as $a){
                  $checkIfAllBooked=DB::table('rooms')->select('no_of_rooms')->whereId($a->room_id)->first();
                  if($a->no_of_rooms>=$checkIfAllBooked->no_of_rooms){
                      $allBookedDates[$ab]['start']=$a->start_date;
                      $allBookedDates[$ab]['end']=$a->end_date;
                      $allBookedRooms[]=$a->room_id;
                      $ab++;
                  }
               }
               $checkIfLeft=DB::table('rooms')->whereAccomId($r->accom_id)->whereNOTIn('id',$allBookedRooms)->get();
               if(!$checkIfLeft){
                  foreach ($allBookedDates as $b){
                     $bRange['date'][]=$this->dateRange($b->start_date,$b->end_date);
                     $k++;
                  }
               }

               if($bRange){
                  $newBRange=[];
                  foreach($bRange['date'] as $nDate){
                     foreach($nDate as $date){
                        $newBRange[]=$date;    
                     }
                     $jz=0;
                     foreach ($newBRange as $bDate) {
                        $returnArrB[$jz]['date']=$bDate;    
                        $returnArrB[$jz]['classname']='label label-danger';
                        $returnArrB[$jz]['title']='Not Available';
                        $jz++;
                     }
                     $z++;
                  }
               }
            }
      }
      $f=0;
      foreach ($returnArr as $val) {
         foreach ($returnArrB as $value) {
            if($value['date']==$val['date']){
               $returnArr[$f]['classname']='label label-danger';
               $returnArr[$f]['title']='Not Avalable';
            }
         }
         $f++;
      }
      echo json_encode($returnArr);die;
   } 
   public function showPrices(Request $r){
      $title='PRICE GOES HERE';
      if($r->accom_id && $r->date){
         $chunks=explode('_',$r->date);
         $month=date('m',strtotime($chunks[3]));
         $year=date('Y',strtotime($chunks[3]));
         $reqSDate=$year.'-'.$month.'-01';
         $reqEDate=$year.'-'.$month.'-31';
         $rooms=[];
         $priceType=DB::table('accomodation_prices')->select('price_unit')->whereAccomId($r->accom_id)->first();
        if($priceType->price_unit=='perweek'){
                   $prices=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday','extra_price_per_person')
                         ->whereAccomId($r->accom_id)
                         /*->where('start_date','<=',$reqSDate)
                         ->where('end_date','>=',$reqEDate)*/ 
                         ->whereMonth('start_date', '=', $month)
                         ->whereYear('start_date', '=', $year)
                         ->get(); 
                   if($prices->isEmpty()){
                      $prices=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday','extra_price_per_person')
                            ->whereAccomId($r->accom_id)
                            /*->where('start_date','<=',$reqSDate)
                            ->where('end_date','>=',$reqEDate)*/
                            ->whereMonth('end_date', '=', $month)
                            ->whereYear('end_date', '=', $year)
                            ->get();
                      if($prices->isEmpty()){
                         $prices=DB::table('accomodation_prices')->select('id','accom_id','start_date','end_date','price_unit','amount','max_persons','min_days','changeday','extra_price_per_person')
                               ->whereAccomId($r->accom_id)
                               /*->where('start_date','<=',$reqSDate)
                               ->where('end_date','>=',$reqEDate)*/
                               ->whereMonth('end_date', '>=', $month)
                               ->whereYear('end_date', '>=', $year)
                               ->get();
                      } 
                   }
        }else{
             if($priceType->price_unit=='perroom'){
                $prices=Room::with('room_price')->whereHas('room_price',function($query) use ($month,$year){
                      $query->orderBy('price','ASC');
                      $query->whereMonth('start_date', '=', $month);
                      $query->whereYear('start_date', '=', $year);
                })
                ->whereAccomId($r->accom_id)
                ->get();   
                if($prices->isEmpty()){
                   $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($month,$year){
                         $query->orderBy('price','ASC');
                         $query->whereMonth('end_date', '=', $month);
                         $query->whereYear('end_date', '=', $year);
                   })
                   ->whereAccomId($r->accom_id)
                   ->get();   
                   if($prices->isEmpty()){
                      $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($month,$year){
                            $query->orderBy('price','ASC');
                            $query->whereMonth('end_date', '>=', $month);
                            $query->whereYear('end_date', '>=', $year);
                      })
                      ->whereAccomId($r->accom_id)
                      ->get(); 
                      if($prices->isEmpty()){
                        $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($reqSDate){
                             $query->orderBy('price1','ASC');
                             $query->where('end_date', '>=', $reqSDate);
                       })
                        ->whereAccomId($r->accom_id)
                        ->get(); 
                      }  
                   }
                }
             }else{
               $prices=Room::with('room_price')->whereHas('room_price',function($query) use ($month,$year){
                     $query->orderBy('price1','ASC');
                     $query->whereMonth('start_date', '=', $month);
                     $query->whereYear('start_date', '=', $year);
               })
               ->whereAccomId($r->accom_id)
               ->get();   
               if($prices->isEmpty()){
                  $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($month,$year){
                        $query->orderBy('price1','ASC');
                        $query->whereMonth('end_date', '=', $month);
                        $query->whereYear('end_date', '=', $year);
                  })
                  ->whereAccomId($r->accom_id)
                  ->get();   
                  if($prices->isEmpty()){
                     $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($month,$year){
                           $query->orderBy('price1','ASC');
                           $query->whereMonth('end_date', '>=', $month);
                           $query->whereYear('end_date', '>=', $year);
                     })
                     ->whereAccomId($r->accom_id)
                     ->get(); 
                    if($prices->isEmpty()){
                      $prices=Room::with('room_price')->whereHas('room_price',function($query)use ($reqSDate){
                           $query->orderBy('price1','ASC');
                           $query->where('end_date', '>=', $reqSDate);
                     })
                      ->whereAccomId($r->accom_id)
                      ->get(); 
                    }  
                  }
               }
             }  
        }

         if(!$prices->isEmpty()){
            $changeDay=DB::table('accomodation_prices')->select('changeday')->whereAccomId($r->accom_id)->first();
            $minDays=DB::table('accomodation_prices')->select('min_days')->whereAccomId($r->accom_id)->first();
            $changeDay=$changeDay->changeday;
            $minDays=$minDays->min_days;
            return view('accomodations.price_hover',compact('prices','priceType','changeDay','minDays')); 
         }else{
            echo false;
         }  
      }
      
   } 
   public function getBookingPrice(Request $r){
      $month=date('m',strtotime($r->start));
      $year=date('Y',strtotime($r->start));
      $rooms=[];
      $priceType=DB::table('accomodation_prices')->select('price_unit')->whereAccomId($r->accom_id)->first();
      if($priceType->price_unit=='perweek'){
         $prices=DB::table('accomodation_prices')->select('id','amount')
               ->whereAccomId($r->accom_id)
               /*->where('start_date','<=',$reqSDate)
               ->where('end_date','>=',$reqEDate)*/ 
               ->whereMonth('start_date', '=', $month)
               ->whereYear('start_date', '=', $year)
               ->get(); 
         if($prices->isEmpty()){
            $prices=DB::table('accomodation_prices')->select('id','amount')
                  ->whereAccomId($r->accom_id)
                  /*->where('start_date','<=',$reqSDate)
                  ->where('end_date','>=',$reqEDate)*/
                  ->whereMonth('end_date', '=', $month)
                  ->whereYear('end_date', '=', $year)
                  ->get();
            if($prices->isEmpty()){
               $prices=DB::table('accomodation_prices')->select('id','amount')
                     ->whereAccomId($r->accom_id)
                     /*->where('start_date','<=',$reqSDate)
                     ->where('end_date','>=',$reqEDate)*/
                     ->whereMonth('end_date', '>=', $month)
                     ->whereYear('end_date', '>=', $year)
                     ->get();
            } 
         }
      }else{
         if($priceType->price_unit=='perroom'){
            $prices=Room::with('room_price')->whereHas('room_price',function($query) use ($month,$year) {
               $query->whereMonth('start_date', '=', $month)->whereYear('start_date', '=', $year);
               $query->orderBy('price','ASC');
            })->whereAccomId($r->accom_id)->get();
            if($prices->isEmpty()){
               $prices=Room::with('room_price')->whereHas('room_price',function($query) use ($month,$year) {
                  $query->whereMonth('end_date', '=', $month)->whereYear('end_date', '=', $year);
                  $query->orderBy('price','ASC');
               })->whereAccomId($r->accom_id)->get();
            }
            if($prices->isEmpty()){
               $prices=Room::with('room_price')->whereHas('room_price',function($query) use ($month,$year) {
                  $query->whereMonth('end_date', '>=', $month)->whereYear('end_date', '>=', $year);
                  $query->orderBy('price','ASC');
               })->whereAccomId($r->accom_id)->get();
            }   
         }else{
            $prices=Room::with('room_price')->whereHas('room_price',function($query){
                  $query->orderBy('price1','ASC');
            })->whereAccomId($r->accom_id)->get();  
         } 
      }
      // dd($prices);
   }
   public function editBooking($id){ 
      if ($id) {
         $booking=DB::table('bookings')->select('bookings.id','bookings.status','bookings.book_to','bookings.book_from',
            'bookings.total_cost','bookings.extra_charges','bookings.adult','bookings.child','bookings.comments','bookings.booking_amount',
            'bookings.room_id','bookings.room_count','bookings.adv_comments',
            'accomodations.name','C.first_name as fName','C.last_name as lName','C.email as cEmail','C.phone_number as cPhone',
            'A.first_name as aFname','A.last_name as aLname','A.email as aEmail','A.phone_number as APhone' 
            )
         ->leftJoin('accomodations', 'bookings.accom_id', '=', 'accomodations.id')
         ->leftJoin('users as C', 'bookings.user_id', '=', 'C.id')
         ->leftJoin('users as A', 'bookings.advertiser_id', '=', 'A.id')
         ->where('bookings.id',$id)
         ->first();
         return view('bookings.edit',compact('booking'));
      }
   }
   public function postEditBooking(Request $r,$id){
      if($id){
         $update=Booking::whereId($id)->first();
         if($update){
            $update->book_from=$r->booking_start_date;
            $update->book_to=$r->booking_end_date;
            $update->adult=$r->adult;
            $update->booking_amount=$r->booking_amount;
            $update->extra_charges=$r->extra_charges;
            $update->total_cost=$r->total_cost;
            $update->adv_comments=$r->adv_comments;
            if(isset($r->child)){
               $update->child=$r->child;
            }
            if($update->save()){ 
               // notify customer via mail/notification for update 
               $booking=DB::table('bookings')->select('user_id','accom_id')->whereId($id)->first();
               Notifications::notifyUser([  
                  'accom_id'=>$booking->accom_id,
                  'message'=>'New price has been quoted for your booking request for property '.$this->propertyName($booking->accom_id),  
                  'to_user'=>$booking->user_id,  
                  'request_type_url'=>'/bookings/my-bookings' 
               ]);
               if(isset($r->notifyAdv)){
                  $custEmail=DB::table('users')->select('email')->whereId($booking->user_id)->first();
                  $url=url('/').'/bookings/my-bookings'; 
                  $this->notifyCustomerForBooking($custEmail->email,
                          'New price quoted for your booking request for propety '.$this->propertyName($booking->accom_id),
                          'New price has been quoted for your booking request by owner for property '.$this->propertyName($booking->accom_id).' .Please login to check',
                          $url
                  );
               } 
               return redirect('/bookings/adv-bookings')->with('success',__('Booking updated'));
            }
         }
         return redirect()->back()->with('error',__('Sorry,booking could not updated,please try again!'));
      } 
   }
   public function approveBooking($id){
      if($id){
        $approve=DB::table('bookings')->whereId($id)->update(['status'=>1]);
        if($approve){
           $b=DB::table('bookings')->whereId($id)->first();
           $booked=false;
            if($b->price_type){
              if($b->price_type=='perweek'){
                  $booked=DB::table('booked_dates')->insertGetId([
                     'accom_id'=>$b->accom_id,
                     'start_date'=>date('Y-m-d',strtotime($b->book_from)).' 00:00:00',
                     'end_date'=>date('Y-m-d',strtotime($b->book_to)).' 23:59:59' ,
                     'created_at'=>date('Y-m-d H:i:s'),
                     'updated_at'=>date('Y-m-d H:i:s')
                  ]);
              }else{
                  if($b->rooms_json){
                    $roomJson=json_decode($b->rooms_json);
                    foreach($roomJson as $r) {
                         $booked=DB::table('booked_dates')->insertGetId([
                            'accom_id'=>$b->accom_id,
                            'start_date'=>date('Y-m-d',strtotime($b->book_from)).' 00:00:00',
                            'end_date'=>date('Y-m-d',strtotime($b->book_to)).' 23:59:59' ,
                            'room_id'=>$r->room_id,
                            'no_of_rooms'=>$r->room_count,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                         ]);  
                    }
                  }    
              }
            }
           if($booked){
              Notifications::notifyUser([  
                 'accom_id'=>$b->accom_id,
                 'message'=>'Your request for booking has been approved for property '.$this->propertyName($b->accom_id),  
                 'to_user'=>$b->user_id,  
                 'request_type_url'=>'/bookings/my-bookings'
              ]); 
              $custEmail=DB::table('users')->select('email')->whereId($b->user_id)->first();
              $url=url('/').'/bookings/my-bookings'; 
              $this->notifyCustomerForBooking($custEmail->email,
                      'Your booking request has been approved for property '.$this->propertyName($b->accom_id),
                      'Your booking request has been approved by owner for property '.$this->propertyName($b->accom_id).' .Please login to check',
                      $url
              );
              $paymentType=DB::table('accomodations')->select('payment_type')->whereId($b->accom_id)->first();
              if($paymentType->payment_type==2){
                  // send advtiser invoice for 10% commission
                  $commission=$b->total_cost/10;
                  $commissionId=DB::table('booking_commissions')->insertGetId([
                     'booking_id'=>$b->id,
                     'user_id'=>Auth::user()->id, 
                     'total_amount'=>$b->total_cost,
                     'commission'=>$commission,
                     'created_at'=>date('Y-m-d H:i:s'),
                     'updated_at'=>date('Y-m-d H:i:s')
                  ]);
                  if($commissionId){
                     $key=uniqid('LBV-'); 
                     $payment=DB::table('booking_payments')->insertGetId([
                        'booking_commission_id'=>$commissionId,
                        'amount'=>$commission,
                        'invoice_key'=>$key,
                        'pay_method_id'=>6,
                        'user_id'=>Auth::user()->id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                     ]); 
                     if($payment){
                        $invoice_number=env('SITE_BOOKING_INVOICE_KEY').$payment;
                        $update=DB::table('booking_payments')->whereId($payment)->update(['invoice_number'=>$invoice_number]);
                        Notifications::notifyUser([  
                           'accom_id'=>$b->accom_id, 
                           'message'=>'10% commission invoice has been generated for booking invoice '.$invoice_number.'.Please pay your invoice soon.',  
                           'to_user'=>$b->advertiser_id,  
                           'request_type_url'=>'/bookings/invoices'
                        ]); 
                        // send invoice mail
                        $user=DB::table('users')->whereId($b->advertiser_id)->select('email')->first();
                        $this->sendBookingCommissionInvoice($key,$user->email,'10% commission invoice has been generated,please login to pay your commission for invoice number '.$invoice_number,$invoice_number,$url);
                     }
                  }
              }
              return redirect()->back()->with('success',__('Booking request approved')); 
           } 
           return redirect()->back()->with('error',__('Booking request could not be approved,please try again'));
        }
      }   
   }
   public function sendBookingCommissionInvoice($key,$user,$msg,$invoice_number,$url) {
       $file=$this->buildBookingInvoice($key); 
       $file->save(public_path('files/pdf/products/invoice.pdf'));  
       try {
           Mail::to($user)->send(new SendBookingPaymentInvoice(Auth::user(),$msg,$url));  
           \Log::log('BookingCommissionInvoiceSent','New booking commission invoice sent to '.$user); 
           unlink(public_path('files/pdf/products/invoice.pdf'));   
       } catch(Exception $e) { 
           Notifications::notifyAdmin([  
                   'accom_id'=>$accom,
                   'from_user'=>Auth::user()->id,
                   'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                   'request_type_url'=>'/subscription_licenses/licenses'
           ]);
            \Log::log('BookingCommissionInvoiceSent','Booking invoice not sent to user '.Auth::user()->id.' for invoice ' .$invoice_number.' server returned error: '.$e->getMessage());   
            if(Auth::user()->role_id==1) {
               // return redirect('/promotion-licenses/license')->with('error','Something went wrong while sending mail.Server returned error: '.$e->getMessage());
            } else {
               return redirect('/bookings/adv-bookings')->with('error','Something went wrong while sending mail,incident has been reported to administrator.please ask administrator to send invoice if not received.Server returned error: '.$e->getMessage());
            }
       }     
   }
   public function buildBookingInvoice($key=null) {
       if($key) {
           $licenses=BookingPayment::where('booking_payments.invoice_key', $key) 
                   ->leftJoin('pay_methods','booking_payments.pay_method_id','=','pay_methods.id')
                   ->leftJoin('users','booking_payments.user_id','=','users.id')
                   ->leftJoin('countries','users.country_id','=','countries.id')
                   ->leftJoin('booking_commissions','booking_commissions.id','=','booking_payments.booking_commission_id')
                   ->leftJoin('mollie_booking_payments','mollie_booking_payments.order_id','=','booking_payments.transaction_id') 
                   ->select(
                           'booking_payments.invoice_number','pay_methods.method_name as paymentName','booking_payments.created_at',
                           'booking_payments.amount','booking_payments.pay_method_id','booking_payments.status',
                           'users.first_name','users.last_name','users.city','users.email','users.user_company','users.id as user_id',
                           'users.country_id','users.phone_number','users.street','users.street_number','users.post_code','users.btw_number',
                           'booking_commissions.total_amount','mollie_booking_payments.method'
                   )
                   ->get();  
           $file= PDF::loadView('bookings.pdf.invoice',compact('licenses'));
           return $file;
       } else {
           return false;
       }   
   }
   public function generateBookingInvoice($key){
      $f=$this->buildBookingInvoice($key);
      return $f->stream(); 
   }
   public function rejectBooking($id){
      if($id){
         $reject=DB::table('bookings')->whereId($id)->update(['status'=>3]);
         if($reject){
            $b=DB::table('bookings')->whereId($id)->first();
            Notifications::notifyUser([  
               'accom_id'=>$b->accom_id,
               'message'=>'Your booking request has been rejected for property '.$this->propertyName($b->accom_id),  
               'to_user'=>$b->user_id,  
               'request_type_url'=>'/bookings/my-bookings'
            ]); 
            $custEmail=DB::table('users')->select('email')->whereId($b->user_id)->first();
            $url=url('/').'/bookings/my-bookings'; 
            $this->notifyCustomerForBooking($custEmail->email,
                    'Your booking request has been rejected for property '.$this->propertyName($b->accom_id),
                    'Your booking request has been rejected by owner for property '.$this->propertyName($b->accom_id).' .Please login to check',
                    $url
            );
            return redirect()->back()->with('success',__('Booking request rejected')); 
         }
         return redirect()->back()->with('error',__('Booking request could not be rejected,please try again'));
      }
      return redirect()->back()->with('error',__('Sorry,id is required to proceed'));
   }
   public function completeBooking($id){
      if($id){
         $booking_token=uniqid();
         $complete=DB::table('bookings')->whereId($id)->update(['status'=>2,'booking_token'=>$booking_token]);
         if($complete){
            $b=DB::table('bookings')->whereId($id)->first();
            Notifications::notifyUser([  
               'accom_id'=>$b->accom_id,
               'message'=>'Your booking request has been completed for property '.$this->propertyName($b->accom_id),  
               'to_user'=>$b->user_id,  
               'request_type_url'=>'/bookings/my-bookings'
            ]); 
            $reviewLink=url('/').'/bookings/write-review/'.$id.'/'.$booking_token;
            $custEmail=DB::table('users')->select('email')->whereId($b->user_id)->first();
            $this->sendReviewMail($custEmail->email,
                    'Please tell us your experience with property '.$this->propertyName($b->accom_id),
                    'Please tell us your experience with property '.$this->propertyName($b->accom_id).' by writing a review.Please click on the link below to write a review',
                    $reviewLink
            );
            return redirect()->back()->with('success',__('Booking request completed')); 
         }
         return redirect()->back()->with('error',__('Booking request could not be completed,please try again'));
      }
      return redirect()->back()->with('error',__('Sorry,id is required to proceed'));
   }
   public function writeReview($id,$bookingToken){
      if($id){
         $book=DB::table('bookings')->whereId($id)->whereTokenUsed(0)->first(); 
         if($book){
            if($book->booking_token==$bookingToken){
               if($book->status==2){
                  return view('bookings.review',compact('bookingToken'));
               }
            }
            return redirect('/')->with('error',__('Invalid request'));
         }
         return redirect('/')->with('error',__('Sorry that token is expired'));
      }
      return redirect('/')->with('error',__('Invalid request'));
   }
   public function postWriteReview(Request $r){
      if($r->review){
         $accomid=DB::table('bookings')->select('id','accom_id','user_id')->whereBookingToken($r->bookingToken)->first();
         $rate=DB::table('rates')->insertGetId([
            'rate_code'=>$r->bookingToken,
            'rate_description'=>$r->review,
            'ratestate_id'=>3,
            'user_id'=>$accomid->user_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
         ]);
         if($rate){
            $accomRate=DB::table('accomodation_ratings')->insertGetId([
               'accom_id'=>$accomid->accom_id,
               'rate_id'=>$rate,
               'created_at'=>date('Y-m-d H:i:s'),
               'updated_at'=>date('Y-m-d H:i:s')
            ]);
            if($accomRate){
               if($r->rate){
                  $error='';
                  foreach ($r->rate as $key => $value) {
                     $rateCategory=DB::table('rate_ratecategories')->insertGetId([
                        'rate_id'=>$rate,
                        'ratecategory_id'=>$key,
                        'score'=>$value,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                     if(!$rateCategory){
                        $error='ERROR';
                     }    
                  } 
                  if($error==''){
                     DB::table('bookings')->whereId($accomid->id)->update(['token_used'=>1]);
                     return redirect('/login')->with('success',__('Thanks for writing review'));
                  }
                  return redirect()->back()->with('error',__('Sory something went wrong,please try again'));
               }
            }
         }
      }
      return redirect()->back()->with('error',__('Please write your review'));
   }
   public function getViewBooking($booking){
      $sendBooking=DB::table('bookings')->select('bookings.id','bookings.accom_id','bookings.status','bookings.book_to','bookings.book_from',
         'bookings.total_cost','bookings.extra_charges','bookings.adult','bookings.child','bookings.comments','bookings.booking_amount',
         'bookings.rooms_json','bookings.adv_comments',
         'accomodations.name','C.first_name as fName','C.last_name as lName','C.email as cEmail','C.phone_number as cPhone',
         'A.first_name as aFname','A.last_name as aLname','A.email as aEmail','A.phone_number as APhone' ,
         'booking_commissions.status as bookStatus','booking_commissions.id as bookId'
         )
      ->leftJoin('accomodations', 'bookings.accom_id', '=', 'accomodations.id')
      ->leftJoin('users as C', 'bookings.user_id', '=', 'C.id')
      ->leftJoin('users as A', 'bookings.advertiser_id', '=', 'A.id')
      ->leftJoin('booking_commissions', 'bookings.id', '=', 'booking_commissions.booking_id')
      ->where('bookings.id',$booking)
      ->first();
      return $sendBooking;
   } 
   public function allotRooms(Request $r){
     $bookedDates=DB::table('booked_dates')->whereAccomId($r->accom_id)
        /*->whereMonth('start_date','=',date('m',strtotime($start)))
        ->whereYear('start_date','=',date('Y',strtotime($start)))*/
        ->whereDate('start_date', '>=', date('Y-m-d',strtotime($r->start)))
        ->whereDate('end_date', '<=', date('Y-m-d',strtotime($r->end)))
        ->get();
      if($bookedDates){
         $k=$z=0;
         $allBookedRooms=$halfBookd=[];
         foreach ($bookedDates as $a){
            $checkIfAllBooked=DB::table('rooms')->select('no_of_rooms')->whereId($a->room_id)->first();
            if($a->no_of_rooms>=$checkIfAllBooked->no_of_rooms){
                $allBookedRooms[]=$a->room_id;
            }else{
                $halfBookd[$k]['room']=$a->room_id;
                $halfBookd[$k]['count']=$a->no_of_rooms;
            }
            $k++;
         }
      }
      $accom=$adult=$child=null;
      $accom=$r->accom_id;
      $adult=$r->adults;
      $child=$r->childs;
      $rooms=Room::with(array('room_price'=>function($query){
         $query->select('id','price','price1','price2','price3','room_id','extra_price_adult','extra_price_child');
        }))
        ->select('rooms.id','rooms.room_type','rooms.adults','rooms.childs','rooms.no_of_rooms','rooms.min_days','rooms.max_adults_per_room','rooms.max_persons_per_room')
        ->whereAccomId($accom)
        ->whereNOTIn('rooms.id',$allBookedRooms)
        /*->when($adult, function($query) use ($adult){
            return $query->where('adults','>=',$adult);
        })
        ->when($child, function($query) use ($child){
            return $query->where('childs','>=',$child);
        })*/
        ->get();

      if($rooms){
        foreach($rooms as $r){
            foreach ($halfBookd as $hB) {
              if($r->id==$hB['room']){
                 $r->no_of_rooms=$r->no_of_rooms-$hB['count'];
              } 
            }
        }
      }



      if($rooms->isEmpty()){
          $rooms=Room::with(array('room_price'=>function($query){
              $query->select('id','price','price1','price2','price3','room_id','extra_price_adult','extra_price_child');
            }))
            ->select('rooms.id','rooms.room_type','rooms.adults','rooms.childs','rooms.no_of_rooms','rooms.min_days','rooms.max_adults_per_room','rooms.max_persons_per_room')
            ->whereAccomId($accom)
            ->whereNOTIn('rooms.id',$allBookedRooms)
            ->when($adult, function($query) use ($adult){
                return $query->where('adults','<=',$adult);
            })
            ->when($child, function($query) use ($child){
                return $query->where('childs','<=',$child);
            })
            ->get();
      } 
      $response['success']=false;
      if(!$rooms->isEmpty()){
        $response['success']=true;
        $response['rooms']=$rooms;
      }
      echo json_encode($response);die;
   }
   public function makeBooking(Request $r){
      $r->validate( 
          [
            'booking_start_date' => 'required',
            'booking_end_date' => 'required',
            'adults' => 'required'
          ],[
            'booking_start_date.required' => __('Please select start date'),
            'booking_end_date.required' => __('Please select end date'),
            'adults.required' => __('Please select adults')
          ]
      ); 
      if($r->price_unit){
         $extraCharges=DB::table('accomodation_price_extra_charges')->select(DB::raw("SUM(amount) as extraAmount"))->whereAccomId($r->accom_id)->whereIsOptional(0)->get();
         $totalCost=$extraCharges[0]->extraAmount+$r->amount;
         $advertiser_id=DB::table('accomodations')->select('user_id','email')->whereId($r->accom_id)->first();
         if($r->price_unit=='perweek'){ 
            $bookedDates=DB::table('booked_dates')->whereAccomId($r->accom_id) 
              ->whereDate('start_date', '>=', date('Y-m-d',strtotime($r->booking_start_date)))
              ->whereDate('end_date', '<=', date('Y-m-d',strtotime($r->booking_end_date)))
              ->get();

            if($bookedDates){
                return redirect()->back()->with('error',__('Sorry,we are already full for this date'));
            } 
            $booking=DB::table('bookings')->insertGetId([
               'accom_id'=>$r->accom_id,
               'user_id'=>Auth::user()->id,
               'booking_amount'=>$r->amount,
               'book_from'=>date('Y-m-d',strtotime($r->booking_start_date)),
               'book_to'=>date('Y-m-d',strtotime($r->booking_end_date)),
               'created_at'=>date('Y-m-d H:i:s'),
               'updated_at'=>date('Y-m-d H:i:s'),
               'comments'=>$r->comments,
               'extra_charges'=>$extraCharges[0]->extraAmount,
               'total_cost'=>$totalCost,
               'adult'=>$r->adults, 
               'child'=>$r->childs,
               'advertiser_id'=>$advertiser_id->user_id,
               'price_type'=>$r->price_unit
            ]);
            if($booking){
               // notify advertiser about booking by mail & notification
               Notifications::notifyUser([  
                  'accom_id'=>$r->accom_id,
                  'message'=>'You have a new booking request for property '.$this->propertyName($r->accom_id),  
                  'to_user'=>$advertiser_id->user_id,  
                  'request_type_url'=>'/bookings/adv-bookings'
               ]);  
               $advEmail=DB::table('users')->select('email')->whereId($advertiser_id->user_id)->first();
               $sendBooking=$this->getViewBooking($booking);
               if($this->checkAccomodationPaymentType($r->accom_id)==2){
                     if($sendBooking){
                        if($sendBooking->status==0 || $sendBooking->status==3){
                              $sendBooking->cEmail='*********';
                              $sendBooking->cPhone='*********';
                        }
                     }                     
               }
               $url=url('/').'/bookings/view/'.$booking; 
               $this->notifyAdvertiserForBooking($advEmail->email,
                       'New booking request has been received',
                       'A new booking request has been received for accomodation '.$this->propertyName($r->accom_id).' .Please login to check',
                       $url
               ); 
               //notify admin for free accom boking
               if($this->checkAccomodationPaymentType($r->accom_id)==2){
                  $this->notifyAdminForBooking(
                     'New booking request has been received for free property '.$this->propertyName($r->accom_id),
                     'New booking request has been received for free property '.$this->propertyName($r->accom_id).' .Please login to check'
                  );
               }
               return redirect('/bookings/my-bookings')->with('success',__('Your booking request has been sent to owner')); 
            }
            return redirect()->back()->with('error',__('Something went wrong,please try again'));   
         }else {
            if($r->validate_booking_persons){
                $bookedDates=DB::table('booked_dates')->whereAccomId($r->accom_id) 
                  ->whereDate('start_date', '>=', date('Y-m-d',strtotime($r->booking_start_date)))
                  ->whereDate('end_date', '<=', date('Y-m-d',strtotime($r->booking_end_date)))
                  ->get();
                if($bookedDates){
                   $k=$z=$ab=0;
                   $allBookedRooms=[];
                   foreach ($bookedDates as $a){
                      $checkIfAllBooked=DB::table('rooms')->select('no_of_rooms')->whereId($a->room_id)->first();
                      if($checkIfAllBooked){
                        if($a->no_of_rooms>=$checkIfAllBooked->no_of_rooms){
                            $allBookedRooms[]=$a->room_id;
                            $ab++;
                        }
                      }
                   }
                   $checkIfLeft=DB::table('rooms')->whereAccomId($r->accom_id)->whereNOTIn('id',$allBookedRooms)->get();
                   if(!$checkIfLeft){
                     return redirect()->back()->with('error',__('Sorry,we are already full for this date'));
                   }
                }  
                $booking=DB::table('bookings')->insertGetId([
                   'accom_id'=>$r->accom_id,
                   'user_id'=>Auth::user()->id,
                   'booking_amount'=>$r->amount,
                   'book_from'=>date('Y-m-d',strtotime($r->booking_start_date)),
                   'book_to'=>date('Y-m-d',strtotime($r->booking_end_date)),
                   'created_at'=>date('Y-m-d H:i:s'),
                   'updated_at'=>date('Y-m-d H:i:s'),
                   'comments'=>$r->comments,
                   'extra_charges'=>$extraCharges[0]->extraAmount,
                   'total_cost'=>$totalCost,
                   'adult'=>$r->adults, 
                   'child'=>$r->childs,
                   'advertiser_id'=>$advertiser_id->user_id,
                   'rooms_json'=>$r->rooms_arr,
                   'price_type'=>$r->price_unit
                ]);
                if($booking){
                   // notify advertiser about booking by mail & notification
                   Notifications::notifyUser([  
                      'accom_id'=>$r->accom_id,
                      'message'=>'You have a new booking request for property '.$this->propertyName($r->accom_id),  
                      'to_user'=>$advertiser_id->user_id,  
                      'request_type_url'=>'/bookings/adv-bookings'
                   ]);  
                   $advEmail=DB::table('users')->select('email')->whereId($advertiser_id->user_id)->first();
                   $sendBooking=$this->getViewBooking($booking);
                   if($this->checkAccomodationPaymentType($r->accom_id)==2){
                         if($sendBooking){
                            if($sendBooking->status==0 || $sendBooking->status==3){
                                  $sendBooking->cEmail='*********';
                                  $sendBooking->cPhone='*********';
                            }
                         }                     
                   }
                   $url=url('/').'/bookings/view/'.$booking; 
                   $this->notifyAdvertiserForBooking($advEmail->email,
                           'New booking request has been received',
                           'A new booking request has been received for accomodation '.$this->propertyName($r->accom_id).' .Please login to check',
                           $url
                   ); 
                   //notify admin for free accom boking
                   if($this->checkAccomodationPaymentType($r->accom_id)==2){
                      $this->notifyAdminForBooking(
                         'New booking request has been received for free property '.$this->propertyName($r->accom_id),
                         'New booking request has been received for free property '.$this->propertyName($r->accom_id).' .Please login to check'
                      );
                   }
                   return redirect('/bookings/my-bookings')->with('success',__('Your booking request has been sent to owner')); 
                }  
                return redirect()->back()->with('error',__('Something went wrong,please try again'));
            }else{
              return redirect()->back()->with('error',__('More rooms required for booking,please try again!'));
            }   
         }
      }
   } 
   public function propertyName($value) {
      $name=DB::table('accomodations')->select('name')->whereId($value)->first();
      return $name->name;
   }
   public function notifyAdvertiserForBooking($adv,$subject,$msg,$url) {
      if($adv) {
         try {
            Mail::to($adv)->send(new NotifyAdvertiserForBooking($subject,$msg,$url));     
         } catch(Exception $e) {
            \Log::log('notifyAdvertiserForBooking','Mail not sent to '.$adv.' Server returned error: '.$e->getMessage()); 
         }
      }
   }
   public function notifyCustomerForBooking($cust,$subject,$msg,$link) {
      if($cust) {
         try {
            Mail::to($cust)->send(new NotifyCustomerForBooking($subject,$msg,$link));     
         } catch(Exception $e) {
            \Log::log('notifyCustomerForBooking','Mail not sent to '.$cust.' Server returned error: '.$e->getMessage()); 
         }
      }
   }
   public function notifyAdminForBooking($subject,$msg) {
      $admins=DB::table('users')->select('email')->whereRoleId(1)->get();
      if($admins) {
         foreach ($admins as  $a) {
            try {
               Mail::to($a->email)->send(new NotifyAdminForBooking($subject,$msg));     
            } catch(Exception $e) {
               \Log::log('notifyAdminForBooking','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage()); 
            }  
         }
      }
   }
   public function getBookingInvoices(){
      return view('bookings.adv_invoice');
   }
   public function getBookingInvoicesAjax(){
      $invoices=DB::table('booking_commissions')
         ->select('booking_commissions.total_amount','booking_commissions.commission','booking_commissions.status','booking_commissions.id',
            'mollie_booking_payments.method','booking_payments.invoice_number','booking_payments.invoice_key'
         )
         ->leftJoin('booking_payments', 'booking_payments.booking_commission_id', '=', 'booking_commissions.id')
         ->leftJoin('mollie_booking_payments', 'booking_commissions.id', '=', 'mollie_booking_payments.booking_commission_id')
         ->where('booking_commissions.user_id',Auth::user()->id)
         ->get();
      $str='';
      return Datatables::of($invoices)
            ->addColumn('action', function ($a) {
               $str='<a href="/bookings/generate-invoice/'.$a->invoice_key.'" class="btn btn-xs btn-info" target="__blank"><i class="fa fa-download"></i></a>';     
               if($a->status==0){
                  $str.='&nbsp;<a href="/bookings/pay/'.$a->id.'" class="btn btn-xs btn-success">'.__('Pay Now').'</a>'; 
               } 
               return $str;
            })
            ->editColumn('status',function($s){
               if($s->status==1){
                  return '<span class="label label-success">'.__('PAID').'</span>';
               }else if($s->status==0){
                  return '<span class="label label-danger">'.__('NoT PAID').'</span>';
               }
            })

            ->escapeColumns([])
            ->make(true); 
   }

   public function checkAccomodationPaymentType($accom){
      if($accom){
         $a=DB::table('accomodations')->select('payment_type')->whereId($accom)->first();
         return $a->payment_type;
      }
   }
   public function sendReviewMail($cust,$subject,$msg,$link) {
      if($cust) {
         try {
            Mail::to($cust)->send(new SendReviewMail($subject,$msg,$link));     
         } catch(Exception $e) {
            \Log::log('SendReviewMail','Mail not sent to '.$cust.' Server returned error: '.$e->getMessage()); 
         }
      }
   }
   public function myBookings(){
      return view('bookings.my_bookings');
   }
   public function myBookingsAjax(){ 
      $bookings=DB::table('bookings')
            ->leftJoin('accomodations', 'bookings.accom_id', '=', 'accomodations.id')
            ->leftJoin('users', 'accomodations.user_id', '=', 'users.id')
            ->select('bookings.id','bookings.accom_id','bookings.status','bookings.book_to','bookings.book_from','accomodations.name','users.first_name as fName','users.last_name as lName','users.email')
            ->where('bookings.user_id',Auth::user()->id)->get();
      return Datatables::of($bookings) 
      ->addColumn('action', function ($a) {    
         $str='<a href="/bookings/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
         return $str;   
      })
      ->editColumn('fName',function($f){
         return $f->fName.' '.$f->lName;
      })
       ->editColumn('email',function($f){
         $paymentType=DB::table('accomodations')->select('payment_type')->whereId($f->accom_id)->first();
         if($paymentType->payment_type==2){
            if($f->status==1 || $f->status==2){
               return $f->email;   
            }else{
               return "*******";
            }
         }else{
            return $f->email;
         }
      })
      ->editColumn('book_from',function($f){
         return date('d-m-Y',strtotime($f->book_from));
      }) 
      ->editColumn('book_to',function($f){
         return date('d-m-Y',strtotime($f->book_to));
      }) 
      ->editColumn('status',function($f){
         $str='';
         if($f->status==1){
            $str='<span class="label label-success">'.__('ACCEPTED').'</span>';
         }elseif($f->status==0){
            $str='<span class="label label-warning">'.__('PENDING').'</span>';
         }elseif($f->status==3){
            $str='<span class="label label-danger">'.__('REJECTED').'</span>';
         }elseif($f->status==2){
            $str='<span class="label label-primary">'.__('COMPLETED').'</span>';
         }
         return $str;
      }) 
      ->escapeColumns([])
      ->make(true);  
   }
   public function getAdvBookings(){ 
      return view('bookings.adv_bookings');
   }
   public function getAdvBookingsAjax(){
      $bookings=DB::table('bookings')
            ->leftJoin('accomodations', 'bookings.accom_id', '=', 'accomodations.id')
            ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
            ->leftJoin('booking_commissions', 'bookings.id', '=', 'booking_commissions.booking_id')
            ->select('bookings.id','bookings.accom_id','bookings.status','bookings.book_to','bookings.book_from','accomodations.name',
               'users.first_name as fName','users.last_name as lName','users.email','booking_commissions.status as bookStatus','booking_commissions.id as bookId')
            ->where('bookings.advertiser_id',Auth::user()->id)->get();
      return Datatables::of($bookings)  
      ->addColumn('action', function ($a) { 
         $str='<a href="/bookings/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>';
       /*  if($a->status==1){
            if($a->bookStatus==0){
               $str.='&nbsp;<a href="/bookings/pay/'.$a->bookId.'" class="btn btn-xs btn-success">'.__('Pay Now').'</a>'; 
            } 
         }*/ 
         return $str;   
      })
      ->editColumn('fName',function($f){
         return $f->fName.' '.$f->lName;
      })
      ->editColumn('email',function($f){
         $paymentType=DB::table('accomodations')->select('payment_type')->whereId($f->accom_id)->first();
         if($paymentType->payment_type==2){
            if($f->status==1 || $f->status==2){
               return $f->email;   
            }else{
               return "*******";
            }
         }else{
            return $f->email;
         }
      }) 
      ->editColumn('book_from',function($f){
         return date('d-m-Y',strtotime($f->book_from));
      }) 
      ->editColumn('book_to',function($f){
         return date('d-m-Y',strtotime($f->book_to));
      }) 
      ->editColumn('status',function($f){
         $str='';
         if($f->status==1){
            $str='<span class="label label-success">'.__('ACCEPTED').'</span>';
         }elseif($f->status==0){
            $str='<span class="label label-warning">'.__('PENDING').'</span>';
         }elseif($f->status==3){
            $str='<span class="label label-danger">'.__('REJECTED').'</span>';
         }elseif($f->status==2){
            $str='<span class="label label-primary">'.__('COMPLETED').'</span>';
         }
         return $str;
      }) 
      ->escapeColumns([])
      ->make(true);  
   }
   public function getAdminBookings(){ 
      return view('bookings.admin_bookings');
   }
   public function getAdminBookingsAjax(){
     $bookings=DB::table('bookings')
                 ->leftJoin('accomodations', 'bookings.accom_id', '=', 'accomodations.id')
                 ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
                 ->select('bookings.id','bookings.accom_id','bookings.status','bookings.book_to','bookings.book_from','accomodations.name','accomodations.payment_type',
                    'users.first_name as fName','users.last_name as lName')
                 ->get();
           return Datatables::of($bookings)  
           ->addColumn('action', function ($a) { 
              $str='<a href="/bookings/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>';
              return $str;   
           })
           ->editColumn('fName',function($f){
              return $f->fName.' '.$f->lName;
           })
           ->editColumn('book_from',function($f){
              return date('d-m-Y',strtotime($f->book_from));
           }) 
           ->editColumn('book_to',function($f){
              return date('d-m-Y',strtotime($f->book_to));
           }) 
           ->editColumn('payment_type',function($p){
              if($p->payment_type==1){
                  return $str='<span class="label label-success">'.__('PAID').'</span>';
              } else{
                  return $str='<span class="label label-danger">'.__('FREE').'</span>'; 
              }
           }) 
           ->editColumn('status',function($f){
              $str='';
              if($f->status==1){
                 $str='<span class="label label-success">'.__('ACCEPTED').'</span>';
              }elseif($f->status==0){
                 $str='<span class="label label-warning">'.__('PENDING').'</span>';
              }elseif($f->status==3){
                 $str='<span class="label label-danger">'.__('REJECTED').'</span>';
              }elseif($f->status==2){
                 $str='<span class="label label-primary">'.__('COMPLETED').'</span>';
              }
              return $str;
           })  
           ->escapeColumns([])
           ->make(true); 
   }
   public function view($id){
      if($id){
         $roomsArr=[];
         $booking=$this->getViewBooking($id);
         if($booking){ 
            $paymentType=DB::table('accomodations')->select('payment_type')->whereId($booking->accom_id)->first();
            $paymentType=$paymentType->payment_type;
            if($booking->rooms_json){
                $rooms=json_decode($booking->rooms_json);
                $j=0;
                foreach ($rooms as $r) {
                  $rD=DB::table('rooms')->select('room_type')->whereId($r->room_id)->first();  
                  $roomsArr[$j]['room_type']=$rD->room_type;
                  $roomsArr[$j]['room_count']=$rooms[$j]->room_count;
                  $j++;
                }
            }
            return view('bookings.view',compact('booking','paymentType','roomsArr'));  
         }
      }
   }
   public function pay($id){
      if($id){
         $payment=DB::table('booking_payments')->where('booking_commission_id',$id)->first();
         return view('bookings.checkout',compact('payment'));
      }
   }
   public function mollieBooking(Request $r){
     if($r->booking_commission_id) {
         $commission=DB::table('booking_payments')->whereBookingCommissionId($r->booking_commission_id)->first();
         if($commission){
            $payment = Mollie::api()->payments()->create([ 
                "amount"      => $commission->amount,
                "description" => __('Booking Commission Payment'),
                "redirectUrl" => $r->getSchemeAndHttpHost()."/bookings/mollie-return/".$r->booking_commission_id,
                "webhookUrl"  => $r->getSchemeAndHttpHost()."/bookings/mollie-webhook/".$r->booking_commission_id  
                // "redirectUrl" => "https://webshop.example.org/order/12345/",
            ]);
            DB::table('mollie_booking_payments')->insertGetId([
                'order_id'=>$payment->id, 
                'booking_commission_id'=>$r->booking_commission_id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            \Log::log('mollie_booking_payments','mollie booking payment created for userId '.Auth::user()->id,json_encode($payment));
            return redirect($payment->links->paymentUrl); 
         }
      } else { 
         return redirect()->with('error',__('No booking id found,redirecting back...'));
      }
   }
   public function mollieBookingReturn(Request $r,$book){ 
      $moliePayment=DB::table('mollie_booking_payments')->where('booking_commission_id',$book)->first();
      $payment = Mollie::api()->payments()->get($moliePayment->order_id);
      if ($payment->isPaid())  {
          $update=DB::table('booking_payments')->whereBookingCommissionId($moliePayment->booking_commission_id)->update([
               'status'=>1, 
               'pay_json'=>json_encode($payment),
               'transaction_id'=>$moliePayment->order_id,
               'updated_at'=>date('Y-m-d H:i:s')
          ]);
          if($update) {
              \Log::log('mollie_booking_payments','new Mollie booking payment created for userId '.Auth::user()->id,json_encode($payment));
              DB::table('mollie_booking_payments')->where('order_id',$payment->id)->update(['status'=>1,'remarks'=>json_encode($payment),'method'=>$payment->method]);
              DB::table('booking_commissions')->whereId($moliePayment->booking_commission_id)->update(['status'=>1]);
              $commission=DB::table('booking_commissions')->whereId($moliePayment->booking_commission_id)->first();
              if($commission){

                  $accom_id=DB::table('bookings')->select('accom_id')->whereId($commission->booking_id)->first();
                   Notifications::notifyAdmin([ 
                           'accom_id'=>$accom_id->accom_id,
                           'from_user'=>Auth::user()->id,
                           'message'=>'New booking commission payment by mollie has been received',
                           'request_type_url'=>'' 
                   ]);   
                  /* $admins=DB::table('users')->select('email')->where('role_id',1)->get();
                   $this->notifyAdminsForProductPurchase($admins,
                           'New Promotion Payment Received',
                           'New promotion payment by mollie has been received for accomodation '.$this->propertyName($moliePayment->accom_id)
                   ); */
                   Notifications::notifyUser([  
                       'accom_id'=>$accom_id->accom_id,
                       'message'=>'Your booking commission payment by mollie has been received',  
                       'to_user'=>Auth::user()->id,  
                       'request_type_url'=>'/bookings/adv-bookings'
                   ]); 
                   /*$this->sendProductInvoice(
                       Auth::user()->email,
                       $proInvoiceKey,
                       $proInvoiceNumber,
                       $moliePayment->accom_id,
                       'Thanks for buying promotion your payment by mollie has been received successfully.please find enclosed invoice for your payment'
                   );  */
                   return redirect('/bookings/adv-bookings')->with('success',__('Thank you for your payment')); 
              }
          } else {
              return redirect('/bookings/adv-bookings')->with('error',__('Internal error,payment not saved'));
          }  
      } else if ($payment->isOpen()) {
          \Log::log('mollie_booking_payments_failed','error for paymentId '.$payment->id,json_encode($payment)); 
          DB::table('mollie_booking_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
          return redirect('/promotions/my-promotions')->with('error',__('Your payment has been marked open'));
      } elseif ($payment->status=='cancelled') {
          DB::table('mollie_booking_payments')->where('order_id',$payment->id)->delete();
          return redirect('/bookings/invoices')->with('error',__('Your payment request cancelled'));
      }
   }
   function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ){
      $dates = array();
      $current = strtotime( $first );
      $last = strtotime( $last );
      while( $current <= $last ) {
         $dates[] = date( $format, $current );
         $current = strtotime( $step, $current );
      }
      return $dates;
   } 
}
