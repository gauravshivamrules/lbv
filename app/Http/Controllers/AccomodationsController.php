<?php

namespace LBV\Http\Controllers;

use LBV\Mail\NotifyAdminsForEditRequest;
use LBV\Model\Accomodation; 
use LBV\Model\AccomodationAddress;
use LBV\Model\AccomodationPrice;
use LBV\Model\Room;
use LBV\Model\RoomPrice;
use LBV\Model\AccomodationPriceExtraCharge;
use LBV\Model\AccomodationSearchtag;
use LBV\Model\GalleryImage;
use LBV\Model\Country;  
use LBV\Model\Region;  
use LBV\Model\Department;   
use LBV\Model\Searchtag; 
use LBV\Model\AccomodationType; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LBV\Http\Requests\AccomodationRequest; 
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables; 
use LBV\Custom\Notifications;
use LBV\Model\Subscription;
use DB;
use Mail;


 
class AccomodationsController extends Controller 
{
    function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ){
       $dates = array();
       $current = strtotime( $first );
       $last = strtotime( $last );
       while( $current <= $last ) {
          $dates[] = date( $format, $current );
          $current = strtotime( $step, $current );
       }
       return $dates;
    }
    public function view($slug){   
      if($slug) {
        $accom=Accomodation::with('accomodation_prices')->with('accomodation_type')->with('gallery_images')
              ->with('room')->with('accomodation_address')->with('searchtag')->with('extra_price') 
              ->whereSlug($slug)->first();
        if($accom) {
            $awardIds=DB::table('accomodation_awards')->whereAccomId($accom->id)->whereApproved(1)->pluck('award_id','award_id');
            $awards=DB::table('awards')->whereIn('id',$awardIds)->get();
            if($accom->accomodation_address) {
                if($accom->accomodation_address->country_id) {
                    $cName=Country::select(['name'])->whereId($accom->accomodation_address->country_id)->first();
                    $accom->accomodation_address->country_id=$cName->name;    
                }
                if($accom->accomodation_address->region_id) {
                    $rName=Region::select(['region_name'])->whereId($accom->accomodation_address->region_id)->first();
                    $accom->accomodation_address->region_id=$rName->region_name;    
                }
                if($accom->accomodation_address->department_id) {
                    $dName=Department::select(['department_name'])->whereId($accom->accomodation_address->department_id)->first();    
                    $accom->accomodation_address->department_id=$dName->department_name;
                }
            }
            $changeDay=$priceUnit=''; 
            $minDay=1;
            if(!$accom->accomodation_prices->isEmpty()) {
                $minDay=$accom->accomodation_prices[0]->min_days;
                $priceUnit=$accom->accomodation_prices[0]->price_unit;
                if($accom->accomodation_prices[0]->price_unit !='perweek') {
                  $accom->room=Room::with('room_price')->where('accom_id',$accom->id)->get();
                  if($accom->accomodation_prices[0]->price_unit=='perroom'){
                    $minPrice=DB::table('room_prices')->select('price')->whereAccomId($accom->id)->min('price');  
                  }else{
                    $minPrice=DB::table('room_prices')->select('price1')->whereAccomId($accom->id)->min('price1');  
                  }
                }else{
                  $month=date('m');
                  $year=date('Y');
                  $minPrice=DB::table('accomodation_prices')->select('amount')
                        ->whereAccomId($accom->id)
                        ->whereMonth('start_date', '=', $month)
                        ->whereYear('start_date', '=', $year)
                        ->min('amount');
                  if(!$minPrice){
                     $minPrice=DB::table('accomodation_prices')->select('amount')
                           ->whereAccomId($accom->id)
                           ->whereMonth('end_date', '=', $month)
                           ->whereYear('end_date', '=', $year)
                           ->min('amount');
                     if(!$minPrice){
                         $minPrice=DB::table('accomodation_prices')->select('amount')
                              ->whereAccomId($accom->id)
                              ->whereMonth('end_date', '>=', $month)
                              ->whereYear('end_date', '>=', $year)
                              ->min('amount'); 
                     } 
                  }
                  if(!$minPrice){
                    $minPrice=DB::table('accomodation_prices')->select('amount')->whereAccomId($accom->id)->min('amount'); 
                  }
                }
                $cDay=DB::table('accomodation_prices')->select('changeday')->whereAccomId($accom->id)->first();
                $changeDay=$cDay->changeday;
            } 
            $ar=[];
            $searchTags=[];
            if(!$accom->searchtag->isEmpty()) {
                foreach($accom->searchtag as $st) {
                    $ar[]=$st->searchtag_id;
                }
                $searchTags=Searchtag::select(['tag_name'])->whereIn('id',$ar)->get();  
            }
            if($accom->user) { 
                if($accom->user->country_id) {
                   $cName=Country::select(['name'])->whereId($accom->user->country_id)->first(); 
                   $accom->user->country_id=$cName->name; 
                }
            }
            $featuredImg='';
            if($accom->featured_image){
                $featuredImg='/images/gallery/'.$accom->id.'/'.'slider_'.$accom->featured_image;
            }else{
              if($accom->gallery_images){
                $featuredImg='/images/gallery/'.$accom->id.'/'.'slider_'.$accom->gallery_images[0]->slider_image;
              }
            }
            $refrer='/';
            if(isset($_SERVER['HTTP_REFERER'])){ 
              $refrer=$_SERVER['HTTP_REFERER'];  
            }

            $bookedDates=DB::table('booked_dates')->whereAccomId($accom->id)->get(); 
            $bookedArrTmp=$bookedArrFinal=[];
            if($priceUnit=='perweek'){
                if($bookedDates){
                    foreach($bookedDates as $b) {
                      $date=date('Y-m-d',strtotime($b->start_date));
                      if(!in_array($date,$bookedArrFinal)){
                          $range=$this->dateRange($b->start_date,$b->end_date);
                          foreach ($range as $r) {
                              $bookedArrFinal[]=$r; 
                          }
                      }
                    }
                }
            }else{
              if($bookedDates){
                 $returnArrB=$bRange=[];
                 $k=$z=$ab=0;
                 $allBookedRooms=[];
                 $allBookedDates=[];
                 foreach ($bookedDates as $a){
                    $checkIfAllBooked=DB::table('rooms')->select('no_of_rooms')->whereId($a->room_id)->first();
                    if($a->no_of_rooms>=$checkIfAllBooked->no_of_rooms){
                        $allBookedDates[$ab]['start']=$a->start_date;
                        $allBookedDates[$ab]['end']=$a->end_date;
                        $allBookedRooms[]=$a->room_id;
                        $ab++;
                    }
                 }
                 $checkIfLeft=DB::table('rooms')->whereAccomId($accom->id)->whereNOTIn('id',$allBookedRooms)->get();
                 if(!$checkIfLeft){
                    foreach ($allBookedDates as $b){
                      $date=date('Y-m-d',strtotime($b->start_date));
                      if(!in_array($date,$bookedArrFinal)){
                          $range=$this->dateRange($b->start_date,$b->end_date);
                          foreach ($range as $r) {
                              $bookedArrFinal[]=$r; 
                          }
                      }
                    }
                 }
              }
            }
            $ratings=DB::table('accomodation_ratings')->where('accomodation_ratings.accom_id',$accom->id)->get();
            if($ratings){
              $rateArr=$finalRateArr=[];
              foreach($ratings as $r) {
                $rateArr[]=$r->rate_id;
              }
              $allRates=DB::table('rates')->whereIn('id',$rateArr)->orderBy('rates.id','desc')->get();
              $i=0;
              foreach ($allRates as $v) {
                if($v->rate_description){
                  if($v->user_id){
                    $user=DB::table('users')->select('first_name','last_name')->whereId($v->user_id)->first();
                    if($user){
                      $finalRateArr[$i]['user']=$user->first_name.' '.$user->last_name;
                    }else{
                      $finalRateArr[$i]['user']='';
                    }
                  }else{
                    $finalRateArr[$i]['user']=$v->rate_default_name;  
                  }
                  $finalRateArr[$i]['created']=date('d-m-Y',strtotime($v->created_at));  
                  $finalRateArr[$i]['description']=$v->rate_description;
                  $finalRateArr[$i]['ratings']=DB::table('rate_ratecategories')->whereRateId($v->id)->get();
                  $i++;
                }
              }
            }
            $similarAccoms=Accomodation::with('accomodation_prices')->with('gallery_images')->with('accomodation_address')
                  ->where('id','!=',$accom->id)
                  ->whereCompleted(1)
                  ->whereSubsValidLicense(1) 
                 ->whereAccomodationTypeId($accom->accomodation_type_id)
                 ->limit(2)
                 ->get();
              $similarAccomArr=[];
              $j=0; 
              $similarFeature='';
              $sMinprice=null;
              foreach ($similarAccoms as $sA) {
                  $similarAccomArr[$j]['id']=$sA->id;
                  $similarAccomArr[$j]['name']=$sA->name;
                  $similarAccomArr[$j]['slug']=$sA->slug;
                  if($sA->featured_image){
                      $similarFeature='/images/gallery/'.$sA->id.'/'.'slider_'.$sA->featured_image;
                  }else{
                    if($accom->gallery_images){
                      $similarFeature='/images/gallery/'.$sA->id.'/'.'slider_'.$sA->gallery_images[0]->slider_image;
                    }
                  }
                  $similarAccomArr[$j]['image']=$similarFeature;
                  if(!$sA->accomodation_prices->isEmpty()) {
                      if($sA->accomodation_prices[0]->price_unit !='perweek') {
                        if($sA->accomodation_prices[0]->price_unit=='perroom'){
                          $sMinprice=DB::table('room_prices')->select('price')->whereAccomId($sA->id)->min('price');  
                        }else{
                          $sMinprice=DB::table('room_prices')->select('price1')->whereAccomId($sA->id)->min('price1');  
                        }
                      }else{
                        $sMinprice=DB::table('accomodation_prices')->select('amount')->whereAccomId($sA->id)->min('amount');  
                      }
                  }

                  $similarAccomArr[$j]['price']=$sMinprice;
                  $j++;
              }
              // dd($minPrice);
            $rooms=Room::with('room_price')->select('id','room_type','adults','childs','max_child_age','no_of_rooms')->whereAccomId($accom->id)->get();
            return view('accomodations.view',compact('accom','bookedArrFinal','minDay','searchTags','priceUnit','awards','similarAccomArr','minPrice','changeDay','featuredImg','refrer','bookedDates','rooms','finalRateArr')); 
        } else {
          return redirect()->back()->with('error',__('Sorry,accomodation not found'));
        }
      }
    }
    public function getGalImgs(Request $r){  
      $galleryImgs=DB::table('gallery_images')->whereAccomId($r->id)->get();
      $jsonGalleryImgs=[];
      if($galleryImgs){
        $g=0;
        $imgPath='/images/gallery/'.$r->id.'/';
        foreach ($galleryImgs as $value) {
          $jsonGalleryImgs[$g]['src']=$imgPath.$value->slider_image;
          $jsonGalleryImgs[$g]['thumb']=$imgPath.$value->thumb_image;
          $g++;
        }
      }
      echo json_encode($jsonGalleryImgs);
    }
    public function getAccomodations(){
        return view('accomodations.index'); 
    }
    public function getPaidView(){
        return view('accomodations.paid_accom');
    }
    public function getNoCureNoPayView(){ 
        return view('accomodations.no_cure_no_pay');
    }
    public function getUnApprovedView(){ 
        return view('accomodations.un_approved_accom');
    }
    public function getRequestedDeleteView(){ 
        return view('accomodations.requested_delete_accom');
    }
    public function getSliderAccomodation(Request $r){
      $accoms=Accomodation::whereApproved(1)->whereSubsValidLicense(1)->orderBy('name','asc')->pluck('name','id');
      $sliderAccoms=Accomodation::select('id')->whereIsSlider(1)->get();
      $ids=[];
      foreach ($sliderAccoms as $s) {
          $ids[]=$s->id;
      }
      return view('accomodations.slider_accom',compact('accoms','ids'));
    } 
    public function postSliderAccomodation(Request $r){
        $ERROR='';
        if($r->accom_id) {
          $maxSliderAccoms=Accomodation::whereIsSlider(1)->count();
          if($maxSliderAccoms<=10) {
             DB::table('accomodations')->update(['is_slider'=>0]);
             foreach($r->accom_id as $a) {
                  $accom=Accomodation::whereId($a)->first();
                  $accom->is_slider=1;
                  if(!$accom->save()) {
                      $ERROR='ERROR';
                  }
             }
             if($ERROR=='') {
                return redirect()->back()->with('success',__('Slider updated successfully'));    
             } else {
              return redirect()->back()->with('error',__('Something went wrong,please try again!'));    
             }
          } else {
            return redirect()->back()->with('error',__('Sorry,you can select max upto 10 accomodations'));  
          }
        } else {
          return redirect()->back()->with('error',__('Please select accomodations'));
        }
    }
    // To get accomodations for datatables
    // request type 1=all,2=paid,3=nocurenopay,4=unapproved
    public function getAccomodationsAjax(Request $r) {
        if($r->query('request_type')) { 
            $request_type=$r->query('request_type'); 
            switch ($r->query('request_type')) {
                case 1:
                    // $accoms = Accomodation::with('user')->with('accomodation_type')->orderBy('id','desc')->limit(50)->get(); 
                $accoms = DB::table('accomodations')->select('accomodations.id','accomodations.name','accomodations.label_id','accomodations.payment_type',
                      'accomodations.approved','accomodations.completed','accomodations.created_at','accomodations.slug',
                      'users.first_name','users.last_name','users.email','users.user_company','users.country_id',
                      'users.phone_number','users.city','users.street','users.street_number','users.post_code','users.btw_number',
                      'accomodation_types.type_name'
                  )
                ->leftJoin('users','users.id','=','accomodations.user_id')
                ->leftJoin('accomodation_types','accomodation_types.id','=','accomodations.accomodation_type_id')
                ->orderBy('accomodations.id','desc')
                ->get(); 
                    // dd($accoms);
                    break;
                case 2:
                    // $accoms = Accomodation::with('user')->with('accomodation_type')->where('payment_type',1)->get();   
                $accoms = DB::table('accomodations')->select('accomodations.id','accomodations.name','accomodations.label_id','accomodations.payment_type',
                      'accomodations.approved','accomodations.completed','accomodations.created_at','accomodations.slug',
                      'users.first_name','users.last_name','users.email','users.user_company','users.country_id',
                      'users.phone_number','users.city','users.street','users.street_number','users.post_code','users.btw_number',
                      'accomodation_types.type_name'
                  )
                ->leftJoin('users','users.id','=','accomodations.user_id')
                ->leftJoin('accomodation_types','accomodation_types.id','=','accomodations.accomodation_type_id')
                ->where('payment_type',1)
                ->orderBy('accomodations.id','desc')
                ->get();
                    break;
                case 3:
                    // $accoms = Accomodation::with('user')->with('accomodation_type')->where('payment_type',2)->get();   
                $accoms = DB::table('accomodations')->select('accomodations.id','accomodations.name','accomodations.label_id','accomodations.payment_type',
                      'accomodations.approved','accomodations.completed','accomodations.created_at','accomodations.slug',
                      'users.first_name','users.last_name','users.email','users.user_company','users.country_id',
                      'users.phone_number','users.city','users.street','users.street_number','users.post_code','users.btw_number',
                      'accomodation_types.type_name'
                  )
                ->leftJoin('users','users.id','=','accomodations.user_id')
                ->leftJoin('accomodation_types','accomodation_types.id','=','accomodations.accomodation_type_id')
                ->where('payment_type',2)
                ->orderBy('accomodations.id','desc')
                ->get();
                    break;
                case 4:
                    // $accoms = Accomodation::with('user')->with('accomodation_type')->where('approved',0)->get();   
                $accoms = DB::table('accomodations')->select('accomodations.id','accomodations.name','accomodations.label_id','accomodations.payment_type',
                      'accomodations.approved','accomodations.completed','accomodations.created_at','accomodations.slug',
                      'users.first_name','users.last_name','users.email','users.user_company','users.country_id',
                      'users.phone_number','users.city','users.street','users.street_number','users.post_code','users.btw_number',
                      'accomodation_types.type_name'
                  )
                ->leftJoin('users','users.id','=','accomodations.user_id')
                ->leftJoin('accomodation_types','accomodation_types.id','=','accomodations.accomodation_type_id')
                ->where('approved',0)
                ->orderBy('accomodations.id','desc')
                ->get();
                    break;
                case 5:
                    // $accoms = Accomodation::with('user')->with('accomodation_type')->where('deletion_request',1)->get();   
                $accoms = DB::table('accomodations')->select('accomodations.id','accomodations.name','accomodations.label_id','accomodations.payment_type',
                      'accomodations.approved','accomodations.completed','accomodations.created_at','accomodations.slug',
                      'users.first_name','users.last_name','users.email','users.user_company','users.country_id',
                      'users.phone_number','users.city','users.street','users.street_number','users.post_code','users.btw_number',
                      'accomodation_types.type_name'
                  )
                ->leftJoin('users','users.id','=','accomodations.user_id')
                ->leftJoin('accomodation_types','accomodation_types.id','=','accomodations.accomodation_type_id')
                ->where('deletion_request',1)
                ->orderBy('accomodations.id','desc')
                ->get();
                    break;
            }
            
            if($accoms) {
                $str='';
                return Datatables::of($accoms) 
                ->addColumn('action', function ($accom) {   
                    $str='<a href="/accomodations/view-accom/'.$accom->slug.'" class="btn btn-xs btn-primary" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
                    $str .='&nbsp;<a href="/accomodations/edit/'.$accom->id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>'; 
                    $str .='&nbsp;<a id="deleteAccom" onclick="confirmDel('.$accom->id.');" href="javascript:;" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>'; 
                    return $str;    
                }) 
                ->editColumn('accomodation_type_id',function($accom) {
                    $type=$accom->type_name;
                    return "<span class='label label-warning'>".$type." </span>"; 
                }) 
                ->editColumn('user_id', function ($accom) { 
                 
                    $c=Country::select(['name'])->where('id',$accom->country_id)->first();
                    $user=''; 
                    $address2='';
                    $name=$accom->first_name.' '.$accom->last_name;
                    $address=$accom->city.' '.$accom->street_number.' '.$accom->street;
                    if($c){
                      $address2=$accom->city.' '.$accom->post_code.' '.$c->name;
                    }
                    
                    $user="<span>".$name."</span><br>";
                    $user .="<span>".$address."</span><br>";
                    $user .="<span>".$address2."</span><br>";
                    $user .="<span>".$accom->email."</span><br>";
                    $user .="<span>".$accom->phone_number."</span><br>";
                    return $user; 
                })   
                ->editColumn('approved',function($accom) {
                    if($accom->approved) {
                        return "<span class='label label-success'>".__('APPROVED')."</span>";
                    } else {
                        return "<span class='label label-danger'>".__('UN-APPROVED')."</span>"; 
                    } 
                }) 
                ->editColumn('payment_type',function($accom) {     
                    if($accom->payment_type==1) {
                        return "<span class='label label-success'>".__('PAID')."</span>";
                    } else   {
                        return "<span class='label label-info'>".__('FREE')."</span>"; 
                    } 
                })
                ->editColumn('label_id',function($accom) {     
                    if($accom->label_id==2) {
                        return "<span class='label label-default'>".__('SALE')."</span>";
                    } else if($accom->label_id==3)   {
                        return "<span class='label label-success'>".__('RENT')."</span>"; 
                    } 
                })
                ->editColumn('completed',function($accom) {     
                    if($accom->completed) {
                        return "<span class='label label-success'>".__('YES')."</span>"; 
                    } else   {
                        return "<span class='label label-danger'>".__('NO')."</span>";  
                    } 
                }) 
                ->editColumn('created_at',function($date) {
                    return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
                })
                ->escapeColumns([])
                ->make(true); 
            }
        }    
    }
    public function getAccomodationsDelete(Request $r) {
       $accoms = Accomodation::with('user')->with('accomodation_type')->where('deletion_request',1)->get(); 
       if($accoms) {
           $str='';
           return Datatables::of($accoms) 
           ->addColumn('action', function ($accom) {   
               $str='<a href="/accomodations/view-accom/'.$accom->slug.'" class="btn btn-xs btn-primary" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
               $str .='&nbsp;<a href="/accomodations/edit/'.$accom->id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>'; 
               $str .='&nbsp;<a id="deleteAccom" onclick="confirmDel('.$accom->id.');" href="javascript:;" title="Delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>'; 
               return $str;    
           }) 
           ->editColumn('accomodation_type_id',function($accom) {
               $type=$accom->accomodation_type->type_name;
               return "<span class='label label-warning'>".$type." </span>"; 
           }) 
           ->editColumn('user_id', function ($accom) {   
               $c=Country::select(['name'])->where('id',$accom->user->country_id)->first();
               $user=''; 
               $name=$accom->user->first_name.' '.$accom->user->last_name;
               $address=$accom->user->city.' '.$accom->user->street_number.' '.$accom->user->street;
               $address2=$accom->user->city.' '.$accom->user->post_code.' '.$c->name;
               $user="<span>".$name."</span><br>";
               $user .="<span>".$address."</span><br>";
               $user .="<span>".$address2."</span><br>";
               $user .="<span>".$accom->user->email."</span><br>";
               $user .="<span>".$accom->user->phone_number."</span><br>";
               return $user; 
           })   
           ->editColumn('approved',function($accom) {
               if($accom->approved) {
                   return "<span class='label label-success'>".__('APPROVED')."</span>";
               } else {
                   return "<span class='label label-danger'>".__('UN-APPROVED')."</span>"; 
               } 
           }) 
           ->editColumn('payment_type',function($accom) {     
               if($accom->payment_type==1) {
                   return "<span class='label label-success'>".__('PAID')."</span>";
               } else   {
                   return "<span class='label label-info'>".__('FREE')."</span>"; 
               } 
           })
           ->editColumn('label_id',function($accom) {     
               if($accom->label_id==2) {
                   return "<span class='label label-default'>".__('SALE')."</span>";
               } else if($accom->label_id==3)   {
                   return "<span class='label label-success'>".__('RENT')."</span>"; 
               } 
           })
           ->editColumn('completed',function($accom) {     
               if($accom->completed) {
                   return "<span class='label label-success'>".__('YES')."</span>"; 
               } else   {
                   return "<span class='label label-danger'>".__('NO')."</span>";  
               } 
           }) 
           ->editColumn('created_at',function($date) {
               return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
           })
           ->escapeColumns([])
           ->make(true); 
       }  
    }
    public function deleteAccom($accom=null) {
        // dd(accom);
        if($accom) {
            $accomodation=Accomodation::whereId($accom)->firstOrFail();  
            $user_id=$accomodation->user_id;
            $priceType='';
            if($accomodation) {
                if($accomodation->delete()) { 
                    $address=AccomodationAddress::where('accom_id',$accom)->first();  
                    if($address) {
                        $address->delete();
                    }
                    $prices=AccomodationPrice::where('accom_id',$accom)->get();
                    if($prices) {
                        if(isset($prices[0])) {
                            $priceType=$prices[0]->price_unit;
                        }
                        foreach($prices as $p) {
                            $p->delete();     
                        }
                        if($priceType!='perweek') {
                            $rooms=Room::where('accom_id',$accom)->get();
                            if($rooms) {
                                foreach($rooms as $r) {
                                    $r->delete();
                                }    
                            }
                            $roomPrices=RoomPrice::where('accom_id',$accom)->get();
                            if($roomPrices) {
                                foreach($roomPrices as $r) {
                                    $r->delete();
                                }        
                            }
                        }  
                    }
                    $extraCharge=AccomodationPriceExtraCharge::where('accom_id',$accom)->get();
                    if($extraCharge) {
                        foreach($extraCharge as $e) {
                            $e->delete();
                        }
                    }
                    $searchTag=AccomodationSearchtag::where('accom_id',$accom)->get();
                    if($searchTag) {
                        foreach($searchTag as $s) {
                            $s->delete();
                        }
                    }
                    $images=GalleryImage::where('accom_id',$accom)->get();
                    if($images) {
                        foreach ($images as $i) {
                            $i->delete();
                        }
                        if (file_exists(public_path('images/gallery/'.$accom))) {
                            $this->removeDirectory(public_path('images/gallery/'.$accom));      
                        }
                        
                    }
                    Notifications::notifyUser([  
                        'accom_id'=>$accom,
                        'message'=>'Your accomodation has been deleted',  
                        'to_user'=>$user_id,  
                        'request_type_url'=>'/accomodations/my-accomodations'
                    ]); 
                    return redirect('/accomodations')->with('success',__('Accomodation deleted successfully'));
                }
                return redirect()->back()->with('error',__('Sorry,accomodation could not be deleted,please try again!'));   
            } else {
                return redirect()->back()->with('error',__('Sorry,invalid accomodation'));
            } 
        }
    } 
    function removeDirectory($path) { 
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }
    public function viewAccom($slug) {
        if($slug) { 
            $accom=Accomodation::with('accomodation_type')->with('gallery_images')->with('accomodation_prices')->with('extra_price')->with('accomodation_address')
                ->with('room')->with('searchtag')->with('user')->whereSlug($slug)->first(); 
                if($accom) {
                    if($accom->accomodation_address) {
                        if($accom->accomodation_address->country_id) {
                            $cName=Country::select(['name'])->whereId($accom->accomodation_address->country_id)->first();
                            $accom->accomodation_address->country_id=$cName->name;    
                        }
                        if($accom->accomodation_address->region_id) {
                            $rName=Region::select(['region_name'])->whereId($accom->accomodation_address->region_id)->first();
                            $accom->accomodation_address->region_id=$rName->region_name;    
                        }
                        if($accom->accomodation_address->department_id) {
                            $dName=Department::select(['department_name'])->whereId($accom->accomodation_address->department_id)->first();    
                            $accom->accomodation_address->department_id=$dName->department_name;
                        }
                    }
                    if(!$accom->accomodation_prices->isEmpty()) {
                        if($accom->accomodation_prices[0]->price_unit !='perweek') {
                           $accom->room=Room::with('room_price')->where('accom_id',$accom->id)->get();
                        }
                    }
                    $ar=[];
                    $searchTags=[];
                    if(!$accom->searchtag->isEmpty()) {
                        foreach($accom->searchtag as $st) {
                            $ar[]=$st->searchtag_id;
                        }
                        $searchTags=Searchtag::select(['tag_name'])->whereIn('id',$ar)->get();  
                    }
                    if($accom->user) { 
                        if($accom->user->country_id) {
                           $cName=Country::select(['name'])->whereId($accom->user->country_id)->first(); 
                           $accom->user->country_id=$cName->name; 
                        }
                    }
                } 
            return view('accomodations.view_accom',compact('accom','searchTags'));
        } 
    }
    public function viewAllFreeUpdateRequest(){
       return view('accomodations.update_req');
    }
    public function getAllFreeUpdateRequestAjax() {
        // $accoms = Accomodation::where('user_id', Auth::user()->id)->get();  
        $accoms=DB::table('edit_accomodations')->select('id','accom_id','name','created_at','status')->whereStatus(0)->get(); 
        $str='';
        return Datatables::of($accoms)
        ->addColumn('action', function ($accom) {    
            return '&nbsp;<a href="/accomodations/view-accom-free/'.$accom->accom_id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>'; 
        })   
      /* ->editColumn('accomodation_type_id',function($accom) {
           $type=$accom->accomodation_type->type_name;
           return "<span class='label label-warning'>".$type." </span>"; 
       }) */
      /* ->editColumn('user_id', function ($accom) {   
           $c=Country::select(['name'])->where('id',$accom->user->country_id)->first();
           $user=''; 
           $name=$accom->user->first_name.' '.$accom->user->last_name;
           $address=$accom->user->city.' '.$accom->user->street_number.' '.$accom->user->street;
           $address2=$accom->user->city.' '.$accom->user->post_code.' '.$c->name;
           $user="<span>".$name."</span><br>";
           $user .="<span>".$address."</span><br>";
           $user .="<span>".$address2."</span><br>";
           $user .="<span>".$accom->user->email."</span><br>";
           $user .="<span>".$accom->user->phone_number."</span><br>";
           return $user; 
       })  */ 
      /* ->editColumn('approved',function($accom) {
           if($accom->approved==1) { 
               return "<span class='label label-success'>".__('APPROVED')."</span>";
           } else {
               return "<span class='label label-danger'>".__('UN-APPROVED')."</span>"; 
           } 
       }) */
       ->editColumn('status',function($accom) {     
          return "<span class='label label-danger'>".__('UN-APPROVED')."</span>"; 
       })
       ->editColumn('created_at',function($date) {
           return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
       })
       ->escapeColumns([])
       ->make(true);     
    }  
    public function viewAccomFree($id) {
        if($id) { 
            $accom=DB::table('edit_accomodations')->whereAccomId($id)->whereStatus(0)->first();
            $oldInfo=DB::table('accomodations')->whereId($id)->first();
            $accomTypes=AccomodationType::pluck('type_name','id');  
            if($accom) { 
              return view('accomodations.view_accom_free',compact('accom','oldInfo','accomTypes')); 
            }
            return redirect()->back()->with('error',__('No data found')); 
        }  
    }
    public function approveAccom($id=null) { 
        if($id) {
            $accom=Accomodation::whereId($id)->first();
            $accom->approved=1;
            $accom->subs_valid_license=1;
            if($accom->save()) {
                Notifications::notifyUser([  
                    'accom_id'=>$id,
                    'message'=>'Your accomodation has been approved',  
                    'to_user'=>$accom->user_id,  
                    'request_type_url'=>'/accomodations/my-accomodations'
                ]); 
                return redirect()->back()->with('success',__('Accomodation approved successfully'));
            }

            return redirect()->back()->with('error',__('Sorry,accomodation could not be approved,please try again!'));
        } 
    }

    // ADVERTISER PART 

    public function getEditAccom(Request $r,$id) {
        if($id) {
            $accom=Accomodation::whereId($id)->first();
            if(Auth::check()) {
                if(Auth::user()->role_id !=1) {
                    if(Auth::user()->id != $accom->user_id) {
                        return redirect('/accomodations/my-accomodations')->with('error',__('Sorry invalid request')); 
                    }  
                } 
            }     
        }
        $accomTypes=AccomodationType::pluck('type_name','id');  
        return view('accomodations.edit',compact('accom','accomTypes'));
    }
    public function getEditFreeAccom(Request $r,$id) {
        if($id) {
            $oldReq=DB::table('edit_accomodations')->whereAccomId($id)->whereStatus(0)->orderBy('id','desc')->first();
            if($oldReq) {
              return redirect('/accomodations/edit-accom-price/'.$id.'?type='.$oldReq->accomodation_type_id)->with('error',__('Request already sent for description update,waiting admin approval.You can update prices etc.'));
            }
            $accom=Accomodation::whereId($id)->first();
            if(Auth::check()) {
                if(Auth::user()->role_id !=1) {
                    if(Auth::user()->id != $accom->user_id) {
                        return redirect('/accomodations/my-accomodations')->with('error',__('Sorry invalid request')); 
                    }  
                } 
            }     
        }

        $accomTypes=AccomodationType::pluck('type_name','id');  
        return view('accomodations.edit_free',compact('accom','accomTypes')); 
    }
    
    public function postEditFreeAccom(Request $r,AccomodationRequest $ar,$id) {
        $accom=DB::table('edit_accomodations')->insertGetId([
            'name'=>$r->get('name'),
            'accom_id'=>$id,
            'accomodation_type_id'=>$r->get('type_id'),
            'email'=>$r->get('email'),
            'website'=>$r->get('website'),
            'number_of_persons'=>$r->get('number_of_persons'),
            'bathrooms'=>$r->get('bathrooms'),
            'bedrooms'=>$r->get('bedrooms'),
            'single_beds'=>$r->get('single_beds'),
            'double_beds'=>$r->get('double_beds'), 
            'description'=>$r->get('description'),
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s') 
        ]);
        if($accom) {
          $pName=app('LBV\Http\Controllers\AwardsController')->propertyName($id);
          Notifications::notifyAdmin([ 
                  'accom_id'=>$id,
                  'from_user'=>Auth::user()->id,
                  'message'=>'A new accomodation description update request has been received for '.$pName,
                  'request_type_url'=>'/accomodations/view-accom-free/'.$id
          ]); 
          $this->notifyAdminsForEditRequest('New accomodation update request has been received.',
                   'A new accomodation description update request has been received for '.$pName
          ); 
          return redirect('/accomodations/edit-accom-price/'.$id.'?type='.$r->get('type_id'))->with('success',__('Request submitted successfully,wating admin approval'));  
        }
        return redirect()->back()->with('error',__('Sorry,Information could not be updated,please try again!')); 
    }
    public function postEditAccom(Request $r,AccomodationRequest $ar,$id) {
        $accom=Accomodation::whereId($id)->first();
        $accom->name=$r->get('name');
        $accom->accomodation_type_id=$r->get('type_id'); 
        $accom->email=$r->get('email');
        $accom->website=$r->get('website');
        $accom->number_of_persons=$r->get('number_of_persons');
        $accom->description=$r->get('description');
        $accom->youtube_url=$r->get('youtube_url');
        $accom->bathrooms=$r->get('bathrooms');
        $accom->bedrooms=$r->get('bedrooms');
        $accom->single_beds=$r->get('single_beds');
        $accom->double_beds=$r->get('double_beds');
        if($accom->save()) { 
            $accom->slug=$this->makeSlug($accom->id,$accom->name);
            $accom->save();
            return redirect('/accomodations/edit-accom-price/'.$accom->id.'?type='.$accom->accomodation_type_id)->with('success',__('Information updated successfully'));  
        }
        return redirect()->back()->with('error',__('Sorry,Information could not be updated,please try again!')); 
    }
    public function notifyAdminsForEditRequest($subject,$msg) {
      $admins=DB::table('users')->select('email')->where('role_id',1)->get();
      if($admins) {
        foreach ($admins as $a) {
          try {
            Mail::to($a->email)->send(new NotifyAdminsForEditRequest($subject,$msg));     
          } catch(Exception $e) {
            \Log::log('NotifyAdminsForEditRequest','Mail not sent to '.Auth::user()->id.' Server returned error: '.$e->getMessage()); 
          }
        }
      }
    }
    public function approveEditAccom(Request $r,$id=null) { 
        if($id) {
            $accom=DB::table('edit_accomodations')->whereAccomId($id)->orderBy('id','desc')->first();
            $user=Accomodation::select('user_id')->whereId($id)->first();
            $update=DB::table('accomodations')->whereId($id)->update([
                'name'=>$r->name,
                'accomodation_type_id'=>$r->type_id,
                'website'=>$r->website,
                'email'=>$r->email,
                'description'=>$r->description,
                'number_of_persons'=>$r->number_of_persons,
                'slug'=>$this->makeSlug($id,$r->name),
                'bathrooms'=>$r->get('bathrooms'),
                'bedrooms'=>$r->get('bedrooms'),
                'single_beds'=>$r->get('single_beds'),
                'double_beds'=>$r->get('double_beds'),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]); 
            if($update) {
              DB::table('edit_accomodations')->whereId($accom->id)->update(['status'=>1]);
              Notifications::notifyUser([  
                  'accom_id'=>$id,
                  'message'=>'Your accomodation update request has been approved',  
                  'to_user'=>$user->user_id,  
                  'request_type_url'=>'/accomodations/my-accomodations'
              ]); 
              return redirect('/accomodations/view-all-update')->with('success',__('Request approved successfully'));  
            } 
            return redirect()->back()->with('error',__('Sorry,accomodation could not be approved,please try again!'));
        } 
    }

    public function rejectEditAccom($id){
        if($id) {
         $reject=DB::table('edit_accomodations')->whereAccomId($id)->update(['status'=>2]);
         if( $reject) {
            $user=Accomodation::select('user_id')->whereId($id)->first();
            Notifications::notifyUser([  
                'accom_id'=>$id,
                'message'=>'Your accomodation update request has been rejected',  
                'to_user'=>$user->user_id,  
                'request_type_url'=>'/accomodations/my-accomodations'
            ]); 
            return redirect('/accomodations')->with('success',__('Request rejected successfully'));
         } 
        }
    }

    public function pay($accom=null) {
        if($accom) {
            $accomodation=Accomodation::select(['id','label_id'])->whereId($accom)->first();
            if($accomodation) {
              $subscriptions = Subscription::select(DB::raw("CONCAT(name,'-EUR ',amount) AS name"),'id')->where('type_id',$accomodation->label_id)->pluck('name', 'id');
              return view('accomodations.pay',compact('subscriptions','accomodation'));  
            } else {
              return redirect()->back()->with('error',__('Sorry no accomodation found'));
            }
        }
    }
    public function getMyAccomodations(){
        return view('accomodations.my_accomodatons');
    }
    public function deleteRequest($accom) {
        if($accom) {
            $accomodation=Accomodation::where('id',$accom)->first();
            if($accomodation) {
                $accomodation->deletion_request=1;
                if($accomodation->save()) {
                    Notifications::notifyAdmin([ 
                            'accom_id'=>$accom,
                            'from_user'=>Auth::user()->id,
                            'message'=>'A new accomodation deletion request has been received',
                            'request_type_url'=>'/accomodations/view-accom/'.$accomodation->slug
                    ]); 
                    return redirect()->back()->with('success',__('Delete request has been submitted successfully,wating for admin approval'));
                }
                return redirect()->back()->with('error',__('Sorry,delete request could not be submitted,please try again!'));
            }
        } 
    }
    public function getMyAccomodationsAjax() {
        $accoms = Accomodation::where('user_id', Auth::user()->id)->get();   
        $str='';
        return Datatables::of($accoms)
        ->addColumn('action', function ($accom) {    
            $str='<a href="/accomodations/view-accom/'.$accom->slug.'" class="btn btn-xs btn-primary" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
            if($accom->payment_type==2){
                $str .='&nbsp;<a href="/accomodations/edit-free/'.$accom->id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>'; 
            } else {
              $str .='&nbsp;<a href="/accomodations/edit/'.$accom->id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>'; 
            } 
            if(!$accom->deletion_request) {
                $str .='&nbsp;<a id="deleteAccom" onclick="confirmDelete('.$accom->id.');"  href="javascript:;" title="Delete" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
            } else {
                $str .='&nbsp;<a id="deleteAccom"  href="javascript:;" class="btn btn-xs btn-default"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
            }
            return $str;   
        })   
       ->editColumn('accomodation_type_id',function($accom) {
           $type=$accom->accomodation_type->type_name;
           return "<span class='label label-warning'>".$type." </span>"; 
       }) 
       ->editColumn('user_id', function ($accom) {   
           $c=Country::select(['name'])->where('id',$accom->user->country_id)->first();
           $user=''; 
           $name=$accom->user->first_name.' '.$accom->user->last_name;
           $address=$accom->user->city.' '.$accom->user->street_number.' '.$accom->user->street;
           $address2=$accom->user->city.' '.$accom->user->post_code.' '.$c->name;
           $user="<span>".$name."</span><br>";
           $user .="<span>".$address."</span><br>";
           $user .="<span>".$address2."</span><br>";
           $user .="<span>".$accom->user->email."</span><br>";
           $user .="<span>".$accom->user->phone_number."</span><br>";
           return $user; 
       })   
       ->editColumn('approved',function($accom) {
           if($accom->approved==1) {
               return "<span class='label label-success'>".__('APPROVED')."</span>";
           } else {
               return "<span class='label label-danger'>".__('UN-APPROVED')."</span>"; 
           } 
       }) 
       ->editColumn('payment_type',function($accom) {     
           if($accom->payment_type==1) {
               return "<span class='label label-success'>".__('PAID')."</span>";
           } else   {
               return "<span class='label label-info'>".__('FREE')."</span>"; 
           } 
       })
       ->editColumn('label_id',function($accom) {     
           if($accom->label_id==2) {
               return "<span class='label label-default'>".__('SALE')."</span>";
           } else if($accom->label_id==3)   {
               return "<span class='label label-success'>".__('RENT')."</span>"; 
           } 
       })
       ->editColumn('completed',function($accom) {     
           if($accom->completed) {
               return "<span class='label label-success'>".__('YES')."</span>"; 
           } else   {
               return "<span class='label label-danger'>".__('NO')."</span>";  
           } 
       }) 
       ->editColumn('created_at',function($date) {
           return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
       })
       ->escapeColumns([])
       ->make(true);     
    }  
    public function getAdd() {  
    	return view('accomodations.add');
    }
    public function postAdd() { 
    	//return view('accomodations.add');
    }   
    public function getAddFree() {
    	$accom=AccomodationType::pluck('type_name','id');
    	return view('accomodations.add_free',compact('accom')); 
    }
    public function postAddFree() {  	
    }
    public function getAddPaid(Request $r) {
        $label=$r->get('label');
        $pay=$r->get('pay');
    	$accom=AccomodationType::pluck('type_name','id'); 
    	return view('accomodations.add_paid',compact('accom','label','pay')); 
    }
    public function getAddAccom(Request $r) {
        $label=$r->get('label');
        $pay=$r->get('pay');
        $accom=AccomodationType::pluck('type_name','id');  
        return view('accomodations.add_accom',compact('accom','label','pay')); 
    }
    public function postAddAccom(Request $r,AccomodationRequest $ar ) {  
       // dd($r);
        $approved=0;
        $accom=new Accomodation();
        $accom->name=$r->get('name');
        $accom->user_id=Auth::user()->id;
        $accom->payment_type=$r->get('payment_type');
        $accom->accomodation_type_id=$r->get('type_id'); 
        $accom->label_id=$r->get('label_id');    
        $accom->email=$r->get('email');
        $accom->website=$r->get('website');
        $accom->youtube_url=$r->get('youtube_url');
        $accom->number_of_persons=$r->get('number_of_persons');
        $accom->description=$r->get('description');
        $accom->bathrooms=$r->get('bathrooms');
        $accom->bedrooms=$r->get('bedrooms');
        $accom->single_beds=$r->get('single_beds');
        $accom->double_beds=$r->get('double_beds');

        if($r->get('payment_type')==1) {
          $approved=1; 
        }
        $accom->approved=$approved;
        if($accom->save()) {
            $accom->slug=$this->makeSlug($accom->id,$accom->name);
            $accom->save();
            return redirect('/accomodations/add_accom_price/'.$accom->id.'?type='.$accom->accomodation_type_id)->with('success',__('Information submitted successfully'));  
        }
        return redirect()->back()->with('error',__('Sorry,Information could not be submitted,please try again!')); 
    }
    public function postAddPaid(Request $r,AccomodationRequest $ar ) {
        $accom=new Accomodation();
        $accom->name=$r->get('name');
        $accom->user_id=Auth::user()->id;
        $accom->payment_type=$r->get('payment_type');
        $accom->accomodation_type_id=$r->get('type_id');
        $accom->label_id=$r->get('label_id'); 
        $accom->email=$r->get('email');
        $accom->website=$r->get('website');
        $accom->number_of_persons=$r->get('number_of_persons');
        $accom->description=$r->get('description');
        $accom->approved=1;
        if($accom->save()) {
            $accom->slug=$this->makeSlug($accom->id,$accom->name);
            $accom->save();
            // redirect to paid accomodation prices
            return redirect('/accomodations/add_paid_price/'.$accom->id.'?type='.$accom->accomodation_type_id)->with('success',__('Information submitted successfully')); 
        }
        return redirect()->back()->with('error',__('Sorry,Information could not be submitted,please try again!')); 
    }
    public function makeSlug($id,$string) { 
        return $id.'-'.str_slug($string); 
    }
}
