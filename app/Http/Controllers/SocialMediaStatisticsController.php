<?php

namespace LBV\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Share;
use Illuminate\Support\Facades\Auth;

class SocialMediaStatisticsController extends Controller
{ 
    public function getPostToMedia(){
    	$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id'); 
    	return view('social_media.post',compact('accoms')); 
    }
    public function postPostToMedia(Request $r){
    	if($r->accom_id) {
    		$slug=DB::table('accomodations')->select('slug','name')->whereId($r->accom_id)->first();
    		$url=url('/').'/accomodations/view/'.$slug->slug;
    		switch ($r->media_id) {
    			case 'fb':
    				$shareUrl=Share::load($url,$slug->name)->facebook();
    				break;
    			case 'gplus':
    				$shareUrl=Share::load($url,$slug->name)->gplus();
    				break;
    			case 'tw':
    				$shareUrl=Share::load($url,$slug->name)->twitter();
    				break;
    			case 'ln':
    				$shareUrl=Share::load($url,$slug->name)->linkedin();
    				break;
    		}
    		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id'); 
    		return view('social_media.post',compact('accoms','shareUrl'));  
    	} else{ 
    		return redirect()->back()->with('error',__('No accomodation found'));
    	}
    }
    public function accomVisits(Request $r){ 
       $info=@unserialize(file_get_contents('http://ip-api.com/php/'.$r->ip())); 
       $accom=DB::table('accomodation_addresses')->select('countries.name','regions.region_name','departments.department_name')
               ->leftJoin('countries','countries.id','=','accomodation_addresses.country_id')
               ->leftJoin('regions','regions.id','=','accomodation_addresses.region_id')
               ->leftJoin('departments','departments.id','=','accomodation_addresses.department_id')
               ->whereAccomId($r->accom_id)->first();
        if($info['status']=='success'){
            DB::table('accomodation_visits')->insertGetId([  
                    'count'=>1,
                    'ip'=>$r->ip(),
                    'accom_id'=>$r->accom_id,
                    'country'=>$info['country'],
                    'country_code'=>$info['countryCode'],
                    'region'=>$info['regionName'],
                    'city'=>$info['city'], 
                    'accom_country'=>$accom->name,
                    'accom_region'=>$accom->region_name,
                    'accom_department'=>$accom->department_name,
                    'json'=>json_encode($info),
                    'status'=>$info['status'],
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
            ]);
        } else{
            DB::table('accomodation_visits')->insertGetId([
                    'count'=>1,
                    'ip'=>$r->ip(),
                    'accom_id'=>$r->accom_id,
                    'country'=>$info['country'],
                    'country_code'=>$info['countryCode'],
                    'region'=>$info['regionName'],
                    'city'=>$info['city'], 
                    'accom_country'=>$accom->name,
                    'accom_region'=>$accom->region_name,
                    'accom_department'=>$accom->department_name,
                    'json'=>json_encode($info),
                    'status'=>$info['status'],  
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
            ]); 
        } 

    }
    public function getStatistics(){
        $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereApproved(1)->whereCompleted(1)->whereSubsValidLicense(1)->pluck('name','id');
        return view('social_media.statistics',compact('accoms'));
    }
    public function getAccomLocation($accom){
        return DB::table('accomodation_addresses')->whereAccomId($accom)->select('countries.name','countries.code','regions.region_name','departments.department_name')
            ->leftJoin('countries','countries.id','=','accomodation_addresses.country_id')
            ->leftJoin('regions','regions.id','=','accomodation_addresses.region_id')
            ->leftJoin('departments','departments.id','=','accomodation_addresses.department_id')
            ->first();
    }
    public function getAccomStats(Request $r){ 
        $accom_id=$r->accom_id;
        $country=$this->getAccomLocation($accom_id); 
        $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW() AND accom_id=$accom_id GROUP BY y"));
        $data['location']=$country;
        $data['siteVisits']=$siteVisits;
        echo json_encode($data);  
    }
    public function getDailyStats(Request $r){
        $accom_id=$r->accom_id;
        if($accom_id){
            // $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 30 DAY ) AND NOW() AND accom_id=$accom_id  GROUP BY y"));
            $country=$this->getAccomLocation($accom_id);
            if($r->yes){
                $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 30 DAY ) AND NOW() AND accom_id=$accom_id  GROUP BY y"));
            }else{
                switch ($r->selectedtab) {
                    case 'Yearly':
                        $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE accom_id=$accom_id  GROUP BY y"));
                        break;
                    case 'Monthly':
                        $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id  GROUP BY y"));
                        break;
                    case 'Daily':
                        $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 30 DAY ) AND NOW() AND accom_id=$accom_id  GROUP BY y"));
                        break;
                }
            }
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            }
        }else{
            $data=[];
        } 
        echo json_encode($data);     
    }
    public function getAllStats(Request $r){
        $accom_id=$r->accom_id; 
        if($accom_id){
            $country=$this->getAccomLocation($accom_id);
            switch ($r->selectedtab) {
                case 'Yearly':
                    $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE accom_id=$accom_id  GROUP BY y"));
                    break;
                case 'Monthly':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id  GROUP BY y"));
                    break;
                case 'Maandelijks':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id  GROUP BY y"));
                    break;
                case 'Daily':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 30 DAY ) AND NOW() AND accom_id=$accom_id  GROUP BY y"));
                    break;
                case 'Dagelijks':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 30 DAY ) AND NOW() AND accom_id=$accom_id  GROUP BY y"));
                    break;
            }
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            }
        }else{
            $data=[];
        } 
        echo json_encode($data);     
    }
    public function getMonthlyStats(Request $r){
        $accom_id=$r->accom_id; 
        if($accom_id){
            $country=$this->getAccomLocation($accom_id);
            $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id GROUP BY y"));
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            }
        }else{
            $data=[];
        }
        echo json_encode($data);     
    }
    public function getYearlyStats(Request $r){
        $accom_id=$r->accom_id;
        if($accom_id){
            $country=$this->getAccomLocation($accom_id);
            $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id GROUP BY y"));
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            }
        }else{
            $data=[];
        } 
        echo json_encode($data);        
    }
    public function getCountryStats(Request $r){ 
        $accom_id=$r->accom_id;
        if($accom_id){ 
            $country=$this->getAccomLocation($accom_id);
            switch ($r->selectedtab) {
                case 'Yearly':
                    $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_country='$country->name' GROUP BY y"));
                    break;
                case 'Monthly':
                    // $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND country_code='$country->code' GROUP BY y"));
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_country='$country->name' GROUP BY y"));
                    break;
                case 'Maandelijks':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_country='$country->name' GROUP BY y"));
                    break;
                case 'Daily':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_country='$country->name' GROUP BY y"));
                    break;
                case 'Dagelijks':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_country='$country->name' GROUP BY y"));
                    break;
            } 
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            } 
        }else{ 
            $data=[];
        } 
        echo json_encode($data);        
    }
    public function getRegionStats(Request $r){ 
        $accom_id=$r->accom_id;
        if($accom_id){ 
            $country=$this->getAccomLocation($accom_id);
            switch ($r->selectedtab) {
                case 'Yearly':
                    $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_region='$country->region_name' GROUP BY y"));
                    break;
                case 'Monthly':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_region='$country->region_name' GROUP BY y"));
                    break;
                case 'Maandelijks':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_region='$country->region_name' GROUP BY y"));
                    break;
                case 'Daily':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_region='$country->region_name' GROUP BY y"));
                    break;
                case 'Dagelijks':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_region='$country->region_name' GROUP BY y"));
                    break;

            }
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            } 
        }else{
            $data=[];
        } 
        echo json_encode($data);        
    }
    public function getDepartmentStats(Request $r){ 
        $accom_id=$r->accom_id;
        if($accom_id){
            $country=$this->getAccomLocation($accom_id);
            switch ($r->selectedtab) {
                case 'Yearly':
                    $siteVisits=DB::select(DB::raw("SELECT YEAR(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_department='$country->department_name' GROUP BY y"));
                    break;
                case 'Monthly':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_department='$country->department_name' GROUP BY y"));
                    break;
                case 'Maandelijks':
                    $siteVisits=DB::select(DB::raw("SELECT MONTH(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_department='$country->department_name' GROUP BY y"));
                    break;
                case 'Daily':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_department='$country->department_name' GROUP BY y"));
                    break;
                case 'Dagelijks':
                    $siteVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE  accom_id=$accom_id AND accom_department='$country->department_name' GROUP BY y"));
                    break;

            }
            $data['location']=$country;
            $data['siteVisits']=$siteVisits;
            foreach ($data['siteVisits'] as $d) {
                $d->y="".$d->y."";
            } 
        }else{
            $data=[];
        } 
        echo json_encode($data);        
    } 
}
