<?php

namespace LBV\Http\Controllers;

use DB;
use PDF;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use LBV\Model\SubscriptionLicense;
use Illuminate\Support\Facades\Auth;

class SubscriptionPaymentsController extends Controller
{
    public function index(){
    	return view('subscription_payments.index');
    }
    public function getSubscriptionPaymentsAjax(Request $r) {
    	/*DB::enableQueryLog();
    	dd(DB::getQueryLog()); */
    	if($r->type) {
    		if($r->type=='completed') {
    			$payments=$this->giveSubscriptionPayments([
    				['subscription_licenses.user_id',Auth::user()->id]
    			],[1,3]);
    		} else if($r->type=='cancelled') { 
    			$payments=$this->giveSubscriptionPayments([
    				['subscription_licenses.user_id',Auth::user()->id]
    			],[2]);
    		} else {
    			$payments=$this->giveSubscriptionPayments([
    				['subscription_licenses.user_id',Auth::user()->id],
    				['subscription_payments.payment_complete',0]
    			]);
    		}
    		//dd($payments);
    		return Datatables::of($payments) 
				->addColumn('action', function ($l) {   
					 $str='<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
				    return $str;    
				})  
				->editColumn('payment_complete', function ($payment) {   
				    if($payment->payment_complete==1) {
				    	return "<span class='label label-success'>".__('COMPLETED')."</span>";
				    } else if($payment->payment_complete==2) {
				    	return "<span class='label label-danger'>".__('CANCELED')."</span>";
				    } else if($payment->payment_complete==3) {
				    	return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
				    } else if($payment->payment_complete==0) {
				    	return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
				    }
				}) 
                ->editColumn('pName',function($name) {
                        return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
                })   
				->editColumn('valid_upto',function($date)  {
			        $str=date('d-m-Y',strtotime($date->valid_upto));
			        if( strtotime('now')>strtotime($date->valid_upto)) {
			            $str .=' <span class="label label-danger">'.__('Expired').'</span>';
			        } else {
			            if($date->payment_complete==1 || $date->payment_complete==3 ) {
			               $str .=' <span class="label label-success">'.__('Active').'</span>';      
			            } else if($date->payment_complete==2) {
			                $str .=' <span class="label label-danger">'.__('Cancelled').'</span>';    
			            } else if($date->payment_complete==0) {
			                 $str .=' <span class="label label-danger">'.__('Not-completed').'</span>';    
			            } 
			        } 
			        if(!$date->license_renewed) {
			          if(in_array($date->payment_complete,[1,3])) { 
			            $str .='<br><a class="label label-default" href="/subscriptions/pay-renew/'.$date->accomId.'">'.__('Renew/Extend').'</a>';   
			          } 
			        } 
			        return $str;
				})
				->editColumn('created_at',function($date) {
					return date('d-m-Y',strtotime($date->created_at));
				})
				->escapeColumns([])
				->make(true);
    	}
    }
    public function subscriptionHistory() { 
       $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->orderBy('name','asc')->pluck('name','id');
       return view('subscription_payments.history',compact('accoms')); 
    }
    public function subscriptionHistoryAjax(Request $r) {
        if($r->accom_id) {
            if($r->type) {
                if($r->type=='active') {
                    $history=$this->giveSubscriptionPayments([
                        ['subscription_licenses.user_id',Auth::user()->id],
                        ['subscription_licenses.valid_upto','>=',date('Y-m-d')],
                        ['subscription_licenses.accom_id',$r->accom_id]
                    ],[1,3]);
                } else {
                    $history=$this->giveSubscriptionPayments([ 
                        ['subscription_licenses.user_id',Auth::user()->id],
                        ['subscription_licenses.valid_upto','<',date('Y-m-d')],
                        ['subscription_licenses.accom_id',$r->accom_id]
                    ]);
                }
                //dd($history);
                return Datatables::of($history)  
                    ->addColumn('action', function ($l) {   
                             $str='<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                            return $str;    
                        })  
                    ->editColumn('payment_complete', function ($payment) {   
                        if($payment->payment_complete==1) {
                            return "<span class='label label-success'>".__('COMPLETED')."</span>";
                        } else if($payment->payment_complete==2) {
                            return "<span class='label label-danger'>".__('CANCELED')."</span>";
                        } else if($payment->payment_complete==3) {
                            return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                        } else if($payment->payment_complete==0) {
                            return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                        }
                    })
                    ->editColumn('pName',function($name) {
                            return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
                    })  
                    ->editColumn('start_from',function($date)  {
                        return date('d-m-Y',strtotime($date->start_from)); 
                    })   
                    ->editColumn('valid_upto',function($date)  {
                        $str=date('d-m-Y',strtotime($date->valid_upto));
                        if( strtotime('now')>strtotime($date->valid_upto)) {
                            $str .=' <span class="label label-danger">'.__('Expired').'</span>';
                        } else {
                            if($date->payment_complete==1 || $date->payment_complete==3 ) {
                               $str .=' <span class="label label-success">'.__('Active').'</span>';      
                            } else if($date->payment_complete==2) {
                                $str .=' <span class="label label-danger">'.__('Cancelled').'</span>';    
                            } else if($date->payment_complete==0) {
                                 $str .=' <span class="label label-danger">'.__('Not-completed').'</span>';    
                            } 
                        } 
                        if(!$date->license_renewed) {
                          if(in_array($date->payment_complete,[1,3])) { 
                            $str .='<br><a class="label label-default" href="/subscriptions/pay-renew/'.$date->accomId.'">'.__('Renew/Extend').'</a>';   
                          } 
                        } 
                        return $str;
                    })
                    ->escapeColumns([])
                    ->make(true);  
            }
        }
    }
    public function payments(){
        return view('subscription_payments.payments');
    }
    public function getAdminSubscriptionPaymentsAjax(Request $r) {  
        if($r->type) {
            if($r->type=='completed') {
                $payments=$this->giveSubscriptionPaymentsAdmin([1,3]);
            } else if($r->type=='cancelled') { 
                $payments=$this->giveSubscriptionPaymentsAdmin([2]);
            } else {
                $payments=$this->giveSubscriptionPaymentsAdmin([0]);
            } 
            return Datatables::of($payments) 
                ->addColumn('action', function ($l) {   
                     $str='<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                    return $str;    
                })  
                ->editColumn('payment_complete', function ($payment) {   
                    if($payment->payment_complete==1) {
                        return "<span class='label label-success'>".__('COMPLETED')."</span>";
                    } else if($payment->payment_complete==2) {
                        return "<span class='label label-danger'>".__('CANCELED')."</span>";
                    } else if($payment->payment_complete==3) {
                        return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                    } else if($payment->payment_complete==0) {
                        return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                    }
                })
                ->editColumn('pName',function($name) {
                    return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
                })   
                ->editColumn('valid_upto',function($date)  {
                    $str=date('d-m-Y',strtotime($date->valid_upto));
                    if( strtotime('now')>strtotime($date->valid_upto)) {
                        $str .=' <span class="label label-danger">'.__('Expired').'</span>';
                    } else {
                        if($date->payment_complete==1 || $date->payment_complete==3 ) {
                           $str .=' <span class="label label-success">'.__('Active').'</span>';      
                        } else if($date->payment_complete==2) {
                            $str .=' <span class="label label-danger">'.__('Cancelled').'</span>';    
                        } else if($date->payment_complete==0) {
                             $str .=' <span class="label label-danger">'.__('Not-completed').'</span>';    
                        } 
                    }  
                    return $str;
                })
                ->editColumn('created_at',function($date) {
                    return date('d-m-Y',strtotime($date->created_at));
                })
                ->escapeColumns([])
                ->make(true);
        }
    }
    public function giveSubscriptionPayments($where=array(),$whereIn=array()) {
        if($where) {
            if($whereIn) {
                $payment=SubscriptionLicense::where($where)->whereIn('subscription_payments.payment_complete',$whereIn)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId','subscription_payments.created_at',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            } else {
                $payment=SubscriptionLicense::where($where)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId','subscription_payments.created_at',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            }
           
            return $payment;
        } else {
            return false;
        }
    }
    public function getIncome($where=[],$from=null,$to=null) {
        if($from && $to) {
            if($where) {
                $income=SubscriptionLicense::whereBetween('subscription_payments.created_at', [date('Y-m-d',strtotime($from)).' 00:00:00', date('Y-m-d',strtotime($to)).' 23:59:59'])
                    ->where($where)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','subscription_payments.user_id','=','users.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId','subscription_payments.created_at',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'users.first_name as uFName','users.last_name as uLName','mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            } else {
                $income=SubscriptionLicense::where($where)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId','subscription_payments.created_at',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'users.first_name as uFName','users.last_name as uLName','mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            }
        } else {
            if($where) {
                //dd($where);
                $income=SubscriptionLicense::where($where)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','subscription_payments.user_id','=','users.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId','subscription_payments.created_at',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'users.first_name as uFName','users.last_name as uLName','mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get();       
            }
        }
        return $income; 
    }
    public function giveSubscriptionPaymentsAdmin($whereIn=array()) {
        if($whereIn) {
            $payment=SubscriptionLicense::whereIn('subscription_payments.payment_complete',$whereIn) 
                 ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                 ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                 ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                 ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                 ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                 ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                 ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                 'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                 'accomodations.id as accomId','pay_methods.method_name as pName',
                                 'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                 'subscription_payments.id as paymentId','subscription_payments.created_at',
                                 'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                 'mollie_subscription_payments.method' 
                             )
                 ->orderBy('subscription_licenses.id','desc')
                 ->get();
                return $payment;   
        }        
    } 
    public function subPaymentsIncome(){
        $SUM=DB::table('subscription_payments')->where('payment_complete',1)->sum('amount_paid');
        $SUMPENDING=DB::table('subscription_payments')->where('payment_complete',0)->sum('amount_paid');
        return view('subscription_payments.income',compact('SUM','SUMPENDING')); 
    }
    public function findSubTotalPaidAmount(Request $r) { 
        $returnArr=[];
        if($r->start_date && $r->end_date) {
            $SUM=DB::table('subscription_payments')->whereBetween('subscription_payments.created_at', [date('Y-m-d',strtotime($r->start_date)), date('Y-m-d',strtotime($r->end_date))])
            ->where('payment_complete',1)->sum('amount_paid'); 
            $PENDINGSUM=DB::table('subscription_payments')->whereBetween('subscription_payments.created_at', [date('Y-m-d',strtotime($r->start_date)), date('Y-m-d',strtotime($r->end_date))])
            ->where('payment_complete',0)->sum('amount_paid');
            $returnArr['sum']=$SUM; 
            $returnArr['pSum']=$PENDINGSUM; 
        } 
        echo json_encode($returnArr);
    }

    public function subPaymentsIncomeAjax(Request $r){
        if($r->type) {    
            switch ($r->type) {  
                case 'completed': 
                     if($r->start_date) {
                         $payments=$this->getIncome([ ['subscription_payments.payment_complete',1]],$r->start_date,$r->end_date);
                     } else {
                        $payments=$this->getIncome([ ['subscription_payments.payment_complete',1]]);
                     }
                    break;
                case 'pending': 
                   if($r->start_date) {
                       $payments=$this->getIncome([ ['subscription_payments.payment_complete',0]], $r->start_date,$r->end_date);
                   } else {
                       $payments=$this->getIncome([ ['subscription_payments.payment_complete',0]]); 
                   }
                    break;
            }
            return Datatables::of($payments)
            ->addColumn('action', function ($l) {  
                   $str='&nbsp;<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                  return $str;    
            })  
            ->editColumn('payment_complete', function ($payment) {   
                  if($payment->payment_complete==1) {
                      return "<span class='label label-success'>".__('COMPLETED')."</span>";
                  } else if($payment->payment_complete==2) {
                      return "<span class='label label-danger'>".__('CANCELED')."</span>";
                  } else if($payment->payment_complete==3) {
                      return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                  } else if($payment->payment_complete==0) {
                      return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                  }
            })
            ->editColumn('pName',function($name) {
                return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
            })
            ->editColumn('created_at',function($date) {
                return date('d-m-Y',strtotime($date->created_at));
            }) 
            ->editColumn('uFName',function($date) {
                return $date->uFName.' '.$date->uLName;
            })  
            ->escapeColumns([])
            ->make(true); 
        }
    }


}
