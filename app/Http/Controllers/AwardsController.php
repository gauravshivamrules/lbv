<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use LBV\Model\Award;
use LBV\Model\AccomodationAward;
use LBV\Custom\Notifications;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables; 
use Illuminate\Support\Facades\Auth;
use LBV\Mail\AskNewAward;
use LBV\Mail\NotifyAdminsForAwardRequest;


class AwardsController extends Controller
{
	public function index() {
		return view('awards.index');
	}
	public function viewAward($id=null){
		if($id) {
			$award=Award::whereId($id)->first();
			return view('awards.view',compact('award')); 
		} else {
			return redirect()->back()->with('error',__('Inavalid request'));
		} 
	}
	public function getEditAward($id=null){
		if($id) {
			$award=Award::whereId($id)->first(); 
			return view('awards.edit',compact('award')); 
		}else {
			return redirect()->back()->with('error',__('Inavalid request'));
		}
	}
	public function addAward(Request $r) {
	    $this->validate($r,[
	    		'award_title'=>'required',
	    		'award_image'=>'required|image|mimes:jpeg,png,jpg,gif|max:1024'
	    	]
	    );
	    if(isset($r->award_image)) {
	    	if($r->file('award_image')->getClientSize() >0) { 
	    		$file=$r->file('award_image');
	    		$path=pathinfo($r->file('award_image')->getClientOriginalName());
	    		$imageName=time().'.'.$path['extension'];
	    		$destination=public_path().'/images/awards/';
	    		$award=DB::table('awards')->insertGetId([
	    			'title'=>$r->award_title,
	    			'image'=>$imageName,
	    			'created_at'=>date('Y-m-d H:i:s'),
	    			'updated_at'=>date('Y-m-d H:i:s')
	    		]);
	    		if($award) {
	    			if($file->move($destination,$imageName)) {
	    				return redirect('/awards')->with('success',__('Award added successfully'));
	    			} else {
	    				return redirect('/awards')->with('error',__('Sorry,award not added,please try again!'));
	    			}
	    		} else {
	    			return redirect('/awards')->with('error',__('Sorry,award not added,please try again!'));
	    		}
	    	}
	    }
	    return redirect('/awards')->with('error',__('Invalid request')); 
	}
	public function postEditAward(Request $r,$id) {
		if($id) {
			$this->validate($r,[
					'award_title'=>'required',
					'award_image'=>'image|mimes:jpeg,png,jpg,gif|max:1024'
				]
			);
			if(isset($r->award_image)) {
				if($r->file('award_image')->getClientSize()>0) { 
					$file=$r->file('award_image');
					$path=pathinfo($r->file('award_image')->getClientOriginalName());
					$imageName=time().'.'.$path['extension'];
					$destination=public_path().'/images/awards/';
					$oldImg=Award::select('image')->whereId($id)->first();
					$award=DB::table('awards')->whereId($id)->update([
						'title'=>$r->award_title,
						'image'=>$imageName,
						'updated_at'=>date('Y-m-d H:i:s')
					]);
					if($award) {
						if($file->move($destination,$imageName)) {
							if(file_exists($destination.$oldImg->image)) {
								unlink($destination.$oldImg->image); 
							}
							return redirect('/awards')->with('success',__('Award updated successfully'));
						} else {
							return redirect('/awards')->with('error',__('Sorry,award not updated,please try again!'));
						} 
					} else {
						return redirect('/awards')->with('error',__('Sorry,award not updated,please try again!'));
					}
				}
			} else {
				$award=DB::table('awards')->whereId($id)->update([
						'title'=>$r->award_title,
						'updated_at'=>date('Y-m-d H:i:s')
					]);
				if($award) {
					return redirect('/awards')->with('success',__('Award updated successfully')); 
				} else {
					return redirect('/awards')->with('error',__('Sorry,award not added,please try again!'));
				}
			}
		}
	    return redirect('/awards')->with('error',__('Invalid request')); 
	}
	public function propertyName($value) {
		$name=DB::table('accomodations')->select('name')->whereId($value)->first();
		return $name->name;
	}
	public function getUserByAccom($accom){
		$user=DB::table('accomodations')->select('user_id')->whereId($accom)->first();
		return $user->user_id; 
	}
	public function advertiserAddAward(){
		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id'); 
		if(!$accoms->count()) { 
			return redirect()->back()->with('error',__('Sorry you must have a valid accomodation to add award'));
		}
		$awards=DB::table('awards')->orderBy('title','asc')->pluck('title','id');
		return view('awards.adv_add',compact('accoms','awards'));
	}
	public function postAdvertiserAddAward(Request $r){
		if($r->accom_id){
			$award=new AccomodationAward();
			$award->accom_id=$r->accom_id;
			$award->award_id=$r->award_id;
			if($award->save()) {
				Notifications::notifyAdmin([ 
				        'accom_id'=>$r->accom_id,
				        'from_user'=>Auth::user()->id,
				        'message'=>'A new award request has been received for accomodation '.$this->propertyName($r->accom_id),
				        'request_type_url'=>'/awards/new-award'
				]);  
				 $admins=DB::table('users')->select('email')->where('role_id',1)->get();
				 $this->notifyAdminsForAwardRequest($admins,
				         'New award add request has been received.',
				         'A new award request has been received for accomodation '.$this->propertyName($r->accom_id)
				 ); 
				return redirect('/awards/my-awards')->with('success',__('Request sent successfully,waiting admin approval'));
			}
			return redirect()->back()->with('error',__('Sorry,request cannot be sent,please try again!'));
		} else {
			return redirect()->back()->with('error',__('Invalid request, no accomodation found'));
		}
	}
	public function notifyAdminsForAwardRequest($admins,$subject,$msg) {
		if($admins) {
			foreach ($admins as $a) {
				try {
					Mail::to($a->email)->send(new NotifyAdminsForAwardRequest($subject,$msg));     
				} catch(Exception $e) {
					\Log::log('notifyAdminsForAwardRequest','Mail not sent to '.Auth::user()->id.' Server returned error: '.$e->getMessage()); 
				}
			}
		}
	}
	public function getMyAwards(){ 
		return view('awards.my_awards');  
	}
	public function getMyAwardsAjax() {
		$awards=DB::table('accomodation_awards')
				->leftJoin('accomodations', 'accomodation_awards.accom_id', '=', 'accomodations.id')
				->leftJoin('awards', 'accomodation_awards.award_id', '=', 'awards.id')
				->select('accomodation_awards.id', 'awards.title','accomodations.name','accomodation_awards.deletion_request')
				->whereUserId(Auth::user()->id)->get();
		return Datatables::of($awards) 
		->addColumn('action', function ($a) {    
			$str='<a href="/awards/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
			if(!$a->deletion_request) {
				$str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			}
			return $str;   
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function askAward(Request $r){
		$this->validate($r,[
				'award_title'=>'required',
				'award_image'=>'required|image|mimes:jpeg,png,jpg,gif|max:1024'
			]
		);
		if(isset($r->award_image)) {
			if($r->file('award_image')->getClientSize() >0) { 
				$file=$r->file('award_image');
				$path=pathinfo($r->file('award_image')->getClientOriginalName());
				$imageName=time().'.'.$path['extension'];
				$destination=public_path().'/images/awards/tmp_awards/';
				$file->move($destination,$imageName);
				$admins=DB::table('users')->select('email')->where('role_id',1)->get();
				$subject='New award add request has been received.'; 
				$msg='New award add request has been received by advertiser '.Auth::user()->first_name.' '.Auth::user()->last_name;
				foreach($admins as $a) {
					try { 
					    Mail::to($a->email)->send(new AskNewAward($msg,$subject,$imageName)); 
					    \Log::log('NewAwardImageSent','New award image sent to  '.Auth::user()->email); 
					} catch(Exception $e) { 
						\Log::log('NewAwardImageSentFailed','New award image not sent to '.$a->email); 
					}
				}
				if(file_exists(public_path().'/images/awards/tmp_awards/'.$imageName)) {
					unlink(public_path().'/images/awards/tmp_awards/'.$imageName );   
				}
				return redirect()->back()->with('success',__('Request sent successfully')); 
			}
		}
	}
	public function getAwardsAjax() {
		$awards=DB::table('awards')->select('id','title')->get();
		$str='';
		return Datatables::of($awards) 
		->addColumn('action', function ($a) {    
			$str='<a href="/awards/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
			$str.='&nbsp;<a href="/awards/edit/'.$a->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
			$str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			return $str;   
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function getNewAwardsAjax() {
		$awards=DB::table('accomodation_awards')
				->leftJoin('accomodations', 'accomodation_awards.accom_id', '=', 'accomodations.id')
				->leftJoin('awards', 'accomodation_awards.award_id', '=', 'awards.id')
				->leftJoin('users', 'users.id', '=', 'accomodations.user_id')
				->select('accomodation_awards.id','accomodation_awards.approved' ,'awards.title','accomodations.name','users.first_name','users.last_name')
				->where('accomodation_awards.approved',0)->get();
		return Datatables::of($awards) 
		->addColumn('action', function ($a) {   
			$str='&nbsp;<a  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger" title="Delete"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			if(!$a->approved) {
				$str.='&nbsp;<a href="javascript:;" onclick="confirmApprove('.$a->id.');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-thumbs-up" title="Approve"></i> </a>'; 
			} 
			return $str;   
		})
		->editColumn('first_name', function ($a) {     
			return $a->first_name.' '.$a->last_name;
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function getNewAwards() {
		return view('awards.new_awards');
	
	}
	public function getDeleteAwards() {
		return view('awards.delete_awards');
	
	} 
	public function getDeleteAwardsAjax(){
		$awards=DB::table('accomodation_awards')
				->leftJoin('accomodations', 'accomodation_awards.accom_id', '=', 'accomodations.id')
				->leftJoin('awards', 'accomodation_awards.award_id', '=', 'awards.id')
				->leftJoin('users', 'users.id', '=', 'accomodations.user_id')
				->select('accomodation_awards.id','accomodation_awards.approved' ,'awards.title','accomodations.name','users.first_name','users.last_name')
				->where('accomodation_awards.deletion_request',1)->get();
		return Datatables::of($awards) 
		->addColumn('action', function ($a) {    
			$str='&nbsp;<a onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			return $str;   
		})
		->editColumn('first_name', function ($a) {     
			return $a->first_name.' '.$a->last_name;
		}) 
		->escapeColumns([])
		->make(true);  
	}
	public function approveAward($id){
		if($id) {
			$award=AccomodationAward::whereId($id)->first();
			if($award) {
				$award->approved=1;
				if($award->save()) {
					Notifications::notifyUser([  
                        'accom_id'=>$award->accom_id,
                        'message'=>'Your award request has been approved for accomodation '.$this->propertyName($award->accom_id),  
                        'to_user'=>$this->getUserByAccom($award->accom_id),  
                        'request_type_url'=>'/awards/my-awards'
                    ]); 
					return redirect()->back()->with('success',__('Award request approved successfully'));
				} else {
					return redirect()->back()->with('error',__('Sorry,award request could not be approved,please try again!'));
				}
			} 
		}
	}
	public function deleteAccomodationAward($id=null) {  
		if($id) {
			$award=AccomodationAward::whereId($id)->first();
			if($award) {
				$oldAward=$award; 
				if($award->delete()) {
					Notifications::notifyUser([  
                        'accom_id'=>$oldAward->accom_id,
                        'message'=>'Your award has been deleted for accomodation '.$this->propertyName($oldAward->accom_id),  
                        'to_user'=>$this->getUserByAccom($oldAward->accom_id),  
                        'request_type_url'=>'/awards/my-awards'
                    ]); 
					return redirect()->back()->with('success',__('Award deleted successfully'));	 
				} else {
					return redirect()->back()->with('error',__('Sorry,cannot delete award,please try again!'));	
				}
			} else {
				return redirect()->back()->with('error',__('Award does not exist'));
			}
		} else {
			return redirect()->back()->with('error',__('Id is required'));
		}
	}
	public function requestAccomodationDeleteAward($id=null) {  
		if($id) {
			$award=AccomodationAward::whereId($id)->first();
			if($award) {
				$award->deletion_request=1;
				if($award->save()) {
					Notifications::notifyAdmin([ 
					        'accom_id'=>$award->accom_id,
					        'from_user'=>Auth::user()->id,
					        'message'=>'Award delete request has been received for accomodation '.$this->propertyName($award->accom_id),
					        'request_type_url'=>'/awards/delete-award' 
					]);
					 $admins=DB::table('users')->select('email')->where('role_id',1)->get();
					 $this->notifyAdminsForAwardRequest($admins,
					         'Award delete request has been received.', 
					         'Award delete request has been received for accomodation '.$this->propertyName($award->accom_id)
					 );  
					return redirect()->back()->with('success',__('Award deletion request sent successfully,waiting approval'));	 
				} else {
					return redirect()->back()->with('error',__('Sorry,cannot send request,please try again!'));	
				}
			} else {
				return redirect()->back()->with('error',__('Award does not exist'));
			}
		} else {
			return redirect()->back()->with('error',__('Id is required'));
		}
	}
	public function deleteAward($id=null) {
		if($id) {
			$award=Award::whereId($id)->first();
			if($award) {
				$img=$award->image;
				if($award->delete()) {
					$FILETODELETE=public_path().'/images/awards/'.$img;
					\File::delete($FILETODELETE); 
					return redirect()->back()->with('success',__('Award deleted successfully'));	 
				} else {
					return redirect()->back()->with('error',__('Sorry,cannot delete award,please try again!'));	
				}
			} else {
				return redirect()->back()->with('error',__('Award does not exist'));
			}
		} else {
			return redirect()->back()->with('error',__('Id is required'));
		}
	} 
}
