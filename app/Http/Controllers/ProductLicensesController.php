<?php

namespace LBV\Http\Controllers;
use DB;
use PDF;
use Mail;
use Mollie;
use LBV\User;
use LBV\Model\Product;
use LBV\Model\Accomodation;
use LBV\Model\ProductLicense; 
use LBV\Model\SubscriptionLicense;
use LBV\Model\ProductPayment;
use LBV\Model\ProductCategory;
use LBV\Model\FreeProductLicense;
use Illuminate\Http\Request;
use LBV\Custom\Notifications;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use LBV\Mail\SendFreeProductCoupon;
use LBV\Mail\SendProductPaymentInvoice;
use LBV\Mail\NotifyAdminsForProductPurchase;
use LBV\Mail\SendProductPaymentInvoiceAdmin;


class ProductLicensesController extends Controller
{
    public function buyPromotion() {
      $isEligible=SubscriptionLicense::whereUserId(Auth::user()->id)->whereLicenseRenewed(0)->orderBy('id','desc')->first();
      if($isEligible) {
        if(strtotime($isEligible->valid_upto)>=strtotime('now')) {
          return $this->returnView();
        } else {
          $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->wherePaymentType(2)->get(); 
          if($accoms->count()) { 
            return $this->returnView();
          } else {
            return redirect()->back()->with('error',__('Sorry you must have a valid subscription license to buy promotions')); 
          } 
        }
      } else {
        $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->wherePaymentType(2)->get(); 
        if($accoms->count()) { 
          return $this->returnView();
        } else {
          return redirect()->back()->with('error',__('Sorry you must have a valid subscription license to buy promotions'));  
        } 
      } 
    }
    public function returnView(){ 
      $category=ProductCategory::select('id','name')->whereActive(1)->get(); 
      return view('product_licenses.buy',compact('category'));
    }
    public function getPromotion(Request $r){ 
    	if($r->id) {
    		    $response=[];
            $whereIN=[0,1,3];
            $nextYear=date('Y')+1;
            $nextToNextYear=$nextYear+1;
    		if($r->id==1) {
                  $monthSold=$allMonths=$count=[];
                  $monthSoldNextYear=$allMonthsNextYear=$countNextYear=$monthsToHideNextYear=[]; 
                  $monthSoldNextToNextYear=$allMonthsNextToNextYear=$countNextToNextYear=$monthsToHideNextToNextYear=[]; 
                  $allTipSold=$this->giveProductLicense([
                    ['product_licenses.product_year',date('Y')],
                    ['product_instances.product_category_id',1]
                    ],$whereIN); 
                  foreach($allTipSold as $a) { 
                      if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                          array_push($monthSold,$a->product_month); 
                          $allMonths[$a->product_month]=$a->product_month; 
                      }
                  }
                  foreach($allMonths as $m) {
                      $count[$m]=$this->giveProductLicense([
                          ['product_licenses.product_month',$m],
                          ['product_licenses.product_year',date('Y')],
                          ['product_instances.product_category_id',1]
                      ],$whereIN)->count();
                  }
                  $monthsToHide=[]; 
                  foreach ($count as $key => $value) { 
                      if($value>5) {
                          array_push($monthsToHide,$key);
                      } 
                  }
                  $uniqueMonthsToHide=array_unique($monthsToHide);
                  $products=Product::where('product_category_id',1)
                                      ->whereNotIn('product_month',$uniqueMonthsToHide)
                                      ->where('product_month','>=',date('n'))
                                      ->where('product_year',date('Y'))
                                      ->get()->toArray();
                  
                   /*NEXT YEAR*/
                  $allTipSoldNextYear=$this->giveProductLicense([
                    ['product_licenses.product_year',$nextYear],
                    ['product_instances.product_category_id',1]
                  ],$whereIN);
                  foreach($allTipSoldNextYear as $a) { 
                      if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                          array_push($monthSoldNextYear,$a->product_month); 
                          $allMonthsNextYear[$a->product_month]=$a->product_month; 
                      }
                  }
                  foreach($allMonthsNextYear as $m) {
                      $count[$m]=$this->giveProductLicense([
                          ['product_licenses.product_month',$m],
                          ['product_licenses.product_year',$nextYear],
                          ['product_instances.product_category_id',1]
                      ],$whereIN)->count();
                  }
                  foreach ($count as $key => $value) { 
                      if($value >10) {
                          array_push($monthsToHideNextYear,$key);
                      } 
                  }
                  $uniqueMonthsToHideNextYear=array_unique($monthsToHideNextYear);
                  $productsNextYear=Product::where('product_category_id',1)
                                    ->where('product_year',$nextYear)
                                    ->whereNotIn('product_month',$uniqueMonthsToHideNextYear)
                                    ->get()->toArray();

                  /* NEXT TO NEXT YEAR*/
                  $allTipSoldNextToNextYear=$this->giveProductLicense([
                    ['product_licenses.product_year',$nextToNextYear],
                    ['product_instances.product_category_id',1]
                  ],$whereIN);
                  foreach($allTipSoldNextToNextYear as $a) { 
                      if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                          array_push($monthSoldNextToNextYear,$a->product_month); 
                          $allMonthsNextToNextYear[$a->product_month]=$a->product_month; 
                      }
                  }
                  foreach($allMonthsNextToNextYear as $m) {
                      $count[$m]=$this->giveProductLicense([
                          ['product_licenses.product_month',$m],
                          ['product_licenses.product_year',$nextToNextYear],
                          ['product_instances.product_category_id',1]
                      ],$whereIN)->count();
                  }
                  foreach ($count as $key => $value) { 
                      if($value>5) {
                          array_push($monthsToHideNextToNextYear,$key); 
                      } 
                  }
                  $uniqueMonthsToHideNextToNextYear=array_unique($monthsToHideNextToNextYear);
                  $productsNextToNextYear=Product::where('product_category_id',1)
                                      ->whereNotIn('product_month',$uniqueMonthsToHideNextToNextYear)
                                      ->where('product_year',$nextToNextYear)
                                      ->get()->toArray();
                  $products=array_merge($products, $productsNextYear,$productsNextToNextYear); 
            if($products) { 
              $response['success']=true;
              $response['products']=$products; 
            } else {
              $response['success']=false;
            }
    		} else if($r->id==2) {
    			// SPOTLIGHT
                $monthSold=$allMonths=$count=[];
                $monthSoldNextYear=$allMonthsNextYear=$countNextYear=$monthsToHideNextYear=[]; 
                $monthSoldNextToNextYear=$allMonthsNextToNextYear=$countNextToNextYear=$monthsToHideNextToNextYear=[]; 
                $allSpotSold=$this->giveProductLicense([
                    ['product_licenses.product_year',date('Y')],
                    ['product_instances.product_category_id',2]
                ],$whereIN);
                foreach($allSpotSold as $a) { 
                    if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                        array_push($monthSold,$a->product_month); 
                        $allMonths[$a->product_month]=$a->product_month; 
                    }
                }
                foreach($allMonths as $m) {
                    $count[$m]=$this->giveProductLicense([
                        ['product_licenses.product_month',$m],
                        ['product_licenses.product_year',date('Y')],
                        ['product_instances.product_category_id',2]
                    ],$whereIN)->count();
                }
                $monthsToHide=[]; 
                foreach ($count as $key => $value) { 
                    if($value>5) {
                        array_push($monthsToHide,$key);
                    } 
                }
                $uniqueMonthsToHide=array_unique($monthsToHide);
                $products=Product::where('product_category_id',2)
                                    ->whereNotIn('product_month',$uniqueMonthsToHide)
                                    ->where('product_month','>=',date('n'))
                                    ->where('product_year',date('Y'))
                                    ->get()->toArray();
                
                 /*NEXT YEAR*/
                $allSpotSoldNextYear=$this->giveProductLicense([
                    ['product_licenses.product_year',$nextYear],
                    ['product_instances.product_category_id',2]
                ],$whereIN);
                foreach($allSpotSoldNextYear as $a) { 
                    if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                        array_push($monthSoldNextYear,$a->product_month); 
                        $allMonthsNextYear[$a->product_month]=$a->product_month; 
                    }
                }
                foreach($allMonthsNextYear as $m) {
                    $count[$m]=$this->giveProductLicense([
                        ['product_licenses.product_month',$m],
                        ['product_licenses.product_year',$nextYear],
                        ['product_instances.product_category_id',2]
                    ],$whereIN)->count();
                }
                foreach ($count as $key => $value) { 
                    if($value >10) {
                        array_push($monthsToHideNextYear,$key);
                    } 
                }
                $uniqueMonthsToHideNextYear=array_unique($monthsToHideNextYear);
                $productsNextYear=Product::where('product_category_id',2)
                                    ->whereNotIn('product_month',$uniqueMonthsToHideNextYear)
                                    ->where('product_year',$nextYear)
                                    ->get()->toArray();

                /* NEXT TO NEXT YEAR*/
                $allSpotSoldNextToNextYear=$this->giveProductLicense([
                    ['product_licenses.product_year',$nextToNextYear],
                    ['product_instances.product_category_id',2]
                ],$whereIN);
                foreach($allSpotSoldNextToNextYear as $a) { 
                    if(!is_null($a->product_month) && !is_null($a->product_year)  ) { 
                        array_push($monthSoldNextToNextYear,$a->product_month); 
                        $allMonthsNextToNextYear[$a->product_month]=$a->product_month; 
                    }
                }
                foreach($allMonthsNextToNextYear as $m) {
                    $count[$m]=$this->giveProductLicense([
                        ['product_licenses.product_month',$m],
                        ['product_licenses.product_year',$nextToNextYear],
                        ['product_instances.product_category_id',2]
                    ],$whereIN)->count();
                }
                
                foreach ($count as $key => $value) { 
                    if($value>5) {
                        array_push($monthsToHideNextToNextYear,$key); 
                    } 
                }
                $uniqueMonthsToHideNextToNextYear=array_unique($monthsToHideNextToNextYear);
                $productsNextToNextYear=Product::where('product_category_id',2)
                                    ->whereNotIn('product_month',$uniqueMonthsToHideNextToNextYear)
                                    ->where('product_year',$nextToNextYear)
                                    ->get()->toArray();
                $products=array_merge($products, $productsNextYear,$productsNextToNextYear); 
    			if($products) { 
    				$response['success']=true;
    				$response['products']=$products;
    			} else {
    				$response['success']=false;
    			}
    		} else { 
    			$otherProducts=Product::whereProductCategoryId($r->id)->get()->toArray();
    			if($otherProducts) { 
    				$response['success']=true; 
    				$response['products']=$otherProducts;
    			} else {
    				$response['success']=false;
    			}
    		}
    		echo json_encode($response);
    	}
    }  
    public function myPromotions(){ 
        return view('products.my_products');
    }
    public function getAllProductLicenses() {
        return view('product_licenses.licenses');
    }
    public function getAllTips(){ 
        return view('product_licenses.tip_license');
    } 
    public function getMyTips(){
        return view('product_licenses.my_tips');
    }
    public function getMySpotlights(){
        return view('product_licenses.my_spotlights');   
    }
    public function getMyLastMinutes(){
        return view('product_licenses.my_last_minutes');
    }
    public function getMyNews(){
        return view('product_licenses.my_news');
    }
    public function getMyPromotions() {
      $licenses=$this->giveProductLicense([['product_licenses.user_id',Auth::user()->id]]);
      return Datatables::of($licenses) 
        ->addColumn('action', function ($l) {   
          $str='';
          if($l->product_category_id==3) {
              if(!$l->activated) {
                  $str='<a href="/promotion/add-news/'.$l->id.'"class="btn btn-xs btn-warning">'.__('Add News').'</a>'; 
              } else {
                  $str='<a href="/promotion/edit-news/'.$l->id.'" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';    
              } 
          } else if($l->product_category_id==5) { 
              if(!$l->activated) {
                  $str='<a href="/promotion/add-lastminute/'.$l->id.'"class="btn btn-xs btn-success">'.__('Add Lastminute').'</a>';  
              } else {
                  $str='<a href="/promotion/edit-lastminute/'.$l->id.'" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';    
              } 
          } 
          $str.='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
          return $str;    
      })  
        ->editColumn('payment_complete', function ($payment) {   
            if($payment->payment_complete==1) {
                return "<span class='label label-success'>".__('COMPLETED')."</span>";
            } else if($payment->payment_complete==2) {
                return "<span class='label label-danger'>".__('CANCELED')."</span>";
            } else if($payment->payment_complete==3) {
                return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
            } else if($payment->payment_complete==0) {
                return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
            }
        }) 
        ->editColumn('vFrom',function($date) {
            if(is_null($date->vFrom))  {
                if($date->product_category_id==3 || $date->product_category_id==5 ) {
                    return "<span class='label label-warning'>".__('4 Weeks')."</span>";
                }
            } else {
                return date('d-m-Y',strtotime($date->vFrom));
            }
        })
        ->editColumn('vUpto',function($date) {
                  if($date->product_category_id==3) {
                      if(is_null($date->vUpto))  {
                          return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                      } else {
                          if(strtotime($date->vUpto)>strtotime('now')) {
                               if(strtotime($date->vFrom)<=strtotime('now')) {
                                   if($date->activated) {
                                        if(in_array($date->payment_complete, [1,3])) {
                                            return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                        } else {
                                            return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                        }
                                   }       
                               } else {
                                   return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                               }
                              
                          } else {
                              return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                          }
                      }
                  } else if($date->product_category_id==5) {
                      if(is_null($date->vUpto))  {
                          return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                      } else {
                         if(strtotime($date->vUpto)>strtotime('now')) {
                              if(strtotime($date->vFrom)<strtotime('now')) {
                                  if($date->activated) {
                                       if(in_array($date->payment_complete, [1,3])) {
                                           return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                       } else {
                                           return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                       }
                                  }       
                              } else {
                                  return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                              }
                             
                         } else {
                             return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                         }
                      }
                  } else {
                      $endMonth=date('m',strtotime($date->vUpto));
                      $endYear=date('Y',strtotime($date->vUpto));
                      if(strtotime('now')>strtotime($date->vUpto)) {
                          return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>";
                      }
                      if(in_array($date->payment_complete, [1,3])) {
                          if(date('m')==$endMonth && date('Y')==$endYear) {
                              return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                          }  
                      } 
                      if(strtotime($date->vUpto)>strtotime('now')) {
                          return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-warning'>".__('IN-ACTIVE')."</span>";
                      }
                  }
        })
        ->editColumn('pName',function($name) {
                return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
        }) 
        ->escapeColumns([])
        ->make(true); 
    } 
    public function getMyAllPromotions(Request $r){
        if($r->type_id) {
          $licenses=$this->giveProductLicense([
            ['product_licenses.user_id',Auth::user()->id], 
            ['products.product_category_id',$r->type_id]
          ]);
            return Datatables::of($licenses) 
            ->addColumn('action', function ($l) {  
                    $str='';
                    if($l->product_category_id==3) {
                        if(!$l->activated) {
                            $str='<a href="/promotion/add-news/'.$l->id.'"class="btn btn-xs btn-warning">'.__('Add News').'</a>'; 
                        } else {
                            $str='<a href="/promotion/edit-news/'.$l->id.'" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';    
                        } 
                    } else if($l->product_category_id==5) {
                        if(!$l->activated) {
                            $str='<a href="/promotion/add-lastminute/'.$l->id.'"class="btn btn-xs btn-success">'.__('Add Lastminute').'</a>';  
                        } else {
                            $str='<a href="/promotion/edit-lastminute/'.$l->id.'" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';    
                        } 
                    } 
                   $str.='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                  return $str;    
            })  
            ->editColumn('payment_complete', function ($payment) {   
                  if($payment->payment_complete==1) {
                      return "<span class='label label-success'>".__('COMPLETED')."</span>";
                  } else if($payment->payment_complete==2) {
                      return "<span class='label label-danger'>".__('CANCELED')."</span>";
                  } else if($payment->payment_complete==3) {
                      return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                  } else if($payment->payment_complete==0) {
                      return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                  }
            }) 
            ->editColumn('vFrom',function($date) {
                 if(is_null($date->vFrom))  {
                     if($date->product_category_id==3 || $date->product_category_id==5 ) {
                         return "<span class='label label-warning'>".__('4 Weeks')."</span>";
                     }
                 } else {
                     return date('d-m-Y',strtotime($date->vFrom));
                 }
            })
            ->editColumn('vUpto',function($date) {
                 if($date->product_category_id==3) {
                     if(is_null($date->vUpto))  {
                         return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                     } else {
                         if(strtotime($date->vUpto)>strtotime('now')) {
                              if(strtotime($date->vFrom)<=strtotime('now')) {
                                  if($date->activated) {
                                       if(in_array($date->payment_complete, [1,3])) {
                                           return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                       } else {
                                           return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                       }
                                  }       
                              } else {
                                  return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                              }
                             
                         } else {
                             return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                         }
                     }
                 } else if($date->product_category_id==5) {
                     if(is_null($date->vUpto))  {
                         return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                     } else { 
                        if(strtotime($date->vUpto)>strtotime('now')) {
                             if(strtotime($date->vFrom)<strtotime('now')) {
                                 if($date->activated) {
                                      if(in_array($date->payment_complete, [1,3])) {
                                          return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                      } else {
                                          return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                      }
                                 }        
                             } else {
                                 return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                             }
                            
                        } else {
                            return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                        }
                     }
                 } else {
                     $endMonth=date('m',strtotime($date->vUpto));
                     $endYear=date('Y',strtotime($date->vUpto));
                     if(strtotime('now')>strtotime($date->vUpto)) {
                         return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>";
                     }
                     if(in_array($date->payment_complete, [1,3])) {
                         if(date('m')==$endMonth && date('Y')==$endYear) {
                             return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                         }  
                     } 
                     if(strtotime($date->vUpto)>strtotime('now')) {
                         return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-warning'>".__('IN-ACTIVE')."</span>";
                     }
                 }
            }) 
            ->editColumn('pName',function($name) {
                    return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
            }) 
            ->escapeColumns([])
            ->make(true);
        }
    }  
    public function checkOut(Request $r) { 
      if(isset($r->pro)) {
        $pros=explode(',',$r->pro[0]);
        if($pros) {
          $products=Product::select('id','product_name','product_month','product_year','product_description','product_price','product_category_id')->whereIn('id',$pros)->get();
          if(!$products->isEmpty()) { 
            $accoms=Accomodation::whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id');  
            return view('product_licenses.checkout',compact('products','accoms'));  
          }
        } 
      } 
      return redirect('/promotions/buy')->with('error',__('Please select promotions')); 
    }
    public function payPalSuccess(Request $r){ 
        if($r->accom_id) {
            if($r->productIds) {
                $realAmount=DB::table('products')->whereIn('id',explode(',',$r->productIds))->sum('product_price');
                if($realAmount!=$r->amount_paid) {
                    return redirect('/promotions/buy')->with('error',__('Sorry,amount paid does not match with product amount'));
                }
                $proInvoiceKey=env('SITE_PRODUCT_INVOICE_KEY').uniqid();
                $paymentId=DB::table('product_payments')->insertGetId([
                        'transaction_id'=>$r->txn_id,
                        'transaction_json'=>json_encode($r->transaction_json),
                        'user_id'=>Auth::user()->id,
                        'payment_complete'=>1,
                        'total_amount'=>$r->amount_paid,
                        'pay_method_id'=>3,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'invoice_key'=>$proInvoiceKey 
                ]);
                if($paymentId) {
                    \Log::log('ProductPayment','new PayPal product payment created for userId '.Auth::user()->id,json_encode($r->transaction_json));
                    $proInvoiceNumber=env('SITE_PRODUCT_INVOICE_KEY').$paymentId;
                    DB::table('product_payments')->whereId($paymentId)->update(['invoice_number'=>$proInvoiceNumber]);
                    $productMonths=explode(',', $r->productMonths);
                    $productYears=explode(',', $r->productYears);
                    $counter=0;
                    foreach(explode(',',$r->productIds) as $pro) {
                        $proPrice=Product::select('product_price')->whereId($pro)->first();
                        $instanceId=DB::table('product_instances')->insertGetId([
                            'product_id'=>$pro,
                            'product_category_id'=>$r->productCategory,
                            'accom_id'=>$r->accom_id,
                            'product_payment_id'=>$paymentId,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'product_price'=>$proPrice->product_price  
                        ]); 
                        $startFrom='01-'.$productMonths[$counter].'-'.$productYears[$counter];
                        $validUpto=date('Y-m-t', strtotime($startFrom));
                        $startFrom=date('Y-m-d',strtotime($startFrom)).' 00:00:00';
                        $validUpto=date('Y-m-d',strtotime($validUpto)).' 23:59:59';
                        if($r->productCategory==3 || $r->productCategory==5) {
                           $startFrom=$validUpto=null;
                        }
                        $licenseId=DB::table('product_licenses')->insertGetId([
                            'product_payment_id'=>$paymentId,
                            'product_instance_id'=>$instanceId,
                            'valid_from'=>$startFrom,
                            'valid_upto'=>$validUpto,
                            'created_at'=>date('Y-m-d H:i:s'),  
                            'updated_at'=>date('Y-m-d H:i:s'), 
                            'accom_id'=>$r->accom_id,
                            'user_id'=>Auth::user()->id,
                            'product_month'=>$productMonths[$counter],
                            'product_year'=>$productYears[$counter]
                        ]);
                        \Log::log('ProductLicense','new PayPal product license created for userId '.Auth::user()->id); 
                        $counter++;     
                    }
                    Notifications::notifyAdmin([ 
                            'accom_id'=>$r->accom_id,
                            'from_user'=>Auth::user()->id,
                            'message'=>'A promotion paypal payment has been received for property '.$this->propertyName($r->accom_id),
                            'request_type_url'=>'/promotion-licenses/license' 
                    ]);   
                   /* $admins=User::select('email')->where('role_id',1)->get();
                    $this->notifyAdminsForProductPurchase($admins,
                            'New Promotion Payment Received',
                            'New promotion payment by paypal has been received for accomodation '.$this->propertyName($r->accom_id)
                    ); */
                    Notifications::notifyUser([  
                        'accom_id'=>$r->accom_id,
                        'message'=>'Your payment by paypal has been received for invoice '.$proInvoiceNumber,  
                        'to_user'=>Auth::user()->id,  
                        'request_type_url'=>'/promotions/my-promotions'
                    ]); 
                    $this->sendProductInvoice(
                        Auth::user()->email,
                        $proInvoiceKey,
                        $proInvoiceNumber,
                        $r->accom_id,
                        'Thanks for buying promotion your payment by paypal has been received successfully.please find enclosed invoice for your payment'
                    );  
                    return redirect('/promotions/my-promotions')->with('success',__('Thanks for buying promotion,your payment has been completed successfully'));  
                } else {
                    return redirect('/promotions/my-promotions')->with('error',__('Internal error,payment not saved'));
                }
            } else {
                return redirect('/promotions/my-promotions')->with('error',__('Internal error,promotions not found'));
            } 
        }
    }
    public function mollieProduct(Request $r){
        if($r->accom_id) {
            if($r->productIds) {
                $productsIds=explode(',',$r->productIds);
                $realAmount=DB::table('products')->whereIn('id',$productsIds)->sum('product_price');
                if($realAmount!=$r->amount_paid) {
                    return redirect('/promotions/buy')->with('error',__('Sorry,amount paid does not match with product amount'));
                }
                $payment = Mollie::api()->payments()->create([ 
                    "amount"      => $r->amount_paid,
                    "description" => __('Promotion Payment'),
                    "redirectUrl" => $r->getSchemeAndHttpHost()."/promotion-licenses/mollie-return/".$r->accom_id,
                    "webhookUrl"  => $r->getSchemeAndHttpHost()."/promotion-licenses/mollie-webhook/".$r->accom_id  
                    // "redirectUrl" => "https://webshop.example.org/order/12345/",
                ]);
                DB::table('mollie_product_payments')->insertGetId([
                    'order_id'=>$payment->id, 
                    'accom_id'=>$r->accom_id,
                    'product_ids'=>$r->productIds, 
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                \Log::log('mollie_product_payments','mollie product payment created for userId '.Auth::user()->id,json_encode($payment));
                return redirect($payment->links->paymentUrl); 
            } else { 
                return redirect()->with('error',__('Please select promotions to buy'));
            }
        } else {
            return redirect()->back()->with('error',__('Internal error,no accomodation selected'));
        }
    }  
    public function mollieProductReturn(Request $r,$accom){
        //dd($r);
        $moliePayment=DB::table('mollie_product_payments')->where('accom_id',$accom)->orderBy('id','desc')->first();
        //dump($moliePayment);
        $payment = Mollie::api()->payments()->get($moliePayment->order_id);
        //dd($payment);
        if ($payment->isPaid())  {
            $proInvoiceKey=env('SITE_PRODUCT_INVOICE_KEY').uniqid();
            $paymentId=DB::table('product_payments')->insertGetId([
                    'transaction_id'=>$moliePayment->order_id,
                    'transaction_json'=>json_encode($payment),
                    'user_id'=>Auth::user()->id,
                    'payment_complete'=>1,
                    'total_amount'=>$payment->amount,
                    'pay_method_id'=>6,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'invoice_key'=>$proInvoiceKey
            ]);
            if($paymentId) {
                \Log::log('ProductPayment','new Mollie product payment created for userId '.Auth::user()->id,json_encode($payment));
                DB::table('mollie_product_payments')->where('order_id',$payment->id)->update(['status'=>1,'remarks'=>json_encode($payment),'method'=>$payment->method]);
                $proInvoiceNumber=env('SITE_PRODUCT_INVOICE_KEY').$paymentId;
                DB::table('product_payments')->whereId($paymentId)->update(['invoice_number'=>$proInvoiceNumber]);
                $boughtProducts=explode(',',$moliePayment->product_ids);
                foreach($boughtProducts as $pro) {
                    $proProduct=Product::whereId($pro)->first();
                    $instanceId=DB::table('product_instances')->insertGetId([
                        'product_id'=>$pro,
                        'product_category_id'=>$proProduct->product_category_id,
                        'accom_id'=>$moliePayment->accom_id,
                        'product_payment_id'=>$paymentId,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'product_price'=>$proProduct->product_price   
                    ]);
                    if($proProduct->product_category_id==3 || $proProduct->product_category_id==5) {
                       $startFrom=$validUpto=null;
                    } else{
                      $startFrom='01-'.$proProduct->product_month.'-'.$proProduct->product_year;
                      $validUpto=date('Y-m-t', strtotime($startFrom));
                      $startFrom=date('Y-m-d',strtotime($startFrom)).' 00:00:00';
                      $validUpto=date('Y-m-d',strtotime($validUpto)).' 23:59:59';
                    }
                    $licenseId=DB::table('product_licenses')->insertGetId([
                        'product_payment_id'=>$paymentId,
                        'product_instance_id'=>$instanceId,
                        'valid_from'=>$startFrom,
                        'valid_upto'=>$validUpto,
                        'created_at'=>date('Y-m-d H:i:s'),  
                        'updated_at'=>date('Y-m-d H:i:s'), 
                        'accom_id'=>$moliePayment->accom_id,
                        'user_id'=>Auth::user()->id,
                        'product_month'=>$proProduct->product_month,
                        'product_year'=>$proProduct->product_year
                    ]);  
                    \Log::log('ProductLicenseCreated','new Mollie product license created for userId '.Auth::user()->id); 
                }
                Notifications::notifyAdmin([ 
                        'accom_id'=>$moliePayment->accom_id,
                        'from_user'=>Auth::user()->id,
                        'message'=>'New promotion payment by mollie has been received for accomodation '.$this->propertyName($moliePayment->accom_id),
                        'request_type_url'=>'/promotion-licenses/license' 
                ]);   
                $admins=User::select('email')->where('role_id',1)->get();
                $this->notifyAdminsForProductPurchase($admins,
                        'New Promotion Payment Received',
                        'New promotion payment by mollie has been received for accomodation '.$this->propertyName($moliePayment->accom_id)
                ); 
                Notifications::notifyUser([  
                    'accom_id'=>$moliePayment->accom_id,
                    'message'=>'Your payment by mollie has been received for invoice '.$proInvoiceNumber,  
                    'to_user'=>Auth::user()->id,  
                    'request_type_url'=>'/promotions/my-promotions'
                ]); 
                $this->sendProductInvoice(
                    Auth::user()->email,
                    $proInvoiceKey,
                    $proInvoiceNumber,
                    $moliePayment->accom_id,
                    'Thanks for buying promotion your payment by mollie has been received successfully.please find enclosed invoice for your payment'
                );  
                return redirect('/promotions/my-promotions')->with('success',__('Thanks for buying promotion,your payment has been completed successfully'));  
            } else {
                return redirect('/promotions/my-promotions')->with('error',__('Internal error,payment not saved'));
            }  
        } else if ($payment->isOpen()) {
            \Log::log('MollieProductPaymentFailed','error for paymentId '.$payment->id,json_encode($payment)); 
            DB::table('mollie_product_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
            return redirect('/promotions/my-promotions')->with('error',__('Your payment has been marked open'));
        } elseif ($payment->status=='cancelled') {
            DB::table('mollie_product_payments')->where('order_id',$payment->id)->delete();
            return redirect('/promotions/buy')->with('error',__('Your payment request cancelled'));
        }  
    }  
    public function mollieProductWebhook(Request $r,$accom) { 
        $moliePayment=DB::table('mollie_product_payments')->where('accom_id',$accom)->orderBy('id','desc')->first();
        $payment    = Mollie::api()->payments()->get($moliePayment->order_id);
        \Log::log('MollieProductPaymentResponseWebHook','mollie product payment response received for user '.Auth::user()->id,json_encode($payment));
         
        if ($payment->isPaid())  {
            /*
             * At this point you'd probably want to start the process of delivering the product
             * to the customer.
             */
                \Log::log('MolliePaymentSucessWebHook','success for paymentId '.$payment->id,json_encode($payment));
                DB::table('mollie_product_payments')->where('order_id',$payment->id)->update(['status'=>1,'remarks'=>json_encode($payment),'method'=>$payment->method]);
        } elseif (! $payment->isOpen()) {
            /*
             * The payment isn't paid and isn't open anymore. We can assume it was aborted.
             */
            \Log::log('MollieProductPaymentFailedWebHook','error for paymentId '.$payment->id,json_encode($payment));  
            DB::table('mollie_product_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
        }
    }
    public function bankTransferPro(Request $r){        
        if($r->accom_id) {
            if($r->productIds) {
                $realAmount=DB::table('products')->whereIn('id',explode(',',$r->productIds))->sum('product_price');
                if($r->amount_paid!=$realAmount) {
                    return redirect()->back()->with('eror',__('Internal Error,amount paid does not match with promotion amount'));
                }
                $proInvoiceKey=env('SITE_PRODUCT_INVOICE_KEY').uniqid();
                $paymentId=DB::table('product_payments')->insertGetId([
                        'user_id'=>Auth::user()->id,
                        'payment_complete'=>0,
                        'total_amount'=>$r->amount_paid,
                        'pay_method_id'=>2,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'invoice_key'=>$proInvoiceKey
                ]);
                if($paymentId) {
                    \Log::log('ProductPayment','new bank transfer product payment created for userId '.Auth::user()->id);
                    $proInvoiceNumber=env('SITE_PRODUCT_INVOICE_KEY').$paymentId;
                    DB::table('product_payments')->whereId($paymentId)->update(['invoice_number'=>$proInvoiceNumber]);
                    $productMonths=explode(',', $r->productMonths);
                    $productYears=explode(',', $r->productYears);
                    $counter=0;
                    foreach(explode(',',$r->productIds) as $pro) {
                        $proPrice=Product::select('product_price')->whereId($pro)->first();
                        $instanceId=DB::table('product_instances')->insertGetId([
                            'product_id'=>$pro,
                            'product_category_id'=>$r->productCategory,
                            'accom_id'=>$r->accom_id,
                            'product_payment_id'=>$paymentId,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'product_price'=>$proPrice->product_price 
                        ]); 
                        $startFrom='01-'.$productMonths[$counter].'-'.$productYears[$counter];
                        $validUpto=date('Y-m-t', strtotime($startFrom));
                        $startFrom=date('Y-m-d',strtotime($startFrom)).' 00:00:00';
                        $validUpto=date('Y-m-d',strtotime($validUpto)).' 23:59:59';
                        if($r->productCategory==3 || $r->productCategory==5) {
                           $startFrom=$validUpto=null;
                        } 
                        $licenseId=DB::table('product_licenses')->insertGetId([
                            'product_payment_id'=>$paymentId,
                            'product_instance_id'=>$instanceId,
                            'valid_from'=>$startFrom,
                            'valid_upto'=>$validUpto,
                            'created_at'=>date('Y-m-d H:i:s'),  
                            'updated_at'=>date('Y-m-d H:i:s'), 
                            'accom_id'=>$r->accom_id,
                            'user_id'=>Auth::user()->id,
                            'product_month'=>$productMonths[$counter],
                            'product_year'=>$productYears[$counter]
                        ]);
                        \Log::log('ProductLicense','new bank transfer product license created for userId '.Auth::user()->id); 
                        $counter++;     
                    }
                    Notifications::notifyAdmin([ 
                            'accom_id'=>$r->accom_id,
                            'from_user'=>Auth::user()->id,
                            'message'=>'A promotion bank-transfer payment has been received for property '.$this->propertyName($r->accom_id),
                            'request_type_url'=>'/promotion-licenses/license' 
                    ]);   
                   /* $admins=User::select('email')->where('role_id',1)->get();
                    $this->notifyAdminsForProductPurchase($admins,
                            'New Promotion Payment Received',
                            'New promotion payment request by bank transfer has been received for accomodation '.$this->propertyName($r->accom_id)
                    ); */
                    
                    Notifications::notifyUser([  
                        'accom_id'=>$r->accom_id,
                        'message'=>'Your payment request has been sent and wating admin approval for invoice '.$proInvoiceNumber,  
                        'to_user'=>Auth::user()->id,  
                        'request_type_url'=>'/promotions/my-promotions'
                    ]); 
                    $this->sendProductInvoice(
                        Auth::user()->email,
                        $proInvoiceKey,
                        $proInvoiceNumber,
                        $r->accom_id,
                        'Thanks for buying promotion your payment request has been sent successfully,wating for admin approval.please find enclosed invoice for your payment'
                    );  
                    return redirect('/promotions/my-promotions')->with('success',__('Thanks for buying promotion,your request submitted successfully,wating admin approval'));  
                } else {
                    return redirect('/promotions/my-promotions')->with('error',__('Internal error,payment not saved'));
                }
            } else {
                return redirect()->back()->with('error',__('Internal error,No promotion selected,please select promotion'));
            }     
        } else {
            return redirect()->back()->with('error',__('Internal error, No accomodation selected')); 
        }
    } 
    public function getAllProductLicensesAjax(Request $r){
        if($r->type_id) {
            // type_id 1=active,2=inactive,3=expired,4=cancelled,5=new transfer
            $licenses=$this->giveProductLicense([
              ['products.product_category_id',$r->type_id]
            ]); 
            
              return Datatables::of($licenses) 
              ->addColumn('action', function ($l) {   
                   $str='<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                   if(!$l->payment_complete) {
                        $str .='&nbsp;<a  href="/promotion-licenses/mark-pay/'.$l->id.'"  class="btn btn-xs btn-success"  title="Mark Paid"><i class="fa fa-thumbs-o-up"></i> </a>'; 
                        $str .='&nbsp;<a href="/promotion-licenses/mark-cancel/'.$l->id.' "class="btn btn-xs btn-danger"  title="Cancel"><i class="fa fa-thumbs-o-down"></i> </a>';  
                   }
                  return $str;    
              })  
              ->editColumn('payment_complete', function ($payment) {   
                  if($payment->payment_complete==1) {
                      return "<span class='label label-success'>".__('COMPLETED')."</span>";
                  } else if($payment->payment_complete==2) {
                      return "<span class='label label-danger'>".__('CANCELED')."</span>";
                  } else if($payment->payment_complete==3) {
                      return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                  } else if($payment->payment_complete==0) {
                      return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                  }
              }) 
              ->editColumn('pName',function($name) {
                  return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
              })
            ->editColumn('vFrom',function($date) {
                 if(is_null($date->vFrom))  {
                     if($date->product_category_id==3 || $date->product_category_id==5 ) {
                         return "<span class='label label-warning'>".__('4 Weeks')."</span>";
                     }
                 } else {
                     return date('d-m-Y',strtotime($date->vFrom));
                 }
            })
            ->editColumn('vUpto',function($date) {
                 if($date->product_category_id==3) {
                     if(is_null($date->vUpto))  {
                         return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                     } else {
                         if(strtotime($date->vUpto)>strtotime('now')) {
                              if(strtotime($date->vFrom)<=strtotime('now')) {
                                  if($date->activated) {
                                       if(in_array($date->payment_complete, [1,3])) {
                                           return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                       } else {
                                           return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                       }
                                  }       
                              } else {
                                  return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                              }
                             
                         } else {
                             return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                         }
                     }
                 } else if($date->product_category_id==5) {
                     if(is_null($date->vUpto))  {
                         return "</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";
                     } else {
                        if(strtotime($date->vUpto)>strtotime('now')) {
                             if(strtotime($date->vFrom)<=strtotime('now')) {
                                 if($date->activated) {
                                      if(in_array($date->payment_complete, [1,3])) {
                                          return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                                      } else {
                                          return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-danger'>".__('IN-ACTIVE')."</span>";               
                                      }
                                 }       
                             } else {
                                 return date('d-m-Y',strtotime($date->vUpto))."</span>&nbsp;<span class='label label-warning'>".__('IN-ACTIVE')."</span>";     
                             }
                            
                        } else {
                            return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>"; 
                        }
                     }
                 } else {
                     $endMonth=date('m',strtotime($date->vUpto));
                     $endYear=date('Y',strtotime($date->vUpto));
                     if(strtotime('now')>strtotime($date->vUpto)) {
                         return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>";
                     }
                     if(in_array($date->payment_complete, [1,3])) {
                         if(date('m')==$endMonth && date('Y')==$endYear) {
                             return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                         }  
                     } 
                     if(strtotime($date->vUpto)>strtotime('now')) {
                         return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-warning'>".__('IN-ACTIVE')."</span>";
                     }
                 }
            })
              ->editColumn('uFName',function($name) {
                  return $name->uFName.' '.$name->uLName;
              })
              ->escapeColumns([])
              ->make(true);
        }
    }
    public function getAllProductLicensesCatWiseAjax(Request $r){
        if($r->type_id) {
            // type_id 1=active,2=inactive,3=expired,4=cancelled,5=new transfer
            if($r->type_id==1) { 
                $licenses=$this->giveProductLicense([
                  ['products.product_category_id',$r->type_id],
                  ['product_licenses.product_month','=',date('n')],
                  //['product_licenses.product_year',date('Y')]
                ]);  
            } else if($r->type_id==2) {
                $licenses=$this->giveProductLicense([
                  ['products.product_category_id',$r->type_id],
                  ['product_licenses.product_month','>',date('n')],
                  ['product_licenses.product_year','>=',date('Y')]
                ]); 
            } else if($r->type_id==3) {
                $licenses=$this->giveProductLicense([
                  ['products.product_category_id',$r->type_id],
                  ['product_licenses.product_month','<',date('m')],
                  ['product_licenses.product_year','<=',date('Y')]
                ]); 
            } else if($r->type_id==4) {
                $licenses=$this->giveProductLicense([
                  ['products.product_category_id',$r->type_id],
                  ['product_payments.payment_complete',2],
                  ['product_licenses.product_year',date('Y')]
                ]); 
            } else if($r->type_id==5) {
                $licenses=$this->giveProductLicense([
                  ['products.product_category_id',$r->type_id],
                  ['product_payments.payment_complete',0]
                ]); 
            }
              return Datatables::of($licenses) 
              ->addColumn('action', function ($l) {   
                   $str='<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                  return $str;    
              })  
              ->editColumn('payment_complete', function ($payment) {   
                  if($payment->payment_complete==1) {
                      return "<span class='label label-success'>".__('COMPLETED')."</span>";
                  } else if($payment->payment_complete==2) {
                      return "<span class='label label-danger'>".__('CANCELED')."</span>";
                  } else if($payment->payment_complete==3) {
                      return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                  } else if($payment->payment_complete==0) {
                      return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                  }
              }) 
              /*->editColumn('vUpto',function($date) {
                  $endMonth=date('m',strtotime($date->vUpto));
                  $endYear=date('Y',strtotime($date->vUpto));
                  if(strtotime('now')>strtotime($date->vUpto)) {
                      return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-danger'>".__('EXPIRED')."</span>";
                  } 
                  if(date('m')==$endMonth && date('Y')==$endYear) {
                      return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-success'>".__('ACTIVE')."</span>";
                  } 
                  if(strtotime($date->vUpto)>strtotime('now')) {
                      return date('d-m-Y',strtotime($date->vUpto))." <span class='label label-warning'>".__('IN-ACTIVE')."</span>";;
                  } 
              })*/
              ->editColumn('vFrom',function($date) {
                  return date('d-m-Y',strtotime($date->vFrom));
              })
              ->editColumn('vUpto',function($date) {
                  return date('d-m-Y',strtotime($date->vUpto));
              })
              ->editColumn('uFName',function($name) {
                  return $name->uFName.' '.$name->uLName;
              })
              ->escapeColumns([])
              ->make(true);
        }
    } 
    public function markPayPro($id) { 
        if($id) {
            $license=$this->giveProductLicense([['product_licenses.id',$id]]);
            if($license) { 
                $license=$license[0];
                $payment=ProductPayment::whereId($license->paymentId)->first();
                if($payment) {
                   $payment->payment_complete=1;
                    if($payment->save()) {
                        Notifications::notifyUser([  
                            'accom_id'=>$license->accomId,
                            'message'=>'Your bank transfer payment has been approved for Invoice: '.$license->invoice,  
                            'to_user'=>$license->user_id,  
                            'request_type_url'=>'/promotions/my-promotions'
                        ]);
                        $advertiser=User::select('email','first_name','last_name')->whereId($license->user_id)->first();
                        $this->sendProductInvoiceAdmin(
                            $advertiser,
                            $license->invoice_key,
                            $license->invoice,
                            $license->accomId,
                            'Thanks for buying promotion your payment request by bank transfer has been approved.please find enclosed invoice for your payment',
                            'Thanks for buying promotion'
                        );  
                        return redirect()->back()->with('success',__('Payment has been approved successfully'));
                    }
                } else {
                    return redirect()->back()->with('error',__('Sorry,no payment found'));
                }   
            } else {
                return redirect()->back()->with('error',__('Internal error,no license found'));
            }
        }
    }
    public function markCancelPro($id) {
        if($id) {
            $license=$this->giveProductLicense([['product_licenses.id',$id]]);
            $license=$license[0];
            $payment=ProductPayment::where('id',$license->paymentId)->first();
            if($payment) {
                $oldAmount=$payment->total_amount;
                $payment->payment_complete=2;
                $payment->total_amount=00;
                $payment->cancelled_amount=$oldAmount; 
                if($payment->save()) {
                    $allInstaces=DB::table('product_instances')->where('product_payment_id',$license->paymentId)->get();
                    foreach($allInstaces as $a) {
                        $oldAmt=$a->product_price;
                        DB::table('product_instances')->whereId($a->id)->update([
                            'product_price'=>00,
                            'cancelled_amount'=>$oldAmt
                        ]);
                    }
                    Notifications::notifyUser([  
                     'accom_id'=>$license->accomId,
                     'message'=>'Your bank transfer payment has been cancelled for Invoice: '.$license->invoice,  
                     'to_user'=>$license->user_id,  
                     'request_type_url'=>'/promotions/my-promotions'
                     ]);
                    $advertiser=User::select('email','first_name','last_name')->whereId($license->user_id)->first();
                    $this->sendProductInvoiceAdmin(
                         $advertiser,
                         $license->invoice_key,
                         $license->invoice,
                         $license->accomId,
                         'Your payment request by bank transfer has been cancelled.please find enclosed invoice for your payment',
                         'Your bank transfer payment has been cancelled'
                     );  
                    return redirect()->back()->with('success',__('Payment has been cancelled successfully')); 
                }
            }
        }
    } 
    public function giveFreeProductLicense() {  
        $accoms=Accomodation::select(DB::raw("CONCAT(name,'(',label_name,')') AS name"),'accomodations.id','labels.label_name' )
                            ->join('labels','labels.id','=','accomodations.label_id')->orderBy('label_id','desc')
                            ->pluck('name','id');
        $products=Product::select(DB::raw("CONCAT(product_name,'(',product_month,'-',product_year,')') AS name"),'products.id')
                            ->pluck('name','id');
        return view('product_licenses.give_free_license',compact('accoms','products')); 
    }
   public function postFreeProductLicense(Request $r) { 
        if($r->product_id) {
            if($r->accom_id) {
                $license=new FreeProductLicense();
                $license->product_id=$r->product_id;
                $license->accom_id=$r->accom_id;
                $license->user_id=$r->userId;
                $coupon=env('SITE_FREE_SUBTOKEN').uniqid();
                $license->token=$coupon;  
                if($license->save()) {
                     try {
                        $user=User::select('first_name','last_name')->whereId($r->userId)->first();
                        $accomName=$this->propertyName($r->accom_id);
                        $productName=$this->productName($r->product_id);
                        Mail::to($r->email)->send(new SendFreeProductCoupon($user,$coupon,$accomName,$productName));     
                     } catch(Exception $e) {
                         return redirect()->back()->with('error',__('Something went wrong while sending mail server returned error: ').$e->getMessage);
                     }
                     return redirect('/promotion-licenses/all-coupons')->with('success',__('Free promotion coupon sent successfully') );
                } 
            } else {
                return redirect()->back()->with('error',__('Sorry,invalid request,no accomodations found'));
            }
        } else {
           return redirect()->back()->with('error',__('Sorry,invalid request,no promotion found'));
        }
   }
   public function getProductCoupons() {
        return view('product_licenses.free_coupons'); 
    }
    public function getAllProductCouponsAjax() {
          $coupons=FreeProductLicense::select('free_product_licenses.token','free_product_licenses.created_at','free_product_licenses.is_closed',
                                      'products.product_name as pName','accomodations.name as aName',DB::raw("CONCAT(first_name,' ',last_name) as uName"))
                      ->join('products','products.id','=','free_product_licenses.product_id')
                      ->join('accomodations','accomodations.id','=','free_product_licenses.accom_id')
                      ->join('users','users.id','=','free_product_licenses.user_id')->get(); 
          return Datatables::of($coupons)  
          ->editColumn('is_closed',function($closed) {
              
              if($closed->is_closed) {
                  return "<span class='label label-success'>".__('Closed')."</span>";
              } else {
                  return "<span class='label label-danger'>".__('Open')."</span>";
              }
          }) 
          ->editColumn('created_at',function($date) {
              return date('d-m-Y',strtotime($date->created_at));
          }) 
         ->escapeColumns([])
         ->make(true);  
    } 
    public function useFreeProCoupon(Request $r){
        if($r->token) {
            if($r->accomId) { 
                $coupon=FreeProductLicense::whereToken($r->token)->first();
                if($coupon) {
                    if(!$coupon->is_closed) {
                        if($coupon->accom_id==$r->accomId) {

                            $proInvoiceKey=env('SITE_PRODUCT_INVOICE_KEY').uniqid();
                            $paymentId=DB::table('product_payments')->insertGetId([
                                    'user_id'=>Auth::user()->id,
                                    'payment_complete'=>3,
                                    'total_amount'=>00,
                                    'pay_method_id'=>5,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'invoice_key'=>$proInvoiceKey
                            ]);
                            if($paymentId) {
                                \Log::log('ProductPayment','new free coupon product payment created for userId '.Auth::user()->id);
                                $proInvoiceNumber=env('SITE_PRODUCT_INVOICE_KEY').$paymentId;
                                DB::table('product_payments')->whereId($paymentId)->update(['invoice_number'=>$proInvoiceNumber]);
                               /* $productMonths=explode(',', $r->productMonths);
                                $productYears=explode(',', $r->productYears);*/
                                $counter=0;
                                //foreach(explode(',',$r->productIds) as $pro) {
                                    $proDetail=Product::select('product_category_id','product_month','product_year')->whereId($coupon->product_id)->first();
                                    $instanceId=DB::table('product_instances')->insertGetId([
                                        'product_id'=>$coupon->product_id,
                                        'product_category_id'=>$proDetail->product_category_id,
                                        'accom_id'=>$coupon->accom_id,
                                        'product_payment_id'=>$paymentId,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'),
                                        'product_price'=>00 
                                    ]); 
                                    $startFrom='01-'.$proDetail->product_month.'-'.$proDetail->product_year;
                                    $validUpto=date('Y-m-t', strtotime($startFrom));
                                    $startFrom=date('Y-m-d',strtotime($startFrom)).' 00:00:00';
                                    $validUpto=date('Y-m-d',strtotime($validUpto)).' 23:59:59';
                                    $licenseId=DB::table('product_licenses')->insertGetId([
                                        'product_payment_id'=>$paymentId,
                                        'product_instance_id'=>$instanceId,
                                        'valid_from'=>$startFrom,
                                        'valid_upto'=>$validUpto,
                                        'created_at'=>date('Y-m-d H:i:s'),  
                                        'updated_at'=>date('Y-m-d H:i:s'), 
                                        'accom_id'=>$coupon->accom_id,
                                        'user_id'=>Auth::user()->id,
                                        'product_month'=>$proDetail->product_month, 
                                        'product_year'=>$proDetail->product_year
                                    ]);
                                    \Log::log('ProductLicense','new free product license created for userId '.Auth::user()->id); 
                                    $counter++;     
                                //}
                                Notifications::notifyAdmin([ 
                                        'accom_id'=>$coupon->accom_id,
                                        'from_user'=>Auth::user()->id,
                                        'message'=>'A free promotion coupon has been used for property '.$this->propertyName($coupon->accom_id).' with invoice '.$proInvoiceNumber,
                                        'request_type_url'=>'/promotion-licenses/license' 
                                ]);    
                                Notifications::notifyUser([  
                                    'accom_id'=>$coupon->accom_id,
                                    'message'=>'Your coupon has been successfully used for  invoice '.$proInvoiceNumber,  
                                    'to_user'=>Auth::user()->id,  
                                    'request_type_url'=>'/promotions/my-promotions'
                                ]); 
                                $coupon->is_closed=1;
                                $coupon->save();
                                $this->sendProductInvoice(
                                    Auth::user()->email,
                                    $proInvoiceKey,
                                    $proInvoiceNumber,
                                    $coupon->accom_id,
                                    'Your coupon has been used successfully,please find enclosed invoice for your payment'
                                );  
                                return redirect('/promotions/my-promotions')->with('success',__('Thanks for using promotion coupon'));  
                            } else {
                                return redirect('/promotions/my-promotions')->with('error',__('Internal error,payment not saved'));
                            }
                        } else {
                            return redirect('/promotions/buy')->with('error',__('This coupon is not valid for this accomodation'));    
                        }
                    } else {
                        return redirect('/promotions/buy')->with('error',__('Coupon closed'));
                    }
                }  else {
                    return redirect('/promotions/buy')->with('error',__('Invalid Coupon'));
                }
            } else  {
                return redirect('/promotions/buy')->with('error',__('Sorry,accomodation selected'));
            }   
        } else {
            return redirect('/promotions/buy')->with('error',__('Coupon is required')); 
        }   
    }
    public function sendProductInvoiceAdmin($user,$key,$invoice_number,$accom,$msg,$subject) {
        $file=$this->buildProductInvoice($key); 
        $file->save(public_path('files/pdf/products/invoice.pdf'));  
        try {
            Mail::to($user->email)->send(new SendProductPaymentInvoiceAdmin($user,$msg,$subject));   
            \Log::log('ProductLicenseInvoiceSent','New product license invoice sent to '.Auth::user()->email); 
            //unlink(public_path('files/pdf/products/invoice.pdf'));   
        } catch(Exception $e) { 
             \Log::log('ProductLicenseInvoiceNotSent','Product invoice not sent to user '.$user()->id.' for invoice ' .$invoice_number.' server returned error: '.$e->getMessage());   
             return redirect('/promotion-licenses/license')->with('error','Something went wrong while sending mail.Server returned error: '.$e->getMessage()); 
        } 
    }
    public function sendProductInvoice($user,$key,$invoice_number,$accom,$msg) {
        $file=$this->buildProductInvoice($key); 
        $file->save(public_path('files/pdf/products/invoice.pdf'));  
        try {
            Mail::to($user)->send(new SendProductPaymentInvoice(Auth::user(),$msg)); 
            \Log::log('ProductLicenseInvoiceSent','New product license invoice sent to '.Auth::user()->email); 
            unlink(public_path('files/pdf/products/invoice.pdf'));   
        } catch(Exception $e) { 
            Notifications::notifyAdmin([  
                    'accom_id'=>$accom,
                    'from_user'=>Auth::user()->id,
                    'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                    'request_type_url'=>'/subscription_licenses/licenses'
            ]);
             \Log::log('ProductLicenseInvoiceNotSent','Product invoice not sent to user '.Auth::user()->id.' for invoice ' .$invoice_number.' server returned error: '.$e->getMessage());   
             if(Auth::user()->role_id==1) {
                return redirect('/promotion-licenses/license')->with('error','Something went wrong while sending mail.Server returned error: '.$e->getMessage());
             } else {
                return redirect('/promotions/my-promotions')->with('error','Something went wrong while sending mail,incident has been reported to administrator.please ask administrator to send invoice if not received.Server returned error: '.$e->getMessage());
             }
        }     
    }
    public function propertyName($accom){
        $name=Accomodation::select('name')->whereId($accom)->first();
        return $name->name;
    }
    public function productName($product){
        $name=Product::select(DB::raw("CONCAT(product_name,'(',product_month,'-',product_year,')') AS name"))->whereId($product)->first();
        return $name->name;
    }
    public function notifyAdminsForProductPurchase($admins,$subject,$msg){
        if($admins) {
            foreach ($admins as $a) {
                try {
                    Mail::to($a->email)->send(new NotifyAdminsForProductPurchase($subject,$msg));     
                } catch(Exception $e) {
                    \Log::log('notifyAdminsForProductPurchase','Mail not sent to '.Auth::user()->id.' Server returned error: '.$e->getMessage()); 
                }
            }
        }
    }
    public function buildProductInvoice($key=null) {
        if($key) {
            $licenses=ProductLicense::where('product_payments.invoice_key', $key)
                    ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                    ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                    ->leftJoin('products','products.id','=','product_instances.product_id')
                    ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','product_licenses.user_id','=','users.id')
                    ->leftJoin('countries','users.country_id','=','countries.id')
                    ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id') 
                    ->select('product_licenses.id','product_licenses.user_id','product_licenses.valid_from','product_licenses.valid_upto',
                            'product_licenses.created_at as invoiceDate',
                            'product_payments.invoice_number','pay_methods.method_name as paymentName',
                            'product_payments.total_amount','product_payments.pay_method_id','product_payments.payment_complete',
                            'product_instances.product_id','products.product_name as pName','products.product_description as pDesc',
                            'products.product_price as pAmount','countries.name as countryName','product_instances.product_price as pPrice',
                            'users.first_name','users.last_name','users.city','users.email','users.user_company',
                            'users.country_id','users.phone_number','users.street','users.street_number','users.post_code','users.btw_number',
                            'mollie_product_payments.method'
                    )
                    ->get(); 
            $licenses->first()->invoiceDate=date('d-m-Y',strtotime($licenses->first()->invoiceDate));
            $file= PDF::loadView('product_licenses.pdf.invoice',compact('licenses'));
            return $file;
        } else {
            return false;
        }  
    }
    public function generateProInvoice($key=null){
        if($key) {
            $file=$this->buildProductInvoice($key);  
            return $file->stream();       
        }  
        return false; 
    }
    public function giveProductLicense($where=array(),$whereIn=array()) {
        if($where) {
            if($whereIn) {
                $licenses=ProductLicense::where($where)->whereIn('product_payments.payment_complete',$whereIn)
                    ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                    ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                    ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                    ->leftJoin('products','products.id','=','product_instances.product_id')
                    ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','product_licenses.user_id','=','users.id')
                    ->select('product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
                        'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
                        'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
                        'product_payments.total_amount as total_amount','product_instances.product_price as productPrice','product_instances.id as instanceId',
                        'pay_methods.method_name as pName','accomodations.name as aName','accomodations.id as accomId','users.first_name as uFName','users.last_name as uLName',
                        'products.product_name','products.product_description','products.product_price','products.product_category_id',
                        'mollie_product_payments.method'
                    )
                    ->orderBy('product_licenses.id','desc')
                    ->get(); 
            } else {
                $licenses=ProductLicense::where($where) 
                    ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                    ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                    ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                    ->leftJoin('products','products.id','=','product_instances.product_id')
                    ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','product_licenses.user_id','=','users.id')
                    ->select('product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
                        'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
                        'product_licenses.product_month','product_licenses.product_year','product_instances.product_price as productPrice','product_instances.id as instanceId',
                        'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
                        'product_payments.total_amount as total_amount',
                        'pay_methods.method_name as pName','accomodations.name as aName','accomodations.id as accomId','users.first_name as uFName','users.last_name as uLName',
                        'products.product_name','products.product_description','products.product_price','products.product_category_id',
                        'mollie_product_payments.method'
                    )
                    ->orderBy('product_licenses.id','desc')  
                    ->get(); 
            }
            return $licenses;
        } else {
            return false;
        }
    }
} 
