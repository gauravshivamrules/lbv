<?php

namespace LBV\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Share;
use Mail;
use LBV\Mail\NotifyAdminForMessage; 
use LBV\Model\Message;
use LBV\Model\MessageReply;
use LBV\Mail\NotifyUserForMessage;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{ 
	public function sendMessageNotification($args=array()){ 
		if($args){ 
			$insert=DB::table('message_notifications')->insertGetId([
				'accom_id'=>$args['accom_id'],
				'to_user'=>$args['to_user'],
				'from_user'=>$args['from_user'],
				'message_id'=>$args['message_id'], 
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s') 
			]);
			if($insert){ 
				return true;
			}
			return false;	 
		}
	}
	public function sendMessageNotificationAdmin($args=array()){
		$admins=DB::table('users')->select('email','id')->whereRoleId(1)->get();
		foreach($admins as $a){ 
			DB::table('message_notifications')->insert([
				'accom_id'=>$args['accom_id'],
				'to_user'=>$a->id,
				'from_user'=>$args['from_user'],
				'message_id'=>$args['message_id'], 
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s') 
			]);
		} 
	} 
	public function sendEmailMessageNotification($userEmail){
		if($userEmail){ 
			try { 
				Mail::to($userEmail)->send(new NotifyUserForMessage('You have a new message request','You have a new message request.Please login to your account to reply'));       
			} catch(Exception $e) {
				\Log::log('sendEmailMessageNotification','Mail not sent to '.$userEmail.' Server returned error: '.$e->getMessage()); 
			}
		}
	}
	public function notifyAdminsMessage($subject,$msg,$userMsg) {
		$admins=DB::table('users')->select('email')->where('role_id',1)->get();
		if($admins) {
			foreach ($admins as $a) {
				try {
					Mail::to($a->email)->send(new NotifyAdminForMessage($subject,$msg,$userMsg));      
				} catch(Exception $e) {
					\Log::log('notifyAdminsMessage','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage()); 
				}
			}
		}
	}
	public function contactAdvertiser(Request $r){
		if($r->id){
			$accomId=DB::table('messages')->select('accom_id','from_user')->whereId($r->id)->first(); 	
			if($accomId){
				$advertiser=DB::table('accomodations')->select('user_id','payment_type')->whereId($accomId->accom_id)->first();
				$userEmail=DB::table('users')->select('email')->whereId($accomId->from_user)->first();
				$approved=0;
				if($advertiser){
					if($advertiser->payment_type==1){
						$approved=1;
					}
				} 
				DB::table('message_replies')->insert([
						'message_id'=>$r->id,
						'reply'=>$r->text,
						'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name, 
						'sent_to'=>$accomId->from_user,  
						'sent_by'=>Auth::user()->id,
						'accom_id'=>$accomId->accom_id,
						'approved'=>$approved, 
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
				]);	 
				if(!$approved){
					$this->notifyAdminsMessage(
					        'New message has been received for free accomodation ',
					        'New message has been received for free accomodation '.$this->propertyName($accomId->accom_id),
					        $r->text
					);
					$this->sendMessageNotificationAdmin(['accom_id'=>$accomId->accom_id,'from_user'=>Auth::user()->id,'message_id'=>$r->id]); 
				}else{
					$this->sendMessageNotification(['accom_id'=>$accomId->accom_id,'to_user'=>$accomId->from_user,'from_user'=>Auth::user()->id,'message_id'=>$r->id]);
					$this->sendEmailMessageNotification($userEmail->email);   
				}
			}else{
				return redirect()->back()->with('error',__('Sorry,no accomodation found'));
			}
		}else{
			return redirect()->back()->with('error',__('Sorry,no id found'));
		}
	}
	public function contactCustomer(Request $r){
		if($r->id){
			$accomId=DB::table('messages')->select('accom_id','from_user')->whereId($r->id)->first(); 	
			if($accomId){
				$advertiser=DB::table('accomodations')->select('user_id','payment_type')->whereId($accomId->accom_id)->first();
				$userEmail=DB::table('users')->select('email')->whereId($accomId->from_user)->first();
				$approved=0;
				if($advertiser->payment_type==1){
					$approved=1;
				} 
				DB::table('message_replies')->insert([
						'message_id'=>$r->id,
						'reply'=>$r->text,
						'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name, 
						'sent_to'=>$advertiser->user_id,  
						'sent_by'=>Auth::user()->id,
						'accom_id'=>$accomId->accom_id,
						'approved'=>$approved, 
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
				]);	 
				if(!$approved){
					$this->notifyAdminsMessage(
					        'New message has been received for free accomodation ',
					        'New message has been received for free accomodation '.$this->propertyName($accomId->accom_id),
					        $r->text
					);
					$this->sendMessageNotificationAdmin(['accom_id'=>$accomId->accom_id,'from_user'=>Auth::user()->id,'message_id'=>$r->id]);
				} else{ 
					$this->sendMessageNotification(['accom_id'=>$accomId->accom_id,'to_user'=>$advertiser->user_id,'from_user'=>Auth::user()->id,'message_id'=>$r->id]);
					$this->sendEmailMessageNotification($userEmail->email);     
				}
			}else{
				return redirect()->back()->with('error',__('Sorry,no accomodation found'));
			}
		}else{
			return redirect()->back()->with('error',__('Sorry,no id found'));
		}
	}
	public function contactOwner(Request $r){ 
		if($r->accom_id){
			$accom=DB::table('accomodations')->whereId($r->accom_id)->first();
			if($accom){
				$advertiser=DB::table('accomodations')->select('user_id','payment_type')->whereId($r->accom_id)->first();
				$userEmail=DB::table('users')->select('email')->whereId($advertiser->user_id)->first();
				$approved=0;
				if($advertiser->payment_type==1){
					$approved=1;
				} 
				$hasNewMsg=DB::table('messages')->whereFromUser(Auth::user()->id)->whereAccomId($r->accom_id)->whereIsDeletedCust(0)->first();
				if(!$hasNewMsg){
					$msgId=DB::table('messages')->insertGetId([
							'message'=>$r->text,
							'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name,  
							'to_user'=>$advertiser->user_id,
							'from_user'=>Auth::user()->id,
							'approved'=>$approved,
							'accom_id'=>$r->accom_id,
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s')
					]); 
				}else{
					DB::table('message_replies')->insert([
							'message_id'=>$hasNewMsg->id,
							'reply'=>$r->text,
							'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name, 
							'sent_to'=>$advertiser->user_id,  
							'sent_by'=>Auth::user()->id,
							'accom_id'=>$r->accom_id,
							'approved'=>$approved, 
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s')
					]);
					$msgId=$hasNewMsg->id;	 
				}
				if(!$approved){
					$this->notifyAdminsMessage(
					        'New message has been received for free accomodation ',
					        'New message has been received for free accomodation '.$this->propertyName($r->accom_id),
					        $r->text
					);
					$this->sendMessageNotificationAdmin(['accom_id'=>$r->accom_id,'from_user'=>Auth::user()->id,'message_id'=>$msgId]);
				} else{ 
					$this->sendMessageNotification(['accom_id'=>$r->accom_id,'to_user'=>$advertiser->user_id,'from_user'=>Auth::user()->id,'message_id'=>$msgId]);
					$this->sendEmailMessageNotification($userEmail->email);     
				}
			}
		}else{
			return redirect()->back()->with('error',__('Sorry,no accomodation found'));
		}
	}
	public function contactAdmin(Request $r){ 
		if($r->id){
			$accomId=DB::table('messages')->select('accom_id','from_user','to_user')->whereId($r->id)->first(); 	
			if($accomId){
				$advertiser=DB::table('accomodations')->select('user_id','payment_type')->whereId($accomId->accom_id)->first();
				$userEmail=DB::table('users')->select('email')->whereId($accomId->from_user)->first();
				$approved=1;
				DB::table('message_replies')->insert([
						'message_id'=>$r->id,
						'reply'=>$r->text,
						'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name, 
						'sent_to'=>$accomId->from_user,  
						'sent_by'=>Auth::user()->id,
						'approved'=>$approved,
						'accom_id'=>$accomId->accom_id, 
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
				]);	
				DB::table('message_replies')->insert([
						'message_id'=>$r->id,
						'reply'=>$r->text,
						'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name, 
						'sent_to'=>$accomId->to_user,  
						'sent_by'=>Auth::user()->id,
						'approved'=>$approved,
						'accom_id'=>$accomId->accom_id, 
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
				]);	
				$this->sendMessageNotification(['accom_id'=>$accomId->accom_id,'to_user'=>$accomId->from_user,'from_user'=>Auth::user()->id,'message_id'=>$r->id]);
				$this->sendEmailMessageNotification($userEmail->email);    
			}else{
				return redirect()->back()->with('error',__('Sorry,no accomodation found'));
			}
		}else{
			return redirect()->back()->with('error',__('Sorry,no id found'));
		}
	}
	public function refreshChats(Request $r){
		$returnArr=[];
		$allChats=DB::table('messages')->whereId($r->id)->first();
		if($allChats){ 
			$returnArr[0]['reply_id']=0;
			$returnArr[0]['from_user']=$allChats->from_user;
			$returnArr[0]['sender_name']=$allChats->sender_name;
			$returnArr[0]['created_at']=$allChats->created_at;
			$returnArr[0]['approved']=$allChats->approved;
			$returnArr[0]['message']=$allChats->message;
			$allReplies=DB::table('message_replies')->whereMessageId($r->id)->whereSentBy(Auth::user()->id)->get();
			$moreReplies=DB::table('message_replies')->whereMessageId($r->id)->whereSentTo(Auth::user()->id)->whereApproved(1)->get(); 
			if($allReplies){
				$c=1;
				foreach ($allReplies as $val) {
					$returnArr[$c]['reply_id']=$val->id;
					$returnArr[$c]['from_user']=$val->sent_by;
					$returnArr[$c]['sender_name']=$val->sender_name;
					$returnArr[$c]['created_at']=$val->created_at;
					$returnArr[$c]['approved']=$val->approved;
					$returnArr[$c]['message']=$val->reply;
					$c++;
				}
			} 
			if($moreReplies){ 
				$m=count($returnArr);
				foreach ($moreReplies as $val) {
					$returnArr[$m]['reply_id']=$val->id; 
					$returnArr[$m]['from_user']=$val->sent_by;
					$returnArr[$m]['sender_name']=$val->sender_name;
					$returnArr[$m]['created_at']=$val->created_at;
					$returnArr[$m]['approved']=$val->approved;
					$returnArr[$m]['message']=$val->reply;
					$m++; 
				}
			}
		}
		$returnArr=$this->array_sort($returnArr,'reply_id',SORT_ASC);
		$returnArr=$this->array_sort($returnArr,'approved',SORT_DESC);
		return view('messages.message',compact('returnArr'));    
	} 
	public function refreshChatsOwner(Request $r){
		$returnArr=[];
		$allChats=DB::table('messages')->where('accom_id',$r->accom_id)->whereFromUser(Auth::user()->id)->whereIsDeletedCust(0)->first();
		if($allChats){
			$returnArr[0]['reply_id']=0;
			$returnArr[0]['from_user']=$allChats->from_user;
			$returnArr[0]['sender_name']=$allChats->sender_name;
			$returnArr[0]['created_at']=$allChats->created_at;
			$returnArr[0]['approved']=$allChats->approved; 
			$returnArr[0]['message']=$allChats->message;
			$allReplies=DB::table('message_replies')->whereMessageId($allChats->id)->whereSentBy(Auth::user()->id)->orderBy('id','DESC')->get();
			$moreReplies=DB::table('message_replies')->whereMessageId($allChats->id)->whereSentTo(Auth::user()->id)->whereApproved(1)->orderBy('id','DESC')->get(); 
			if($moreReplies){ 
				$m=1;
				foreach ($moreReplies as $val) {
					$returnArr[$m]['reply_id']=$val->id;
					$returnArr[$m]['from_user']=$val->sent_by;
					$returnArr[$m]['sender_name']=$val->sender_name;
					$returnArr[$m]['created_at']=$val->created_at;
					$returnArr[$m]['approved']=$val->approved;
					$returnArr[$m]['message']=$val->reply;
					$m++;
				}
			}
			if($allReplies){
				$c=count($returnArr);
				foreach ($allReplies as $val) {
					$returnArr[$c]['reply_id']=$val->id;
					$returnArr[$c]['from_user']=$val->sent_by;
					$returnArr[$c]['sender_name']=$val->sender_name;
					$returnArr[$c]['created_at']=$val->created_at;
					$returnArr[$c]['approved']=$val->approved;
					$returnArr[$c]['message']=$val->reply;
					$c++;
				}
			}
			
		}
		$returnArr=$this->array_sort($returnArr,'reply_id',SORT_ASC); 
		$returnArr=$this->array_sort($returnArr,'approved',SORT_DESC);
		return view('messages.message',compact('returnArr'));    
	}
	function array_sort($array, $on, $order=SORT_ASC){
	    $new_array = array();
	    $sortable_array = array();
	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }
	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }
	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }
	    return $new_array;
	}
	public function refreshChatsAdvertiser(Request $r){ 
		$returnArr=[];
		$allChats=DB::table('messages')->where('messages.id',$r->id)->first();
		if($allChats){
			$returnArr[0]['reply_id']=0;
			$returnArr[0]['from_user']=$allChats->from_user;
			$returnArr[0]['sender_name']=$allChats->sender_name;
			$returnArr[0]['created_at']=$allChats->created_at;
			$returnArr[0]['approved']=$allChats->approved; 
			$returnArr[0]['message']=$allChats->message;
			$allReplies=DB::table('message_replies')->whereMessageId($r->id)->whereSentBy(Auth::user()->id)->get();
			$moreReplies=DB::table('message_replies')->whereMessageId($r->id)->whereSentTo(Auth::user()->id)->whereApproved(1)->get(); 
			if($allReplies){
				$c=count($returnArr);
				foreach ($allReplies as $val) {
					$returnArr[$c]['reply_id']=$val->id;
					$returnArr[$c]['from_user']=$val->sent_by;
					$returnArr[$c]['sender_name']=$val->sender_name;
					$returnArr[$c]['created_at']=$val->created_at;
					$returnArr[$c]['approved']=$val->approved;
					$returnArr[$c]['message']=$val->reply;
					$c++;
				}
			}
			if($moreReplies){ 
				$m=count($returnArr);
				foreach ($moreReplies as $val) {
					$returnArr[$m]['reply_id']=$val->id;
					$returnArr[$m]['from_user']=$val->sent_by;
					$returnArr[$m]['sender_name']=$val->sender_name;
					$returnArr[$m]['created_at']=$val->created_at;
					$returnArr[$m]['approved']=$val->approved;
					$returnArr[$m]['message']=$val->reply;
					$m++;
				}
			}	
		} 
		$returnArr=$this->array_sort($returnArr,'reply_id',SORT_ASC);
		$returnArr=$this->array_sort($returnArr,'approved',SORT_DESC);
		return view('messages.adv_msg',compact('returnArr'));  
	} 
	public function refreshChatsAdmin(Request $r){  
		$returnArr=[];
		$allChats=DB::table('messages')->where('messages.id',$r->id)->get();
		if($allChats){
			$d=0;
			$replyIds=[];
			foreach($allChats as $a){
				$replyIds[]=$a->id;
				$returnArr[$d]['id']=$r->id;
				$returnArr[$d]['from_user']=$a->from_user;
				$returnArr[$d]['sender_name']=$a->sender_name;
				$returnArr[$d]['created_at']=$a->created_at;
				$returnArr[$d]['approved']=$a->approved;
				$returnArr[$d]['message']=$a->message; 
				$returnArr[$d]['type']='msg'; 
				$d++;
			}
			$allReplies=DB::table('message_replies')->whereIn('message_id',$replyIds)->get();
			if($allReplies){
				$c=count($returnArr);
				foreach ($allReplies as $val) {
					$returnArr[$c]['id']=$val->id;
					$returnArr[$c]['from_user']=$val->sent_by;
					$returnArr[$c]['sender_name']=$val->sender_name;
					$returnArr[$c]['created_at']=$val->created_at;
					$returnArr[$c]['approved']=$val->approved;
					$returnArr[$c]['message']=$val->reply;
					$returnArr[$c]['type']='reply'; 
					$c++;
				}
			}
		}   
		$returnArr=$this->array_sort($returnArr,'approved',SORT_DESC); 
		return view('messages.admin_msg',compact('returnArr'));    
	} 
	public function editMessage(Request $r){
		if($r->type){
			if($r->type=='reply'){
				$res['msg']=DB::table('message_replies')->whereId($r->id)->first();
			}else{
				$res['msg']=DB::table('messages')->whereId($r->id)->first();
			}
			return json_encode($res);
			die;
		}
	}
	public function updateMessage(Request $r){
		
		if($r->type){
			if($r->type=='1'){

				$msg=MessageReply::where('id',$r->id)->first();
				$msg->reply=$r->text;
				// $update=DB::table('message_replies')->whereId($r->id)->update(['reply'=>$r->text]);
				if($msg->save()){
					return "DONE";
				}
			}else{
				$msg=Message::where('id',$r->id)->first();
				$msg->message=$r->text;
				if($msg->save()){
					return "DONE";
				}
			}
		}
	}
	public function approveMsg($id){
		if($id){
			$update=DB::table('messages')->whereId($id)->update(['approved'=>1]);
			if($update){
				$data=DB::table('messages')->whereId($id)->first();
				if($data){
					$user=DB::table('users')->select('email')->whereId($data->to_user)->first(); 
					$this->sendMessageNotification(['accom_id'=>$data->accom_id,'to_user'=>$data->to_user,'from_user'=>$data->from_user,'message_id'=>$data->id]);
					$this->sendEmailMessageNotification($user->email);   
					return redirect()->back()->with('success',__('Message has been approved')); 
				}
			}
			return redirect()->back()->with('error',__('Internal error,cannot approve,try again!'));	
		}else{
			return redirect()->back()->with('error',__('Sorry no id found'));
		}
	}
	public function approveMsgReply($id){
		if($id){
			$update=DB::table('message_replies')->whereId($id)->update(['approved'=>1]);
			if($update){
				$data=DB::table('message_replies')->whereId($id)->first();
				if($data){
					$user=DB::table('users')->select('email')->whereId($data->sent_to)->first();
					$this->sendMessageNotification(['accom_id'=>$data->accom_id,'to_user'=>$data->sent_to,'from_user'=>$data->sent_by,'message_id'=>$data->id]);
					$this->sendEmailMessageNotification($user->email);    
					return redirect()->back()->with('success',__('Message has been approved')); 
				}
			} 
		}else{ 
			return redirect()->back()->with('error',__('Sorry no id found'));
		}
	}
	public function deleteMsg($id){
		if($id){
			if(Auth::user()->role_id==1){
				$delete=DB::table('messages')->whereId($id)->update(['is_deleted_admin'=>1]);
			}else if(Auth::user()->role_id==2){
				$delete=DB::table('messages')->whereId($id)->update(['is_deleted_cust'=>1]);
			}else{
				$delete=DB::table('messages')->whereId($id)->update(['is_deleted_adv'=>1]);
			}
			if($delete){ 
				return redirect()->back()->with('success',__('Message has been deleted')); 
			}
			return redirect()->back()->with('error',__('Internal error,cannot delete,try again!'));	
		}else{
			return redirect()->back()->with('error',__('Sorry no id found'));
		}
	}
	public function advMessages(){
		return view('messages.adv_messge');
	}
	public function advMessagesAjax(){ 
		$chats=DB::table('messages')
				->leftJoin('accomodations', 'messages.accom_id', '=', 'accomodations.id')
				->leftJoin('users', 'messages.from_user', '=', 'users.id')
				->select('accomodations.name','messages.from_user','accomodations.id as accom_id','users.first_name','users.last_name','users.email','messages.created_at','messages.id','messages.approved')
				->whereToUser(Auth::user()->id)
				->where('messages.approved',1)
				->where('from_user','!=',Auth::user()->id)
				->whereIsDeletedAdv(0) 
				//->groupBy('accom_id') 
				//->groupBy('from_user')
				->orderBy('messages.id','DESC') 
				->get();
		return Datatables::of($chats) 
		->editColumn('first_name',function($name){
			return $name->first_name.' '.$name->last_name;
		})
		->editColumn('created_at',function($date){
			return date('d-m-Y',strtotime($date->created_at));
		})
		->editColumn('approved',function($approved){
			if(!$approved->approved){
				return '<span class="label label-danger">'.__('Un-approved').'</span>';
			}else{
				return '<span class="label label-success">'.__('Approved').'</span>';
			}
		})
		->editColumn('email',function($mail){
			$isPaid=DB::table('accomodations')->select('payment_type')->whereId($mail->accom_id)->first();
			if($isPaid->payment_type==1){
				return $mail->email;
			}else{
				return '**********';
			}
		})
		->addColumn('action', function ($a) {
			$str='<a href="/tickets/chat/'.$a->id. '"class="btn btn-xs btn-info" title="Edit"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
			$str.='&nbsp;<a href="javascript:;" title="Delete" onclick="confirmDelete('.$a->id.');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </a>';  
			if($a->approved){
				$unread=DB::table('messages')->whereId($a->id)->whereToUser(Auth::user()->id)->whereSeenByAdv(0)->whereApproved(1)->count();
				if(!$unread){
					$unread=DB::table('message_replies')->where('message_id',$a->id)->whereSentTo(Auth::user()->id)->whereSeenByAdv(0)->whereApproved(1)->count();
				}
				if($unread>0){
					$str.="&nbsp;<i class='fa fa-envelope' title='Unread'></i>";
				}
			}
			return $str;   
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function custMessages(){
		return view('messages.cust_messge');
	}
	public function custMessagesAjax(){
		$chats=DB::table('messages')
				->leftJoin('accomodations', 'messages.accom_id', '=', 'accomodations.id')
				->leftJoin('users', 'messages.to_user', '=', 'users.id')
				->select('accomodations.name','messages.from_user','accomodations.id as accom_id','users.first_name','users.last_name','users.email','messages.created_at','messages.id','messages.approved')
				->whereFromUser(Auth::user()->id) 
				->whereIsDeletedCust(0) 
				//->where('messages.approved',1)
				->orderBy('messages.id','DESC') 
				->groupBy('accom_id') 
				->get();
		if($chats){
			return Datatables::of($chats) 
			->editColumn('first_name',function($name){
				return $name->first_name.' '.$name->last_name;
			})
			->editColumn('created_at',function($date){
				return date('d-m-Y',strtotime($date->created_at));
			})
			->editColumn('approved',function($approved){
				if(!$approved->approved){
					return '<span class="label label-danger">'.__('Un-approved').'</span>';
				}else{
					return '<span class="label label-success">'.__('Approved').'</span>';
				}
			})
			->editColumn('email',function($mail){ 
				$isPaid=DB::table('accomodations')->select('payment_type')->whereId($mail->accom_id)->first();
				if($isPaid->payment_type==1){
					return $mail->email;
				}else{
					return '**********';
				}
			})
			->addColumn('action', function ($a) {    
				$str='<a href="/tickets/cust-chat/'.$a->id.'" title="Edit" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
				$str.='&nbsp;<a href="javascript:;" title="Delete" onclick="confirmDelete('.$a->id.');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </a>';  
				if($a->approved){
					$unread=DB::table('message_replies')->where('message_id',$a->id)->whereSentTo(Auth::user()->id)->whereSeenByCust(0)->whereApproved(1)->count();
					if($unread>0){
						$str.="&nbsp;<i class='fa fa-envelope' title='Unread'></i>"; 
					} 
				}
				return $str;   
			}) 
			->escapeColumns([])
			->make(true); 
		}
		 
	}
	public function markSeen($id=null){  
		if($id){ 
			DB::table('message_notifications')->whereMessageId($id)->update(['seen'=>1]);
			if(Auth::user()->role_id==3){
				DB::table('messages')->whereId($id)->whereSeenByAdv(0)->whereApproved(1)->update(['seen_by_adv'=>1]);
				DB::table('message_replies')->whereMessageId($id)->whereSentTo(Auth::user()->id)->whereSeenByAdv(0)->whereApproved(1)->update(['seen_by_adv'=>1]); 
			}else if(Auth::user()->role_id==2){
				DB::table('messages')->whereId($id)->whereSeenByCust(0)->whereApproved(1)->update(['seen_by_cust'=>1]);
				DB::table('message_replies')->whereMessageId($id)->whereSentTo(Auth::user()->id)->whereSeenByCust(0)->whereApproved(1)->update(['seen_by_cust'=>1]);  
			}
		}
	} 
	public function chat($id=null){ 
		if($id){
			$this->markSeen($id);
			return view('messages.chat',compact('id')); 
		}
		return redirect()->back()->with('error',('Sorry no chat found'));
	}
	public function custChat($id=null){
		if($id){
			$this->markSeen($id);
			return view('messages.cust_chat',compact('id'));   
		}
		return redirect()->back()->with('error',('Sorry no chat found'));
	}
	public function adminChat($id=null){
		if($id){
			return view('messages.admin_chat',compact('id')); 
		}
		return redirect()->back()->with('error',('Sorry no chat found'));
	}
	public function ownerChat($accom_id){
		if($accom_id){
			return view('messages.contact_owner',compact('accom_id'));
		}
		return redirect()->back()->with('error',('Sorry accomodation found'));
	}
	
	public function propertyName($value) {
		$name=DB::table('accomodations')->select('name')->whereId($value)->first();
		return $name->name;
	}
	public function getAdminMessage(){
		return view('messages.admin_message');
	}
	public function getAdminMessageAjax(){
		$chats=DB::table('messages')
				->leftJoin('accomodations', 'messages.accom_id', '=', 'accomodations.id')
				->leftJoin('users', 'messages.from_user', '=', 'users.id')
				->select('accomodations.name','messages.from_user','accomodations.id as accom_id','users.first_name','users.last_name','users.email','messages.created_at','messages.id','messages.approved')
				->whereIsDeletedAdmin(0)
				// ->groupBy('from_user')
				// ->groupBy('accom_id')
				->orderBy('messages.id','DESC') 
				->get(); 
		return Datatables::of($chats) 
		->editColumn('first_name',function($name){
			return $name->first_name.' '.$name->last_name;
		}) 
		->editColumn('created_at',function($date){
			return date('d-m-Y',strtotime($date->created_at));
		})
		->editColumn('approved',function($approved){
			if(!$approved->approved){
				return '<span class="label label-danger">'.__('Un-approved').'</span>';
			}else{
				$allReplies=DB::table('message_replies')->whereMessageId($approved->id)->whereApproved(0)->get();
				if($allReplies->count()>0){
					return '<span class="label label-danger">'.__('Un-approved').'</span>';		
				}else{
					return '<span class="label label-success">'.__('Approved').'</span>';
				}
			}
		})
		->addColumn('action', function ($a) {    
			$str='<a href="/tickets/admin-chat/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>&nbsp;'; 
			$str.='<a href="javascript:;" onclick="confirmDelete('.$a->id.');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </a>';  
			return $str;   
		}) 
		->escapeColumns([])
		->make(true); 
	} 
}