<?php

namespace LBV\Http\Controllers;

use DB;
use Mail;
use Illuminate\Http\Request;
use LBV\Model\Accomodation; 
use LBV\Model\Newsletter;
use LBV\Model\NewsletterContent;
use LBV\Model\NewsletterSubscriber;
use Yajra\Datatables\Datatables;
use LBV\Mail\SendNewsletter;
use Illuminate\Support\Facades\Auth;


class NewsletterController extends Controller
{
	public function index(){
		return view('newsletters.index');
	}
	public function getAddNewsletter(){
		$finalArr=[];
		$tip=$this->getCurrentMonthPromotionalAccoms(1);
		$spots=$this->getCurrentMonthPromotionalAccoms(2);
		$lastMinutes=$this->getCurrentMonthPromotionalAccoms(5);
		$news=$this->getCurrentMonthPromotionalAccoms(3);
		if(!$tip->isEmpty()) {
			$tipIds=[];
			foreach($tip as $t) {
				$tipIds[]=$t->accom_id;
			}
			$finalArr['tipOfTheMonth']=$this->getAccomodationDetails($tipIds);
		} 
		if(!$spots->isEmpty()) {
			$spotIds=[];
			foreach($spots as $s) {
				$spotIds[]=$s->accom_id;
			}
			$finalArr['spotsOfTheMonth']=$this->getAccomodationDetails($spotIds);
		}
		if(!$lastMinutes->isEmpty()) {
			$lastIds=[];
			foreach ($lastMinutes as $l) {
				$lastIds[]=$l->accom_id;
			}
			$finalArr['lastOfTheMonth']=$this->getAccomodationDetails($lastIds);
		}
		if(!$news->isEmpty()){
			$newsIds=[];
			foreach ($news as $l) {
				$newsIds[]=$l->accom_id;
			}
			$finalArr['newsOfTheMonth']=$this->getAccomodationDetails($newsIds);
		}
		$accoms=Accomodation::whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id'); 
	    return view('newsletters.add',compact('finalArr','accoms')); 
	} 
	public function getAccomodationDetails($accoms=[]){
		$returnArr=[];
		$property=DB::table('accomodations')->select('id','name','address_id','description','featured_image','slug')->whereIn('id',$accoms)->get();
		$acms=$property->toArray();
		if($acms) {
			$c=0;
			foreach ($acms as $a) {
				if($a->featured_image) {
					$gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->whereThumbImage('thumb_'.$a->featured_image)->first();
					$addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
					$country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
					$region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
					$returnArr['accom'][]=$a;
					$returnArr['accom'][$c]->country=$country->name;
					$returnArr['accom'][$c]->region=$region->region_name;
					$returnArr['accom'][$c]->thumb=$gal?$gal->thumb_image:null; 	
				} else {
					$gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();	
					$addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
					$country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
					$region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
					$returnArr['accom'][]=$a; 
					$returnArr['accom'][$c]->country=$country->name;
					$returnArr['accom'][$c]->region=$region->region_name;
					$returnArr['accom'][$c]->thumb=$gal?$gal->thumb_image:null; 	 	
				}
				$c++;	
			} 
		}
	 	return $returnArr;
	} 
	public function postAddNewsletter(Request $r){
		$exists=DB::table('newsletters')->where('year',date('Y'))->where('month',date('n'))->exists();
		if(!$exists) {
			$newsletter=new Newsletter();
			$newsletter->year=date('Y');
			$newsletter->month=date('n');
			$newsletter->title='Newsletter for the month of '.date('M').' '.date('Y'); 
			if($newsletter->save()) {
				$newsletterContent=new NewsletterContent();
				$newsletterContent->newsletter_id=$newsletter->id;
				$newsletterContent->content=$r->message;
				if($r->tip) {
					$newsletterContent->tip=$r->tip;
				}
				if($r->spot) {
					$newsletterContent->spots=$r->spot;
				}
				if($r->news) {
					$newsletterContent->news=$r->news;
				}
				if($r->minutes) {
					$newsletterContent->minutes=$r->minutes;
				}
				if($r->others) {
					$others=implode(',',$r->others);
					$newsletterContent->others=$others;
				}
				if($newsletterContent->save()) { 
					return redirect('/newsletter')->with('success',__('Newsletter added successfully'));
				} else {
					return redirect()->back()->with('error',__('Sorry cannot add newsletter,please try again!'));
				}
			}else {
				return redirect()->back()->with('error',__('Sorry cannot add newsletter,please try again!'));
			}
		} else {
			return redirect('/newsletter')->with('error',__('Sorry newsletter for this month already exists')); 	
		}
	}  
	public function getCurrentMonthPromotionalAccoms($promotionType){
		if($promotionType){
			if($promotionType==5 || $promotionType==3 ) {
				return DB::table('product_licenses')
					->select('product_licenses.accom_id','product_instances.product_category_id','accomodation_news.id')
					->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
					->leftJoin('product_payments','product_payments.id','=','product_licenses.product_payment_id')
					->leftJoin('accomodation_news','product_licenses.id','=','accomodation_news.product_license_id')
					->where('product_instances.product_category_id',$promotionType)
					->where('product_licenses.valid_upto','>=',date('Y-m-d'))
					->where('product_licenses.valid_from','<=',date('Y-m-d'))
					->whereIn('product_payments.payment_complete',[1,3])
					->whereProductYear(date('Y'))->whereActivated(1)
					->get(); 
			} else {
				return DB::table('product_licenses')
					->select('product_licenses.accom_id','product_instances.product_category_id')
					->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
					->where('product_instances.product_category_id',$promotionType)
					->whereProductYear(date('Y'))->whereProductMonth(date('n'))
					->get(); 
			}
		}
	}
	public function getNewsletterAjax(){
		$newsletters=DB::table('newsletters')->get();
		return Datatables::of($newsletters)   
		->addColumn('action', function ($a) {    
			$str='<a href="/newsletter/view/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
			$str.='&nbsp;<a href="/newsletter/edit/'.$a->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> </a>'; 
			$str.='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			$str.='&nbsp;<a href="/newsletter/preview/'.$a->id.'" class="btn btn-xs btn-success" target="__blank">Preview</a>'; 
			return $str;    
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function view($id){
	 	if($id) {
	 		$finalArr=[]; 
	 		$newsletter=Newsletter::with('newsletter_content')->whereId($id)->first();
	 		$finalArr['tipOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->tip)); 
	 		$finalArr['spotsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->spots));
	 		$finalArr['lastOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->minutes));
	 		$finalArr['newsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->news));
	 		$finalArr['others']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->others));
	 		return view('newsletters.view',compact('finalArr','newsletter'));
	 	}
	}  
	public function preview($id){ 
		if($id) {
			$newsletter=Newsletter::with('newsletter_content')->whereId($id)->first(); 
			$finalArr['tipOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->tip));
			$finalArr['spotsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->spots));
			$finalArr['lastOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->minutes));
			$finalArr['newsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->news));
			$finalArr['others']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->others));
			$accoms=Accomodation::whereApproved(1)->pluck('name','id');
			return view('emails.newsletters.newsletter',compact('newsletter','content','finalArr','accoms'));  
		}
	}
	public function delete($id){ 
		if($id) {
			$newsletter=Newsletter::whereId($id)->first();
			if($newsletter->delete()) {
				$content=NewsletterContent::whereNewsletterId($id)->first();
				if($content->delete()) {
					return redirect()->back()->with('success',__('Newsletter deleted successfully'));
				} else {
					return redirect()->with('error',__('Sorry cannot delete newsletter,please try again!')); 
				}
			} else {
				return redirect('/newsletter')->with('error',__('Sorry cannot delete newsletter,please try again!')); 	
			}
		}
	}
	public function getEdit($id){
		if($id) {
			$newsletter=Newsletter::with('newsletter_content')->whereId($id)->first();
			$finalArr['tipOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->tip));
			$finalArr['spotsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->spots));
			$finalArr['lastOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->minutes));
			$finalArr['newsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->news));
			$finalArr['others']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->others));
			$accoms=Accomodation::whereApproved(1)->pluck('name','id');
			return view('newsletters.edit',compact('newsletter','content','finalArr','accoms')); 
		} 
	} 
	public function postEdit(Request $r,$contentId){
		if($r->message) {	
			$content=NewsletterContent::whereId($contentId)->first();
			$content->content=$r->message;
			$content->others=implode(',',$r->others);
			if($content->save()) {
				\Log::log('newsletter_edited','Newsletter with ID:'.$content->newsletter_id.' has been edited by User:'.Auth::user()->email); 
				return redirect('/newsletter')->with('success',__('Newsletter upated successfully'));
			}else {
				return redirect()->with('error',__('Sorry cannot upate newsletter,please try again!')); 
			}
		} 
	} 
	public function send($id){
		if($id) {
			$newsletter=Newsletter::with('newsletter_content')->whereId($id)->first(); 
			$finalArr['tipOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->tip));
			$finalArr['spotsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->spots));
			$finalArr['lastOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->minutes));
			$finalArr['newsOfTheMonth']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->news));
			$finalArr['others']=$this->getAccomodationDetails(explode(',',$newsletter->newsletter_content->others));
			//$path=env('SITE_URL').public_path('/images/gallery/'.$finalArr['tipOfTheMonth']['accom'][0]->id.'/'.$finalArr['tipOfTheMonth']['accom'][0]->thumb); 
			//dd($path);
			$this->sendNewsletter($newsletter->title,$finalArr,$newsletter);
			return redirect()->back()->with('success',__('Newsletter sent successfully'));
		}
	}
	public function sendNewsletter($subject,$finalArr,$newsletter) {
		$subscribers=DB::table('newsletter_subscribers')->whereActive(1)->get(); 
		if($subscribers) {
			foreach ($subscribers as $u) {
				if(env('IS_MAIL_QUEUE')) {  
				    Mail::to($u->email)->queue(new SendNewsletter($subject,$finalArr,$newsletter)); 
				    \Log::log('newsletter_sent',$subject.' sent to '.$u->email); 
				} else { 
					try {
						Mail::to($u->email)->send(new SendNewsletter($subject,$finalArr,$newsletter));     
						\Log::log('newsletter_sent',$subject.' sent to '.$u->email); 
					} catch(Exception $e) {
						\Log::log('newsletter_not_sent',$subject.' not sent to '.$u->email.' Server returned error: '.$e->getMessage()); 
					}
				} 
			} 
		}
	}
	public function subscribers(){
		return view('newsletters.subscribers');
	}
	public function subscribersAjax(){
		$subscribers=DB::table('newsletter_subscribers')->get();
		return Datatables::of($subscribers)   
		->addColumn('action', function ($a){    
			$str='<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			return $str;    
		})
		->editColumn('active',function($a){
			if($a->active) {
				return "<span class='label label-success'>".__('Active')."</span>";
			} else {
				return "<span class='label label-danger'>".__('In-Active')."</span>";
			}
		}) 
		->escapeColumns([])
		->make(true); 
	}
	public function deleteSubscriber($id){
		if($id){
			$subscriber=NewsletterSubscriber::whereId($id)->first();
			if($subscriber) {
				if($subscriber->delete()) {
					return redirect()->back()->with('success',__('Subscriber deleted successfully'));
				} else {
					return redirect()->back()->with('error',__('Subscriber not deleted,please try again!'));
				}
			}
		}
	} 
}
