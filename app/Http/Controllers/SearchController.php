<?php

namespace LBV\Http\Controllers;

use DB;
use LBV\Model\Accomodation;
use LBV\Model\SearchingGroup;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cache;
// use Illuminate\Support\Facades\Paginator;

class SearchController extends Controller
{
  public $time_start;
   public function getHomeSearch(){
      $types=DB::table('accomodation_types')->orderBy('type_name','asc')->pluck('type_name','id');
      $countries=DB::table('countries')->orderBy('name','asc')->pluck('name','id');
      $searchGroup=SearchingGroup::all()->toArray();
      $count=count($searchGroup);
      for($i=0;$i<$count;$i++){
         $searchGroupWithTags=DB::table('searchtag_searching_groups')
           ->leftJoin('searchtags', 'searchtag_searching_groups.searchtag_id', '=', 'searchtags.id')->where('searching_group_id',$searchGroup[$i]['id'])
           ->get()->toArray();
         $searchGroup[$i]['SearchingTags']=$searchGroupWithTags;
      }
		return view('search.search',compact('types','countries','searchGroup'));   
   }
  public function homeSearch(Request $r){
      $this->time_start = microtime(true); 
      $searchTerm=$r->all();
      // $result=$this->getSearchResults($searchTerm);
      $result=$this->searchResultsNew($r,$searchTerm);
      // $result=$this->searchResults($r,$searchTerm); 
      $promos=$this->getPromotionalAccoms();
      return view('search.results',compact('accoms','accomsArr','promos','result'));      
  } 
  public function homeTextSearch(Request $r){
    //dd($r); 
    $searchTerm=trim($r->term);
    $searchChunks=explode(',',$searchTerm);
    //dump($searchChunks);
    DB::enableQueryLog();
     
    for($i=0;$i<count($searchChunks);$i++){
        $searchChunks[$i]=trim($searchChunks[$i]);
       
        $countries[]=DB::table('countries')->select('countries.id','countries.name as lable','countries.name as value')
         ->where('countries.is_approved',1)
         ->where('countries.name','like','%'.$searchChunks[$i].'%')
         ->orderBy('lable','ASC')
         ->get()->toArray(); 
        // dump($countries); 

         $regions[]=DB::table('regions')->select('regions.id','regions.region_name as lable','regions.region_name as value')
                   ->where('regions.is_approved',1)
                   ->where('regions.region_name','like','%'.$searchChunks[$i].'%')
                   ->orderBy('lable','ASC')
                   ->get()->toArray();


        $departments[]=DB::table('departments')->select('departments.id','departments.department_name as lable','departments.department_name as value')
         ->where('departments.is_approved',1)
         ->where('departments.department_name','like','%'.$searchChunks[$i].'%')
         ->orderBy('lable','ASC')
         ->get()->toArray();

         //dump($departments);
        $types[]=DB::table('accomodation_types')->select('accomodation_types.id','accomodation_types.type_name as lable','accomodation_types.type_name as value')
         ->where('accomodation_types.type_name','like','%'.$searchChunks[$i].'%')
         ->orderBy('lable','ASC')
         ->get()->toArray();


        $amnities[]=DB::table('searchtags')->select('searchtags.id','searchtags.tag_name as lable','searchtags.tag_name as value')
          ->where('searchtags.tag_name','like','%'.$searchChunks[$i].'%')
          ->orderBy('lable','ASC')
          ->get()->toArray();

         // dump($searchChunks[$i]);
        $description[]=DB::table('accomodations')->select('accomodations.id','accomodations.name as lable','accomodations.name as value')
            ->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchChunks[$i]."' IN BOOLEAN MODE)")
            ->get()->toArray();
    }
  


   $FINALRETURNARR=[];

     if($countries) {
       foreach ($countries[0] as $c) {
        if($c){
          $c->type='country';
          $FINALRETURNARR[]=$c;
        }
       }
     }
     if($regions) {
       foreach ($regions[0] as $c) {
        if($c){
          $c->type='region';
          $cId=DB::table('regions')->select('country_id')->whereId($c->id)->first();
          $c->country_id=$cId->country_id;
          $FINALRETURNARR[]=$c;  
        }
       }
     }
     if($departments) {
       foreach ($departments[0] as $c) {
        if($c){
          $c->type='department';
          $region=DB::table('departments')->select('region_id')->whereId($c->id)->first();
          $cId=DB::table('regions')->select('country_id')->whereId($region->region_id)->first();
          $c->country_id=$cId->country_id;
          $FINALRETURNARR[]=$c;
        }
       }
     }
     if($types) {
       foreach ($types[0] as $c) {
        if($c){
          $c->type='type';
          $FINALRETURNARR[]=$c;
        }
       }
     }
     if($amnities) {
       foreach ($amnities[0] as $c) {
        if($c){
          $c->type='amnity';
          $FINALRETURNARR[]=$c;
        }
       }
     }
     if($description){
        //dd($description[0]);
        foreach ($description[0] as $d) {
          $d->type='desc';
          $FINALRETURNARR[]=$d;
        }
     }

     //dd($FINALRETURNARR);

     echo json_encode($FINALRETURNARR);
     die; 
  }
  public function getDetailsAccom($array=array()){
    if($array){
       $accomodations=DB::table('accomodations')
             // DB::raw('DISTINCT()')
             ->select('accomodations.id', 'accomodations.name','accomodations.number_of_persons','accomodations.bathrooms','accomodations.bedrooms','accomodations.slug',
                         'accomodations.featured_image','accomodation_prices.price_unit','accomodation_prices.amount',
                         'countries.name as cName','regions.region_name as rName','departments.department_name as dName'
             ) 
             ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
             ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
             ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
             ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
             ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
             ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
             ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
             ->where('accomodations.approved',1)
             ->where('accomodations.completed',1)
             ->where('accomodations.subs_valid_license',1)
             ->whereIn('accomodations.id',$array)
             ->groupBy('accomodations.id')
             ->get();
        $accomsArr=$accomodations->toArray();
        /*$c=0;
        foreach($accomsArr as $a){
            if($a->featured_image){
               $accomsArr[$c]->featured_image='thumb_'.$a->featured_image;
            }else{
             $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
             if($img){
                $accomsArr[$c]->featured_image=$img->thumb_image;
             }
            }
            $minPrice=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
            if($minPrice){
              $accomsArr[$c]->amount=$minPrice;
            }
            $c++;
        }*/
        return $accomsArr; 
    }
  }
  public function getLastMinutes(){
    return DB::table('accomodation_lastminutes')->select('accomodation_lastminutes.accom_id')
        ->leftJoin('product_licenses','product_licenses.id','=','accomodation_lastminutes.product_license_id') 
        ->where('accomodation_lastminutes.start_from','<=',date('Y-m-d H:i:s'))
        ->where('accomodation_lastminutes.valid_upto','>=',date('Y-m-d H:i:s'))
       /* ->where('product_licenses.valid_from','<=',date('Y-m-d H:i:s'))
        ->where('product_licenses.valid_upto','>=',date('Y-m-d H:i:s'))*/
        ->where('product_licenses.activated',1)
        ->get();
  }
  public function getNews(){
    return DB::table('accomodation_news')->select('accomodation_news.accom_id')
        ->leftJoin('product_licenses','product_licenses.id','=','accomodation_news.product_license_id') 
        ->where('accomodation_news.start_from','<=',date('Y-m-d H:i:s'))
        ->where('accomodation_news.valid_upto','>=',date('Y-m-d H:i:s'))
       /* ->where('product_licenses.valid_from','<=',date('Y-m-d H:i:s'))
        ->where('product_licenses.valid_upto','>=',date('Y-m-d H:i:s'))*/
        ->where('product_licenses.activated',1)
        ->get();
  }
  public function getPromotionalAccoms(){
      $tips=app('LBV\Http\Controllers\NewsletterController')->getCurrentMonthPromotionalAccoms(1);
      $spots=app('LBV\Http\Controllers\NewsletterController')->getCurrentMonthPromotionalAccoms(2);
      $lastMinutes=$this->getLastMinutes();
      $news=$this->getNews();

      $returnArr['Tips']=$tips;
      $returnArr['Spots']=$spots;
      $returnArr['Lastminutes']=$lastMinutes;
      $returnArr['News']=$news;
      $returnFinalArr=[];
      foreach ($returnArr as $key => $value) {
          foreach ($value as $i) {
              $returnFinalArr[$key][]=$i->accom_id;
          }
      }
      return $returnFinalArr;
  }
  public function getClusterAccomodations($r,$cluster){
    if($cluster){
          
      $promotions=$this->promotionalAccomIDS(); 
            $accomodations=DB::table('accomodations')
                  // DB::raw('DISTINCT()')
                  ->select('accomodations.id','accomodations.name','accomodations.number_of_persons','accomodations.bathrooms','accomodations.bedrooms','accomodations.slug',
                                         'accomodations.featured_image',
                                         'accomodation_prices.price_unit','accomodation_prices.amount',
                                         'countries.name as cName','regions.region_name as rName','departments.department_name as dName'
                             ) 
                  ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                  ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
                  ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
                  ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
                  ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
                  // ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
                  ->where('accomodations.approved',1)
                  ->where('accomodations.completed',1)
                  ->where('accomodations.subs_valid_license',1)
                  ->whereRaw('accomodations.id IN('.$cluster.')')
                  
                  
        
                  ->groupBy('accomodations.id')
                  ->get();
            $accomsArr=$accomodations->toArray();
            $finalReturn=$searchPromos=[]; 
            foreach ($accomsArr as $v) {
              if(in_array($v->id ,$promotions['tipsids'])) {
                  $searchPromos[]=$v;
              }
            }
            foreach ($accomsArr as $v) {
              if(in_array($v->id , (array) $promotions['spotsIds'])) {
                     $searchPromos[]=$v;
              }
            }
            foreach ($accomsArr as $v) {
              if(in_array($v->id , (array) $promotions['lastIds'])) {
                      $searchPromos[]=$v;
                  }
            }
            foreach ($accomsArr as $v) {
              if(in_array($v->id , (array) $promotions['newsids'])) {
                     $searchPromos[]=$v;
                 }
            }

            $finalReturn=$searchPromos+$accomsArr;
            $finalReturn=array_unique($finalReturn, SORT_REGULAR);
            
             $paginate = 10;
             $page = $r->page?$r->page:1;
             $offSet = ($page * $paginate) - $paginate; 
             $itemsForCurrentPage = array_slice($finalReturn, $offSet, $paginate, true);  
             $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($finalReturn), $paginate, $page);
             $resultAccom= $result->toArray();
             $c=$offSet;
            // dump($resultAccom);
             foreach($resultAccom['data'] as $k=>$a){
                 if($a->featured_image){
                    $resultAccom['data'][$k]->featured_image='thumb_'.$a->featured_image;
                 }else{
                  $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
                  if($img){
                     $resultAccom['data'][$k]->featured_image=$img->thumb_image;
                  }
                 }
                 $minPrice=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                 if($minPrice){
                   $resultAccom['data'][$k]->amount=$minPrice;
                 }
                 $c++;
             }
            //dd($resultAccom);
             $finalArr['accoms']=$result;
             $finalArr['accomsArr']=$resultAccom['data']; 
             return $finalArr;




    }
  }
  public function getSearchResults($searchTerm){
      $finalArr=[];
      if(isset($searchTerm['clusterAccoms'])){
         $accoms=$this->getClusterAccomodations($searchTerm['clusterAccoms']);
         return $accoms;
      }else{
          $accoms=DB::table('accomodations')
                // DB::raw('DISTINCT()')
                ->select('accomodations.id', 'accomodations.name','accomodations.number_of_persons','accomodations.bathrooms','accomodations.bedrooms','accomodations.slug',
                            'accomodations.featured_image','accomodation_prices.price_unit','accomodation_prices.amount',
                            'countries.name as cName','regions.region_name as rName','departments.department_name as dName'
                ) 
                ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
                ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
                ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
                ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
                ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
                ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
                ->where('accomodations.approved',1)
                ->where('accomodations.completed',1)
                ->where('accomodations.subs_valid_license',1)
                // ->where('subscription_licenses.valid_upto','>',date('Y-m-d')) 
                //->orWhere('accomodations.payment_type',2)  
                ->where(function ($query) {
                        $query->where('subscription_licenses.valid_upto','>',date('Y-m-d'))
                            ->orWhere('accomodations.payment_type', 2);
                })
                ->when($searchTerm['label'], function($query) use ($searchTerm){
                  return $query->where('accomodations.label_id',$searchTerm['label']);
                })
                ->when($searchTerm['country'], function($query) use ($searchTerm){
                  return $query->where('accomodation_addresses.country_id',$searchTerm['country']);
                })
                ->when($searchTerm['region'], function($query) use ($searchTerm){
                  return $query->where('accomodation_addresses.region_id',$searchTerm['region']);
                })
                ->when($searchTerm['department'], function($query) use ($searchTerm){
                  return $query->where('accomodation_addresses.department_id',$searchTerm['department']);
                })
                ->when($searchTerm['typeId'], function($query) use ($searchTerm){
                  return $query->whereIn('accomodations.accomodation_type_id',[$searchTerm['typeId']]);
                })
                ->when($searchTerm['noofperson'], function($query) use ($searchTerm){
                  return $query->where('accomodations.number_of_persons','<=',$searchTerm['noofperson']);
                })
                ->when($searchTerm['keywords'], function($query) use ($searchTerm){
                  return $query->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchTerm['keywords']."' IN BOOLEAN MODE)");
                })
                ->when($searchTerm['price'], function($query) use ($searchTerm){
                  $price=explode('-',$searchTerm['price']);
                  return $query->whereBetween('accomodation_prices.amount',[$price[0],$price[1]]); 
                })
                ->when($searchTerm['search_end_date'], function($query) use ($searchTerm){
                  return $query->whereBetween('accomodation_prices.start_date',[$searchTerm['search_start_date'],$searchTerm['search_end_date']]); 
                })
                ->when($searchTerm['amnityArr'], function($query) use ($searchTerm){
                  return $query->whereIn('accomodation_searchtags.searchtag_id',[$searchTerm['amnityArr']]);
                })
                ->groupBy('accomodations.id')
                ->paginate(10); 
                $accomsArr=$accoms->toArray();

                $c=0;
                foreach($accomsArr['data'] as $a){
                    if(!$a->featured_image){
                       $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
                       if($img){
                          $accomsArr['data'][$c]->featured_image=$img->thumb_image;
                       }
                    }else{
                      $accomsArr['data'][$c]->featured_image='thumb_'.$a->featured_image;
                    }
                    $minPrice=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                    if($minPrice){
                      $accomsArr['data'][$c]->amount=$minPrice;
                    }
                    $c++;
                }
                $finalArr['accoms']=$accoms;
                $finalArr['accomsArr']=$accomsArr;
                return $finalArr;
      }
  } 
  public function promotionalAccomIDS(){
    $promos=$this->getPromotionalAccoms();
    $return=$news=$minutes=$spots=$tips=$ids=[];
    $newsids=$tipsids=$spotsIds=$lastIds=[];
    if(isset($promos['News'])){
       foreach ($promos['News'] as $accom){
         $newsids[]=$accom;
         $ids[]=$accom;
       } 
       $news=$this->getDetailsAccom($promos['News']); 
    }
     if(isset($promos['Lastminutes'])){
         foreach ($promos['Lastminutes'] as $accom){
           $ids[]=$accom;
           $lastIds[]=$accom;
         }
         $minutes=$this->getDetailsAccom($promos['Lastminutes']);
     }
     if(isset($promos['Spots'])){
       foreach ($promos['Spots'] as $accom){
         $spotsIds[]=$accom;
         $ids[]=$accom;
       }
       $spots=$this->getDetailsAccom($promos['Spots']);
     }
     if(isset($promos['Tips'])){
       foreach ($promos['Tips'] as $accom){
         /*$search=array_search($accom,array_column($result,'id')); 
         $this->move_to_top($result,$search);*/
         $ids[]=$accom;
         $tipsids[]=$accom;
         
       }  
       $tips=$this->getDetailsAccom($promos['Tips']);
     }

     $return['tips']=$tips;
     $return['Lastminutes']=$minutes;
     $return['Spots']=$spots;
     $return['News']=$news;


     $return['tipsids']=$tipsids;
     $return['spotsIds']=$spotsIds;
     $return['lastIds']=$lastIds;
     $return['newsids']=$newsids;
     $return['ids']=$ids;
     return $return;
  }
  function in_object($value,$object) { 
      dump($value);
      dump($object);
      if (is_object($object)) { 
        foreach($object as $key => $item) { 
          if ($value==$item) {
            return true; 
          }
        } 
      } 
      return false; 
  }
  public function searchResultsNew($r,$searchTerm){
    $promotions=$this->promotionalAccomIDS(); 
    if(isset($searchTerm['clusterAccoms'])){
       $accoms=$this->getClusterAccomodations($r,$searchTerm['clusterAccoms']);
       $finalArr['accoms']=$accoms['accoms'];
       $finalArr['accomsArr']=$accoms['accomsArr']; 
       return $finalArr;
    }else{
        DB::enableQueryLog();
        // dump('Total execution time BEFORE QUERY in seconds: ' . (microtime(true) - $this->time_start)); 
        $accomodations=DB::table('accomodations')
              // DB::raw('DISTINCT()')
              ->select('accomodations.id', 'accomodations.name','accomodations.number_of_persons','accomodations.bathrooms','accomodations.bedrooms','accomodations.slug',
                          'accomodations.featured_image','accomodation_prices.price_unit','accomodation_prices.amount',
                          'countries.name as cName','regions.region_name as rName','departments.department_name as dName'
              ) 
              ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
              ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
              ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
              ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
              ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
              ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
              ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
              ->where('accomodations.approved',1)
              ->where('accomodations.completed',1)
              ->where('accomodations.subs_valid_license',1)
              ->where(function ($query) {
                      $query->where('subscription_licenses.valid_upto','>',date('Y-m-d'))
                          ->orWhere('accomodations.payment_type', 2);
              })
              ->when($searchTerm['label'], function($query) use ($searchTerm){
                return $query->where('accomodations.label_id',$searchTerm['label']);
              })
              ->when($searchTerm['country'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.country_id',$searchTerm['country']);
              })
              ->when($searchTerm['region'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.region_id',$searchTerm['region']);
              }) 
              ->when($searchTerm['department'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.department_id',$searchTerm['department']);
              })
              ->when($searchTerm['typeId'], function($query) use ($searchTerm) {
                $types=explode(',',$searchTerm['typeId']);  
                if($types){ 
                  $accType=array_values(array_unique($types));   
                  if(in_array(2,$types) || in_array(13,$types) ) {
                    $accType[]=13;
                    $accType[]=2; 
                  }    
                  if(in_array(1,$types) || in_array(6,$types) || in_array(12,$types) ) {
                    $accType[]=1;
                    $accType[]=6;
                    $accType[]=12;
                  }
                }
                return $query->whereIn('accomodations.accomodation_type_id',$accType);
              })
              ->when($searchTerm['noofperson'], function($query) use ($searchTerm){
                return $query->where('accomodations.number_of_persons','<=',$searchTerm['noofperson']);
              })
              ->when($searchTerm['keywords'], function($query) use ($searchTerm){
                return $query->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchTerm['keywords']."' IN BOOLEAN MODE)");
              })
              ->when($searchTerm['price'], function($query) use ($searchTerm){
                $price=explode('-',$searchTerm['price']);
                return $query->whereBetween('accomodation_prices.amount',[$price[0],$price[1]]); 
              })
              ->when($searchTerm['search_end_date'], function($query) use ($searchTerm){
                return $query->whereBetween('accomodation_prices.start_date',[$searchTerm['search_start_date'],$searchTerm['search_end_date']]); 
              })
              ->when($searchTerm['amnityArr'], function($query) use ($searchTerm){
                return $query->whereIn('accomodation_searchtags.searchtag_id',[$searchTerm['amnityArr']]);
              })
              //->whereNotIn('accomodations.id',$promotions['ids'])
              ->groupBy('accomodations.id')
              ->get();
              // dump($accomodations);
        // dump(DB::getQueryLog());
        $accomsArr=$accomodations->toArray();
        $finalReturn=$searchPromos=[]; 
        // dump('Total execution time AFTER QUERY in seconds: ' . (microtime(true) - $this->time_start)); 
        foreach ($accomsArr as $v) {
          if(in_array($v->id ,$promotions['tipsids'])) {
              $searchPromos[]=$v;
          }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['spotsIds'])) {
                 $searchPromos[]=$v;
          }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['lastIds'])) {
                  $searchPromos[]=$v;
              }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['newsids'])) {
                 $searchPromos[]=$v;
             }
        }

        $finalReturn=$searchPromos+$accomsArr;
        $finalReturn=array_unique($finalReturn, SORT_REGULAR);
        
         $paginate = 10;
         $page = $r->page?$r->page:1;
         $offSet = ($page * $paginate) - $paginate; 
         $itemsForCurrentPage = array_slice($finalReturn, $offSet, $paginate, true);  
         $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($finalReturn), $paginate, $page);
         $resultAccom= $result->toArray();
         $c=$offSet;
        // dump($resultAccom);
         foreach($resultAccom['data'] as $k=>$a){
             if($a->featured_image){
                $resultAccom['data'][$k]->featured_image='thumb_'.$a->featured_image;
             }else{
              $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
              if($img){
                 $resultAccom['data'][$k]->featured_image=$img->thumb_image;
              }
             }
             $minPrice=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
             if($minPrice){
               $resultAccom['data'][$k]->amount=$minPrice;
             }
             $c++;
         }
        //dd($resultAccom);
         /*dump('Total execution time at END in seconds: ' . (microtime(true) - $this->time_start)); 
          dd('stop 575');*/
         $finalArr['accoms']=$result;
         $finalArr['accomsArr']=$resultAccom['data']; 
         return $finalArr;

    } 
  } 
  public function searchResults($r,$searchTerm){
    $promotions=$this->promotionalAccomIDS(); 
    if(isset($searchTerm['clusterAccoms'])){
       $accoms=$this->getClusterAccomodations($r,$searchTerm['clusterAccoms']);
       $finalArr['accoms']=$accoms['accoms'];
       $finalArr['accomsArr']=$accoms['accomsArr']; 
       return $finalArr;
    }else{
        // DB::enableQueryLog(); 
        $accomodations=DB::table('accomodations')
              // DB::raw('DISTINCT()')
              ->select('accomodations.id', 'accomodations.name','accomodations.number_of_persons','accomodations.bathrooms','accomodations.bedrooms','accomodations.slug',
                          'accomodations.featured_image','accomodation_prices.price_unit','accomodation_prices.amount',
                          'countries.name as cName','regions.region_name as rName','departments.department_name as dName'
              ) 
              ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
              ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
              ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
              ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
              ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
              ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
              ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
              ->where('accomodations.approved',1)
              ->where('accomodations.completed',1)
              ->where('accomodations.subs_valid_license',1)
              ->where(function ($query) {
                      $query->where('subscription_licenses.valid_upto','>',date('Y-m-d'))
                          ->orWhere('accomodations.payment_type', 2);
              })
              ->when($searchTerm['label'], function($query) use ($searchTerm){
                return $query->where('accomodations.label_id',$searchTerm['label']);
              })
              ->when($searchTerm['country'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.country_id',$searchTerm['country']);
              })
              ->when($searchTerm['region'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.region_id',$searchTerm['region']);
              })
              ->when($searchTerm['department'], function($query) use ($searchTerm){
                return $query->where('accomodation_addresses.department_id',$searchTerm['department']);
              })
              ->when($searchTerm['typeId'], function($query) use ($searchTerm){
                return $query->whereIn('accomodations.accomodation_type_id',[$searchTerm['typeId']]);
              })
              ->when($searchTerm['noofperson'], function($query) use ($searchTerm){
                return $query->where('accomodations.number_of_persons','<=',$searchTerm['noofperson']);
              })
              ->when($searchTerm['keywords'], function($query) use ($searchTerm){
                return $query->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchTerm['keywords']."' IN BOOLEAN MODE)");
              })
              ->when($searchTerm['price'], function($query) use ($searchTerm){
                $price=explode('-',$searchTerm['price']);
                return $query->whereBetween('accomodation_prices.amount',[$price[0],$price[1]]); 
              })
              ->when($searchTerm['search_end_date'], function($query) use ($searchTerm){
                return $query->whereBetween('accomodation_prices.start_date',[$searchTerm['search_start_date'],$searchTerm['search_end_date']]); 
              })
              ->when($searchTerm['amnityArr'], function($query) use ($searchTerm){
                return $query->whereIn('accomodation_searchtags.searchtag_id',[$searchTerm['amnityArr']]);
              })
              //->whereNotIn('accomodations.id',$promotions['ids'])
              ->groupBy('accomodations.id')
              ->get();
              // dump($accomodations);
         // dd(DB::getQueryLog());
        $accomsArr=$accomodations->toArray();
        $finalReturn=$searchPromos=[]; 
        foreach ($accomsArr as $v) {
          if(in_array($v->id ,$promotions['tipsids'])) {
              $searchPromos[]=$v;
          }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['spotsIds'])) {
                 $searchPromos[]=$v;
          }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['lastIds'])) {
                  $searchPromos[]=$v;
              }
        }
        foreach ($accomsArr as $v) {
          if(in_array($v->id , (array) $promotions['newsids'])) {
                 $searchPromos[]=$v;
             }
        }

        $finalReturn=$searchPromos+$accomsArr;
        $finalReturn=array_unique($finalReturn, SORT_REGULAR);
        
         $paginate = 10;
         $page = $r->page?$r->page:1;
         $offSet = ($page * $paginate) - $paginate; 
         $itemsForCurrentPage = array_slice($finalReturn, $offSet, $paginate, true);  
         $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($finalReturn), $paginate, $page);
         $resultAccom= $result->toArray();
         $c=$offSet;
        // dump($resultAccom);
         foreach($resultAccom['data'] as $k=>$a){
             if($a->featured_image){
                $resultAccom['data'][$k]->featured_image='thumb_'.$a->featured_image;
             }else{
              $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
              if($img){
                 $resultAccom['data'][$k]->featured_image=$img->thumb_image;
              }
             }
             $minPrice=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
             if($minPrice){
               $resultAccom['data'][$k]->amount=$minPrice;
             }
             $c++;
         }
        //dd($resultAccom);
         $finalArr['accoms']=$result;
         $finalArr['accomsArr']=$resultAccom['data']; 
         return $finalArr;

    } 
  } 
  public function homeSearchMap(Request $r){
    //dd();
    $searchTerm=$r->all();
    /*$accoms=DB::table('accomodations')
          ->select(
            'accomodations.id','accomodations.name','accomodations.featured_image','accomodations.slug',
            'accomodation_addresses.lattitude','accomodation_addresses.longitude','countries.name as country_name',
            'regions.region_name'
            )
                  ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                    ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
                    ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
                    ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
                    ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
                    ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
                    ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
                    ->where('accomodations.approved',1)
                    ->where('accomodations.completed',1)
                    ->where('accomodations.subs_valid_license',1)
                    ->where(function ($query) {
                            $query->where('subscription_licenses.valid_upto','>',date('Y-m-d'))
                                ->orWhere('accomodations.payment_type', 2);
                    })
                    ->when($searchTerm['label'], function($query) use ($searchTerm){
                      return $query->where('accomodations.label_id',$searchTerm['label']);
                    })
                    ->when($searchTerm['country'], function($query) use ($searchTerm){
                      return $query->where('accomodation_addresses.country_id',$searchTerm['country']);
                    })
                    ->when($searchTerm['region'], function($query) use ($searchTerm){
                      return $query->where('accomodation_addresses.region_id',$searchTerm['region']);
                    })
                    ->when($searchTerm['department'], function($query) use ($searchTerm){
                      return $query->where('accomodation_addresses.department_id',$searchTerm['department']);
                    })
                    ->when($searchTerm['typeId'], function($query) use ($searchTerm){
                      return $query->whereIn('accomodations.accomodation_type_id',[$searchTerm['typeId']]);
                    })
                    ->when($searchTerm['noofperson'], function($query) use ($searchTerm){
                      return $query->where('accomodations.number_of_persons','<=',$searchTerm['noofperson']);
                    })
                    ->when($searchTerm['keywords'], function($query) use ($searchTerm){
                      return $query->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchTerm['keywords']."' IN BOOLEAN MODE)");
                    })
                    ->when($searchTerm['price'], function($query) use ($searchTerm){
                      $price=explode('-',$searchTerm['price']);
                      return $query->whereBetween('accomodation_prices.amount',[$price[0],$price[1]]); 
                    })
                    ->when($searchTerm['search_end_date'], function($query) use ($searchTerm){
                      return $query->whereBetween('accomodation_prices.start_date',[$searchTerm['search_start_date'],$searchTerm['search_end_date']]); 
                    })
                    ->when($searchTerm['amnityArr'], function($query) use ($searchTerm){
                      return $query->whereIn('accomodation_searchtags.searchtag_id',[$searchTerm['amnityArr']]);
                    })
                    //->whereNotIn('accomodations.id',$promotions['ids'])
                    ->groupBy('accomodations.id')
                    ->get();*/
                   $accoms=DB::table('accomodations')
                         // DB::raw('DISTINCT()')
                         ->select(
                                    'accomodations.id','accomodations.name','accomodations.featured_image','accomodations.slug',
                                    'accomodation_addresses.lattitude','accomodation_addresses.longitude','countries.name as country_name',
                                    'regions.region_name'
                                    )
                         ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                         ->leftJoin('accomodation_prices','accomodation_prices.accom_id','=','accomodations.id')
                         ->leftJoin('countries','accomodation_addresses.country_id','=','countries.id')
                         ->leftJoin('regions','accomodation_addresses.region_id','=','regions.id')
                         ->leftJoin('departments','accomodation_addresses.department_id','=','departments.id')
                         ->leftJoin('accomodation_searchtags','accomodation_searchtags.accom_id','=','accomodations.id')
                         ->leftJoin('subscription_licenses','subscription_licenses.accom_id','=','accomodations.id')
                         ->where('accomodations.approved',1)
                         ->where('accomodations.completed',1)
                         ->where('accomodations.subs_valid_license',1)
                         ->where(function ($query) {
                                 $query->where('subscription_licenses.valid_upto','>',date('Y-m-d'))
                                     ->orWhere('accomodations.payment_type', 2);
                         })
                         ->when($searchTerm['label'], function($query) use ($searchTerm){
                           return $query->where('accomodations.label_id',$searchTerm['label']);
                         })
                         ->when($searchTerm['country'], function($query) use ($searchTerm){
                           return $query->where('accomodation_addresses.country_id',$searchTerm['country']);
                         })
                         ->when($searchTerm['region'], function($query) use ($searchTerm){
                           return $query->where('accomodation_addresses.region_id',$searchTerm['region']);
                         })
                         ->when($searchTerm['department'], function($query) use ($searchTerm){
                           return $query->where('accomodation_addresses.department_id',$searchTerm['department']);
                         })
                         ->when($searchTerm['typeId'], function($query) use ($searchTerm){
                           return $query->whereIn('accomodations.accomodation_type_id',[$searchTerm['typeId']]);
                         })
                         ->when($searchTerm['noofperson'], function($query) use ($searchTerm){
                           return $query->where('accomodations.number_of_persons','<=',$searchTerm['noofperson']);
                         })
                         ->when($searchTerm['keywords'], function($query) use ($searchTerm){
                           return $query->whereRaw("MATCH(accomodations.name,accomodations.description) AGAINST( '".$searchTerm['keywords']."' IN BOOLEAN MODE)");
                         })
                         ->when($searchTerm['price'], function($query) use ($searchTerm){
                           $price=explode('-',$searchTerm['price']);
                           return $query->whereBetween('accomodation_prices.amount',[$price[0],$price[1]]); 
                         })
                         ->when($searchTerm['search_end_date'], function($query) use ($searchTerm){
                           return $query->whereBetween('accomodation_prices.start_date',[$searchTerm['search_start_date'],$searchTerm['search_end_date']]); 
                         })
                         ->when($searchTerm['amnityArr'], function($query) use ($searchTerm){
                           return $query->whereIn('accomodation_searchtags.searchtag_id',[$searchTerm['amnityArr']]);
                         })
                         //->whereNotIn('accomodations.id',$promotions['ids'])
                         ->groupBy('accomodations.id')
                         ->get();
                    //$accomsArr=$accomodations->toArray();
          $finalArr=[];
          $finalArr['type']='FeatureCollection';
          $c=0;
          // dd($accoms);
          foreach($accoms as $a) {
            /*dump($a);
            dump($a->longitude);
            dump($a->lattitude);*/
            //if(!is_null($a->longitude) && !is_null($a->lattitude)){
                $finalArr['features'][$c]['type']='Feature';
                $finalArr['features'][$c]['geometry']['type']='Point'; 
                //[$a->longitude,$a->lattitude]
               
                $finalArr['features'][$c]['geometry']['coordinates']=[$a->longitude,$a->lattitude];
               
                $finalArr['features'][$c]['properties']['propertyId']=$a->id;
                $finalArr['features'][$c]['properties']['propertyName']=$a->name;
                $finalArr['features'][$c]['properties']['countryName']=$a->country_name?$a->country_name:'';
                $finalArr['features'][$c]['properties']['regionName']=$a->region_name?$a->region_name:'';
                $finalArr['features'][$c]['properties']['propertyImage']=$a->featured_image;
                $finalArr['features'][$c]['properties']['propertySlug']=$a->slug;
                if(!$a->featured_image){
                   $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
                   if($img){
                      $finalArr['features'][$c]['properties']['propertyImage']=$img->thumb_image;
                   }
                }
            //}
            $c++;
            
          }
          //die;
          echo json_encode($finalArr);die;  
  }
  public function getCords(Request $r){  
      //dd($r); 
   		$accoms=DB::table('accomodations')->select(
            'accomodations.id','accomodations.name','accomodations.featured_image','accomodations.slug',
            'accomodation_addresses.lattitude','accomodation_addresses.longitude','countries.name as country_name',
            'regions.region_name'
            )
         ->leftJoin('accomodation_addresses', 'accomodation_addresses.accom_id', '=', 'accomodations.id') 
         ->leftJoin('countries', 'accomodation_addresses.country_id','=','countries.id')
         ->leftJoin('regions', 'accomodation_addresses.region_id','=','regions.id')
         ->get();
   		$finalArr=[];
   		$finalArr['type']='FeatureCollection';
   		$c=0;
   		foreach($accoms as $a) {
   			$finalArr['features'][$c]['type']='Feature';
   			$finalArr['features'][$c]['geometry']['type']='Point';
   			$finalArr['features'][$c]['geometry']['coordinates']=[$a->longitude,$a->lattitude];
        $finalArr['features'][$c]['properties']['propertyId']=$a->id;
   			$finalArr['features'][$c]['properties']['propertyName']=$a->name;
        $finalArr['features'][$c]['properties']['countryName']=$a->country_name;
        $finalArr['features'][$c]['properties']['regionName']=$a->region_name;
   			$finalArr['features'][$c]['properties']['propertyImage']=$a->featured_image;
        $finalArr['features'][$c]['properties']['propertySlug']=$a->slug;
        if(!$a->featured_image){
           $img=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first();
           if($img){
              $finalArr['features'][$c]['properties']['propertyImage']=$img->thumb_image;
           }
        } 
   			$c++;
   		}
   		echo json_encode($finalArr);die; 
  }
  public function cleanDB(){
    DB::table('users')->delete();
    DB::table('accomodations')->delete();
    DB::table('accomodation_prices')->delete();
    DB::table('rooms')->delete();
    DB::table('room_prices')->delete();
    DB::table('accomodation_price_extra_charges')->delete();
    DB::table('accomodation_addresses')->delete();
    DB::table('accomodation_searchtags')->delete();
    DB::table('extra_tabs')->delete();
    DB::table('accomodation_awards')->delete();
    DB::table('gallery_images')->delete();
    DB::table('subscriptions')->delete();
    DB::table('subscription_instances')->delete();
    DB::table('subscription_payments')->delete();
    DB::table('subscription_licenses')->delete();
    DB::table('products')->delete();
    DB::table('product_instances')->delete();
    DB::table('product_licenses')->delete();
    DB::table('product_payments')->delete();
    DB::table('messages')->delete();
    DB::table('message_replies')->delete(); 
    DB::table('accomodation_lastminutes')->delete(); 
    DB::table('accomodation_news')->delete();
    dd('CLEANED');
  }

  public function test2(){
    // lastminutes,news,messages
    $promos=DB::table('old_lbv.promos')->get();
    foreach ($promos as $p) {
      $content=DB::table('old_lbv.lastminutes')->wherePromoId($p->id)->first();
      DB::table('accomodation_lastminutes')->insert([
          'id'=>$p->id,
          'accom_id'=>$p->accom_id,
          //'title'=>
          'description'=>$content?$content->promo_content:'',
          'start_from'=>$p->promo_start,
          'valid_upto'=>$p->promo_end,
          'product_license_id'=>$p->product_license_id,
          'created_at'=>$p->created,
          'updated_at'=>date('Y-m-d H:i:s') 
      ]);
    }
    $news=DB::table('old_lbv.acc_news')->get();
    foreach ($news as $n) {
        DB::table('accomodation_news')->insert([
            'id'=>$n->id,
            'accom_id'=>$n->accom_id,
            'title'=>$n->news_title,
            'description'=>$n->news_description,
            'start_from'=>$n->news_startdate,
            'valid_upto'=>$n->news_enddate,
            'product_license_id'=>$n->product_license_id,
            'created_at'=>$n->created,
            'updated_at'=>date('Y-m-d H:i:s') 
        ]);
    }

    $messages=DB::table('old_lbv.contact_tickets')->get();
    if($messages){
        foreach ($messages as $m) {
              DB::table('messages')->insert([
                  'id'=>$m->id,
                  'message'=>$m->message, 
                  'to_user'=>$m->to_user,
                  'from_user'=>$m->from_user,
                  'sender_name'=>$m->sender_name,
                  'accom_id'=>$m->accom_id,
                  'approved'=>$m->approved,
                  'seen_by_adv'=>$m->seen_by_adv,
                  'seen_by_cust'=>$m->seen_by_cust,
                  'created_at'=>$m->created,
                  'updated_at'=>date('Y-m-d H:i:s') 
              ]);
              $replies=DB::table('old_lbv.contact_ticket_replies')->whereContactTicketId($m->id)->get();
              if($replies){
                foreach ($replies as $r) {
                    DB::table('message_replies')->insert([
                      'id'=>$r->id,
                      'message_id'=>$m->id,
                      'accom_id'=>$m->accom_id,
                      'reply'=>$r->reply_message,
                      //'sender_name'=>
                      'approved'=>$r->approved,
                      'mail_to'=>$r->mail_to,
                      'user_id'=>$r->user_id,
                      'seen_by_adv'=>$r->seen_by_adv,
                      'seen_by_cust'=>$r->seen_by_cust,
                      'sent_by'=>$r->sent_by,
                      'sent_to'=>$r->sent_to,
                      'created_at'=>$r->created,
                      'updated_at'=>date('Y-m-d H:i:s') 
                    ]);
                }
              }
        }
    }
 
  }

  // subscription and product licenses
 /* public function test2(){
    $subscriptions=DB::table('old_lbv.sale_rent_subscriptions')->get();
    if($subscriptions){
      foreach ($subscriptions as $s) {
          DB::table('subscriptions')->insert([
              'id'=>$s->id,
              'name'=>$s->subscription_name,
              'description'=>$s->subscription_desc,
              'amount'=>$s->amount,
              'type_id'=>$s->subscription_type_id,
              'duration'=>$s->duration,
              'duration_type'=>$s->duration_type,
              'created_at'=>$s->created,
              'updated_at'=>date('Y-m-d H:i:s') 
          ]); 
      }
    } 
     $subInstances=DB::table('old_lbv.subscription_instances')->get();
     if($subInstances){
          foreach ($subInstances as $sI) {
              $startP=DB::table('old_lbv.subscription_payments')->select('id')->whereSubscriptionInstanceId($sI->id)->first();
              if($startP){
                $startEnd=DB::table('old_lbv.subscription_licenses')->whereSubscriptionPaymentId($startP->id)->first();
              }
              // dump($startEnd);
              $instance=DB::table('subscription_instances')->insertGetId([
                  'id'=>$sI->id,
                  'subscription_id'=>$sI->sale_rent_subscription_id,
                  'accom_id'=>$startEnd?$startEnd->accom_id:null,
                  'start_from'=>$startEnd?$startEnd->created:null,
                  'valid_upto'=>$startEnd?$startEnd->valid_upto:null,
                  'amount'=>$sI->amount,
                  'created_at'=>$sI->created,
                  'updated_at'=>date('Y-m-d H:i:s')
              ]);
          }
     }
    // die;

     $subPayments=DB::table('old_lbv.subscription_payments')->get();
     if($subPayments){
          foreach ($subPayments as $sP) {
              $payment=DB::table('subscription_payments')->insertGetId([
                  'id'=>$sP->id,
                  'subscription_instance_id'=>$sP->subscription_instance_id,
                  'user_id'=>$sP->user_id,
                  'amount_paid'=>$sP->amount_paid,
                  'cancelled_amount'=>$sP->cancelled_amount,
                  'discount'=>$sP->discount,
                  'pay_method_id'=>$sP->pay_method_id,
                  'payment_complete'=>$sP->payment_complete,
                  'sub_transaction_id'=>$sP->sub_transaction_id,
                  'pay_json'=>$sP->pay_json,
                  'paypal_ipn_response'=>$sP->paypal_ipn_response,
                  'paypal_ipn_response_id'=>$sP->paypal_ipn_response_id,
                  'ipn_verified'=>$sP->ipn_verified,
                  'created_at'=>$sP->created,
                  'updated_at'=>date('Y-m-d H:i:s')
              ]);
          }
     }
     $subLicense=DB::table('old_lbv.subscription_licenses')->get();
     if($subLicense){
          foreach ($subLicense as $sL) {
              $license=DB::table('subscription_licenses')->insertGetId([
                  'id'=>$sL->id,
                  'subscription_payment_id'=>$sL->subscription_payment_id,
                  'start_from'=>$sL->created,
                  'valid_upto'=>$sL->valid_upto,
                  'accom_id'=>$sL->accom_id,
                  'subscription_key'=>$sL->subscription_key,
                  'invoice_number'=>$sL->invoice_number,
                  'order_number'=>$sL->order_number,
                  'user_id'=>$sL->user_id,
                  'license_renewed'=>$sL->license_renewed,
                  'four_notified'=>$sL->four_notified,
                  'two_notified'=>$sL->two_notified,
                  'one_notified'=>$sL->one_notified,
                  'after_expire'=>$sL->after_expire,
                  'token'=>$sL->token,
                  'tmp_valid_upto'=>$sL->tmp_valid_upto,
                  'created_at'=>$sL->created,
                  'updated_at'=>date('Y-m-d H:i:s')
              ]);
          }
     }
     $products=DB::table('old_lbv.products')->get();
     if($products){
          foreach ($products as $p) {
               DB::table('products')->insert([
                  'id'=>$p->id,
                  'product_category_id'=>$p->product_category_id,
                  'product_name'=>$p->product_name,
                  'product_description'=>$p->product_description,
                  'product_price'=>$p->product_price,
                  'product_month'=>$p->product_month,
                  'product_year'=>$p->product_year,
                  'created_at'=>$p->created,
                  'updated_at'=>date('Y-m-d H:i:s')
              ]);
          }
      }
      $proInstances=DB::table('old_lbv.product_instances')->get();
      foreach ($proInstances as $p) {
          $instanceAccomID=DB::table('old_lbv.product_licenses')->select('accom_id')->whereProductInstanceId($p->id)->first();
          DB::table('product_instances')->insert([
              'id'=>$p->id,
              'product_id'=>$p->product_id,
              'product_category_id'=>$p->product_category_id,
              'accom_id'=>$instanceAccomID?$instanceAccomID->accom_id:null,
              'product_payment_id'=>$p->product_payment_id,
              'product_price'=>$p->product_price,
              'cancelled_amount'=>$p->cancelled_amount,
              'created_at'=>$p->created,
              'updated_at'=>date('Y-m-d H:i:s')
          ]);
      }
     
      $proLicense=DB::table('old_lbv.product_licenses')->get();
       foreach ($proLicense as $p) {
          $instancePaymentID=DB::table('old_lbv.product_instances')->select('product_payment_id')->whereId($p->product_instance_id)->first();
          $proUserId=DB::table('old_lbv.product_payments')->select('user_id')->whereId($instancePaymentID->product_payment_id)->first();
          DB::table('product_licenses')->insert([
               'id'=>$p->id,
               'user_id'=>$proUserId?$proUserId->user_id:null,
               'product_payment_id'=>$instancePaymentID->product_payment_id,
               'product_instance_id'=>$p->product_instance_id,
               'accom_id'=>$p->accom_id,
               'valid_from'=>$p->valid_from,
               'valid_upto'=>$p->valid_upto,
               'discarded'=>$p->discarded,
               'product_month'=>$p->product_month,
               'product_year'=>$p->product_year,
               'created_at'=>$p->created,
               'updated_at'=>date('Y-m-d H:i:s')

           ]);
       }
       $proPayments=DB::table('old_lbv.product_payments')->get();
         foreach ($proPayments as $p) {
             DB::table('product_payments')->insert([
                 'id'=>$p->id,
                 'invoice_number'=>$p->invoice_num,
                 'transaction_id'=>$p->transaction_id,
                 'transaction_json'=>$p->transaction_json,
                 'user_id'=>$p->user_id,
                 'payment_complete'=>$p->payment_status,
                 'total_amount'=>$p->total_amount,
                 'cancelled_amount'=>$p->cancelled_amount,
                 'discount'=>$p->discount,
                 'pay_method_id'=>$p->pay_method_id,
                 'invoice_key'=>$p->invoice_key,
                 'paypal_ipn_response'=>$p->paypal_ipn_response,
                 'paypal_ipn_response_id'=>$p->paypal_ipn_response_id,
                 'ipn_verified'=>$p->ipn_verified,
                 'created_at'=>$p->created,
                 'updated_at'=>date('Y-m-d H:i:s') 
             ]);
         }
  }*/

  public function test(){ 

    $oldUsers=DB::table('old_lbv.users')->limit(5)->get(); 
    // dump($oldUsers);
    if($oldUsers){ 
        foreach($oldUsers as $u){
            $userAddress=DB::table('old_lbv.user_addresses')->whereId($u->user_address_id)->first();
            $newUser=DB::table('users')->insertGetId([
                'id'=>$u->id,
                'role_id'=>$u->role_id,
                'created_at'=>$u->created,
                'updated_at'=>date('Y-m-d H:i:s'),
                'first_name'=>$u->first_name,
                'last_name'=>$u->sur_name,
                'email'=>$u->email,
                'btw_number'=>$u->uservat,
                'user_company'=>$u->user_company,
                'phone_number'=>$u->phone,
                'country_id'=>$userAddress->country_id,
                'city'=>$userAddress->city,
                'street'=>$userAddress->street,
                'street_number'=>$userAddress->street_number,
                'post_code'=>$userAddress->zip,
                'complete_profile'=>$u->complete_profile,
                'is_verified'=>1
            ]);
            if($u->role_id==3){
              $userAccoms=DB::table('old_lbv.accoms')->whereUserId($u->id)->get();
              if($userAccoms){
                 foreach($userAccoms as $a){
                    $accomLable=DB::table('old_lbv.acc_labels')->whereAccomId($a->id)->first();
                    $accom=DB::table('accomodations')->insertGetId([
                        'id'=>$a->id,
                        'user_id'=>$a->user_id,
                        'name'=>$a->accom_name,
                        'created_at'=>$a->created,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'approved'=>$a->approved,
                        'completed'=>1,
                        'label_id'=>$accomLable->label_id,
                        'accomodation_type_id'=>$a->acc_type_id,
                        'payment_type'=>$a->accom_payment_type,
                        'subs_valid_license'=>$a->subs_valid_license,
                        'website'=>$a->accom_website,
                        'email'=>$a->accom_email,
                        'description'=>$a->accom_description,
                        'number_of_persons'=>$a->accom_number_persons,
                        'deletion_request'=>$a->deletion_request,
                        'featured_image'=>$a->featured_image,
                        'editable'=>$a->editable,
                        'is_slider'=>$a->is_slider,
                        'reservation_active'=>$a->reservation_active,
                        'slug'=>$a->accom_slug
                    ]); 
                    $accomPrices=DB::table('old_lbv.acc_prices')->whereAccomId($a->id)->get();
                    if($accomPrices){
                        foreach ($accomPrices as $p) {
                          $unit='perweek';
                          if($p->price_unit_id==1){
                            $unit='perweek';
                          }else if($p->price_unit_id==2){
                            $unit='perroom';
                          }else {
                            $unit='perperson';
                          }
                          $price=DB::table('accomodation_prices')->insertGetId([
                              'accom_id'=>$a->id,
                              'created_at'=>$p->created,
                              'updated_at'=>date('Y-m-d H:i:s'),
                              'price_unit_id'=>$p->price_unit_id,
                              'price_unit'=>$unit,
                              'amount'=>$p->price_amount,
                              'max_persons'=>$p->min_persons,
                              'min_days'=>$p->min_days,
                              'start_date'=>$p->price_period_start,
                              'end_date'=>$p->price_period_ends,
                              'changeday'=>$p->change_day,
                              'extra_price_per_person'=>$p->extra_price
                          ]);
                        }
                        $rooms=DB::table('old_lbv.rooms')->whereAccomId($a->id)->get();
                        if($rooms){
                            foreach ($rooms as $r) {
                              $room=DB::table('rooms')->insertGetId([
                                  'accom_id'=>$a->id,
                                  //'accom_price_id'=>
                                  'room_type'=>$r->room_type,
                                  'description'=>$r->description,
                                  'adults'=>$r->adults,
                                  'childs'=>$r->childs,
                                  'max_child_age'=>$r->child_age,
                                  'no_of_rooms'=>$r->room,
                                  'total_adults'=>$r->total_adults,
                                  'total_childs'=>$r->total_childs,
                                  'min_days'=>'2',
                                  'max_adults_per_room'=>$r->max_adults,
                                  'max_persons_per_room'=>$r->max_persons,
                                  'created_at'=>$r->created,
                                  'updated_at'=>date('Y-m-d H:i:s')
                              ]);
                              // $accomPrices->first()->min_days
                              $roomPrice=DB::table('old_lbv.room_prices')->whereRoomId($r->id)->get();
                              if($roomPrice){
                                foreach ($roomPrice as $rP) {
                                  $rPrice=DB::table('room_prices')->insertGetId([
                                    'room_id'=>$room,
                                    'accom_id'=>$a->id,
                                    'start_date'=>$rP->start_date,
                                    'end_date'=>$rP->end_date,
                                    'price'=>$rP->price,
                                    'price1'=>$rP->price1,
                                    'price2'=>$rP->price2,
                                    'price3'=>$rP->price3,
                                    'extra_price_adult'=>$rP->extra_price_adult,
                                    'extra_price_child'=>$rP->extra_price_child,
                                    'created_at'=>$rP->created,
                                    'updated_at'=>date('Y-m-d H:i:s')
                                  ]);
                                }
                              }
                            }
                        }
                    }
                    $extraPrices=DB::table('old_lbv.acc_price_extra_charges')->whereAccomId($a->id)->get();
                    if($extraPrices){
                        foreach ($extraPrices as $e) {
                          $extra=DB::table('accomodation_price_extra_charges')->insertGetId([
                              'accom_id'=>$a->id,
                              'amount'=>$e->amount,
                              'description'=>$e->description,
                              'is_optional'=>$e->is_optional,
                              'charge_type'=>$e->optional_charge_type, 
                              'is_per_person'=>$e->is_per_person,
                              'created_at'=>$e->created,
                              'updated_at'=>date('Y-m-d H:i:s')
                          ]);
                        }
                    }
                    $accomLocation=DB::table('old_lbv.acc_addresses')->whereId($a->acc_address_id)->first();
                    if($accomLocation){
                      $location=DB::table('accomodation_addresses')->insertGetId([
                        'accom_id'=>$a->id,
                        'country_id'=>$accomLocation->country_id,
                        'region_id'=>$accomLocation->region_id,
                        'department_id'=>$accomLocation->department_id,
                        'street'=>$accomLocation->addr_street,
                        'street_number'=>$accomLocation->addr_street_number,
                        'city'=>$accomLocation->addr_city,
                        'postal_code'=>$accomLocation->addr_zip,
                        'lattitude'=>$accomLocation->addr_longitude,
                        'longitude'=>$accomLocation->addr_lattitude,
                        'created_at'=>$accomLocation->created,
                        'updated_at'=>date('Y-m-d H:i:s')
                      ]);
                    }
                    $accomAmnities=DB::table('old_lbv.accom_searchtags')->whereAccomId($a->id)->get();
                    if($accomAmnities){
                        foreach ($accomAmnities as $am) {
                          $searchTag=DB::table('accomodation_searchtags')->insertGetId([
                              'accom_id'=>$a->id,
                              'searchtag_id'=>$am->searchtag_id,
                              'created_at'=>date('Y-m-d H:i:s'),
                              'updated_at'=>date('Y-m-d H:i:s')
                          ]);
                        }
                    }
                    $accomExtraTabs=DB::table('old_lbv.extra_tabs')->whereAccomId($a->id)->get();
                    if($accomExtraTabs){
                      foreach ($accomExtraTabs as $et) {
                        $extra=DB::table('extra_tabs')->insertGetId([
                            'accom_id'=>$a->id,
                            'page_name'=>$et->page_name,
                            'tab_content'=>$et->tab_content,
                            'status'=>$et->status,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                      }
                    }
                    $accomAwards=DB::table('old_lbv.acc_awards')->whereAccomId($a->id)->get();
                    foreach ($accomAwards as $aw) {
                      $award=DB::table('accomodation_awards')->insertGetId([
                          'accom_id'=>$a->id,
                          'award_id'=>$aw->award_id,
                          'approved'=>$aw->approved,
                          'deletion_request'=>$aw->deletion_request,
                          'created_at'=>$aw->created,
                          'updated_at'=>date('Y-m-d H:i:s')
                      ]);
                    }
                    $galImages=DB::table('old_lbv.gallery_images')->whereAccomId($a->id)->get();
                    if($galImages){
                      foreach ($galImages as $g) {
                        $gal=DB::table('gallery_images')->insertGetId([
                            'accom_id'=>$a->id,
                            'image'=>$g->main_img,
                            'thumb_image'=>$g->search_img,
                            'slider_image'=>$g->slider_img,
                            'sliderbig_image'=>$g->slider_big_image,
                            'created_at'=>$g->created,
                            'updated_at'=>date('Y-m-d H:i:s') 
                        ]);
                      }
                    }
                 }
              }
            }
        }
        dd('COMPLETED');
    }
    // SUBSCRIPTIONS and promotions & Messages goes here... 
  }





}
