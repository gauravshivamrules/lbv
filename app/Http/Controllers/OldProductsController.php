<?php

namespace LBV\Http\Controllers;

use DB;
use PDF;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables; 
use Illuminate\Support\Facades\Auth;

class OldProductsController extends Controller
{
    
    public function getAllProducts(){
    	return view('old_products.all');
    }
    public function getAllProductsAjax(Request $r){
    	$products=DB::table('old_products')->where('subscription_name', 'NOT LIKE', '%'.'Gratis'.'%')->select('old_products.*')->get();
    	return Datatables::of($products) 
    	->addColumn('action', function ($a) {    
    		return '<a href="/old-products/generate-invoice/'.$a->order_number.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>';  
    		
    	})  
    	->escapeColumns([])
    	->make(true); 
    }
    public function getAdvertiserProducts(){
    	return view('old_products.my_products');
    }
    public function getAdvertiserProductsAjax(){
    	$products=DB::table('old_products')
    			->where('user_id',Auth::user()->id)
    			->where('subscription_name', 'Not like', '%' .'Gratis'.'%')
    			->select('old_products.*')
    			->get();
    	return Datatables::of($products) 
    	->addColumn('action', function ($a) {    
    		return '<a href="/old-products/generate-invoice/'.$a->order_number.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>';  
    	}) 
    	->escapeColumns([])
    	->make(true); 
    }
    public function generateOldInvoice($id) { 
        if($id) {
            $chunks=explode('.', $id);
            if($chunks) {
                $invoice=$this->buildOldInvoice($chunks[0]);  
                return $invoice->stream();   
            }
        }
    }
    public function buildOldInvoice($id=null) {
        $returnArr=[];
        if($id) {
            $invoice=DB::table('old_products')
                ->leftJoin('users','old_products.user_id','=','users.id')
                ->select('old_products.*','users.first_name','users.last_name','users.city','users.email','users.user_company',
                       'users.country_id','users.phone_number','users.street','users.street_number','users.post_code','users.btw_number'
                    )
                ->where('order_number', $id)
                ->get();
            $totalAmount=DB::table('old_products')->select( DB::raw('SUM(amount) as totalAmount'))->where('order_number',$id)->get();
            $invoice->first()->invoice_date=date('d-m-Y',strtotime($invoice->first()->invoice_date));

            if(is_null($invoice->first()->method) ) {
                $invoice->first()->method='Old Payment Method';
            }
            $totalAmount=$totalAmount->first()->totalAmount;
            $file= PDF::loadView('old_products.pdf.invoice',compact('invoice','totalAmount')); 
            return $file;
        } else {
            return false;
        }  
    }
} 
