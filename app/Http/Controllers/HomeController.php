<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use LBV\Model\Terms; 
use LBV\Model\Privacy;
use LBV\Mail\SendContactRequest;
use Illuminate\Http\Request;
use LBV\Model\ProductLicense; 

class HomeController extends Controller
{
    
    public function home(Request $r){  
        $tipAccom=[];
        $license=$this->giveProductLicense([
                        ['product_licenses.product_year',date('Y')],
                        ['product_licenses.product_month',date('n')],
                        ['product_instances.product_category_id',1]
                    ],[1,3]);
        if(!$license->isEmpty()) {
            $ids=[];
            foreach($license as $sL) {
                $ids[]=$sL->accomId;
            }
            $tipAccom=$this->getAccomodationDetails($ids);
        }
        // dd($tipAccom);
        $spotLicenses=$this->giveProductLicense([
                        ['product_licenses.product_year',date('Y')],
                        ['product_licenses.product_month',date('n')],
                        ['product_instances.product_category_id',2]
                    ],[1,3]);
        $ids=[];
        foreach($spotLicenses as $sL) {
            $ids[]=$sL->accomId;
        }
        $spotAccoms=$this->getAccomodationDetailsSpot($ids);
    	return view('home.home',compact('tipAccom','spotAccoms'));      
    } 
    public function getNews(){
        $news=[];
        $license=$this->giveProductLicense([
                        ['product_licenses.valid_from','<=',date('Y-m-d H:i:s')],
                        ['product_licenses.valid_upto','>=',date('Y-m-d H:i:s')],
                        ['product_instances.product_category_id',3]
                    ],[1,3]);
        if($license){
            $ids=[];
            foreach ($license as $l) {
                $ids[]=$l->accomId; 
            }
            $n=DB::table('accomodation_news')->whereIn('accomodation_news.accom_id',$ids)
                ->select('countries.name as cName','regions.region_name as rName','accomodations.id','accomodations.name','accomodations.slug','accomodations.featured_image',
                    'accomodation_news.title','accomodation_news.description')
                ->leftJoin('accomodations','accomodation_news.accom_id','=','accomodations.id')
                ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                ->leftJoin('countries','countries.id','=','accomodation_addresses.country_id')
                ->leftJoin('regions','regions.id','=','accomodation_addresses.region_id')
                ->where('start_from','<=',date('Y-m-d H:i:s'))
                ->where('valid_upto','>=',date('Y-m-d H:i:s'))
                ->get();
            $news=$n->toArray();
            $c=0;
            foreach ($news as $n) {
                if(!$n->featured_image) {
                    $img=DB::table('gallery_images')->whereAccomId($n->id)->first();
                    if($img){
                        $news[$c]->featured_image=$img->thumb_image;
                    }
                    $c++;
                }
            }
        }
        return view('home.news',compact('news')); 
    } 
    public function getLastMinutes(){
        $minutes=[]; 
        $license=$this->giveProductLicense([
                        ['product_licenses.valid_from','<=',date('Y-m-d H:i:s')],
                        ['product_licenses.valid_upto','>=',date('Y-m-d H:i:s')],
                        ['product_instances.product_category_id',5]
                    ],[1,3]); 
        if($license){
            $ids=[];
            foreach ($license as $l) {
                $ids[]=$l->accomId; 
            }
            $n=DB::table('accomodation_lastminutes')->whereIn('accomodation_lastminutes.accom_id',$ids)
                ->select('countries.name as cName','regions.region_name as rName','accomodations.id','accomodations.name','accomodations.slug','accomodations.featured_image',
                    'accomodation_lastminutes.title','accomodation_lastminutes.description')
                ->leftJoin('accomodations','accomodation_lastminutes.accom_id','=','accomodations.id')
                ->leftJoin('accomodation_addresses','accomodation_addresses.accom_id','=','accomodations.id')
                ->leftJoin('countries','countries.id','=','accomodation_addresses.country_id')
                ->leftJoin('regions','regions.id','=','accomodation_addresses.region_id')
                ->where('start_from','<=',date('Y-m-d H:i:s'))
                ->where('valid_upto','>=',date('Y-m-d H:i:s'))
                ->get();
            $minutes=$n->toArray();
            $c=0;
            foreach ($minutes as $n) {
                if(!$n->featured_image) {
                    $img=DB::table('gallery_images')->whereAccomId($n->id)->first(); 
                    if($img){
                        $minutes[$c]->featured_image=$img->thumb_image;
                    }
                    $c++;
                }
            }
        }  
        return view('home.last_minute',compact('minutes')); 
    }
    public function getAdvertisers(){
        $data=DB::table('subscriptions')->select('name','amount','duration','duration_type','type_id')->get();
        return view('home.advertisers',compact('data'));
    }
    public function getContactUs(){  
        return view('home.contact_us');
    }
    public function refreshCaptcha(){
        return response()->json(['captcha'=> captcha_img()]);
    }
    public function postContactUs(Request $r){
        $this->validate($r,[
                        'email'=>'required|email',
                        'name'=>'required',
                        'message'=>'required|max:255',
                        'captcha' => 'required|captcha'
                    ],[
                        'email.required'=>__('Email is required.'),
                        'email.email'=>__('Enter Valid Email.'),
                        'name.required'=>__('Name is required.'),
                        'message.required'=>__('Message is required.'),
                        'message.max'=>__('Message should be less that 255 characters.'),
                        'captcha.captcha'=>'Invalid captcha code.'
                    ]
        ); 
        $contact=DB::table('contacts')->insertGetId([
            'name'=>$r->name,
            'email'=>$r->email,
            'message'=>$r->message,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if($contact){
            $admins=DB::table('users')->select('email')->whereRoleId(1)->get();
            foreach($admins as $a){
                try {
                    Mail::to($a->email)->send(new SendContactRequest('New contact request has been received','New contact request has been received with the following details',
                        $r->name,$r->email,$r->message
                    ));     
                } catch(Exception $e) {
                    \Log::log('sendPasswordResetLink','Mail not sent to '.$email.' Server returned error: '.$e->getMessage()); 
                    return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
                }
            }
            return redirect()->back()->with('success',__('Request sent successfully,we will contact you soon'));
        }
        return redirect()->back()->with('error',__('Sorry,request could not be sent,please try again!'));
    }
    public function getAccomodationDetails($accoms=[]){
        $returnArr=[];
        $property=DB::table('accomodations')->select('id','name','address_id','featured_image','slug')
                    ->whereIn('id',$accoms)->get();
        $acms=$property->toArray();
        if($acms) {
            $c=0;
            foreach ($acms as $a) {
                if($a->featured_image) {
                    $gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->whereThumbImage('thumb_'.$a->featured_image)->first();
                    $addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
                    $country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
                    $region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
                    $returnArr['accom'][]=$a;
                    $returnArr['accom'][$c]->country=$country->name;
                    $returnArr['accom'][$c]->region=$region->region_name;
                    $price=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                    $returnArr['accom'][$c]->price=$price;
                    if($gal) {
                        $returnArr['accom'][$c]->thumb=$gal->thumb_image; 
                    } else {
                        $returnArr['accom'][$c]->thumb=''; 
                    }
                } else {
                    $gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first(); 
                    $addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
                    $country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
                    $region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
                    $returnArr['accom'][]=$a; 
                    $returnArr['accom'][$c]->country=$country->name;
                    $returnArr['accom'][$c]->region=$region->region_name;
                    $price=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                    $returnArr['accom'][$c]->price=$price; 
                    if($gal) {
                        $returnArr['accom'][$c]->thumb=$gal->thumb_image;    
                    } else {
                        $returnArr['accom'][$c]->thumb=''; 
                    }
                }
                $c++;   
            }
            
        }
        return $returnArr;
    } 
    public function getAccomodationDetailsSpot($accoms=[]){
        $returnArr=[];
        $property=DB::table('accomodations')->select('id','name','address_id','featured_image','slug')
                    ->whereIn('id',$accoms)->get();
        $acms=$property->toArray();
        if($acms) {
            $c=0;
            foreach ($acms as $a) {
                if($a->featured_image) {
                    $gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->whereThumbImage('thumb_'.$a->featured_image)->first();
                    $addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
                    $country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
                    $region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
                    $returnArr['accom'][]=$a;
                    $returnArr['accom'][$c]->country=$country->name;
                    $returnArr['accom'][$c]->region=$region->region_name;
                    $price=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                    $returnArr['accom'][$c]->price=$price;
                    if($gal) {
                        $returnArr['accom'][$c]->thumb=$gal->thumb_image; 
                    } else {
                        $returnArr['accom'][$c]->thumb=''; 
                    }
                } else {
                    $gal=DB::table('gallery_images')->select('thumb_image')->whereAccomId($a->id)->first(); 
                    $addressId=DB::table('accomodation_addresses')->whereAccomId($a->id)->first();
                    $country=DB::table('countries')->select('name')->whereId($addressId->country_id)->first();
                    $region=DB::table('regions')->select('region_name')->whereId($addressId->region_id)->first(); 
                    $returnArr['accom'][]=$a; 
                    $returnArr['accom'][$c]->country=$country->name;
                    $returnArr['accom'][$c]->region=$region->region_name;
                    $price=DB::table('accomodation_prices')->whereAccomId($a->id)->min('amount');
                    $returnArr['accom'][$c]->price=$price; 
                    if($gal) {
                        $returnArr['accom'][$c]->thumb=$gal->thumb_image;    
                    } else {
                        $returnArr['accom'][$c]->thumb=''; 
                    }
                }
                $c++;   
            } 
        }
        return $returnArr;
    } 
    public function giveProductLicense($where=array(),$whereIn=array()) {
        if($where) {
            if($whereIn) {
                $licenses=ProductLicense::where($where)->whereIn('product_payments.payment_complete',$whereIn)
                    ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                    ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                    ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                    ->leftJoin('products','products.id','=','product_instances.product_id')
                    ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','product_licenses.user_id','=','users.id')
                    ->select(
                        'accomodations.name as aName','accomodations.id as accomId','accomodations.slug as aSlug','accomodations.featured_image as featured_image','product_payments.payment_complete'
                    )
                    ->orderBy('product_licenses.id','desc')
                    ->get(); 
            } else {
                $licenses=ProductLicense::where($where) 
                    ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                    ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                    ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                    ->leftJoin('products','products.id','=','product_instances.product_id')
                    ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','product_licenses.user_id','=','users.id')
                   ->select(
                                          'accomodations.name as aName','accomodations.id as accomId','accomodations.slug as aSlug','accomodations.featured_image as featured_image','product_payments.payment_complete'
                                      )
                    ->orderBy('product_licenses.id','desc')  
                    ->get(); 
            }
            return $licenses;
        } else {
            return false;
        }
    }
    public function tos(){ 
        $terms=Terms::first();
        return view('home.tos',compact('terms'));  
    }
    public function privacy(){
        $privacy=Privacy::first();
        return view('home.privacy',compact('privacy'));
    }  
    public function siteVisits(Request $r){ 
        $info=@unserialize(file_get_contents('http://ip-api.com/php/'.$r->ip()));  
        DB::table('site_visits')->insert([
            'count'=>1,
            'ip'=>$r->ip(),
            'json'=>json_encode($info), 
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
