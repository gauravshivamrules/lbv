<?php
namespace LBV\Http\Controllers;

use Illuminate\Http\Request;
use LBV\Model\AdminNotification;
use LBV\Model\UserNotification;
use DB; 
class NotificationsController extends Controller 
{
	public function markSeen(Request $r) {
		if($r->user) {
			if($r->id) {
				if($r->user==1) {
					$notification=AdminNotification::whereId($r->id)->first();
					$notification->seen=1;
					$notification->save();
				}else {
					$notification=UserNotification::whereId($r->id)->first();
					$notification->seen=1;
					$notification->save();
				}
			} 
		}
	}
	public function markSeenMsg(Request $r){
		if($r->id){
			DB::table('message_notifications')->whereId($r->id)->update(['seen'=>1]); 
		}
	}
} 