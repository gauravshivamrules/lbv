<?php
namespace LBV\Http\Controllers;

// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use ThrottlesLogins;
use PDF;
use DB;
use Mail;
use Socialite;
use LBV\User;
use LBV\Model\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use LBV\Http\Requests\UserRequest; 
use LBV\Http\Requests\NewUserRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables; 
use LBV\Mail\SendPasswordResetLink; 
use LBV\Mail\SendVerificationLink;
use Illuminate\Foundation\Auth\AuthenticatesUsers;  

   
class UsersController extends Controller 
{ 
	use AuthenticatesUsers;

	protected $auth;
	protected $redirectTo = '';   
    /**
    * lockoutTime
    *
    * @var
    */
	protected $lockoutTime; 
   /**
    * maxLoginAttempts
    *
    * @var
    */
	protected $maxLoginAttempts;

	public function __construct(Guard $auth) {
	    // $this->middleware('guest', ['except' => 'logout']); 
	    $this->auth = $auth;
	    $this->lockoutTime  = 6;    //lockout for 1 minute (value is in minutes)   
	    $this->maxLoginAttempts = 2;    //lockout after 5 attempts        
	}
	public function index() {
		$users=User::all();	
		return view('users.index',compact('users')); 
	}
	public function login(Request $req) {
		$this->validate($req,[
				'email'=>'required|email',
				'password'=>'required'
			],[
				'email.required'=>__('Email is required.'),
				'email.email'=>__('Enter Valid Email.'),
				'password.required'=>__('Password is required.')
			]
		);
		if ($this->hasTooManyLoginAttempts($req)) {    
	        $this->fireLockoutEvent($req);	 
	        return $this->sendLockoutResponse($req);  
	    }
		if( Auth::attempt(['email'=>$req->input('email'),'password'=>$req->input('password') ]) ) { 
			$user = Auth::user();
			$this->clearLoginAttempts($req);
			if($user->role_id==1) {
				return redirect()->intended('/dashboard');  
			} else if($user->role_id==2) {
				if($user->is_verified){ 
					return redirect()->intended('/dashboard/cust-dashboard');
				}else{
					Auth::logout(); 
					return redirect()->back()->with('error',__('An email has been sent to you,please click on the link on email to verify your account'));
				}
			} else if($user->role_id==3) {
				if($user->is_verified){
					return redirect()->intended('/dashboard/adv-dashboard');  
				}else{
					Auth::logout(); 
					return redirect()->back()->with('error',__('An email has been sent to you,please click on the link on email to verify your account'));
				}
			} 
		}  
		$this->incrementLoginAttempts($req); 
		return redirect('/login')->with('error',__('Oops,Bad email or password!')); 
	}
	public function loginView(Request $req) { 
		$this->validate($req,[
				'email'=>'required|email',
				'password'=>'required'
			],[
				'email.required'=>__('Email is required.'),
				'email.email'=>__('Enter Valid Email.'),
				'password.required'=>__('Password is required.')
			]
		);
		if ($this->hasTooManyLoginAttempts($req)) {    
	        $this->fireLockoutEvent($req);	 
	        return $this->sendLockoutResponse($req);  
	    }
		if( Auth::attempt(['email'=>$req->input('email'),'password'=>$req->input('password') ]) ) {
			$user = Auth::user();
			$this->clearLoginAttempts($req);
			if($user->role_id==2) {
				if($user->is_verified){
					if($req->accom_id){ 
						// return redirect()->intended('/tickets/cust-chat/'.$req->accom_id);    
						return redirect()->intended('/tickets/contact-owner/'.$req->accom_id)->with('success',__('You can chat with owner here'));      
					}
				}else{  
					Auth::logout(); 
					return redirect()->back()->with('error',__('An email has been sent to you,please click on the link on email to verify your account'));
				}
			}else{ 
				return redirect()->back()->with('error',__('Sorry,you are not allowed to login here'));   
			} 
		}  
		$this->incrementLoginAttempts($req); 
		return redirect()->back()->with('error',__('Oops,Bad email or password!')); 
	}
	protected function hasTooManyLoginAttempts(Request $request) {
	    return $this->limiter()->tooManyAttempts(
	         $this->throttleKey($request), $this->maxLoginAttempts, $this->lockoutTime
	    );
	}
	public function logout() {
		Auth::logout(); 
		return redirect('/login');  
	} 
	public function profile($id) {
		if($id) {
			$countries=Country::pluck('name','id');
			$user=User::whereId($id)->firstOrFail();
			return view('users.profile',compact('user','countries'));  
		}
	}
	public function user($id) {
		if($id) {
			$user=User::whereId($id)->first();
			return view('users.user',compact('user'));  
		}
	}  
	public function updateProfile(Request $req,UserRequest $u,$id) {
		$user=User::whereId($id)->firstOrFail();
		$oldImage=$user->image;
		$user->first_name=$req->get('first_name');
		$user->last_name=$req->get('last_name');
		$user->email=$req->get('email'); 
		$user->user_company=$req->get('user_company');
		$user->country_id=$req->get('country_id');
		$user->city=$req->get('city');
		$user->phone_number=$req->get('phone_number');
		$user->street=$req->get('street');
		$user->street_number=$req->get('street_number');
		$user->post_code=$req->get('post_code'); 
		$user->btw_number=$req->get('vat'); 
		if($user->save()) {
			if(Auth::user()->role_id==1) {
				return redirect('/users')->with('success',__('Profile updated successfully'));
			}
			return redirect()->back()->with('success',__('Profile updated successfully'));
			// return redirect()->route('route.name')->with('success',__('Profile updated successfully'));
		} else { 
			return redirect()->back()->with('error',__('Profile could not be updated,Please try again!'));
		} 
	}
	public function updateUserProfile($id,Request $req) {
		$this->validate($req,[ 
				'name'=>'required|max:30',
				'email'=>'required|email|unique:users,id',
				'file'=>'image|mimes:jpeg,png,jpg,gif|max:500'  // IN KB 
			],[  
				'name.required'=>'Name is required.',
				'name.max'=>'Maximum 30 characters',
				'email.required'=>'Email is required.',
				'email.email'=>'Enter valid email.',
				'email.unique'=>'Email already exists.',  
				'file.image'=>'Only jpeg,jpg,png and gif files are allowed.',
				'file.max'=>'Max file size is 500 KB.' 
			] 
		);
		$user=User::whereId($id)->firstOrFail();
		$oldImage=$user->image;
		$user->name=$req->get('name');
		$user->email=$req->get('email'); 
		if(isset($req->file)) {
			if($req->file('file')->getClientSize() >0) {
				$file=$req->file('file');
				$path=pathinfo($req->file('file')->getClientOriginalName());
				$imageName=time().'.'.$path['extension'];
				$destination=public_path().'/images/user_avatars/';
				$file->move($destination,$imageName);
				$user->image=$imageName;
				$FILETODELETE=public_path().'/images/user_avatars/'.$oldImage;
				\File::delete($FILETODELETE);   
			}
		}
		if($user->save()) {
			return redirect('/users/user_dashboard')->with('success','Profile updated successfully.');
		} else {    
			return redirect()->back()->with('error','Profile could not be updated,Please try again!');
		} 
	}
	public function delete($id) {   
		$user=User::whereId($id)->firstOrFail();
		if($user) {
			if($user->delete()) {
				return redirect('/users')->with('success',__('User deleted successfully'));
			} else {
				return redirect()->back()->with('error',__('Sorry,user could not be deleted,please try again!'));
			} 
		}
	}  
	public function getAddUser() { 
		$countries=Country::pluck('name','id');
		return view('users.add',compact('countries')); 
	}
	public function postAddUser(Request $req,NewUserRequest $u) {  
		$user=new User(); 
		$user->first_name=$req->get('first_name');
		$user->last_name=$req->get('last_name');
		$user->email=$req->get('email'); 
		$user->user_company=$req->get('user_company');
		$user->country_id=$req->get('country_id');
		$user->city=$req->get('city');
		$user->phone_number=$req->get('phone_number');
		$user->street=$req->get('street');
		$user->street_number=$req->get('street_number');
		$user->post_code=$req->get('post_code'); 
		$user->btw_number=$req->get('btw_number');
		$user->password=$req->get('password');
		$user->role_id=$req->get('role_id');
		if($user->save()) {
			return redirect('/users')->with('success',__('User added successfully'));
		} else { 
			return redirect()->back()->with('error',__('Sorry,user could not be added,please try again!'));
		} 
	} 
	public function getEdit($id) {
		if($id) {
			$countries=Country::pluck('name','id');
			$user=User::whereId($id)->firstOrFail();
			return view('users.edit',compact('user','countries'));  
		}
	}
	public function postEdit(Request $req,UserRequest $u,$id) {
		$user=User::where('id',$id)->first();
		$user->first_name=$req->get('first_name');
		$user->last_name=$req->get('last_name');
		$user->email=$req->get('email'); 
		$user->user_company=$req->get('user_company');
		$user->country_id=$req->get('country_id');
		$user->city=$req->get('city');
		$user->phone_number=$req->get('phone_number');
		$user->street=$req->get('street');
		$user->street_number=$req->get('street_number');
		$user->post_code=$req->get('post_code'); 
		$user->btw_number=$req->get('btw_number');
		$user->password=$req->get('password');
		$user->role_id=$req->get('role_id');
		if($user->save()) {
			return redirect('/users')->with('success',__('User updated successfully'));
		} else { 
			return redirect()->back()->with('error',__('Sorry,user could not be updated,please try again!'));
		} 
	}
	public function getUsers(Request $req) {
		$users = User::select(['id', 'first_name','last_name','role_id','phone_number','email','created_at'])->where('role_id','!=',1)->get();   
		$str='';
		return Datatables::of($users)
		->addColumn('action', function ($user) {    
			$str='<a href="/users/edit/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
			$str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$user->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
			$str .='&nbsp;<a id="switchUser"  onclick="confirmSwitch('.$user->id.');" href="javascript:;" class="btn btn-xs btn-success"> Switch</a>'; 
			return $str;   
		}) 
		->editColumn('first_name',function($user) {
			$first_name=$user->first_name.' '.$user->last_name; 
			return $first_name;
		})  
		->editColumn('role_id',function($user) {     
			if($user->role_id==2)  {
				return "<span class='label label-warning'> Customer </span>"; 
			} else if($user->role_id==3) {
				return "<span class='label label-danger'> Advertiser </span>"; 
			}
		})
		->editColumn('created_at',function($date) {
			return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
		})
		->escapeColumns([])
		->make(true);   
	}
	public function switchAccount($id) {
		if($id) {
			session(['back_id' => Auth::user()->id ]); 
			$newUser=User::where('id',$id)->first();
			if(Auth::loginUsingId($newUser->id)) {
				if(Auth::user()->role_id==3) {
					return redirect('/dashboard/adv-dashboard')->with('success',__('Account switched successfully'));
				} else if(Auth::user()->role_id==2) {
					return redirect('/dashboard/cust-dashboard')->with('success',__('Account switched successfully'));
				} 
			} else {
				session()->pull('back_id');
				return redirect()->back()->with('error',__('Sorry, Error in switchning account, please try again!'));
			}
		}
	}
	public function switchAccountBack($id) {
		if($id) {
 			if($id==session('back_id')) {
 				$newUser=User::where('id',$id)->first();
 				if($newUser) {
 					if(Auth::loginUsingId($newUser->id)) {
 						session()->pull('back_id');
 						if(Auth::user()->role_id==1) {
 							return redirect('/dashboard')->with('success',__('Account switched successfully'));			
 						}
 					} else { 
 						session()->pull('back_id');
 						return redirect()->back()->with('error',__('Sorry, Error in switchning account, please try again!'));			
 					}
 				} else {
 					return redirect()->back()->with('error',__('Sorry, user doesn\'t exists'));
 				}
 			} else {
 				return redirect()->back()->with('error',__('Sorry, back id is not correct!'));
 			}
		}
	}  
	public function viewPDF($id) { 
	    $user=User::whereId($id)->firstOrFail(); 
	    if($user->role_id==1) {
	    	$user->role_id='Admin';
	    } else {
	    	$user->role_id='User';
	    } 
	    $pdf=PDF::loadView('users.pdf.pdf',compact('user')); 
	    return $pdf->stream(); 
	}
	public function showChangePassword() { 
		return view('users.change_password');
	}
	public function changePassword(Request $req) {
		$this->validate($req, [
	        'password'=> 'required', 
	        'confirm_password' => 'required|same:password'
		]);  
		$user=User::find(Auth::user()->id);
		$user->password=$req->get('password'); 
		if($user->save()) {
			return redirect()->back()->with('success',__('Password updated successfully'));
		} else {
			return redirect()->back()->with('error',__('Sorry,password could not be updated,please try again!'));
		} 
	} 

	/**
	* Redirect the user to the OAuth Provider.
	*
	* @return Response 
	*/
	public function redirectToProvider(Request $r,$provider) { 
		$r->session()->forget('social_role_id');
		if(isset($r->role)){
			$r->session()->put('social_role_id',$r->role);
		}
		return Socialite::driver($provider)->redirect(); 
	}

	/**
    * Obtain the user information from provider.  Check if the user already exists in our
    * database by looking up their provider_id in the database.
    * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
    * redirect them to the authenticated users homepage.
    *
    * @return Response  
    */
    public function handleProviderCallback(Request $r,$provider){
        $user = Socialite::driver($provider)->user();
        if($r->session()->has('social_role_id')) {
        	$checkIfExist=User::where('email',$user->email)->exists();
        	if($checkIfExist){ 
        		return redirect('/login')->with('error','Sorry,email already exists');
        	}
        	$authUser=$this->registerFromSocialNetwork($r,$user,$provider);
        }else{
        	$authUser=$this->loginFromSocialNetwork($user); 
        }        
        if($authUser){
        	if(Auth::loginUsingId($authUser->id)){
        		if(Auth::user()->role_id==3){
        			return redirect('/dashboard/adv-dashboard')->with('success',__('LoggedIn successfully with  '.ucfirst($provider)));
        		}elseif(Auth::user()->role_id==2){
        			return redirect('/dashboard/cust-dashboard')->with('success',__('LoggedIn successfully with  '.ucfirst($provider))); 
        		}else{
        			return redirect('/login')->with('error',__('Sorry,you are not allowed to login from social network,please login here')); 
        		}
        	}else{
        		return redirect('/login')->with('error',__('Error while logging you in,please try again!'));  
        	}
        }else{
        	return redirect('/login')->with('error',__('Sorry,No account found,please register as advertiser or customer'));
        }
    }    
    /**
    * If a user has registered before using social auth, return the user
    * else, create a new user object.
    * @param  $user Socialite user object
    * @param $provider Social auth provider
    * @return  User
    */ 
    public function registerFromSocialNetwork(Request $r,$user,$provider) {  
    	$checkIfExist=User::where('email',$user->email)->exists();
    	if($checkIfExist){
    		$return=false;
    		return $return;
    	}else{
    		$name=explode(' ',$user->name);
    		$newUser=new User();
    		$newUser->first_name=$name[0];
    		$newUser->last_name=$name[1];
    		$newUser->email=$user->email;
    		$newUser->provider=$provider;
    		$newUser->provider_id=$user->id;
    		$newUser->role_id=$r->session()->get('social_role_id');
    		$newUser->is_verified=1;
    		if($newUser->save()){
    			$r->session()->forget('social_role_id');
    			return $newUser;
    		} 
    	}
    }
    public function loginFromSocialNetwork($user) { 
		$authUser=User::where('email',$user->email)->first();
		if ($authUser) {
		    return $authUser;   
		}
		$return=false;
		return $return;   
    }
    public function getForgotPassword(){
    	return view('users.forgot_password');
    }
    public function postForgotPassword(Request $r){
    	if($r->email) { 
    		$user=User::whereEmail($r->email)->first(); 
    		if($user) {
    			$token=uniqid();
    			DB::table('password_resets')->insert([
    				'user_id'=>$user->id,
    				'token'=>$token, 
    				'created_at'=>date('Y-m-d H:i:s'),
    				'updated_at'=>date('Y-m-d H:i:s')
    			]);
    			$url=url('/').'/users/reset-password/'.$token; 
    			$this->sendPasswordResetLink($r->email,'Password Reset Link','Please click on the link below to reset your password',$url);
    			return redirect('/login')->with('success',__('Please check your inbox for reset link')); 
    		} else { 
    			return redirect()->back()->with('error',__('Sorry,email does not exist'));
    		}
    	} 
    }
    public function getResetPassword($token){
    	if($token) {
    		$status=DB::table('password_resets')->whereToken($token)->first();
    		if($status) {
    			if(!$status->status) {
    				return view('users.reset_password',compact('token'));  
    			} else {
    				return redirect('/login')->with('error',__('Invalid token'));	
    			}
    		} else {
    			return redirect('/login')->with('error',__('Invalid token'));	
    		}
    	} 
    }
    public function postResetPassword(Request $r,$token){
    	$this->validate($r, [
	        'password'=> 'required', 
	        'confirm_password' => 'required|same:password'
		]);
		$status=DB::table('password_resets')->whereToken($token)->first(); 
		if($status) {
			if(!$status->status) {
				$user=User::find($status->user_id);
				$user->password=$r->get('password'); 
				if($user->save()) { 
					DB::table('password_resets')->whereToken($token)->update(['status'=>1]);
					return redirect('/login')->with('success',__('Password updated successfully,please login'));
				} else {
					return redirect()->back()->with('error',__('Sorry,password could not be updated,please try again!'));
				} 
			} else {
				return redirect()->back()->with('error',__('Invalid token'));	
			}
		} else {
			return redirect()->back()->with('error',__('Invalid token'));	
		} 
    }
    public function sendPasswordResetLink($email,$subject,$msg,$link) {
    	if($email) {
    		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    			try {
    				Mail::to($email)->send(new SendPasswordResetLink($subject,$msg,$link));     
    			} catch(Exception $e) {
    				\Log::log('sendPasswordResetLink','Mail not sent to '.$email.' Server returned error: '.$e->getMessage()); 
    				return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
    			}
    		}
    	}
    }
    public function sendVerificationLink($email,$subject,$msg,$link) {
    	if($email) {
    		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    			try {
    				Mail::to($email)->send(new SendPasswordResetLink($subject,$msg,$link));     
    			} catch(Exception $e) {
    				\Log::log('sendVerificationLink','Mail not sent to '.$email.' Server returned error: '.$e->getMessage()); 
    				return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
    			}
    		}
    	}
    }
    public function getAdvertiserRegister(){ 
    	return view('users.advertiser_register');
    }
    public function postAdvertiserRegister(Request $r){
    	$this->validate($r,[
                'email'=>'required|email|unique:users',
    			'password'=>'required',
    			'tos'=>'required'
    		],[
    			'email.required'=>__('Email is required.'),
    			'email.unique'=>__('Email already exists.'),
    			'email.email'=>__('Enter Valid Email.'),
    			'password.required'=>__('Password is required.'),
    			'tos.required'=>__('Please agree to terms.')
    		]
    	);
    	$user=new User();
    	$user->email=$r->email;
    	$user->password=$r->password;
    	$user->role_id=3;
    	$user->first_name=$r->first_name;
    	$user->last_name=$r->last_name;
    	if($user->save()) {
    		$token=uniqid();
    		DB::table('users')->whereId($user->id)->update(['token'=>$token]);
    		$url=url('/').'/users/verification/'.$token; 
    		$this->sendVerificationLink($user->email,'Please verify your account','Thanks for registering with us,Please click on the link below to verify and activate your account',$url);
    		return redirect('/login')->with('success',__('Accont created successfully,check your inbox for verification link'));
    	}
    	return redirect()->back()->with('error',__('Sorry,accont could not be created,please try again!'));
    }
    public function getCustomerRegister(){
    	return view('users.customer_register');
    }
    public function postCustomerRegister(Request $r){
		$this->validate($r,[
	            'email'=>'required|email|unique:users',
				'password'=>'required',
				'tos'=>'required'
			],[
				'email.required'=>__('Email is required.'),
				'email.unique'=>__('Email already exists.'),
				'email.email'=>__('Enter Valid Email.'),
				'password.required'=>__('Password is required.'),
				'tos.required'=>__('Please agree to terms.')
			]
		);
		$user=new User();
		$user->email=$r->email;
		$user->password=$r->password;
		$user->role_id=2;
		$user->first_name=$r->first_name;
		$user->last_name=$r->last_name;
		if($user->save()) {
			$token=uniqid();
			DB::table('users')->whereId($user->id)->update(['token'=>$token]);
			$url=url('/').'/users/verification/'.$token; 
			$this->sendVerificationLink($user->email,'Please verify your account','Thanks for registering with us,Please click on the link below to verify and activate your account',$url);
			return redirect('/login')->with('success',__('Accont created successfully,check your inbox for verification link'));
		}
		return redirect()->back()->with('error',__('Sorry,accont could not be created,please try again!'));
    }
    public function postCustomerRegisterView(Request $r){
		$this->validate($r,[
	            'email'=>'required|email|unique:users',
				'password'=>'required',
				'tos'=>'required'
			],[
				'email.required'=>__('Email is required.'),
				'email.unique'=>__('Email already exists.'),
				'email.email'=>__('Enter Valid Email.'),
				'password.required'=>__('Password is required.'),
				'tos.required'=>__('Please agree to terms.')
			]
		);
		$user=new User();
		$user->email=$r->email;
		$user->password=$r->password;
		$user->role_id=2;
		$user->first_name=$r->first_name;
		$user->last_name=$r->last_name;
		if($user->save()) {
			$token=uniqid();
			DB::table('users')->whereId($user->id)->update(['token'=>$token]);
			$url=url('/').'/users/verification/'.$token; 
			$this->sendVerificationLink($user->email,'Please verify your account','Thanks for registering with us,Please click on the link below to verify and activate your account',$url);
			if(Auth::loginUsingId($user->id)){ 
				if($r->accom_id){ 
					return redirect()->intended('/tickets/contact-owner/'.$r->accom_id)->with('success',__('You can chat with owner here.Please verify your account by clicking on the link sent on email'));      
				}
			}else{
				return redirect('/login')->with('error',__('Sorry cannot auto login,please verify your account and login'));	
			}
		}
		return redirect()->back()->with('error',__('Sorry,accont could not be created,please try again!'));
    }
    public function verfiyUser($token){
    	if($token){
    		if(DB::table('users')->whereToken($token)->exists()){
    			if(DB::table('users')->whereToken($token)->update(['is_verified'=>1,'token'=>null])){ 
    				return redirect('/')->with('success',__('Your account verified successfully'));   
    			}else{
    				return redirect('/login')->with('error',__('Internal error,cannot verify user,please try again!'));
    			}
    		}else{
    			return redirect('/login')->with('error',__('Sorry,verification token not found'));
    		}
    	}
    } 




}
