<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LBV\Mail\SendQuickMail; 

class DashboardController extends Controller
{
    public function adminDashboard() {
    	$users=DB::table('users')->where('role_id','!=',1)->count();
    	$accoms=DB::table('accomodations')->count();
        $visitorsThisMonth=0;
        $visitorsThisMonth=DB::select(DB::raw('SELECT MONTH(created_at) as y,SUM(count) as v FROM `site_visits` WHERE MONTH(created_at) = MONTH(CURRENT_DATE()) AND YEAR(created_at) = YEAR(CURRENT_DATE()) GROUP BY y'));
        if($visitorsThisMonth){
            $visitorsThisMonth=$visitorsThisMonth[0]->v; 
        }
        $dataPro=DB::table('product_payments')->select(DB::raw('YEAR(created_at) as year'), DB::raw("SUM(total_amount) as value"))->groupby('year')->get()->toArray();
        $dataSub=DB::table('subscription_payments')->select(DB::raw('YEAR(created_at) as year'), DB::raw("SUM(amount_paid) as value"))->groupby('year')->get()->toArray();
        if($dataPro){
            foreach ($dataPro as $d) {
                $d->year="".$d->year."";
            }
        }
        if($dataSub){
            foreach ($dataSub as $d) {
                $d->year="".$d->year."";
            }
        } 
        $siteVisits=DB::select(DB::raw('SELECT DATE(created_at) as y,SUM(count) as v FROM `site_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW() GROUP BY y'));
        $dataPro=json_encode($dataPro);
        $dataSub=json_encode($dataSub);
        $siteVisits=json_encode($siteVisits); 
    	return view('dashboards.dashboard',compact('users','accoms','dataSub','dataPro','siteVisits','visitorsThisMonth'));  
    }
    public function custDashboard() {  
    	return view('dashboards.cust_dashboard');
    }
    public function advDashboard() {
        $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->count();
        $subscriptions=DB::table('subscription_licenses')->whereUserId(Auth::user()->id)->where('valid_upto','>=',date('Y-m-d H:i:s'))->count();
        $promotions=DB::table('product_licenses')->whereUserId(Auth::user()->id)->where('valid_upto','>=',date('Y-m-d H:i:s'))->where('valid_from','<=',date('Y-m-d H:i:s'))->count();
        $dataPro=DB::table('product_payments')->select(DB::raw('YEAR(created_at) as year'), DB::raw("SUM(total_amount) as value"))->groupby('year')->whereUserId(Auth::user()->id)->get()->toArray();
        $dataSub=DB::table('subscription_payments')->select(DB::raw('YEAR(created_at) as year'), DB::raw("SUM(amount_paid) as value"))->groupby('year')->whereUserId(Auth::user()->id)->get()->toArray();
        if($dataPro){
            foreach ($dataPro as $d) {
                $d->year="".$d->year."";
            }
        }
        if($dataSub){
            foreach ($dataSub as $d) {
                $d->year="".$d->year."";
            }
        }
        $accom=DB::table('accomodations')->select('id')->whereUserId(Auth::user()->id)->orderBy('id','asc')->first();
        $accomVisits=[];
        if($accom){
            $accomVisits=DB::select(DB::raw("SELECT DATE(created_at) as y,SUM(count) as v FROM `accomodation_visits` WHERE `created_at` BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW() AND accom_id=$accom->id  GROUP BY y"));
        }
        $dataPro=json_encode($dataPro);
        $dataSub=json_encode($dataSub);
        $accomVisits=json_encode($accomVisits);
    	return view('dashboards.adv_dashboard',compact('users','accoms','dataSub','dataPro','accomVisits','promotions','subscriptions')); 
    }
    public function sendQuickMail(Request $r){
        $this->validate($r,[ 
                'subject'=>'required', 
                'message'=>'required' 
            ]
        );
        $this->notifyAdmins('New Quick Mail Request Received',$r->message,$r->subject);
        return redirect()->back()->with('success',__('Your message has been sent successfully,we wil reply soon')); 
    }
    public function notifyAdmins($subject,$msg,$advSubject) {
       $admins=DB::table('users')->select('email')->where('role_id',1)->get();
        if($admins) {
            foreach ($admins as $a) {
                try {
                    Mail::to($a->email)->send(new SendQuickMail($subject,$msg,$advSubject,Auth::user()));     
                } catch(Exception $e) {
                    \Log::log('SendQuickMail','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage()); 
                }
            } 
        }
    } 
}
