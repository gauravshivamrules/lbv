<?php
namespace LBV\Http\Controllers;
use Illuminate\Http\Request;
use LBV\Model\Setting;
use LBV\Model\Privacy;
use LBV\Model\Terms; 
use LBV\Model\UserHelp; 
use LBV\Model\Searchtag;
use LBV\Model\SearchingGroup;
use LBV\Model\SearchtagSearchingGroup; 
use Yajra\Datatables\Datatables; 
class Settings extends Controller 
{
    public function index() {
    	return view('settings.index');
    } 
    public function getSettings() {
    	$settings = Setting::select(['id', 'option_key', 'option_value'])->get();   
    	$str='';
    	return Datatables::of($settings)
    	->addColumn('action', function ($s) {    
    		return '<a href="javascript:;" onclick="getData('.$s->id.');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a>';   
    	}) 
    	->escapeColumns([])
    	->make(true);   
    }
    public function edit($id) {
    	$setting=Setting::select(['id','option_key','option_value'])->where('id',$id)->first();
    	echo json_encode($setting);
    	die;
    } 
    public function update(Request $req) {
    	$this->validate($req,[
    			'option_id'=>'required',
    			'option_value'=>'required'
    		],[
    			'option_id.required'=>__('ID is required.'),
    			'option_value.required'=>__('Value is required')
    		]
    	);
    	$setting=Setting::where('id',$req->get('option_id'))->first();
    	$setting->option_value=$req->get('option_value');
    	if($setting->save()) {
    		return redirect('/settings')->with('success',__('Setting updated successfully'));
    	} 
    	return redirect()->back()->with('error',__('Sorry,cannot update settings,please try again!'));
    }
    public function privacy() {
    	$privacy=Privacy::first();
    	return view('settings.privacy',compact('privacy')); 
    }
    public function updatePrivacy(Request $req) {
    	$this->validate($req,[
    			'privacy_content'=>'required'
    		],[
    			'privacy_content.required'=>__('Content is required'),
    		]
    	); 
    	$privacy=Privacy::where('id',$req->get('id'))->first();
    	$privacy->content=$req->get('privacy_content');
    	if($privacy->save()) {
    		return redirect('/settings')->with('success',__('Privacy updated successfully'));
    	}
    	return redirect()->back()->with('error',__('Sorry,cannot update privacy,please try again!')); 
    }
    public function tos() {
    	$tos=Terms::first();
    	return view('settings.tos',compact('tos')); 
    }
    public function updateTos(Request $req) { 
    	$this->validate($req,[
    			'tos_content'=>'required'
    		],[
    			'tos_content.required'=>__('Content is required'),
    		]
    	);  
    	$tos=Terms::where('id',$req->get('id'))->first();
    	$tos->content=$req->get('tos_content');
    	if($tos->save()) {
    		return redirect('/settings')->with('success',__('Terms & Conditions updated successfully'));
    	}
    	return redirect()->back()->with('error',__('Sorry,cannot update Terms & Conditions,please try again!')); 
    } 
    public function help() {
        return view('settings.help');
    }
    public function getHelps() {
        $helps=UserHelp::select(['id','title'])->get();
        $str='';
        return Datatables::of($helps)
        ->addColumn('action', function ($s) {    
            $str ='<a href="javascript:;" onclick="getDataView('.$s->id.');" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>&nbsp';   
            $str .='<a href="/settings/edit_help/'.$s->id.'" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';   
            $str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$s->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i> </a>'; 
            return $str;
        })   
        ->escapeColumns([])
        ->make(true);   
    }
    public function updateHelp(Request $r){
        $this->validate($r,[ 
                'title'=>'required',
                'content'=>'required'
            ],[
                'title.required'=>__('Title is required.'),
                'content.required'=>__('Content is required')
            ]
        );
        $h=UserHelp::where('id',$r->get('id'))->first();
        $h->title=$r->get('title');
        $h->content=$r->get('content');
        if($h->save()) {
            return redirect('/settings/help')->with('success',__('Help section updated successfully'));
        }
        return redirect()->back()->with('error',__('Sorry,cannot update help section,please try again!')); 
    }
    public function getHelpEdit(Request $r,$id) { 
        $h=UserHelp::where('id',$id)->first();
        return view('settings.edit_help',compact('h'));
    } 
    public function getHelpView(Request $r) { 
        $h=UserHelp::where('id',$r->get('id'))->first();
        echo json_encode($h);
    } 
    public function getAllHelps() { 
        $h=UserHelp::select(['id','title'])->get()->toArray();
        echo json_encode($h);
        die;
    } 
    function get_help_byid(Request $r) {
        $h=UserHelp::where('id',$r->get('id'))->first();
        echo json_encode($h);die;
    }
    public function delete($id) {   
        $help=UserHelp::whereId($id)->firstOrFail();
        if($help) {
            if($help->delete()) {
                return redirect('/settings/help')->with('success',__('Help section  deleted successfully'));
            } else {
                return redirect()->back()->with('error',__('Sorry,help section could not be deleted,please try again!'));
            } 
        }
    } 
    public function searchTags() {
        $searchgroups=SearchingGroup::pluck('group_name','id');
        return view('settings.search_tags',compact('searchgroups'));
    }
    public function addSearchTags(Request $r) {
        $this->validate($r,[ 
                'search_tag'=>'required|unique:searchtags,tag_name'
            ],[
                'search_tag.required'=>__('Search tag is required.'),
                'search_tag.unique'=>__('Search tag already exists.')
            ]
        );
        $s=new Searchtag();
        $s->tag_name=$r->get('search_tag');
        if($s->save()) {
            $sGroup=new SearchtagSearchingGroup();
            $sGroup->searching_group_id=$r->get('search_group_id');
            $sGroup->searchtag_id=$s->id;
            $sGroup->save();
            return redirect()->back()->with('success',__('Searchtag added successfully'));
        } 
        return redirect()->back()->with('error',__('Sorry,searchtag could not be added,please try again!')); 
    } 
}
