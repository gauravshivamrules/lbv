<?php
namespace LBV\Http\Controllers;
use DB;
use Validator;
use Image;
use LBV\Model\GalleryImage;
use LBV\Model\Accomodation;
use Illuminate\Http\Request;

class AccomodationImagesController extends Controller 
{

  /*public function test(){
    $allImages=DB::table('old_gallery_images')->limit(10)->get();
    $destinationPath=public_path('images/gallery/');
    $originPath=public_path('images/old_gallery/');
    foreach ($allImages as $img) { 
        if ($dh = opendir($originPath.$img->accom_id)){
            while (($file = readdir($dh)) !== false){
              if($file==$img->image){
                if (!file_exists($destinationPath.$img->accom_id)) {
                    mkdir($destinationPath.$img->accom_id,0777,true);
                }
                $photo=$originPath.$img->accom_id.'/'.$file;
                $picName=pathinfo($img->image);
                $imagename = uniqid().'.'.$picName['extension']; 
                $thumb='thumb_'.$imagename;
                $slider='slider_'.$imagename;
                $thumb_img=Image::make($photo)->resize(500,316);
                $slider_img=Image::make($photo)->resize(830, null,function ($constraint){
                    $constraint->aspectRatio();
                });
                $featuredImg=DB::table('accomodations')->select('featured_image')->whereId($img->accom_id)->first();
                if($featuredImg){
                  if($featuredImg->featured_image==$img->image){
                     DB::table('accomodations')->whereId($img->accom_id)->update(['featured_image'=>$imagename]);       
                  }  
                } 
                $image=new GalleryImage();
                $image->accom_id=$img->accom_id;
                $image->image=$imagename;
                $image->thumb_image=$thumb;
                $image->slider_image=$slider; 
                if($image->save()) {
                    $thumb_img->save($destinationPath.$img->accom_id.'/'.$thumb,80);
                    $slider_img->save($destinationPath.$img->accom_id.'/'.$slider,80);
                    copy($originPath.$img->accom_id.'/'.$file,$destinationPath.$img->accom_id.'/'.$file);
                    rename($destinationPath.$img->accom_id.'/'.$file, $destinationPath.$img->accom_id.'/'.$imagename); 
                    dump('Images saved for Accommodation ID:'.$img->accom_id); 
                }else{
                   dump('Images not saved for Accommodation ID:'.$img->accom_id); 
                }
              }
            }
            closedir($dh);
        }    
    }
    dd('DONE');
  }*/


  	public function getPaidImages(Request $r,$accom) {
      $prices=DB::table('accomodation_prices')->whereAccomId($accom)->first();
      if(!$prices) {
        return redirect()->back()->with('error',__('Sorry,you must add prices to continue'));
      } 
      $featuredImage=Accomodation::select(['featured_image'])->whereId($accom)->first(); 
      $galleryImages=GalleryImage::where('accom_id',$accom)->orderBy('sort_order')->get();
  		return view('accomodations.add_accom_images',compact('accom','galleryImages','featuredImage'));  
  	}
  	public function postPaidImages(Request $request,$accom=null) {
      $validator = Validator::make($request->all(), [ 'file'=>'image|mimes:jpeg,png,jpg,gif|max:5000' ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          header("HTTP/1.0 400 Bad Request");
          echo $errors->first('file');
          die;
      }
      $photo = $request->file;
      $imagename = uniqid().'.'.$photo->getClientOriginalExtension(); 
      $thumb='thumb_'.$imagename;
      $slider='slider_'.$imagename;
      $sliderbig_image='sliderbig_'.$imagename;

      $destinationPath = public_path('images/gallery/'.$accom);
      if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
      }
      // 385,255
      /*$thumb_img = Image::make($photo->getRealPath())->resize(420,315);
      $slider_img = Image::make($photo->getRealPath())->resize(815,380);*/


      $thumb_img = Image::make($photo->getRealPath())->resize(500,316);
      // , function ($constraint) {
      //           $constraint->aspectRatio();
      //       }); 
      $slider_img = Image::make($photo->getRealPath())->resize(830, null, function ($constraint) {
          $constraint->aspectRatio();
      }); 
      /*$slider_img_big = Image::make($photo->getRealPath())->resize(1500, null, function ($constraint) {
          $constraint->aspectRatio();
      });*/ 
      $image=new GalleryImage();
      $image->accom_id=$accom;
      $image->image=$imagename;
      $image->thumb_image=$thumb;
      $image->slider_image=$slider;
     // $image->sliderbig_image=$sliderbig_image;
      if($image->save()) {
          $thumb_img->save($destinationPath.'/'.$thumb,80);
          $slider_img->save($destinationPath.'/'.$slider,80);
          //$slider_img_big->save($destinationPath.'/'.$sliderbig_image,80);
          $photo->move($destinationPath, $imagename); 
          return response()->json(['success'=>$thumb]);  
          exit();   
      }  
      header("HTTP/1.0 400 Bad Request");
      echo $errors->first('file');
  	}
    public function deleteImage($id=null,$accom=null) {
      if($id) {
          $image=GalleryImage::whereId($id)->first();
          if($image) {
              $name=$image->image;
              if($image->delete()) {
                $path=public_path('images/gallery/'.$accom.'/');
                $file=$path.$name;
                if (file_exists($file)) {
                   $thumb=$path.'thumb_'.$name;
                   $slider=$path.'slider_'.$name;
                   //$sliderBig=$path.'sliderbig_'.$name;
                   unlink($file);unlink($thumb);unlink($slider);
                   //unlink($sliderBig);
                   $featured=Accomodation::whereId($accom)->first();
                   if($featured->featured_image) {
                      if($featured->featured_image==$name) {
                          $featured->featured_image='';
                          $featured->save();
                      }
                   } 
                }  
                return redirect()->back()->with('success',__('Image deleted successfully'));  
              } 
              return redirect()->back()->with('error',__('Sorry,image could not deleted,please try again!'));
          }
      }
    }
    public function getImagesEdit(Request $r,$accom) {
      $featuredImage=Accomodation::select(['featured_image'])->whereId($accom)->first(); 
      $galleryImages=GalleryImage::where('accom_id',$accom)->orderBy('sort_order')->get();
      return view('accomodations.edit_accom_images',compact('accom','galleryImages','featuredImage'));  
    }
    public function makeFeatured($accom,$feaured) {
      if($accom) {
        $accomodation=Accomodation::whereId($accom)->first();
        if($accomodation) {
            $accomodation->featured_image=$feaured;
            if($accomodation->save()) { 
                return redirect()->back()->with('success',__('Featured image marked successfully'));
            } 
            return redirect()->back()->with('error',__('Sorry,featured image could not be marked,please try again!'));
        }
      }
    }
    public function sortImages(Request $r) {
      if($r->positions) {
          $return=[];
          $ERROR='';
          $pos=explode(';',$r->positions);
          if($pos){
            foreach($pos as $sOrder=>$val) {
              $id=explode('_', $val);
              if($id) {
                $img=GalleryImage::whereId($id[0])->first();
                $img->sort_order=++$sOrder;
                if(!$img->save()) {
                  $ERROR='ERROR';
                }
              }
            }
            if($ERROR!='') {
              $return['success']=false;
            }else {
              $return['success']=true;
            }
            echo json_encode($return);
          }
      }
    }
}
