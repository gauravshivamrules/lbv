<?php

namespace LBV\Http\Controllers;

use DB;
use PDF;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use LBV\Model\ProductLicense;
use Illuminate\Support\Facades\Auth;

class ProductPaymentsController extends Controller
{
 	
	public function index(){
		return view('product_payments.index');
	}
  public function payments(){
      return view('product_payments.payments');
  }
	public function getProductPaymentsAjax(Request $r) {
		if($r->type) {
			switch ($r->type) { 
				case 'active':
					$payments=$this->giveProductPayment([['product_licenses.user_id',Auth::user()->id],['product_licenses.valid_upto','>=',date('Y-m-d')]] , [1,3]);
					break;
				case 'cancelled':
					$payments=$this->giveProductPayment([ ['product_licenses.user_id',Auth::user()->id] , ['product_payments.payment_complete',2]]);
					break;
				case 'expired':
					$payments=$this->giveProductPayment([ ['product_licenses.user_id',Auth::user()->id] , ['product_licenses.valid_upto','<',date('Y-m-d')]]);
					break;
				case 'outstanding':
					$payments=$this->giveProductPayment([  ['product_licenses.user_id',Auth::user()->id] , ['product_payments.payment_complete',0]]);
					break;
			}
			return Datatables::of($payments)
			->addColumn('action', function ($l) {  
			       $str='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
			      return $str;    
			})  
			->editColumn('payment_complete', function ($payment) {   
                  if($payment->payment_complete==1) {
                      return "<span class='label label-success'>".__('COMPLETED')."</span>";
                  } else if($payment->payment_complete==2) {
                      return "<span class='label label-danger'>".__('CANCELED')."</span>";
                  } else if($payment->payment_complete==3) {
                      return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                  } else if($payment->payment_complete==0) {
                      return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                  }
            }) 
      ->editColumn('pName',function($name) {
              return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
      })  
			->editColumn('vFrom',function($date) {
				return date('d-m-Y',strtotime($date->vFrom));
			}) 
			->editColumn('vUpto',function($date) {
				return date('d-m-Y',strtotime($date->vUpto));
			}) 
			->escapeColumns([])
			->make(true); 
		}
	}
  public function getAdminProductPaymentsAjax(Request $r) {
      if($r->type) { 
          switch ($r->type) { 
              case 'active':
                  $payments=$this->giveProductPayment([ ['product_licenses.valid_upto','>=',date('Y-m-d')]] , [1,3]);
                  break;
              case 'cancelled':
                  $payments=$this->giveProductPayment([  ['product_payments.payment_complete',2]]);
                  break;
              case 'expired':
                  $payments=$this->giveProductPayment([  ['product_licenses.valid_upto','<',date('Y-m-d')]]);
                  break;
              case 'outstanding':
                  $payments=$this->giveProductPayment([ ['product_payments.payment_complete',0]]);
                  break;
          }
          return Datatables::of($payments)
          ->addColumn('action', function ($l) {  
                 $str='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
                return $str;    
          })  
          ->editColumn('payment_complete', function ($payment) {   
                if($payment->payment_complete==1) {
                    return "<span class='label label-success'>".__('COMPLETED')."</span>";
                } else if($payment->payment_complete==2) {
                    return "<span class='label label-danger'>".__('CANCELED')."</span>";
                } else if($payment->payment_complete==3) {
                    return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                } else if($payment->payment_complete==0) {
                    return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
                }
          }) 
          ->editColumn('pName',function($name) {
              return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
          })
          ->editColumn('vFrom',function($date) {
              return date('d-m-Y',strtotime($date->vFrom));
          }) 
          ->editColumn('vUpto',function($date) {
              return date('d-m-Y',strtotime($date->vUpto));
          }) 
          ->escapeColumns([])
          ->make(true); 
      }
  }
	public function getProSubscriptionHistory() { 
       $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->orderBy('name','asc')->pluck('name','id');
       return view('product_payments.history',compact('accoms'));  
  }
  public function getProSubscriptionHistoryAjax(Request $r) {
      if($r->accom_id) {
          if($r->type) {
              if($r->type=='active') {
                  $history=$this->giveProductPayment([
                      ['product_licenses.user_id',Auth::user()->id],
                      ['product_licenses.valid_upto','>=',date('Y-m-d')],
                      ['product_licenses.accom_id',$r->accom_id]
                  ],[1,3]);
              } else {
                  $history=$this->giveProductPayment([
                      ['product_licenses.user_id',Auth::user()->id],
                      ['product_licenses.valid_upto','<',date('Y-m-d')],
                      ['product_licenses.accom_id',$r->accom_id]
                  ]);
              }
              return Datatables::of($history)  
              ->addColumn('action', function ($l) {  
              	$str='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
              	return $str;    
              }) 
              ->editColumn('pName',function($name) {
                  return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
              }) 
              ->editColumn('payment_complete', function ($payment) {   
              	if($payment->payment_complete==1) {
              		return "<span class='label label-success'>".__('COMPLETED')."</span>";
              	} else if($payment->payment_complete==2) {
              		return "<span class='label label-danger'>".__('CANCELED')."</span>";
              	} else if($payment->payment_complete==3) {
              		return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
              	} else if($payment->payment_complete==0) {
              		return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
              	} 
              }) 
              ->editColumn('vFrom',function($date) {
              	return date('d-m-Y',strtotime($date->vFrom));
              }) 
              ->editColumn('vUpto',function($date) {
              	return date('d-m-Y',strtotime($date->vUpto));
              }) 
              ->escapeColumns([])
              ->make(true);  
          }
      }
  }
 	public function giveProductPayment($where=array(),$whereIn=array()) {
 	    if($where) {
 	        if($whereIn) {
 	            $payment=ProductLicense::where($where)->whereIn('product_payments.payment_complete',$whereIn)
 	                ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                  ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
 	                ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
 	                ->leftJoin('products','products.id','=','product_instances.product_id')
 	                ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
 	                ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
 	                ->leftJoin('users','product_licenses.user_id','=','users.id')
 	                ->select('product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
 	                    'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
 	                    'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
 	                    'product_payments.total_amount as total_amount','product_instances.product_price as productPrice','product_instances.id as instanceId',
 	                    'pay_methods.method_name as pName','accomodations.name as aName','accomodations.id as accomId','users.first_name as uFName','users.last_name as uLName',
 	                    'products.product_name','products.product_description','products.product_price','products.product_category_id','mollie_product_payments.method'
 	                )
 	                ->orderBy('product_licenses.id','desc')
 	                ->get(); 
 	        } else {
 	            $payment=ProductLicense::where($where)  
 	                ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                  ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
 	                ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
 	                ->leftJoin('products','products.id','=','product_instances.product_id')
 	                ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
 	                ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
 	                ->leftJoin('users','product_licenses.user_id','=','users.id')
 	                ->select('product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
 	                    'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
 	                    'product_licenses.product_month','product_licenses.product_year','product_instances.product_price as productPrice','product_instances.id as instanceId',
 	                    'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
 	                    'product_payments.total_amount as total_amount',
 	                    'pay_methods.method_name as pName','accomodations.name as aName','accomodations.id as accomId','users.first_name as uFName','users.last_name as uLName',
 	                    'products.product_name','products.product_description','products.product_price','products.product_category_id','mollie_product_payments.method'
 	                )
 	                ->orderBy('product_licenses.id','desc')  
 	                ->get(); 
 	        }
 	        return $payment; 
 	    } else {
 	        return false;
 	    }
 	}  
  public function paymentsIncome(){
      $SUM=DB::table('product_payments')->where('payment_complete',1)->sum('total_amount');
      $SUMPENDING=DB::table('product_payments')->where('payment_complete',0)->sum('total_amount');
      return view('product_payments.income',compact('SUM','SUMPENDING')); 
  }
  public function findTotalPaidAmount(Request $r) {
      $returnArr=[];
      if($r->start_date && $r->end_date) {
          $SUM=DB::table('product_payments')->whereBetween('product_payments.created_at', [date('Y-m-d',strtotime($r->start_date)), date('Y-m-d',strtotime($r->end_date))])
          ->where('payment_complete',1)->sum('total_amount'); 
          $PENDINGSUM=DB::table('product_payments')->whereBetween('product_payments.created_at', [date('Y-m-d',strtotime($r->start_date)), date('Y-m-d',strtotime($r->end_date))])
          ->where('payment_complete',0)->sum('total_amount');
          $returnArr['sum']=$SUM; 
          $returnArr['pSum']=$PENDINGSUM; 
      } 
      echo json_encode($returnArr);
  }
  public function paymentsIncomeAjax(Request $r){ 
     if($r->type) {    
         switch ($r->type) {  
             case 'active':
                  if($r->start_date) {
                      $payments=$this->getIncome([ ['product_payments.payment_complete',1]],$r->start_date,$r->end_date);
                  } else {
                      $payments=$this->getIncome([ ['product_payments.payment_complete',1]]);
                  }
                 break;
             case 'outstanding': 
                if($r->start_date) {
                    $payments=$this->getIncome([ ['product_payments.payment_complete',0]], $r->start_date,$r->end_date);
                } else {
                    $payments=$this->getIncome([ ['product_payments.payment_complete',0]]); 
                }
                break; 
         }
         return Datatables::of($payments)
         ->addColumn('action', function ($l) {  
                $str='&nbsp;<a href="/promotion-licenses/generate-invoice/'.$l->invoice_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
               return $str;    
         })  
         ->editColumn('payment_complete', function ($payment) {   
               if($payment->payment_complete==1) {
                   return "<span class='label label-success'>".__('COMPLETED')."</span>";
               } else if($payment->payment_complete==2) {
                   return "<span class='label label-danger'>".__('CANCELED')."</span>";
               } else if($payment->payment_complete==3) {
                   return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
               } else if($payment->payment_complete==0) {
                   return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
               }
         })
         ->editColumn('pName',function($name) {
          return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
        })
         ->editColumn('created_at',function($date) {
             return date('d-m-Y',strtotime($date->created_at));
         })
         ->editColumn('uFName',function($date) {
             return $date->uFName.' '.$date->uLName;
         })    
         ->escapeColumns([])
         ->make(true); 
     } 
  }
  public function getIncome($where=[],$from=null,$to=null){
      if($from && $to) {
          if($where) {
              $income=ProductLicense::whereBetween('product_payments.created_at', [date('Y-m-d',strtotime($from)).' 00:00:00', date('Y-m-d',strtotime($to)).' 23:59:59'])
                  ->where($where)
                  ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                  ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                  ->leftJoin('products','products.id','=','product_instances.product_id')
                  ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                  ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                  ->leftJoin('users','product_licenses.user_id','=','users.id')
                  ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                  ->select(
                      'product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
                      'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
                      'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
                      'product_payments.total_amount as total_amount','product_payments.created_at',
                      'products.product_name','products.product_description','products.product_price','products.product_category_id',
                      'product_instances.product_price as productPrice','product_instances.id as instanceId',
                      'pay_methods.method_name as pName',
                      'accomodations.name as aName','accomodations.id as accomId',
                      'users.first_name as uFName','users.last_name as uLName',
                      'products.product_name','products.product_description','products.product_price','products.product_category_id',
                      'mollie_product_payments.method' 
                  )
                  ->orderBy('product_licenses.id','desc')
                  ->get(); 
          }
      } else {
          if($where) {
              $income=ProductLicense::where($where)
                  ->leftJoin('product_payments','product_licenses.product_payment_id','=','product_payments.id')
                  ->leftJoin('product_instances','product_instances.id','=','product_licenses.product_instance_id')
                  ->leftJoin('products','products.id','=','product_instances.product_id')
                  ->leftJoin('accomodations','product_licenses.accom_id','=','accomodations.id')
                  ->leftJoin('pay_methods','product_payments.pay_method_id','=','pay_methods.id')
                  ->leftJoin('users','product_licenses.user_id','=','users.id')
                  ->leftJoin('mollie_product_payments','mollie_product_payments.order_id','=','product_payments.transaction_id')
                  ->select(
                      'product_licenses.id','product_licenses.valid_from as vFrom','product_licenses.valid_upto as vUpto','product_licenses.user_id as user_id',
                      'product_licenses.product_year','product_licenses.product_month','product_licenses.activated',
                      'product_payments.payment_complete','product_payments.invoice_number as invoice','product_payments.invoice_key','product_payments.id as paymentId',
                      'product_payments.total_amount as total_amount','product_payments.created_at',
                      'products.product_name','products.product_description','products.product_price','products.product_category_id',
                      'product_instances.product_price as productPrice','product_instances.id as instanceId',
                      'pay_methods.method_name as pName',
                      'accomodations.name as aName','accomodations.id as accomId',
                      'users.first_name as uFName','users.last_name as uLName',
                      'products.product_name','products.product_description','products.product_price','products.product_category_id',
                      'mollie_product_payments.method'  
                  )
                  ->orderBy('product_licenses.id','desc')
                  ->get(); 
          }
      } 
      return $income; 
  }
}
