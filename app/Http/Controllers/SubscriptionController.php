<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use LBV\Custom\Notifications;
use Illuminate\Http\Request;
use LBV\Model\Accomodation;
use LBV\Model\Subscription;
use Yajra\Datatables\Datatables; 
use LBV\Model\SubscriptionLicense;
use Illuminate\Support\Facades\Auth;
use LBV\Mail\NotifyAdminsForSwitchRequest;
use LBV\Http\Requests\SubscriptionAddRequest; 

class SubscriptionController extends Controller
{	
	public function choosePayment(Request $r,$accom){ 
		$subscription=Subscription::whereId($r->type_id)->first();
		if($subscription) {
			if($subscription->type_id==2) {
				$subscription->type_id='Sale';
			} else if($subscription->type_id==3) {
				$subscription->type_id='Rent';
			}
			return view('subscriptions.choose_payment',compact('subscription','accom','payment'));	
		} 
	}
	public function chooseRenewPayment(Request $r,$accom){ 
		$lastLicense=SubscriptionLicense::select(['subscription_licenses.id','subscription_licenses.start_from','subscription_licenses.valid_upto','subscription_payments.payment_complete'])->where('accom_id',$accom)->orderBy('subscription_licenses.id','desc')
			->join('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
			->first();
		// dd($lastLicense);
		$subscription=Subscription::whereId($r->type_id)->first();
		dd($subscription);
		if($subscription) {
			if($subscription->type_id==2) {
				$subscription->type_id='Sale';
			} else if($subscription->type_id==3) {
				$subscription->type_id='Rent';
			}

			return view('subscriptions.choose_renew_payment',compact('subscription','accom','lastLicense'));	
		} 
	}
	public function payRenew($accom=null){
	    if($accom) { 
	        $accomodation=Accomodation::select(['id','label_id'])->whereId($accom)->first();
	        $subscriptions = Subscription::select(DB::raw("CONCAT(name,'-EUR ',amount) AS name"),'id')->where('type_id',$accomodation->label_id)->pluck('name', 'id');
	        return view('subscriptions.pay_renew',compact('subscriptions','accomodation','lastLicense'));    
	    }
	}

	public function getSubscriptionsView(){
		return view('subscriptions.index');
	}
	public function getSubscriptions(){
		$subscriptions=Subscription::all();
		if($subscriptions) {
		    $str=$type='';
		    return Datatables::of($subscriptions) 
		    ->addColumn('action', function ($sub) {   
		        $str ='&nbsp;<a href="/subscriptions/edit/'.$sub->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
		        $str .='&nbsp;<a id="deleteAccom" onclick="confirmDel('.$sub->id.');" href="javascript:;" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>'; 
		        return $str;    
		    })  
		    ->editColumn('type_id',function($sub) {
		        $label='warning';
		        if($sub->type_id==2) {
		        	$type='Sale';
		        } else if($sub->type_id==3) {
		        	$type='Rent';
		        	$label='info';
		        } 
		        return "<span class='label label-$label'>".__($type)." </span>"; 
		    }) 
		    ->escapeColumns([])
		    ->make(true); 
		} 
	}
	public function getSubscriptionsViewAdd(){
		return view('subscriptions.add'); 
	}
	public function postSubscriptionsViewAdd(Request $r,SubscriptionAddRequest $sAR){
		$subscription=new Subscription(); 
		$subscription->name=$r->get('name');
		$subscription->description=$r->get('description');
		$subscription->duration=$r->get('duration');
		$subscription->duration_type=$r->get('duration_type');
		$subscription->amount=$r->get('amount');
		$subscription->type_id=$r->get('type_id'); 
		if($subscription->save()) {
			return redirect('/subscriptions')->with('success',__('Subscription added successfully'));
		}
		return redirect()->back()->with('error',__('Sorry,subscription could not be added,please try again!'));
	}
	public function getSubscriptionsViewEdit(Request $r,$id){
		if($id) {
			$subscription=Subscription::whereId($id)->first();
			return view('subscriptions.edit',compact('subscription'));
		}
	} 
	public function postSubscriptionsViewEdit(Request $r,SubscriptionAddRequest $sAR,$id){
		$subscription=Subscription::whereId($id)->first(); 
		if($subscription) {
			$subscription->name=$r->get('name');
			$subscription->description=$r->get('description');
			$subscription->duration=$r->get('duration');
			$subscription->duration_type=$r->get('duration_type');
			$subscription->amount=$r->get('amount');
			$subscription->type_id=$r->get('type_id'); 
			if($subscription->save()) {
				return redirect('/subscriptions')->with('success',__('Subscription updated successfully'));
			}
		}
		return redirect()->back()->with('error',__('Sorry,subscription could not be updated,please try again!'));
	}
	public function deleteSubscription($id) { 
		if($id) {
			$subscription=Subscription::whereId($id)->first();
			if($subscription) { 
				if($subscription->delete()) {
					return redirect('/subscriptions')->with('success',__('Subscription deleted successfully'));
				}
				return redirect()->back()->with('error',__('Sorry,subscription could not be deleted,please try again!'));
			}
		}
	} 
	public function getSwitchToFree(){
		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->wherePaymentType(1)->whereApproved(1)->whereCompleted(1)->pluck('name','id');
		if(!$accoms->isEmpty()) {
			return view('subscriptions.switch_to_free',compact('accoms'));	
		}
		return redirect()->back()->with('error',__('Sorry,no valid accomodations found'));
	}
	public function postSwitchToFree(Request $r){
		if($r->accom_id) {
			$accom=Accomodation::whereId($r->accom_id)->first();
			if($accom) {
				$accom->payment_type=2;
				//$accom->subs_valid_license=0;
				$accom->approved=0;
				if($accom->save()) {
					\Log::log('AccomodationSwitch','switch request to no cure no pay created for accomID:'.$r->accom_id); 
					Notifications::notifyAdmin([ 
					        'accom_id'=>$r->accom_id,
					        'from_user'=>Auth::user()->id,
					        'message'=>'A new subscription switch request to No cure no pay has been received for accomodation '.$accom->name,
					        'request_type_url'=>'/accomodations/view-accom/'.$accom->slug 
					]);  
					 $admins=DB::table('users')->select('email')->where('role_id',1)->get();
					 $this->notifyAdminsForSwitchRequest($admins,
					        'New subscription switch request has been received.',
					        'New subscription switch request to No cure no pay has been received for accomodation '.$accom->name
					 ); 
					 return redirect('/accomodations/my-accomodations')->with('success',__('Your accomodation successfully converted to no cure no pay,wating admin approval')); 
				} else {
					return redirect()->back()->with('error',__('Internal Error,accomodation not updated'));
				}
			}
		}
	}
	public function getSwitchToPaid(){  
		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->wherePaymentType(2)->whereApproved(1)->whereCompleted(1)->pluck('name','id');
		if(!$accoms->isEmpty()) { 
			return view('subscriptions.switch_to_paid',compact('accoms'));	
		}
		return redirect()->back()->with('error',__('Sorry,no valid accomodations found'));
	}
	public function postSwitchToPaid(Request $r){ 
		if($r->accom_id) {
			$accom=Accomodation::whereId($r->accom_id)->first();
			if($accom) {
				$accom->payment_type=1;
				$accom->subs_valid_license=0;
				if($accom->save()) {
					\Log::log('AccomodationSwitch','switch request to paid created for accomID:'.$r->accom_id); 
					return redirect('/accomodations/pay/'.$r->accom_id)->with('success',__('Your accomodation successfully converted to paid,please select subscription to continue')); 
				} else {
					return redirect()->back()->with('error',__('Internal Error,accomodation not updated'));
				}
			}
		}
	}

	public function notifyAdminsForSwitchRequest($admins,$subject,$msg) {
		if($admins) {
			foreach ($admins as $a) {
				try {
					Mail::to($a->email)->send(new NotifyAdminsForSwitchRequest($subject,$msg));     
				} catch(Exception $e) {
					\Log::log('notifyAdminsForSwitchRequest','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage()); 
				}
			}
		}
	}

}
