<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use LBV\Model\ExtraTab;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables; 
use Illuminate\Support\Facades\Auth;
use LBV\Mail\NotifyAdminsForExtraTabRequest;
use LBV\Custom\Notifications;

class ExtraTabsController extends Controller
{
   public function getAccomAdd() {
   		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->whereCompleted(1)->whereApproved(1)->whereSubsValidLicense(1)->orderBy('id','asc')->pluck('name','id'); 
         if(!$accoms->count()) { 
            return redirect()->back()->with('error',__('Sorry you must have a valid accomodation to add extra tab'));
         } 
   		return view('extra_tabs.add',compact('accoms')); 
   }
   public function postAccomAdd(Request $r) {
   		$this->validate($r,[ 
   				'title'=>'required', 
   				'content'=>'required' 
   			]
   		);
         $count=ExtraTab::whereAccomId($r->accom_id)->count();
         if($count>2){
            return redirect()->back()->with('error',__('Sorry you can add max.3 tabs per accomodation'));
         }
   		$status=1;
   		if($this->checkAccomPaymentType($r->accom_id)==2) {
   			$status=0;
   			 $this->notifyAdminsForExtraTabRequest(
   			         'New extra tab request has been received.',
   			         'New extra tab request has been received for accomodation '.$this->propertyName($r->accom_id)
   			 );  
   			Notifications::notifyAdmin([ 
   			        'accom_id'=>$r->accom_id,
   			        'from_user'=>Auth::user()->id,
   			        'message'=>'New extra tab request has been received for accomodation '.$this->propertyName($r->accom_id),
   			        'request_type_url'=>'/extra-tab/all-tabs'
   			]);  
   		} 
   		$ExtraTab=new ExtraTab();
   		$ExtraTab->accom_id=$r->accom_id;
   		$ExtraTab->page_name=$r->title;
   		$ExtraTab->tab_content=$r->content;
   		$ExtraTab->status=$status;
   		if($ExtraTab->save()) {
   			return redirect('/extra-tabs')->with('success',__('Extra Tab saved successfully'));
   		} else {
   			return redirect()->back()->with('error',__('Sorry,operation failed,please try again!'));
   		}
   } 
   public function getAdvExtraTabs(){
   		return view('extra_tabs.index');
   }
   public function deleteTab($id){
      if($id) {
         $tab=ExtraTab::whereId($id)->first();
         if($tab) {
            if($tab->delete()) {
               return redirect()->back()->with('success',__('Tab deleted successfully'));
            }
            return redirect()->back()->with('error',__('Tab could not delete,please try again!'));
         } 
      } 
   }

   public function notifyAdminsForExtraTabRequest($subject,$msg) {
      $admins=DB::table('users')->select('email')->where('role_id',1)->get();
   	if($admins) {
   		foreach ($admins as $a) {
   			try {
   				Mail::to($a->email)->send(new NotifyAdminsForExtraTabRequest($subject,$msg));     
   			} catch(Exception $e) {
   				\Log::log('notifyAdminsForExtraTabRequest','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage()); 
   			}
   		}
   	}
   }
   public function getAdvertiserTabs(Request $r){ 
   		$accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->pluck('id','id');
   		$status=0;
   		if($r->type_id) {
   			$status=$r->type_id;
   		}
   		$tabs=ExtraTab::whereIn('accom_id',$accoms)->whereStatus($status)->select('extra_tabs.page_name','extra_tabs.id','extra_tabs.accom_id','accomodations.name as name','extra_tabs.status')
	   		->leftJoin('accomodations', 'extra_tabs.accom_id', '=', 'accomodations.id')
	   		->get(); 
            // dd($tabs);
   		return Datatables::of($tabs) 
   		->addColumn('action', function ($a) { 
            $str='<a href="/extra-tabs/view/'.$a->id.'" class="btn btn-xs btn-info" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>';    
            if($a->status==1) { 
               if($this->checkAccomPaymentType($a->accom_id)==1) {
                  $str.='&nbsp;<a href="/extra-tabs/edit/'.$a->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
               } else {
                  $str.='&nbsp;<a href="/extra-tabs/edit-free/'.$a->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
               }
               $str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>';    
            } 
   			return $str;    
   		}) 
         //die; 
   		->escapeColumns([])
   		->make(true);  
   } 

   public function getTabView($id){
      if($id) {
         $tab=ExtraTab::whereId($id)->first(); 
         if($tab) { 
            $accom=$this->propertyName($tab->accom_id); 
            return view('extra_tabs.view',compact('tab','accom'));
         } else {
            return redirect()->back()->with('error',__('No data found')); 
         }
      }
   }
   public function getTabViewUpdate($id){ 
      if($id) {
         $tab=DB::table('edit_extra_tabs')->whereId($id)->first(); 
         if($tab) { 
            $accom=$this->propertyName($tab->accom_id); 
            return view('extra_tabs.view_update',compact('tab','accom'));
         } else {
            return redirect()->back()->with('error',__('No data found')); 
         }
      }
   }

   public function getTabEdit($id){ 
      $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->pluck('name','id');
      if($id) {
         $tab=ExtraTab::whereId($id)->first(); 
         if($tab) { 
            $accomName=$this->propertyName($tab->accom_id);
            return view('extra_tabs.edit',compact('tab','accoms','accomName'));
         } else {
            return redirect()->back()->with('error',__('No data found')); 
         }
      }
   }
  
   public function getTabEditFree($id){
      $accoms=DB::table('accomodations')->whereUserId(Auth::user()->id)->pluck('name','id');
      if($id) {
         $already=DB::table('edit_extra_tabs')->whereExtraTabId($id)->whereStatus(0)->first();
         if($already) {
            return redirect()->back()->with('error',__('Sorry request already sent'));
         }
         $tab=ExtraTab::whereId($id)->first(); 
         if($tab) { 
            return view('extra_tabs.edit_free',compact('tab','accoms'));
         } else {
            return redirect()->back()->with('error',__('No data found')); 
         }
      }
   }

   public function postTabEditFree(Request $r,$id){
      if($id) {
         $this->validate($r,[ 
               'title'=>'required', 
               'content'=>'required' 
            ]
         ); 
         $Old=ExtraTab::whereId($id)->first();
         $tab=DB::table('edit_extra_tabs')->insertGetId([ 
            'accom_id'=>$Old->accom_id,
            'extra_tab_id'=>$id,
            'page_name'=>$r->title,
            'tab_content'=>$r->content,
            'status'=>0,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
         ]);
         
         if($tab) { 
            Notifications::notifyAdmin([ 
                    'accom_id'=>$Old->accom_id,
                    'from_user'=>Auth::user()->id,
                    'message'=>'New extra tab update request has been received for accomodation '.$this->propertyName($Old->accom_id),
                    'request_type_url'=>'/extra-tab/tabs-update' 
            ]); 
             $this->notifyAdminsForExtraTabRequest(
                     'New extra tab udpate request has been received.',
                     'New extra tab update request has been received for accomodation '.$this->propertyName($Old->accom_id)
             ); 
             if(Auth::user()->role_id==1) {
               return redirect('/extra-tab/all-tabs')->with('success',__('Tab updated successfully'));
             } else {
               return redirect('/extra-tabs')->with('success',__('Tab update request sent successfully,wating admin approval'));
             } 
         } 
         return redirect()->with('error',__('Sorry,request not completed,try again!'));
      }
   }
      public function listAllExtraTabsEdit(){
         return view('extra_tabs.update'); 
      } 
      public function getAllTabsEdit(){
         $tabs=DB::table('edit_extra_tabs')->whereStatus(0)->select('edit_extra_tabs.page_name','edit_extra_tabs.id','accomodations.name as name','edit_extra_tabs.status')
               ->leftJoin('accomodations', 'edit_extra_tabs.accom_id', '=', 'accomodations.id')
               ->get(); 
         return Datatables::of($tabs) 
         ->addColumn('action', function ($a) {    
            $str='<a href="/extra-tabs/view-update/'.$a->id.'" class="btn btn-xs btn-info" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>'; 
            $str.='&nbsp;<a href="/extra-tabs/approve/'.$a->id.'" class="btn btn-xs btn-success" title="Approve"><i class="glyphicon glyphicon-thumbs-up"></i> </a>'; 
            $str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$a->id.');" href="javascript:;" class="btn btn-xs btn-danger" title="Reject"> <i class="glyphicon glyphicon-thumbs-down"></i> </a>'; 
            return $str;   
         })   
         ->escapeColumns([])
         ->make(true); 
      }
   public function postTabEdit(Request $r,$id){
      if($id) {
         $tab=ExtraTab::whereId($id)->first();
         $tab->page_name=$r->title;
         $tab->tab_content=$r->content;
         if($tab->save()) {
            if(Auth::user()->role_id==1) {
               return redirect('/extra-tab/all-tabs')->with('success',__('Tab updated successfully'));
            }else {
               return redirect('/extra-tabs')->with('success',__('Tab updated successfully'));
            }
         } 
         return redirect()->with('error',__('Sorry,request not completed,try again!'));
      }
   }

   public function approveTab($id){
      if($id) {
         $tab=ExtraTab::whereId($id)->first();
         if($tab) {
            $tab->status=1;
            if($tab->save()) {
               $user=app('LBV\Http\Controllers\AwardsController')->getUserByAccom($tab->accom_id);
               Notifications::notifyUser([  
                        'accom_id'=>$tab->accom_id,
                        'message'=>'Your extra tab request has been approved for accomodation '.$this->propertyName($tab->accom_id),  
                        'to_user'=>$user,  
                        'request_type_url'=>'/extra-tabs'
               ]); 
               return redirect()->back()->with('success',__('Tab aproved successfully'));
            }
         } else {
            return redirect()->back()->with('error',__('No data found'));   
         }
         return redirect()->back()->with('error',__('Sorry,request not completed,try again!'));
      } 
   }
   public function approveTabUpdate($id){
      if($id) { 
         $tab=DB::table('edit_extra_tabs')->whereId($id)->first();
         if($tab) {
            $tabUpdate=DB::table('edit_extra_tabs')->whereId($id)->update(['status'=>1]);
            if($tabUpdate) {
               $update=ExtraTab::whereId($tab->extra_tab_id)->first();
               if($update) {
                  $update->page_name=$tab->page_name;
                  $update->tab_content=$tab->tab_content;
                  if($update->save()) {
                     $user=app('LBV\Http\Controllers\AwardsController')->getUserByAccom($tab->accom_id);
                     Notifications::notifyUser([  
                              'accom_id'=>$tab->accom_id,
                              'message'=>'Your extra tab update request has been approved for accomodation '.$this->propertyName($tab->accom_id),  
                              'to_user'=>$user,  
                              'request_type_url'=>'/extra-tabs'
                     ]); 
                     return redirect()->back()->with('success',__('Tab aproved successfully'));
                  } else {
                     return redirect()->back()->with('error',__('Internal Error,unable to approve'));         
                  }
               } else {
                  return redirect()->back()->with('error',__('No data found'));         
               }
            }
         } else {
            return redirect()->back()->with('error',__('No data found'));   
         }
         return redirect()->back()->with('error',__('Sorry,request not completed,try again!'));
      } 
   }
   public function rejectTab($id){
      if($id) {
         $tab=ExtraTab::whereId($id)->first();
         if($tab) {
            $tab->status=2;
            if($tab->save()) {
               $user=app('LBV\Http\Controllers\AwardsController')->getUserByAccom($tab->accom_id);
               Notifications::notifyUser([  
                        'accom_id'=>$tab->accom_id,
                        'message'=>'Your extra tab request has been rejected for accomodation '.$this->propertyName($tab->accom_id),  
                        'to_user'=>$user,  
                        'request_type_url'=>'/extra-tabs' 
               ]); 
               return redirect()->back()->with('success',__('Tab rejected successfully'));
            }
         } else {
            return redirect()->back()->with('error',__('No data found'));   
         } 
         return redirect()->back()->with('error',__('Sorry,request not completed,try again!'));
      }
   }
   public function rejectTabUpdate($id){
      if($id) {
         $tab=DB::table('edit_extra_tabs')->whereId($id)->first();
         if($tab) {
            $tab->status=2;
            if($tab->save()) { 
               $user=app('LBV\Http\Controllers\AwardsController')->getUserByAccom($tab->accom_id);
               Notifications::notifyUser([  
                        'accom_id'=>$tab->accom_id,
                        'message'=>'Your extra tab update request has been rejected for accomodation '.$this->propertyName($tab->accom_id),  
                        'to_user'=>$user,  
                        'request_type_url'=>'/extra-tabs' 
               ]); 
               return redirect()->back()->with('success',__('Tab rejected successfully'));
            }
         } else {
            return redirect()->back()->with('error',__('No data found'));   
         } 
         return redirect()->back()->with('error',__('Sorry,request not completed,try again!'));
      }
   } 
   public function listAllExtraTabs(){
      return view('extra_tabs.all');
   }
   public function getAllTabs(){
      $tabs=ExtraTab::whereStatus(0)->select('extra_tabs.page_name','extra_tabs.id','accomodations.name as name','extra_tabs.status')
            ->leftJoin('accomodations', 'extra_tabs.accom_id', '=', 'accomodations.id')
            ->get(); 
      return Datatables::of($tabs) 
      ->addColumn('action', function ($a) {    
         $str='<a href="/extra-tabs/view/'.$a->id.'" class="btn btn-xs btn-info" title="View"><i class="glyphicon glyphicon-eye-open"></i> </a>';
         $str.='&nbsp;<a href="/extra-tabs/edit/'.$a->id.'" class="btn btn-xs btn-primary" title="Edit"><i class="glyphicon glyphicon-edit"></i> </a>';
         
         $str.='&nbsp;<a href="/extra-tabs/approve/'.$a->id.'" class="btn btn-xs btn-success" title="Approve"><i class="glyphicon glyphicon-thumbs-up"></i> </a>'; 
         $str .='&nbsp;<a href="/extra-tabs/reject/'.$a->id.'" class="btn btn-xs btn-danger" title="Reject"> <i class="glyphicon glyphicon-thumbs-down"></i> </a>'; 
         return $str;    
      })  
      ->escapeColumns([])
      ->make(true);  
   } 
   public function checkAccomPaymentType($accom) {
   		$accom=DB::table('accomodations')->select('payment_type')->whereId($accom)->first();
   		return $accom->payment_type;
   }
   public function propertyName($value) {
   	$name=DB::table('accomodations')->select('name')->whereId($value)->first();
   	return $name->name;
   }
}
