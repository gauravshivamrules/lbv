<?php
namespace LBV\Http\Controllers;

use DB;
use LBV\Model\Room;
use LBV\Model\RoomPrice;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use LBV\Model\AccomodationPrice as AP;
use LBV\Model\Accomodation;
use LBV\Http\Requests\AccomationPriceAddRequest as APAR; 
use LBV\Http\Requests\AccomationPriceEditRequest as APER; 
use LBV\Model\AccomodationPriceExtraCharge as EXTRACHARGE;
 
class AccomodationPricesController extends Controller
{
    public function getAccomPricesAdd(Request $r,$accom) {  
        $accomType=$r->query('type');
        $CHANGEDAY=$PRICEUNIT='';
        $HASPRICES=false;
        $allPrices=[];
        $ap=AP::where('accom_id',$accom)->first();
        $SALERENT=Accomodation::select(['label_id'])->whereId($accom)->first();
        $LABEL=$SALERENT->label_id;
        if($ap) { 
            if($ap->changeday) {
                $CHANGEDAY=$ap->changeday;
                $HASPRICES=true;
                $PRICEUNIT=$ap->price_unit;
                if($PRICEUNIT!='') {
                  switch ($PRICEUNIT) {
                    case 'perweek':
                      $allPrices=AP::where('accom_id',$accom)->get()->toArray();
                      break;
                    case 'perroom':
                      $allPrices=Room::with('room_price')->where('accom_id',$accom)->get()->toArray();
                      break;
                    case 'perperson':
                      $allPrices=Room::with('room_price')->where('accom_id',$accom)->get()->toArray();
                      break;
                  }
                  
                }       
            } 
        }
        $extraPrices=EXTRACHARGE::where('accom_id',$accom)->get()->toArray(); 
        return view('accomodations.add_accom_price',compact('accomType','accom','CHANGEDAY','HASPRICES','PRICEUNIT','LABEL','extraPrices','allPrices'));    
    }
    public function getAccomPricesEdit(Request $r,$accom) {  
       $accomType=$r->query('type');
       $CHANGEDAY=$PRICEUNIT='';
       $HASPRICES=false;
       $allPrices=[];
       $ap=AP::where('accom_id',$accom)->first();
       $SALERENT=Accomodation::select(['label_id'])->whereId($accom)->first();
       $LABEL=$SALERENT->label_id;
       if($LABEL==2) {
          $allPrices=$ap;
       } else {
        if($ap) { 
            if($ap->changeday) {
                $CHANGEDAY=$ap->changeday;
                $HASPRICES=true;
                $PRICEUNIT=$ap->price_unit;
                if($PRICEUNIT!='') {
                  switch ($PRICEUNIT) {
                    case 'perweek':
                      $allPrices=AP::where('accom_id',$accom)->get()->toArray();
                      break;
                    case 'perroom':
                      $allPrices=Room::with('room_price')->where('accom_id',$accom)->get()->toArray();
                      break;
                    case 'perperson':
                      $allPrices=Room::with('room_price')->where('accom_id',$accom)->get()->toArray();
                      break;
                  } 
                }
            } 
        }
       }
       $extraPrices=EXTRACHARGE::where('accom_id',$accom)->get()->toArray(); 
       return view('accomodations.edit_accom_price',compact('accomType','accom','CHANGEDAY','HASPRICES','PRICEUNIT','LABEL','extraPrices','allPrices'));  
    }
    public function getAccomodationPricesEdit(Request $r) {
      if($r->unit) {
          if($r->id) {
              if($r->unit=='perweek') {
                  $price=AP::whereId($r->id)->first();
                  $price->start_date=date('d-m-Y',strtotime($price->start_date));
                  $price->end_date=date('d-m-Y',strtotime($price->end_date));
              } else {
                  $price=Room::with('room_price')->whereId($r->id)->first();
                  if($price) {
                    $info=AP::select(['min_days'])->whereId($price->accom_price_id)->first();
                    if($price->room_price) {
                        foreach($price->room_price as $rp) {
                            $rp->start_date=date('d-m-Y',strtotime($rp->start_date));
                            $rp->end_date=date('d-m-Y',strtotime($rp->end_date));  
                        }
                    }
                    $price->min_days=$info->min_days;
                  }
              }
              echo json_encode($price);  
              exit;
          }
      }
    }
    public function postAccomPricesEdit(Request $r,APER $ap,$accom) {
        if($r->price_unit=='perweek' ) {
             $price=AP::where('id',$r->id)->first();
             $price->amount=$r->amount;
             $price->max_persons=$r->max_persons;
             $price->min_days=$r->min_days;
             $price->price_unit=$r->price_unit;
             $price->start_date=date('Y-m-d',strtotime($r->start_date));
             $price->end_date=date('Y-m-d',strtotime($r->end_date)).' 23:59:59';
             $price->extra_price_per_person=$r->extra_price_per_person;
             if($price->save()) {  
                 DB::table('accomodation_prices')->where('accom_id',$accom)->update(['changeday'=>$r->changeday]);
                 return redirect()->back()->with('success',__('Price updated successfuly'));
             }  
             return redirect()->back()->with('error',__('Sorry,price could not be updated,please try again!'));
        } else {
             if($r->price_unit !='') {
                if($r->price_unit=='perperson') {
                    DB::table('accomodation_prices')->where('id',$r->price_id)->update([
                                 'price_unit'=>$r->price_unit,
                                 'min_days'=>$r->min_days,
                                 'changeday'=>$r->changeday,
                                 'amount'=>$r->price1, 
                                 'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $room=DB::table('rooms')->where('id',$r->id)->update([
                                 'room_type'=>$r->room_type,
                                 'description'=>$r->description,
                                 'adults'=>$r->adults,
                                 'childs'=>$r->childs,
                                 'max_child_age'=>$r->max_child_age,
                                 'no_of_rooms'=>$r->no_of_rooms, 
                                 'min_days'=>$r->min_days,
                                 'max_adults_per_room'=>$r->max_adults_per_room,
                                 'max_persons_per_room'=>$r->max_adults_per_room,
                                 'updated_at' => date('Y-m-d H:i:s')       
                     ]);
                    if($room) {
                         $roomPrice=DB::table('room_prices')->where('id',$r->room_price_id)->update([
                                 'start_date'=>date('Y-m-d',strtotime($r->start_date)),
                                 'end_date'=>date('Y-m-d',strtotime($r->end_date)),
                                 'price1'=>$r->price1,
                                 'price2'=>$r->price2, 
                                 'price3'=>$r->price3,
                                 'extra_price_adult'=>$r->extra_price_adult,
                                 'extra_price_child'=>$r->extra_price_child, 
                                 'updated_at' => date('Y-m-d H:i:s')
                         ]);
                         if($roomPrice) {
                             return redirect()->back()->with('success',__('Price updated successfuly'));
                         } 
                         return redirect()->back()->with('error',__('Sorry,price could not be updated,please try again!'));
                    }
                                  
                }  else { 
                     if($r->price_unit=='perroom') {
                         DB::table('accomodation_prices')->where('id',$r->id)->update([
                                 'price_unit'=>$r->price_unit,
                                 'amount'=>$r->price,
                                 'min_days'=>$r->min_days_perroom,
                                 'changeday'=>$r->changeday,
                                 'updated_at' => date('Y-m-d H:i:s')
                         ]); 
                         DB::table('rooms')->where('id',$r->room_id)->update([
                                  'room_type'=>$r->room_type_perroom,
                                  'description'=>$r->description_perroom,
                                  'adults'=>$r->adults,
                                  'childs'=>$r->childs,
                                  'max_child_age'=>$r->max_child_age,
                                  'no_of_rooms'=>$r->no_of_rooms, 
                                  'updated_at' => date('Y-m-d H:i:s')   
                         ]);
                         $r=DB::table('room_prices')->where('id',$r->room_price_id)->update([
                                  'start_date'=>date('Y-m-d',strtotime($r->start_date_perroom)),
                                  'end_date'=>date('Y-m-d',strtotime($r->end_date_perroom)),
                                  'price'=>$r->amount,
                                  'updated_at' => date('Y-m-d H:i:s')
                         ]);
                          if($r) {
                              return redirect()->back()->with('success',__('Price updated successfuly'));
                          } 
                          return redirect()->back()->with('error',__('Sorry,price could not be updated,please try again!'));
                     } 
                 }   
             }
        }
    }
    public function postAccomPricesAdd(Request $r,APAR $ap,$accom) { 
        if( $this->checkAccomodationType($r->accom_type)=='perweek' ) {
            $price=new AP();
            $price->accom_id=$accom; 
            $price->amount=$r->amount;
            $price->max_persons=$r->max_persons;
            $price->min_days=$r->min_days;
            $price->price_unit='perweek';
            $price->start_date=date('Y-m-d',strtotime($r->start_date));
            $price->end_date=date('Y-m-d',strtotime($r->end_date)).' 23:59:59';
            $price->extra_price_per_person=$r->extra_price_per_person;
            // dd($price); 
            if($price->save()) {
                DB::table('accomodation_prices')->where('accom_id',$accom)->update(['changeday'=>$r->changeday]);
                return redirect()->back()->with('success',__('Price added successfuly'));
            } 
            return redirect()->back()->with('error',__('Sorry,price could not be added,please try again!'));
        } else {
                if($r->price_unit !='') {
                    if($r->price_unit=='perperson') {
                        $exists=Room::where('room_type',$r->room_type)->where('accom_id',$accom)->exists(); 
                        if(!$exists) {
                              $p=DB::table('accomodation_prices')->insertGetId([
                                'accom_id'=>$accom,
                                'price_unit'=>$r->price_unit,
                                'amount'=>$r->price1,
                                'min_days'=>$r->min_days,
                                'changeday'=>$r->changeday,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')       
                        ]); 
                       $room=DB::table('rooms')->insertGetId([
                                    'accom_id'=>$accom,
                                    'accom_price_id'=>$p,
                                    'room_type'=>$r->room_type,
                                    'description'=>$r->description,
                                    'adults'=>$r->adults,
                                    'childs'=>$r->childs,
                                    'max_child_age'=>$r->max_child_age,
                                    'no_of_rooms'=>$r->no_of_rooms, 
                                    'min_days'=>$r->min_days,
                                    'max_adults_per_room'=>$r->max_adults_per_room,
                                    'max_persons_per_room'=>$r->max_adults_per_room,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')       
                        ]);
                       if($room) {
                            $roomPrice=DB::table('room_prices')->insert([
                                    'accom_id'=>$accom,
                                    'room_id'=>$room,
                                    'start_date'=>date('Y-m-d',strtotime($r->start_date)),
                                    'end_date'=>date('Y-m-d',strtotime($r->end_date)).' 23:59:59',
                                    'price1'=>$r->price1,
                                    'price2'=>$r->price2, 
                                    'price3'=>$r->price3,
                                    'extra_price_adult'=>$r->extra_price_adult,
                                    'extra_price_child'=>$r->extra_price_child,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                            ]);
                            if($roomPrice) {
                                return redirect()->back()->with('success',__('Price added successfuly'));
                            } 
                            return redirect()->back()->with('error',__('Sorry,price could not be added,please try again!'));
                       }
                        }
                      
                                     
                    }  else {
                        if($r->price_unit=='perroom') {
                          $exists=Room::where('room_type',$r->room_type_perroom)->where('accom_id',$accom)->exists();
                          if(!$exists) { 
                              $p=DB::table('accomodation_prices')->insertGetId([
                                      'accom_id'=>$accom,
                                      'price_unit'=>$r->price_unit,
                                      'amount'=>$r->amount,
                                      'min_days'=>$r->min_days_perroom,
                                      'changeday'=>$r->changeday,
                                      'start_date'=>date('Y-m-d',strtotime($r->start_date_perroom)),
                                      'end_date'=>date('Y-m-d',strtotime($r->end_date_perroom)).' 23:59:59',
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'updated_at' => date('Y-m-d H:i:s')     
                              ]); 
                              $room=DB::table('rooms')->insertGetId([
                                       'accom_id'=>$accom,
                                       'accom_price_id'=>$p,
                                       'room_type'=>$r->room_type_perroom,
                                       'description'=>$r->description_perroom,
                                       'adults'=>$r->adults_per_room?$r->adults_per_room:$r->adults,
                                       'childs'=>$r->childs_per_room?$r->childs_per_room:$r->childs,
                                       'max_child_age'=>$r->max_child_age,
                                       'no_of_rooms'=>$r->no_of_rooms, 
                                       'max_adults_per_room'=>$r->max_adults_per_room,
                                       'max_persons_per_room'=>$r->max_adults_per_room,
                                       'created_at' => date('Y-m-d H:i:s'),
                                       'updated_at' => date('Y-m-d H:i:s')       
                               ]);
                              if($room) {
                                   $roomPrice=DB::table('room_prices')->insert([
                                           'accom_id'=>$accom,
                                           'room_id'=>$room,
                                           'price'=>$r->amount, 
                                           'start_date'=>date('Y-m-d',strtotime($r->start_date_perroom)),
                                           'end_date'=>date('Y-m-d',strtotime($r->end_date_perroom)).' 23:59:59',
                                           'extra_price_adult'=>$r->extra_price_adult,
                                           'extra_price_child'=>$r->extra_price_child,
                                           'created_at' => date('Y-m-d H:i:s'),
                                           'updated_at' => date('Y-m-d H:i:s')
                                   ]);
                                   if($roomPrice) {
                                       return redirect()->back()->with('success',__('Price added successfuly'));
                                   } 
                                   return redirect()->back()->with('error',__('Sorry,price could not be added,please try again!'));
                              }
                          } else {
                            return redirect()->back()->with('error',__('Sorry,this room type is already registered'));
                          }
                            
                        } 
                    }   
                }
        }
    } 
    public function getAccomodationPrices(Request $r) {
      $prices = AP::where('accom_id', $r->query('accom'))->get()->toArray(); 
    }

    public function getAccomCalendarPrices(Request $r) {
        $jsonArr=array();
        if($r->query('accom')) {
            $priceType=AP::select(['price_unit','min_days','changeday'])->where('accom_id',$r->query('accom'))->first(); 
            if($priceType) {
                if($priceType->price_unit=='perweek') {
                    $prices=AP::where('accom_id',$r->query('accom') )->get(); 
                    $i=0;
                    foreach ($prices as $p) {
                        $hover=strtoupper($p->price_unit).' | € '.$p->amount.' | '.'Min Days: '.$p->min_days.PHP_EOL.'  ChangeDay: '.ucfirst($p->changeday).PHP_EOL.
                            '| Extra Price Per Person: (€) '.$p->extra_price_per_person.' | Start: '.date('d-M-Y',strtotime($p->start_date)).' | End: '.date('d-M-Y',strtotime($p->end_date));
                        $title=strtoupper($p->price_unit).' | €'.$p->amount.' Max Persons: '.$p->max_persons.' Min Days: '.$p->min_days. ' Extra Price Per Person (€)'.$p->extra_price_per_person;

                        $jsonArr[$i]['price_id']=$p->id; 
                        $jsonArr[$i]['title']=$title; 
                        $jsonArr[$i]['start']=$p->start_date;
                        $jsonArr[$i]['end']=$p->end_date;
                        $jsonArr[$i]['hover']=$hover;
                        $jsonArr[$i]['allDay']=false;
                        $jsonArr[$i]['min_days']=$p->min_days;
                        $jsonArr[$i]['max_persons']=$p->max_persons;
                        $jsonArr[$i]['amount']=$p->amount;
                        $jsonArr[$i]['extra_price_per_person']=$p->extra_price_per_person;
                        $jsonArr[$i]['start_date']=$p->start_date;
                        $jsonArr[$i]['end_date']=$p->end_date;
                        $jsonArr[$i]['price_unit']=$priceType->price_unit;  
                        $i++;
                    }
                } else  {
                   $p=Room::with('room_price')->where('accom_id',$r->query('accom') )->get(); 
                   $roomPrices=$p->toArray(); 
                   $i=0;
                   foreach ($roomPrices as $p) {
                        /*$hover='Price Type: '.ucfirst( $priceType->price_unit).' '.'Min Days: '.$priceType->min_days.PHP_EOL.'  ChangeDay: '.ucfirst($priceType->changeday).PHP_EOL.
                            ' Room Type: '.$p['room_type'].'  Start: '.date('d-M-Y',strtotime($p['room_price'][0]['start_date'])).' End: '.date('d-M-Y',strtotime($p['room_price'][0]['end_date'])).PHP_EOL.
                            ' Perperson/night(€): '.$p['room_price'][0]['price1'].PHP_EOL.'  2 Person/night(€):'.$p['room_price'][0]['price2'].PHP_EOL.' Family/night(€):'.$p['room_price'][0]['price3'];*/
                        
                        if($priceType->price_unit=='perroom') {
                            $title='Room Type: '.$p['room_type'].'  | €'.$p['room_price'][0]['price'].' Adults: '.$p['adults'].' Childs: '.$p['childs'];  
                            $jsonArr[$i]['amount']=$p['room_price'][0]['price'];
                            $hover='Price Type: '.ucfirst( $priceType->price_unit).' '.'Min Days: '.$priceType->min_days.PHP_EOL.'  ChangeDay: '.ucfirst($priceType->changeday).PHP_EOL.
                                ' Room Type: '.$p['room_type'].'  Start: '.date('d-M-Y',strtotime($p['room_price'][0]['start_date'])).' End: '.date('d-M-Y',strtotime($p['room_price'][0]['end_date'])).PHP_EOL.
                                ' Price(€): '.$p['room_price'][0]['price'].' Adults: '.$p['adults'].' Childs: '.$p['childs'];
                        } else {
                            $title='Room Type: '.$p['room_type'].'  | €'.$p['room_price'][0]['price1'].'  | €'.$p['room_price'][0]['price2'].'  | €'.$p['room_price'][0]['price3'].' Adults: '.$p['adults'].' Childs: '.$p['childs'];       
                            $jsonArr[$i]['price1']=$p['room_price'][0]['price1'];
                            $jsonArr[$i]['price2']=$p['room_price'][0]['price2'];
                            $jsonArr[$i]['price3']=$p['room_price'][0]['price3'];
                            $jsonArr[$i]['extra_price_adult']=$p['room_price'][0]['extra_price_adult'];
                            $jsonArr[$i]['extra_price_child']=$p['room_price'][0]['extra_price_child'];
                            $jsonArr[$i]['max_persons_per_room']=$p['max_persons_per_room'];
                            $hover='Price Type: '.ucfirst( $priceType->price_unit).' '.'Min Days: '.$priceType->min_days.PHP_EOL.'  ChangeDay: '.ucfirst($priceType->changeday).PHP_EOL.
                            ' Room Type: '.$p['room_type'].'  Start: '.date('d-M-Y',strtotime($p['room_price'][0]['start_date'])).' End: '.date('d-M-Y',strtotime($p['room_price'][0]['end_date'])).PHP_EOL.
                            ' Perperson/night(€): '.$p['room_price'][0]['price1'].PHP_EOL.'  2 Person/night(€):'.$p['room_price'][0]['price2'].PHP_EOL.' Family/night(€):'.$p['room_price'][0]['price3'].PHP_EOL.
                            ' Adults: '.$p['adults'].' Childs: '.$p['childs'];
                        }
                       $jsonArr[$i]['id']=$p['accom_price_id']; 
                       $jsonArr[$i]['room_id']=$p['id']; 
                       $jsonArr[$i]['room_price_id']=$p['room_price'][0]['id']; 
                       $jsonArr[$i]['title']=$title; 
                       $jsonArr[$i]['start']=$p['room_price'][0]['start_date'];
                       $jsonArr[$i]['start_date']=$p['room_price'][0]['start_date'];
                       $jsonArr[$i]['end']=$p['room_price'][0]['end_date'];
                       $jsonArr[$i]['end_date']=$p['room_price'][0]['end_date'];
                       $jsonArr[$i]['room_type']=$p['room_type'];
                       $jsonArr[$i]['description']=$p['description'];
                       $jsonArr[$i]['no_of_rooms']=$p['no_of_rooms'];
                       $jsonArr[$i]['adults']=$p['adults'];
                       $jsonArr[$i]['childs']=$p['childs'];
                       $jsonArr[$i]['hover']=$hover;
                       // $jsonArr[$i]['allDay']=false;
                       $jsonArr[$i]['min_days']=$priceType->min_days;
                       $jsonArr[$i]['price_unit']=$priceType->price_unit;
                       $i++;
                   } 
                }  
            }
            echo json_encode($jsonArr); 
        } 
    } 
    public function deleteAccomPrice($type,$id,$room=null,$roomPrice=null) {
        if($type=='perweek') { 
            if($id) {
              $price=AP::whereId($id)->firstOrFail();
              if($price) {
                if($price->delete()) {
                    return redirect()->back()->with('success',__('Price deleted successfully'));
                } 
                return redirect()->back()->with('error',__('Sorry,price could not be deleted,please try again!'));
              }
            }
        } else {
            if($id) {
              $price=AP::whereId($id)->firstOrFail();
              if($price) {
                if($price->delete()) {
                    $room=Room::whereId($room)->firstOrFail();
                    if($room->delete()) {
                        $rPrice=RoomPrice::whereId($roomPrice)->firstOrFail();
                        if($rPrice->delete()) {
                            return redirect()->back()->with('success',__('Price deleted successfully'));        
                        }
                    } 
                } 
                return redirect()->back()->with('error',__('Sorry,price could not be deleted,please try again!'));
              }
            }
        } 
    }
    public function deleteAccomExtraCharge($id) { 
        if($id) {
            $charge=EXTRACHARGE::whereId($id)->firstOrFail();
            if($charge) {
                if($charge->delete()) {
                    return redirect()->back()->with('success',__('Price deleted successfully'));
                }
                return redirect()->back()->with('error',__('Sorry,price could not be deleted,please try again!'));
            }
        }
    } 
    public function postSaleAccomPriceAdd(Request $r,$accom) {
        $acm=new AP(); 
        $acm->accom_id=$accom;
        $acm->amount=$r->get('amount');
        if($acm->save()) {
            return redirect('/accomodations/add_accom_images/'.$accom)->with('success',__('Price added successfuly'));
        } 
        return redirect()->back()->with('error',__('Sorry,price could not be added,please try again!'));
    }
    public function postSaleAccomPriceEdit(Request $r,$accom) {
        $acm=AP::where('accom_id',$accom)->first(); 
        $acm->amount=$r->get('amount');
        if($acm->save()) {
            return redirect('/accomodations/edit-accom-images/'.$accom)->with('success',__('Price updated successfuly')); 
        } 
        return redirect()->back()->with('error',__('Sorry,price could not be updated,please try again!'));
    }
    public function postExtraPricesAdd(Request $r,$accom) {
    	$r->validate([
    	        'amount' => 	'required',
    	        'description' => 'required',
    	        'is_optional' => 'required',
    	        'charge_type' => 'required',
    	    ]);
    	$e=new EXTRACHARGE();
    	$e->accom_id=$accom;
    	$e->amount=$r->get('amount');
    	$e->description=$r->get('description');
    	$e->is_optional=$r->get('is_optional');
    	$e->charge_type=$r->get('charge_type');
    	$is_per_person=0;
    	if($r->get('is_per_person')) {
    		$is_per_person=$r->get('is_per_person');
    	}
    	$e->is_per_person=$is_per_person; 
    	if($e->save()) { 
    		return redirect()->back()->with('success',__('Extra price added successfuly'));
    	}
    	return redirect()->back()->with('error',__('Sorry,extra price could not be added,please try again!'));
    } 
    public function getAccomExtraCharge($id) {
        $extra=EXTRACHARGE::whereId($id)->firstOrFail();
        if($extra) {
            echo json_encode($extra);
        }
    } 
    public function postAccomExtraCharge(Request $r) {
        $r->validate([
                'amount' =>     'required',
                'description' => 'required',
                'is_optional' => 'required',
                'charge_type_edit' => 'required',
            ]);
        $e=EXTRACHARGE::whereId($r->id)->firstOrFail();
        $e->amount=$r->get('amount');
        $e->description=$r->get('description');
        $e->is_optional=$r->get('is_optional');
        $e->charge_type=$r->get('charge_type_edit');
        $is_per_person=0;
        if($r->get('is_per_person')) {
            $is_per_person=$r->get('is_per_person');
        }
        $e->is_per_person=$is_per_person; 
        if($e->save()) { 
            return redirect()->back()->with('success',__('Price updated successfuly'));
        }
        return redirect()->back()->with('error',__('Sorry,price could not be added,please try again!'));
    }
    public function switchAccomodationPrice($accom) {
        if($accom) {
            $prices=AP::where('accom_id',$accom)->get();
            $priceType=$ERROR='';
            if($prices) {
                if(isset($prices[0])) {
                    $priceType=$prices[0]->price_unit;
                }
                foreach($prices as $p) {
                    if(!$p->delete()) {
                        $ERROR='ERROR';
                    }      
                }
                if($priceType!='perweek') {
                    $rooms=Room::where('accom_id',$accom)->get();
                    if($rooms) {
                        foreach($rooms as $r) {
                            if(!$r->delete()) {
                                $ERROR='ERROR';
                            }
                            
                        }    
                    }
                    $roomPrices=RoomPrice::where('accom_id',$accom)->get();
                    if($roomPrices) {
                        foreach($roomPrices as $r) {
                            if(!$r->delete()) {
                                $ERROR='ERROR';
                            }
                            
                        }
                        return redirect()->back()->with('success',__('All your prices are deleted,you can add new prices now.'));        
                    }
                    if($ERROR!='') { 
                        return redirect()->back()->with('error',__('Oops something went wrong,please try again!'));         
                    }     
                }
            }
        }
    }
    function checkAccomodationType($type) {
        if($type==1 || $type==5 || $type==6 || $type==8 || $type==9 || $type==12 ) {
            return 'perweek';
        } else {
            return 'notperweek'; 
        }
    }
}
