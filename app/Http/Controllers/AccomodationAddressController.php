<?php

namespace LBV\Http\Controllers;
use DB;
use Mail;
use LBV\Custom\Notifications;
use LBV\Model\Country;
use LBV\Model\Region;
use LBV\Model\Department;
use Yajra\Datatables\Datatables; 
use Illuminate\Support\Facades\Auth;
use LBV\Model\AccomodationAddress;
use LBV\Http\Requests\AccomodationLocationRequest as ALR;
use LBV\Mail\NotifyAdminsForNewLocationRequest;

use Illuminate\Http\Request;

class AccomodationAddressController extends Controller
{
	public function getRegions(Request $r) {
		$regions=Region::select(['id','region_name'])->orderBy('region_name','asc')->where('country_id',$r->country_id)->where('is_approved',1)->get()->toArray(); 
		echo json_encode($regions); 
	}
	public function getDepartments(Request $r) {
		$departments=Department::select(['id','department_name'])->orderBy('department_name','asc')->where('region_id',$r->region_id)->where('is_approved',1)->get()->toArray(); 
		echo json_encode($departments);  
	}
    public function getLocationAdd(Request $r,$accom) {
      $images=DB::table('gallery_images')->whereAccomId($accom)->first();
      if(!$images) {
        return redirect()->back()->with('error',__('Sorry,you must add images to continue')); 
      }
      $countries=Country::select(['id','name'])->orderBy('name','asc')->whereIsApproved(1)->where('is_approved',1)->pluck('name','id');    
    	return view('accomodations.add_accom_location',compact('countries','accom'));   
    }
    public function notifyAdminsForNewLocationRequest($subject,$msg) {
       $admins=DB::table('users')->select('email')->where('role_id',1)->get();
        if($admins) {
            foreach ($admins as $a) {
                try {
                    Mail::to($a->email)->send(new NotifyAdminsForNewLocationRequest($subject,$msg));     
                } catch(Exception $e) {
                    \Log::log('notifyAdminsForNewLocationRequest','Mail not sent to '.$a->email.' Server returned error: '.$e->getMessage());  
                }
            }
        }
    }
    public function getAddNewLocation(){
      $countries=Country::select(['id','name'])->orderBy('name','asc')->where('is_approved',1)->pluck('name','id');    
      return view('accomodations.add_new_location',compact('countries')); 
    }
    public function postAddNewLocation(Request $r){
      $this->validate($r,[
          'country_id'=>'required',
          'region_id'=>'required'
          //'department_id'=>'required'
        ]
      );
      $c=DB::table('countries')->insertGetId([
          'name'=>$r->country_id,
          'is_approved'=>1,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s')
      ]);
      if($c) {
        $rg=DB::table('regions')->insertGetId([
          'region_name'=>$r->region_id,
          'is_approved'=>1,
          'country_id'=>$c,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if($rg){
            $d=DB::table('departments')->insertGetId([
              'department_name'=>$r->department_id,
              'region_id'=>$rg,
              'is_approved'=>1,
              'created_at'=>date('Y-m-d H:i:s'),
              'updated_at'=>date('Y-m-d H:i:s')
            ]);
            return redirect()->back()->with('success',__('Location added successfully'));
        } 
      } else {
        return redirect()->back()->with('error',__('Internal error cannot save country,please try again!'));
      }
    }
    public function postAddNewRegion(Request $r){
      $this->validate($r,[
               'country_id'=>'required',
               'region_id'=>'required'
             ]
           );
       $rg=DB::table('regions')->insertGetId([
         'region_name'=>$r->region_id,
         'is_approved'=>1,
         'country_id'=>$r->country_id,
         'created_at'=>date('Y-m-d H:i:s'),
         'updated_at'=>date('Y-m-d H:i:s')
       ]); 
       if($rg){
          if($r->department_id) {
             $d=DB::table('departments')->insertGetId([
             'department_name'=>$r->department_id,
             'region_id'=>$rg,
             'is_approved'=>1,
             'created_at'=>date('Y-m-d H:i:s'),
             'updated_at'=>date('Y-m-d H:i:s')
           ]);  
          }
        return redirect()->back()->with('success',__('Location added successfully'));
       } 
           
    }
    public function postAddNewDepartment(Request $r){ 
      $this->validate($r,[
                     'country_id'=>'required',
                     'region_id'=>'required',
                     'department_id'=>'required'
                   ]
                 );
                   
       $d=DB::table('departments')->insertGetId([
         'department_name'=>$r->department_id,
         'region_id'=>$r->region_id,
         'is_approved'=>1,
         'created_at'=>date('Y-m-d H:i:s'),
         'updated_at'=>date('Y-m-d H:i:s')
       ]);
       if($d) { 
        return redirect()->back()->with('success',__('Location added successfully'));
       }
       return redirect()->back()->with('error',__('Sorry,Something went wrong,please try again!'));
                   
    }
    public function getNewLocations(){
        $countries=Country::whereIsApproved(0)->get()->toArray();
        $dpts=$rgns=[]; 
        if($countries) {
          $i=0;
          foreach ($countries as $l) {
              $region=Region::whereIsApproved(0)->whereCountryId($l['id'])->first();
              if($region) {
                $region=$region->toArray();
                $rgns[]=$region['id'];
                $countries[$i]['region']=$region;
              }
              $department=Department::whereIsApproved(0)->whereRegionId($region['id'])->first();
              if($department) {
                $department=$department->toArray();
                $dpts[]=$department['id'];
                $countries[$i]['department']=$department;
              }
              $i++;
          }
        }
        $regions=Region::whereIsApproved(0)->whereNotIn('id',$rgns)->get()->toArray();
        if($regions) {
           $r=0;
           foreach ($regions as $l) {
               $country=Country::whereId($l['country_id'])->first()->toArray();
               $department=Department::whereRegionId($l['id'])->first()->toArray();
               $dpts[]=$department['id'];
               $regions[$r]['country']=$country;
               $regions[$r]['department']=$department;
               $r++;
           }
        }
        $departments=Department::whereIsApproved(0)->whereNotIn('id',$dpts)->get()->toArray();
        if($departments) {
           $d=0;
           foreach ($departments as $l) {
               $region=Region::whereId($l['region_id'])->first()->toArray();
               $country=Country::whereId($region['country_id'])->first()->toArray();
               $departments[$d]['country']=$country;
               $departments[$d]['region']=$region;
               $d++;
           }
        }
        return view('accomodations.view_new_locations',compact('countries','regions','departments'));
    }
    public function getEditNewLocations($country,$region,$department){ 
      $cName=Country::select('name')->whereId($country)->first();
      $rName=Region::select('region_name')->whereId($region)->first();
      $dName=Department::select('department_name')->whereId($department)->first();
      return view('accomodations.edit_new_location',compact('country','region','department','cName','rName','dName'));
    }
    public function postEditNewLocations(Request $r,$country,$region,$department){
        $cUpdate=DB::table('countries')->whereId($country)->update([
            'name'=>$r->country_name,
            'is_approved'=>1,
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if($cUpdate) {
          $rUpdate=DB::table('regions')->whereId($region)->update([
              'region_name'=>$r->region_name,
              'is_approved'=>1,
              'updated_at'=>date('Y-m-d H:i:s')
          ]);
          if($rUpdate) {
              $dUpdate=DB::table('departments')->whereId($department)->update([
                  'department_name'=>$r->department_name,
                  'is_approved'=>1,
                  'updated_at'=>date('Y-m-d H:i:s')
              ]);
              if($dUpdate) {
                return redirect('/accomodations/view-new-location')->with('success',__('Location approved and updated successfully'));
              } else {
                return redirect()->back()->with('error',__('Internal error cannot update department,please try again!'));
              }
          } else {
            return redirect()->back()->with('error',__('Internal error cannot update region,please try again!'));
          }
        } else {
          return redirect()->back()->with('error',__('Internal error cannot update country,please try again!'));
        }
    } 
    public function postLocationAdd(Request $r,ALR $a,$accom) {  
      //dd($r);
        if(is_numeric($r->country_id) && is_numeric($r->region_id)) {
                if($r->department_id) {
                        if(is_numeric($r->department_id)) {
                              $location=new AccomodationAddress(); 
                              $location->accom_id=$accom; 
                              $location->longitude=$r->lng;
                              $location->lattitude=$r->lat;
                              $location->country_id=$r->country_id;
                              $location->region_id=$r->region_id;
                              $location->department_id=$r->department_id;
                              $location->street=$r->street;
                              $location->street_number=$r->street_number;
                              $location->city=$r->city_town;
                              $location->postal_code=$r->postal_code;
                              if($location->save()) {
                                  return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                              }
                              return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!'));        
                        } else {
                            if($r->other_department && $r->department_id=='other') {
                              if($this->checkUniqueLocation(null,null,$r->other_department)) {
                                 return redirect()->back()->with('error',__('Department already exists'));
                              }
                                $newDepartment=DB::table('departments')->insertGetId([
                                    'department_name'=>$r->other_department,
                                    'region_id'=>$r->region_id,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s')
                                ]);
                                if($newDepartment){
                                    $this->notifyAdminsForNewLocationRequest(
                                             'New accomodation location request has been received.',
                                             'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                                     );  
                                    Notifications::notifyAdmin([  
                                            'accom_id'=>$accom,
                                            'from_user'=>Auth::user()->id,
                                            'message'=>'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                            'request_type_url'=>'/accomodations/view-new-location'
                                    ]);  

                                    $location=new AccomodationAddress(); 
                                    $location->accom_id=$accom; 
                                    $location->longitude=$r->lng;
                                    $location->lattitude=$r->lat;
                                    $location->country_id=$r->country_id;
                                    $location->region_id=$r->region_id;
                                    $location->department_id=$newDepartment;
                                    $location->street=$r->street;
                                    $location->street_number=$r->street_number;
                                    $location->city=$r->city_town;
                                    $location->postal_code=$r->postal_code;
                                    if($location->save()) {
                                        return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                                    }
                                    return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!'));   
                                }

                            } else {
                              $location=new AccomodationAddress(); 
                              $location->accom_id=$accom; 
                              $location->longitude=$r->lng;
                              $location->lattitude=$r->lat;
                              $location->country_id=$r->country_id;
                              $location->region_id=$r->region_id;
                              $location->street=$r->street;
                              $location->street_number=$r->street_number;
                              $location->city=$r->city_town;
                              $location->postal_code=$r->postal_code;
                              if($location->save()) {
                                  return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                              }
                              return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!')); 
                            }
                        } 
                } else {
                  $location=new AccomodationAddress(); 
                  $location->accom_id=$accom; 
                  $location->longitude=$r->lng;
                  $location->lattitude=$r->lat;
                  $location->country_id=$r->country_id;
                  $location->region_id=$r->region_id;
                  $location->street=$r->street;
                  $location->street_number=$r->street_number;
                  $location->city=$r->city_town;
                  $location->postal_code=$r->postal_code;
                  if($location->save()) {
                      return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                  }
                  return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!'));      
                }
        } else {
           if($r->country_id=='other') {
                if($this->checkUniqueLocation($r->other_country,$r->other_region,$r->other_department)) {
                   return redirect()->back()->with('error',__('Location already exists'));
                }
               $location=new AccomodationAddress(); 
               $newCountry=DB::table('countries')->insertGetId([
                   'name'=>$r->other_country,
                   'created_at'=>date('Y-m-d H:i:s'),
                   'updated_at'=>date('Y-m-d H:i:s')
               ]);
               if($newCountry) {
                   $newRegion=DB::table('regions')->insertGetId([
                       'region_name'=>$r->other_region,
                       'country_id'=>$newCountry,
                       'created_at'=>date('Y-m-d H:i:s'),
                       'updated_at'=>date('Y-m-d H:i:s')
                   ]);
                   if($newRegion){
                       //if($r->other_department && $r->department_id=='other') {
                           $newDepartment=DB::table('departments')->insertGetId([
                               'department_name'=>$r->other_department,
                               'region_id'=>$newRegion,                               
                               'created_at'=>date('Y-m-d H:i:s'),
                               'updated_at'=>date('Y-m-d H:i:s')
                           ]);
                           $location->accom_id=$accom; 
                           $location->longitude=$r->lng;
                           $location->lattitude=$r->lat;
                           $location->country_id=$newCountry;
                           $location->region_id=$newRegion;
                           $location->department_id=$newDepartment;
                           $location->street=$r->street;
                           $location->street_number=$r->street_number;
                           $location->city=$r->city_town;
                           $location->postal_code=$r->postal_code;
                       }
                       
                   //}
                   if($location->save()) {
                        $this->notifyAdminsForNewLocationRequest(
                                 'New accomodation location request has been received.',
                                 'New accomodation location country request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                         );  
                        Notifications::notifyAdmin([  
                                'accom_id'=>$accom,
                                'from_user'=>Auth::user()->id,
                                'message'=>'New accomodation location country request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                'request_type_url'=>'/accomodations/view-new-location'
                        ]); 
                       return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                   }
                   return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!'));    

               } else {
                   return redirect()->back()->with('error',__('Internal error, cannot save country,please try again!'));
               }
           } else if($r->region_id=='other'){
               if($this->checkUniqueLocation(null,$r->other_region,$r->other_department)) {
                  return redirect()->back()->with('error',__('Region/Department already exists'));
               }
                $location=new AccomodationAddress(); 
                $newRegion=DB::table('regions')->insertGetId([
                    'region_name'=>$r->other_region,
                    'country_id'=>$r->country_id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                if($newRegion) {
                    $newDepartment=null;
                    if($r->other_department) {
                      $newDepartment=DB::table('departments')->insertGetId([
                          'department_name'=>$r->other_department,
                          'region_id'=>$newRegion,                               
                          'created_at'=>date('Y-m-d H:i:s'),
                          'updated_at'=>date('Y-m-d H:i:s')
                      ]);
                    }
                    $location->accom_id=$accom; 
                    $location->longitude=$r->lng;
                    $location->lattitude=$r->lat;
                    $location->country_id=$r->country_id;
                    $location->region_id=$newRegion;
                    $location->department_id=$newDepartment;
                    $location->street=$r->street;
                    $location->street_number=$r->street_number;
                    $location->city=$r->city_town;
                    $location->postal_code=$r->postal_code;
                    if($location->save()) {
                         $this->notifyAdminsForNewLocationRequest(
                                  'New accomodation location request has been received.',
                                  'New accomodation location region request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                          );  
                         Notifications::notifyAdmin([  
                                 'accom_id'=>$accom,
                                 'from_user'=>Auth::user()->id,
                                 'message'=>'New accomodation location region request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                 'request_type_url'=>'/accomodations/view-new-location'
                         ]); 
                        return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                    }
                    return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!'));  
                }
           } else if($r->department_id=='other'){
                if($this->checkUniqueLocation(null,null,$r->other_department)) {
                   return redirect()->back()->with('error',__('Department already exists'));
                } 
                $location=new AccomodationAddress(); 
                $newDepartment=DB::table('departments')->insertGetId([
                    'department_name'=>$r->other_department,
                    'region_id'=>$r->region_id,                               
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                $location->accom_id=$accom; 
                $location->longitude=$r->lng;
                $location->lattitude=$r->lat;
                $location->country_id=$r->country_id;
                $location->region_id=$r->region_id;
                $location->department_id=$newDepartment;
                $location->street=$r->street;
                $location->street_number=$r->street_number;
                $location->city=$r->city_town;
                $location->postal_code=$r->postal_code;
                if($location->save()) {
                     $this->notifyAdminsForNewLocationRequest(
                              'New accomodation location request has been received.',
                              'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                      );  
                     Notifications::notifyAdmin([  
                             'accom_id'=>$accom,
                             'from_user'=>Auth::user()->id,
                             'message'=>'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                             'request_type_url'=>'/accomodations/view-new-location'
                     ]); 
                    return redirect('/accomodations/add_accom_amnities/'.$accom)->with('success',__('Accomodation location saved successfully'));
                }
                return redirect()->back()->with('error',__('Sorry,accomodation location could not be saved,please try again!')); 
           }    
        }
    }
    public function getLocationEdit(Request $r,$accom) {
        $address=AccomodationAddress::where('accom_id',$accom)->first(); 
        //dd($address);
        if($address) {
            $regions=Region::select(['id','region_name'])->orderBy('region_name','asc')->where('country_id',$address->country_id)->pluck('region_name','id');
            if($address->region_id) {
              $departments=Department::select(['id','department_name'])->orderBy('department_name','asc')
                                              ->where('region_id',$address->region_id)->pluck('department_name','id');      
            }   
        }
        
        $countries=Country::select(['id','name'])->orderBy('name','asc')->pluck('name','id'); 
        $IsApprovedCountry=$isApprovedRegion=$isApprovedDepartment=$departmentName=$regionName=$countryName=null;
        if($address) {
            $c=Country::whereId($address->country_id)->first();
            if($c) {
              $IsApprovedCountry=$c->is_approved;
              $countryName=$c->name;
            }
            $rg=Region::whereId($address->region_id)->first();
            if($rg) {
               $isApprovedRegion=$rg->is_approved;
               $regionName=$rg->region_name;
            }
            if($address->department_id) {
              $d=Department::whereId($address->department_id)->first();
              if($d) {
                $isApprovedDepartment=$d->is_approved;
                $departmentName=$d->department_name;  
              }
            }
            
        } 
        return view('accomodations.edit_accom_location',compact('countries','accom','address','departments','regions',
            'IsApprovedCountry','isApprovedRegion','isApprovedDepartment','countryName','regionName','departmentName'));       
    }
    public function checkUniqueLocation($country=null,$region=null,$department=null){
        if($country) {
           return Country::whereName($country)->exists();
        }
        if($region) {
          return Region::whereRegionName($region)->exists();
        }
        if($department) {
          return Department::whereDepartmentName($department)->exists(); 
        }  
    } 
    public function postLocationEdit(Request $r,ALR $a,$accom){ 
        if(is_numeric($r->country_id) && is_numeric($r->region_id)){
                if($r->department_id) {
                        if(is_numeric($r->department_id)) {
                                $location=AccomodationAddress::whereAccomId($accom)->first();
                                if(!$location) {
                                  $location=new AccomodationAddress();
                                } 
                                $location->accom_id=$accom; 
                                $location->longitude=$r->lng;
                                $location->lattitude=$r->lat;
                                $location->country_id=$r->country_id;
                                $location->region_id=$r->region_id;
                                $location->department_id=$r->department_id;
                                $location->street=$r->street;
                                $location->street_number=$r->street_number;
                                $location->city=$r->city_town;
                                $location->postal_code=$r->postal_code;
                                if($location->save()) {
                                    return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                                }
                                return redirect()->back()->with('error',__('Sorry,accomodation location could not updated,please try again!'));        
                        } else {
                           if(!isset($r->new_department)) {
                              if($r->other_department){
                                   if($this->checkUniqueLocation(null,null,$r->other_department)) {
                                      return redirect()->back()->with('error',__('Department already exists'));
                                   }
                                    $newDepartment=DB::table('departments')->insertGetId([
                                        'department_name'=>$r->other_department,
                                        'region_id'=>$r->region_id,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s')
                                    ]);
                                    if($newDepartment){
                                        $this->notifyAdminsForNewLocationRequest(
                                                 'New accomodation location request has been received.',
                                                 'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                                         );  
                                        Notifications::notifyAdmin([  
                                                'accom_id'=>$accom,
                                                'from_user'=>Auth::user()->id,
                                                'message'=>'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                                'request_type_url'=>'/accomodations/view-new-location#showDepartments'
                                        ]);  
                                        $location=AccomodationAddress::whereAccomId($accom)->first();
                                        if(!$location) {
                                         $location=new AccomodationAddress();
                                        }  
                                        $location->accom_id=$accom; 
                                        $location->longitude=$r->lng;
                                        $location->lattitude=$r->lat;
                                        $location->country_id=$r->country_id;
                                        $location->region_id=$r->region_id;
                                        $location->department_id=$newDepartment;
                                        $location->street=$r->street;
                                        $location->street_number=$r->street_number;
                                        $location->city=$r->city_town;
                                        $location->postal_code=$r->postal_code;
                                        if($location->save()) {
                                            return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                                        }
                                        return redirect()->back()->with('error',__('Sorry,accomodation location could not be updated,please try again!'));   
                                    }
                               
                              }
                           } else {
                              $location=AccomodationAddress::whereAccomId($accom)->first();
                              if(!$location) {
                               $location=new AccomodationAddress();
                              }  
                              $location->accom_id=$accom; 
                              $location->longitude=$r->lng; 
                              $location->lattitude=$r->lat;
                              $location->country_id=$r->country_id;
                              $location->region_id=$r->region_id;
                              //$location->department_id=$newDepartment;
                              $location->street=$r->street;
                              $location->street_number=$r->street_number;
                              $location->city=$r->city_town;
                              $location->postal_code=$r->postal_code;
                              if($location->save()) {
                                  return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                              }
                              return redirect()->back()->with('error',__('Sorry,accomodation location could not be updated,please try again!'));  
                           }     
                        }
                }
        } else {
          if(!isset($r->new_country) && !isset($r->new_region) && !isset($r->new_department)) {
              if($r->country_id=='other') {
                 if($this->checkUniqueLocation($r->other_country,$r->other_region,$r->other_department)) {
                    return redirect()->back()->with('error',__('Location already exists'));
                 }
                  $location=AccomodationAddress::whereAccomId($accom)->first(); 
                  if(!$location) {
                   $location=new AccomodationAddress();
                  } 
                  $newCountry=DB::table('countries')->insertGetId([
                      'name'=>$r->other_country,
                      'created_at'=>date('Y-m-d H:i:s'),
                      'updated_at'=>date('Y-m-d H:i:s')
                  ]);
                  if($newCountry) {
                      $newRegion=DB::table('regions')->insertGetId([
                          'region_name'=>$r->other_region,
                          'country_id'=>$newCountry,
                          'created_at'=>date('Y-m-d H:i:s'),
                          'updated_at'=>date('Y-m-d H:i:s')
                      ]); 
                      $newDepartment=null;
                      if($newRegion){
                             if($r->other_department!='') {
                               $newDepartment=DB::table('departments')->insertGetId([
                                   'department_name'=>$r->other_department,
                                   'region_id'=>$newRegion,                               
                                   'created_at'=>date('Y-m-d H:i:s'),
                                   'updated_at'=>date('Y-m-d H:i:s')
                               ]);
                             }
                              $location->accom_id=$accom; 
                              $location->longitude=$r->lng;
                              $location->lattitude=$r->lat;
                              $location->country_id=$newCountry;
                              $location->region_id=$newRegion;
                              $location->department_id=$newDepartment;
                              $location->street=$r->street;
                              $location->street_number=$r->street_number;
                              $location->city=$r->city_town;
                              $location->postal_code=$r->postal_code;
                          }
                      if($location->save()) {
                           $this->notifyAdminsForNewLocationRequest(
                                    'New accomodation location request has been received.',
                                    'New accomodation location country request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                            );  
                           Notifications::notifyAdmin([  
                                   'accom_id'=>$accom,
                                   'from_user'=>Auth::user()->id,
                                   'message'=>'New accomodation location country request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                   'request_type_url'=>'/accomodations/view-new-location'
                           ]); 
                          return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                      }
                      return redirect()->back()->with('error',__('Sorry,accomodation location could not be updated,please try again!'));    

                  } else {
                      return redirect()->back()->with('error',__('Internal error, cannot updated country,please try again!'));
                  }
              } else if($r->region_id=='other'){
                   if($this->checkUniqueLocation(null,$r->other_region,$r->other_department)) {
                      return redirect()->back()->with('error',__('Region/Department already exists'));
                   }
                   $location=AccomodationAddress::whereAccomId($accom)->first();
                   if(!$location) {
                    $location=new AccomodationAddress();
                   } 
                   if(!$location) {
                    $location=new AccomodationAddress();
                   } 
                   $newRegion=DB::table('regions')->insertGetId([
                       'region_name'=>$r->other_region,
                       'country_id'=>$r->country_id,
                       'created_at'=>date('Y-m-d H:i:s'),
                       'updated_at'=>date('Y-m-d H:i:s')
                   ]);
                   if($newRegion) {
                       $newDepartment=null;
                       if($r->other_department) {
                         $newDepartment=DB::table('departments')->insertGetId([
                             'department_name'=>$r->other_department,
                             'region_id'=>$newRegion,                               
                             'created_at'=>date('Y-m-d H:i:s'),
                             'updated_at'=>date('Y-m-d H:i:s')
                         ]);
                       }
                      // dump($accom);
                       $location->accom_id=$accom; 
                       $location->longitude=$r->lng;
                       $location->lattitude=$r->lat;
                       $location->country_id=$r->country_id;
                       $location->region_id=$newRegion;
                       $location->department_id=$newDepartment;
                       $location->street=$r->street;
                       $location->street_number=$r->street_number;
                       $location->city=$r->city_town;
                       $location->postal_code=$r->postal_code;
                       if($location->save()) {
                            $this->notifyAdminsForNewLocationRequest(
                                     'New accomodation location request has been received.',
                                     'New accomodation location region request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                             );  
                            Notifications::notifyAdmin([  
                                    'accom_id'=>$accom,
                                    'from_user'=>Auth::user()->id,
                                    'message'=>'New accomodation location region request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                    'request_type_url'=>'/accomodations/view-new-location#showRegions'
                            ]); 
                           return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                       }
                       return redirect()->back()->with('error',__('Sorry,accomodation location could not be updated,please try again!'));  
                   }
              } else if($r->department_id=='other'){
                   if($this->checkUniqueLocation(null,null,$r->other_department)) {
                      return redirect()->back()->with('error',__('Department already exists'));
                   }
                     $location=AccomodationAddress::whereAccomId($accom)->first();
                     if(!$location) {
                      $location=new AccomodationAddress();
                     }  
                     $newDepartment=DB::table('departments')->insertGetId([
                         'department_name'=>$r->other_department,
                         'region_id'=>$r->region_id,                               
                         'created_at'=>date('Y-m-d H:i:s'),
                         'updated_at'=>date('Y-m-d H:i:s')
                     ]);
                     $location->accom_id=$accom; 
                     $location->longitude=$r->lng;
                     $location->lattitude=$r->lat;
                     $location->country_id=$r->country_id;
                     $location->region_id=$r->region_id;
                     $location->department_id=$newDepartment;
                     $location->street=$r->street;
                     $location->street_number=$r->street_number;
                     $location->city=$r->city_town;
                     $location->postal_code=$r->postal_code;
                     if($location->save()) {
                          $this->notifyAdminsForNewLocationRequest(
                                   'New accomodation location request has been received.',
                                   'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom) 
                           );  
                          Notifications::notifyAdmin([  
                                  'accom_id'=>$accom,
                                  'from_user'=>Auth::user()->id,
                                  'message'=>'New accomodation location department request has been received for accomodation '.app('LBV\Http\Controllers\AwardsController')->propertyName($accom),
                                  'request_type_url'=>'/accomodations/view-new-location#showDepartments'
                          ]); 
                         return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
                     }
                     return redirect()->back()->with('error',__('Sorry,accomodation location could not be updated,please try again!'));
              }  
          } else {
            $location=AccomodationAddress::whereAccomId($accom)->first();
            if(!$location) {
             $location=new AccomodationAddress();
            }  
            $location->accom_id=$accom; 
            $location->longitude=$r->lng;
            $location->lattitude=$r->lat;
            $location->street=$r->street;
            $location->street_number=$r->street_number;
            $location->city=$r->city_town;
            $location->postal_code=$r->postal_code;
            if(!isset($r->new_region)) {
              $location->region_id=$newRegion;
            }
            if(!isset($r->new_department)) {
                $location->department_id=$newDepartment;
            }
            if($location->save()) {
                return redirect('/accomodations/edit-accom-amnities/'.$accom)->with('success',__('Accomodation location updated successfully'));
            }
          }
        } 
    } 
}

