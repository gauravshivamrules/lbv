<?php

namespace LBV\Http\Controllers;

use DB;
use PDF;
use Mail;
use Mollie;
use LBV\User;
use LBV\Model\Accomodation;
use Illuminate\Http\Request;
use LBV\Custom\Notifications;
use Yajra\Datatables\Datatables; 
use LBV\Model\Subscription;
use LBV\Model\SubscriptionLicense;
use LBV\Model\FreeSubscriptionLicense;
use LBV\Model\SubscriptionPayment;
use Illuminate\Support\Facades\Auth;
use LBV\Mail\NewBankTransferRequestAdmin;
use LBV\Mail\SendPaymentInvoiceTransfer;
use LBV\Mail\SendPaidSubscriptionInvoice;
use LBV\Mail\CancelPaymentInvoiceTransfer;
use LBV\Mail\SendPaymentInvoicePaypal;
use LBV\Mail\SendPaymentInvoiceMollie;
use LBV\Mail\SendFreeSubscriptionCoupon; 
use LBV\Mail\SendFreeSubscriptionInvoice; 
use LBV\Mail\SendNewPaymentInvoice;
use LBV\Mail\SendAdminPaymentNotification; 
use LBV\Mail\SendContactUserMail;


class SubscriptionLicensesController extends Controller
{ 

    public function findUnpaidInvoice(){
      $accoms=Accomodation::whereUserId(Auth::user()->id)->whereCompleted(1)->wherePaymentType(1)->get();
      $response=$toPaid=[];
      if($accoms){
        foreach($accoms as $a){
          $license=SubscriptionLicense::whereAccomId($a->id)->first();
          if(!$license) {
            $toPaid[]=$a->id;
          }
        }
      } 
      if($toPaid){
        $response['success']=true;
        $response['accom']=$toPaid[0];
      } else {
         $response['success']=false;
      }
      echo json_encode($response);
      die();
    }

    public function moollieRenew(Request $r,$accom){
        $subscription=Subscription::select('id','amount','name')->whereId($r->subscription_id)->first();
        $payment = Mollie::api()->payments()->create([
            "amount"      => $subscription->amount,
            "description" => env('SITE_TITLE_BACK').' '.$subscription->name,
            "redirectUrl" => $r->getSchemeAndHttpHost()."/subscription-licenses/mollie-return-renew/".$accom,
            "webhookUrl"  => $r->getSchemeAndHttpHost()."/subscription-licenses/mollie-webhook/".$accom  
            // "redirectUrl" => "https://webshop.example.org/order/12345/",
        ]);
        DB::table('mollie_subscription_payments')->insertGetId([
            'order_id'=>$payment->id, 
            'accom_id'=>$accom,
            'subscription_id'=>$subscription->id, 
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'start_from'=>date('Y-m-d',strtotime($r->start_from)),
            'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)),
        ]);
        \Log::log('MollieRenewSubscriptionPaymentCreated','mollie renew subscription payment created for userId '.Auth::user()->id,json_encode($payment));
        return redirect($payment->links->paymentUrl);
    }

    public function renewSubscriptionLicense($accom,$instance,$payment,$license,$adminiNotification,$mailSubject,$mailMessage) {

        
    }

    public function mollieReturnRenew(Request $r,$accom){
        $moliePayment=DB::table('mollie_subscription_payments')->where('accom_id',$accom)->orderBy('id','desc')->first();
        $payment = Mollie::api()->payments()->get($moliePayment->order_id);
        if ($payment->isPaid())  {
            \Log::log('MollieRenewPaymentSucess','success for paymentId '.$payment->id,json_encode($payment));
            $subscription=Subscription::select('id','amount','name','duration','duration_type')->whereId($moliePayment->subscription_id)->first();

            if($payment->amount!=$subscription->amount) {
                return redirect()->back()->with('error',__('Sorry amount paid does not match with subscription amount')); 
            } 
           // $valid_upto=date('Y-m-d',strtotime('+ '.$subscription->duration.$subscription->duration_type));
               if($moliePayment->valid_upto) {
                   $subscriptionAmount=Subscription::select('amount')->whereId($moliePayment->subscription_id)->first();
                   if($payment->amount!=$subscriptionAmount->amount) {
                       return redirect()->back()->with('error',__('Sorry amount paid does not match with subscription amount')); 
                   } 
                   $start_from=date('Y-m-d H:i:s',strtotime($moliePayment->start_from));
                   $valid_upto=date('Y-m-d',strtotime($moliePayment->valid_upto));
                   $instance=DB::table('subscription_instances')->insertGetId([
                       'subscription_id'=>$moliePayment->subscription_id,
                       'accom_id'=>$accom,
                       'start_from'=>$start_from,
                       'valid_upto'=>$valid_upto.' 23:59:59',
                       'amount'=>$payment->amount,
                       'created_at' => date('Y-m-d H:i:s'), 
                       'updated_at' => date('Y-m-d H:i:s')   
                   ]);
                   if($instance) {
                       $payment=DB::table('subscription_payments')->insertGetId([
                           'subscription_instance_id'=>$instance,
                           'user_id'=>Auth::user()->id,
                           'amount_paid'=>$payment->amount,
                           'pay_method_id'=>6,
                           'payment_complete'=>1,
                           'sub_transaction_id'=>$payment->id,
                           'created_at' => date('Y-m-d H:i:s'),
                           'updated_at' => date('Y-m-d H:i:s')   
                       ]);
                       if($payment) {
                           $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                           $license=DB::table('subscription_licenses')->insertGetId([
                               'subscription_payment_id'=>$payment,
                               'start_from'=>$start_from,
                                'valid_upto'=>$valid_upto.' 23:59:59',
                               'accom_id'=>$accom,
                               'subscription_key'=>$subscriptionKey, 
                               'user_id'=>Auth::user()->id,
                               'created_at' => date('Y-m-d H:i:s'),
                               'updated_at' => date('Y-m-d H:i:s')    
                           ]);
                           $invoice_number=env('SITE_INVOICE_KEY').$license;
                           DB::table('subscription_licenses')->where('id',$license)->update(['invoice_number'=>$invoice_number]);
                           DB::table('subscription_licenses')->whereId($r->last_license_id)->update(['license_renewed'=>1]);
                           $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
                           Notifications::notifyAdmin([ 
                                   'accom_id'=>$accom,
                                   'from_user'=>Auth::user()->id,
                                   'message'=>'A renew subscription mollie payment has been received for property '.$accomodation->name,
                                   'request_type_url'=>'/subscription_licenses/licenses'
                           ]);  
                           /*$admins=User::select('email')->where('role_id',1)->get();
                           $this->sendAdminMails($admins,$accomodation);  */
                           $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                           $file->save(public_path('files/pdf/invoice.pdf'));  
                           try {
                               Mail::to(Auth::user()->email)->send(new SendPaymentInvoiceMollie(Auth::user())); 
                               unlink(public_path('files/pdf/invoice.pdf'));  
                           } catch(Exception $e) { 
                               Notifications::notifyAdmin([  
                                       'accom_id'=>$accom,
                                       'from_user'=>Auth::user()->id,
                                       'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                                       'request_type_url'=>'/subscription_licenses/licenses'
                               ]);  
                               return redirect()->back()->with('error','Something went wrong while sending mail,incident has been reported to administrator.please ask administrator to send invoice if not received.Server returned error: '.$e->getMessage());
                           }
                           return redirect('/subscription-licenses')->with('success',__('Thanks for your payment,your subscription license has been created'));
                       } else {
                           return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                       }
                   } else {
                       return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                   } 
               } else {
                   return redirect()->back()->with('error',__('Sorry,invalid request,no valid upto found'));
               }
           
        }  else if ($payment->isOpen()) {
            \Log::log('RenewMolliePaymentFailed','error for paymentId '.$payment->id,json_encode($payment)); 
            DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
            return redirect('/subscription-licenses')->with('error',__('Your payment has been marked open')); 
        } elseif ($payment->status=='cancelled') {
            DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->delete();
            return redirect('/subscription-licenses')->with('error',__('Your payment request cancelled'));
        }  
    }


    public function moollie(Request $r,$accom){
        $subscription=Subscription::select('id','amount','name')->whereId($r->subscription_id)->first();
        $payment = Mollie::api()->payments()->create([
            "amount"      => $subscription->amount,
            "description" => env('SITE_TITLE_BACK').' '.$subscription->name,
            "redirectUrl" => $r->getSchemeAndHttpHost()."/subscription-licenses/mollie-return/".$accom,
            "webhookUrl"  => $r->getSchemeAndHttpHost()."/subscription-licenses/mollie-webhook/".$accom  
            // "redirectUrl" => "https://webshop.example.org/order/12345/",
        ]);
        DB::table('mollie_subscription_payments')->insertGetId([
            'order_id'=>$payment->id, 
            'accom_id'=>$accom,
            'subscription_id'=>$subscription->id, 
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        \Log::log('MollieSubscriptionPaymentCreated','mollie subscription payment created for userId '.Auth::user()->id,json_encode($payment));
        return redirect($payment->links->paymentUrl); 
    }   
    public function mollieWebhook(Request $r,$accom) { 
        $moliePayment=DB::table('mollie_subscription_payments')->where('accom_id',$accom)->orderBy('id','desc')->first();
        $payment    = Mollie::api()->payments()->get($moliePayment->order_id,['testmode'=>true]);
        \Log::log('MolliePaymentResponseWebHook','mollie payment response received for user '.Auth::user()->id,json_encode($payment));
         
        if ($payment->isPaid())  {
            /*
             * At this point you'd probably want to start the process of delivering the product
             * to the customer.
             */
                \Log::log('MolliePaymentSucessWebHook','success for paymentId '.$payment->id,json_encode($payment));
                DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->update(['status'=>1,'remarks'=>json_encode($payment),'method'=>$payment->method]);
        } elseif (! $payment->isOpen()) {
            /*
             * The payment isn't paid and isn't open anymore. We can assume it was aborted.
             */
            \Log::log('MolliePaymentFailedWebHook','error for paymentId '.$payment->id,json_encode($payment)); 
            DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
        } 
    }
    public function mollieReturn(Request $r,$accom){ 
        $moliePayment=DB::table('mollie_subscription_payments')->where('accom_id',$accom)->orderBy('id','desc')->first();
        $payment = Mollie::api()->payments()->get($moliePayment->order_id);
        if ($payment->isPaid())  {
            \Log::log('MolliePaymentSucess','success for paymentId '.$payment->id,json_encode($payment));
            $subscription=Subscription::select('id','amount','name','duration','duration_type')->whereId($moliePayment->subscription_id)->first();
            if($payment->amount!=$subscription->amount) {
                return redirect()->back()->with('error',__('Sorry amount paid does not match with subscription amount')); 
            } 
            $valid_upto=date('Y-m-d',strtotime('+ '.$subscription->duration.$subscription->duration_type));
            $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
            $mailSubject='Thanks for choosing mollie';
            $mailMessage='Your payment by moollie has been received successfully and your subscription license has been activated.Please find enclosed invoice for your payment.';
            $license=$this->makeSubscriptionLicense($accom,[
                'subscription_id'=>$subscription->id, 
                'accom_id'=>$accom,
                'start_from'=>date('Y-m-d H:i:s'),
                'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                'amount'=>$payment->amount,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')   
            ],[
                'user_id'=>Auth::user()->id,
                'amount_paid'=>$payment->amount,
                'pay_method_id'=>6,
                'payment_complete'=>1,
                'sub_transaction_id'=>$payment->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')   
            ],[
                'start_from'=>date('Y-m-d H:i:s'),
                'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                'accom_id'=>$accom,
                'user_id'=>Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')    
            ],[ 
                'accom_id'=>$accom,
                'from_user'=>Auth::user()->id,
                'message'=>'A new subscription mollie payment has been received for property '.$accomodation->name,
                'request_type_url'=>'/subscription_licenses/licenses'
            ],$mailSubject,$mailMessage);
            if($license['success']) { 
                \Log::log('NewSubscriptionLicenseCreated','New subscription license has been created for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
                DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->update(['status'=>1,'remarks'=>json_encode($payment),'method'=>$payment->method]);
                return redirect('/subscription-licenses')->with('success',__('Thanks for your payment,your subscription license has been created'));
            } else {
                \Log::log('NewSubscriptionLicenseFailed','New subscription license has been failed for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
                return redirect('/subscription-licenses')->with('error',__($license['error']));
            }  
        }  else if ($payment->isOpen()) {
            \Log::log('MolliePaymentFailed','error for paymentId '.$payment->id,json_encode($payment)); 
            DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->update(['status'=>2,'remarks'=>json_encode($payment),'method'=>$payment->method]);
            return redirect('/subscription-licenses')->with('error',__('Your payment has been marked open'));
        }  elseif ($payment->status=='cancelled') { 
            DB::table('mollie_subscription_payments')->where('order_id',$payment->id)->delete();
            return redirect('/subscription-licenses')->with('error',__('Your payment request cancelled')); 
        }  
    }
    public function mollieCancel() {
        return redirect('/subscription-licenses')->with('error',__('Your payment has been cancelled'));
    }

    // make subscriptions for paypal,transfer and mollie
    public function makeSubscriptionLicense($accom,$instance=array(),$payment=array(),$license=array(),$adminiNotification=array(),$mailSubject,$mailMessage){
        $return=[];
        if($instance) {
            $instanceId=DB::table('subscription_instances')->insertGetId($instance);
            if($instanceId) {
               $subscriptionPayment=DB::table('subscription_payments')->insertGetId($payment); 
               if($subscriptionPayment) {
                    DB::table('subscription_payments')->whereId($subscriptionPayment)->update(['subscription_instance_id'=>$instanceId]);    
                    
                    $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                    $licenseId=DB::table('subscription_licenses')->insertGetId($license);
                    if($licenseId) {
                        $invoice_number=env('SITE_INVOICE_KEY').$licenseId; 
                        DB::table('subscription_licenses')->where('id',$licenseId)->update([
                            'invoice_number'=>$invoice_number,
                            'subscription_payment_id'=>$subscriptionPayment,
                            'subscription_key'=>$subscriptionKey, 
                        ]);

                        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
                        if($accomodation) {
                            DB::table('accomodations')->whereId($accom)->update(['subs_valid_license'=>1]);
                        } 
                        Notifications::notifyAdmin($adminiNotification); 
                        
                        //$admins=User::select('email')->where('role_id',1)->get();
                        //$this->sendAdminMails($admins,$accomodation);  

                        $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                        $file->save(public_path('files/pdf/invoice.pdf'));  
                        try {
                            Mail::to(Auth::user()->email)->send(new SendNewPaymentInvoice(Auth::user(),$mailSubject,$mailMessage));  
                            unlink(public_path('files/pdf/invoice.pdf')); 
                        } catch(Exception $e) { 
                            Notifications::notifyAdmin([  
                                    'accom_id'=>$accomodation->id,
                                    'from_user'=>Auth::user()->id,
                                    'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.Auth::user()->email.' manually.Server returned error: '.$e->getMessage(),
                                    'request_type_url'=>'/subscription_licenses/licenses' 
                            ]);   
                            return redirect()->back()->with('error','Something went wrong while sending mail,please ask administrator to send invoice.Server returned error: '.$e->getMessage());
                        }
                        $return['success']=true;
                        return $return;
                    } else {
                        $return['success']=false;
                        $return['error']=__('License not saved');
                        return $return;
                    }   
               } else {
                    $return['success']=false;
                    $return['error']=__('Payment not saved');
                    return $return;
               }
            } else {
                $return['success']=false;
                $return['error']=__('Instance not saved');
                return $return;
            } 
        }
    }

    function useCoupon(Request $r,$accom){
        if($r->token) {
            if($accom) {
                $coupon=FreeSubscriptionLicense::where('token',$r->token)->first();
                if($coupon) {
                    if($coupon->accom_id==$accom) {
                        if(!$coupon->is_closed) {
                            $subscription=Subscription::select('duration','duration_type')->whereId($coupon->subscription_id)->first();
                            $valid_upto=date('Y-m-d H:i:s',strtotime('+'.$subscription->duration.$subscription->duration_type));
                            $instance=DB::table('subscription_instances')->insertGetId([
                                'subscription_id'=>$coupon->subscription_id,
                                'accom_id'=>$accom,
                                'start_from'=>date('Y-m-d H:i:s'),
                                'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                                'amount'=>0,
                                'created_at' => date('Y-m-d H:i:s'), 
                                'updated_at' => date('Y-m-d H:i:s')   
                            ]);
                            if($instance) {
                                $payment=DB::table('subscription_payments')->insertGetId([
                                    'subscription_instance_id'=>$instance,
                                    'user_id'=>Auth::user()->id,
                                    'amount_paid'=>0,
                                    'pay_method_id'=>5,
                                    'payment_complete'=>3,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')   
                                ]);
                                if($payment) {
                                    $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                                    $license=DB::table('subscription_licenses')->insertGetId([
                                        'subscription_payment_id'=>$payment,
                                        'start_from'=>date('Y-m-d H:i:s'),
                                        'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                                        'accom_id'=>$accom,
                                        'subscription_key'=>$subscriptionKey, 
                                        'user_id'=>Auth::user()->id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')    
                                    ]);
                                    $invoice_number=env('SITE_INVOICE_KEY').$license;
                                    DB::table('subscription_licenses')->where('id',$license)->update(['invoice_number'=>$invoice_number]);
                                    $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
                                    if($accomodation) {
                                        DB::table('accomodations')->whereId($accom)->update(['subs_valid_license'=>1]);
                                    } 
                                    Notifications::notifyAdmin([ 
                                            'accom_id'=>$accom,
                                            'from_user'=>Auth::user()->id,
                                            'message'=>'A new free subscription coupon has been used for property '.$accomodation->name,
                                            'request_type_url'=>'/subscription_licenses/licenses'
                                    ]); 
                                    $admins=User::select('email')->where('role_id',1)->get();
                                    // $this->sendAdminMails($admins,$accomodation);   
                                    $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                                    $file->save(public_path('files/pdf/invoice.pdf'));  
                                    try {
                                        Mail::to(Auth::user()->email)->send(new SendFreeSubscriptionInvoice(Auth::user())); 
                                        unlink(public_path('files/pdf/invoice.pdf')); 
                                    } catch(Exception $e) {
                                        Notifications::notifyAdmin([  
                                                'accom_id'=>$r->accomodation_id,
                                                'from_user'=>Auth::user()->id,
                                                'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                                                'request_type_url'=>'/subscription_licenses/licenses' 
                                        ]);  
                                        return redirect()->back()->with('error','Something went wrong while sending mail,please ask administrator to send invoice.Server returned error: '.$e->getMessage());
                                    }
                                    $coupon->is_closed=1;
                                    $coupon->save(); 
                                    return redirect('/subscription-licenses')->with('success',__('Thanks for using coupon,your subscription license has been created'));
                                } else {
                                    return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                                }
                            } else {
                                return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                            } 

                        } else {
                            return redirect()->back()->with('error',__('Sorry,that coupon has been closed'));    
                        }
                    } else {
                        return redirect()->back()->with('error',__('Sorry,this coupon is not valid for this accomodation'));    
                    }
                } else {
                    return redirect()->back()->with('error',__('Sorry,coupon is not valid'));
                }
            }
        } 
    }
    public function useCouponRenew(Request $r,$accom) {
        if($r->token) {
            if($accom) {
                $coupon=FreeSubscriptionLicense::where('token',$r->token)->first();
                if($coupon) {
                    if($coupon->accom_id==$accom) {
                        if(!$coupon->is_closed) {
                            $subscription=Subscription::select('duration','duration_type')->whereId($coupon->subscription_id)->first();
                            $lastLicense=SubscriptionLicense::select(['id','start_from','valid_upto'])->where('accom_id',$accom)->orderBy('id','desc')->first();
                            $lastValidUpTo=strtotime($lastLicense->valid_upto);
                            if($lastValidUpTo>strtotime('now')) {
                                $newStartFrom=$lastLicense->valid_upto;
                                $newValidUpTo = strtotime ( '+'.$subscription->duration.$subscription->duration_type , strtotime ( $newStartFrom ));
                                $newValidUpTo=date('d-m-Y',$newValidUpTo);
                            } else {
                                $newStartFrom=date('d-m-Y',strtotime('now'));
                                $newValidUpTo=date('d-m-Y',strtotime('+ '.$subscription->duration.$subscription->duration_type));
                            }
                           
                            $instance=DB::table('subscription_instances')->insertGetId([
                                'subscription_id'=>$coupon->subscription_id,
                                'accom_id'=>$accom,
                                'start_from'=>date('Y-m-d H:i:s',strtotime($newStartFrom)),
                                'valid_upto'=>date('Y-m-d',strtotime($newValidUpTo)).' 23:59:59',
                                'amount'=>0,
                                'created_at' => date('Y-m-d H:i:s'), 
                                'updated_at' => date('Y-m-d H:i:s')   
                            ]);
                            if($instance) {
                                $payment=DB::table('subscription_payments')->insertGetId([
                                    'subscription_instance_id'=>$instance,
                                    'user_id'=>Auth::user()->id,
                                    'amount_paid'=>0,
                                    'pay_method_id'=>5,
                                    'payment_complete'=>3,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')   
                                ]);
                                if($payment) {
                                    $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                                    $license=DB::table('subscription_licenses')->insertGetId([
                                        'subscription_payment_id'=>$payment,
                                        'start_from'=>date('Y-m-d H:i:s',strtotime($newStartFrom)),
                                        'valid_upto'=>date('Y-m-d',strtotime($newValidUpTo)).' 23:59:59',
                                        'accom_id'=>$accom,
                                        'subscription_key'=>$subscriptionKey, 
                                        'user_id'=>Auth::user()->id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')    
                                    ]);
                                    $invoice_number=env('SITE_INVOICE_KEY').$license;
                                    DB::table('subscription_licenses')->where('id',$license)->update(['invoice_number'=>$invoice_number]);
                                    DB::table('subscription_licenses')->whereId($lastLicense->id)->update(['license_renewed'=>1]);
                                    $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
                                    if($accomodation) {
                                        DB::table('accomodations')->whereId($accom)->update(['subs_valid_license'=>1]);
                                    } 
                                    Notifications::notifyAdmin([ 
                                            'accom_id'=>$accom,
                                            'from_user'=>Auth::user()->id,
                                            'message'=>'A new free subscription coupon has been used for property '.$accomodation->name,
                                            'request_type_url'=>'/subscription_licenses/licenses'
                                    ]); 
                                    $admins=User::select('email')->where('role_id',1)->get();
                                    // $this->sendAdminMails($admins,$accomodation);   
                                    $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                                    $file->save(public_path('files/pdf/invoice.pdf'));  
                                    try {
                                        Mail::to(Auth::user()->email)->send(new SendFreeSubscriptionInvoice(Auth::user())); 
                                        unlink(public_path('files/pdf/invoice.pdf')); 
                                    } catch(Exception $e) {
                                        Notifications::notifyAdmin([  
                                                'accom_id'=>$r->accomodation_id,
                                                'from_user'=>Auth::user()->id,
                                                'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                                                'request_type_url'=>'/subscription_licenses/licenses' 
                                        ]);  
                                        return redirect()->back()->with('error','Something went wrong while sending mail,please ask administrator to send invoice.Server returned error: '.$e->getMessage());
                                    }
                                    $coupon->is_closed=1;
                                    $coupon->save(); 
                                    return redirect('/subscription-licenses')->with('success',__('Thanks for using coupon,your subscription license has been created'));
                                } else {
                                    return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                                }
                            } else {
                                return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                            } 

                        } else {
                            return redirect()->back()->with('error',__('Sorry,that coupon has been closed'));    
                        }
                    } else {
                        return redirect()->back()->with('error',__('Sorry,this coupon is not valid for this accomodation'));    
                    }
                } else {
                    return redirect()->back()->with('error',__('Sorry,coupon is not valid'));
                }
            }
        }  
    }

    public function getFreeSubscriptionLicense() { 
        $accoms=Accomodation::select(DB::raw("CONCAT(name,'(',label_name,')') AS name"),'accomodations.id','labels.label_name' )
                            ->join('labels','labels.id','=','accomodations.label_id')->orderBy('label_id','desc')
                            ->pluck('name','id');
        $subscriptions=Subscription::select(DB::raw("CONCAT(name,' EUR',amount) as name"),'id')->orderBy('type_id','desc')->pluck('name','id');
        return view('subscription_licenses.give_free_license',compact('accoms','subscriptions'));
    }
    public function getSubscriptionCoupons() {
        return view('subscription_licenses.free_coupons');
    }
    public function postFreeSubscriptionLicense(Request $r) {
        if($r->subscription_id) {
           $license=new FreeSubscriptionLicense();
           $license->subscription_id=$r->subscription_id;
           $license->accom_id=$r->accom_id;
           $license->user_id=$r->userId;
           $coupon=env('SITE_FREE_SUBTOKEN').uniqid();
           $license->token=$coupon; 
           if($license->save()) {
                try {
                   $user=User::select('first_name','last_name')->whereId($r->userId)->first();
                   $accom=Accomodation::select('name')->whereId($r->accom_id)->first();
                   $accomName=$accom->name;
                   Mail::to($r->email)->send(new SendFreeSubscriptionCoupon($user,$coupon,$accomName));    
                } catch(Exception $e) {
                    return redirect()->back()->with('error',__('Something went wrong while sending mail server returned error: ').$e->getMessage);
                }
                return redirect('/subscription-licenses/all-coupons')->with('success',__('Free subscription coupon sent successfully') );
           }
        } else {
            return redirect()->back()->with('error',__('Sorry,invalid request,no subscription found'));
        }
    }

    public function getAllSubscriptionCouponsAjax() {
        $coupons=FreeSubscriptionLicense::select('free_subscription_licenses.token','free_subscription_licenses.created_at','free_subscription_licenses.is_closed',
                                    'subscriptions.name as sName','accomodations.name as aName',DB::raw("CONCAT(first_name,' ',last_name) as uName"))
                    ->join('subscriptions','subscriptions.id','=','free_subscription_licenses.subscription_id')
                    ->join('accomodations','accomodations.id','=','free_subscription_licenses.accom_id')
                    ->join('users','users.id','=','free_subscription_licenses.user_id')->get();
        return Datatables::of($coupons)  
        ->editColumn('is_closed',function($closed) {
            
            if($closed->is_closed) {
                return "<span class='label label-success'>".__('Closed')."</span>";
            } else {
                return "<span class='label label-danger'>".__('Open')."</span>";
            }
        }) 
        ->editColumn('created_at',function($date) {
            return date('d-m-Y',strtotime($date->created_at));
        }) 
       ->escapeColumns([])
       ->make(true);  
    }

    public function getUser(Request $r)  {
        if($r->accom) {
            $userId=Accomodation::select('user_id')->whereId($r->accom)->first();
            if($userId) {
                $user=User::select('id','email')->whereId($userId->user_id)->first();
                if($user) {
                    echo json_encode($user);
                } 
            }
        }
    }
    public function newSubscriptionPayPalSuccess(Request $r,$accom) {
        $subscriptionAmount=Subscription::select('amount')->whereId($r->subscription_id)->first();
        if($r->amount_paid!=$subscriptionAmount->amount) {
            return redirect()->back()->with('error',__('Sorry amount paid does not match with subscription amount'));
        }
        $subscription=Subscription::select('id','amount','name','duration','duration_type')->whereId($r->subscription_id)->first();
        $valid_upto=date('Y-m-d',strtotime('+ '.$subscription->duration.$subscription->duration_type));
        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
        $mailSubject='Thanks for choosing paypal';
        $mailMessage='Your payment by paypal has been received successfully and your subscription license has been activated.Please find enclosed invoice for your payment.';
        $license=$this->makeSubscriptionLicense($accom,
            [
            'subscription_id'=>$r->subscription_id,
            'accom_id'=>$accom,
            'start_from'=>date('Y-m-d H:i:s'),
            'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
            'amount'=>$r->amount_paid,
            'created_at' => date('Y-m-d H:i:s'), 
            'updated_at' => date('Y-m-d H:i:s')   
        ], [
            'user_id'=>Auth::user()->id,
            'amount_paid'=>$r->amount_paid,
            'pay_method_id'=>3,
            'payment_complete'=>1,
            'sub_transaction_id'=>$r->txn_id,
            'paypal_cart_id'=>$r->cart_id, 
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')   
        ], [
            'start_from'=>date('Y-m-d H:i:s'),
            'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
            'accom_id'=>$accom,
            'user_id'=>Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')    
        ],[ 
            'accom_id'=>$accom,
            'from_user'=>Auth::user()->id,
            'message'=>'A new subscription paypal payment has been received for property '.$accomodation->name,
            'request_type_url'=>'/subscription_licenses/licenses'
            ],$mailSubject,$mailMessage);
        if($license['success']) {
           \Log::log('NewSubscriptionLicenseCreated','New subscription license has been created for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
           return redirect('/subscription-licenses')->with('success',__('Thanks for your payment,your subscription license has been created')); 
        } else {
            \Log::log('NewSubscriptionLicenseFailed','New subscription license has been failed for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
            return redirect('/subscription-licenses')->with('error',__($license['error']));
        }  
    } 
    public function renewSubscriptionPayPalSuccess(Request $r,$accom) {
        if($r->subscription_id) { 
            if($r->valid_upto) {
                $subscriptionAmount=Subscription::select('amount')->whereId($r->subscription_id)->first();
                if($r->amount_paid!=$subscriptionAmount->amount) {
                    return redirect()->back()->with('error',__('Sorry amount paid does not match with subscription amount')); 
                } 
                $start_from=date('Y-m-d',strtotime($r->start_from));
                $instance=DB::table('subscription_instances')->insertGetId([
                    'subscription_id'=>$r->subscription_id,
                    'accom_id'=>$accom,
                    'start_from'=>date('Y-m-d H:i:s',strtotime($start_from)),
                    'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                    'amount'=>$r->amount_paid,
                    'created_at' => date('Y-m-d H:i:s'), 
                    'updated_at' => date('Y-m-d H:i:s')   
                ]);
                if($instance) {
                    $payment=DB::table('subscription_payments')->insertGetId([
                        'subscription_instance_id'=>$instance,
                        'user_id'=>Auth::user()->id,
                        'amount_paid'=>$r->amount_paid,
                        'pay_method_id'=>3,
                        'payment_complete'=>1,
                        'sub_transaction_id'=>$r->txn_id,
                        'paypal_cart_id'=>$r->cart_id, 
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')   
                    ]);
                    if($payment) {
                        $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                        $license=DB::table('subscription_licenses')->insertGetId([
                            'subscription_payment_id'=>$payment,
                            'start_from'=>date('Y-m-d H:i:s',strtotime($start_from)),
                            'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                            'accom_id'=>$accom,
                            'subscription_key'=>$subscriptionKey, 
                            'user_id'=>Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')    
                        ]);
                        $invoice_number=env('SITE_INVOICE_KEY').$license;
                        DB::table('subscription_licenses')->where('id',$license)->update(['invoice_number'=>$invoice_number]);
                        DB::table('subscription_licenses')->whereId($r->last_license_id)->update(['license_renewed'=>1]);
                        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
                        Notifications::notifyAdmin([ 
                                'accom_id'=>$accom,
                                'from_user'=>Auth::user()->id,
                                'message'=>'A renew subscription paypal payment has been received for property '.$accomodation->name,
                                'request_type_url'=>'/subscription_licenses/licenses'
                        ]);  
                        /*$admins=User::select('email')->where('role_id',1)->get();
                        $this->sendAdminMails($admins,$accomodation);  */
                        $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                        $file->save(public_path('files/pdf/invoice.pdf'));  
                        try {
                            Mail::to(Auth::user()->email)->send(new SendPaymentInvoicePaypal(Auth::user())); 
                            unlink(public_path('files/pdf/invoice.pdf')); 
                        } catch(Exception $e) { 
                            Notifications::notifyAdmin([  
                                    'accom_id'=>$r->accomodation_id,
                                    'from_user'=>Auth::user()->id,
                                    'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                                    'request_type_url'=>'/subscription_licenses/licenses'
                            ]);  
                            return redirect()->back()->with('error','Something went wrong while sending mail,incident has been reported to administrator.please ask administrator to send invoice if not received.Server returned error: '.$e->getMessage());
                        }
                        return redirect('/subscription-licenses')->with('success',__('Thanks for your payment,your subscription license has been created'));
                    } else {
                        return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                    }
                } else {
                    return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                } 
            } else {
                return redirect()->back()->with('error',__('Sorry,invalid request,no valid upto found'));
            }
        } else {
            return redirect()->back()->with('error',__('Sorry,invalid request,no subscription found'));
        }
    } 

    public function markPay($id) {
        if($id) {
            $license=$this->giveSubscriptionLicenses([['subscription_licenses.id',$id]]);
            if($license) { 
                $license=$license[0];
                $license->start_from=date('Y-m-d H:i:s');
                $license->valid_upto=date('Y-m-d',strtotime('+ '.$license->duration.$license->duration_type)).' 23:59:59';
                if($license->save()) {
                    $payment=SubscriptionPayment::whereId($license->paymentId)->first();
                    if($payment) {
                        $payment->payment_complete=1;
                        if($payment->save()) {
                            $accom=Accomodation::whereId($license->accomId)->first();
                            if($accom) {
                                $accom->subs_valid_license=1;
                                if($accom->save()) {
                                    Notifications::notifyUser([  
                                        'accom_id'=>$license->accomId,
                                        'message'=>'Your bank transfer payment has been approved for Invoice: '.$license->invoice_number,  
                                        'to_user'=>$license->user_id,  
                                        'request_type_url'=>'/subscription-licenses'
                                    ]);
                                    $file=$this->buildSubscriptionInvoice($license->subscription_key); 
                                    $file->save(public_path('files/pdf/invoice.pdf'));   
                                    $advertiser=User::select('email','first_name','last_name')->whereId($license->user_id)->first();
                                    try {
                                        if($advertiser) { 
                                            Mail::to($advertiser->email)->send(new SendPaidSubscriptionInvoice($advertiser)); 
                                        } 
                                    } catch(Exception $e) {
                                        return redirect()->back()->with('error','Something went wrong while sending mail,Server returned error: '.$e->getMessage());
                                    }
                                    unlink(public_path('files/pdf/invoice.pdf'));  
                                    return redirect('/subscription-licenses/licenses')->with('success',__('Payment has been approved successfully'));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public function markCancel($id) {
        if($id) {
            $license=$this->giveSubscriptionLicenses([['subscription_licenses.id',$id]]);
            $license=$license[0];
            $payment=SubscriptionPayment::where('id',$license->paymentId)->first();
            if($payment) {
                $oldAmount=$payment->amount_paid;
                $payment->payment_complete=2;
                $payment->amount_paid=00;
                $payment->cancelled_amount=$oldAmount; 
                if($payment->save()) {
                    $accom=Accomodation::whereId($license->accomId)->first();
                    if($accom) {
                        if(strtotime($license->start_from)>strtotime('now')) {
                            $accom->subs_valid_license=1;    
                        } else {
                            $accom->subs_valid_license=0;
                        }
                        
                        if($accom->save()) {
                            Notifications::notifyUser([   
                                'accom_id'=>$license->accomId,
                                'message'=>'Your bank transfer payment has been cancelled for Invoice: '.$license->invoice_number,  
                                'to_user'=>$license->user_id,  
                                'request_type_url'=>'/subscription-licenses'
                            ]);
                            $file=$this->buildSubscriptionInvoice($license->subscription_key); 
                            $file->save(public_path('files/pdf/invoice.pdf'));   
                            $advertiser=User::select('email','first_name','last_name')->whereId($license->user_id)->first();
                            try {
                                if($advertiser) { 
                                    Mail::to($advertiser->email)->send(new CancelPaymentInvoiceTransfer($advertiser));  
                                } 
                            } catch(Exception $e) {
                                return redirect()->back()->with('error','Something went wrong while sending mail,Server returned error: '.$e->getMessage());
                            } 
                            unlink(public_path('files/pdf/invoice.pdf'));   
                            return redirect('/subscription-licenses/licenses')->with('success',__('Payment has been cancelled successfully'));
                        }
                    }
                }
            }
        }
    }
    /* 
    *   @Accepts $type=1=active,2=in-active,3=expired,4=new transfers
    */
    public function getAllLicenses(Request $r)
    {
        if($r->type) {
            $isTransfer=false; 
            switch ($r->type) {
                case 1:
                    $where=[
                        ['accomodations.approved',1], 
                        ['accomodations.subs_valid_license',1],
                        ['subscription_licenses.valid_upto','>',date('Y-m-d')]
                    ];
                    $whereIN=[1,3];
                    $licenses=$this->giveSubscriptionLicenses($where,$whereIN);
                    break;
                case 2:
                    $where=[
                        ['subscription_licenses.valid_upto','>',date('Y-m-d')],
                        ['subscription_payments.payment_complete',2],
                        // ['accomodations.subs_valid_license',0]
                    ];
                    $licenses=$this->giveSubscriptionLicenses($where);
                    break;
                case 3:
                    $where=[
                        ['subscription_licenses.valid_upto','<',date('Y-m-d')],
                        // ['accomodations.subs_valid_license',0],
                        // ['subscription_licenses.license_renewed',0],
                    ];
                    $licenses=$this->giveSubscriptionLicenses($where);
                    break;
                case 4:
                    $where=[
                        ['subscription_payments.payment_complete',0],
                        // ['subscription_payments.pay_method_id',2]
                    ];
                    $licenses=$this->giveSubscriptionLicenses($where);
                    $isTransfer=true;
                    break;
            }
            return $this->buildDataTable($licenses,$isTransfer,$r->type);  
        }
    }
    public function buildDataTable($data,$TRANSFER=false,$status=null) {
        if($data) {
            return Datatables::of($data)  
            ->addColumn('action', function ($l) use($TRANSFER,$status) {  
                $str='<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>';  
                $checkClass='';
                if($status) { 
                    switch ($status) {
                        case '1':
                          $checkClass='rowActive';
                          break;
                        case '2':
                          $checkClass='rowInActive';
                          break;

                        case '3':
                          $checkClass='rowExpired';
                          break;

                        case '4':
                          $checkClass='rowTransfer';
                          break;
                    } 
                  $str.='<input type="checkbox" value="'.$l->user_id.'" class="'.$checkClass.'">';
                }
                if($TRANSFER) {
                    $str .='&nbsp;<a  href="/subscription-licenses/mark-pay/'.$l->id.'"  class="btn btn-xs btn-success"  title="Mark Paid"><i class="fa fa-thumbs-o-up"></i> </a>'; 
                    $str .='&nbsp;<a href="/subscription-licenses/mark-cancel/'.$l->id.' "class="btn btn-xs btn-danger"  title="Cancel"><i class="fa fa-thumbs-o-down"></i> </a>'; 
                }
                return $str;     
            })  
            ->editColumn('payment_complete', function ($payment){   
                if($payment->payment_complete==1) {
                    return "<span class='label label-success'>".__('COMPLETED')."</span>";
                } else if($payment->payment_complete==2) {
                    return "<span class='label label-danger'>".__('CANCELED')."</span>";
                } else if($payment->payment_complete==3) {
                    return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
                } else if($payment->payment_complete==0) {
                    return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>"; 
                }
            })   
            ->editColumn('pName',function($name) {
                return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
            })
            ->editColumn('valid_upto',function($date) {
                return date('d-m-Y',strtotime($date->valid_upto));
            }) 
            ->editColumn('first_name',function($date) {
                return $date->first_name.' '.$date->last_name;
            }) 
            ->escapeColumns([])
            ->make(true);
        } else {
            return false;
        }
    }
    public function conatctUsers(Request $r){
      // dd($r); 
      if($r->users) {
          if($r->subject) {
            if($r->message) {
                $users=explode(',', $r->users);
                if($users) {
                  foreach ($users as $u) {
                      $email=User::select('email')->whereId($u)->first();
                      if (filter_var($email->email, FILTER_VALIDATE_EMAIL)) {
                          $this->sendContactUsersMail($email->email,strip_tags($r->subject),$r->message);
                          return redirect()->back()->with('success',__('Message sent successfully'));     
                      }
                    }
                }
            }
          }
      }   
    }
    public function sendContactUsersMail($email,$sub,$msg){
      if(env('IS_MAIL_QUEUE')) {  
          Mail::to($email)->queue(new SendContactUserMail($sub,$msg));  
      } else {
        if($email) { 
            try {
              Mail::to($email)->send(new SendContactUserMail($sub,$msg));  
            } catch(Exception $e){
                \Log::log('MailNotSent','email not sent to '.$email.' server returned error '.$e->getMessage());
            } 
        } 
      }
    } 
    public function giveSubscriptionLicenses($where=array(),$whereIn=array()) {
        if($where) {
            if($whereIn) {
                $licenses=SubscriptionLicense::where($where)->whereIn('subscription_payments.payment_complete',$whereIn)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','subscription_payments.user_id','=','users.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'users.first_name','users.last_name',
                                    'mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            } else {
                $licenses=SubscriptionLicense::where($where)
                    ->leftJoin('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                    ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                    ->leftJoin('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                    ->leftJoin('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                    ->leftJoin('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                    ->leftJoin('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                    ->leftJoin('users','subscription_payments.user_id','=','users.id')
                    ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                    'subscription_licenses.invoice_number','subscription_licenses.subscription_key','accomodations.name as aName','subscription_licenses.license_renewed',
                                    'accomodations.id as accomId','pay_methods.method_name as pName',
                                    'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                    'subscription_payments.id as paymentId',
                                    'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.duration','subscriptions.duration_type',
                                    'users.first_name','users.last_name', 'mollie_subscription_payments.method' 
                                )
                    ->orderBy('subscription_licenses.id','desc')
                    ->get(); 
            }
           
            return $licenses;
        } else {
            return false;
        }
    }
    public function bankTransfer(Request $r) {
    	$subscription=Subscription::select('id','amount','name','duration','duration_type')->whereId($r->subscription_id)->first();
        $valid_upto=date('Y-m-d',strtotime('+ '.$subscription->duration.$subscription->duration_type));
        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($r->accomodation_id)->first(); 
        $mailSubject='Thanks for choosing bank transfer';
        $mailMessage='Your payment request by bank transfer has been received successfully,wating for admin approval.Please find enclosed invoice for your payment.';
        $license=$this->makeSubscriptionLicense($r->accomodation_id,
                [
                    'subscription_id'=>$r->subscription_id,
                    'accom_id'=>$r->accomodation_id,
                    'start_from'=>date('Y-m-d H:i:s'),
                    'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                    'amount'=>$r->amount,
                    'created_at' => date('Y-m-d H:i:s'), 
                    'updated_at' => date('Y-m-d H:i:s')   
                ],[
                    'user_id'=>Auth::user()->id,
                    'amount_paid'=>$r->amount,
                    'pay_method_id'=>2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')   
                ],[
                    'start_from'=>date('Y-m-d H:i:s'),
                    'valid_upto'=>date('Y-m-d',strtotime($valid_upto)).' 23:59:59',
                    'accom_id'=>$r->accomodation_id,
                    'user_id'=>Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')    
                    ],[ 
                    'accom_id'=>$r->accomodation_id,
                    'from_user'=>Auth::user()->id,
                    'message'=>'A new subscription bank transfer request has been received for property '.$accomodation->name,
                    'request_type_url'=>'/subscription_licenses/licenses#showBankTransfer' 
                ],
            $mailSubject,$mailMessage
        );
        if($license['success']) {
          \Log::log('NewSubscriptionLicenseCreated','New subscription license has been created for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
          return redirect('/subscription-licenses')->with('success',__('Thanks for your payment request,now wating approval from admin')); 
        } else {
            \Log::log('NewSubscriptionLicenseFailed','New subscription license has been failed for userId '.Auth::user()->id.' for accomodationId '.$accomodation->id);
            return redirect('/subscription-licenses')->with('error',__($license['error']));
        }  
    }   
 
    public function bankTransferRenew(Request $r) {
        if($r->subscription_id) {
            if($r->valid_upto) {
                $start_from=date('Y-m-d',strtotime($r->start_from));
                $instance=DB::table('subscription_instances')->insertGetId([
                    'subscription_id'=>$r->subscription_id,
                    'accom_id'=>$r->accomodation_id,
                    'start_from'=>date('Y-m-d H:i:s',strtotime($start_from)),
                    'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)),
                    'amount'=>$r->amount,
                    'created_at' => date('Y-m-d H:i:s'), 
                    'updated_at' => date('Y-m-d H:i:s')   
                ]);
                if($instance) {
                    $payment=DB::table('subscription_payments')->insertGetId([
                        'subscription_instance_id'=>$instance,
                        'user_id'=>Auth::user()->id,
                        'amount_paid'=>$r->amount,
                        'pay_method_id'=>2,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')   
                    ]);
                    if($payment) {
                        $subscriptionKey=env('SITE_SUBSCRIPTION_KEY').uniqid();
                        $license=DB::table('subscription_licenses')->insertGetId([
                            'subscription_payment_id'=>$payment,
                            'start_from'=>date('Y-m-d H:i:s',strtotime($start_from)),
                            'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)),
                            'accom_id'=>$r->accomodation_id,
                            'subscription_key'=>$subscriptionKey, 
                            'user_id'=>Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')    
                        ]);
                        $invoice_number=env('SITE_INVOICE_KEY').$license;
                        DB::table('subscription_licenses')->where('id',$license)->update(['invoice_number'=>$invoice_number]);
                        DB::table('subscription_licenses')->whereId($r->last_license_id)->update(['license_renewed'=>1]);
                        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($r->accomodation_id)->first(); 
                        Notifications::notifyAdmin([ 
                                'accom_id'=>$r->accomodation_id,
                                'from_user'=>Auth::user()->id,
                                'message'=>'A renew subscription bank transfer request has been received for property '.$accomodation->name,
                                'request_type_url'=>'/subscription_licenses/licenses#showBankTransfer'
                        ]);  
                        /*$admins=User::select('email')->where('role_id',1)->get();
                        $this->sendAdminMails($admins,$accomodation);  */
                        $file=$this->buildSubscriptionInvoice($subscriptionKey); 
                        $file->save(public_path('files/pdf/invoice.pdf'));  
                        try {
                            Mail::to(Auth::user()->email)->send(new SendPaymentInvoiceTransfer(Auth::user())); 
                            unlink(public_path('files/pdf/invoice.pdf')); 
                        } catch(Exception $e) { 
                            Notifications::notifyAdmin([  
                                    'accom_id'=>$r->accomodation_id,
                                    'from_user'=>Auth::user()->id,
                                    'message'=>'Sorry,invoice mail could not be sent to advertiser,Please send invoice to  '.$invoice_number.' manually.Server returned error: '.$e->getMessage(),
                                    'request_type_url'=>'/subscription_licenses/licenses#showBankTransfer'
                            ]);  
                            return redirect()->back()->with('error','Something went wrong while sending mail,incident has been reported to administrator.please ask administrator to send invoice if not received.Server returned error: '.$e->getMessage());
                        }
                        return redirect('/subscription-licenses')->with('success',__('Thanks for your payment,your subscription license has been created and wating approval from admin'));
                    } else {
                        return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                    }
                } else {
                    return redirect()->back()->with('error',__('Sorry,something went wrong,your payment could not be saved by system,please try again!'));
                } 
            } else {
                return redirect()->back()->with('error',__('Sorry,invalid request,no valid upto found'));
            }
        } else {
            return redirect()->back()->with('error',__('Sorry,invalid request,no subscription found'));
        }
    }


    public function sendAdminMails($admins=array(),$data,$subject,$msg,$accom) {
        if($admins) {
            foreach($admins as $a) {
                try {

                    Mail::to($a->email)->send(new SendAdminPaymentNotification($accom,$subject,$msg,$data));    
                } catch(Exception $e) {
                    // return redirect()->back()->with('error','Something went wrong while sending mail,server returned error: '.$e->getMessage());
                }
            }
        }
    }
    public function buildSubscriptionInvoice($key=null) {
        if($key) {
            $licenses=SubscriptionLicense::where('subscription_licenses.subscription_key', $key)
                        ->join('subscription_payments','subscription_licenses.subscription_payment_id','=','subscription_payments.id')
                        ->join('subscription_instances','subscription_instances.id','=','subscription_payments.subscription_instance_id')
                        ->join('subscriptions','subscriptions.id','=','subscription_instances.subscription_id')
                        ->join('accomodations','subscription_licenses.accom_id','=','accomodations.id')
                        ->join('pay_methods','subscription_payments.pay_method_id','=','pay_methods.id')
                        ->join('users','subscription_licenses.user_id','=','users.id')
                        ->join('countries','users.country_id','=','countries.id')
                        ->leftJoin('mollie_subscription_payments','mollie_subscription_payments.order_id','=','subscription_payments.sub_transaction_id')
                        ->select('subscription_licenses.id','subscription_licenses.user_id','subscription_licenses.start_from','subscription_licenses.valid_upto',
                                        'subscription_licenses.created_at as invoiceDate','subscription_licenses.order_number',
                                        'subscription_licenses.invoice_number','accomodations.name as aName','pay_methods.method_name as pName',
                                        'subscription_payments.amount_paid','subscription_payments.pay_method_id','subscription_payments.payment_complete',
                                        'subscription_instances.subscription_id','subscriptions.name as sName','subscriptions.description as sDesc',
                                        'subscriptions.amount as sAmount','users.*','countries.name as countryName',
                                        'mollie_subscription_payments.method' 
                                )
                        ->first(); 
            if($licenses){
              $file=PDF::loadView('subscription_licenses.pdf.invoice',compact('licenses'));  
              return $file;
            }else{
              return false;
            } 
        } else {
            return false;
        }  
    }
    public function generateInvoice($key=null) {
        if($key) {
            $file=$this->buildSubscriptionInvoice($key); 
            if($file){
              return $file->stream();       
            }else{
              return redirect()->back()->with('error',__('No licenses found'));
            }  
        }  
        return false; 
    } 
    public function licenses() {
        return view('subscription_licenses.licenses');
    }
    public function getAdvertiserLicensesView() {
    	return view('subscription_licenses.index');
    }
    public function getAdvertiserLicenses() {
      $licenses=$this->giveSubscriptionLicenses([['subscription_licenses.user_id',Auth::user()->id]]);
    	return Datatables::of($licenses) 
    	->addColumn('action', function ($l) {   
    		 $str='<a href="/subscription-licenses/generate-invoice/'.$l->subscription_key.'.pdf "class="btn btn-xs btn-primary" target="__blank" title="Invoice"><i class="fa fa-download"></i> </a>'; 
    	    return $str;    
    	})  
    	->editColumn('payment_complete', function ($payment) {   
    	    if($payment->payment_complete==1) {
    	    	return "<span class='label label-success'>".__('COMPLETED')."</span>";
    	    } else if($payment->payment_complete==2) {
    	    	return "<span class='label label-danger'>".__('CANCELED')."</span>";
    	    } else if($payment->payment_complete==3) {
    	    	return "<span class='label label-primary'>".__('FREE LICENSE')."</span>";
    	    } else if($payment->payment_complete==0) {
    	    	return "<span class='label label-danger'>".__('NOT-COMPLETED')."</span>";
    	    }
    	})   
    	->editColumn('valid_upto',function($date)  {
            $str=date('d-m-Y',strtotime($date->valid_upto));
            if( strtotime('now')>strtotime($date->valid_upto)) {
                $str .=' <span class="label label-danger">'.__('Expired').'</span>';
            } else {
                if($date->payment_complete==1 || $date->payment_complete==3 ) {
                   $str .=' <span class="label label-success">'.__('Active').'</span>';      
                } else if($date->payment_complete==2) {
                    $str .=' <span class="label label-danger">'.__('Cancelled').'</span>';    
                } else if($date->payment_complete==0) {
                     $str .=' <span class="label label-danger">'.__('Not-completed').'</span>';    
                } 
            } 
            if(!$date->license_renewed) {
              if(in_array($date->payment_complete,[1,3])) { 
                $str .='<br><a class="label label-default" href="/subscriptions/pay-renew/'.$date->accomId.'">'.__('Renew/Extend').'</a>';   
              } 
            } 
            return $str;
    	})
      ->editColumn('pName',function($name) {
              return $name->pName."&nbsp;<span class='label label-danger'>".$name->method."</span>";
      })
    	->escapeColumns([])
    	->make(true); 
    }
 


}
