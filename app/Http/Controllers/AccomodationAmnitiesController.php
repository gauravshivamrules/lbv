<?php

namespace LBV\Http\Controllers;
use LBV\Model\SearchTag;
use LBV\Model\SearchingGroup;
use LBV\Model\SearchtagSearchingGroup; 
use LBV\Model\AccomodationSearchtag;
use Illuminate\Http\Request;
use LBV\Model\Accomodation; 
use Illuminate\Support\Facades\Auth;
use LBV\Custom\Notifications;
use DB;
use Mail;
use LBV\User; 
use LBV\Mail\NewAccomodationRegistered;

class AccomodationAmnitiesController extends Controller 
{
    public function getAminityAdd(Request $r,$accom) {
    	$searchGroup=SearchingGroup::all()->toArray();
    	for($i=0;$i<count($searchGroup);$i++){
    		$searchGroupWithTags=DB::table('searchtag_searching_groups')
            ->leftJoin('searchtags', 'searchtag_searching_groups.searchtag_id', '=', 'searchtags.id')->where('searching_group_id',$searchGroup[$i]['id'])
            ->get()->toArray();
    		$searchGroup[$i]['SearchingTags']=$searchGroupWithTags;
    	}
    	return view('accomodations.add_accom_amnity',compact('accom','searchGroup')); 
    }
    public function postAminityAdd(Request $r,$accom) { 
    	$ERROR=$msg='';
        if($r->searchtag_id) {
                foreach($r->searchtag_id as $v) {
                    $accomTag=new AccomodationSearchtag(); 
                    $accomTag->accom_id=$accom;
                    $accomTag->searchtag_id=$v;
                    if(!$accomTag->save()) {
                        $ERROR='ERROR';
                    }
                }    
        }
    	$a=Accomodation::where('id',$accom)->first();
    	if($a) {
            $LABEL='';
    		$a->completed=1;
            if($a->label_id==3) {
                $LABEL='RENT';
            } else {
                $LABEL='SALE';
            }
    		if($a->payment_type==1) { 
    			$a->approved=1; 
                $msg='A new paid '.$LABEL.' accomodation has been created and marked completed';
    		} else { 
                $msg='A new free accomodation has been created and marked completed';
            }
    		$a->save(); 
    	}
    	if($ERROR !='') { 
    		return redirect()->back()->with('error',__('Sorry some of your amnities could not be saved,please try again!'));
    	}
        Notifications::notifyAdmin([ 
                'accom_id'=>$accom,
                'from_user'=>Auth::user()->id,
                'message'=>$msg,  
                'request_type_url'=>'/accomodations/view-accom/'.$a->slug  
        ]);  
        $accomodation=Accomodation::select(['name','user_id','id','payment_type','label_id'])->with('user')->with('accomodation_address')->whereId($accom)->first(); 
        $admins=User::select('email')->where('role_id',1)->get();
        foreach($admins as $ad) {
            try {
                Mail::to($ad->email)->send(new NewAccomodationRegistered($accomodation));    
            } catch(Exception $e) {
                // return redirect()->back()->with('error','Something went wrong while sending mail,server returned error: '.$e->getMessage());
            }
        }
       
        if($a->payment_type==1) {  
            return redirect('/accomodations/pay/'.$accom)->with('success',__('Your accomodation has been marked completed,please select your subscription'));
        } else {
            return redirect('/accomodations/my-accomodations')->with('success',__('Your accomodation has been marked completed'));     
        }
    }
    public function getAminityEdit(Request $r,$accom) {
        $searchGroup=SearchingGroup::all()->toArray();
        for($i=0;$i<count($searchGroup);$i++){
            $searchGroupWithTags=DB::table('searchtag_searching_groups')
               ->leftJoin('searchtags', 'searchtag_searching_groups.searchtag_id', '=', 'searchtags.id')->where('searching_group_id',$searchGroup[$i]['id'])
               ->get()->toArray();
            $searchGroup[$i]['SearchingTags']=$searchGroupWithTags;
        }
        $searchtags=AccomodationSearchtag::where('accom_id',$accom)->pluck('searchtag_id','searchtag_id')->toArray();
        if($searchtags){
            $searchtags=array_values($searchtags);
        }
        return view('accomodations.edit_accom_aminity',compact('accom','searchGroup','searchtags'));  
    }
    public function postAminityEdit(Request $r,$accom) { 
        $accomTags=AccomodationSearchtag::where('accom_id',$accom)->get(); 
        $ERROR=$msg='';

        if($accomTags) {
            foreach ($accomTags as $a) {
                $a->delete();
            }
        }
        // $ERROR='';
        if($r->searchtag_id) {
            foreach($r->searchtag_id as $v) {
                $newTags=new AccomodationSearchtag();
                $newTags->accom_id=$accom;
                $newTags->searchtag_id=$v;
                if(!$newTags->save()) {
                    $ERROR='ERROR';
                }
            }
        }
       
        $a=Accomodation::where('id',$accom)->first();
        if($a) {
            $LABEL='';
            if($a->payment_type==1) { 
                if($a->label_id==3) {
                    $LABEL='RENT';
                } else {
                    $LABEL='SALE';
                }
                $a->approved=1;
                $msg='A new paid '.$LABEL.' accomodation has been created and marked completed';
            } else {
                $msg='A new free accomodation has been created and marked completed';
            } 
            if(Auth::user()->role_id==3) {
                if(!$a->completed) {
                    Notifications::notifyAdmin([ 
                            'accom_id'=>$accom,
                            'from_user'=>Auth::user()->id,
                            'message'=>$msg,  
                            'request_type_url'=>'/accomodations/view-accom/'.$a->slug   
                    ]);
                }    
            }
            $a->completed=1;
            $a->save(); 
        }     
        if($ERROR !='') {
            return redirect()->back()->with('error',__('Sorry some of our amnities could not be updated,please try again!'));
        }
       
        if(Auth::user()->role_id==1) {
            return redirect('/accomodations')->with('success',__('Accomodation has been updated successfully')); 
        } else {
            if($a->payment_type==1) {
                if($a->subs_valid_license==0) {
                    return redirect('/accomodations/pay/'.$accom)->with('success',__('Accomodation amnities updated successfully,please select your subscription'));
                } 
            } 
            return redirect('/accomodations/my-accomodations')->with('success',__('Your accomodation has been updated successfully')); 
        }
    } 	 
}
