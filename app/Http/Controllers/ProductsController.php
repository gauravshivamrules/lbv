<?php

namespace LBV\Http\Controllers;
use DB;
use LBV\Model\Product;
use Illuminate\Http\Request;
use LBV\Model\ProductCategory;
use Yajra\Datatables\Datatables;
use LBV\Http\Requests\ProductAddRequest;


class ProductsController extends Controller
{
     
    public function index() {
    	return view('products.index');
    }
    public function getAdd() {
    	$p=ProductCategory::pluck('name','id');
    	return view('products.add',compact('p'));
    }
    public function postAdd(Request $r,ProductAddRequest $p) {
    	$p=new Product();
    	$p->product_name=$r->get('product_name');
    	$p->product_description=$r->get('product_description');
    	$p->product_price=$r->get('product_price');
    	$p->product_month=$r->get('product_month');
    	$p->product_year=$r->get('product_year');
    	$p->product_category_id=$r->get('product_category_id'); 
    	if($p->save()) {
    		return redirect('/products')->with('success',__('Promotion added successfully'));
    	}
    	return redirect()->back()->with('error',__('Sorry,promotion could not be added,please try again!'));
    }
    public function getEdit(Request $r,$id) {
        $product=Product::whereId($id)->first();
        $p=ProductCategory::pluck('name','id');
        return view('products.edit',compact('product','p')); 
    }
    public function postEdit(Request $r,ProductAddRequest $p,$id) {
        $pro=Product::whereId($id)->first();
        $pro->product_name=$r->get('product_name');
        $pro->product_description=$r->get('product_description');
        $pro->product_price=$r->get('product_price');
        $pro->product_category_id=$r->get('product_category_id');
        $pro->product_month=$r->get('product_month');
        $pro->product_year=$r->get('product_year');
        if($pro->save()) {
            return redirect('/products')->with('success',__('Promotion updated successfully'));
        }
        return redirect()->back()->with('error',__('Sorry,promotion could not be updated,please try again!'));
    }  
    public function getProducts() {  
    	$pro=Product::select(['id','product_name','product_description','product_price','product_month','product_year','product_category_id'])->get();
        $str='';
    	return Datatables::of($pro)
    	->addColumn('action', function ($s) {    
    	    $str ='<a href="/products/edit/'.$s->id.'" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>&nbsp';   
    	    $str .='&nbsp;<a id="deleteProduct" href="/products/delete/'.$s->id.'" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i> </a>'; 
    	    return $str;
    	})
        ->editColumn('product_category_id',function($p) {
            $product_cat;
            if(!is_null($p->product_category_id)) {
                $product_cat=ProductCategory::select(['name'])->whereId($p->product_category_id)->first();
                return $product_cat['name'];
            } 
        })   
    	->editColumn('product_month',function($m) {
    		$product_month;
    		if(!is_null($m->product_month)) {
    			$product_month=$this->monthName($m->product_month);
    			return $product_month;
    		}
    	})   
    	->escapeColumns([])
    	->make(true); 
    }
    function monthName($month_int) {
		$month_int = (int)$month_int;
		$timestamp = mktime(0, 0, 0, $month_int);
		return date("F", $timestamp);  
	} 
    public function addNews($licenseId=null){
        if($licenseId) {
            $product=DB::table('product_licenses')->select('accom_id')->whereId($licenseId)->first();
            $accom=$this->giveAccomName($product->accom_id);
            return view('products.add_news',compact('accom','licenseId')); 
        } 
    } 
    public function postAddNews(Request $r,$licenseId){
        if($licenseId) {
            $license=DB::table('product_licenses')->whereId($licenseId)->first();
            if($license) {
                $news=DB::table('accomodation_news')->insertGetId([
                        'accom_id'=>$license->accom_id,
                        'title'=>$r->news_title,
                        'description'=>$r->news_description,
                        'start_from'=>date('Y-m-d H:i:s',strtotime($r->start_from)),
                        'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                        'product_license_id'=>$licenseId,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                ]);
                if($news) {
                    DB::table('product_licenses')->whereId($licenseId)->update([
                        'valid_from'=>date('Y-m-d H:i:s',strtotime($r->start_from)),
                        'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                        'activated'=>1
                    ]);
                    return redirect('/promotions/my-news')->with('success',__('News added successfully'));
                } else {
                    return redirect()->back()->with('error',__('Sorry,Something went wrong,please try again!'));
                }
            } else { 
                return redirect()->back()->with('error',__('Invalid request,no license found'));
            } 
        } else {
            return redirect()->back()->with('error',__('Invalid request,no license found'));
        }
    }
    public function editNews($licenseId=null){
        if($licenseId) {
            $news=DB::table('accomodation_news')->whereProductLicenseId($licenseId)->first();
            $accom=$this->giveAccomName($news->accom_id);
            return view('products.edit_news',compact('news','accom','licenseId'));
        } else {
            return redirect()->back()->with('error',__('Invalid request,ID is required'));
        }
    }
    public function postEditNews(Request $r,$licenseId) {
        if($licenseId) {
            if(DB::table('accomodation_news')->whereProductLicenseId($licenseId)->update(['title'=>$r->news_title,'description'=>$r->news_description])) {
                return redirect('/promotions/my-news')->with('success',__('News updated successfully'));
            } else {
                return redirect()->back()->with('error',__('Internal error,please resize your picture and try again!'));
            }
        } else {
            return redirect()->back()->with('error',__('Invalid request,ID is required'));
        }
    }
    public function addLastMinute($licenseId=null){
        if($licenseId) { 
            $product=DB::table('product_licenses')->select('accom_id')->whereId($licenseId)->first();
            $accom=$this->giveAccomName($product->accom_id);
            return view('products.add_lastminute',compact('accom','licenseId'));
        }
    }
    public function postAddLastMinute(Request $r,$licenseId){ 
        if($licenseId) {
            $license=DB::table('product_licenses')->whereId($licenseId)->first();
            if($license) {
                $lastMinute=DB::table('accomodation_lastminutes')->insertGetId([
                        'accom_id'=>$license->accom_id,
                        'title'=>$r->news_title,
                        'description'=>$r->news_description,
                        'start_from'=>date('Y-m-d H:i:s',strtotime($r->start_from)),
                        'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                        'product_license_id'=>$licenseId,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                ]);
                if($lastMinute) {
                    DB::table('product_licenses')->whereId($licenseId)->update([
                        'valid_from'=>date('Y-m-d H:i:s',strtotime($r->start_from)),
                        'valid_upto'=>date('Y-m-d',strtotime($r->valid_upto)).' 23:59:59',
                        'activated'=>1
                    ]);
                    return redirect('/promotions/my-lastminutes')->with('success',__('LastMinute added successfully'));
                } else {
                    return redirect()->back()->with('error',__('Sorry,Something went wrong,please try again!'));
                }
            } else {
                return redirect()->back()->with('error',__('Invalid request,no license found'));
            } 
        } else {
            return redirect()->back()->with('error',__('Invalid request,no license found'));
        }   
    }
    public function editLastMinute($licenseId=null){
     if($licenseId) {
        $lastminute=DB::table('accomodation_lastminutes')->whereProductLicenseId($licenseId)->first();
        $accom=$this->giveAccomName($lastminute->accom_id);
        return view('products.edit_lastminute',compact('lastminute','accom','licenseId')); 
     } else {
        return redirect()->back()->with('error',__('Invalid request,ID is required'));
     }
    }
    public function postEditLastMinute(Request $r,$licenseId){ 
        if($licenseId) {
            if(DB::table('accomodation_lastminutes')->whereProductLicenseId($licenseId)->update(['title'=>$r->news_title,'description'=>$r->news_description])) {
                return redirect('/promotions/my-lastminutes')->with('success',__('LastMinute updated successfully'));
            } else {
                return redirect()->back()->with('error',__('Internal error,please resize your picture and try again!'));
            }
        } else {
            return redirect()->back()->with('error',__('Invalid request,ID is required'));
        }
    } 
    public function giveAccomName($accom){ 
        $accom=DB::table('accomodations')->select('name')->whereId($accom)->first();
        return $accom->name;
    } 
}
