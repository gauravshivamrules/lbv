<?php

namespace LBV\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'product_name'=>'required',
              'product_description'=>'required',
              'product_price'=> 'required|numeric', 
              'product_category_id' => 'required',
              'product_month'=>'required',
              'product_year'=>'required'
        ];
    }  
    public function messages()  
    {
        return [
            'product_name'=>__('Product name is required'),
            'product_description'=>__('Product description is required'),
            'product_price'=> __('Product amount is required'), 
            'product_category_id' => __('Product type is required'),
            'product_month'=>__('Product month is required'),
            'product_year'=>__('Product year is required')
        ];
    }
}
