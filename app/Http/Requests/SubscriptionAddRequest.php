<?php

namespace LBV\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; 
    }
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'name'=>'required',
              'description'=>'required',
              'duration'=> 'required|numeric', 
              'duration_type' => 'required',
              'amount'=>'required|numeric'
        ];
    }  
}
