<?php

namespace LBV\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; 
    }
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'first_name'=>'required',
              'last_name'=>'required',
              'password'=> 'required|min:4', 
              'confirm_password' => 'required|same:password',
              'email'=>'required|email|unique:users',
              'phone_number'=>'required|digits_between:10,13',
        ];
    }
 
    public function messages()  
    {
         return [
              'first_name.required' => __('Please enter first name'),
              'last_name.required' => __('Please enter last name'),
              'email.required' => __('Please enter email'),
              'email.email' => __('Please enter valid email'),
              'email.unique' => __('Sorry,email already exists'),
              'phone_number.required' => __('Please enter phone number'),
              'phone_number.numeric' => __('Please enter valid phone number'),
              'phone_number.min' => __('Phone number minimum 10 digits'),
              'password.required'=> __('Please enter password')
         ];
    }
}
