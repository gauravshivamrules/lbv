<?php

namespace LBV\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class AccomationPriceAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */ 
    public function rules(Request $r)  
    {
        $type=$r->get('accom_type');
        if($type==1 || $type==5 || $type==6 || $type==8 || $type==9 || $type==12 ) {
               return [
                       'changeday'=>'required',
                       'start_date'=>'required',
                       'end_date'=> 'required', 
                       'min_days' => 'required',
                       'max_persons'=>'required',
                       'amount'=>'required',
                       'extra_price_per_person'=>'required'
               ]; 
        } else {
            if($r->price_unit=='perperson') {
                return [ 
                        'changeday'=>'required',
                        'start_date'=>'required',
                        'end_date'=> 'required', 
                        'min_days' => 'required|numeric',
                        'price_unit'=>'required',
                        'room_type'=>'required',
                        'description'=>'required',
                        'price1'=>'required',
                        'price2'=>'required',
                        'price3'=>'required',
                        'extra_price_adult'=>'required',
                        'adults'=>'required|numeric',
                        'childs'=>'required|numeric',
                        'extra_price_child'=>'required',
                        'no_of_rooms'=>'required|numeric',
                        'max_adults_per_room'=>'required|numeric'
                ];
            } else {
                    return [
                        'changeday'=>'required', 
                        'start_date_perroom'=>'required',
                        'end_date_perroom'=> 'required', 
                        'min_days_perroom' => 'required|numeric',
                        'price_unit'=>'required',
                        'room_type_perroom'=>'required',
                        'description_perroom'=>'required',
                        'amount'=>'required',
                        'adults'=>'required|numeric',
                        'childs'=>'required|numeric',
                        'no_of_rooms'=>'required|numeric'
                    ]; 
            }
        } 
    }

}
