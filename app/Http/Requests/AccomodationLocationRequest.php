<?php

namespace LBV\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccomodationLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'required',
            'region_id'=>'required',
            'street'=> 'required', 
            'postal_code'=>'required'
        ]; 
    }
    public function messages()
    {
        return [ 
            'country_id'=>__('Country is required'),
            'region_id.required'=>__('Region is required'),
            'street.required'=> __('Street is required'), 
            'postal_code.required'=>__('Zip is required')
        ];  
    }
}
