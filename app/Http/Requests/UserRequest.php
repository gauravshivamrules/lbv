<?php

namespace LBV\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {   
        return [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>Rule::unique('users')->ignore($req->get('user_id'), 'id'),
            'phone_number'=>'required|digits_between:10,13'
        ];
    }
 
    public function messages()
    {
         return [
              'first_name.required' => __('Please enter first name'),
              'last_name.required' => __('Please enter last name'),
              'email.required' => __('Please enter email'),
              'email.email' => __('Please enter valid email'),
              'email.unique' => __('Sorry,email already exists'),
              'phone_number.required' => __('Please enter phone number'),
              'phone_number.numeric' => __('Please enter valid phone number'),
              'phone_number.min' => __('Phone number minimum 10 digits') 
         ];
    }
}
