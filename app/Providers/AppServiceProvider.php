<?php

namespace LBV\Providers;


use Illuminate\Support\ServiceProvider;
use LBV\Model\AdminNotification;
use LBV\Model\UserNotification;
use Illuminate\Support\Facades\Auth;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $auth = $this->app['auth'];
        view()->composer('*', function ($view) use($auth) {  
            if($auth->user()) {
                $adminNotification=AdminNotification::select(['id','message','created_at','request_type_url'])->orderBy('id','desc')->where('seen',0)->get();
                $userNotification=UserNotification::select(['id','message','created_at','request_type_url','to_user'])->orderBy('id','desc')->where('seen',0)->where('to_user',$auth->user()->id)->get();
                $msgNotification=DB::table('message_notifications')->select('id','accom_id','from_user','message','created_at','request_type_url')->orderBy('id','desc')->where('seen',0)->where('to_user',$auth->user()->id)->get();
                view()->share('adminNotification', $adminNotification);
                view()->share('userNotification', $userNotification);  
                view()->share('msgNotification', $msgNotification);   
            }
                $setting=DB::table('settings')->select('option_value')->whereOptionKey('CONTACT_PHONE')->first(); 
                $settingFooter=DB::table('settings')->select('option_value')->whereOptionKey('FOOTER_TEXT')->first(); 
                view()->share('CONTACT_PHONE', $setting->option_value);
                view()->share('FOOTER_TEXT', $settingFooter->option_value);     
        });
    } 

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
