<?php
Route::get('/','HomeController@home')->name('home');  


Route::get('/search','SearchController@getHomeSearch');
Route::get('/search/get-cords','SearchController@getCords');       
Route::post('/search/ajax-search','SearchController@homeSearch');
Route::post('/search/ajax-search-map','SearchController@homeSearchMap'); 
Route::get('/search/home-text-search','SearchController@homeTextSearch'); 

//Route::get('/search/home-text-search','SearchController@homeTextSearch'); 



Route::get('/testimg','AccomodationImagesController@test'); 

/*Route::get('/test','SearchController@test');  
Route::get('/test2','SearchController@test2'); 
Route::get('/clean','SearchController@cleanDB');*/

Route::get('auth/{provider}', 'UsersController@redirectToProvider');
Route::get('auth/{provider}/callback', 'UsersController@handleProviderCallback');  


Route::view('/login','users.login')->name('getlogin');

Route::post('/login','UsersController@login')->name('login');
Route::post('/login-view','UsersController@loginView')->name('loginView'); 

Route::get('/logout','UsersController@logout')->name('logout'); 
Route::get('/forgot-password','UsersController@getForgotPassword')->name('getForgotPassword'); 
Route::post('/forgot-password','UsersController@postForgotPassword')->name('postForgotPassword');
Route::get('/users/reset-password/{token}','UsersController@getResetPassword');  
Route::post('/users/reset-password/{token}','UsersController@postResetPassword');   
Route::get('/news','HomeController@getNews')->name('getNews');  
Route::get('/last-minutes','HomeController@getLastMinutes')->name('getLastMinutes');  
Route::get('/advertisers','HomeController@getAdvertisers')->name('getAdvertisers');  
Route::get('/contact-us','HomeController@getContactUs')->name('getContactUs');   
Route::post('/contact-us','HomeController@postContactUs')->name('postContactUs'); 
Route::get('/refresh-captcha', 'HomeController@refreshCaptcha')->name('refresh_captcha');   
 
 
Route::get('/accomodations/view/{slug}','AccomodationsController@view');  
Route::get('/accomodations/getgal-image','AccomodationsController@getGalImgs'); 

Route::get('/users/adv-register','UsersController@getAdvertiserRegister')->name('getAdvertiserRegister');
Route::post('/users/adv-register','UsersController@postAdvertiserRegister')->name('postAdvertiserRegister');

Route::get('/users/cust-register','UsersController@getCustomerRegister')->name('getCustomerRegister');
Route::post('/users/cust-register','UsersController@postCustomerRegister')->name('postCustomerRegister'); 
Route::post('/users/cust-register-view','UsersController@postCustomerRegisterView')->name('postCustomerRegisterView');  

Route::get('/users/verification/{token}','UsersController@verfiyUser')->name('verfiyUser');  

Route::get('/tos','HomeController@tos')->name('tos'); 
Route::get('/privacy','HomeController@privacy')->name('privacy');  

 
Route::get('/auth/{provider}', 'UsersController@redirectToProvider');
Route::get('/auth/{provider}/callback', 'UsersController@handleProviderCallback');

Route::get('/site-visit','HomeController@siteVisits')->name('siteVisits');  
Route::get('/stastics/accom-visit','SocialMediaStatisticsController@accomVisits')->name('accomVisits');   

Route::get('/bookings/avability-dates','BookingsController@getAvabilityDates')->name('getAvabilityDates'); 

Route::get('/bookings/get-booking-price','BookingsController@getBookingPrice')->name('getBookingPrice');


// BOOKINGS
Route::get('/bookings/show-prices','BookingsController@showPrices');
Route::get('/bookings/allot-rooms','BookingsController@allotRooms');




 


// CHATTING   

Route::post('/tickets/contact-advertiser','MessageController@contactAdvertiser')->name('contactAdvertiser');      
Route::post('/tickets/contact-customer','MessageController@contactCustomer')->name('contactCustomer');    
Route::post('/tickets/contact-owner','MessageController@contactOwner')->name('contactOwner');    
Route::post('/tickets/contact-admin','MessageController@contactAdmin')->name('contactAdmin');      



Route::get('/tickets/refresh-chats','MessageController@refreshChats')->name('refreshChats');  
Route::get('/tickets/refresh-chats-owner','MessageController@refreshChatsOwner')->name('refreshChatsOwner');  
Route::get('/tickets/refresh-chats-adv','MessageController@refreshChatsAdvertiser')->name('refreshChatsAdvertiser');   
Route::get('/tickets/refresh-chats-admin','MessageController@refreshChatsAdmin')->name('refreshChatsAdmin');   
   
Route::get('/tickets/adv-message','MessageController@advMessages')->name('advMessages');
Route::get('/tickets/adv-message-ajax','MessageController@advMessagesAjax')->name('advMessagesAjax');
Route::get('/tickets/chat/{id}','MessageController@chat')->name('chat');  

 
//Route::post('/tickets/contact-customer-start','MessageController@contactCustomer')->name('contactCustomer');    


//Route::get('/tickets/contact-owner/{accom}','MessageController@custChat')->name('custChat'); 

Route::get('/tickets/cust-chat/{id}','MessageController@custChat')->name('custChat'); 
Route::get('/tickets/contact-owner/{accom}','MessageController@ownerChat')->name('ownerChat'); 
// Route::get('/tickets/refresh-chats-cust','MessageController@');








Route::get('/tickets/message','MessageController@getAdminMessage')->name('getAdminMessage');
Route::get('/tickets/message-ajax','MessageController@getAdminMessageAjax')->name('getAdminMessageAjax');
Route::get('/tickets/admin-chat/{id}','MessageController@adminChat')->name('adminChat');

Route::get('/tickets/approve/{id}','MessageController@approveMsg')->name('approveMsg');
Route::get('/tickets/approve-reply/{id}','MessageController@approveMsgReply')->name('approveMsgReply');
Route::get('/tickets/delete/{id}','MessageController@deleteMsg')->name('deleteMsg');




Route::get('/accomodations/get_regions','AccomodationAddressController@getRegions')->name('getRegion');
Route::get('/accomodations/get_departments','AccomodationAddressController@getDepartments')->name('getDepartment');





 



  

// User Routes i.e. Only authenticated user is allowed to access these routes
Route::group(['middleware' => ['auth','admin_customer_advertiser']], function () {  
	Route::get('/bookings/view/{id}','BookingsController@view');
	Route::get('/users/profile/{id}','UsersController@profile')->name('getProfile');
	Route::post('/users/profile/{id}','UsersController@updateProfile')->name('updateProfile');
	Route::get('/users/change_password','UsersController@showChangePassword')->name('showChangePassword');   
	Route::post('/users/change_password','UsersController@changePassword')->name('changePassword'); 
	Route::get('/users/switch_back/{id}','UsersController@switchAccountBack')->name('switchAccountBack');  

	Route::get('/settings/getall_helps','Settings@getAllHelps')->name('get_all_helps');
	Route::get('/settings/get_help_byid','Settings@get_help_byid');

	Route::get('/notifications/mark-seen','NotificationsController@markSeen')->name('markSeen');
	Route::get('/notifications/mark-seen-msg','NotificationsController@markSeenMsg')->name('markSeenMsg');

	Route::get('/awards/view/{id}','AwardsController@viewAward')->name('viewAward');
	
 	Route::get('/extra-tabs/view/{id}','ExtraTabsController@getTabView')->name('getTabView'); 
 	Route::get('/extra-tabs/view-update/{id}','ExtraTabsController@getTabViewUpdate')->name('getTabViewUpdate');   

 	Route::get('/extra-tabs/edit/{id}','ExtraTabsController@getTabEdit')->name('getTabEdit');   
 	Route::post('/extra-tabs/edit/{id}','ExtraTabsController@postTabEdit')->name('postTabEdit'); 


	// Only Admin and advertiser
	Route::get('/accomodations/view-accom/{slug}','AccomodationsController@viewAccom')->name('viewAccom');  
	Route::get('/accomodations/view-accom-free/{id}','AccomodationsController@viewAccomFree')->name('viewAccomFree');   

	Route::get('/accomodations/edit/{id}','AccomodationsController@getEditAccom')->name('getEditAccom');
	Route::post('/accomodations/edit/{id}','AccomodationsController@postEditAccom')->name('postEditAccom'); 

	Route::get('/accomodations/edit-free/{id}','AccomodationsController@getEditFreeAccom')->name('getEditFreeAccom'); 
	Route::post('/accomodations/edit-free/{id}','AccomodationsController@postEditFreeAccom')->name('postEditFreeAccom'); 


	Route::post('/accomodations/edit_accom_price/{id}','AccomodationPricesController@postAccomPricesEdit')->name('postAccomPricesEdit'); 
	Route::get('/accomodations/edit_extra_charge/{id}','AccomodationPricesController@getAccomExtraCharge')->name('getAccomExtraCharge');  
	Route::get('/accomodations/get-accomodation-prices','AccomodationPricesController@getAccomodationPrices')->name('getAccomodationPrices'); 

	Route::get('/accomodations/get-accomodation-prices-edit','AccomodationPricesController@getAccomodationPricesEdit')->name('getAccomodationPricesEdit'); 


	

	Route::post('/accomodations/edit_extra_charge','AccomodationPricesController@postAccomExtraCharge')->name('postAccomExtraCharge');  
	Route::get('/accomodations/delete_price/{type}/{id}/{room?}/{roomP?}','AccomodationPricesController@deleteAccomPrice')->name('deleteAccomPrice'); 
	Route::get('/accomodations/delete_extra_charge/{id}','AccomodationPricesController@deleteAccomExtraCharge')->name('deleteAccomExtraCharge');  
	Route::get('/accomodations/get_calendar_prices','AccomodationPricesController@getAccomCalendarPrices')->name('getAccomCalendarPrices');  
	Route::get('/accomodations/delete-image/{id}/{accom}','AccomodationImagesController@deleteImage')->name('deleteGalleryImage');
	Route::get('/accomodations/edit-accom-location/{id}','AccomodationAddressController@getLocationEdit')->name('getLocationEdit');
	Route::post('/accomodations/edit-accom-location/{id}','AccomodationAddressController@postLocationEdit')->name('postLocationEdit'); 
	
	Route::get('/accomodations/edit-accom-images/{id}','AccomodationImagesController@getImagesEdit')->name('getImagesEdit');  
	Route::post('/accomodations/edit-accom-images/{id}','AccomodationImagesController@postPaidImages')->name('postPaidImages');  

	
	Route::get('/accomodations/edit-accom-price/{id}','AccomodationPricesController@getAccomPricesEdit')->name('getAccomPricesEdit');  
	Route::post('/accomodations/edit-sale-accom-price/{id}','AccomodationPricesController@postSaleAccomPriceEdit')->name('postSaleAccomPriceEdit');
	Route::get('/accomodations/edit-accom-amnities/{id}','AccomodationAmnitiesController@getAminityEdit')->name('getAminityEdit');
	Route::post('/accomodations/edit-accom-amnities/{id}','AccomodationAmnitiesController@postAminityEdit')->name('postAminityEdit');  
	Route::get('/accomodations/add_accom_amnities/{id}','AccomodationAmnitiesController@getAminityAdd')->name('getAminityAdd');
	Route::post('/accomodations/add_accom_amnities/{id}','AccomodationAmnitiesController@postAminityAdd')->name('postAminityAdd'); 
	Route::get('/accomodations/switch_prices/{id}','AccomodationPricesController@switchAccomodationPrice')->name('switchAccomodationPrice'); 
	Route::get('/accomodations/make-featured/{id}/{image}','AccomodationImagesController@makeFeatured')->name('makeFeatured');  

	Route::post('/accomodations/sort-images','AccomodationImagesController@sortImages')->name('sortImages');  
	Route::get('/subscription-licenses/generate-invoice/{key}.pdf','SubscriptionLicensesController@generateInvoice')->name('generateInvoice');
	Route::get('/promotion-licenses/generate-invoice/{key}.pdf','ProductLicensesController@generateProInvoice')->name('generateProInvoice');

	Route::get('/extra-tabs/delete/{id}','ExtraTabsController@deleteTab')->name('deleteTab');   

	Route::get('/old-products/generate-invoice/{id}','OldProductsController@generateOldInvoice')->name('generateOldInvoice');  
});   
       
// Admin Routes i.e. only Admin is allowed to access these routes
Route::group(['middleware'=>['auth','admin']],function(){ 

	Route::get('/bookings/admin-bookings','BookingsController@getAdminBookings')->name('getAdminBookings'); 
	Route::get('/bookings/admin-bookings-ajax','BookingsController@getAdminBookingsAjax')->name('getAdminBookingsAjax'); 


	Route::get('/tickets/edit','MessageController@editMessage');
	Route::post('/tickets/update-message','MessageController@updateMessage'); 

	Route::get('/users',function() { return view('users.index');})->name('listAllUsers'); 
	Route::get('/users/getUsers','UsersController@getUsers')->name('getUsers');
	Route::get('/users/add','UsersController@getAddUser')->name('getAddUser');
	Route::post('/users/add','UsersController@postAddUser')->name('postAddUser');
	Route::get('/users/edit/{id}','UsersController@getEdit')->name('getEdit');
	Route::post('/users/edit/{id}','UsersController@postEdit')->name('postEdit');
	Route::get('/users/delete/{id}','UsersController@delete'); 
	Route::get('/dashboard','DashboardController@adminDashboard')->name('adminDashboard');  
	Route::get('/users/switch/{id}','UsersController@switchAccount')->name('switchAccount'); 

	Route::get('/settings','Settings@index')->name('getSettings'); 
	Route::get('/settings/getSettings','Settings@getSettings')->name('get_setting_datatable');
	Route::get('/settings/help','Settings@help')->name('helps');
	Route::get('/settings/get_helps','Settings@getHelps')->name('get_helps');
	Route::post('/settings/update_help','Settings@updateHelp')->name('update_help');
	Route::get('/settings/edit_help/{id}','Settings@getHelpEdit')->name('get_helps_edit');  
	Route::get('/settings/view_help','Settings@getHelpView')->name('get_help_view');  
	Route::get('/settings/delete/{id}','Settings@delete');  
	Route::get('/setting/edit/{id}','Settings@edit');  
	Route::post('/setting/update','Settings@update')->name('update_setting');
	Route::get('/settings/privacy','Settings@privacy')->name('privacy'); 
	Route::post('/settings/privacy','Settings@updatePrivacy')->name('updatePrivacy');
	Route::get('/settings/tos','Settings@tos')->name('privacy');
	Route::post('/settings/tos','Settings@updateTos')->name('updateTos'); 
	Route::get('/settings/search_tags','Settings@searchTags')->name('searchTags');
	Route::post('/settings/search_tags','Settings@addSearchTags')->name('addSearchTags');  


	Route::get('/awards','AwardsController@index')->name('getAwards');
	Route::post('/awards/add','AwardsController@addAward')->name('addAward');
	Route::get('/awards/get-awards','AwardsController@getAwardsAjax')->name('getAwardsAjax');
	Route::get('/awards/delete/{id}','AwardsController@deleteAward')->name('deleteAward'); 
	
	
	Route::get('/awards/edit/{id}','AwardsController@getEditAward')->name('getEditAward'); 
	Route::post('/awards/edit/{id}','AwardsController@postEditAward')->name('postEditAward');

	Route::get('/awards/new-award','AwardsController@getNewAwards')->name('getNewAwards');
	Route::get('/awards/new-award-ajax','AwardsController@getNewAwardsAjax')->name('getNewAwardsAjax');
	Route::get('/awards/delete-award','AwardsController@getDeleteAwards')->name('getDeleteAwards'); 
	Route::get('/awards/delete-award-ajax','AwardsController@getDeleteAwardsAjax')->name('getDeleteAwardsAjax'); 

	Route::get('/awards/approve/{id}','AwardsController@approveAward')->name('approveAward');
	Route::get('/awards/delete-award/{id}','AwardsController@deleteAccomodationAward')->name('deleteAccomodationAward');  


	Route::get('/extra-tab/all-tabs','ExtraTabsController@listAllExtraTabs')->name('listAllExtraTabs');  
	Route::get('/extra-tabs/get-all-tabs','ExtraTabsController@getAllTabs')->name('getAllTabs'); 


	Route::get('/extra-tab/tabs-update','ExtraTabsController@listAllExtraTabsEdit')->name('listAllExtraTabsEdit');  
	Route::get('/extra-tabs/get-all-tabs-update','ExtraTabsController@getAllTabsEdit')->name('getAllTabsEdit'); 


	Route::get('/extra-tabs/approve/{id}','ExtraTabsController@approveTab')->name('approveTab'); 
	Route::get('/extra-tabs/reject/{id}','ExtraTabsController@rejectTab')->name('rejectTab');   

	Route::get('/extra-tabs/approve-update/{id}','ExtraTabsController@approveTabUpdate')->name('approveTabUpdate'); 
	Route::get('/extra-tabs/reject-update/','ExtraTabsController@rejectTabUpdate')->name('rejectTabUpdate');  

	Route::get('/old-products','OldProductsController@getAllProducts')->name('getAllProducts');
	Route::get('/old-products/get-all-ajax','OldProductsController@getAllProductsAjax')->name('getAllProductsAjax');


	Route::get('/products','ProductsController@index')->name('showAllProducts');
	Route::get('/products/add','ProductsController@getAdd')->name('getProductAdd'); 
	Route::get('/products/edit/{id}','ProductsController@getEdit')->name('getProductEdit');
	Route::post('/products/edit/{id}','ProductsController@postEdit')->name('postProductEdit'); 
	Route::post('/products/add','ProductsController@postAdd')->name('postProductAdd');  
	Route::get('/products/get_products','ProductsController@getProducts')->name('getProducts');	
 
	Route::get('/accomodations','AccomodationsController@getAccomodations')->name('getAccomodations'); 
	Route::get('/accomodations/get_accomodations','AccomodationsController@getAccomodationsAjax')->name('getAccomodationsAjax'); 
	Route::get('/accomodations/paid-view','AccomodationsController@getPaidView')->name('getPaidView'); 
	Route::get('/accomodations/no-cure-no-pay','AccomodationsController@getNoCureNoPayView')->name('getNoCureNoPayView'); 
	Route::get('/accomodations/un-approved-view','AccomodationsController@getUnApprovedView')->name('getUnApprovedView'); 
	Route::get('/accomodations/requested-delete-view','AccomodationsController@getRequestedDeleteView')->name('getRequestedDeleteView');  
	Route::get('/accomodations/requested-delete','AccomodationsController@getAccomodationsDelete')->name('getAccomodationsDelete');  
	Route::get('/accomodations/delete-accomodation/{id}','AccomodationsController@deleteAccom')->name('deleteAccom');
	Route::get('/accomodations/approve-accomodation/{id}','AccomodationsController@approveAccom')->name('approveAccom');
 
	Route::post('/accomodations/approve-edit-accomodation/{id}','AccomodationsController@approveEditAccom')->name('approveEditAccom'); 
	Route::get('/accomodations/reject-edit-accomodation/{id}','AccomodationsController@rejectEditAccom')->name('rejectEditAccom');

	Route::get('/accomodations/view-all-update','AccomodationsController@viewAllFreeUpdateRequest')->name('viewAllFreeUpdateRequest');   
	Route::get('/accomodations/view-update-ajax','AccomodationsController@getAllFreeUpdateRequestAjax')->name('getAllFreeUpdateRequestAjax');    
	 

	Route::get('/subscriptions','SubscriptionController@getSubscriptionsView')->name('getSubscriptionsView'); 
	Route::get('/subscriptions/get-subscriptions','SubscriptionController@getSubscriptions')->name('getSubscriptions'); 
	Route::get('/subscriptions/add','SubscriptionController@getSubscriptionsViewAdd')->name('getSubscriptionsView');
	Route::post('/subscriptions/add','SubscriptionController@postSubscriptionsViewAdd')->name('postSubscriptionsViewAdd');
	Route::get('/subscriptions/edit/{id}','SubscriptionController@getSubscriptionsViewEdit')->name('getSubscriptionsView');
	Route::post('/subscriptions/edit/{id}','SubscriptionController@postSubscriptionsViewEdit')->name('postSubscriptionsViewEdit');
	Route::get('/subscriptions/delete/{id}','SubscriptionController@deleteSubscription')->name('deleteSubscription');
 	

	Route::get('/subscription_licenses/licenses','SubscriptionLicensesController@licenses')->name('licenses');
	Route::get('/subscription-licenses/licenses','SubscriptionLicensesController@licenses')->name('licenses');
	Route::get('/subscription-licenses/get-all-licenses','SubscriptionLicensesController@getAllLicenses')->name('getAllLicenses');
	Route::get('/subscription-licenses/mark-pay/{id}','SubscriptionLicensesController@markPay')->name('markPay');
	Route::get('/subscription-licenses/mark-cancel/{id}','SubscriptionLicensesController@markCancel')->name('markCancel'); 


	Route::post('/subscription-licenses/contact-user','SubscriptionLicensesController@conatctUsers')->name('conatctUsers');

  
	Route::get('/subscription-licenses/give-free-license','SubscriptionLicensesController@getFreeSubscriptionLicense')->name('getFreeSubscriptionLicense');
	Route::post('/subscription-licenses/give-free-license','SubscriptionLicensesController@postFreeSubscriptionLicense')->name('postFreeSubscriptionLicense');
	Route::get('/subscription-licenses/get-user','SubscriptionLicensesController@getUser')->name('getUser');
	 
	Route::get('/subscription-licenses/all-coupons','SubscriptionLicensesController@getSubscriptionCoupons')->name('getSubscriptionCoupons');
	Route::get('/subscription-licenses/all-coupons-ajax','SubscriptionLicensesController@getAllSubscriptionCouponsAjax')->name('getAllSubscriptionCouponsAjax');

	Route::get('/promotion-licenses/license','ProductLicensesController@getAllProductLicenses')->name('getAllProductLicenses');
 
	Route::get('/promotions-license/get-all-licenses-ajax','ProductLicensesController@getAllProductLicensesAjax')->name('getAllProductLicensesAjax');

	Route::get('/promotions-license/get-catwise-products','ProductLicensesController@getAllProductLicensesCatWiseAjax')->name('getAllProductLicensesCatWiseAjax');
	Route::get('/promotions-license/get-all-tips','ProductLicensesController@getAllTips')->name('getAllTips');; 

	Route::get('/promotion-licenses/mark-pay/{id}','ProductLicensesController@markPayPro')->name('markPayPro');
	Route::get('/promotion-licenses/mark-cancel/{id}','ProductLicensesController@markCancelPro')->name('markCancelPro');  

	Route::get('/promotion-licenses/give-free-license','ProductLicensesController@giveFreeProductLicense')->name('giveFreeProductLicense'); 
	Route::post('/promotion-licenses/give-free-license','ProductLicensesController@postFreeProductLicense')->name('postFreeProductLicense'); 
	Route::get('/promotion-licenses/all-coupons','ProductLicensesController@getProductCoupons')->name('getProductCoupons');
	Route::get('/promotion-licenses/all-coupons-ajax','ProductLicensesController@getAllProductCouponsAjax')->name('getAllProductCouponsAjax');

	Route::get('/accomodations/view-new-location','AccomodationAddressController@getNewLocations')->name('getNewLocations');
	Route::get('/accomodations/edit-new-locations/{c}/{r}/{d}','AccomodationAddressController@getEditNewLocations')->name('getEditNewLocations');
	Route::post('/accomodations/edit-new-locations/{c}/{r}/{d}','AccomodationAddressController@postEditNewLocations')->name('postEditNewLocations');  

	
	Route::get('/accomodations/add-new-locations','AccomodationAddressController@getAddNewLocation')->name('getAddNewLocation');
	Route::post('/accomodations/add-new-locations','AccomodationAddressController@postAddNewLocation')->name('postAddNewLocation');
	Route::post('/accomodations/add-new-region','AccomodationAddressController@postAddNewRegion')->name('postAddNewRegion');
	Route::post('/accomodations/add-new-department','AccomodationAddressController@postAddNewDepartment')->name('postAddNewDepartment'); 

	Route::get('/subscription-payments/payments','SubscriptionPaymentsController@payments')->name('payments');
	Route::get('/subscription-payments/payments-ajax ','SubscriptionPaymentsController@getAdminSubscriptionPaymentsAjax')->name('getAdminSubscriptionPaymentsAjax'); 
	Route::get('/subscription-payments/payments-income','SubscriptionPaymentsController@subPaymentsIncome')->name('subPaymentsIncome');   
	Route::get('/subscription-payments/payments-income-ajax','SubscriptionPaymentsController@subPaymentsIncomeAjax')->name('subPaymentsIncomeAjax');   
	Route::get('/subscription-payments/find-sum','SubscriptionPaymentsController@findSubTotalPaidAmount')->name('findSubTotalPaidAmount'); 
	Route::get('/promotion-payments/payments','ProductPaymentsController@payments')->name('proPayments');  
	Route::get('/promotion-payments/pro-payments-ajax ','ProductPaymentsController@getAdminProductPaymentsAjax')->name('getAdminProductPaymentsAjax'); 
	Route::get('/promotion-payments/payments-income','ProductPaymentsController@paymentsIncome')->name('paymentsIncome');   
	Route::get('/promotion-payments/payments-income-ajax','ProductPaymentsController@paymentsIncomeAjax')->name('paymentsIncomeAjax');  
	Route::get('/promotion-payments/find-sum','ProductPaymentsController@findTotalPaidAmount')->name('findTotalPaidAmount');  

	Route::get('/accomodations/slider-accomodations','AccomodationsController@getSliderAccomodation')->name('getSliderAccomodation');   
	Route::post('/accomodations/slider-accomodations','AccomodationsController@postSliderAccomodation')->name('postSliderAccomodation');

	Route::get('/newsletter/add','NewsletterController@getAddNewsletter')->name('getAddNewsletter');    
	Route::post('/newsletter/add','NewsletterController@postAddNewsletter')->name('postAddNewsletter'); 
	Route::get('/newsletter','NewsletterController@index')->name('getAllNewsletter');
	Route::get('/newsletter-ajax','NewsletterController@getNewsletterAjax')->name('getAllNewsletterAjax');        
	Route::get('/newsletter/delete/{id}','NewsletterController@delete')->name('deleteNewsletter');      
	Route::get('/newsletter/edit/{id}','NewsletterController@getEdit')->name('getNewsletterEdit');      
	Route::post('/newsletter/edit/{id}','NewsletterController@postEdit')->name('postNewsletterEdit');  
	Route::get('/newsletter/view/{id}','NewsletterController@view')->name('getNewsletterView');       
	Route::get('/newsletter/preview/{id}','NewsletterController@preview')->name('previewNewsletter');       	
	Route::post('/newsletter/send/{id}','NewsletterController@send')->name('send');   
	Route::get('/newsletter/subscribers','NewsletterController@subscribers')->name('newsletterSubscribers');       	
	Route::get('/newsletter/subscribers-ajax','NewsletterController@subscribersAjax')->name('newsletterSubscribersAjax'); 
	Route::get('/newsletter/delete-subscriber/{id}','NewsletterController@deleteSubscriber')->name('deleteSubscriber');
});  
 
  
// Advertiser Routes i.e. only Advertiser is allowed to access these routes 
Route::group(['middleware'=>['auth','advertiser']],function() { 

	Route::get('/bookings/adv-bookings','BookingsController@getAdvBookings')->name('getAdvBookings');
	Route::get('/bookings/adv-bookings-ajax','BookingsController@getAdvBookingsAjax')->name('getAdvBookingsAjax');

	Route::get('/bookings/edit/{id}','BookingsController@editBooking')->name('editBooking');
	Route::post('/bookings/edit/{id}','BookingsController@postEditBooking')->name('postEditBooking');

	Route::get('/bookings/approve/{id}','BookingsController@approveBooking')->name('approveBooking');
	Route::get('/bookings/reject/{id}','BookingsController@rejectBooking')->name('rejectBooking');
	Route::get('/bookings/compelete/{id}','BookingsController@completeBooking')->name('completeBooking'); 
	Route::get('/bookings/pay/{id}','BookingsController@pay');  
	 
	Route::post('/bookings/mollie','BookingsController@mollieBooking');
	Route::post('/bookings/mollie-return/{id}','BookingsController@mollieBookingReturn');
	Route::get('/bookings/mollie-return/{id}','BookingsController@mollieBookingReturn');

	Route::get('/bookings/invoices','BookingsController@getBookingInvoices')->name('getBookingInvoices');
	Route::get('/bookings/invoices-ajax','BookingsController@getBookingInvoicesAjax')->name('getBookingInvoicesAjax');
	Route::get('/bookings/generate-invoice/{key}','BookingsController@generateBookingInvoice')->name('generateBookingInvoice'); 
	

	 


	Route::get('/dashboard/adv-dashboard','DashboardController@advDashboard')->name('advDashboard');
	Route::post('/adv-dashboard/quick-mail','DashboardController@sendQuickMail')->name('sendQuickMail');  
	Route::get('/accomodations/add','AccomodationsController@getAdd')->name('getAddAccomAdv'); 
	Route::get('/accomodations/add_free','AccomodationsController@getAddFree')->name('getAddFreeAdv');
	Route::post('/accomodations/add_free','AccomodationsController@postAddFree')->name('postAddFreeAdv');  

	Route::get('/accomodations/add_accom','AccomodationsController@getAddAccom')->name('getAddAccomAdv');
	Route::post('/accomodations/add_accom','AccomodationsController@postAddAccom')->name('postAddAccomAdv'); 
	Route::get('/accomodations/add_accom_price/{id}','AccomodationPricesController@getAccomPricesAdd')->name('getPaidPricesAdd');  
	Route::post('/accomodations/add_accom_price/{id}','AccomodationPricesController@postAccomPricesAdd')->name('postAccomPricesAdd');
	Route::post('/accomodations/add-sale-accom-price/{id}','AccomodationPricesController@postSaleAccomPriceAdd')->name('postSaleAccomPriceAdd'); 
	 

	Route::post('/accomodations/add_paid_extra_price/{id}','AccomodationPricesController@postExtraPricesAdd')->name('postPaidExtraPricesAdd');  
	Route::get('/accomodations/add_accom_images/{id}','AccomodationImagesController@getPaidImages')->name('getPaidImages');  
	Route::post('/accomodations/add_accom_images/{id}','AccomodationImagesController@postPaidImages')->name('postPaidImages'); 
	Route::get('/accomodations/add_accom_location/{id}','AccomodationAddressController@getLocationAdd')->name('getLocationAdd');
	Route::post('/accomodations/add_accom_location/{id}','AccomodationAddressController@postLocationAdd')->name('postLocationAdd');    
	
	 
	Route::get('/accomodations/my-accomodations','AccomodationsController@getMyAccomodations')->name('getMyAccomodations'); 
	Route::get('/accomodations/get_my_accomodations','AccomodationsController@getMyAccomodationsAjax')->name('getMyAccomodationsAjax'); 
	Route::get('/accomodations/delete/{id}','AccomodationsController@deleteRequest')->name('deleteRequest'); 

	Route::get('/subscription-licenses','SubscriptionLicensesController@getAdvertiserLicensesView')->name('getAdvertiserLicensesView');
	Route::get('/subscription-licenses/get-licenses','SubscriptionLicensesController@getAdvertiserLicenses')->name('getAdvertiserLicenses');

 
	Route::get('/accomodations/pay/{id}','AccomodationsController@pay')->name('pay');   
	Route::get('/subscriptions/choose-payment/{id}','SubscriptionController@choosePayment')->name('choosePayment');

	Route::post('/subscriptions/bank-transfer','SubscriptionLicensesController@bankTransfer')->name('bankTransfer');
	Route::get('/subscription-licenses/find-unpaid-invoice','SubscriptionLicensesController@findUnpaidInvoice')->name('findUnpaidInvoice'); 
	Route::get('/subscriptions/pay-renew/{id}','SubscriptionController@payRenew')->name('payRenew');   
	Route::get('/subscriptions/choose-renew-payment/{id}','SubscriptionController@chooseRenewPayment')->name('chooseRenewPayment');   
	Route::post('/subscriptions/bank-transfer-renew','SubscriptionLicensesController@bankTransferRenew')->name('bankTransferRenew'); 
	Route::post('/subscription-licenses/pay-pal/{accom}','SubscriptionLicensesController@newSubscriptionPayPalSuccess')->name('newSubscriptionPayPalSuccess'); 
	Route::post('/subscription-licenses/pay-pal-renew/{accom}','SubscriptionLicensesController@renewSubscriptionPayPalSuccess')->name('renewSubscriptionPayPalSuccess'); 

	Route::post('/subscriptions/use-coupon/{accom}','SubscriptionLicensesController@useCoupon')->name('useCoupon'); 
	Route::post('/subscriptions/use-coupon-renew/{accom}','SubscriptionLicensesController@useCouponRenew')->name('useCouponRenew'); 

	/* MOOLIE */
	Route::get('/subscription-licenses/mollie/{accom}','SubscriptionLicensesController@moollie')->name('moollie'); 
	Route::post('/subscription-licenses/mollie/{accom}','SubscriptionLicensesController@moollie')->name('moollie'); 
	Route::get('/subscription-licenses/mollie-return/{accom}','SubscriptionLicensesController@mollieReturn')->name('mollieReturn'); 
	Route::post('/subscription-licenses/mollie-return','SubscriptionLicensesController@mollieReturn')->name('mollieReturn');   
	Route::get('/subscription-licenses/mollie-renew/{accom}','SubscriptionLicensesController@moollieRenew')->name('moollieRenew');  
	Route::post('/subscription-licenses/mollie-renew/{accom}','SubscriptionLicensesController@moollieRenew')->name('moollieRenew');  
	Route::get('/subscription-licenses/mollie-return-renew/{accom}','SubscriptionLicensesController@mollieReturnRenew')->name('mollieReturnRenew'); 

	
	/* Promotions */ 

	Route::get('/promotions/buy','ProductLicensesController@buyPromotion')->name('buyPromotion'); 
	Route::get('/promotions/get-promotion','ProductLicensesController@getPromotion')->name('getPromotion');
	Route::get('/promotions/my-promotions','ProductLicensesController@myPromotions')->name('myPromotions');
	Route::get('/promotions/get-my-promotions','ProductLicensesController@getMyPromotions')->name('getMyPromotions'); 
	Route::get('/promotions/my-tips','ProductLicensesController@getMyTips')->name('getMyTips');
	Route::get('/promotions/my-spotlights','ProductLicensesController@getMySpotlights')->name('getMySpotlights');
	Route::get('/promotions/my-lastminutes','ProductLicensesController@getMyLastMinutes')->name('getMyLastMinutes');
	Route::get('/promotions/my-news','ProductLicensesController@getMyNews')->name('getMyNews');
	Route::get('/promotions/get-my-all-promotions','ProductLicensesController@getMyAllPromotions')->name('getMyAllPromotions');
	Route::post('/promotions/checkout','ProductLicensesController@checkOut')->name('checkOut'); 
	Route::get('/promotions/checkout','ProductLicensesController@checkOut')->name('checkOut'); 
	Route::post('/promotion-licenses/pay-pal','ProductLicensesController@payPalSuccess')->name('payPalSuccess');
	Route::get('/promotion-licenses/mollie','ProductLicensesController@mollieProduct')->name('mollieProduct'); 
	Route::post('/promotion-licenses/mollie','ProductLicensesController@mollieProduct')->name('mollieProduct');  
	Route::get('/promotion-licenses/mollie-return/{accom}','ProductLicensesController@mollieProductReturn')->name('mollieProductReturn'); 
	Route::post('/promotion-licenses/mollie-return','ProductLicensesController@mollieProductReturn')->name('mollieProductReturn');   
	Route::post('/promotion-licenses/bank-transfer','ProductLicensesController@bankTransferPro')->name('bankTransferPro');  
	Route::post('/promotion-licenses/use-free-license','ProductLicensesController@useFreeProCoupon')->name('useFreeProCoupon');      
	Route::get('/promotion/add-news/{id}','ProductsController@addNews')->name('addNews');
	Route::get('/promotion/add-lastminute/{id}','ProductsController@addLastMinute')->name('addLastMinute'); 
	Route::post('/promotion/add-news/{id}','ProductsController@postAddNews')->name('postAddNews'); 
	Route::post('/promotion/add-lastminute/{id}','ProductsController@postAddLastMinute')->name('postAddLastMinute');   
	Route::get('/promotion/edit-news/{id}','ProductsController@editNews')->name('editNews'); 
	Route::get('/promotion/edit-lastminute/{id}','ProductsController@editLastMinute')->name('editLastMinute');  
	Route::post('/promotion/edit-news/{id}','ProductsController@postEditNews')->name('postEditNews'); 
	Route::post('/promotion/edit-lastminute/{id}','ProductsController@postEditLastMinute')->name('postEditLastMinute');     
	Route::get('/subscription-payments','SubscriptionPaymentsController@index')->name('getSubscriptionPayments');
	Route::get('/subscription-payments/get-payments-ajax ','SubscriptionPaymentsController@getSubscriptionPaymentsAjax')->name('getSubscriptionPaymentsAjax'); 
	Route::get('/promotion-payments','ProductPaymentsController@index')->name('getProductPayments'); 
	Route::get('/promotion-payments/get-payments-ajax ','ProductPaymentsController@getProductPaymentsAjax')->name('getProductPaymentsAjax'); 
	Route::get('/subscription-payments/history','SubscriptionPaymentsController@subscriptionHistory')->name('getSubscriptionHistory'); 
	Route::get('/subscription-payments/history-ajax','SubscriptionPaymentsController@subscriptionHistoryAjax')->name('getSubscriptionHistoryAjax'); 
	Route::get('/promotion-payments/history','ProductPaymentsController@getProSubscriptionHistory')->name('getProSubscriptionHistory'); 
	Route::get('/promotion-payments/history-ajax','ProductPaymentsController@getProSubscriptionHistoryAjax')->name('getProSubscriptionHistoryAjax'); 
	Route::get('/awards/request','AwardsController@advertiserAddAward')->name('advertiserAddAward'); 
	Route::post('/awards/request','AwardsController@postAdvertiserAddAward')->name('postAdvertiserAddAward');   
	Route::get('/awards/my-awards','AwardsController@getMyAwards')->name('getMyAwards'); 
	Route::get('/awards/my-awards-ajax','AwardsController@getMyAwardsAjax')->name('getMyAwardsAjax');   
	Route::get('/awards/request-delete/{id}','AwardsController@requestAccomodationDeleteAward')->name('requestAccomodationDeleteAward');
	Route::post('/awards/ask_award','AwardsController@askAward')->name('askAward');   
	Route::get('/extra-tabs','ExtraTabsController@getAdvExtraTabs')->name('getAdvExtraTabs');
	Route::get('/extra-tabs/add','ExtraTabsController@getAccomAdd')->name('getAccomAdd');
	Route::post('/extra-tabs/add','ExtraTabsController@postAccomAdd')->name('postAccomAdd'); 
	Route::get('/extra-tabs/get-advtabs','ExtraTabsController@getAdvertiserTabs')->name('getAdvertiserTabs'); 
 	Route::get('/extra-tabs/edit-free/{id}','ExtraTabsController@getTabEditFree')->name('getTabEditFree');
 	Route::post('/extra-tabs/edit-free/{id}','ExtraTabsController@postTabEditFree')->name('postTabEditFree');  
 	Route::get('/old-products/my-invoices','OldProductsController@getAdvertiserProducts')->name('getAdvertiserProducts');
 	Route::get('/old-products/get-all-my-invoices','OldProductsController@getAdvertiserProductsAjax')->name('getAdvertiserProductsAjax');
 	Route::get('/subscriptions/switch-to-free','SubscriptionController@getSwitchToFree')->name('getSwitchToFree');
 	Route::post('/subscriptions/switch-to-free','SubscriptionController@postSwitchToFree')->name('postSwitchToFree'); 
 	Route::get('/subscriptions/switch-to-paid','SubscriptionController@getSwitchToPaid')->name('getSwitchToPaid');
 	Route::post('/subscriptions/switch-to-paid','SubscriptionController@postSwitchToPaid')->name('postSwitchToPaid');  

 	Route::get('/social-media/post','SocialMediaStatisticsController@getPostToMedia')->name('getPostToMedia');
 	Route::post('/social-media/post','SocialMediaStatisticsController@postPostToMedia')->name('postPostToMedia');
 	Route::get('/social-media/statistics','SocialMediaStatisticsController@getStatistics')->name('getStatistics');  
 	Route::get('/social-media/statistics-ajax','SocialMediaStatisticsController@getAccomStats')->name('getAccomStats');  
 	Route::get('/social-media/monthly-statistics-ajax','SocialMediaStatisticsController@getMonthlyStats')->name('getMonthlyStats');  
 	Route::get('/social-media/yearly-statistics-ajax','SocialMediaStatisticsController@getYearlyStats')->name('getYearlyStats');  
 	Route::get('/social-media/daily-statistics-ajax','SocialMediaStatisticsController@getDailyStats')->name('getDailyStats');  
 	Route::get('/social-media/country-statistics-ajax','SocialMediaStatisticsController@getCountryStats')->name('getCountryStats');  
 	Route::get('/social-media/region-statistics-ajax','SocialMediaStatisticsController@getRegionStats')->name('getRegionStats');  
 	Route::get('/social-media/department-statistics-ajax','SocialMediaStatisticsController@getDepartmentStats')->name('getDepartmentStats');  
 	Route::get('/social-media/all-statistics-ajax','SocialMediaStatisticsController@getAllStats')->name('getAllStats');  

 	 

 	Route::get('/bookings/no-avaibality','BookingsController@getNoAvaibility')->name('getNoAvaibility'); 
 	Route::post('/bookings/add-noavability','BookingsController@addNoAvaibility')->name('addNoAvaibility'); 
 	Route::get('/bookings/get-accom-noavability/{accom}','BookingsController@getAccomNoAvaibility')->name('getAccomNoAvaibility'); 
 	Route::post('/bookings/update-noavability','BookingsController@updateNoAvaibility')->name('updateNoAvaibility');  
 	Route::get('/bookings/delete-noavability/{id}','BookingsController@deleteNoAvaibility')->name('deleteNoAvaibility'); 
 	Route::get('/bookings/get-no-of-rooms/{accom}','BookingsController@getNoOfRooms')->name('getNoOfRooms');  
 	
 	

 	



});

Route::get('/subscription-licenses/mollie-webhook/{accom}','SubscriptionLicensesController@mollieWebhook')->name('mollieWebhook');   
Route::post('/subscription-licenses/mollie-webhook/{accom}','SubscriptionLicensesController@mollieWebhook')->name('mollieWebhook');  

Route::post('/bookings/mollie-webhook/{id}','SubscriptionLicensesController@mollieWebhook')->name('mollieWebhook');   
 

//Route::get('/subscription-licenses/mollie/{accom}','SubscriptionController@moollie')->name('moollie'); 



Route::get('/bookings/write-review/{id}/{token}','BookingsController@writeReview')->name('writeReview');
Route::post('/bookings/write-review','BookingsController@postWriteReview')->name('postWriteReview');

// Customer Routes i.e. Only Customer user is allowed to access these routes
Route::group(['middleware' => ['auth','customer']], function () {  
	Route::get('/dashboard/cust-dashboard','DashboardController@custDashboard')->name('custDashboard');
	Route::get('/messages/my-messages','MessageController@custMessages')->name('custMessages'); 
	Route::get('/messages/my-messages-ajax','MessageController@custMessagesAjax')->name('custMessagesAjax'); 
	Route::get('/bookings/make-booking','BookingsController@makeBooking');
	Route::get('/bookings/my-bookings','BookingsController@myBookings')->name('myBookings'); 
	Route::get('/bookings/my-bookings-ajax','BookingsController@myBookingsAjax')->name('myBookingsAjax'); 
});






                 
  
