var map;
var markerss=[];
var markerArr=[];
jQuery(document).ready(function($){
		initAutocomplete();
    $('#country_id').change(function(){
      if($(this).val()=='other') {
        setOthers(); 
      } else {
        getRegions($(this).val());      
      }
    });
    $('#region_id').change(function(){ 
      if($(this).val()=='other') {
        setOthers();
      } else {
        getDepartments($(this).val());      
      }
    });
     $('#department_id').change(function(){ 
      if($(this).val()=='other') {
        setOthers();
      } else {
        if($('#otherDepartment').is(":visible")) {
          $('#otherDepartment').hide(); 
        }
      }
     });
}); 
function initAutocomplete(){ 
   var myLatLng = {lat: 46.2276, lng: 2.2137};
   map = new google.maps.Map(document.getElementById('map'), {    
       zoom: 2,
       center: myLatLng
   });
} 

function ucfirst(str,force){
  str=force ? str.toLowerCase() : str;
  return str.replace(/(\b)([a-zA-Z])/,
     function(firstLetter){
      return   firstLetter.toUpperCase();
  });
}

function clearLocations(){
  for (var i = 0; i < markerArr.length; i++) {
    markerArr[i].setMap(null);
  }
  markerArr.length = 0;
} 

function findAddressOnMap(){
    var country='';
    var region='';
    var department='';
    var street='';
    var streetNo='';
    var city='';
    var zip='';
    
    var selectedCountry = document.getElementById('country_id');
    if($('#country_id').val()){
        country= ucfirst(selectedCountry.options[selectedCountry.selectedIndex].text,true);
    } 

    var selectedRegion = document.getElementById('region_id');
    if($('#region_id').val()){
        region= ucfirst(selectedRegion.options[selectedRegion.selectedIndex].text,true); 
    }


   var selectedDepartment = document.getElementById('department_id');
   if($('#department_id').val()){ 
     department= ucfirst(selectedDepartment.options[selectedDepartment.selectedIndex].text,true);   
   }
   street=$('#street').val();
   streetNo=$('#street_number').val();
   city=$('#city_town').val();
   zip=$('#postal_code').val();

  
   if(department) {
      address=region+','+department+','+country;
   }

   if(region) {
      if(department) {
          address=region+','+department+','+country;
      } else {
          address=region+','+country;
      }
   }

   if(country) {
      if(region){
          address=region+','+department+','+country;
      } else if(department) {
           address=department+','+country;
      } else {
           address=country;
      }
   } else {
        address=''; 
   }

   if(city) {
      address=city+','+address; 
   }

   if(street) {
      address=street+','+address;
   }
   if(streetNo) {
     address=streetNo+','+address; 
   }
   if(zip) {
      address=address+'-'+zip;
   } 
    var geocoder= new google.maps.Geocoder();
    markers=''; 
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        clearLocations();
        map.setCenter(results[0].geometry.location);
        marker = new google.maps.Marker({
            map: map,
            draggable: true, 
            position: results[0].geometry.location
        });
        markerArr.push(marker);  

        $('#lat').val(results[0].geometry.location.lat());
        $('#lng').val(results[0].geometry.location.lng());  
 
        google.maps.event.addListener(marker, 'dragend', function(event) { 
            updateCoordinatesDisplayed(event.latLng.lat(), event.latLng.lng());
        });
        function updateCoordinatesDisplayed(latitude, longitude) {  

            if (document.getElementById('lat')) {
                document.getElementById('lat').value = latitude;
            }
            if (document.getElementById('lng')) { 
                document.getElementById('lng').value = longitude;
            }
        } 

      } 
      // else {
      //   alert('Geocode was not successful for the following reason: ' + status);
      // }
    });
} 
function getRegions(country){
    if(country) {
      $.ajax({
        url:'/accomodations/get_regions?country_id='+country,
        dataType:'json',
        success:function(r) {
          if(r) {
            $('#region_id').empty();
            $('#department_id').empty();
            $.each(r, function(key, value) {
                $('#region_id').append($("<option/>", {
                    value: value.id, 
                    text: value.region_name
                }));
            });
            findAddressOnMap();
          }
          $('#region_id').append($("<option/>", {
              value: 'other', 
              text: "Other"
          }));
          /*$('#otherCountry').hide(); 
          $('#otherRegion').hide(); 
          $('#otherDepartment').hide(); 
          $('#other_country').removeAttr('required');
          $('#other_region').removeAttr('required');
          $('#other_department').removeAttr('required');
          $('#regionDiv').show();
          $('#departmentDiv').show(); */
          removeOthers();
        }
      }); 
    }
}
function getDepartments(region){
    if(region) {
      $.ajax({
        url:'/accomodations/get_departments?region_id='+region,
        dataType:'json',
        success:function(r) { 
          if(r) {
            $('#department_id').empty();
            $.each(r, function(key, value) {
                $('#department_id').append($("<option/>", {
                    value: value.id,
                    text: value.department_name
                }));
            });
            findAddressOnMap();
          }
          $('#department_id').append($("<option/>", {
              value: 'other', 
              text: "Other"
          }));
         removeOthers();
        }
      }); 
    }
}
function setOthers() {
  if($('#country_id').val()=='other'){
    $('#otherCountry').show(); 
    $('#otherRegion').show(); 
    $('#otherDepartment').show();
    $('#regionDiv').hide();
    $('#departmentDiv').hide(); 
  }
  if($('#region_id').val()=='other') {
      $('#departmentDiv').hide(); 
      $('#otherRegion').show();
      $('#otherDepartment').show(); 
  }
  if($('#department_id').val()=='other') {
      $('#otherDepartment').show(); 
  }
} 
function removeOthers () {
  $('#otherCountry').hide(); 
  $('#otherRegion').hide(); 
  $('#otherDepartment').hide(); 
  $('#other_country').removeAttr('required');
  $('#other_region').removeAttr('required');
  $('#other_department').removeAttr('required');
  $('#regionDiv').show();
  $('#departmentDiv').show(); 
}






 