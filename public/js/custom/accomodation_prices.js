$(document).ready(function(){	
	// buildCalendar();
	showCalendarPrices();
	setChangeDayAfter($("input[name='changeday']:checked").val());
	$('input[type=radio][name=changeday]').change(function() {
		setChangeDay(this.value); 
	});
	$('#PriceStartDate').focus(function() { 
		checkChangeDay();
	});
	$('#PriceEndDate').focus(function() {
		checkChangeDay(); 
	});
	$('#PriceStartDatePerWeek').focus(function() {
		checkChangeDay();
	});
	$('#PriceEndDatePerWeek').focus(function() {
		checkChangeDay(); 
	});
	$('#PriceStartDatePerRoom').focus(function() {
		checkChangeDay();
	});
	$('#PriceEndDatePerRoom').focus(function() {
		checkChangeDay(); 
	});
	$('#PriceUnit').change(function() {
		showHideDiv($(this).val());
	});
}); 

function setChangeDayAfter(day){
	setChangeDay(day); 
}


function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}



CLICKEDDATE={};
function buildCalendar() {
	$('#calendar').fullCalendar({
		header    : {
		       left  : 'prev,next today',
		       center: 'title',
		       right : 'month,agendaWeek,agendaDay'
		     },
		     buttonText: {
		       today: 'today',
		       month: 'month',
		       week : 'week',
		       day  : 'day'
		     }, 
	    dayClick: function(date,jsEvent,view) {
	    	CLICKEDDATE=date;
	    	if(Date.parse(date)-Date.parse(new Date())<0) {
	    		alert('Sorry,you can add prices for future dates only'); 
	    	} else { 
	    		// PER WEEK 
	    		if(checkAccomType()=='perweek') {
	    			setDates('PriceStartDatePerWeek','PriceEndDatePerWeek',date);
	    			$('#priceModalPerWeek').modal('show');  
	    		} else {
	    			if($('#PriceUnit').val()=='perroom') {
	    				setDates('PriceStartDatePerRoom','PriceEndDatePerRoom',date); 
	    			} else {
	    				setDates('PriceStartDate','PriceEndDate',date);
	    			}					
	    			$('#priceModal').modal('show'); 	
	    		}
	    	}
	    }, 
	    // events: '/accomodations/get_calendar_prices?accom=64',

	}); 
}
function setDates(start,end,date) { 
	$("#"+start).datepicker('destroy');
	$("#"+end).datepicker('destroy');
	$('#'+start).val(date.format('DD-MM-YYYY'));
	$('#'+start).datepicker({
		changeMonth:true,
		changeYear:true,
		dateFormat:"dd-mm-yy",
		minDate:0,
		onSelect:function(date1) {
	    	var selectedDate=new Date(date1);
	    	var endDate=new Date(selectedDate.getTime());
	    	$("#"+end).datepicker("option","minDate",$("#"+start).datepicker("getDate"));   
		}
	});
	$('#'+end).datepicker({
		changeMonth:true,
		changeYear:true,
		dateFormat:"dd-mm-yy", 
		minDate:0 
	}); 
	$("#"+end).datepicker("option","minDate",new Date(date)); 
}
function setDateWithoutDate(start,end,showDay) {
	$('#'+start).val(''); 	
	$("#"+start).datepicker('destroy');
	$("#"+end).datepicker('destroy');
	if(showDay==0 || showDay==6) { 
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	var endDate=new Date(selectedDate.getTime() );
		    	// $('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	    $('#'+start).datepicker("getDate"));      
		    }, 
		    beforeShowDay: function(date) {
		    	console.log(date);
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    	// return [day==6,''];
		    }, 
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    beforeShowDay: function(date) {
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    } 
		});
	} else { 
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	var endDate=new Date(selectedDate.getTime() );
		    	$('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	     $('#'+start).datepicker("getDate"));     
		    }
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0
		});
	}	 
}
function setDateAndChangeDay(start,end,showDay) {
	//$('#'+start).val(''); 
	$("#"+start).datepicker('destroy');
	$("#"+end).datepicker('destroy');
	if(showDay==0 || showDay==6) {
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	console.log('onSelect');
		    	var endDate=new Date(selectedDate.getTime() );
		    	// $('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	    $('#'+start).datepicker("getDate"));       
		    }, 
		    beforeShowDay: function(date) {
		    	console.log('setDateAndChangeDay beforeShowDay'); 
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    } 
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    beforeShowDay: function(date) {
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    } 
		});
	} else { 
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	var endDate=new Date(selectedDate.getTime() );
		    	// $('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	     $('#'+start).datepicker("getDate"));     
		    }
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0
		});
	}	 
}
function setChangeDay(day) { 
	checkChangeDay(); 
	if(checkAccomType()=='perweek' ) {
		if(day=='saturday') {
			setDateWithoutDate('PriceStartDatePerWeek','PriceEndDatePerWeek',6);
			setDateAndChangeDay('PriceStartDatePerWeek','PriceEndDatePerWeek',6);
			//setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',6);
		} else if(day=='sunday') {	
			setDateWithoutDate('PriceStartDatePerWeek','PriceEndDatePerWeek',0);
			setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',0);
		} else {
			setDateWithoutDate('PriceStartDatePerWeek','PriceEndDatePerWeek',10); 
			setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',10);
		} 
	} else { 
		if(day=='saturday') {
			if($('#PriceUnit').val() !='') {
				if($('#PriceUnit').val()=='perroom') {
					setDateWithoutDate('PriceStartDatePerRoom','PriceEndDatePerRoom',6);
					setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',6);
				} else {
					/*setDateWithoutDate('PriceStartDate','PriceEndDate',6);
					setDateAndChangeDay('PriceStartDate','PriceEndDatePerRoomEdit',6);*/
					setDateWithoutDate('PriceStartDate','PriceEndDate',6);
					setDateAndChangeDay('PriceStartDate','PriceEndDate',6);

				}
			} 
		} else if(day=='sunday') {
			if($('#PriceUnit').val() !='') { 
				if($('#PriceUnit').val()=='perroom') {
					setDateWithoutDate('PriceStartDatePerRoom','PriceEndDatePerRoom',0);
					setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',0);
				} else {
					setDateAndChangeDay('PriceStartDate','PriceEndDate',0);
					setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',0);
				}
			}  
		} else {
			if($('#PriceUnit').val() !='') {
				if($('#PriceUnit').val()=='perroom') {
					setDateWithoutDate('PriceStartDatePerRoom','PriceEndDatePerRoom',10);
					setDateWithoutDate('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',10); 
				} else {
					setDateWithoutDate('PriceStartDate','PriceEndDate',10); 
					setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',10); 
					//setDateWithoutDate('PriceStartDate','PriceEndDate',10);
				}
			} 
		}  
	} 	
}   
function checkChangeDay() {   
	if(checkAccomType()=='perweek') {
		if ( typeof $("input[name='changeday']:checked").val() =='undefined') {
			$("#PriceStartDatePerWeek").datepicker('destroy');
			$("#PriceEndDatePerWeek").datepicker('destroy'); 
			$('#changeDayErrPerWeek').html('Please select change day');
			$('#addBtn').attr('disabled',true); 
		} else { 
			if($("input[name='changeday']:checked").val()=='saturday'){
				setDateAndChangeDay('PriceStartDatePerWeek','PriceEndDatePerWeek',6);  
				setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',6);
			} else if($("input[name='changeday']:checked").val()=='sunday'){
				setDateAndChangeDay('PriceStartDatePerWeek','PriceEndDatePerWeek',0);  
				setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',0);
			} else{
				setDateAndChangeDay('PriceStartDatePerWeek','PriceEndDatePerWeek',10);  
				setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',10);
			}
			$('#changeDayErrPerWeek').html(''); 
			$('#addBtn').attr('disabled',false); 
		}
	} else {
		if($('#PriceUnit').val() !='') {
			if($('#PriceUnit').val()=='perroom') {
				if ( typeof $("input[name='changeday']:checked").val() =='undefined') { 
					$("#PriceStartDatePerRoom").datepicker('destroy');
					$("#PriceEndDatePerRoom").datepicker('destroy');	
					$('#changeDayErr').html('Please select change day');	
					$('#addBtn').attr('disabled',true); 
				} else {
					if($("input[name='changeday']:checked").val()=='saturday'){
						setDateAndChangeDay('PriceStartDatePerRoom','PriceEndDatePerRoom',6);
						setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',6); 
					} else if($("input[name='changeday']:checked").val()=='sunday'){
						setDateAndChangeDay('PriceStartDatePerRoom','PriceEndDatePerRoom',0);
						setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',0); 
					} else{
						setDateAndChangeDay('PriceStartDatePerRoom','PriceEndDatePerRoom',10);
						setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',10); 
					}
					$('#changeDayErr').html(''); 
					$('#addBtn').attr('disabled',false); 
				}
			} else {
				if ( typeof $("input[name='changeday']:checked").val() =='undefined') { 
					$("#PriceStartDate").datepicker('destroy');
					$("#PriceEndDate").datepicker('destroy');
					$('#changeDayErr').html('Please select change day');	
					$('#addBtn').attr('disabled',true); 
				} else { 
					if($("input[name='changeday']:checked").val()=='saturday'){
						setDateAndChangeDay('PriceStartDate','PriceEndDate',6);
						setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',6); 
					} else if($("input[name='changeday']:checked").val()=='sunday'){
						setDateAndChangeDay('PriceStartDate','PriceEndDate',0);
						setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',0);  
					} else{
						console.log('332');
						setDateAndChangeDay('PriceStartDate','PriceEndDate',10); 
						setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',10); 
					}
					$('#changeDayErr').html(''); 
					$('#addBtn').attr('disabled',false); 
				}
			}
		}
	} 
}  
function showHideDiv(div) {	
	var showday;
	if ( typeof $("input[name='changeday']:checked").val() !=='undefined') { 
		if( $("input[name='changeday']:checked").val()=='saturday') {
			showday=6;
		} else if( $("input[name='changeday']:checked").val()=='sunday') {
			showday=0;
		} else {
			showday=10;
		}
	}
	if(div=='perroom') {
		$('#PerPersonDiv').hide();
		$('#PriceRoomType').removeAttr('required');
		$('#PriceEndDate').removeAttr('required');
		$('#Price1').removeAttr('required');
		$('#Price2').removeAttr('required');
		$('#Price3').removeAttr('required');
		setDateWithoutDate('PriceStartDatePerRoom','PriceEndDatePerRoom',showday); 
		$('#PerRoomDiv').show();  
	} else {
		$('#PerRoomDiv').hide(); 
		$('#PriceRoomTypePerRoom').removeAttr('required');
		$('#PriceStartDatePerRoom').removeAttr('required');
		$('#PriceEndDatePerRoom').removeAttr('required');
		$('#PriceAmountPerRoom').removeAttr('required');
		setDateWithoutDate('PriceStartDate','PriceEndDate',showday);  
		$('#PerPersonDiv').show();
	}
}  
function checkAccomType() {
	var str='';
	if($('#accomType').val()==1 || $('#accomType').val()==5 || $('#accomType').val()==6 || $('#accomType').val()==8 || 
	    $('#accomType').val()==9 || $('#accomType').val()==12 ) {
		str='perweek';
	} else {
		str='notperweek';
	}
	return str;
}  
function showHideChildDiv(child) {
	if(checkAccomType() !='perweek') {
		if(child>0) {
			if($('#PriceUnit').val() !='') {
				if($('#PriceUnit').val()=='perperson') {
					$('#maxChildAgePerPerson').show();
				} else {
					$('#maxChildAgeDivPerRoom').show();
				}
			}		
		}	
	}
}
function setNormalDate(start,end,showDay) {
	$("#"+start).datepicker('destroy');
	$("#"+end).datepicker('destroy');
	$('#'+start).datepicker({
		changeMonth:true,
		changeYear:true,
		dateFormat:"dd-mm-yy",
		minDate:0,
		onSelect:function(date1) {
	    	var selectedDate=new Date(date1);
	    	var endDate=new Date(selectedDate.getTime());
	    	$("#"+end).datepicker("option","minDate",$("#"+start).datepicker("getDate"));   
		},
		beforeShowDay: function(date) {
			var day=date.getDay();
			return [day==showDay,''];
		}
	});
	$('#'+end).datepicker({
		changeMonth:true,
		changeYear:true,
		dateFormat:"dd-mm-yy", 
		minDate:0,
		beforeShowDay: function(date) {
			var day=date.getDay();
			return [day==showDay,''];
		} 
	}); 
	$("#"+end).datepicker("option","minDate",$("#"+start).datepicker("getDate"));  
}
function displayCal(events) {  
	$('#calendar').fullCalendar( 'removeEvents'); 
	$('#calendar').fullCalendar( 'addEventSource', events);  
	$('#calendar').fullCalendar(
		{
			header    : {
			       left  : 'prev,next today',
			       center: 'title',
			       right : 'month,agendaWeek,agendaDay'
			     },
			     buttonText: { 
			       today: 'today',
			       month: 'month',
			       week : 'week',
			       day  : 'day'
			     }, 
		    dayClick: function(date,jsEvent,view) {
		    	// console.log(jsEvent);
		    	CLICKEDDATE=date;
		    	if(Date.parse(date)-Date.parse(new Date())<0) {
		    		alert('Sorry,you can add prices for future dates only'); 
		    	} else { 
		    		// PER WEEK 
		    		if(checkAccomType()=='perweek') {
		    			setDates('PriceStartDatePerWeek','PriceEndDatePerWeek',date);
		    			$('#priceModalPerWeek').modal('show');  
		    		} else  {
		    			
		    			if($('#PriceUnit').val()=='perroom') {
		    				console.log('PERROOM');
		    				setDates('PriceStartDatePerRoom','PriceEndDatePerRoom',date); 
		    			} else {
		    				console.log('PERPERSON'); 
		    				setDates('PriceStartDate','PriceEndDate',date);
		    			}					
		    			$('#priceModalNew').modal('show');   	
		    		}
		    	}
		    },
			events: events,
			eventRender: function(event, element) {
			    $(element).tooltip({title: event.hover});             
			}, 
			eventClick: function(event) {  
				console.log(event);
				var changeday=$("input[name='changeday']:checked"). val();
				var showday=0;
				if(changeday=='saturday') {
					showday=6;
				} else if ('changeday'=='sunday') { 
					showday=0;
				} else {
					showday=10;
				}
				if(event.price_unit=='perweek') {
					setDateAndChangeDay('PriceStartDatePerWeekEdit','PriceEndDatePerWeekEdit',showday);
					$("#EditPriceID").val(event.price_id);  
					$("#PriceStartDatePerWeekEdit").val(event.start_date); 
					$("#PriceEndDatePerWeekEdit").val(event.end_date);
					$('#PriceMinDay').val(event.min_days);
					$('#PriceAmountEdit').val(event.amount);
					$('#PriceMaxPersonEdit').val(event.max_persons);
					$('#PriceExtraPricePerPersonEdit').val(event.extra_price_per_person);
					$('#deletePriceWeek').attr('price_id',event.price_id);
					$('#EditPriceIDWeek').val(event.price_id);  
					$('#editPriceModal').modal('show');  	
				} else if(event.price_unit=='perperson')  {
					setDateAndChangeDay('PriceStartDateEdit','PriceEndDateEdit',showday); 
					$('#PriceRoomTypeEdit').val(event.room_type);
					$('#PriceRoomDescriptionEdit').val(event.description);
					$('#PriceStartDateEdit').val(event.start_date);
					$('#PriceEndDateEdit').val(event.end_date);
					$('#PriceMinDayEdit').val(event.min_days);
					$('#PriceNoOfRoomEdit').val(event.no_of_rooms);
					$('#PriceAdultEdit').val(event.adults);
					$('#PriceChildsEdit').val(event.childs);
					$('#PriceExtraPriceAdultEdit').val(event.extra_price_adult);
					$('#PriceExtraPriceChildEdit').val(event.extra_price_child);
					$('#PriceMaxPersonPerRoomEdit').val(event.max_persons_per_room);
					$('#Price1Edit').val(event.price1);
					$('#Price2Edit').val(event.price2);
					$('#Price3Edit').val(event.price3);
					$('#EditPriceIDPerson').val(event.id);
					$('#EditRoomIDPerson').val(event.room_id);
					$('#EditRoomPriceIDPerson').val(event.room_price_id);
					$('#deletePricePerson').attr('price_id',event.id);
					$('#deletePricePerson').attr('room_id',event.room_id);
					$('#deletePricePerson').attr('room_price_id',event.room_price_id);
					$('#editPricePersonModal').modal('show');   
				} else if(event.price_unit=='perroom') { 
					setDateAndChangeDay('PriceStartDatePerRoomEdit','PriceEndDatePerRoomEdit',showday);
					$("#PriceStartDatePerRoomEdit").val(event.start_date); 
					$("#PriceEndDatePerRoomEdit").val(event.end_date);
					$('#PriceRoomTypePerRoomEdit').val(event.room_type);
					$('#PriceRoomDescriptionPerRoomEdit').val(event.description);
					$('#PriceMinDayPerRoomEdit').val(event.min_days);
					$('#PriceAmountPerRoomEdit').val(event.amount);
					$('#PriceNoOfRoomPerRoomEdit').val(event.no_of_rooms);
					$('#PriceAdultPerRoomEdit').val(event.adults);
					$('#PriceChildsPerRoomEdit').val(event.childs);
					$('#EditPriceIDPerRoom').val(event.id);  
					$('#EditRoomIDPerRoom').val(event.room_id); 
					$('#EditRoomPriceIDPerRoom').val(event.room_price_id);  
					$('#deletePriceRoom').attr('price_id',event.id);
					$('#deletePriceRoom').attr('room_id',event.room_id);
					$('#deletePriceRoom').attr('room_price_id',event.room_price_id);
					$('#editPriceModalPerRoom').modal('show');  	 
				}
				
			}
		}
	);
} 


function getAccomIDFromURL() {
	var href=window.location.href;
	var chunks=href.split('/');
	if(chunks) {
		accom=chunks[5].split('?');
		return accom[0];
	}
}
function showCalendarPrices() {
	$.ajax({
		url:'/accomodations/get_calendar_prices?accom='+getAccomIDFromURL(),
		success:function(r) {
			events=$.parseJSON(r); 
			if(events) { 
				displayCal(events);
			}
		}
	}); 
} 

function editExtra(id) {
    if(id) {
    	$.ajax({
    		url:'/accomodations/edit_extra_charge/'+id,
    		dataType:'json',
    		success:function(x) {
    			console.log(x);
    			if(x) {
    				$('#editExtraChargeID').val(x.id);
    				$('#ExtraPriceAmountEdit').val(x.amount);
    				$('#ExtraDescriptionEdit').val(x.description);
    				$('#ExtraIsOptional option[value='+x.is_optional+']').attr('selected','selected');
    				if(x.is_per_person) {
    					$('#PerPersonCheckBox').attr("checked","checked");
    				}
    				$('input:radio[name=charge_type_edit][value='+x.charge_type+']').attr('checked', true);
    			}
    			$('#editExtraCharge').modal('show');
    		}
    	});
    }
}


function editPriceTable(id,unit) {
	$.ajax({
		url:"/accomodations/get-accomodation-prices-edit?id="+id+"&unit="+unit, 
		dataType:'json',
		success:function(r) {
			// console.log(r);
			if(unit=='perweek') {
				showPerWeekEditModalTable(r);
			} else if (unit=='perroom') {
				showPerRoomEditModalTable(r);
			} else if(unit=='perperson') {
				showPerPersonEditModalTable(r);
			}
		}
	});
	
		
}
function showDay() {
	var changeday=$("input[name='changeday']:checked"). val();
	var showday=0;
	if(changeday=='saturday') {
		showday=6;
	} else if ('changeday'=='sunday') { 
		showday=0;
	} else {
		showday=10;
	}
	return showday
}

function showPerWeekEditModalTable(arg) {
	setDateAndChangeDay('PriceStartDatePerWeekEditTable','PriceEndDatePerWeekEditTable',showDay()); 
	$("#EditPriceIDWeekTable").val(arg.id);  
	$("#PriceStartDatePerWeekEditTable").val(arg.start_date); 
	$("#PriceEndDatePerWeekEditTable").val(arg.end_date);
	$('#PriceMinDayTable').val(arg.min_days);
	$('#PriceAmountEditTable').val(arg.amount);
	$('#PriceMaxPersonEditTable').val(arg.max_persons);
	$('#PriceExtraPricePerPersonEditTable').val(arg.extra_price_per_person);
	$('#editPriceModalTable').modal('show');   
}

function showPerRoomEditModalTable (arg) {
	console.log(arg);
	setDateAndChangeDay('PriceStartDatePerRoomEditTable','PriceEndDatePerRoomEditTable',showDay());
	$("#PriceStartDatePerRoomEditTable").val(arg.room_price[0].start_date); 
	$("#PriceEndDatePerRoomEditTable").val(arg.room_price[0].end_date);
	$('#PriceRoomTypePerRoomEditTable').val(arg.room_type);
	$('#PriceRoomDescriptionPerRoomEditTable').val(arg.description);
	$('#PriceMinDayPerRoomEditTable').val(arg.min_days);
	$('#PriceAmountPerRoomEditTable').val(arg.room_price[0].price);
	$('#PriceNoOfRoomPerRoomEditTable').val(arg.no_of_rooms);
	$('#PriceAdultPerRoomEditTable').val(arg.adults);
	$('#PriceChildsPerRoomEditTable').val(arg.childs);
	$('#EditPriceIDPerRoomTable').val(arg.id);  
	$('#EditRoomIDPerRoomTable').val(arg.id); 
	$('#EditRoomPriceIDPerRoomTable').val(arg.room_price[0].id); 
	$('#PriceRoomIdTable').val(arg.id); 
	$('#RoomPriceIdTable').val(arg.room_price[0].id);  
	$('#PriceIdTable').val(arg.accom_price_id);


		

	$('#editPriceModalPerRoomTable').modal('show');    
}
function showPerPersonEditModalTable(arg) {
	console.log(arg);
	$('#PriceRoomTypeEditTable').val(arg.room_type);
	$('#PriceRoomDescriptionEditTable').val(arg.description);
	$('#PriceMinDayEditTable').val(arg.min_days);
	$('#PriceNoOfRoomEditTable').val(arg.no_of_rooms);
	$('#PriceAdultEditTable').val(arg.adults);
	$('#PriceChildsEditTable').val(arg.childs);
	
	$('#PriceExtraPriceAdultEditTable').val(arg.room_price[0].extra_price_adult);
	$('#PriceExtraPriceChildEditTable').val(arg.room_price[0].extra_price_child);
	$('#PriceMaxAdultsPerRoomEditTable').val(arg.max_adults_per_room);
	$('#PriceMaxPersonPerRoomEditTable').val(arg.max_persons_per_room);
	$('#appendPrices').html('');
	$.each(arg.room_price,function(i,v) {
		div='<div class="form-group col-md-3">';
		div+='<input name="start_date" class="form-control valid" value="'+v.start_date+'" type="text" id="PriceModalStartDateEditTable'+i+'" required="true">';
		div+='</div>';
		div+='<div class="form-group col-md-3">';
		div+='<input name="end_date" class="form-control valid" value="'+v.end_date+'" type="text" id="PriceModalEndDateEditTable'+i+'" required="true">';
		div+='</div>';
		div+='<div class="form-group col-md-2">';
		div+='<input name="price1" class="form-control valid" type="number" min="1" value="'+v.price1+'" id="PriceModalPrice1'+i+'" required="true">';
		div+='</div>';
		div+='<div class="form-group col-md-2">';
		div+='<input name="price2" class="form-control valid" type="number" min="1" value="'+v.price2+'" id="PriceModalPrice2'+i+'" required="true">';
		div+='</div>';
		div+='<div class="form-group col-md-2">';
		div+='<input name="price3" class="form-control valid" type="number" min="0" value="'+v.price3+'" id="PriceModalPrice3'+i+'">';
		div+='</div>';
		$('#appendPrices').append(div);
		setDateEdit("PriceModalStartDateEditTable"+i,"PriceModalEndDateEditTable"+i,showDay()); 
	});
	 
	$('#PriceRoomIdEditTable').val(arg.id); 
	$('#PriceRoomPriceIdEditTable').val(arg.room_price[0].id); 
	$('#PriceIdEditTable').val(arg.accom_price_id);  
	$('#editPriceModalPerPersonTable').modal('show');    
}

function setDateEdit(start,end,showDay) {
	$("#"+start).datepicker('destroy');
	$("#"+end).datepicker('destroy');
	if(showDay==0 || showDay==6) {
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	var endDate=new Date(selectedDate.getTime() );
		    	// $('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	    $('#'+start).datepicker("getDate"));      
		    }, 
		    beforeShowDay: function(date) {
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    	// return [day==6,''];
		    }, 
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    beforeShowDay: function(date) {
		    	var day=date.getDay();
		    	return [day==showDay,''];
		    } 
		});
	} else { 
		$("#"+start).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0,
		    onSelect:function(date) {
		    	var selectedDate=new Date(date);
		    	var endDate=new Date(selectedDate.getTime() );
		    	// $('#'+end).val(CLICKEDDATE.format('DD-MM-YYYY'));
		    	jQuery('#'+end).datepicker("option","minDate",
		    	     $('#'+start).datepicker("getDate"));     
		    }
		});
		$("#"+end).datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    changeMonth:true,
		    changeYear:true,
		    minDate:0
		});
	}	 
}

