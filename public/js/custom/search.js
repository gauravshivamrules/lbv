$(document).ready(function()
{
	$(document).on('click', '.pagination a',function(event)
	{
		$('li').removeClass('active');
		$(this).parent('li').addClass('active');
		event.preventDefault();
		var myurl = $(this).attr('href');
		page=$(this).attr('href').split('page=')[1];
		submitSearchForm();
	});
	$("#search_start_date").datepicker({ 
	    dateFormat: 'dd-mm-yy',
	    minDate:0,
	    onSelect:function(date) {
	      var selectedDate=new Date(date);
	      var endDate=new Date(selectedDate.getTime());
	      $("#search_end_date").datepicker("option","minDate",$("#search_start_date").datepicker("getDate"));   
	    }
	});
	$("#search_end_date").datepicker({
	   minDate:0,
	   dateFormat:'dd-mm-yy'
	});
	$(".flip").click(function(){
		$(this).parent().next("div").slideToggle(); 
	});
});
//getCords();
submitSearchForm();

$( function() {
   $( "#slider-range" ).slider({
     range: true,
     min: 1,
     max: 5000,
     values: [ 1, 200 ],
     slide: function( event, ui ) {
       $( "#price" ).val(ui.values[0]+"-"+ui.values[1]);
       $( "#amount" ).html( "€" + ui.values[0] +" - €" + ui.values[1] );
       setTimeout(function(){ submitSearchForm(); },1000);
     }
   });
   $( "#amount" ).html( "€" + $( "#slider-range" ).slider( "values", 0 ) +
     " - €" + $( "#slider-range" ).slider( "values", 1 ) );
 });


$("#countryID").chosen({placeholder_text_single: "Country",no_results_text: "Oops, nothing found!"});
$("#typeID").chosen({
	placeholder_text_multiple: "Type",
	no_results_text: "Oops, nothing found!"
}).change(function(){
	$('#typeId').val($(this).val());
});
$("#region").chosen({placeholder_text_single: "Region",no_results_text: "Oops, nothing found!"});
$("#department").chosen({placeholder_text_single: "Department",no_results_text: "Oops, nothing found!"});
// $("#price").chosen({placeholder_text_single: "Price",no_results_text: "Oops, nothing found!"});
$("#label").chosen({placeholder_text_single: "Price",no_results_text: "Oops, nothing found!"});
$("#noOfPerson").chosen({placeholder_text_single: "No Of Person",no_results_text: "Oops, nothing found!"});
$('#countryID').change(function(){
	getRegions($(this).val());
	$('#department').empty();
	$('#department').append("<option value=''>Department</option>");
}); 
$('#region').change(function(){getDepartments($(this).val());});


function submitSearchForm(){
	var amnityArr=[];
	$("input:checkbox[id=amnity]:checked").each(function(){amnityArr.push($(this).val());});
	$('#amnityArr').val(amnityArr);
	if(typeof page === "undefined"){
		page=1;
	}
	submitSearchFormMap();
	$('#searchResults').html('');
	$('#loadingAjax').show();
	$.ajax({
		type:'POST',
		data:$('#searchForm').serialize(),
		url: '/search/ajax-search?page='+page,
		success:function(r){
			$('#searchResults').html(r);
			$('#loadingAjax').hide();
		},
		error:function(e){
			$('#loadingAjax').hide();	
		}
	});
}
function getRegions(country){
    if(country) {
    	$('#region').empty();
    	$('#region').append("<option value=''>Region</option>");
      $.ajax({
        url:'/accomodations/get_regions?country_id='+country,
        dataType:'json',
        success:function(r) {
          if(r) {
            $.each(r, function(key, value) {
                $('#region').append($("<option/>", {
                    value: value.id, 
                    text: value.region_name
                }));
            });
            $("#region").trigger("chosen:updated");
            $("#department").trigger("chosen:updated"); 
          }
        }
      }); 
    }
}
function getDepartments(region){
    if(region) {
    	$('#department').empty();
    	$('#department').append("<option value=''>Department</option>");
      $.ajax({
        url:'/accomodations/get_departments?region_id='+region,
        dataType:'json',
        success:function(r) { 
          if(r) {
            $.each(r, function(key, value) {
                $('#department').append($("<option/>", {
                    value: value.id,
                    text: value.department_name
                }));
            });
            $("#department").trigger("chosen:updated"); 
          }
        }
      }); 
    }
}
function submitSearchFormMap() {
	$('#newMap').remove();
	var newMap="<div id='newMap' style='width: 100%;height: 100%'> </div>";   
	$.ajax({
		type:'POST',
		data:$('#searchForm').serialize(),
		url:'/search/ajax-search-map',  
		// url:'/search/get-cords',
		dataType:'json',
		success:function(r){ 
			/*console.log(r);
			$.each(r,function(i,v){
					console.log(v); 
			}); */
			pointsS=r;
			var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 10,
					attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				}),
			latlng = new L.LatLng(31.1048, 77.1734);
			var markers = new L.markerClusterGroup();
			$('#map').append(newMap);
			var map = new L.Map('newMap', {center: latlng, zoom: 2, layers: [tiles]}); 
			if(r.features){
				/*geojsonLayer = L.geoJson(r);
		    	map.fitBounds(geojsonLayer.getBounds());*/
			   	var points = new L.geoJson(r,{ 
			        onEachFeature: popUp
			    });
				markers.on('clusterclick', function (a) {
				    var childrenMarkerCluster = a.layer.getAllChildMarkers();
				    // console.log(childrenMarkerCluster);
				    var accoms=getClusterAccomodationIds(childrenMarkerCluster);
				    getClusterAccomodations(accoms);
				});
			    markers.addLayer(points);
			    map.addLayer(markers);
		   }

		    function popUp(feature, layer){
			   	var text='';
			   	if(feature.properties['propertyImage']){
			   		text='<a href="/accomodations/view/'+feature.properties['propertySlug']+'" ><img src="/images/gallery/'+feature.properties['propertyId']+'/'+feature.properties['propertyImage']+'" class="img-responsive"></a>';
			   	}else{
			   		text="No Image Avaliable";
			   	}
			       text+='<a href="/accomodations/view/'+feature.properties['propertySlug']+'" ><h3>' + feature.properties['propertyName'] + '</h3></a><br>'; 
			       text+="<h5>"+feature.properties['regionName']+','+feature.properties['countryName']+"</h5>";
			       layer.bindPopup(text); 
			} 
		}
	});	
}
function getClusterAccomodationIds(cluster) {
	if(cluster){
		var clusterAccoms=[];
		$.each(cluster,function (ind,marker) {
	        clusterAccoms.push(marker.feature.properties.propertyId);
		});
		return clusterAccoms;
	}
}
function getClusterAccomodations(accoms) {
	var amnityArr=[];
	$("input:checkbox[id=amnity]:checked").each(function(){amnityArr.push($(this).val());});
	$('#amnityArr').val(amnityArr);
	if(typeof page === "undefined"){
		page=1;
	}
	$('#searchResults').html('');
	$('#loadingAjax').show();
	var data=$('#searchForm').serializeArray();
	data.push({name: 'clusterAccoms', value: accoms});
	$.ajax({
		type:'POST',
		data:data,
		url: '/search/ajax-search?page='+page,
		success:function(r){
			$('#searchResults').html(r);
			$('#loadingAjax').hide();
		},
		error:function(e){
			$('#loadingAjax').hide();	
		}
	});
}