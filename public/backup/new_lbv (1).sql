-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 10, 2018 at 09:07 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_lbv`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE `accomodations` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `completed` int(11) DEFAULT '0',
  `label_id` int(11) NOT NULL COMMENT '2=Sale,3=Rent',
  `accomodation_type_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL COMMENT '1=Paid,2=Free',
  `subs_valid_license` int(11) NOT NULL DEFAULT '0' COMMENT '1=>License valid 0=>License Not Valid',
  `address_id` int(10) UNSIGNED DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `number_of_persons` int(10) UNSIGNED DEFAULT NULL,
  `search_tags` int(11) DEFAULT NULL,
  `deletion_request` tinyint(1) NOT NULL DEFAULT '0',
  `tip_accom` int(11) NOT NULL DEFAULT '0',
  `featured_image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spotlight` int(11) NOT NULL DEFAULT '0',
  `editable` int(11) NOT NULL DEFAULT '1',
  `add_finished` int(11) NOT NULL DEFAULT '0',
  `new_accom` int(11) NOT NULL DEFAULT '1',
  `is_slider` int(11) NOT NULL DEFAULT '0',
  `reservation_active` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `user_id`, `name`, `created_at`, `updated_at`, `approved`, `completed`, `label_id`, `accomodation_type_id`, `payment_type`, `subs_valid_license`, `address_id`, `website`, `email`, `description`, `number_of_persons`, `search_tags`, `deletion_request`, `tip_accom`, `featured_image`, `spotlight`, `editable`, `add_finished`, `new_accom`, `is_slider`, `reservation_active`, `slug`) VALUES
(135, 2, 'For sale', '2018-04-08 21:34:45', '2018-04-08 21:40:09', 1, 1, 2, 4, 1, 1, NULL, NULL, 'tesr@test.com', 'Fff', 5, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '135-for-sale'),
(134, 2, 'Coupon Test', '2018-04-08 19:10:42', '2018-04-10 20:44:35', 1, 1, 3, 2, 1, 0, NULL, NULL, 'test@testing.com', 'dfdfdf', 3, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '134-coupon-test'),
(133, 2, 'Test Accom', '2018-04-08 19:04:48', '2018-04-08 19:08:14', 1, 1, 3, 4, 1, 1, NULL, NULL, 'test@testing.com', '121212', 10, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '133-test-accom'),
(132, 2, 'Test SALE', '2018-04-08 12:12:05', '2018-04-08 18:37:33', 1, 1, 2, 1, 1, 0, NULL, NULL, 'test@testing.com', 'dfdfdf', 3, NULL, 0, 0, '5aca0721292df.jpg', 0, 1, 0, 1, 0, 0, '132-test-sale'),
(129, 2, 'Free Property', '2018-04-08 08:12:28', '2018-04-08 08:31:53', 0, 1, 3, 10, 2, 0, NULL, 'https://testtesting.in', 'test@testing.com', 'description goes here..........', 10, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '129-free-property'),
(127, 24, 'Test no cure 7 april', '2018-04-07 08:26:52', '2018-04-08 06:59:05', 0, 1, 3, 13, 2, 0, NULL, 'https://www.lencouet.be', 'lencouet@gmail.com', 'Test no cure 7 april', 6, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '127-test-no-cure-7-april'),
(128, 24, 'test 8 april no cure no pay', '2018-04-08 06:48:15', '2018-04-08 08:29:53', 1, 1, 3, 1, 2, 0, NULL, NULL, 'lencouet@gmail.com', 'test 8 april no cure no pay', 6, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '128-test-8-april-no-cure-no-pay'),
(130, 24, 'test', '2018-04-08 08:21:36', '2018-04-08 08:27:03', 1, 1, 3, 2, 1, 0, NULL, NULL, 'lencouet@gmail.com', 'test', 6, NULL, 0, 0, NULL, 0, 1, 0, 1, 0, 0, '130-test'),
(131, 2, 'Paid Per Room Property', '2018-04-08 08:45:42', '2018-04-08 21:09:36', 1, 1, 3, 10, 1, 1, NULL, NULL, 'test@testing.com', 'desc....', 2, NULL, 0, 0, '5ac9d6eb9a738.jpg', 0, 1, 0, 1, 0, 0, '131-paid-per-room-property'),
(92, 24, 'test 19 maart B&B', '2018-03-19 10:23:25', '2018-04-04 17:16:23', 1, 1, 3, 2, 1, 0, NULL, 'https://www.lencouet.be', 'lencouet@gmail.com', 'test B&B 19 maart', 6, NULL, 0, 0, '5ab0b9d836a7b.jpg', 0, 1, 0, 1, 0, 0, '92-test-19-maart-bb');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_addresses`
--

CREATE TABLE `accomodation_addresses` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `accom_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lattitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accomodation_addresses`
--

INSERT INTO `accomodation_addresses` (`id`, `accom_id`, `created_at`, `updated_at`, `country_id`, `region_id`, `department_id`, `street`, `street_number`, `city`, `postal_code`, `lattitude`, `longitude`) VALUES
(40, 131, '2018-04-08 08:49:19', '2018-04-08 08:49:19', 38, 133, 166, 'kasumpti', NULL, 'shimla', '171001', '31.1011641', '77.17439830000001'),
(5, 92, '2018-03-19 10:25:38', '2018-04-04 17:14:37', 3, 6, 7, 'lencouet', NULL, 'Feugarolles', '47230', '44.1964094447964', '0.3582492910156816'),
(42, 133, '2018-04-08 19:05:09', '2018-04-08 19:05:09', 38, 133, 166, 'kasumpti', NULL, 'shimla', '171009', '31.0782327', '77.18446189999997'),
(39, 130, '2018-04-08 08:26:41', '2018-04-08 08:26:41', 3, 6, 7, 'lencouet', NULL, 'feugarolles', '47230', '44.195671', '0.3568760000000566'),
(31, 123, '2018-04-05 04:32:25', '2018-04-05 04:32:25', 38, 133, NULL, 'Shimla, Himachal Pradesh, India', NULL, '-Shimla', '171001', '31.1011641', '77.17439830000001'),
(32, 123, '2018-04-05 17:24:26', '2018-04-05 17:24:26', 3, 6, 7, 'fragullus', NULL, 'fragullus', '123', '45.14694859999999', '0.7572205000000167'),
(29, 120, '2018-04-05 04:30:08', '2018-04-05 04:30:08', 38, 133, NULL, 'shimla', NULL, 'shimla', '171001', '31.1011641', '77.17439830000001'),
(41, 132, '2018-04-08 12:12:37', '2018-04-08 12:12:37', 38, 133, 166, 'kasumpti', NULL, 'shimla', '171009', '31.0782327', '77.18446189999997'),
(38, 129, '2018-04-08 08:21:10', '2018-04-08 08:21:10', 38, 133, 166, 'kasumpti', NULL, 'shimla', '171001', '31.1011641', '77.17439830000001'),
(36, 128, '2018-04-08 06:51:55', '2018-04-08 06:51:55', 3, 6, 7, 'Lencouet', NULL, 'Feugarolles', '47230', '44.195671', '0.3568760000000566'),
(37, 127, '2018-04-08 06:58:55', '2018-04-08 06:58:55', 3, 6, 7, 'Lencouet', NULL, 'Feugarolles', '47230', '44.195671', '0.3568760000000566'),
(43, 134, '2018-04-08 19:11:07', '2018-04-08 19:11:07', 3, 6, 7, 'street', NULL, 'france', '1234', '44.0553248', '0.25891939999996794'),
(44, 135, '2018-04-08 21:36:16', '2018-04-08 21:36:16', 38, 133, 166, 'Shimla', NULL, 'Shimla', '171009', '31.1048145', '77.17340330000002');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_prices`
--

CREATE TABLE `accomodation_prices` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `accom_id` mediumint(9) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `price_unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'perweek,perperson,perroom',
  `amount` double(10,2) DEFAULT NULL,
  `max_persons` int(10) UNSIGNED DEFAULT NULL,
  `min_days` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `changeday` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_price_per_person` double(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accomodation_prices`
--

INSERT INTO `accomodation_prices` (`id`, `accom_id`, `created_at`, `updated_at`, `price_unit`, `amount`, `max_persons`, `min_days`, `start_date`, `end_date`, `changeday`, `extra_price_per_person`) VALUES
(8, 49, NULL, NULL, 'perperson', NULL, NULL, 1, NULL, NULL, 'anyday', NULL),
(54, 92, '2018-03-19 10:24:10', '2018-04-06 17:37:46', 'perroom', NULL, NULL, 1, NULL, NULL, 'anyday', NULL),
(61, 92, '2018-03-20 07:35:22', '2018-04-06 18:21:09', 'perroom', NULL, NULL, 1, NULL, NULL, 'anyday', NULL),
(82, 92, '2018-04-04 17:11:15', '2018-04-06 17:37:53', 'perroom', NULL, NULL, 1, '2019-01-01 00:00:00', '2019-01-31 23:59:59', 'anyday', NULL),
(95, 127, '2018-04-07 08:27:40', '2018-04-07 08:27:40', 'perweek', 395.00, 6, 7, '2018-04-07 00:00:00', '2018-06-30 23:59:59', 'saturday', 0.00),
(96, 128, '2018-04-08 06:48:46', '2018-04-08 06:48:46', 'perweek', 395.00, 6, 7, '2018-04-14 00:00:00', '2018-06-30 23:59:59', 'saturday', 0.00),
(97, 128, '2018-04-08 06:49:44', '2018-04-08 06:49:44', 'perweek', 995.00, 6, 7, '2018-07-01 00:00:00', '2018-08-31 23:59:59', 'saturday', 0.00),
(98, 129, '2018-04-08 08:13:15', '2018-04-08 08:13:40', 'perroom', NULL, NULL, 1, '2018-04-08 00:00:00', '2018-04-25 23:59:59', 'anyday', NULL),
(99, 129, '2018-04-08 08:15:21', '2018-04-08 08:15:21', 'perroom', 20.00, NULL, 1, '2018-04-08 00:00:00', '2018-04-25 23:59:59', 'anyday', NULL),
(100, 130, '2018-04-08 08:22:43', '2018-04-08 08:23:45', 'perroom', NULL, NULL, 1, '2018-04-08 00:00:00', '2018-04-11 23:59:59', 'anyday', NULL),
(101, 130, '2018-04-08 08:25:17', '2018-04-08 08:25:17', 'perroom', 70.00, NULL, 1, '2018-04-08 00:00:00', '2018-04-14 23:59:59', 'anyday', NULL),
(102, 131, '2018-04-08 08:46:09', '2018-04-08 08:46:09', 'perroom', 10.00, NULL, 1, '2018-04-10 00:00:00', '2018-04-20 23:59:59', 'anyday', NULL),
(103, 132, '2018-04-08 12:12:09', '2018-04-08 14:55:29', NULL, 50001.00, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 132, '2018-04-08 14:54:06', '2018-04-08 14:54:06', NULL, 50001.00, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 132, '2018-04-08 14:54:29', '2018-04-08 14:54:29', NULL, 50001.00, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 135, '2018-04-08 21:34:50', '2018-04-10 17:53:36', NULL, 5001.00, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_price_extra_charges`
--

CREATE TABLE `accomodation_price_extra_charges` (
  `id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_optional` int(11) NOT NULL DEFAULT '0' COMMENT '0=Required,1=Optional',
  `charge_type` varchar(100) DEFAULT NULL,
  `is_per_person` int(11) DEFAULT '0' COMMENT '0=No,1=Yes	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation_price_extra_charges`
--

INSERT INTO `accomodation_price_extra_charges` (`id`, `accom_id`, `amount`, `description`, `created_at`, `updated_at`, `is_optional`, `charge_type`, `is_per_person`) VALUES
(5, 41, 5.00, 'Req', '2018-02-28 19:04:00', '2018-02-28 19:04:00', 1, 'perday', 1),
(6, 44, 3.00, 'Cycling', '2018-03-01 20:55:48', '2018-03-01 20:55:48', 1, 'perday', 1),
(7, 48, 10.00, 'BreakFast', '2018-03-02 07:26:45', '2018-03-02 07:26:45', 1, 'perday', 1),
(8, 48, 20.00, 'Cleaning', '2018-03-02 07:26:57', '2018-03-02 07:26:57', 0, 'fix', 0),
(20, 92, 50.00, 'cleaning', '2018-03-19 10:24:25', '2018-03-19 10:24:25', 0, 'fix', 0),
(28, 128, 50.00, 'Schoonmaak', '2018-04-08 06:50:02', '2018-04-08 06:50:02', 0, 'fix', 0),
(29, 130, 60.00, 'sfghj', '2018-04-08 08:25:48', '2018-04-08 08:25:48', 0, 'fix', 0);

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_searchtags`
--

CREATE TABLE `accomodation_searchtags` (
  `id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `searchtag_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation_searchtags`
--

INSERT INTO `accomodation_searchtags` (`id`, `accom_id`, `searchtag_id`, `created_at`, `updated_at`) VALUES
(426, 133, 53, '2018-04-10 17:29:52', '2018-04-10 17:29:52'),
(405, 132, 53, '2018-04-08 15:07:51', '2018-04-08 15:07:51'),
(425, 133, 16, '2018-04-10 17:29:52', '2018-04-10 17:29:52'),
(429, 135, 53, '2018-04-10 17:53:58', '2018-04-10 17:53:58'),
(416, 134, 16, '2018-04-08 19:12:08', '2018-04-08 19:12:08'),
(415, 134, 2, '2018-04-08 19:12:08', '2018-04-08 19:12:08'),
(423, 131, 53, '2018-04-10 16:42:59', '2018-04-10 16:42:59'),
(422, 131, 49, '2018-04-10 16:42:59', '2018-04-10 16:42:59'),
(421, 131, 32, '2018-04-10 16:42:59', '2018-04-10 16:42:59'),
(420, 131, 25, '2018-04-10 16:42:59', '2018-04-10 16:42:59'),
(424, 133, 2, '2018-04-10 17:29:52', '2018-04-10 17:29:52'),
(377, 130, 12, '2018-04-08 08:27:03', '2018-04-08 08:27:03'),
(367, 127, 32, '2018-04-08 06:59:05', '2018-04-08 06:59:05'),
(375, 128, 10, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(374, 128, 24, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(373, 128, 5, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(372, 128, 22, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(371, 128, 3, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(370, 128, 25, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(369, 128, 4, '2018-04-08 07:07:09', '2018-04-08 07:07:09'),
(428, 135, 4, '2018-04-10 17:53:58', '2018-04-10 17:53:58'),
(427, 135, 2, '2018-04-10 17:53:58', '2018-04-10 17:53:58'),
(368, 127, 22, '2018-04-08 06:59:05', '2018-04-08 06:59:05'),
(376, 130, 22, '2018-04-08 08:27:03', '2018-04-08 08:27:03'),
(404, 132, 26, '2018-04-08 15:07:51', '2018-04-08 15:07:51'),
(403, 132, 25, '2018-04-08 15:07:51', '2018-04-08 15:07:51'),
(402, 132, 4, '2018-04-08 15:07:51', '2018-04-08 15:07:51'),
(326, 92, 10, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(325, 92, 1, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(324, 92, 3, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(323, 92, 26, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(322, 92, 25, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(321, 92, 6, '2018-04-04 17:14:41', '2018-04-04 17:14:41'),
(320, 92, 4, '2018-04-04 17:14:41', '2018-04-04 17:14:41');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_types`
--

CREATE TABLE `accomodation_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_group` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accomodation_types`
--

INSERT INTO `accomodation_types` (`id`, `type_name`, `type_group`, `created_at`, `updated_at`) VALUES
(1, 'Gite', 2, NULL, NULL),
(2, 'B&B', 1, NULL, NULL),
(4, 'Camping', 1, NULL, NULL),
(5, 'Studio', 2, NULL, NULL),
(6, 'Vakantiehuis', 2, NULL, NULL),
(7, 'Agriturismo', 1, NULL, NULL),
(8, 'Appartement', 2, NULL, NULL),
(9, 'Bungalow', 2, NULL, NULL),
(10, 'Hotel', 1, NULL, NULL),
(12, 'Villa', 2, NULL, NULL),
(13, 'Chambres d\'Hotes', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE `admin_notifications` (
  `id` int(11) NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `from_user` int(11) NOT NULL,
  `accom_id` int(11) DEFAULT NULL,
  `seen` bit(1) NOT NULL DEFAULT b'0',
  `ticket_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `request_type_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 for approve,0 for delete,3 for contact message,4 for contact message reply,5 for New Booking Request,6 for Booking Approval Request,7 for Booking Payment Request,8 For Booking Request Rejected,9= New Price Quoted,10 New Award Request,11= Award Delete Request,12=New Property Edit Request,13=image update,14=overschrijving payment request,15=new extra tab request,16=New Product Bought,17=Property Switch Request,18=New Location Request',
  `completed` int(11) NOT NULL DEFAULT '0',
  `booking_payment_id` int(11) DEFAULT NULL,
  `edit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_notifications`
--

INSERT INTO `admin_notifications` (`id`, `message`, `from_user`, `accom_id`, `seen`, `ticket_id`, `created_at`, `updated_at`, `request_type_url`, `completed`, `booking_payment_id`, `edit_id`) VALUES
(1, 'A new subscription bank transfer request has been received for property Test SALE', 2, 132, b'1', NULL, '2018-04-08 15:07:59', '2018-04-08 15:13:07', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(2, 'A new subscription bank transfer request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-08 17:43:46', '2018-04-08 17:45:18', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(3, 'A new paid RENT accomodation has been created and marked completed', 2, 133, b'1', NULL, '2018-04-08 19:05:28', '2018-04-08 19:07:36', '/accomodations/view-accom/133-test-accom', 0, NULL, NULL),
(4, 'A new subscription bank transfer request has been received for property Test Accom', 2, 133, b'1', NULL, '2018-04-08 19:06:08', '2018-04-08 19:07:44', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(5, 'A new paid RENT accomodation has been created and marked completed', 2, 134, b'1', NULL, '2018-04-08 19:11:13', '2018-04-08 19:13:16', '/accomodations/view-accom/134-gggg', 0, NULL, NULL),
(6, 'A new subscription bank transfer request has been received for property GGGG', 2, 134, b'1', NULL, '2018-04-08 19:12:13', '2018-04-08 19:13:25', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(7, 'A renew subscription bank transfer request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-08 20:59:22', '2018-04-08 21:01:21', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(8, 'A renew subscription bank transfer request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-08 21:18:04', '2018-04-08 21:18:45', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(9, 'A new paid SALE accomodation has been created and marked completed', 2, 135, b'1', NULL, '2018-04-08 21:36:28', '2018-04-08 21:38:51', '/accomodations/view-accom/135-for-sale', 0, NULL, NULL),
(10, 'A new subscription bank transfer request has been received for property For sale', 2, 135, b'1', NULL, '2018-04-08 21:37:15', '2018-04-08 21:39:45', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(11, 'A renew subscription bank transfer request has been received for property For sale', 2, 135, b'1', NULL, '2018-04-08 21:42:22', '2018-04-08 21:43:25', '/subscription_licenses/licenses#showBankTransfer', 0, NULL, NULL),
(12, 'A new subscription paypal request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-10 15:15:01', '2018-04-10 17:43:28', '/subscription_licenses/licenses', 0, NULL, NULL),
(13, 'A new subscription paypal request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-10 15:15:33', '2018-04-10 17:43:24', '/subscription_licenses/licenses', 0, NULL, NULL),
(14, 'A new subscription paypal request has been received for property For sale', 2, 135, b'1', NULL, '2018-04-10 15:19:16', '2018-04-10 17:43:21', '/subscription_licenses/licenses', 0, NULL, NULL),
(15, 'A new subscription paypal request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-10 16:43:48', '2018-04-10 17:43:17', '/subscription_licenses/licenses', 0, NULL, NULL),
(16, 'A renew subscription paypal request has been received for property Paid Per Room Property', 2, 131, b'1', NULL, '2018-04-10 17:26:47', '2018-04-10 17:43:13', '/subscription_licenses/licenses', 0, NULL, NULL),
(17, 'A new subscription paypal request has been received for property Test Accom', 2, 133, b'1', NULL, '2018-04-10 17:30:27', '2018-04-10 17:42:03', '/subscription_licenses/licenses', 0, NULL, NULL),
(18, 'A new subscription paypal payment has been received for property For sale', 2, 135, b'1', NULL, '2018-04-10 17:54:54', '2018-04-10 18:00:30', '/subscription_licenses/licenses', 0, NULL, NULL),
(19, 'A renew subscription paypal payment has been received for property For sale', 2, 135, b'1', NULL, '2018-04-10 17:58:26', '2018-04-10 18:00:34', '/subscription_licenses/licenses', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_approved` int(11) NOT NULL COMMENT '0=No,1=Yes',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `is_approved`, `created`) VALUES
(1, 'België ', 1, '0000-00-00 00:00:00'),
(2, 'Nederland', 1, '0000-00-00 00:00:00'),
(3, 'Frankrijk', 1, '0000-00-00 00:00:00'),
(4, 'Portugal', 1, '0000-00-00 00:00:00'),
(5, 'Spanje', 1, '0000-00-00 00:00:00'),
(6, 'Oostenrijk', 1, '0000-00-00 00:00:00'),
(7, 'Italië', 1, '0000-00-00 00:00:00'),
(8, 'Curaçao', 1, '0000-00-00 00:00:00'),
(9, 'Zweden', 1, '0000-00-00 00:00:00'),
(10, 'Luxemburg', 1, '0000-00-00 00:00:00'),
(11, 'Duitsland', 1, '0000-00-00 00:00:00'),
(12, 'Griekenland', 1, '0000-00-00 00:00:00'),
(13, 'Ierland', 1, '0000-00-00 00:00:00'),
(14, 'Brazilië ', 1, '0000-00-00 00:00:00'),
(15, 'Roemenië', 1, '0000-00-00 00:00:00'),
(16, 'Slovakije', 1, '0000-00-00 00:00:00'),
(17, 'Tsjechië', 1, '0000-00-00 00:00:00'),
(18, 'Slovenië', 1, '0000-00-00 00:00:00'),
(19, 'Dominicaanse Republiek', 1, '0000-00-00 00:00:00'),
(20, 'Kaapverdië', 1, '0000-00-00 00:00:00'),
(21, 'Kroatië', 1, '0000-00-00 00:00:00'),
(22, 'Marokko', 1, '0000-00-00 00:00:00'),
(23, 'Panama', 1, '0000-00-00 00:00:00'),
(24, 'Kenia', 1, '0000-00-00 00:00:00'),
(25, 'Polen', 1, '0000-00-00 00:00:00'),
(26, 'Senegal', 1, '0000-00-00 00:00:00'),
(27, 'Sri Lanka', 1, '0000-00-00 00:00:00'),
(28, 'Turkije', 1, '0000-00-00 00:00:00'),
(29, 'Zuid Afrika', 1, '0000-00-00 00:00:00'),
(30, 'Zwitserland', 1, '0000-00-00 00:00:00'),
(31, 'Indonesië', 1, '0000-00-00 00:00:00'),
(32, 'Gambia', 1, '0000-00-00 00:00:00'),
(34, 'Hongarije', 1, '0000-00-00 00:00:00'),
(35, 'Bulgarije', 1, '0000-00-00 00:00:00'),
(36, 'Thailand', 1, '0000-00-00 00:00:00'),
(37, 'USA', 1, '0000-00-00 00:00:00'),
(38, 'india', 1, '0000-00-00 00:00:00'),
(40, 'Canada', 1, '0000-00-00 00:00:00'),
(54, 'Malta', 1, '2018-01-02 16:58:22'),
(57, 'Zimbabwe', 1, '2018-01-03 19:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) NOT NULL,
  `is_approved` int(11) NOT NULL COMMENT '0=No,1=Yes',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `region_id`, `is_approved`, `created`) VALUES
(2, 'Bas-Rhin', 5, 1, '0000-00-00 00:00:00'),
(3, 'Haut-Rhin', 5, 1, '0000-00-00 00:00:00'),
(4, 'Dordogne', 6, 1, '0000-00-00 00:00:00'),
(5, 'Gironde', 6, 1, '0000-00-00 00:00:00'),
(6, 'Landes', 6, 1, '0000-00-00 00:00:00'),
(7, 'Lot-et-Garonne', 6, 1, '0000-00-00 00:00:00'),
(8, 'Pyrénées-Atlantiques', 6, 1, '0000-00-00 00:00:00'),
(9, 'Allier', 7, 1, '0000-00-00 00:00:00'),
(10, 'Cantal', 7, 1, '0000-00-00 00:00:00'),
(11, 'Haute-Loire', 7, 1, '0000-00-00 00:00:00'),
(12, 'Puy-de-Dôme', 7, 1, '0000-00-00 00:00:00'),
(13, 'Calvados', 8, 1, '0000-00-00 00:00:00'),
(14, 'Orne', 8, 1, '0000-00-00 00:00:00'),
(15, 'Côte d\'Or', 9, 1, '0000-00-00 00:00:00'),
(16, 'Nièvre', 9, 1, '0000-00-00 00:00:00'),
(17, 'Saône-et-Loire', 9, 1, '0000-00-00 00:00:00'),
(18, 'Yonne', 9, 1, '0000-00-00 00:00:00'),
(19, 'Côtes d\'Armor', 10, 1, '0000-00-00 00:00:00'),
(20, 'Finistère', 10, 1, '0000-00-00 00:00:00'),
(21, 'Ille-et-Vilaine', 10, 1, '0000-00-00 00:00:00'),
(22, 'Morbihan', 10, 1, '0000-00-00 00:00:00'),
(23, 'Cher', 11, 1, '0000-00-00 00:00:00'),
(24, 'Eure-et-Loir', 11, 1, '0000-00-00 00:00:00'),
(25, 'Indre', 11, 1, '0000-00-00 00:00:00'),
(26, 'Indre-et-Loire', 11, 1, '0000-00-00 00:00:00'),
(27, 'Loiret', 11, 1, '0000-00-00 00:00:00'),
(28, 'Loir-et-Cher', 11, 1, '0000-00-00 00:00:00'),
(29, 'Ardennes', 12, 1, '0000-00-00 00:00:00'),
(30, 'Aube', 12, 1, '0000-00-00 00:00:00'),
(31, 'Haute-Marne', 12, 1, '0000-00-00 00:00:00'),
(32, 'Marne', 12, 1, '0000-00-00 00:00:00'),
(33, 'Corse du Sud', 13, 1, '0000-00-00 00:00:00'),
(34, 'Haute-Corse', 13, 1, '0000-00-00 00:00:00'),
(35, 'Doubs', 14, 1, '0000-00-00 00:00:00'),
(36, 'Haute-Saône', 14, 1, '0000-00-00 00:00:00'),
(37, 'Jura', 14, 1, '0000-00-00 00:00:00'),
(38, 'Territoire-de-Belfort', 14, 1, '0000-00-00 00:00:00'),
(39, 'Eure', 15, 1, '0000-00-00 00:00:00'),
(40, 'Seine-Maritime', 15, 1, '0000-00-00 00:00:00'),
(41, 'Essonne', 16, 1, '0000-00-00 00:00:00'),
(42, 'Hauts-de-Seine', 16, 1, '0000-00-00 00:00:00'),
(43, 'Paris', 16, 1, '0000-00-00 00:00:00'),
(44, 'Seine-et-Marne', 16, 1, '0000-00-00 00:00:00'),
(45, 'Seine-St-Denis', 16, 1, '0000-00-00 00:00:00'),
(46, 'Val-de-Marne', 16, 1, '0000-00-00 00:00:00'),
(47, 'Val-d\'Oise', 16, 1, '0000-00-00 00:00:00'),
(48, 'Yvelines', 16, 1, '0000-00-00 00:00:00'),
(49, 'Aude', 17, 1, '0000-00-00 00:00:00'),
(50, 'Gard', 17, 1, '0000-00-00 00:00:00'),
(51, 'Hérault', 17, 1, '0000-00-00 00:00:00'),
(52, 'Lozère', 17, 1, '0000-00-00 00:00:00'),
(53, 'Pyrénées-Orientales', 17, 1, '0000-00-00 00:00:00'),
(54, 'Corrèze', 18, 1, '0000-00-00 00:00:00'),
(55, 'Creuse', 18, 1, '0000-00-00 00:00:00'),
(56, 'Haute-Vienne', 18, 1, '0000-00-00 00:00:00'),
(57, 'Meurthe-et-Moselle', 19, 1, '0000-00-00 00:00:00'),
(58, 'Meuse', 19, 1, '0000-00-00 00:00:00'),
(59, 'Moselle', 19, 1, '0000-00-00 00:00:00'),
(60, 'Vosges', 19, 1, '0000-00-00 00:00:00'),
(61, 'Ariège', 20, 1, '0000-00-00 00:00:00'),
(62, 'Aveyron', 20, 1, '0000-00-00 00:00:00'),
(63, 'Gers', 20, 1, '0000-00-00 00:00:00'),
(64, 'Haute-Garonne', 20, 1, '0000-00-00 00:00:00'),
(65, 'Hautes-Pyrénées', 20, 1, '0000-00-00 00:00:00'),
(66, 'Lot', 20, 1, '0000-00-00 00:00:00'),
(67, 'Tarn', 20, 1, '0000-00-00 00:00:00'),
(68, 'Tarn-et-Garonne', 20, 1, '0000-00-00 00:00:00'),
(69, 'Nord', 21, 1, '0000-00-00 00:00:00'),
(70, 'Pas-de-Calais', 21, 1, '0000-00-00 00:00:00'),
(71, 'Manche', 22, 1, '0000-00-00 00:00:00'),
(72, 'Loire-Atlantique', 23, 1, '0000-00-00 00:00:00'),
(73, 'Maine-et-Loire', 23, 1, '0000-00-00 00:00:00'),
(74, 'Mayenne', 23, 1, '0000-00-00 00:00:00'),
(75, 'Sarthe', 23, 1, '0000-00-00 00:00:00'),
(76, 'Vendée', 23, 1, '0000-00-00 00:00:00'),
(77, 'Aisne', 24, 1, '0000-00-00 00:00:00'),
(78, 'Oise', 24, 1, '0000-00-00 00:00:00'),
(79, 'Somme', 24, 1, '0000-00-00 00:00:00'),
(80, 'Charente', 25, 1, '0000-00-00 00:00:00'),
(81, 'Charente Maritime', 25, 1, '0000-00-00 00:00:00'),
(82, 'Deux-Sèvres', 25, 1, '0000-00-00 00:00:00'),
(83, 'Vienne', 25, 1, '0000-00-00 00:00:00'),
(84, 'Alpes de Haute-Provence', 26, 1, '0000-00-00 00:00:00'),
(85, 'Alpes-Maritimes', 26, 1, '0000-00-00 00:00:00'),
(86, 'Bouches du Rhône', 26, 1, '0000-00-00 00:00:00'),
(87, 'Hautes-Alpes', 26, 1, '0000-00-00 00:00:00'),
(88, 'Var', 26, 1, '0000-00-00 00:00:00'),
(89, 'Vaucluse', 26, 1, '0000-00-00 00:00:00'),
(90, 'Ain', 27, 1, '0000-00-00 00:00:00'),
(91, 'Ardèche', 27, 1, '0000-00-00 00:00:00'),
(92, 'Drôme', 27, 1, '0000-00-00 00:00:00'),
(93, 'Haute-Savoie', 27, 1, '0000-00-00 00:00:00'),
(94, 'Isère', 27, 1, '0000-00-00 00:00:00'),
(95, 'Loire', 27, 1, '0000-00-00 00:00:00'),
(96, 'Rhône', 27, 1, '0000-00-00 00:00:00'),
(97, 'Savoie', 27, 1, '0000-00-00 00:00:00'),
(98, 'Almería', 46, 1, '0000-00-00 00:00:00'),
(99, 'Cádiz', 46, 1, '0000-00-00 00:00:00'),
(100, 'Córdoba', 46, 1, '0000-00-00 00:00:00'),
(101, 'Granada', 46, 1, '0000-00-00 00:00:00'),
(102, 'Huelva', 46, 1, '0000-00-00 00:00:00'),
(103, 'Jaén', 46, 1, '0000-00-00 00:00:00'),
(104, 'Malaga', 46, 1, '0000-00-00 00:00:00'),
(105, 'Sevilla', 46, 1, '0000-00-00 00:00:00'),
(106, 'Huesca', 47, 1, '0000-00-00 00:00:00'),
(107, 'Teruel', 47, 1, '0000-00-00 00:00:00'),
(108, 'Zaragoza', 47, 1, '0000-00-00 00:00:00'),
(109, 'Asturië', 48, 1, '0000-00-00 00:00:00'),
(110, 'Majorca', 49, 1, '0000-00-00 00:00:00'),
(111, 'Minorca ', 49, 1, '0000-00-00 00:00:00'),
(112, 'Ibiza ', 49, 1, '0000-00-00 00:00:00'),
(113, 'Formentera', 49, 1, '0000-00-00 00:00:00'),
(114, 'Cabrera', 49, 1, '0000-00-00 00:00:00'),
(115, 'Álava', 50, 1, '0000-00-00 00:00:00'),
(116, 'Biskaje', 50, 1, '0000-00-00 00:00:00'),
(117, 'Gipuzkoa', 50, 1, '0000-00-00 00:00:00'),
(118, 'El Hierro / Ferro', 51, 1, '0000-00-00 00:00:00'),
(119, 'Fuerteventura', 51, 1, '0000-00-00 00:00:00'),
(120, 'Gran Canaria', 51, 1, '0000-00-00 00:00:00'),
(121, 'La Gomera', 51, 1, '0000-00-00 00:00:00'),
(122, 'Lanzarote', 51, 1, '0000-00-00 00:00:00'),
(123, 'La Palma', 51, 1, '0000-00-00 00:00:00'),
(124, 'Tenerife', 51, 1, '0000-00-00 00:00:00'),
(125, 'La Graciosa', 51, 1, '0000-00-00 00:00:00'),
(126, 'Cantabrië', 52, 1, '0000-00-00 00:00:00'),
(127, 'Albacete', 53, 1, '0000-00-00 00:00:00'),
(128, 'Ciudad Real', 53, 1, '0000-00-00 00:00:00'),
(129, 'Cuenca', 53, 1, '0000-00-00 00:00:00'),
(130, 'Guadalajara', 53, 1, '0000-00-00 00:00:00'),
(131, 'Toledo', 53, 1, '0000-00-00 00:00:00'),
(132, 'Ãƒï¿½vila', 54, 1, '0000-00-00 00:00:00'),
(133, 'Burgos', 54, 1, '0000-00-00 00:00:00'),
(134, 'LeÃƒÂ³n	', 54, 1, '0000-00-00 00:00:00'),
(135, 'Palencia', 54, 1, '0000-00-00 00:00:00'),
(136, 'Salamanca', 54, 1, '0000-00-00 00:00:00'),
(137, 'Segovia', 54, 1, '0000-00-00 00:00:00'),
(138, 'Soria', 54, 1, '0000-00-00 00:00:00'),
(139, 'Valladolid', 54, 1, '0000-00-00 00:00:00'),
(140, 'Zamora', 54, 1, '0000-00-00 00:00:00'),
(141, 'Barcelona', 55, 1, '0000-00-00 00:00:00'),
(142, 'Gerona', 55, 1, '0000-00-00 00:00:00'),
(143, 'Lerida', 55, 1, '0000-00-00 00:00:00'),
(144, 'Tarragona', 55, 1, '0000-00-00 00:00:00'),
(145, 'Badajoz', 56, 1, '0000-00-00 00:00:00'),
(146, 'CÃƒÂ¡ceres', 56, 1, '0000-00-00 00:00:00'),
(147, 'A CoruÃƒÂ±a', 57, 1, '0000-00-00 00:00:00'),
(148, 'Lugo', 57, 1, '0000-00-00 00:00:00'),
(149, 'Ourense', 57, 1, '0000-00-00 00:00:00'),
(150, 'Pontevedra', 57, 1, '0000-00-00 00:00:00'),
(151, 'Madrid', 58, 1, '0000-00-00 00:00:00'),
(152, 'Murcia', 59, 1, '0000-00-00 00:00:00'),
(153, 'Navarra', 60, 1, '0000-00-00 00:00:00'),
(154, 'Alicante', 62, 1, '0000-00-00 00:00:00'),
(155, 'CastellÃƒÂ³n', 62, 1, '0000-00-00 00:00:00'),
(156, 'Valencia', 62, 1, '0000-00-00 00:00:00'),
(157, 'La Rioja', 61, 1, '0000-00-00 00:00:00'),
(164, 'test testing', 147, 1, '0000-00-00 00:00:00'),
(165, 'East India departement 1', 151, 1, '2017-09-25 10:28:36'),
(166, 'Himachal', 133, 1, '2018-04-08 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `extra_tabs`
--

CREATE TABLE `extra_tabs` (
  `id` int(11) NOT NULL,
  `accomodation_id` int(11) NOT NULL,
  `page_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tab_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=pending,1=approved,2=rejected',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `free_product_licenses`
--

CREATE TABLE `free_product_licenses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `accomodation_id` int(11) DEFAULT NULL,
  `product_type_id` int(11) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  `is_closed` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `free_subscription_licenses`
--

CREATE TABLE `free_subscription_licenses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `accom_id` int(11) DEFAULT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `token` varchar(100) NOT NULL,
  `is_closed` int(11) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `free_subscription_licenses`
--

INSERT INTO `free_subscription_licenses` (`id`, `user_id`, `accom_id`, `subscription_id`, `token`, `is_closed`, `created_at`, `updated_at`) VALUES
(5, 2, 133, 2, 'LBV-5acd1bcfcf95e', 1, '2018-04-10 20:17:19', '2018-04-10 20:17:19'),
(6, 2, 131, 2, 'LBV-5acd1cd1a961b', 0, '2018-04-10 20:21:37', '2018-04-10 20:21:37'),
(7, 2, 132, 3, 'LBV-5acd21447fa4a', 0, '2018-04-10 20:40:36', '2018-04-10 20:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `accom_id`, `image`, `sort_order`, `created_at`, `updated_at`) VALUES
(62, 92, '5ab0b9d836a7b.jpg', 1, '2018-03-20 07:35:52', '2018-04-06 17:15:30'),
(136, 92, '5abf5622eaec4.jpg', 2, '2018-03-31 09:34:26', '2018-04-06 17:15:30'),
(140, 127, '5ac9bd9275cc7.jpg', NULL, '2018-04-08 06:58:26', '2018-04-08 06:58:26'),
(141, 128, '5ac9bf9608473.jpg', NULL, '2018-04-08 07:07:02', '2018-04-08 07:07:02'),
(142, 129, '5ac9cfc700f1a.jpg', 1, '2018-04-08 08:16:07', '2018-04-08 08:17:00'),
(143, 129, '5ac9cfc761e60.jpg', 2, '2018-04-08 08:16:07', '2018-04-08 08:17:05'),
(144, 129, '5ac9cfc76f7b7.png', 3, '2018-04-08 08:16:07', '2018-04-08 08:17:05'),
(145, 130, '5ac9d21fca8bd.jpg', NULL, '2018-04-08 08:26:07', '2018-04-08 08:26:07'),
(146, 131, '5ac9d6e3d9285.jpg', 3, '2018-04-08 08:46:27', '2018-04-08 08:48:47'),
(147, 131, '5ac9d6e4483bd.png', 4, '2018-04-08 08:46:28', '2018-04-08 08:48:47'),
(148, 131, '5ac9d6eb9a738.jpg', 1, '2018-04-08 08:46:35', '2018-04-08 08:48:54'),
(149, 131, '5ac9d6ebb7eca.jpg', 2, '2018-04-08 08:46:35', '2018-04-08 08:48:54'),
(150, 132, '5aca07212695a.jpg', 2, '2018-04-08 12:12:17', '2018-04-08 14:56:38'),
(151, 132, '5aca0721292df.jpg', 1, '2018-04-08 12:12:17', '2018-04-08 14:56:38'),
(152, 132, '5aca07214cc4b.jpg', 3, '2018-04-08 12:12:17', '2018-04-08 14:56:38'),
(153, 132, '5aca072151f35.jpg', 4, '2018-04-08 12:12:17', '2018-04-08 14:56:38'),
(154, 135, '5aca8b0f75460.png', 2, '2018-04-08 21:35:11', '2018-04-08 21:35:43'),
(155, 135, '5aca8b104229b.png', 1, '2018-04-08 21:35:12', '2018-04-08 21:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `label_name` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `label_description` text COLLATE utf8_unicode_ci NOT NULL,
  `label_color` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `label_name`, `label_description`, `label_color`) VALUES
(1, 'LBV', 'Accoms of LBV only', ''),
(2, 'For Sale', 'Accoms for sale only', ''),
(3, 'For Rent', 'rental accoms only', ''),
(4, 'Rate', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_02_07_185643_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_methods`
--

CREATE TABLE `pay_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `method_name` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method_description` longtext COLLATE utf8_unicode_ci,
  `active` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pay_methods`
--

INSERT INTO `pay_methods` (`id`, `method_name`, `method_description`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Sofort', 'Eén van de populairste betaalmethodes in Duitsland & België en actief in 10 landen. <br />\r\n<strong>Verwerking: onmiddellijk</strong>', 1, NULL, NULL),
(2, 'Overschrijving', 'U krijgt een factuur toegezonden met daarop gegevens om een overschrijving uit te voeren.<br />\n<strong>Verwerking: 2-3 dagen</strong>', 1, NULL, NULL),
(3, 'Paypal', 'Met PayPal kan u betalen met verschillende creditkaarten, bankkaarten of uw PayPal-account zelf.<br />\r\n<strong>Verwerking: onmiddellijk</strong>', 1, NULL, NULL),
(4, 'Old Payment', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `price_units`
--

CREATE TABLE `price_units` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_name` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `price_units`
--

INSERT INTO `price_units` (`id`, `unit_name`) VALUES
(1, 'Per week'),
(2, 'Per nacht'),
(3, 'Per persoon per nacht'),
(4, 'Per persoon per week'),
(5, 'Verkoopprijs'),
(6, '2 personen per nacht'),
(7, ' Familiekamer/nacht');

-- --------------------------------------------------------

--
-- Table structure for table `privacies`
--

CREATE TABLE `privacies` (
  `id` int(11) NOT NULL,
  `content` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacies`
--

INSERT INTO `privacies` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<h1 class="entry-title single-title" data-mce-style="box-sizing: border-box; margin: 0px 15px 15px 0px; font-size: 24px; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; width: 1108px; float: left; word-break: normal; padding-top: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-top: 0px; margin-right: 15px; margin-bottom: 15px; font-size: 24px; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); width: 1108px; float: left; word-break: normal; padding-top: 15px;"><font style="vertical-align: inherit;">Privacy policy</font></h1><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"> </p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"><font style="vertical-align: inherit;">»GENERAL</font></h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"><font style="vertical-align: inherit;">LogerenbijVlamingen.com respects and protects the privacy of all users of its website. </font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">LogerenbijVlamingen.com makes every reasonable effort to ensure that all personal information that is provided will always be treated confidentially and will only be used for the purposes described in this Privacy Policy.</font><br data-mce-style="box-sizing: border-box;"> </p><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"><font style="vertical-align: inherit;">LogerenbijVlamingen.com collects three types of data for three different purposes:</font></p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">" PRIVATE DATA</font></h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"><font style="vertical-align: inherit;">When you contact us, we are entitled to store information such as your name, postal address, e-mail address, type of request and other information you have sent to us. </font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">Your information will only be used by </font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">LogerenbijVlamingen.com to provide you with personal and efficient information, to provide you with the most relevant information and to fulfill the obligations arising from the agreements you </font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">enter into </font><font style="vertical-align: inherit;">with </font><font style="vertical-align: inherit;">LogerenbijVlamingen.com. </font></font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">LogerenbijVlamingen.com will in no way provide personal data to third parties, unless legally compelled (by a judge for example).</font></p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"> <br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">»YOUR HOLIDAY HOME ADVERTISE DATA</font></h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;">LogerenbijVlamingen.com plaatst de advertentie-gegevens van uw vakantiehuis met de daarbij behorende contactgegevens op de website, ten behoeve van bezoekers van de website om contact met u op te nemen. De gegevens die u plaatst in uw advertentie op Internet zijn voor iedereen met een Internetverbinding beschikbaar en openbaar. Het is niet mogelijk om een advertentie van uw vakantiehuis te plaatsen bij LogerenbijVlamingen.com zonder dat u contactgegevens opgeeft, zoals de naam, telefoon, fax en/of email van de contactpersoon.</p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"> <br data-mce-style="box-sizing: border-box;">» SURF- EN BEZOEKERSGEGEVENS</h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;">Op de website houdt LogerenbijVlamingen.com algemene bezoekgegevens bij, zoals de website waar bezoekers vandaan komen, het aantal hits, datum en tijdstip van bezoek, de gebruikte browser, de meest opgevraagde pagina\'s, de pagina waar de website wordt verlaten, enzovoort.<br data-mce-style="box-sizing: border-box;">Deze informatie wordt uitsluitend gebruikt om het gebruik van de site te monitoren, de website verder te ontwikkelen en deze zo gebruikersvriendelijk mogelijk te ontwerpen.<br data-mce-style="box-sizing: border-box;">Om de privacy van de bezoekers te beschermen, zal alle informatie die op deze wijze wordt verzameld algemeen en anoniem zijn. Het is niet mogelijk om aan de hand van deze informatie een bezoeker te identificeren.</p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"> <br data-mce-style="box-sizing: border-box;">» COOKIEBELEID</h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;">Deze website maakt gebruik van Google Analytics, een webanalyse-service die wordt aangeboden door Google Inc. ("Google"). Google Analytics maakt gebruik van "cookies" (tekstbestandjes die op Uw computer worden geplaatst) om de website te helpen analyseren hoe gebruikers de site gebruiken. De door het cookie gegenereerde informatie over Uw gebruik van de website (met inbegrip van Uw IP-adres) wordt overgebracht naar en door Google opgeslagen op servers in de Verenigde Staten. Google gebruikt deze informatie om bij te houden hoe u de website gebruikt, rapporten over de website-activiteit op te stellen voor website-exploitanten en andere diensten aan te bieden met betrekking tot website-activiteit en internetgebruik.</p><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"> </p><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;">Google mag deze informatie aan derden verschaffen indien Google hiertoe wettelijk wordt verplicht, of voor zover deze derden de informatie namens Google verwerken. Google zal Uw IP-adres niet combineren met andere gegevens waarover Google beschikt. U kunt het gebruik van cookies weigeren door in Uw browser de daarvoor geëigende instellingen te kiezen. Wij wijzen u er echter op dat u in dat geval wellicht niet alle mogelijkheden van deze website kunt benutten.</p><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"> </p><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;">Door gebruik te maken van deze website geeft u toestemming voor het verwerken van de informatie door Google op de wijze en voor de doeleinden zoals hiervoor omschreven.</p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"> <br data-mce-style="box-sizing: border-box;">» WIJZIGING & VERWIJDERING PERSOONSGEGEVENS</h2><p data-mce-style="box-sizing: border-box; margin: 0px 0px 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 15px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"><font style="vertical-align: inherit;">You can decide to have your details changed or removed at any time by sending an email to </font><a href="https://logerenbijvlamingen.com/%22mailto:info@logerenbijvlamingen.com/%22" data-mce-href="../%22mailto:info@logerenbijvlamingen.com/%22" data-mce-style="box-sizing: border-box; background-color: transparent; color: #b881fc; text-decoration: none; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.25s ease;" style="color: rgb(184, 129, 252); border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.25s ease;">info@logerenbijvlamingen.com</a><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">Logging atVlamingen.com will respond to your call as soon as possible.</font></p><h2 data-mce-style="box-sizing: border-box; font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: #4d5567; margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="font-family: Raleway, sans-serif; font-weight: bold; line-height: 1.3em; color: rgb(77, 85, 103); margin: 0px 0px 15px; font-size: 20px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline;"> <br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">»CHANGE PRIVACY POLICY</font></h2><p data-mce-style="box-sizing: border-box; margin: 0px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-size: 14px; line-height: 26px; font-weight: normal; color: #a1a7b4; font-family: Raleway, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" style="margin-bottom: 0px; border: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 26px; color: rgb(161, 167, 180); font-family: Raleway, sans-serif;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Should LogerenbijVlamingen.com decide to change its Privacy Policy, then these changes will be immediately included in this online Privacy Policy. </font><font style="vertical-align: inherit;">Everyone is therefore always aware of what information is collected and for what purposes it is used. </font></font><br data-mce-style="box-sizing: border-box;"><font style="vertical-align: inherit;">The Privacy Policy was last changed on 01 January 2013.  </font></p><p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;"> </p>', '2017-01-20 12:12:32', '2018-02-07 21:12:06');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `product_name` varchar(300) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_month` varchar(10) DEFAULT NULL,
  `product_year` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_category_id`, `product_name`, `product_description`, `product_price`, `product_month`, `product_year`, `created_at`, `updated_at`) VALUES
(2, 2, 'In de kijker', 'Sta gedurende 1 maand bovenaan met 5 andere in de kijker', 50.00, '8', '2018', '2018-02-14 19:56:15', '2018-02-23 14:19:33'),
(3, 5, 'Last minute', 'Last minute', 15.00, '5', '2018', '2018-02-14 19:57:41', '2018-02-23 14:20:18'),
(4, 1, 'Tip van de maand', 'Sta gedurende 1 maand alleen bovenaan met grote foto', 80.00, '10', '2018', '2018-02-20 18:02:07', '2018-02-23 14:20:45'),
(5, 3, 'Nieuws', 'Hebt u iets te melden, doe dit met een nieuwsitem', 10.00, '1', '2018', '2018-02-23 14:21:30', '2018-02-23 14:21:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Tip', NULL, NULL),
(2, 'Spotlight', NULL, NULL),
(3, 'News', NULL, NULL),
(4, 'Newsletter', NULL, NULL),
(5, 'Last Minute', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `region_name` varchar(767) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `is_approved` int(11) NOT NULL COMMENT '0=No,1=Yes',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region_name`, `country_id`, `is_approved`, `created`) VALUES
(1, 'Antwerpen', 1, 1, '0000-00-00 00:00:00'),
(2, 'Limburg', 1, 1, '0000-00-00 00:00:00'),
(3, 'Noord-Brabant', 2, 1, '0000-00-00 00:00:00'),
(4, 'Friesland', 2, 1, '0000-00-00 00:00:00'),
(5, 'Alsace', 3, 1, '0000-00-00 00:00:00'),
(6, 'Aquitaine', 3, 1, '0000-00-00 00:00:00'),
(7, 'Auvergne', 3, 1, '0000-00-00 00:00:00'),
(8, 'Basse-Normandie', 3, 1, '0000-00-00 00:00:00'),
(9, 'Bourgogne', 3, 1, '0000-00-00 00:00:00'),
(10, 'Bretagne', 3, 1, '0000-00-00 00:00:00'),
(11, 'Centre', 3, 1, '0000-00-00 00:00:00'),
(12, 'Champagne-Ardenne', 3, 1, '0000-00-00 00:00:00'),
(13, 'Corse', 3, 1, '0000-00-00 00:00:00'),
(14, 'Franche-Comte', 3, 1, '0000-00-00 00:00:00'),
(15, 'Haute-Normandie', 3, 1, '0000-00-00 00:00:00'),
(16, 'Ile-de-France', 3, 1, '0000-00-00 00:00:00'),
(17, 'Languedoc-Roussillon', 3, 1, '0000-00-00 00:00:00'),
(18, 'Limousin', 3, 1, '0000-00-00 00:00:00'),
(19, 'Lorraine', 3, 1, '0000-00-00 00:00:00'),
(20, 'Midi-Pyrenees', 3, 1, '0000-00-00 00:00:00'),
(21, 'Nord', 3, 1, '0000-00-00 00:00:00'),
(22, 'Normandie', 3, 1, '0000-00-00 00:00:00'),
(23, 'Pays-de-la-Loire', 3, 1, '0000-00-00 00:00:00'),
(24, 'Picardie', 3, 1, '0000-00-00 00:00:00'),
(25, 'Poitou-Charente', 3, 1, '0000-00-00 00:00:00'),
(26, 'Provence-Alpes-Cote d Azur', 3, 1, '0000-00-00 00:00:00'),
(27, 'Rhone-Alpes', 3, 1, '0000-00-00 00:00:00'),
(28, 'Drenthe', 2, 1, '0000-00-00 00:00:00'),
(29, 'Flevoland', 2, 1, '0000-00-00 00:00:00'),
(30, 'Gelderland', 2, 1, '0000-00-00 00:00:00'),
(31, 'Groningen', 2, 1, '0000-00-00 00:00:00'),
(32, 'Limburg', 2, 1, '0000-00-00 00:00:00'),
(33, 'Noord-Holland', 2, 1, '0000-00-00 00:00:00'),
(34, 'Overijssel', 2, 1, '0000-00-00 00:00:00'),
(35, 'Utrecht', 2, 1, '0000-00-00 00:00:00'),
(36, 'Zeeland', 2, 1, '0000-00-00 00:00:00'),
(37, 'Zuid-Holland', 2, 1, '0000-00-00 00:00:00'),
(38, 'Oost-Vlaanderen', 1, 1, '0000-00-00 00:00:00'),
(39, 'Vlaams-Brabant', 1, 1, '0000-00-00 00:00:00'),
(40, 'West-Vlaanderen', 1, 1, '0000-00-00 00:00:00'),
(41, 'Henegouwen', 1, 1, '0000-00-00 00:00:00'),
(42, 'Luik', 1, 1, '0000-00-00 00:00:00'),
(43, 'Luxemburg', 1, 1, '0000-00-00 00:00:00'),
(44, 'Namen ', 1, 1, '0000-00-00 00:00:00'),
(45, 'Waals-Brabant', 1, 1, '0000-00-00 00:00:00'),
(46, 'Andalusie', 5, 1, '0000-00-00 00:00:00'),
(47, 'Aragon', 5, 1, '0000-00-00 00:00:00'),
(48, 'Asturie', 5, 1, '0000-00-00 00:00:00'),
(49, 'Balearen', 5, 1, '0000-00-00 00:00:00'),
(50, 'Baskenland', 5, 1, '0000-00-00 00:00:00'),
(51, 'Canarische Eilanden', 5, 1, '0000-00-00 00:00:00'),
(52, 'Cantabrië', 5, 1, '0000-00-00 00:00:00'),
(53, 'Castilie-La Mancha', 5, 1, '0000-00-00 00:00:00'),
(54, 'Castilie en Leon', 5, 1, '0000-00-00 00:00:00'),
(55, 'Catalonie', 5, 1, '0000-00-00 00:00:00'),
(56, 'Extremadura', 5, 1, '0000-00-00 00:00:00'),
(57, 'Galicie', 5, 1, '0000-00-00 00:00:00'),
(58, 'Madrid', 5, 1, '0000-00-00 00:00:00'),
(59, 'Murcia', 5, 1, '0000-00-00 00:00:00'),
(60, 'Navarra', 5, 1, '0000-00-00 00:00:00'),
(61, 'La Rioja', 5, 1, '0000-00-00 00:00:00'),
(62, 'Valencia', 5, 1, '0000-00-00 00:00:00'),
(63, 'Abruzzen', 7, 1, '0000-00-00 00:00:00'),
(64, 'Apulie', 7, 1, '0000-00-00 00:00:00'),
(65, 'Basilicata', 7, 1, '0000-00-00 00:00:00'),
(66, 'Calabrië', 7, 1, '0000-00-00 00:00:00'),
(67, 'Campania', 7, 1, '0000-00-00 00:00:00'),
(68, 'Emilia-Romagna', 7, 1, '0000-00-00 00:00:00'),
(69, 'Friuli-Venezia Giulia', 7, 1, '0000-00-00 00:00:00'),
(70, 'Lazio', 7, 1, '0000-00-00 00:00:00'),
(71, 'Ligurie', 7, 1, '0000-00-00 00:00:00'),
(72, 'Lombardije', 7, 1, '0000-00-00 00:00:00'),
(73, 'Marche', 7, 1, '0000-00-00 00:00:00'),
(74, 'Molise', 7, 1, '0000-00-00 00:00:00'),
(75, 'Piemont', 7, 1, '0000-00-00 00:00:00'),
(76, 'Sardinië', 7, 1, '0000-00-00 00:00:00'),
(77, 'Sicilie', 7, 1, '0000-00-00 00:00:00'),
(78, 'Trentino-Zuid-Tirol', 7, 1, '0000-00-00 00:00:00'),
(79, 'Toscane', 7, 1, '0000-00-00 00:00:00'),
(80, 'Umbrië', 7, 1, '0000-00-00 00:00:00'),
(81, 'Valle d\'Aosta', 7, 1, '0000-00-00 00:00:00'),
(82, 'Veneto', 7, 1, '0000-00-00 00:00:00'),
(83, 'Noord Portugal', 4, 1, '0000-00-00 00:00:00'),
(84, 'De Beiras - Midden Portugal', 4, 1, '0000-00-00 00:00:00'),
(85, 'Vale de Tejo - Taag', 4, 1, '0000-00-00 00:00:00'),
(86, 'Alentejo', 4, 1, '0000-00-00 00:00:00'),
(87, 'Algarve', 4, 1, '0000-00-00 00:00:00'),
(88, 'Noord', 25, 1, '0000-00-00 00:00:00'),
(89, 'Caribbean', 8, 1, '0000-00-00 00:00:00'),
(90, 'Zuid salzburgerland', 6, 1, '0000-00-00 00:00:00'),
(91, 'Bali', 31, 1, '0000-00-00 00:00:00'),
(92, 'Limpopo', 29, 1, '0000-00-00 00:00:00'),
(93, 'Boheemse woud', 17, 1, '0000-00-00 00:00:00'),
(94, 'Rhodos', 12, 1, '0000-00-00 00:00:00'),
(95, 'Tensift haouz marrakech', 22, 1, '0000-00-00 00:00:00'),
(96, 'Tisza meer', 34, 1, '0000-00-00 00:00:00'),
(97, 'Bahia', 14, 1, '0000-00-00 00:00:00'),
(98, 'Eiland', 30, 1, '0000-00-00 00:00:00'),
(99, 'Caraiben', 19, 1, '0000-00-00 00:00:00'),
(100, 'Sosua', 19, 1, '0000-00-00 00:00:00'),
(101, 'Oued souss', 22, 1, '0000-00-00 00:00:00'),
(102, 'Western cape', 29, 1, '0000-00-00 00:00:00'),
(103, 'Sauerland', 11, 1, '0000-00-00 00:00:00'),
(104, 'Logarska', 18, 1, '0000-00-00 00:00:00'),
(105, 'Chiriqui', 23, 1, '0000-00-00 00:00:00'),
(106, 'Magnisia', 12, 1, '0000-00-00 00:00:00'),
(107, 'Saly niakhniakhal', 26, 1, '0000-00-00 00:00:00'),
(108, 'Mbour', 26, 1, '0000-00-00 00:00:00'),
(109, 'Zuid kust', 24, 1, '0000-00-00 00:00:00'),
(110, 'Zuidwesten (neder-silezie)', 25, 1, '0000-00-00 00:00:00'),
(111, 'Sal', 20, 1, '0000-00-00 00:00:00'),
(112, 'Istrie', 21, 1, '0000-00-00 00:00:00'),
(113, 'Co. Clare', 13, 1, '0000-00-00 00:00:00'),
(114, 'Salzburgerland', 6, 1, '0000-00-00 00:00:00'),
(115, 'Kandy', 27, 1, '0000-00-00 00:00:00'),
(116, 'Vorarlberg', 6, 1, '0000-00-00 00:00:00'),
(117, 'Antalya', 28, 1, '0000-00-00 00:00:00'),
(118, 'Vratsa', 35, 1, '0000-00-00 00:00:00'),
(119, 'Gambia', 32, 1, '0000-00-00 00:00:00'),
(120, 'Zweden', 9, 1, '0000-00-00 00:00:00'),
(121, 'Luxemburg', 10, 1, '0000-00-00 00:00:00'),
(122, 'Roemenie', 15, 1, '0000-00-00 00:00:00'),
(123, 'Slovakije', 16, 1, '0000-00-00 00:00:00'),
(124, 'Leiria - Costa de Prata', 4, 1, '0000-00-00 00:00:00'),
(125, 'Sine Saloum', 26, 1, '0000-00-00 00:00:00'),
(126, 'Thailand', 36, 1, '0000-00-00 00:00:00'),
(129, 'Triglav nationaal park', 18, 1, '0000-00-00 00:00:00'),
(130, 'Georgia', 37, 1, '0000-00-00 00:00:00'),
(131, 'Karinthie', 6, 1, '0000-00-00 00:00:00'),
(132, 'Stayerska', 18, 1, '0000-00-00 00:00:00'),
(133, 'NORTH', 38, 1, '0000-00-00 00:00:00'),
(134, 'Blagoevgrad', 35, 1, '0000-00-00 00:00:00'),
(136, 'British Columbia', 40, 1, '0000-00-00 00:00:00'),
(147, 'testing', 51, 1, '0000-00-00 00:00:00'),
(151, 'East india 1', 38, 1, '2017-09-25 10:28:33'),
(152, 'Auvergne', 52, 1, '2017-11-30 17:10:49'),
(154, 'Kreta', 12, 1, '2017-12-28 16:06:12'),
(156, 'Gozo', 54, 1, '2018-01-02 19:52:45');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Customer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Advertiser', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `accom_price_id` int(11) DEFAULT NULL,
  `room_type` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `adults` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `max_child_age` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `no_of_rooms` int(11) DEFAULT '1',
  `total_adults` int(11) DEFAULT NULL,
  `total_childs` int(11) DEFAULT NULL,
  `min_days` int(11) DEFAULT NULL,
  `max_adults_per_room` int(11) DEFAULT NULL,
  `max_persons_per_room` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `accom_id`, `accom_price_id`, `room_type`, `description`, `adults`, `childs`, `max_child_age`, `created_at`, `updated_at`, `no_of_rooms`, `total_adults`, `total_childs`, `min_days`, `max_adults_per_room`, `max_persons_per_room`) VALUES
(17, 49, 8, 'dfdf', 'dfdfdf', 1, 1, 0, '2018-03-02 11:24:59', '2018-03-02 11:24:59', 1, NULL, NULL, 1, 1, 1),
(38, 92, 54, 'test 19 maart', 'test 19 maart B&B', 1, 1, 0, '2018-03-19 10:24:10', '2018-04-06 17:37:46', 1, NULL, NULL, NULL, NULL, NULL),
(41, 92, 61, 'room 2', 'room 2', 2, 0, 0, '2018-03-20 07:35:22', '2018-04-06 18:21:09', 1, NULL, NULL, NULL, NULL, NULL),
(55, 92, 82, 'room 2', 'room 2', 2, 1, 0, '2018-04-04 17:11:15', '2018-04-06 17:37:53', 1, NULL, NULL, NULL, NULL, NULL),
(64, 129, 98, 'Delux Room', 'Delux Room Description goes here.......', 1, 0, 0, '2018-04-08 08:13:15', '2018-04-08 08:13:40', 1, NULL, NULL, NULL, 1, 1),
(65, 129, 99, 'ORDINARY ROOM', 'description......', 1, 0, 0, '2018-04-08 08:15:21', '2018-04-08 08:15:21', 1, NULL, NULL, NULL, NULL, NULL),
(66, 130, 100, 'test 2', 'test', 1, 1, 0, '2018-04-08 08:22:43', '2018-04-08 08:23:45', 1, NULL, NULL, NULL, 1, 1),
(67, 130, 101, 'kamer', 'kamer', 2, 0, 0, '2018-04-08 08:25:17', '2018-04-08 08:25:17', 1, NULL, NULL, NULL, NULL, NULL),
(68, 131, 102, 'Deluxe Room', 'deluxe room', 1, 1, 0, '2018-04-08 08:46:09', '2018-04-08 08:46:09', 1, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `room_prices`
--

CREATE TABLE `room_prices` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `price1` double(10,2) DEFAULT NULL,
  `price2` double(10,2) DEFAULT NULL,
  `price3` double(10,2) DEFAULT NULL,
  `extra_price_adult` double(10,2) DEFAULT NULL,
  `extra_price_child` double(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_prices`
--

INSERT INTO `room_prices` (`id`, `room_id`, `accom_id`, `start_date`, `end_date`, `price`, `price1`, `price2`, `price3`, `extra_price_adult`, `extra_price_child`, `created_at`, `updated_at`) VALUES
(11, 17, 49, '2018-03-26 00:00:00', '2018-03-26 00:00:00', NULL, 2.00, 3.00, 4.00, 1.00, 1.00, '2018-03-02 11:24:59', '2018-03-02 11:24:59'),
(32, 38, 92, '2018-03-19 00:00:00', '2018-10-31 00:00:00', 90.00, NULL, NULL, NULL, NULL, NULL, '2018-03-19 10:24:10', '2018-04-06 17:37:46'),
(35, 41, 92, '2018-11-01 00:00:00', '2019-12-31 00:00:00', 65.00, NULL, NULL, NULL, NULL, NULL, '2018-03-20 07:35:22', '2018-04-06 18:21:09'),
(49, 55, 92, '2019-01-01 00:00:00', '2019-01-31 00:00:00', 60.00, NULL, NULL, NULL, NULL, NULL, '2018-04-04 17:11:15', '2018-04-06 17:37:53'),
(58, 64, 129, '2018-04-08 00:00:00', '2018-04-25 00:00:00', 50.00, NULL, NULL, NULL, 1.00, 1.00, '2018-04-08 08:13:15', '2018-04-08 08:13:40'),
(59, 65, 129, '2018-04-08 00:00:00', '2018-04-25 23:59:59', 20.00, NULL, NULL, NULL, NULL, NULL, '2018-04-08 08:15:21', '2018-04-08 08:15:21'),
(60, 66, 130, '2018-04-08 00:00:00', '2018-04-11 00:00:00', 50.00, NULL, NULL, NULL, 1.00, 1.00, '2018-04-08 08:22:43', '2018-04-08 08:23:45'),
(61, 67, 130, '2018-04-08 00:00:00', '2018-04-14 23:59:59', 70.00, NULL, NULL, NULL, NULL, NULL, '2018-04-08 08:25:17', '2018-04-08 08:25:17'),
(62, 68, 131, '2018-04-10 00:00:00', '2018-04-20 23:59:59', 10.00, NULL, NULL, NULL, 1.00, 1.00, '2018-04-08 08:46:09', '2018-04-08 08:46:09');

-- --------------------------------------------------------

--
-- Table structure for table `searching_groups`
--

CREATE TABLE `searching_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(2000) NOT NULL,
  `group_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `searching_groups`
--

INSERT INTO `searching_groups` (`id`, `group_name`, `group_order`) VALUES
(1, 'Algemeen', 1),
(2, 'Locatie / omgeving', 2),
(3, 'Faciliteiten', 3),
(4, 'Activiteiten', 4),
(5, 'Rolstoeltoegankelijkheid', 5);

-- --------------------------------------------------------

--
-- Table structure for table `searchtags`
--

CREATE TABLE `searchtags` (
  `id` int(11) NOT NULL,
  `tag_name` varchar(2000) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `searchtags`
--

INSERT INTO `searchtags` (`id`, `tag_name`, `created_at`, `updated_at`) VALUES
(1, 'Zwembad', NULL, NULL),
(2, 'Huisdieren toegelaten', NULL, NULL),
(3, 'Rustig gelegen', NULL, NULL),
(4, 'Kindvriendelijk', NULL, NULL),
(5, 'Internet / WiFi', NULL, NULL),
(6, 'Roken niet toegestaan', NULL, NULL),
(7, 'Aan zee', NULL, NULL),
(8, 'Aan bos', NULL, NULL),
(9, 'Dichtbij meer', NULL, NULL),
(10, 'Dichtbij golfbaan', NULL, NULL),
(11, 'Rolstoel toegankelijk', NULL, NULL),
(12, 'Wintersport', NULL, NULL),
(13, 'Overwinteren', NULL, NULL),
(14, 'Fietsenverhuur', NULL, NULL),
(15, 'Wandelroutes', NULL, NULL),
(16, 'Geen kinderen -12 j', NULL, NULL),
(17, 'Dichtbij manege', NULL, NULL),
(18, 'Paardenrijschool', NULL, NULL),
(19, 'In de bergen', NULL, NULL),
(20, 'Airconditioning', NULL, NULL),
(21, 'Fietsroutes', NULL, NULL),
(22, 'Aan rivier', NULL, NULL),
(23, 'Historische site', NULL, NULL),
(24, 'Verwarmd zwembad', NULL, NULL),
(25, 'Table d\'hotes', NULL, NULL),
(26, 'Ontbijt', NULL, NULL),
(27, 'Vegetarische keuken', NULL, NULL),
(28, 'Wellness', NULL, NULL),
(29, 'Televisie', NULL, NULL),
(30, 'Workshops', NULL, NULL),
(31, 'Naturisme', NULL, NULL),
(32, 'Geschikt voor grote groepen', NULL, NULL),
(33, 'Op wijngaard', NULL, NULL),
(34, 'Op de boerderij', NULL, NULL),
(35, 'In/bij dorp', NULL, NULL),
(36, 'In/bij stad', NULL, NULL),
(37, 'Privé zwembad', NULL, NULL),
(38, 'Binnenzwembad', NULL, NULL),
(39, 'Tennisbaan', NULL, NULL),
(40, 'Sauna', NULL, NULL),
(41, 'Jacuzzi', NULL, NULL),
(42, 'Vaatwasser', NULL, NULL),
(43, 'Wasmachine', NULL, NULL),
(44, 'Speeltuin', NULL, NULL),
(45, 'Cursusruimte', NULL, NULL),
(46, 'Mountainbike routes', NULL, NULL),
(47, 'Hengelsport', NULL, NULL),
(48, 'Pretpark', NULL, NULL),
(49, 'Watersport', NULL, NULL),
(50, 'Bergsport', NULL, NULL),
(51, 'Vliegsport', NULL, NULL),
(52, 'Pottenbakken', NULL, NULL),
(53, 'Yoga', NULL, NULL),
(54, 'Kookcursus', NULL, NULL),
(55, 'Taalcursus', NULL, NULL),
(56, 'Schildercursus', NULL, NULL),
(57, 'Geen kastjes onder wasbak', NULL, NULL),
(58, 'Keukenblad lager dan 93cm', NULL, NULL),
(59, 'Toilet verzwaard + handsteunen', NULL, NULL),
(60, 'Royale inrijdouche', NULL, NULL),
(61, 'Verharde paden', NULL, NULL),
(62, 'Plaats voor tilt lift', NULL, NULL),
(63, 'Deuren 90cm breed', NULL, NULL),
(64, 'Nederlandstalige hulp', NULL, NULL),
(65, 'Survivalpark', NULL, NULL),
(66, 'Chloorvrij zwembad', NULL, NULL),
(67, 'Hondensport', NULL, NULL),
(68, 'Massage', NULL, NULL),
(69, 'Begeleide wandelingen', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `searchtag_searching_groups`
--

CREATE TABLE `searchtag_searching_groups` (
  `id` int(11) NOT NULL,
  `searching_group_id` int(11) NOT NULL,
  `searchtag_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `searchtag_searching_groups`
--

INSERT INTO `searchtag_searching_groups` (`id`, `searching_group_id`, `searchtag_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 2, 3, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 3, 5, NULL, NULL),
(6, 1, 6, NULL, NULL),
(7, 2, 7, NULL, NULL),
(8, 2, 8, NULL, NULL),
(9, 2, 9, NULL, NULL),
(10, 4, 10, NULL, NULL),
(11, 3, 11, NULL, NULL),
(12, 4, 12, NULL, NULL),
(13, 1, 13, NULL, NULL),
(14, 3, 14, NULL, NULL),
(15, 4, 15, NULL, NULL),
(16, 1, 16, NULL, NULL),
(17, 4, 17, NULL, NULL),
(18, 4, 18, NULL, NULL),
(19, 2, 19, NULL, NULL),
(20, 3, 20, NULL, NULL),
(21, 4, 21, NULL, NULL),
(22, 2, 22, NULL, NULL),
(23, 2, 23, NULL, NULL),
(24, 3, 24, NULL, NULL),
(25, 1, 25, NULL, NULL),
(26, 1, 26, NULL, NULL),
(27, 1, 27, NULL, NULL),
(28, 4, 28, NULL, NULL),
(29, 3, 29, NULL, NULL),
(30, 4, 30, NULL, NULL),
(31, 1, 31, NULL, NULL),
(32, 1, 32, NULL, NULL),
(33, 2, 33, NULL, NULL),
(34, 2, 34, NULL, NULL),
(35, 2, 35, NULL, NULL),
(36, 2, 36, NULL, NULL),
(37, 3, 37, NULL, NULL),
(38, 3, 38, NULL, NULL),
(39, 3, 39, NULL, NULL),
(40, 3, 40, NULL, NULL),
(41, 3, 41, NULL, NULL),
(42, 3, 42, NULL, NULL),
(43, 3, 43, NULL, NULL),
(44, 3, 44, NULL, NULL),
(45, 3, 45, NULL, NULL),
(46, 4, 46, NULL, NULL),
(47, 4, 47, NULL, NULL),
(48, 4, 48, NULL, NULL),
(49, 4, 49, NULL, NULL),
(50, 4, 50, NULL, NULL),
(51, 4, 51, NULL, NULL),
(52, 4, 52, NULL, NULL),
(53, 4, 53, NULL, NULL),
(54, 4, 54, NULL, NULL),
(55, 4, 55, NULL, NULL),
(56, 4, 56, NULL, NULL),
(57, 5, 57, NULL, NULL),
(58, 5, 58, NULL, NULL),
(59, 5, 59, NULL, NULL),
(60, 5, 60, NULL, NULL),
(61, 5, 61, NULL, NULL),
(62, 5, 62, NULL, NULL),
(63, 5, 63, NULL, NULL),
(64, 5, 64, NULL, NULL),
(65, 4, 65, NULL, NULL),
(66, 3, 66, NULL, NULL),
(67, 4, 67, NULL, NULL),
(68, 4, 68, NULL, NULL),
(69, 4, 69, NULL, NULL),
(71, 2, 82, NULL, NULL),
(72, 2, 83, NULL, NULL),
(73, 2, 84, NULL, NULL),
(74, 1, 85, NULL, NULL),
(75, 1, 86, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `option_key`, `option_value`, `created_at`, `updated_at`) VALUES
(1, 'CONTACT_EMAIL', 'test@testing.in', '2018-02-08 00:00:00', '2018-02-08 08:02:01'),
(2, 'CONTACT_PHONE', '9625585013', '2018-02-08 00:00:00', '2018-02-08 00:00:00'),
(3, 'FACEBOOK_URL', 'www.facebook.com/something', '2018-02-08 00:00:00', '2018-02-10 06:19:53'),
(4, 'TWITTER_URL', 'www.twitter.com', '2018-02-08 00:00:00', '2018-02-07 20:05:11'),
(5, 'GOOGLE_URL', 'www.google.com', '2018-02-08 00:00:00', '2018-02-08 00:00:00'),
(6, 'HEADER_TEXT', 'Staying with the Flemish: Feel at home, even abroad!', '2018-02-08 00:00:00', '2018-02-07 20:09:27'),
(7, 'CONTACT_ADDRESS', 'Kasumpti Shimla Himachal Pradesh India', '2018-02-08 00:00:00', '2018-02-07 20:09:45'),
(8, 'FOOTER_TEXT', 'Logerenbijvlamingen.com offers rental of holiday homes, Gîtes, Chambres d\'hôtes, guest rooms and villas to Flemish and Dutch speakers worldwide. Partner of Holidays with Dutch people', '2018-02-08 00:00:00', '2018-02-08 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` double(10,2) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT '2=Sale,3=Rent',
  `duration` int(11) NOT NULL,
  `duration_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `created_at`, `updated_at`, `name`, `description`, `amount`, `type_id`, `duration`, `duration_type`) VALUES
(2, '2018-03-28 18:57:33', '2018-04-05 17:40:08', 'Rent Advertisement', 'Rent 1 Year Advertisement', 70.00, 3, 1, 'Year'),
(3, '2018-03-28 19:08:50', '2018-04-05 17:40:23', 'Sale Advertisement', 'Sale Advertisement For 6 Months', 50.00, 2, 6, 'Month');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_instances`
--

CREATE TABLE `subscription_instances` (
  `id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `accom_id` int(11) NOT NULL,
  `start_from` datetime NOT NULL,
  `valid_upto` datetime NOT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_instances`
--

INSERT INTO `subscription_instances` (`id`, `subscription_id`, `accom_id`, `start_from`, `valid_upto`, `amount`, `created_at`, `updated_at`) VALUES
(1, 2, 131, '2018-04-10 16:43:48', '2019-04-10 23:59:59', 70.00, '2018-04-10 16:43:48', '2018-04-10 16:43:48'),
(2, 2, 131, '2019-04-10 00:00:00', '2020-04-10 23:59:59', 70.00, '2018-04-10 17:26:47', '2018-04-10 17:26:47'),
(3, 2, 133, '2018-04-10 17:30:27', '2019-04-10 23:59:59', 70.00, '2018-04-10 17:30:27', '2018-04-10 17:30:27'),
(4, 3, 135, '2018-04-10 17:54:54', '2018-10-10 23:59:59', 50.00, '2018-04-10 17:54:54', '2018-04-10 17:54:54'),
(5, 3, 135, '2018-10-10 00:00:00', '2019-04-10 23:59:59', 50.00, '2018-04-10 17:58:26', '2018-04-10 17:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_licenses`
--

CREATE TABLE `subscription_licenses` (
  `id` int(11) NOT NULL,
  `subscription_payment_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `start_from` datetime NOT NULL,
  `valid_upto` datetime NOT NULL,
  `accom_id` int(11) DEFAULT NULL,
  `subscription_key` varchar(300) NOT NULL,
  `invoice_number` varchar(50) DEFAULT NULL,
  `order_number` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `license_renewed` int(11) NOT NULL DEFAULT '0',
  `four_notified` int(11) NOT NULL DEFAULT '0' COMMENT '4 week notification',
  `two_notified` int(11) NOT NULL DEFAULT '0' COMMENT '2 week notification',
  `one_notified` int(11) NOT NULL DEFAULT '0' COMMENT '1 week notification',
  `on_expire` int(11) NOT NULL DEFAULT '0' COMMENT 'onexpire',
  `after_expire` int(11) NOT NULL DEFAULT '0' COMMENT 'after expire',
  `token` varchar(50) DEFAULT NULL,
  `tmp_valid_upto` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_licenses`
--

INSERT INTO `subscription_licenses` (`id`, `subscription_payment_id`, `created_at`, `updated_at`, `start_from`, `valid_upto`, `accom_id`, `subscription_key`, `invoice_number`, `order_number`, `user_id`, `license_renewed`, `four_notified`, `two_notified`, `one_notified`, `on_expire`, `after_expire`, `token`, `tmp_valid_upto`) VALUES
(1, 1, '2018-04-10 16:43:48', '2018-04-10 16:43:48', '2018-04-10 16:43:48', '2019-04-10 23:59:59', 131, 'LBV-5acce9c4580a9', 'LBV-LIC-1', NULL, 2, 1, 0, 0, 0, 0, 0, NULL, NULL),
(2, 2, '2018-04-10 17:26:47', '2018-04-10 17:26:47', '2019-04-10 00:00:00', '2020-04-10 23:59:59', 131, 'LBV-5accf3d7afde3', 'LBV-LIC-2', NULL, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(3, 3, '2018-04-10 17:30:27', '2018-04-10 17:30:27', '2018-04-10 17:30:27', '2019-04-10 23:59:59', 133, 'LBV-5accf4b3eca92', 'LBV-LIC-3', NULL, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(4, 4, '2018-04-10 17:54:54', '2018-04-10 17:54:54', '2018-04-10 17:54:54', '2018-10-10 23:59:59', 135, 'LBV-5accfa6e37528', 'LBV-LIC-4', NULL, 2, 1, 0, 0, 0, 0, 0, NULL, NULL),
(5, 5, '2018-04-10 17:58:26', '2018-04-10 17:58:26', '2018-10-10 00:00:00', '2019-04-10 23:59:59', 135, 'LBV-5accfb4239239', 'LBV-LIC-5', NULL, 2, 0, 0, 0, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscription_payments`
--

CREATE TABLE `subscription_payments` (
  `id` int(11) NOT NULL,
  `subscription_instance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount_paid` float(8,2) NOT NULL,
  `cancelled_amount` double(10,2) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `pay_method_id` int(11) DEFAULT '0',
  `payment_complete` int(11) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes,2=Cancelled,3=Free License',
  `sub_transaction_id` varchar(100) DEFAULT NULL,
  `pay_json` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `paypal_ipn_response` longtext,
  `paypal_ipn_response_id` int(11) DEFAULT NULL,
  `ipn_verified` int(11) DEFAULT NULL COMMENT '0=No,1=Yes',
  `paypal_cart_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_payments`
--

INSERT INTO `subscription_payments` (`id`, `subscription_instance_id`, `user_id`, `amount_paid`, `cancelled_amount`, `discount`, `pay_method_id`, `payment_complete`, `sub_transaction_id`, `pay_json`, `created_at`, `updated_at`, `paypal_ipn_response`, `paypal_ipn_response_id`, `ipn_verified`, `paypal_cart_id`) VALUES
(1, 1, 2, 70.00, NULL, NULL, 3, 1, 'PAY-0PW6117762005831NLLGOTHI', NULL, '2018-04-10 16:43:48', '2018-04-10 16:43:48', NULL, NULL, NULL, '55P218936J227223U'),
(2, 2, 2, 70.00, NULL, NULL, 3, 1, 'PAY-5YE42479RS814580NLLGPFGY', NULL, '2018-04-10 17:26:47', '2018-04-10 17:26:47', NULL, NULL, NULL, '01X253931S778314W'),
(3, 3, 2, 70.00, NULL, NULL, 3, 1, 'PAY-79979063YE6032703LLGPJHQ', NULL, '2018-04-10 17:30:27', '2018-04-10 17:30:27', NULL, NULL, NULL, '2UX711780D910601D'),
(4, 4, 2, 50.00, NULL, NULL, 3, 1, 'PAY-6G551841JA207041ALLGPUSA', NULL, '2018-04-10 17:54:54', '2018-04-10 17:54:54', NULL, NULL, NULL, '2NR714048L392873N'),
(5, 5, 2, 50.00, NULL, NULL, 3, 1, 'PAY-38641282S1726684GLLGPVZQ', NULL, '2018-04-10 17:58:26', '2018-04-10 17:58:26', NULL, NULL, NULL, '1F018689PK1842337');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `content`, `created_at`, `updated_at`) VALUES
(2, '<h1 class="entry-title single-title" style="margin-top: 0px; margin-right: 15px; margin-bottom: 15px; font-size: 24px; font-family: Raleway, sans-serif; line-height: 1.3em; color: rgb(77, 85, 103); font-variant-numeric: normal; font-variant-east-asian: normal; width: 1108px; float: left; word-break: normal; padding-top: 15px; widows: 1;">Algemene voorwaarden</h1><h1 class="entry-title single-title" style="margin-top: 0px; margin-right: 15px; margin-bottom: 15px; font-size: 24px; font-family: Raleway, sans-serif; line-height: 1.3em; color: rgb(77, 85, 103); width: 1108px; float: left; word-break: normal; padding-top: 15px; font-variant-numeric: normal; font-variant-east-asian: normal; widows: 1;"><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Welkom bij Logeren bij Vlamingen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De website, logerenbijvlamingen.com, wordt aangeboden door Joeri De Bonnaire, eigenaar van Logeren bij Vlamingen, kantoorhoudende te Feugarolles, lieu dit Lencouet, Frankrijk en ingeschreven onder siret no. 53453667700014</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Deze website is een platform waar eigenaars/adverteerders van vakantieaccommodaties hun accommodaties te huur aanbieden op deze website, door middel van een advertentie, aan bezoekers van deze website.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Door de website te bezoeken en te gebruiken verklaart u zich akkoord met deze algemene voorwaarden. Wij adviseren u dan ook deze algemene voorwaarden te lezen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Elke afgesloten huurovereenkomst gebeurd rechtstreeks tussen verhuurder en huurder. Joeri De Bonnaire en logerenbijvlamingen.com zijn en worden geen partij bij een tot stand gekomen huurovereenkomst.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"=""><br>In ons Privacybeleid wordt uitgelegd hoe logerenbijvlamingen.com uw privacy beschermd wanneer u onze Website gebruikt. Daarnaast leggen we uit hoe logerenbijvlamingen.com omgaat met uw persoonlijke gegevens. Ons Privacybeleid kunt u via de link&nbsp;onder aan de website raadplegen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Definities:</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Accommodatie: Het te huur aangeboden vakantieverblijf dat op logerenbijvlamingen te huur wordt aangeboden door de eigenaar of verhuurder.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Adverteerder: Eigenaar of verhuurder van een accommodatie die op deze site te huur aangeboden wordt.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Huurder: Persoon die een accommodatie huurt van een adverteerder/eigenaar op deze site.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Huurprijs: De totale huursom die bij de huurder in rekening wordt gebracht. Reserveringskosten, huur van linnenpakketten, eindschoonmaak en dergelijke worden ook geacht tot de totale huur te behoren zulks met uitsluiting van de waarborgsom.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Reservering: &nbsp;Een reservering komt tot stand nadat de verhuurder/adverteerder aan logerenbijvlamingen.com heeft aangegeven de aanvraag tot reservatie van een huurder te zullen accepteren.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Provisie: De provisie is een vast percentage dat over de huur in rekening wordt gebracht als vergoeding voor het plaatsen van de accommodatie op de website.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Incasso: De provisie die de verhuurder uit hoofde van geaccepteerde boekingen verschuldigd is wordt automatisch per reservatie aan de verhuurder middels een factuur kenbaar gemaakt.&nbsp;<br>De verhuurder is gehouden deze factuur binnen 14 dagen te betalen. Betaling kan door middel van paypal, sofort banking of via overschrijving.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">kalender: De adverteerder/eigenaar is verplicht om zijn kalender op logerenbijvlamingen.com actueel houden, bij nalatigheid kan de advertentie verwijderd worden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Meerdere accommodaties : &nbsp;Indien een adverteerder over meerdere accommodaties beschikt dan kan in één advertentie slechts één accommodatie worden aangeboden en als zodanig dienen de huurprijzen, omschrijvingen, verhuurkalenders, foto\'s overeen te komen met deze ene accommodatie. Het is niet toegestaan om meerdere accommodaties voor de prijs van één aan te bieden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Klachten van huurder: Wij zullen klachten over een accommodatie nooit zelf inhoudelijk behandelen of optreden als bemiddelaar tussen betrokken partijen. Als een klacht niet behandeld wordt door de verhuurder heeft logerenbijvlamingen.com het recht om een advertentie te verwijderen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Verlengen betaalde advertentie: Vaste abonnementen worden niet automatisch verlengt. 28 en 14 dagen voordat uw abonnement afloopt, ontvangt u een e-mail van logerenbijvlamingen.com om u op de hoogte te brengen en de vraag of u uw abonnement wil verlengen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">BTW: logerenbijvlamingen.com is gevestigd in Frankrijk en ingeschreven als auto-entrepreneur en vrijgesteld van BTW. Dit zal vermeldt staan op alle facturen als "BTW niet van toepassing volgens artikel 293 B van het CGI." U als adverteerder kan dan ook geen BTW terugvorderen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 1. Gebruik van de website</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Bij het gebruik van deze website dient er gehoorzaamd te worden aan de Algemene Voorwaarden. U mag deze website enkel gebruiken voor het plaatsen, opzoeken, beoordelen en contacteren van accommodaties en diens eigenaren. Indien er enige vorm van wederrechtelijke praktijken ontdekt wordt, is het verbannen of opstarten van een rechtelijke procedure een mogelijke en gerechtigde vorm van reactie of sanctie. Daarbij wordt u verondersteld niet enkel deze voorwaarden na te leven, maar elke andere Europese en niet-Europese wet te handhaven. Wij kunnen niet verantwoordelijk gesteld worden voor wederrechtelijke acties of praktijken door onze gebruikers.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 2. Regels voor het plaatsen van een advertentie</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"=""><br>Logerenbijvlamingen.com is uitsluitend en nadrukkelijk bestemd voor particuliere eigenaren van vakantie accommodaties, die hun accommodatie willen aanbieden en verhuren op Logerenbijvlamingen.com.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Het is alleen voor Nederlandstalige verhuurders mogelijk om een advertentie te plaatsen op Logerenbijvlamingen.com. Mocht Logerenbijvlamingen.com een advertentie tegenkomen in een andere taal dan het Nederlands of Logerenbijvlamingen.com heeft het vermoeden dat de adverteerder/verhuurder de Nederlandse taal niet machtig is, dan plaatst Logerenbijvlamingen.com deze advertentie offline. Vervolgens zal Logerenbijvlamingen.com de adverteerder/verhuurder per e-mail aanschrijven. Mocht de adverteerder/verhuurder niet in staat blijken in correct Nederlands zijn advertentie te plaatsen en te beheren, dan wordt de advertentie definitief verwijderd en het abonnement beëindigd. Abonnementskosten zullen in dit geval niet worden terugbetaald</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De titel en de tekst van een advertentie moeten juist en helder worden omschreven en mogen dan ook niet onjuist of misleidend zijn. Foto’s of video’s bij een advertentie moeten betrekking hebben op de aangeboden accommodatie of direct daar mee verband houden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com heeft het recht om de door de adverteerder/verhuurder geplaatste teksten, afbeeldingen en andere gegevens geheel of gedeeltelijk aan te passen of te weigeren.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 3. Gegevens bij registratie</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Bij de registratie om toegang te krijgen tot ons klantenpaneel en diens diensten, wordt u gevraagd enkele persoonlijke gegevens in te vullen. Wij verwachten van u dat dit waarheidsgetrouwe gegevens zijn, daar deze op elke factuur gebruikt zullen worden. Indien deze gegevens foutief zijn, berust dit volledig op de verantwoordelijkheid van de verhuurder of huurder zelf en wordt dit bij eventuele fiscale of juridische problemen gemeld.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 4. Toevoegen van zelfgegenereerde inhoud</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Op deze website kan de gebruiker/adverteerder op allerlei mogelijke manieren inhoud toevoegen aan de website. Dit bedraagt zowel visueel materiaal als text. Steeds draagt de gebruiker zelf de verantwoordelijkheid om correcte, duidelijke en legale informatie te verstrekken. Indien de gebruiker zich niet houdt aan bij wet vastgelegde standaarden of eisen, wordt niet deze website, maar de gebruiker zelf rechtelijk vervolgd. Wij zijn niet verplicht om elke mogelijke vorm van inhoud te controleren, daar de gebruiker zelf dat hoort te doen. Als er toch wederrechtelijke of ethisch onverantwoorde informatie teruggevonden wordt, zal de gebruiker zelf daarop aangesproken worden en kunnen er eventueel juridische maatregelen getroffen worden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 5. Abonnementen</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Er zijn 2 type abonnementen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">A)&nbsp;<u>No cure no pay abonnement</u>:</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Wanneer u een no cure no pay abonnement afsluit, wordt per geaccepteerde reserveringsaanvraag een commissie van 10% berekend over de totale huursom exclusief borg en toeristenbelasting. Er zijn geen verdere abonnementskosten verschuldigd.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Het is niet toegestaan om uw contactgegevens zoals emailadres/telefoonnummer/URL naar uw eigen website bij een "no cure no pay" advertentie op logerenbijvlamingen.com te vermelden. Dit is alleen toegestaan indien u het vast abonnement heeft afgesloten.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Het is eveneens niet toegestaan om uw contactgegevens zoals e-mailadres en/of telefoonnummer en/of URL naar uw eigen website, bij het beantwoorden van een contactaanvraag te vermelden. De communicatie wordt altijd gevolgd door logerenbijvlamingen.com. Mocht er bij controle een e-mailadres, telefoonnummer en/of URL/link tegenkomen dan heeft logerenbijvlamingen.com het recht een sanctie van € 100 op te leggen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com controleert elke advertentie regelmatig op deze regels en behoudt zich het recht voor dergelijke verwijzingen te verwijderen. Na meerdere inbreuken van deze regels heeft Logerenbijvlamingen.com het recht de advertentie en de adverteerder te verwijderen van de website.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Indien een reserveringsaanvraag wordt afgewezen door de verhuurder zonder geldige reden, wordt er € 25 annuleringskosten in rekening gebracht. Indien de verschuldigde commissie van de afgewezen boekingsaanvraag lager is dan de annuleringskosten wordt er 10% commissie in rekening gebracht.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Bij een niet beantwoorde reserveringsaanvraag wordt er eveneens € 25 annuleringskosten in rekening gebracht. Indien de verschuldigde commissie van de verlopen boekingsaanvraag lager is dan de annuleringskosten wordt er 10% commissie in rekening gebracht.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Onder “reserveringsaanvraag” wordt het volgende verstaan: een potentiële huurder verstuurt het reserveringsformulier via de kalender in de advertentie van de verhuurder en plaatst zo de reserveringsaanvraag bij de verhuurder</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Een reserveringsaanvraag aan de verhuurder is bindend. Verhuurders zijn verplicht zo spoedig mogelijk, uiterlijk binnen 3 dagen na verzending van een reserveringsaanvraag door een huurder, te reageren.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Mocht een reserveringsaanvraag geannuleerd worden door een reden buiten de verhuurder om, dan dient de verhuurder dit zo snel mogelijk te melden aan logerenbijvlamingen.com via info@logerenbijvlamingen.com. Als de verhuurder met bewijsmateriaal (voorbeeld een e-mail van de huurder) kan aantonen dat de schuld niet bij de verhuurder ligt zal logerenbijvlamingen.com geen annulatie kosten aanrekenen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Van een valse annulering is sprake indien de adverteerder/verhuurder al dan niet in samenwerking met de huurder of andere partijen aan logerenbijvlamingen.com laat weten dat de verhuur door een huurder is geannuleerd terwijl dit niet zo is. Van een valse annulering is ook sprake indien er wordt geannuleerd en vervolgens op andere voorwaarden en/of een andere huurtermijn een huurovereenkomst met betrokken partijen wordt aangegaan. Indien er sprake is van een valse annulering dan is de adverteerder/verhuurder terstond alsnog de overeengekomen provisie verschuldigd vermeerderd met een direct opeisbare boete gelijk aan 5 keer de overeengekomen provisie. Aangezien de handelingen van de huurder bij een valse annulering gelijk worden gesteld met fraude en oplichting is een wettelijke ingebrekestelling niet noodzakelijk. Vervolgens zal de advertentie van de website worden verwijderd en zal bij de politie in de vestigingsplaats van logerenbijvlamingen.com en/of in de woonplaats van de adverteerder/verhuurder of de plaats waar de woonplaats onder ressorteert aangifte bij de politie dan wel de gendarmerie worden gedaan en/ of bij overige instanties.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De looptijd van een advertentie op basis van provisie is in principe onbeperkt en er is geen opzegtermijn. De advertentie kan slechts worden beëindigd wanneer alle lopende informatieverzoeken, reserveringen, opties etc. zijn afgerond en nadat door de adverteerder alle provisies zijn betaald.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;B)&nbsp;<u>Vast Abonnement</u>:</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Wanneer u een vast abonnement afsluit wordt er geen commissie per reserveringsaanvraag in rekening gebracht. De abonnementskosten bedragen op dit ogenblik € 50 per jaar.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">U kan uw abonnement altijd nog aanpassen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Het abonnement wordt niet automatisch verlengt.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;Artikel 6. Beoordeling door derden</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Indien derden bepaalde gegevens of geplaatste inhoud van andere gebruikers beoordelen, dienen deze eerst na te denken over wat ze schrijven en zijn ze gebonden aan voorwaarden of eisen opgenomen in deze verklaringen of wettelijke bronnen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;Artikel 7. Andere wederrechtelijke acties</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Als gebruikers andere wederrechtelijke acties plegen, zich niet houden aan elders vermelde regels of schade aan de website leveren, worden er steeds maatregelen getroffen, al dan niet op rechtelijke basis.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;Artikel 8. Inzake ongehoorzaamheid</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Bij het niet naleven van bovenstaande verklaringen en eisen door de gebruikers van deze website, zijn wij genoodzaakt daar steeds maatregelen tegen te nemen. Deze maatregelen vallen niet te betwisten door de gebruiker in geval van duidelijke bewijzen. We hebben steeds het recht, zelfs de plicht, om illigale of wederrechtelijke praktijken te melden en eventueel voor te brengen bij rechtelijke instanties.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 9. Uitsluitingen en beperkingen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Volgende acommodaties op de website van Logerenbijvlamingen.com komen&nbsp;<u>niet</u>&nbsp;als verhuuradvertenties in aanmerking:</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">de adverteerder weigert of nalaat de beschikbaarheid van de accommodatie actueel te houden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">de adverteerder door middel van zijn advertentie en/of gedragingen ons slechte publiciteit bezorgt.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Indien op enig moment blijkt dat de advertentie niet voldoet aan bovengenoemde voorwaarden dan kan de advertentie van de website worden verwijderd. Dit geldt ook wanneer er door huurders en/of andere belanghebbenden klachten worden ingediend en deze klachten terecht blijken te zijn. Hierbij willen wij uitdrukkelijk vermelden dat wij eventuele klachten over de advertentie en/of de accommodatie niet inhoudelijk zullen behandelen noch zullen optreden als bemiddelaar.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 10.&nbsp;<em>Het recht van weigeren, wijzigen en verwijderen van advertenties</em></p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com heeft het recht om advertenties te weigeren of te verwijderen. Logerenbijvlamingen.com heeft ook het recht om advertenties aan te passen indien de inhoud niet strookt met onze algemene voorwaarden en overige bepalingen.&nbsp;<br>Dit artikel geldt ook wanneer de inhoud van de advertentie beledigende en/of kwetsende teksten bevat en/of naar algemeen maatschappelijk aanvaarde normen niet acceptabele inhoud bevat. In geval van verwijdering is Logerenbijvlamingen.com niet aansprakelijk voor enige vorm van schade die door de verwijdering zou zijn veroorzaakt of zal worden veroorzaakt.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 11 Toegang account en data Verhuurder</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"=""><br>Voor beheers- en configuratiedoeleinden en voor de financiële afwikkeling van een transactie en boekingsaanvraag, heeft Logerenbijvlamingen.com toegang tot het account van de adverteerder op de Website.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com is niet aansprakelijk voor schade ontstaan doordat derden zich onbevoegd toegang verschaffen (hacken) tot de account van de Verhuurder.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De adverteerder is zelf verantwoordelijk voor zijn data en Content. Logerenbijvlamingen.com is niet aansprakelijk voor schade voortvloeiend uit het verlies van (een deel) van deze data.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De adverteerder en huurder zijn zelf verantwoordelijk voor het zorgvuldig bewaren en beheren van de door Logerenbijvlamingen.com aan hen verstrekte gebruikersnamen en wachtwoorden voor hun account. Logerenbijvlamingen.com accepteert geen enkele aansprakelijkheid voortvloeiend uit het (al dan niet onzorgvuldige) gebruik van deze gegevens.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 12.&nbsp;<em>Beperking van aansprakelijkheid</em></p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com host haar website bij een gerenommeerd bedrijf met 99% uptime garantie. Logerenbijvlamingen.com is echter bij een storing of uitval van de website of niet goed functioneren van de website op geen enkele wijze aansprakelijk voor alle vormen van schade die hierdoor gelijk of later zouden kunnen optreden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com is evenmin aansprakelijk voor de gedragingen, handelen, tekortkomingen en dergelijk van een huurder in de breedst mogelijke context.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 13. &nbsp;<em>Onderhouden van de advertentie</em></p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De adverteerder/verhuurder is verplicht om de verhuurkalender actueel te houden. Het is voor ons en voor een huurder enorm vervelend om achteraf te moeten horen dat een accommodatie niet meer beschikbaar blijkt te zijn maar dat op de website de betreffende huurperiode als vrij te boek staat. Indien blijkt dat de kalender onjuiste informatie bevat, dan heeft Logerenbijvlamingen.com de mogelijkheid de advertentie te verwijderen</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Bij totstandkoming van een overeenkomst van opdracht tussen Logerenbijvlamingen.com en adverteerde/verhuurder dient de adverteerder/verhuurder ten allen tijde zorg te dragen voor een werkend e-mail adres welke frequent wordt gelezen. De adverteerder/verhuurder staat in dat het door hem opgegeven e-mail adres werkt en frequent wordt gelezen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 14.&nbsp;<em>Nietigheid</em></p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Indien één of meerdere van de bepalingen in deze algemene voorwaarden nietig wordt geoordeeld, heeft dit geen gevolg voor de geldigheid van de overige bepalingen van deze algemene voorwaarden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 15.&nbsp;<em>Welk recht is van toepassing</em></p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Enkel de afdelingen van de rechtbanken of het vredegerecht bevoegd voor de plaats van de uitbatingszetel van logerenbijvlamingen.com zijn bevoegd in geval van een geschil.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Artikel 16. Inzake wijziging van de algemene voorwaarden en tarieven</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com mag de algemene voorwaarden op elk tijdstip wijzigen. Geregistreerde gebruikers (adverteerders en huurders) worden hier steeds van op de hoogte gebracht via mail of andere communicatiekanalen. Niet-geregistreerde gebruikers moeten zich steeds houden aan de actuele versie van de Algemene Voorwaarden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">De nieuwe versie van de algemene voorwaarden is altijd van toepassing op reserveringsaanvragen en iedere overeenkomst tussen logerenbijvlamingen.com en Bezoekers, Huurders en Verhuurders</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Logerenbijvlamingen.com is gerechtigd haar tarieven te wijzigen. Wijzigingen van de provisie percentage worden schriftelijk of per e-mail bekendgemaakt en treden 7 dagen na bekendmaking in.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Wijzigingen van prijzen van vaste abonnementen kan ten allen tijde gebeuren zonder verwittiging.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Deze wijziging geldt uiteraard niet voor lopende vaste abonnementen maar voor alle nieuwe afgesloten abonnementen.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Vragen? Stel ze aan ons!</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">Indien er nog vragen zijn over deze voorwaarden, of andere juridische maatregelen, kan u steeds contact opnemen via info@logerenbijvlamingen.com. Uw vragen zullen dan zo snel mogelijk verwerkt worden.</p><p style="color: rgb(0, 0, 0);" source="" sans="" pro",="" "helvetica="" neue",="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-weight:="" 400;="" widows:="" 2;"="">&nbsp;</p></h1>', NULL, '2018-04-10 17:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT '1=admin,2=customer,3=advertiser',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_company` varchar(100) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `phone_number` varchar(13) NOT NULL,
  `street` varchar(50) DEFAULT NULL,
  `street_number` int(11) DEFAULT NULL,
  `post_code` int(11) DEFAULT NULL,
  `btw_number` varchar(50) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `provider` varchar(200) DEFAULT NULL,
  `provider_id` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `user_company`, `country_id`, `city`, `phone_number`, `street`, `street_number`, `post_code`, `btw_number`, `remember_token`, `provider`, `provider_id`) VALUES
(1, 1, '2018-02-03 19:34:27', '2018-02-10 06:17:54', 'Admin', 'sharma', 'gauravshivamrules@gmail.com', '$2y$10$owUu80ICXsRpLNic6jzACulFzL5MV0qKCW8b/7rw6J7IG3dURnTQq', NULL, 11, NULL, '1234567890', NULL, NULL, NULL, NULL, '9ufVStw2d6guDqcetZNxWTf51y4NxYUyF4MmXInqsLHcrEnpQxVqViM8lcqa', NULL, NULL),
(2, 3, '2018-02-03 19:52:00', '2018-02-28 19:06:45', 'Advertiser', 'testing', 'gauravshivamrules@yahoo.in', '$2y$10$owUu80ICXsRpLNic6jzACulFzL5MV0qKCW8b/7rw6J7IG3dURnTQq', 'my company', 7, 'city', '1234567890', NULL, 123, NULL, NULL, 'RgFROvEdhfZ4WKkdbPU6dGJn0Ep29lliIIyPSIlx7wJUVEPbpukwqdyyBhcm', NULL, NULL),
(14, 2, '2018-02-09 18:50:02', '2018-02-09 19:56:22', 'Customer', 'Testing', 'gauravshivamrules@hotmail.com', '$2y$10$owUu80ICXsRpLNic6jzACulFzL5MV0qKCW8b/7rw6J7IG3dURnTQq', NULL, 10, NULL, '1234567890', NULL, NULL, NULL, NULL, 'V79NGxoQhM5FVUYjwmgCoB3OWLBoxW8sUKhDR8nHZQgHcy355Oy1MMld2Rx6', NULL, NULL),
(23, 1, '2018-02-10 07:32:42', '2018-02-10 07:32:42', 'Joeri', 'De Bonnaire', 'joeridebonnaire@gmail.com', '$2y$10$qBe6I9w8bj8eSpTtllHTHOV5sh7fwJb6y7d33Lh66mWd4seH8KL0C', NULL, 3, 'Feugarolles', '0033553483407', 'Lencouet', NULL, 47230, NULL, 'bDUfDCRaGqRBk3C3Qnh1dSJcic8c4hLWoR9iEdDumgcjQcZTQ5hZFd3C69mK', NULL, NULL),
(24, 3, '2018-02-10 07:34:47', '2018-02-16 10:20:07', 'Brigitte', 'De Bonnaire', 'lencouet@gmail.com', '$2y$10$n5u1QXrxty4/CTpZR/hna.dRz2BLrM3OGDt2XcvLU52USxJnYznsq', NULL, 3, 'Feugarolles', '0033553483407', 'Lencouet', NULL, 47230, NULL, 'jbNettS7dDLHHMaPSN7LoF1XCVnq4W0ehwpiJO3uHyrpOtmuy7Zi63G2FHSF', NULL, NULL),
(25, 2, '2018-02-11 10:01:33', '2018-02-11 10:01:33', 'Joe', 'DB', 'joeridebonnaire@hotmail.com', '$2y$10$qm8ii4ImLkzCvD3j8kUeleBakOhDxvyxAFimwcdDvY0JWTORsTxnS', NULL, 3, 'Feugarolles', '1234567890987', 'Lencouet', NULL, 47230, NULL, 'y3nATVpQWi8s3JNIGvsQVvX3UrduZhxRVqWOWMCPQYelkU2HFUzSEqr0Y4Sw', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_number` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `label_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `address`, `street`, `street_number`, `city`, `zip`, `country_id`, `country`, `region_id`, `department_id`, `label_id`, `created_at`, `updated_at`) VALUES
(1, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(2, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(3, '', 'Ramal das Medas', '', 'Arganil - Coja', '3305/183', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(4, '', 'le maine', '', 'belves', '24170', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(5, '', 'Route de Sauveclare', '2355', 'Flayosc', '83780', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(6, '', 'chemin bas des fa�sses', '246', 'Regusse', '83630', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(7, '', 'Le Village', '16', 'Seynes', '30580', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(8, '', 'coumerme', '1', 'montauriol', '66300', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(9, '', 'Quartier Les grand pres', '44', 'Roaix', '84110', NULL, 'Vaucluse', 0, 0, NULL, NULL, NULL),
(10, '', 'VALE DA VILA CASA ROBEJA', '', 'SILVES', '8200-371', 4, 'portugal', 0, 0, NULL, NULL, NULL),
(11, '', 'route de champdeniers', '', 'Aug', '79400', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(12, '', 'Carretera los Arcos-Algar ', ' km 4.5', 'Arcos de la Frontera', '11630', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(13, '', 'Chabrat', '', 'Liginiac', '19160', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(14, '', 'los olivas', '', 'sedella', '29715', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(15, '', 'Les Fayes', '', 'Goutti', '63390', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(16, '', 'La Faye', '', 'Vertolaye', '63480', NULL, 'Frakrijk', 0, 0, NULL, NULL, NULL),
(17, '', 'Le Bois Berranger', '1', 'Saint Urbain- Vendee', '85230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(18, '', 'Route des Iles - lieu dit Isle Basse', '-', 'Fontanes', '46230', 3, 'France', 0, 0, NULL, NULL, NULL),
(19, '', 'Villaine den bas', '', '03190 Verneix dep Allier', '03190', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(20, 'Allemagne en PROVENCE', 'Impasse du Bosquet ( les moullières)', '', 'Allemagne en PROVENCE', '04500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(21, '', 'chemin de galinier ', '821', 'avejan', '30430', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(22, '', 'La Serre', '', 'St Victor et Melvieu', '12400', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(23, '', 'pago esparraguerras', '95', 'Competa', '29754', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(24, 'La Caunette', 'Grand Rue, Hameau de Vialanove', '5', 'La Caunette', '34210', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(25, '', 'Canadalaan', '36', 'ST JOB', '2960', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(26, '', 'Moulin Haut', '', 'Rouffignac St.Cernin', '24580', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(27, '', 'route de saint aignan', '145', 'couddes', '41700', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(28, '', 'Rue du Rechandet', '29', 'Ougney La Roche', '25640', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(29, '', 'Faubourg Saint Martin', '88', 'Le Caylar', '34520', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(30, '', '744 chemin du rialet', '', 'Entraigues', '84320', NULL, 'vaucluse', 0, 0, NULL, NULL, NULL),
(31, '', 'lieu-dit Figuier', 'D107E', 'Vacqui', '34270', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(32, '', 'Lieu dit La Poujade', '', 'Caylus', '82160', 3, 'France', 0, 0, NULL, NULL, NULL),
(33, '', 'Via Palagio', '52', 'Montaione', 'I- 50050', NULL, 'Italia', 0, 0, NULL, NULL, NULL),
(34, '', 'vocabolo acquavigna ', '3', 'poggiolo', '05032', NULL, 'italia', 0, 0, NULL, NULL, NULL),
(35, '', 'Impasse les Maisons Neuves', '85', 'Montret', '71440', 3, 'France', 0, 0, NULL, NULL, NULL),
(36, '', 'casal dos custodios ', '2', 'salir do porto', '2500-658', 4, 'portugal', 0, 0, NULL, NULL, NULL),
(37, '', 'Avenue De Plaissan - Lieu dit Saint Marcel', '', 'Saint Pargoire PEZENAS', '34230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(38, '', 'Larocal', 'D19E', 'sainte sabine born', '24440', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(39, '', 'Via Montalfoglio', '', 'San Lorenzo in Campo', '61047', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(40, '', 'LE CASTANT', '1', 'CENDRIEUX', '24380', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(41, '', 'Sitio dos Lombos', ' CP 198x', 'Lagoa', '8400-411', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(42, '', 'Chemin de la digue', '36', 'istres', '13800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(43, '', 'Impasse du Presbytere', '5', 'Greffeil', '11250', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(44, '', 'Route D\\\\\\\'Escoussans', '', 'Arbis', '33760', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(45, '', 'lieu-dit Lard', '', 'Flaugnac', '46170', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(46, '', 'Vocabolo Belvedere', '97A', 'Todi Casemasce', '06059', NULL, 'Itali', 0, 0, NULL, NULL, NULL),
(47, '', 'Solana 11', 'B 20/34', 'Hondon De Las Nieves', '03688', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(48, '', 'Le Village', '1', 'Fabas', '31230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(49, '', 'Paisos Catalans', '4', 'Sant Dalmai (Gerona)', '17183', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(50, '', 'Mas des roches', '', 'Labastide De Virac', '07150', 3, 'France', 0, 0, NULL, NULL, NULL),
(51, '', 'LE BOURG', '', 'MENIL-HERMEI', '61210', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(52, '', 'Chateau de Vareilles', '1', 'Sommant', '71540', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(53, '', 'Barran', '', 'Montbernard', '31230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(54, '', 'rue de bellevue', '9', 'La Péruse', '16270', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(55, '', 'La Mouyssarie', '', 'Mauzens et Miremont', '24260', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(56, '', 'LD Jean Blanc', '1', 'Mauvezin-sur-Gupie', '47200', 3, 'France', 0, 0, NULL, NULL, NULL),
(57, '', 'LES GOUTS', '1', 'LE BRETHON', '03350', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(58, 'Buis-les-Baronnies', 'Route de Propiac', '1375', 'Buis-les-Baronnies', '26170', 3, 'France', 0, 0, NULL, NULL, NULL),
(59, '', 'Kruishoutemstraat ', '42', 'Zulte', '9870', NULL, 'O/V', 0, 0, NULL, NULL, NULL),
(60, '', 'fraz Spagna', ' 9', 'Montecalvo Versiggia', '27047', NULL, 'info@duepadroni.it', 0, 0, NULL, NULL, NULL),
(61, '', 'DEPARTEMENTALE', '938', 'RUE', '80120', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(62, '', 'pourret', '-', 'castelmoron sur lot', '47260', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(63, '', 'les Charrauds de Bronzeau', '9', 'Saint-Léger-Magnazeix', '87190', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(64, '', 'Le Bosc', '', 'Les Assions', '07140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(65, '', 'LAPALOUP', '', 'COUPIAC', '12550', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(66, '', 'le haut', '{number}', 'serley', '71310', 3, 'france', 0, 0, NULL, NULL, NULL),
(67, '', 'Route de Caromb ', '1859', 'Carpentras', '84200', 3, 'France', 0, 0, NULL, NULL, NULL),
(68, '', 'Le Viala', '', 'St Frézal de Ventalon', '48240', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(69, '', 'Av.des Acacias', '53', 'Menton', '06500', 3, 'France', 0, 0, NULL, NULL, NULL),
(70, '', 'BEELDHOUWERSSTRAAT', '30/1', 'GENT', '9040', 1, 'BELGIË', 0, 0, NULL, NULL, NULL),
(71, '', 'Garara', '', 'Wadi Musa', '71810', NULL, 'Jordanie', 0, 0, NULL, NULL, NULL),
(72, '', 'Chemin du Long Pont', '951', 'Lagnes', '84800', 3, 'France ', 0, 0, NULL, NULL, NULL),
(73, '', 'Pierneef Street', '7', 'Somerset West', '7130', NULL, 'Zuid-Afrika', 0, 0, NULL, NULL, NULL),
(74, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(75, '', 'Vocabolo Vallata ', '27', 'Valfabbrica , Perugia', '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(76, '', 'Jalan Pura Gunung Sari ', '4', 'Peliatan (Ubud) - Bali', '80571', 31, 'Indonesië', 0, 0, NULL, NULL, NULL),
(77, '', 'Redaweg ', '57a', 'St Michiel', '0000', 8, 'Curaçao', 0, 0, NULL, NULL, NULL),
(78, '', 'STR COM CANNELLA-RONCITELLI', '26', 'RONCITELLI DI SENIGALLIA', '60019', NULL, 'Italië	', 0, 0, NULL, NULL, NULL),
(79, '', 'Route de la Jaurezie', '{number}', 'Lempaut', '81700', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(80, '', 'Markt', '245', 'Mauterndorf im Lungau', '5570', 6, 'Oostenrijk', 0, 0, NULL, NULL, NULL),
(81, '', 'Largo da Portela', '', 'Marinha da Mendiga', '2480-212 ', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(82, '', 'Travessa das Faias', '8', 'Souto De Cima Leiria', '2410 - 131', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(83, '', 'NATURE RESERVE GRIETJIE, 22', '', 'Phalaborwa', '1390', NULL, 'Limpopo', 0, 0, NULL, NULL, NULL),
(84, '', 'vézouillac', 'lieu-dit', 'verrieres', '12520', 3, 'france', 0, 0, NULL, NULL, NULL),
(85, '', 'Strada Fratticiola Selvatica-Piccione', '3', 'Fratticiola Selvatica (PG)', '06134', NULL, 'Italia', 0, 0, NULL, NULL, NULL),
(86, '', 'Hay Kenaria Derb Boutouil', '79', 'Marrakech', '40040', 22, 'Marokko', 0, 0, NULL, NULL, NULL),
(87, '', 'Rua Da Capela', '5', 'Póvoa (Cadima)', '3060-110', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(88, '', 'lieu dit', '1', 'Millau (Saint Georges de Luzençon)', '12100', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(89, '', 'Via Pelleria ', '11', 'Lucca', '55100', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(90, '', 'Contrada  Serrone', '3', 'Montelparo', '63853 (FM)', 7, 'Italie ', 0, 0, NULL, NULL, NULL),
(91, '', 'Calle Cierzos y Cabreras ', '6', 'Iznajar', '14970', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(92, '', 'Ctra de mijas s/n', '', 'Co', '29100', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(93, '', 'don ramon', '17', 'malaga', '29568', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(94, '', 'Cami de la Moneda,ramal 5a', '', 'Ontinyent - Valencia', '46870', NULL, 'Espana', 0, 0, NULL, NULL, NULL),
(95, '', 'Camino de la sal', '42', 'Albatera', '03340', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(96, '', 'Plaza Asegador y Calistro', '38', 'Benitachell', '03726', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(97, '', 'Pandols, Poligono 4,Parcel', '98', 'Calig', '12589', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(98, '', 'CARRER MAYOR', '16', 'VALL DE GALLINERA', '03788', 5, 'SPANJE', 0, 0, NULL, NULL, NULL),
(99, '', 'LACHANIA', '43', 'LACHANIA', '85109', NULL, 'GREECE', 0, 0, NULL, NULL, NULL),
(100, '', 'Acacialaan 39 - 9840 De Pinte', '', 'De Pinte', '9840', 1, 'België', 0, 0, NULL, NULL, NULL),
(101, '', 'chemin des violettes', '5', 'Louzignac', '17160', 3, 'france', 0, 0, NULL, NULL, NULL),
(102, '', 'Moulin de Sevoux', '', 'Malétable', '61290', NULL, 'Franrkijk', 0, 0, NULL, NULL, NULL),
(103, '', 'Largo da Eira Velha', '5', 'Carvalho / Penacova', '3360-034', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(104, '', 'Ld Majoulassie', '', 'Gavaudun', '47150', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(105, '', 'Le Cassan', '1', 'Lescure Jaoul', '12440', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(106, '', 'pin bernard', '', 'salernes', '83690', 3, 'France', 0, 0, NULL, NULL, NULL),
(107, '', 'Priesterstraat', '35', 'Poperinge', '8970', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(108, '', 'Le Raccourci Antraigues', '1', 'Vals les Bains', '07600 ', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(109, '', 'rue Principale (Moulin-le-Comte)', '44', 'Aire-sur-la-Lys', '62120', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(110, 'Vauquois', 'Rue du Général Deprez', '3', 'Vauquois', '55270', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(111, '', 'Ady Endre ut', '16', 'Tiszaderzs', '5243', 34, 'Hongarije', 0, 0, NULL, NULL, NULL),
(112, '', 'Waterval ', '12', 'Sint Pietersvoeren ', '3792', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(113, '', 'Les Varaizes', '', 'Beaune D\'Allier', '03390', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(114, '', 'Pastoor Goossenslaan', '39', 'Uikhoven', '3631', 1, 'België', 0, 0, NULL, NULL, NULL),
(115, '', 'Via Valsermenza', '4', 'ROSSA', '13020', NULL, 'Italië	', 0, 0, NULL, NULL, NULL),
(116, '', 'Rue du paradis', '1', 'Tauves', '63690', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(117, '', 'TER VARENTSTRAAT', '', 'MORTSEL', '2640', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(118, '', 'Cortijo Lasnavillasmm, carreetera de Illora', 'sn', 'Montefrio', '18270', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(119, '', ' Lencouet', ' ', ' Feugarolles', ' 47230', NULL, ' Frankrijk', 0, 0, NULL, NULL, NULL),
(120, '', 'Sandbakken', '', 'Flatraket', '6717', NULL, 'Noorwegen', 0, 0, NULL, NULL, NULL),
(121, '', 'Urbanizaci', '164', 'Carmona-Sevilla', '41410', NULL, 'Espa', 0, 0, NULL, NULL, NULL),
(122, '', 'rue du 11 novembre', '', 'Mezin', '47170', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(123, '', 'Nederenamestraat', '153', 'Nederename', '9700', 1, 'België', 0, 0, NULL, NULL, NULL),
(124, '', 'moulin de peychenval', '', 'Lamonzie Montastruc', '24520', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(125, '', 'Steenovenstraat ', '111', 'Essen Wildert', '2910', NULL, 'Belgium', 0, 0, NULL, NULL, NULL),
(126, '', 'Günninghauser Strasse', '30', 'Winterberg', ' 59955', 11, 'Duitsland', 0, 0, NULL, NULL, NULL),
(127, '', 'Chablat', '', 'Saint-Martin-Cantalès', '15140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(128, 'IZNAJAR', 'Valenzuela Y Llanadas 22', '22', 'IZNAJAR', '14979', 5, 'SPANJE', 0, 0, NULL, NULL, NULL),
(129, '', 'Lieu-dit Loste', '', 'Beauville', '47470', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(130, '', 'Rue Du Dolmen', '90', 'St alban auriolles', '07120', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(131, '', 'Caixa Postal', '003', 'Conde', '48300-000', NULL, 'Brazili', 0, 0, NULL, NULL, NULL),
(132, '', 'stavros akrotiri', '', 'chania', '73100', NULL, 'crete', 0, 0, NULL, NULL, NULL),
(133, '', 'Pastoor Goossenslaan', '39', 'Uikhoven', '3631', 1, 'België', 0, 0, NULL, NULL, NULL),
(134, '', 'Barriada García Agua', '32', 'Estación de Cártama', '29580', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(135, '', 'Avenue de la gare', '2', 'Faugères', '34600', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(136, '', 'DOMAINE DES LAUNES', 'D558', 'LA GARDE-FREINET', '83680', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(137, '', 'via della Pace 6 - loc. Arcille', '6', 'Campagnatico', '58042', NULL, 'Italia', 0, 0, NULL, NULL, NULL),
(138, '', 'Calle Piano - Reparto Tavares', '1', 'Sosua / Puerto Plata', '57000', 19, 'Dominicaanse Republiek', 0, 0, NULL, NULL, NULL),
(139, '', 'Vignal', '0', 'Monflanquin', '47150', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(140, '', 'Poco das Figueiras, Apartado ', '287', 'Moncarapacho ', '8701-906', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(141, '', 'rue paul riquet 6', '', 'le somail/ginestas', '11120', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(142, '', 'Rue du Marché', '', 'Besse et Saint Anastaise', '63610', 3, 'France', 0, 0, NULL, NULL, NULL),
(143, '', 'Les Filloux', '2', 'Saint Dizier-Leyrenne', '23400', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(144, '', 'Rue de Chavenne', '1401', 'La Chapelle Saint Sauveur', '71310', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(145, '', 'couvent de Tavagna', '', 'Pero-Casevecchie', '20230', NULL, 'France  (corsica)', 0, 0, NULL, NULL, NULL),
(146, '', 'apartado', '7', 'Boliqueime', '8100-070', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(147, '', 'Rue des Ecoles  - La Levade', '30110', 'La Grand Combe', '30110', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(148, '', 'St.barbarastraat', '19', 'Diepenbeek', '3590', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(149, '', 'Noordhoekstraat', '58', 'De Panne', '8660', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(150, '', 'Apartado de correos', '39', 'Camarles-L\'Ampolla', '43895', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(151, '', 'hageveldstraat', '38A', 'denderleeuw', '9470', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(152, '', 'kortrijk', ' ', 'dadizele', '8890', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(153, '', 'RUE MONTLAUR', '6', 'caunes-minervois', '11160', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(154, '', '122 RUE BORGNE', 'Les Clos de Guéniers', 'Jausiers', '04850', NULL, 'Provence-Alpes-Côte d\\\\\\\'Azur', 0, 0, NULL, NULL, NULL),
(155, '', 'Bellestraat', '213A', 'Affligem', '1790', 1, 'België', 0, 0, NULL, NULL, NULL),
(156, '', 'Bairro Antonio Maria Dinis 50', '', 'Povoa de Midoes - Tabua', '3420-201', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(157, '', 'moerasrivier ', '233/97', 'george', '6530', NULL, 'Zuid-Afrika', 0, 0, NULL, NULL, NULL),
(158, '', 'Sint Annastraat', '50A', 'Overijse', '3090', 1, 'België', 0, 0, NULL, NULL, NULL),
(159, '', 'Lieudit le risdoux', '1', 'Vireux-Wallrand', '08320', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(160, '', 'nachtegalendreef', '14', 'halle', '2980', NULL, ' belgie', 0, 0, NULL, NULL, NULL),
(161, '', 'Horta dos Revoredos, Barrada', '', 'Monsaraz (Évora)', '7200-172', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(162, '', 'smatevz', '49', 'gomilsko', '3303', 18, 'slovenie', 0, 0, NULL, NULL, NULL),
(163, '', 'Ghemra', '', 'Taroudant', '80000', NULL, 'Marrokko', 0, 0, NULL, NULL, NULL),
(164, '', 'Grote Hoef ', ' 13', 'Lommel', '3920', 1, 'België', 0, 0, NULL, NULL, NULL),
(165, '', 'Rambla Clavi', '3', 'Chirivel', '04825', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(166, '', 'Le Bourg', '', 'Le Bastit', '46500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(167, '', 'Le Bourg', '', 'Le Bastit', '46500', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(168, '', 'Rua Alferes Curado', '20', 'Maçãs de Dona Maria', '3250-258', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(169, '', 'L.Ruelensstraat 60', '', 'kessel lo', '3010', 1, 'belgië', 0, 0, NULL, NULL, NULL),
(170, '', 'rue charles demandre', '31', 'AILLEVILLERS ET LYAUMONT', '70320', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(171, '', 'Chemie de la greze', '1', 'Labastide-Murat', '46240', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(172, '', 'camino de la pinada', 'xb13', 'chinorlet', '03649', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(173, '', '                                  ', '   ', '           ', '    ', 39, '                ', 0, 0, NULL, NULL, NULL),
(174, '', 'Rua das Violetas', 'Casa dos Tres Caminhos', 'Ulgueira', '2705-349', NULL, 'Colares', 0, 0, NULL, NULL, NULL),
(175, '', 'eveninglaan', '43', 'Brussel', '1200', 1, 'België', 0, 0, NULL, NULL, NULL),
(176, '', 'd\'Uitseweg 117', '', 'Erembodegem', '9320', NULL, 'Belgium', 0, 0, NULL, NULL, NULL),
(177, '', 'Fietelstraat', '12', 'Oudenaarde', '9700', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(178, '', 'Dauwstraat', '28', 'Stekene', '9190', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(179, '', 'Leeuwerikenstraat', '26', 'Maasmechelen', '3630', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(180, '', 'Favard', '', 'Trentels', '47140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(181, '', 'Grote Kerkweg', '26', 'Schriek', '2223', 1, 'België', 0, 0, NULL, NULL, NULL),
(182, '', ' St, Veit ', ' 5', ' Prags', ' 39030', NULL, ' Italien', 0, 0, NULL, NULL, NULL),
(183, '', 'Plateia', '1', 'Pinakates', '37010', 12, 'Griekenland', 0, 0, NULL, NULL, NULL),
(184, '', 'Barrierestraat', '55', 'brugge', '8200', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(185, '', ' barrierestraat', '55', 'brugge', '8200', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(186, '', 'Burgemeester Van Ackerwijk K', '292', 'Zele', '9240', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(187, '', 'bosstraat', '22', 'laarne', '9270', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(188, '', 'Ninovestraat', '56a', 'Ronse', ' 9600', NULL, ' Belgie', 0, 0, NULL, NULL, NULL),
(189, '', ' Rue de la Mer', ' ', 'Warang, Mbour', ' BP 104', NULL, ' Senegal', 0, 0, NULL, NULL, NULL),
(190, '', 'de ast ', '7', 'wingene', '8850', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(191, '', 'Lejourstraat', '20', 'Waarschoot', '9950', 1, 'België', 0, 0, NULL, NULL, NULL),
(192, '', 'liezeledorp', '86', 'liezele', '2870', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(193, 'Cruviers-Lascours', 'Chemin de la Guinguette', '221', 'Cruviers-Lascours', '30360', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(194, '', 'Via Sant\' Amico', '52', 'Morro D\'Alba', '60030', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(195, '', 'Korte Geemstraat', '44', 'Hamme', '9220', 1, 'België', 0, 0, NULL, NULL, NULL),
(196, '', 'Kerkstraat', '124 ', 'STEKEN', '9190', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(197, '', 'Voorstraat', '48', 'Poeldijk', '2685EN', 2, 'Nederland', 0, 0, NULL, NULL, NULL),
(198, '', 'zeedijk', '133', 'mariakerke oostende', '8400', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(199, '', 'Borsbekestraat ', '80', 'Ressegem', '9551', 1, 'BelgiE', 0, 0, NULL, NULL, NULL),
(200, '', 'Moraitika', ' ', 'Moraitika', '49084', 12, 'Griekenland', 0, 0, NULL, NULL, NULL),
(201, '', 'ter eike ', '30', 'staden', '8840', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(202, '', 'CAETSBEEKSTRAAT 9', '', 'DIEPENBEEK', '3590', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(203, '', 'Walbosstraat', '36', 'Destelbergen', '9070', 1, 'België', 0, 0, NULL, NULL, NULL),
(204, '', 'Cami de Bell lloc', 'S/N', 'Palamos', '17230', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(205, '', 'Kerkendijk', '17', 'Kalmthout', '2920', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(206, '', 'Bosveldstraat ', '30', 'ZOTTEGEM', '9620', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(207, '', 'Kalmthoutsebaan 10', '10', 'Zandvliet', '2040', NULL, 'Antwerpen', 0, 0, NULL, NULL, NULL),
(208, '', 'leu le pavillon', '0', 'thionne', '03220', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(209, '', 'vrouweneekhoekstraat', '1', 'sint-niklaas', '9100', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(210, '', 'Bay Beach Avenue', '85', 'Sunset Beach - Cape Town', '7441', NULL, 'South - Africa', 0, 0, NULL, NULL, NULL),
(211, '', 'sergeant segersstraat', '24', 'emelgem', '8870', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(212, '', 'Keizersdreef', '49', 'Evergem', '9940', 1, 'België', 0, 0, NULL, NULL, NULL),
(213, '', 'sint-jansbeekveld', '25', 'Mariekerke', '2880', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(214, '', 'Zomerstraat', '3/6', '3500', '3500', 1, 'België', 0, 0, NULL, NULL, NULL),
(215, '', 'MELKSTRAAT ', '1', 'HOEGAARDEN', '3321', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(216, '', 'Sérère', '', 'Warang - Mbour', '1739', 26, 'Senegal', 0, 0, NULL, NULL, NULL),
(217, '', 'Moraitika', ' ', 'Moraitika Corfu (Greece)', '49084', 12, 'Griekenland', 0, 0, NULL, NULL, NULL),
(218, '', 'Quinta do Pinheiro Manso', '', 'São Geraldo, Tábua', '3420-066', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(219, '', 'Stormestraat 75', 'bus 4.1', 'Waregem', '8790', 1, 'België', 0, 0, NULL, NULL, NULL),
(220, '', 'Bosstraat', '95', 'Erpe-Mere', '9420', 1, 'België', 0, 0, NULL, NULL, NULL),
(221, '', 'Rue de la Mer', '21', 'Saly', '00000', 26, 'Senegal', 0, 0, NULL, NULL, NULL),
(222, '', 'rakoczi utca', '15', 'andocs', '8675', 34, 'hongarije', 0, 0, NULL, NULL, NULL),
(223, '', 'Domaine La Broutie', '', 'Maleville', '12350', 3, 'France', 0, 0, NULL, NULL, NULL),
(224, '', 'Wallestraat37', '37', 'Waarbeke', '9506', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(225, '', 'apartado 126', '', 'fuseta', '8701-908', 4, 'portugal', 0, 0, NULL, NULL, NULL),
(226, '', 'St Waldetrudisstraat', '103', 'Herentals', '2200', 1, 'België', 0, 0, NULL, NULL, NULL),
(227, '', 'felix d\'hoopstraat', '172', 'tielt', '8700', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(228, '', 'Martelaarslaan', '361', 'Gent', '9000', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(229, '', 'Hobosstraat', '6', 'Overpelt', '3900', 1, 'België', 0, 0, NULL, NULL, NULL),
(230, '', 'Werfstraat', '36', 'Ieper', '8900', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(231, '', 'Kaya Jan Thiel ', '82', 'Jan Thiel', '00000', 8, 'Curacao', 0, 0, NULL, NULL, NULL),
(232, '', 'pes-do-cerro', 'geen', 'moncarapacho', '8700-124', 4, 'portugal', 0, 0, NULL, NULL, NULL),
(233, '', 'Chemin des vergers aux baux', '564', 'Bedoin', '84410', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(234, '', 'C. Pozanos', '64', 'Burgos', '09006', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(235, 'qala / Gozo', '28th april 1688street', '39', 'qala / Gozo', 'QLA 1011', 54, 'Malta', 0, 0, NULL, NULL, NULL),
(236, '', 'lieu-dit Gamouty', '-', 'Fumel', 'Fr 47500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(237, '', 'Route de Huismes ', '1-5', 'Chinon', '37500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(238, '', 'El Adelantado', '119', 'Iznajar', '14978', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(239, '', 'MEDECIN', '', 'dunes', '82340', 3, 'france', 0, 0, NULL, NULL, NULL),
(240, '', 'Quartier de Pouzadouire', '', 'Laviolle', '07530', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(241, '', 'rue des Blart', '570', 'Rang du Fliers', '62180', 3, 'France', 0, 0, NULL, NULL, NULL),
(242, '', 'le moulin', '', 'Comus', '11340', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(243, '', 'Paguères de Douelle', '{number}', '31230 cazac', '31230', 3, 'France', 0, 0, NULL, NULL, NULL),
(244, '', 'Montée de la Castre', '2', 'Saint Paul de vence', '06570', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(245, '', ' route de can tony', ' ', ' serralongue', ' 66230', NULL, ' frankrijk', 0, 0, NULL, NULL, NULL),
(246, '', 'beach road', '5747', 'Diani Beach', '80401', 24, 'Kenia', 0, 0, NULL, NULL, NULL),
(247, '', 'beach road', ' ', 'Diani Beach', '80401', 24, 'Kenia', 0, 0, NULL, NULL, NULL),
(248, '', 'Via Carlo Viola', '1', 'Curiglia con Monteviasco', '21010', NULL, 'Italia', 0, 0, NULL, NULL, NULL),
(249, '', 'Estrada da Foia', 'apartado 97', 'Monchique', '8550-909', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(250, '', ' Sucha', '28', 'Krizany ( Liberec )', '46353', 17, 'Tsjechië', 0, 0, NULL, NULL, NULL),
(1228, 'Poperinge', 'Priesterstraat', '35', 'Poperinge', '8970', 1, '', 0, 0, NULL, NULL, NULL),
(252, '', 'Sitio do Pereiro', '', 'Moncarapacho', '8700-123', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(253, '', 'Wolschaerderveldenstraat', ' 30', ' Mortsel', '2640', NULL, ' België', 0, 0, NULL, NULL, NULL),
(254, '', 'Montee des Oliviers ', '114a', 'Rochefort du Gard', '30650', 3, 'France', 0, 0, NULL, NULL, NULL),
(255, '', 'Schoonzichtlaan', '76', 'Winksele', '3020', 1, 'België', 0, 0, NULL, NULL, NULL),
(256, '', 'la Garnasette', '', 'Rosières', '43800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(257, '', 'Eetsvelde', '30', 'Erpe-Mere', '9420', 1, 'België', 0, 0, NULL, NULL, NULL),
(258, '', 'MOLENSTRAAT', '56', 'SIJSELE', '8340', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(259, '', 'le bruel ', '358', 'Génerargues', '30140', 3, 'France', 0, 0, NULL, NULL, NULL),
(260, '', 'Domaine de RIBIERE', '', 'Grospierres', '07120', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(261, '', 'Valierstraat ', '23', 'Liedekerke ', '1770', 1, 'België', 0, 0, NULL, NULL, NULL),
(263, '', 'Lieu-dit Fontblanque', '', 'Penne (Midi-Pyreneeen)', '81140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(264, '', '53,Av.des Acacias', ' 06500', 'Menton', '06500', 3, 'France', 0, 0, NULL, NULL, NULL),
(265, '', 'Vivenda Sumertime - Vale de Arges', '', 'Carvoeiro', '8400-557', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(266, '', 'Penant', '1', 'Dréhance', '5500', 1, 'België', 0, 0, NULL, NULL, NULL),
(267, '', 'Dennenlaan', '6', 'Apeldoorn', '7313 AL', 2, 'Nederland', 0, 0, NULL, NULL, NULL),
(268, 'RASTEAU', 'Rue de la République', '1', 'RASTEAU', '84110', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(269, '', 'Herestraat ', '72', 'zonhoven', '3520', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(270, '', 'la Garnasette', ' ', 'Rosières', '43800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(271, '', 'rue Thomas', '6', 'Dangolsheim', '67310', 3, 'France', 0, 0, NULL, NULL, NULL),
(272, '', 'MA5401 Ctra El Burgo-Ardales', 'km 3,5', 'El Burgo', '29420', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(273, '', 'Tiengebodensteeg', '8', 'Antwerpen', '2060', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(274, '', 'Boite Postale', '1', 'Toubacouta', '00000', 26, 'Senegal', 0, 0, NULL, NULL, NULL),
(275, '', 'Flikbergstraat', '51', 'Genk', '3600', 1, 'België', 0, 0, NULL, NULL, NULL),
(276, '', 'Örvényeshegy', '14', 'Zalacsány', '8782', NULL, 'info@kalandor.nl', 0, 0, NULL, NULL, NULL),
(277, '', 'HARINGHOEK 38', '', 'NIEUWPOORT', '8620', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(278, '', 'Rua  de Souto', '150', 'Ponte da Barca', '4980-258', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(279, '', 'Doornstraat', '136', '2610', '2610', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(280, '', 'Av de la gare ', '8', 'Neufchateau', '6840', 1, 'België', 0, 0, NULL, NULL, NULL),
(281, '', 'de blokskens', '10', 'zoersel', '2980', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(282, '', 'IJzerenwegstraat 40', '', 'Kalmthout', '2920', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(283, '', 'Landstrasse', '171', 'Klosters', '7250', NULL, 'Zwitserland', 0, 0, NULL, NULL, NULL),
(284, '', 'Grote Markt  24', '', 'Boom', '2850', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(285, 'Barjols', 'Chemin de la Pin', '', 'Barjols', '83670', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(286, '', 'houtvoortstraat', '41', 'sint-gillis-waas', '917O', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(287, '', 'Romestraat', '36', 'Oostende', '8400', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(288, '', 'schoendalestraat', '79', 'sint-eloois-vijve', '8793', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(289, '', 'Hof van \'t henneken ', '68', 'Roeselare', '8800', 1, 'België', 0, 0, NULL, NULL, NULL),
(290, '', 'cerro colorado 12', '', 'lavinuela', '29713', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(291, '', 'Le Bourg ', '', 'Promilhanes', '46260', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(292, '', 'Veldeken 39', '', 'Zele', '9240', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(293, '', 'Los Roques', '16', 'El Sao', '35120', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(294, '', 'Sint Jobstraat', '1', 'Aalst', '9300', 1, 'België', 0, 0, NULL, NULL, NULL),
(295, '', 'Drabstraat ', '73', 'Mortsel', '2640', 1, 'België ', 0, 0, NULL, NULL, NULL),
(296, '', 'via castelsilano', '11-13', 'Rome', '00118', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(297, '', 'apartado', '26', 'midoes (tabua) Coimbra', '3420-136', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(298, '', 'rue du cul du sac', '8', 'Grandchamp', '52600', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(299, '', 'Au Marchay', '1', 'Nojals et Clottes', '24440', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(300, '', 'lieu dit Esperou', '', 'St Denis', '11310', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(301, '', 'Les Marchands', '2', 'Chezelle', '03140', NULL, 'Frankrijk, Auvergne,dep.d\\\\Allier\'\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\', 0, 0, NULL, NULL, NULL),
(302, '', 'Tanya', '1221', 'Bokros', '6648', 34, 'Hongarije', 0, 0, NULL, NULL, NULL),
(303, '', 'Merellaan', '9', 'Wielsbeke', '8710', 1, 'België', 0, 0, NULL, NULL, NULL),
(304, '', 'Sitio da Vilharinha', 'cxp-540 E', 'S.B. Messines', '8375-030', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(305, '', 'Lieudit Bergès', '', 'Castéra-Verduzan', '32410', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(306, '', 'Bredaseweg', '353', 'Tilburg', '5037LC', 2, 'Nederland', 0, 0, NULL, NULL, NULL),
(307, '', 'Koludroviceva ', '4', 'kastel novi', '21216', 21, 'Kroatie', 0, 0, NULL, NULL, NULL),
(308, '', 'STIPPELBERG', '201', 'WESTERLO', '2260', 1, 'België', 0, 0, NULL, NULL, NULL),
(309, '', 'Avenue des Auberges', '17', 'Ornaisons', '11200', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(310, '', 'Strada Tocile De Sus', '141', 'Tocile', '557220', 15, 'Roemenie', 0, 0, NULL, NULL, NULL),
(311, '', 'LA BESSEDE', '', 'SIMEYROLS', '24370', 3, 'FRANKRIJK', 0, 0, NULL, NULL, NULL),
(312, '', 'Beauzonet', '1', 'Lablachere', '07230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(313, '', 'Limburgstraat ', '8/2', 'Gent', '9000', 1, 'België', 0, 0, NULL, NULL, NULL),
(314, '', 'contrada sant\'antonio', '6', 'Petritoli', '63848', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(315, '', 'Rastaillou', 'LD', 'Salles de Cadouin', '24480', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(316, '', 'Europalaan ', '29/1', 'Hasselt', 'B-3500', 1, 'België', 0, 0, NULL, NULL, NULL),
(317, 'Sainte Alvère', 'Lieu dit: La plumardie basse', 'Route D703', 'Sainte Alvère', '24510', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(318, '', 'Les Fraysses', '', 'Valroufié', '46090', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(319, '', 'joan de maragall ', '28', 'sant feliu de guixols', '17220', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(320, '', 'zone la pineda', '26', 'macanet de la selva', '17412', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(321, '', 'Essendallaan', '14', 'Vilvoorde', '1800', 1, 'België', 0, 0, NULL, NULL, NULL),
(322, '', 'Cortijada Herrera', '10', 'Villanueva de Algaidas', '29310', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(323, '', 'Partido del rio', '71', 'Riogordo', '29180', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(324, '', 'Broekstraat', '15', 'Oud-Heverlee', '3050', 1, 'België', 0, 0, NULL, NULL, NULL),
(325, '', ' 53/57 Moo3 Soi Tong Kail', ' Chaweng Noi', ' Koh Samui', ' 84310', 36, 'Thailand', 0, 0, NULL, NULL, NULL),
(326, '', 'Schl', '2 b', 'Hirschegg', '6992', 6, 'Oostenrijk', 0, 0, NULL, NULL, NULL),
(327, '', 'Postweg', '193', 'Vlezenbeek', '1602', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(328, '', 'Carril De Las Alberquillas', '14', 'Alcaucin/Malaga', '29711', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(329, '', 'rue docteur chrestien', '6', 'Sommières', '30250', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(330, '', 'Gamard', '', 'Saint Eutrope de Born', '47210', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(331, '', 'Le Bouchet', '', 'Genestelle', '07530', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(332, '', 'san juan de dios', '212', 'Havana', 'geen', NULL, 'Cuba', 0, 0, NULL, NULL, NULL),
(333, '', 'yazır köyü kilisi yakası mevki', '109/160', 'kumluca-antalya', '07350', 28, 'turkije', 0, 0, NULL, NULL, NULL),
(334, '', 'Plan Des Amarens', '0', 'Malaucene', '84340', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(335, '', 'Rua Por Do Sol', '14', 'Lourinha', '2530-077', NULL, 'Lissabon', 0, 0, NULL, NULL, NULL),
(336, '', 'St.Rivalain', '1', 'saint-barthelemy', '56150', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(337, '', 'Kilmoon', '', 'Lisdoonvarna', 'Co. Clare', 13, 'Ierland', 0, 0, NULL, NULL, NULL),
(338, '', 'Limburgstraat ', '8/2', 'Gent', '9000', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(339, '', '85 Impasse les maisons Neuves', ' ', 'Montret', '71440', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(340, '', 'Blütenweg', '24a', 'Buchloe-Lindenberg', '86807', 11, 'Duitsland', 0, 0, NULL, NULL, NULL),
(341, '', 'Bourdiel', '', 'Engayrac', '47470', 3, 'Frankrijk ', 0, 0, NULL, NULL, NULL),
(342, '', 'Neyrac Bas', ' ', 'Meyras (07380), ', '07380', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(343, '', 'Lieu dit Le Chec', '', 'Saint-Ost', '32300', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(344, '', 'lieu dit Lescos', '', 'Marcellus', '47200', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(345, '', 'Partido del Rio ', '71', 'Riogordo', '29180', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(346, '', 'chemin de la combes d\'oriol', '208', 'fayence', '83440', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(347, '', '18260', '', 'jars', '18260', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(348, '', 'Bunderstraat 11', '11', 'Beigem', '1852', 1, 'België', 0, 0, NULL, NULL, NULL),
(349, '', 'Chemin des Constantins', '6', 'Opio', '06650', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(350, '', 'frissehoeken ', '4', 'Mol', '2400', 1, 'België', 0, 0, NULL, NULL, NULL),
(351, '', 'Barriada de Santiago S/N', 'geen', 'Montejaque', '29360', NULL, 'Malaga', 0, 0, NULL, NULL, NULL),
(352, 'Cublac', 'Rue du Panorama', '21', 'Cublac', '19520', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(353, '', 'Boulevard du Bougnon', '526', 'Les Issambres', '83380', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(354, '', 'Gerweplein', '10', 'Beveren-Leie', '8791', 1, 'België', 0, 0, NULL, NULL, NULL),
(355, '', 'CTRA VILLANUEVA DE TAPIA,MA215', 'MA6100', 'VILLANUEVE DE ALGAIDAS', '29310', 5, 'SPANJE', 0, 0, NULL, NULL, NULL),
(356, '', 'PETRIU', '1-3', 'ORBA', '03792', 5, 'SPANJE', 0, 0, NULL, NULL, NULL),
(357, '', 'Rue philibert commerçon', '29', 'Toulon sur Arroux', '71320', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(358, '', 'Apartodo de Correos', '350', 'Torre Del Mar', '29740', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(359, '', 'chantemerles', '', 'nyons', '26110', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(360, '', 'Grotstraat ', '25', 'Ramsdonk', '1880', 1, 'België', 0, 0, NULL, NULL, NULL),
(361, '', 'Groendal', '164', 'Erpe-Mere', '9420', 1, 'België', 0, 0, NULL, NULL, NULL),
(362, '', 'ptda Macheve ', '11', 'Bolulla', '03518', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(363, '', 'Grootveldstraat', '107', 'Berg', '1910', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(364, '', 'Zagerijstraat', '11', 'Massenhoven', '2240', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(365, '', 'La Cube', '', 'Le Brugeron', '63880', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(366, '', 'De Robianostraat', '47', 'borsbeek', '2150', 1, 'België', 0, 0, NULL, NULL, NULL),
(367, '', 'Livadia / Parikia', '1', 'Paros', '84400', 12, 'Griekenland', 0, 0, NULL, NULL, NULL),
(368, 'Heist-aan-Zee', 'Vissershuldeplein', '2', 'Heist-aan-Zee', '8301', 1, 'Belgi', 0, 0, NULL, NULL, NULL),
(369, 'balocas', 'rua da seara', 'rua da seara, 1', 'balocas', '3420-061', 4, 'centraal portugal', 0, 0, NULL, NULL, NULL),
(370, '', 'Messines de Cima', '45 S', 'S.B.Messines', '8375-047', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(371, '', 'pont notre Dame', '', 'Coulaures', '24420', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(372, '', 'Molle Molle', '1', 'Curahuasi', '03041', NULL, 'Peru', 0, 0, NULL, NULL, NULL),
(373, '', '38/189 MOO 4', '38/189', 'PHUKET', '83110', 36, 'THAILAND', 0, 0, NULL, NULL, NULL),
(374, '', 'Juan Luis', '17A', 'West Groot St. Joris', '5400', 8, 'Curacao', 0, 0, NULL, NULL, NULL),
(375, '', 'Ramal das Medas', '{number}', '{city}', '3305-183', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(376, '', 'Atembekeweg 34', '', 'Geraardsbergen', '9500', 1, 'België', 0, 0, NULL, NULL, NULL),
(377, '', ' Pont Notre Dame', ' ', ' Coulaures', ' 24420', NULL, ' Frankrijk', 0, 0, NULL, NULL, NULL),
(378, '', 'HUISERVELDSTRAAT', '7', 'HASSELT', '3500', 1, 'BELGIE', 0, 0, NULL, NULL, NULL),
(379, '', 'Contrada Torroni', '8', 'Atri', '64032', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(380, '', 'Località Caiti', '2', 'Merana', '15010', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(381, '', 'jalan tegal cupek II', '', 'kerobokan', '80361', 31, 'indonesie', 0, 0, NULL, NULL, NULL),
(382, '', 'Alfambras', '', 'Aljezur', '8670160', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(383, '', 'vlasendaalstraat', '98', 'dilbeek', '1700', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(1231, 'Antwerpen', 'Kleipikker', '2', 'Antwerpen', '2960', 1, '', 0, 0, NULL, NULL, NULL),
(385, '', 'Cami del Pla de Pons Ramal 5A ', 'Apdo. Correos 202', 'Ontinyent', '46870', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(386, '', 'Talmuka', '152', 'Kapuliyadda', '2840', 27, 'Sri Lanka', 0, 0, NULL, NULL, NULL),
(387, '', 'zorgvliet', '16', 'st-kat-waver', '2860', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(388, 'Les Coteaux Perigourdins', 'Linoir', '', 'Les Coteaux Perigourdins', '24120', 3, 'Frankrijk ', 0, 0, NULL, NULL, NULL),
(390, '', 'Bardouquet', '0', 'Castelnau-Montratier', '46170', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(391, '', 'Albus Rylaan', '7441', 'Sunset Beach, Kaapstad', '7441', NULL, 'Suid Afrika', 0, 0, NULL, NULL, NULL),
(392, '', 'L Etournerie', '4', 'Chateau Guibert', '85320', NULL, 'Vendee', 0, 0, NULL, NULL, NULL),
(393, '', 'Chmin la toquerie', '84', 'St-pierre des IFS', '27450', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(394, '', 'quartier les Sares', ' ', 'Tourves', '83170', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(395, '', 'Wijnstraat', '31d', 'Sint-Pauwels Waas', '9170', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(396, '', 'Bialy Zdroj 27, 78-540 Kalisz Pomorski', ' ', 'Kalisz pomorski', '78-540', 25, 'Polen', 0, 0, NULL, NULL, NULL),
(397, '', 'Grljevacka', '114', 'Podstrana ', '21312', 21, 'Kroatië ', 0, 0, NULL, NULL, NULL),
(398, '', 'Frazione San P', '37', 'Arcevia', '60011', NULL, 'Itali', 0, 0, NULL, NULL, NULL),
(399, '', 'Lieu dit Troniac', ' ', 'Saux', '46800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(400, '', 'Camino los Solanos', 's/n', 'Alhaurin de la Torre', '29130', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(401, '', '38 Rue du Manoir', '', 'La Séguinière', '49280', 3, 'France', 0, 0, NULL, NULL, NULL),
(402, '', 'Rákóczi utca 48', '', 'Somogyhárságy', '7925', NULL, 'Baranya', 0, 0, NULL, NULL, NULL),
(403, '', 'los llanos', '78', 'albanchez', '9255', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(404, '', 'Jalan Pantai Pererenan', '129', 'Pererenan', '1000', NULL, 'Bali', 0, 0, NULL, NULL, NULL),
(405, '', 'chemin des violettes', '5', 'louzignac', '17160', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(406, '', 'Schoolstraat', '20', 'Massenhoven', '2240', 1, 'België', 0, 0, NULL, NULL, NULL),
(407, '', 'Sitio Da Boica', '', 'Sao Bras De Alportel', '8150', NULL, 'Algarve', 0, 0, NULL, NULL, NULL),
(408, '', 'Calle Perseo', '8', 'Torrevieja', '03183', 5, 'SPANJE', 0, 0, NULL, NULL, NULL),
(409, '', 'Astridlaan 141', '', 'Assebroek-Brugge', '8310', NULL, 'West-Vlaanderen', 0, 0, NULL, NULL, NULL),
(410, '', 'Plan du Four', ' ', 'Canes-et-Clairan', '30260', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(411, '', ' wingensesteenweg 111', ' ', ' tielt', ' 8700', 1, 'belgie ', 0, 0, NULL, NULL, NULL),
(412, '', 'Sätilavägen', '386', 'Fjärås', '43972', 9, 'Zweden', 0, 0, NULL, NULL, NULL),
(413, '', 'Calle del cura', '5', 'Villasbuenas de Gata ', '10858', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(414, '', 'Germaine Goyenslaan 6', '', 'Gingelom', '3890', NULL, 'Belg', 0, 0, NULL, NULL, NULL),
(415, '', 'C/ Alcalde Pedro Hurtado Calero ', '85', 'Cártama', '29570', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(416, '', 'bogaertstraat', '9', 'ursel', '9910', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(417, '', 'Los Olivos / Puerto del Olivar', '', 'Sedella', '29715', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(418, '', 'La Combe', '1', 'Les Eyzies', '24620', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(419, '', 'Gelemis yali', '21', 'Gelemis- Kas', '07976', 28, 'Turkije', 0, 0, NULL, NULL, NULL),
(420, '', 'Denderbellestraat ', '132', 'Dendermonde', '9200', 1, 'België', 0, 0, NULL, NULL, NULL),
(421, '', 'Palandran', '', 'Penne d\'Agenais', '47140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(422, '', '14, La Dure', ' ', 'Mazirat', '03420', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(423, '', 'Velka bukovina', '146', 'Velka Bukovina', '40729', 17, 'Tsjechie', 0, 0, NULL, NULL, NULL),
(424, '', 'La Dure', '14', 'Mazirat', '03420', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(425, '', 'le bosc, les assions', 's/n', 'les vans', '07140', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(426, '', 'Hagedoornlei', '1', 'Edegem', '2650', 1, 'België', 0, 0, NULL, NULL, NULL),
(427, '', 'Moo 3', '54/14', 'Nai Yang', '83110', 36, 'Thailand', 0, 0, NULL, NULL, NULL),
(428, '', 'Cappennelle', ' 32', 'Castiglione del Lago', '06061', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(429, '', 'placa des pou vell', '10', 'sant llorenc des cardassar', '07530', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(430, '', 'Everslaan', '28', 'Everberg', '3078', 1, 'België', 0, 0, NULL, NULL, NULL),
(431, '', 'Corro de los juaneles', 'S/N', ' La garapacha', '30620', NULL, ' Spanje', 0, 0, NULL, NULL, NULL),
(432, '', 'champ Lambert', '8', 'pleine fougeres', '35610', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(433, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(434, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(435, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(437, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(438, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(439, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(440, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(441, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(442, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(443, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(444, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(445, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(446, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(447, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(448, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(449, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(450, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(451, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(452, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(453, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(454, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(455, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(456, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(457, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(458, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(459, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(460, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(461, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(462, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(463, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(466, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(467, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(468, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(469, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(470, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(471, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(472, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(473, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(474, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(475, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(476, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(477, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(478, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(479, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(480, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(481, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(482, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(483, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(484, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(485, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(486, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(487, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(488, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(489, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(490, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(491, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(492, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(493, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(494, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(495, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(496, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(497, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(498, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(499, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(500, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(501, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(502, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(503, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(504, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(505, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(506, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(507, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(508, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(509, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(510, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(511, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(512, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(513, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(514, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(515, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(516, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(517, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(518, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(519, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(520, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(521, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(522, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(523, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(524, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(525, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(526, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(527, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(528, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(529, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(530, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(531, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(532, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(533, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(534, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(535, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(536, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(537, '', 'Pago de Fuencaliente', 'S/N', 'Hu', '18830', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(538, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(539, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(540, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(541, '', 'rue des Maraichers', '15', 'Montillot', '89660', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(542, '', 'Contrada Torroni', '8', 'Atri', '64032', NULL, 'Italy', 0, 0, NULL, NULL, NULL),
(543, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL);
INSERT INTO `user_addresses` (`id`, `address`, `street`, `street_number`, `city`, `zip`, `country_id`, `country`, `region_id`, `department_id`, `label_id`, `created_at`, `updated_at`) VALUES
(544, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(545, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(546, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(1227, 'duffel', 'hoogstraat', '14B', 'duffel', '2570', 1, '', 0, 0, NULL, NULL, NULL),
(548, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(551, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(552, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(553, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(554, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(555, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(556, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(557, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(558, '', 'Las Gondolas', '9', 'La Manga del Mar Menor', '30380', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(559, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(560, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(561, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(562, '', 'iznate', '1', 'iznate', '29792', NULL, 'spain', 0, 0, NULL, NULL, NULL),
(563, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(564, '', 'Ommegangstraat ', '16', 'izegem', '8870', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(565, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(566, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(567, '', 'D4 Route de Vallon ', 'Quartier combe grand', 'saint-rem', '07700 ', 3, 'France', 0, 0, NULL, NULL, NULL),
(568, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(569, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(570, '', 'Lencouet', '0', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(571, '', 'Lencouet', '0', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(572, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(573, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(574, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(575, '', 'Walter Thijsstraat', '6 bus 2.02', 'Hasselt', '3500', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(576, '', 'Voskenslaan', '36 A', 'Gent', '9000', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(577, '', 'sava geurgi rakovski', '1', 'rogozen bulgarije', '3360', NULL, 'Vratsa', 0, 0, NULL, NULL, NULL),
(578, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(579, '', 'Rue de smet', '0', 'Bijilo ', '', 32, 'Gambia', 0, 0, NULL, NULL, NULL),
(580, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(581, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(582, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(583, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(584, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(585, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(586, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(587, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(588, '', 'Lencouet', '0', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1232, 'brugge', 'bossuytlaan', '38', 'brugge', '8310', 1, '', 0, 0, NULL, NULL, NULL),
(591, '', 'Chemin de Moncade', '500', 'Mons', '40700', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(592, '', 'C. San Pedro,', '16', 'Nerja', '29780', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(593, '', 'Lencouet', '1', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(594, '', 'Route de Mirabeau, Chemin des Escaravillons,', '0', 'Beaumont de Pertuis', '84120', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(595, '', 'Le Petit Mas', '0', 'Veyrines de Vergt', '24380', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(596, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(597, '', 'rue du tilleul', '7', 'Wibrin-Houffalize', '6666', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(598, '', 'Le Lac', '2', 'M', '23420', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(599, '', 'Braambos', '11', 'Westerhoven', '5563AB', 2, 'Nederland', 0, 0, NULL, NULL, NULL),
(600, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(601, '', 'Rua Principal', '115', 'Macalhona-Alfeizer', '2460-202', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(602, '', 'Vompdes', '0', 'Chambonas', '07140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(603, '', 'claeyssensstraat', '13', 'roeselare', '8800', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(604, '', 'C/ de la te', '15', 'Costa del silencio', '38630', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(605, '', 'cx postal', '1499', 'Aljezur', '8670-211', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(606, '', 'bruggestraat ', '210 c', 'zwevzele', '8750', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(607, 'Chirivel', 'Rambla Clavi', '3', 'Chirivel', '04825', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(608, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(609, '', 'BP', '1', 'Toubacouta', '00001', 26, 'Senegal', 0, 0, NULL, NULL, NULL),
(610, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(611, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(612, '', 'Volderij ', '9 bus 0001', 'Kontich', '2550', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(613, '', 'Landsmanstraat', '15', 'Zwevezele', '8750', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(614, '', 'Algarve', '116', 'Tavira', '8800', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(615, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(616, '', 'Vinha dos Almargens', '---', 'Sao Bras De Alportel', '8150-013', NULL, 'Algarve', 0, 0, NULL, NULL, NULL),
(617, '', 'Le Grand Cloire', '1', 'Lessac', '16500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(618, '', 'Pas de Gazaille', 'Domaine Elias', 'M', '09230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(619, '', 'Les Goulets', '2', 'Parsac', '23140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(620, '', 'St Pargoire', '0', 'Lieu dit ST MARCEL, Wve de PLAISSAN', '34230', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(621, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(622, '', 'Chamin', '0', 'S.B.de Messines ( Silves)', '8375-038', 4, 'PORTUGAL', 0, 0, NULL, NULL, NULL),
(624, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(625, '', 'Route de Foix', '38 bis', 'Quillan', '11500', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(626, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(627, '', 'Chemin de Rodi', '', 'Colombi', '34390', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(628, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(629, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(630, '', 'Domaine La Mon', 'N/A', 'Limoux', '11300', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(631, '', 'baan nathacha', '375', 'kamala', '5575', 36, 'thailand', 0, 0, NULL, NULL, NULL),
(632, '', 'Heirbaan ', '35', 'DENDERMONDE', '9200', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(633, '', 'Calle La Canela', '9a', 'Los Cocoteros', '35544', NULL, 'Lanzarote', 0, 0, NULL, NULL, NULL),
(634, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(635, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(636, '', 'quinta da geia', '', 'aldeia das dez', '3400 214', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(637, '', 'N8, Rua Barbara torta ', 'S/n', 'Alfeizerao ', '2460-191', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(638, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(639, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(640, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(641, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(642, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(643, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(644, '', 'Partida El Boch', '183', 'Crevillente - Alicante', '03330', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(645, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(646, '', 'Lobroeken', '21', 'Hever', '3191', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(647, '', ' Rue du Guinot, Le Lac Est', '21', 'Saint Martin de Gurson', '24610', NULL, 'Dorodgne', 0, 0, NULL, NULL, NULL),
(648, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(649, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(650, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(651, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(652, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(653, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(654, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(655, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(656, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(657, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(658, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(659, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(660, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(661, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(662, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(663, '', 'klaverstraat', '2/205', 'Gent', '9000', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(664, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(665, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(666, '', 'podmelec', '45', 'Tolmin', '5220', NULL, 'Sloveni', 0, 0, NULL, NULL, NULL),
(667, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(668, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(669, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(670, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(671, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(672, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(673, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(674, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(675, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(676, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(677, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(678, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(679, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(680, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(681, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(682, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(683, '', 'Chemin de la Gleizasse', '315', 'Salavas', '07150', 3, 'France', 0, 0, NULL, NULL, NULL),
(684, '', 'lacalcinie', '', 'Auradou', '47140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(685, '', 'Chemin de Brunet', '1903', 'Oraison', '04700', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(686, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(687, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(688, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(689, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(690, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(691, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(692, '', 'Voc. Grimaldi ', '20', 'Matelica', '62024', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(693, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(694, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(695, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(696, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(697, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(698, '', 'Alemagna str.', '4', 'toblach-Dobbiaco', '39034', 7, 'Italie', 0, 0, NULL, NULL, NULL),
(699, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(700, '', 'Petite Rue', '13', 'Martinvelle', '88410', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(701, '', 'Pda.la hoya polligono3', '69', 'Elche', '03294', 5, 'spanje', 0, 0, NULL, NULL, NULL),
(702, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(703, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(704, '', 'Zandstraat', '123', 'Zoersel', '2980', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(705, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(706, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(707, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(708, '', 'N 3rd st', '509', 'Vienna', '31092', NULL, 'USA, Georgia', 0, 0, NULL, NULL, NULL),
(709, '', 'ROUTE DE ARRY', 'LE THUREL', 'RUE', '80120', NULL, 'SOMME', 0, 0, NULL, NULL, NULL),
(710, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(711, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(712, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(713, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(714, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(715, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(716, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(717, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(718, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(719, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(720, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(721, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(722, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(723, '', 'Koningin Astridlaan', '224', 'Sint-Michiels', '8200', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(724, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(725, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(726, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(727, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(728, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(729, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(730, '', 'Rakestraat', '5b', 'Wingene', '8750', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(731, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(732, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(733, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(734, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(735, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(736, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(737, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(738, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(739, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(740, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(741, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(742, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(743, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(744, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(745, '', 'Chemin du Long Pont', '957', 'Lagnes  (Vaucluse)', '84800', 3, 'France', 0, 0, NULL, NULL, NULL),
(746, '', 'Les Fraysses, Constans', '', 'Valroufi', '46090', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(747, '', 'Mallnitz', '18', 'Mallnitz', '9822', 6, 'Oostenrijk', 0, 0, NULL, NULL, NULL),
(748, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(749, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(750, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(751, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(752, '', 'Mieregemstraat', '', 'Merchtem', '1785', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(753, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(754, '', 'Avenue de la Gare', '16', 'Corr', '19800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(755, '', 'Impasse du Manbarrat', '1', 'La Roque-sur-C', '30200', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(756, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(757, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(758, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(759, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(760, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(761, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(762, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(763, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(764, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(765, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(766, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(767, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(768, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(769, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(770, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(771, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(772, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(773, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(774, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(775, '', 'LOMBO COMPRIDO', 'CP28', 'VILA DAS POMBAS - SANTO ANTAO', '000000', NULL, 'CABO VERDE', 0, 0, NULL, NULL, NULL),
(776, '', 'isenbaertlei', '39', 'Brasschaat', '2930', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(777, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(778, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(779, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(780, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(781, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(782, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(783, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(784, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(785, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(786, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(787, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(788, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(789, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(790, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(791, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(792, '', 'Goursolas', '.', 'Saint Jory De Chalais', '24800', 3, 'France', 0, 0, NULL, NULL, NULL),
(793, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(794, '', 'Les Terrasses du Chassezac', '', 'Chambonas', '07140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(795, '', 'Zgornji Dol', '1', 'Gornjigrad', '3342', 18, 'Slovenie', 0, 0, NULL, NULL, NULL),
(796, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(797, '', 'Dol ', '1', 'Gornjigrad', '3342', 18, 'Slovenie', 0, 0, NULL, NULL, NULL),
(798, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(799, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(800, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(801, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(802, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(803, 'Almedinilla - Cordoba', 'Los Rios', '19', 'Almedinilla - Cordoba', '14813', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(804, '', 'spechtweg', '11', 'keerbergen', '3140', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(805, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(806, '', 'route de Buache Chastan', '1000', 'Soyans', '26400', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(807, '', 'Rue du Coteau', '55', 'Chevetogne', '5590', 3, 'France', 0, 0, NULL, NULL, NULL),
(808, '', '', '', 'Saint Mesmin', '24270', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(809, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(810, '', 'Launay', '', 'Saint Clement', '50140', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(811, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(812, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(813, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(814, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(815, '', 'Kauwlei', '24', 'Kontich', '2550', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(816, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(817, '', 'Pago Macapas ', '3', 'Alcaucin', '29711', NULL, 'Spain', 0, 0, NULL, NULL, NULL),
(818, '', 'pech de cassagne', '', 'tr', '47140', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(819, '', 'Zavelstraat', '9', 'Mechelen', '2800', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(820, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(821, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(822, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(823, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(824, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(825, '', 'Barrio Vista Alegre', '34', 'Castell - Platja d\'Aro', '17249', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(826, '', 'Michielsenstraat', '1a', 'Assenede', '9960', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(827, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(828, '', 'Le perrier', '', 'Bertignat', '63480', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(829, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(830, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(831, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(832, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(833, '', 'wurfelderweg ', '7', 'maaseik', '3680', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(834, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(835, '', 'moulin des borderies', '', 'singles', '63690', 3, 'frankrijk', 0, 0, NULL, NULL, NULL),
(836, '', 'via Territoriale, 19', '19', 'Ostra', '60010', NULL, 'Itali', 0, 0, NULL, NULL, NULL),
(837, '', 'Hortensialaan', '15', 'Lochristi', '9080', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(838, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(839, '', 'Brujas', '00', 'Vagnas', '07150', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(840, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(841, '', 'Molentjesstraat ', '70', 'Kooigem', '8510', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(842, '', 'LD Chavy', '2', 'Lurcy-L', '03320', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(843, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(844, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(845, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(846, '', 'Avenida 28 De febrero ( the post shop box 173 )', '44', 'Albox', '04800', NULL, 'Spain', 0, 0, NULL, NULL, NULL),
(847, '', 'Hameau St Marcel', 'Av du Plaissan. Saint Pargoire', 'Pezenas', '34230', 3, 'FRANCE', 0, 0, NULL, NULL, NULL),
(848, '', 'le cheveryon 13', '', 'Bonnat', '23220', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(849, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(850, '', 'Le Renaudin', '', 'Mont et Marr', '58110', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(851, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(852, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(853, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(854, '', 'All', '1', 'Le Somail', '11120', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(855, '', 'Nijverheidsstraat ', '43 A bus 16', 'Gavere', '9890', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(856, '', 'Hollestraat', '17', 'Kerksken', '9451', 3, 'France', 0, 0, NULL, NULL, NULL),
(857, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(858, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(859, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(860, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(861, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(862, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(863, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(864, '', 'Camino de los Buitragos Dip. Zarzalico', 'Panel 2 buzon 12', 'Puerto Lumbreras (Lorca)', '30890', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(865, '', 'Calle El Castillo', '8', 'Santopetar', '04692', 5, 'Spanje', 0, 0, NULL, NULL, NULL),
(866, '', ' rue montlaur', '6', 'Caunes-Minervois', '11160', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(867, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(868, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(869, '', 'La Germondiere', '', 'Secondigny', '79130', 3, 'France', 0, 0, NULL, NULL, NULL),
(870, '', 'hengelhoefdreef', '', 'houthalen', '3530', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(871, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(872, '', 'loc. Schiopparello', '156', 'Portoferraio', '57037', 7, 'italie', 0, 0, NULL, NULL, NULL),
(873, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(874, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(875, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(876, '', 'Bergvelden', '52', 'Heist op den Berg', '2220', NULL, 'belgi', 0, 0, NULL, NULL, NULL),
(877, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(878, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(879, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(880, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(881, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(882, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(883, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(884, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(885, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(886, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(887, '', 'Lehoucklaan ', '10 bus 6', 'Oostduinkerke', '8670', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(888, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(889, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(890, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(891, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(892, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(893, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(894, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(895, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(896, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(897, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(898, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(899, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(900, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(901, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(902, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(903, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(904, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(905, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(906, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(907, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(908, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(909, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(910, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(911, '', 'Les Sarilles', '', 'Cortambert', '71250', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(912, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(913, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(914, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(915, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(916, '', 'La Cour', '', 'Bar', '19800', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(917, '', 'oud sluisstraat', '28', 'wijnegem', '2110', 1, 'belgie', 0, 0, NULL, NULL, NULL),
(918, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(919, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(920, '', 'C.P. 1190-Z', '1', 'Santa Catarina da Fonte do Bispo', '8800-167', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(921, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(922, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(923, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(924, 'TRANS-EN-PROVENCE', 'Chemin des Suous', 'Chemin des Suous', 'TRANS-EN-PROVENCE', '83720', 3, 'BELGIE', 0, 0, NULL, NULL, NULL),
(925, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(926, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(927, '', 'Wildestraat ', '13', 'Evergem', '9940', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(928, '', 'La l', '1', 'Pensol', '87440', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(929, '', 'Torenstraat ', '1', 'Wijgmaal', '3018', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(930, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(931, '', 'Le puech', '', 'Bouquet', '30580', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(932, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(933, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(934, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(935, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(936, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(937, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(938, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(939, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(940, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(941, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(942, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(943, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(944, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(945, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(946, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(947, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(948, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(949, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(950, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(951, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(952, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(953, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(954, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(955, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(956, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(957, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(958, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(959, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(960, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(961, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(962, '', 'Malosewaver ', '105', 'Geel ', '2440', NULL, 'Belgi', 0, 0, NULL, NULL, NULL),
(963, '', 'cavallano', '55', 'casole d"elsa', '53031', 7, 'italie', 0, 0, NULL, NULL, NULL),
(964, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(965, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(966, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(967, '', 'Vlierbeekstraat', '6', 'Blanden', '3052', 1, 'Belgie', 0, 0, NULL, NULL, NULL),
(968, '', 'Chemin des Constants', '', 'Cabasse', '83340', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(969, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(970, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(971, '', '12,Rue des P', '', 'Migr', '17330', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(972, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(973, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(974, '', 'Lieu dit Rozas', '13', 'Saint Orse', '24210', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(975, '', 'Rua do Casal das Leivas', '21', 'Olho Marinho', '2510-545', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(976, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(977, '', 'Boulevard Desanat', '1', 'Tarascon', '13150', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(978, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(979, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(980, '', 'Combabessou', '1', 'Saint Bazile de la Roche', '19320', 3, 'France', 0, 0, NULL, NULL, NULL),
(981, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(982, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(983, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(984, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(985, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(986, '', NULL, NULL, NULL, NULL, 3, 'France', 0, 0, NULL, NULL, NULL),
(987, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(988, 'ssss', 'ssss', '333', 'ssss', '1234', 3, '', 0, 0, NULL, NULL, NULL),
(989, 'dddd', 'Contrada  Serrone', '3', NULL, '63853 (FM)', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(1237, 'Schoten', 'Rosierslei', '2', 'Schoten', '2900', 1, '', 0, 0, NULL, NULL, NULL),
(991, 'Feugarolles', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(992, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(993, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(994, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(995, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(996, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(997, 'ddd', 'Vocabolo Vallata ', '27', NULL, '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(998, 'Fratticiola Selvatica', 'Vocabolo Vallata ', '27', 'Fratticiola Selvatica', '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(999, 'Fratticiola Selvatica', 'Vocabolo Vallata ', '27', 'Fratticiola Selvatica', '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(1000, 'Fratticiola Selvatica', 'Vocabolo Vallata ', '27', 'Fratticiola Selvatica', '06029', 7, 'Italië', 0, 0, NULL, NULL, NULL),
(1001, 'shimla, kasumpti', '24', '24', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1002, 'shimla, kasumpti', '241', '24', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1003, 'Feugarolles', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1225, 'Sušice', 'Nuzerov', '10', 'Sušice', '34201', 17, '', 0, 0, NULL, NULL, NULL),
(1226, 'OUDENAARDE', 'kerkgate', '72', 'OUDENAARDE', '9700', 1, '', 0, 0, NULL, NULL, NULL),
(1004, '', '241', '24', '', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1005, 'shimla, kasumpti', 'shimla', '', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1006, 'shimla, kasumpti', 'shimla', 'kasumpti', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1007, 'shimla, kasumpti', 'shimla', 'kasumpti', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1008, '', 'shimla', '', 'city', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1009, '', 'shimla', 'kasumpti', 'city', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1010, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1011, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1012, '', '', '', '', '40000', 3, '', 0, 0, NULL, NULL, NULL),
(1013, 'Feugarolles', 'Lencouet', '', NULL, '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1014, 'Cúllar', 'Aldea de Pulpite', '46', 'Cúllar', '18859', 5, '', 0, 0, NULL, NULL, NULL),
(1015, '', 'D4 Route de Vallon', 'hemin de combe grand', 'Saint-Remèze', '07700', 3, '', 0, 0, NULL, NULL, NULL),
(1016, '', 'D4 Route de Vallon', 'hemin de combe grand', 'Saint-Remèze', '07700', 3, '', 0, 0, NULL, NULL, NULL),
(1017, 'Saint-Remèze', 'Chemin de combe grand 417A', '1', 'Saint-Remèze', '07700', 3, '', 0, 0, NULL, NULL, NULL),
(1018, '', 'Via Palagio', '52', 'Montaione', 'I- 50050', 7, '', 0, 0, NULL, NULL, NULL),
(1019, '', 'Quinta do Pinheiro Manso', '', 'São Geraldo, Tábua', '3420-066', 4, '', 0, 0, NULL, NULL, NULL),
(1020, '', 'Les Marchands', '2', 'Chezelle', '03140', 3, '', 0, 0, NULL, NULL, NULL),
(1021, '', '12,Rue des P', '', 'Migré', '17330', 3, '', 0, 0, NULL, NULL, NULL),
(1022, 'place', 'street', '1123', 'place', '1234', 38, '', 0, 0, NULL, NULL, NULL),
(1023, 'Feugarolles', 'Lencouet', '', NULL, '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1024, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1025, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1026, 'Heffen', 'Steenweg op Heindonk', '82', 'Heffen', '2801', 1, '', 0, 0, NULL, NULL, NULL),
(1213, 'Zonhoven', 'Zavelstraat', '108', NULL, '3520', 1, '', 0, 0, NULL, NULL, NULL),
(1028, 'VILLANUEVA DE ALGAIDAS', 'CTRA VILLANUEVA DE TAPIA,MA6100', 'MA6100', 'VILLANUEVA DE ALGAIDAS', '29310', 5, '', 0, 0, NULL, NULL, NULL),
(1029, '', 'Chemin de la digue', '36', 'istres', '13800', 3, '', 0, 0, NULL, NULL, NULL),
(1030, '', 'Chemin de la digue', '36', 'istres', '13800', 3, '', 0, 0, NULL, NULL, NULL),
(1031, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1032, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1033, 'shaimla', 'kasumpti', '24', NULL, '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1034, 'shimla', 'kasumpti', '24', NULL, '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1153, 'hamont', 'windmolenstraat', '52', 'hamont', '3930', 1, '', 0, 0, NULL, NULL, NULL),
(1036, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1037, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, '', 0, 0, NULL, NULL, NULL),
(1038, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1039, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1040, '', 'LACHANIA', '43', 'LACHANIA', '85109', 12, '', 0, 0, NULL, NULL, NULL),
(1041, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1042, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1043, 'Feugarolles', 'Lencouet', '', NULL, '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1044, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1045, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1046, 'Castex', 'Au Peïmarti', 'Route D3', 'Castex', '32170', 3, '', 0, 0, NULL, NULL, NULL),
(1047, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1048, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1049, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1050, '', 'SHIMLA', '10', 'SHIMLA', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1051, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1052, 'shimla', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1054, 'Strumyani', 'Mura', '1', 'Strumyani', '2825', 35, '', 0, 0, NULL, NULL, NULL),
(1214, 'Zonhoven', 'Zavelstraat', '108', NULL, '3520', 1, '', 0, 0, NULL, NULL, NULL),
(1056, 'Marsciano (PG)', 'Via Madonna delle Grazie/Loc. Castello delle Forme', '13', 'Marsciano (PG)', '06055', 7, '', 0, 0, NULL, NULL, NULL),
(1057, 'wambeek', 'langestraat', '36', 'wambeek', '1741', 1, '', 0, 0, NULL, NULL, NULL),
(1058, 'Lebbeke', 'Heizijdestraat', '27', 'Lebbeke', '9280', 1, '', 0, 0, NULL, NULL, NULL),
(1284, 'Feugarolles', 'Lencouet', '', NULL, '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1060, 'Opwijk', 'Den Bogaard', '2', 'Opwijk', '1745', 1, '', 0, 0, NULL, NULL, NULL),
(1061, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1062, 'Feugarolles', 'Lieu dit Lencouet', '', NULL, '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1063, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1064, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1065, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1066, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1067, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1068, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1069, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1070, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1071, 'Turnhout', 'Harten', '8', 'Turnhout', '2300', 1, '', 0, 0, NULL, NULL, NULL),
(1072, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, '', 0, 0, NULL, NULL, NULL),
(1073, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1074, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1075, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1076, 'LISSEWEG', 'TER DOESTSTRAAT', '49', 'LISSEWEG', '8380', 1, '', 0, 0, NULL, NULL, NULL),
(1077, 'Evergem', 'Bernt', '40', 'Evergem', '9940', 1, '', 0, 0, NULL, NULL, NULL),
(1078, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1079, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1080, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1081, '', 'le maine', '', 'belves', '24170', 3, '', 0, 0, NULL, NULL, NULL),
(1082, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1083, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1084, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1085, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1086, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1087, '', 'rue des Maraichers', '15', 'Montillot', '89660', 3, '', 0, 0, NULL, NULL, NULL),
(1088, 'Oudegem', 'O.L.Vrouwstraat', '47', 'Oudegem', '9200', 1, '', 0, 0, NULL, NULL, NULL),
(1089, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1090, 'Hasselt', 'Boekstraat', '75', 'Hasselt', '3500', 1, '', 0, 0, NULL, NULL, NULL),
(1091, '', 'Le Chagnot', 'Le Chagnot', 'Mont et Marré', '58110', 3, '', 0, 0, NULL, NULL, NULL),
(1092, '', 'Le Chagnot', 'Le Chagnot', 'Mont et Marré', '58110', 3, '', 0, 0, NULL, NULL, NULL),
(1093, 'Alcaucin/Malaga', 'Carril De Las Alberquillas', '14', 'Alcaucin/Malaga', '29711', 5, '', 0, 0, NULL, NULL, NULL),
(1094, '', 'Heirbaan', '35', 'DENDERMONDE', '9200', 1, '', 0, 0, NULL, NULL, NULL),
(1095, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1096, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1097, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1098, 'Heusden-Zolder', 'Kapelstraat', '114', 'Heusden-Zolder', '3550', 1, '', 0, 0, NULL, NULL, NULL),
(1099, '', 'Tieltsesteenweg', '22/14', 'Eeklo', '9900', 1, '', 0, 0, NULL, NULL, NULL),
(1100, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1101, '', 'N8, Rua Barbara torta', 'S/n', 'Alfeizerao', '2460-191', 4, '', 0, 0, NULL, NULL, NULL),
(1102, '', 'Lencouet', '0', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1103, '', 'Rue de la Mer BP155', '234', 'Saly, Mbour, Senegal', '23002', 26, '', 0, 0, NULL, NULL, NULL),
(1104, 'lummen', 'St.-Rochusstraat', '37', 'lummen', '3560', 1, '', 0, 0, NULL, NULL, NULL),
(1105, '', 'apartado', '7', 'Boliqueime', '8100-070', 4, '', 0, 0, NULL, NULL, NULL),
(1106, '', 'apartado', '7', 'Boliqueime', '8100-070', 4, '', 0, 0, NULL, NULL, NULL),
(1107, 'Schelle', 'Koekoekstraat', '20', 'Schelle', '2627', 1, '', 0, 0, NULL, NULL, NULL),
(1108, '', 'Corro de los juaneles', 'S/N', 'La garapacha', '30620', 5, '', 0, 0, NULL, NULL, NULL),
(1109, '', 'Corro de los juaneles', 'S/N', 'La garapacha', '30620', 5, '', 0, 0, NULL, NULL, NULL),
(1110, '', 'Corro de los juaneles', 'S/N', 'La garapacha', '30620', 5, '', 0, 0, NULL, NULL, NULL),
(1111, '', 'Corro de los juaneles', 'S/N', 'La garapacha', '30620', 5, '', 0, 0, NULL, NULL, NULL),
(1112, '', 'Corro de los juaneles', 'S/N', 'La garapacha', '30620', 5, '', 0, 0, NULL, NULL, NULL),
(1113, '', 'Lieu dit Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1114, '', 'Lencouet', '', 'Feugarolles', '47230', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1115, '', 'Rue du paradis', '1', 'Tauves', '63690', 3, '', 0, 0, NULL, NULL, NULL),
(1116, 'Calig', 'Viejo cartera de Benicarlo a Calig', '', 'Calig', '12589', 5, '', 0, 0, NULL, NULL, NULL),
(1117, '', 'Plaza Asegador y Calistro', '38', 'Benitachell', '03726', 5, '', 0, 0, NULL, NULL, NULL),
(1118, 'Cúllar', 'Aldea de Pulpite', '46', 'Cúllar', '18859', 5, '', 0, 0, NULL, NULL, NULL),
(1119, '', 'Villaine den bas', '', '03190 Verneix dep Allier', '03190', 3, '', 0, 0, NULL, NULL, NULL),
(1120, '', 'Urbanización Torrepalma', '164', 'Carmona-Sevilla', '41410', 5, '', 0, 0, NULL, NULL, NULL),
(1121, 'Koh Samui', 'Tambon Maret', '441/14', 'Koh Samui', '84310', 36, '', 0, 0, NULL, NULL, NULL),
(1122, 'Avelgem', 'Oudenaarsesteenweg', '84', 'Avelgem', '8580', 1, '', 0, 0, NULL, NULL, NULL),
(1123, '', 'Calle Cierzos y Cabreras', '6', 'Iznajar', '14970', 5, '', 0, 0, NULL, NULL, NULL),
(1124, '', 'Bardouquet', '1', 'Castelnau-Montratier', '46170', 3, '', 0, 0, NULL, NULL, NULL),
(1126, 'Beaumontois en Périgord', 'Sainte Sabine - Le Merle', '', 'Beaumontois en Périgord', '24440', 3, '', 0, 0, NULL, NULL, NULL),
(1127, 'Antwerpen', 'Korte Van Ruusbroecstraat', '35', 'Antwerpen', '2018', 1, '', 0, 0, NULL, NULL, NULL),
(1128, 'Veurne', 'Houtemstraat', '22', 'Veurne', '8630', 1, '', 0, 0, NULL, NULL, NULL),
(1129, 'Haacht', 'Wespelaarsesteenweg', '17', NULL, '3150', 1, '', 0, 0, NULL, NULL, NULL),
(1130, 'Negombo', 'St.Ann\'s Road , near Church', '71/3', 'Negombo', '11500', 27, '', 0, 0, NULL, NULL, NULL),
(1131, 'Sint Katelijne waver', 'Groenstraat', '', 'Sint Katelijne waver', '2860', 1, '', 0, 0, NULL, NULL, NULL),
(1132, 'Sayalonga', 'Calle Carraspite', '', 'Sayalonga', '29752', 5, '', 0, 0, NULL, NULL, NULL),
(1133, '', 'Nachtegalendreef', '14', 'HALLE', '2980', 1, '', 0, 0, NULL, NULL, NULL),
(1134, '', 'Nachtegalendreef', '14', 'HALLE', '2980', 1, '', 0, 0, NULL, NULL, NULL),
(1236, '0644667047', '0644667047', '', '0644667047', '1011 AC', 2, '', 0, 0, NULL, NULL, NULL),
(1136, 'Brugge', 'baron de serretstraat', '109', 'Brugge', '8200', 1, '', 0, 0, NULL, NULL, NULL),
(1137, 'Póvoa de Midões', 'Quinta da Malhadoura', '1', 'Póvoa de Midões', '3420201', 4, '', 0, 0, NULL, NULL, NULL),
(1138, 'Walstrostraat, 5', 'Walstrostraat, 5', 'Walstrostraat, 5', 'Walstrostraat, 5', '8200', 1, '', 0, 0, NULL, NULL, NULL),
(1139, 'Arcevia', 'Frazione San Pietro', '37', 'Arcevia', '60011', 7, '', 0, 0, NULL, NULL, NULL),
(1140, '', 'Limburgstraat ', '8/2', 'Gent', '9000', 1, 'België ', 0, 0, NULL, NULL, NULL),
(1141, '', 'Limburgstraat ', '8/2', 'Gent', '9000', 1, 'België ', 0, 0, NULL, NULL, NULL),
(1142, '', 'Sainte Sabine - Le Merle', '', 'Beaumontois en Périgord', '24440', 3, '', 0, 0, NULL, NULL, NULL),
(1143, 'Beaumontois en Périgord', 'Sainte Sabine - Le Merle', '', 'Beaumontois en Périgord', '24440', 3, '', 0, 0, NULL, NULL, NULL),
(1144, 'Saint Maurice les Couches', 'Nignolle', '4', 'Saint Maurice les Couches', '71490', 3, '', 0, 0, NULL, NULL, NULL),
(1163, 'vaujany', 'Le Perrier', '', 'vaujany', '38114', 3, '', 0, 0, NULL, NULL, NULL),
(1145, 'HAVERSTRAAT, 10', 'HAVERSTRAAT, 10', 'HAVERSTRAAT, 10', 'HAVERSTRAAT, 10', '2350', 1, '', 0, 0, NULL, NULL, NULL),
(1146, 'Gullegem', 'schoonwater', '89', NULL, '8560', 1, '', 0, 0, NULL, NULL, NULL),
(1147, 'Zottegem', 'Kronegem', '42', 'Zottegem', '9620', 1, '', 0, 0, NULL, NULL, NULL),
(1148, 'Eppegem', 'Schranslaan', '40', NULL, '1980', 1, '', 0, 0, NULL, NULL, NULL),
(1152, 'shimla', 'shimla', 'shimla, kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL);
INSERT INTO `user_addresses` (`id`, `address`, `street`, `street_number`, `city`, `zip`, `country_id`, `country`, `region_id`, `department_id`, `label_id`, `created_at`, `updated_at`) VALUES
(1154, 'fuengirola', 'calle las palmas 23', 'calle las palmas 23', 'fuengirola', '29640', 5, '', 0, 0, NULL, NULL, NULL),
(1155, 'Oud-turnhout', 'Koningin fabiolalaan', '20', 'Oud-turnhout', '2360', 1, '', 0, 0, NULL, NULL, NULL),
(1156, 'Mechelen', 'Brusselsesteenweg', '564', 'Mechelen', '2800', 1, '', 0, 0, NULL, NULL, NULL),
(1157, '', 'Brusselsesteenweg', '564', 'Mechelen', '2800', 1, 'België ', 0, 0, NULL, NULL, NULL),
(1158, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1159, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1160, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1161, 'Arico', 'Finca Reveron 24, X4445113H', 'Finca Reveron 24, X4445113H', 'Arico', '38580', 5, '', 0, 0, NULL, NULL, NULL),
(1162, 'x', 'Ninove', 'Pamelstraat 16', 'x', '9400', 1, '', 0, 0, NULL, NULL, NULL),
(1164, 'Ichtegem', 'Sint-bertinuslaan', '30', 'Ichtegem', '8480', 1, '', 0, 0, NULL, NULL, NULL),
(1165, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1166, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1167, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1168, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1169, 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, '', 0, 0, NULL, NULL, NULL),
(1170, 'Geldrop', 'Oosteinde', '10', 'Geldrop', '5663PZ', 2, '', 0, 0, NULL, NULL, NULL),
(1171, 'Eeklo', 'Zandstraat, 154', 'Zandstraat, 154', NULL, '9900', 1, '', 0, 0, NULL, NULL, NULL),
(1172, 'Gentbrugge', 'Ooievaarsnest', '43', 'Gentbrugge', '9050', 1, '', 0, 0, NULL, NULL, NULL),
(1173, 'nemaiah valley', 'nemaiah valley road', '8160', 'nemaiah valley', 'V0L 1X0', 40, '', 0, 0, NULL, NULL, NULL),
(1174, 'Terlicht4', 'terlicht', '4', 'Terlicht4', '9451', 1, '', 0, 0, NULL, NULL, NULL),
(1175, 'kuurne', 'Burgemeester Decoenestraat', '19', 'kuurne', '8520', 1, '', 0, 0, NULL, NULL, NULL),
(1176, 'bierbeek', 'Bremtstraat,', '20', 'bierbeek', '3360', 1, '', 0, 0, NULL, NULL, NULL),
(1193, 'shimla', 'shimla', '22', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1178, 'Fuengirola', 'Calle Isabel Martos Aguilera', '1', 'Fuengirola', '29640', 5, '', 0, 0, NULL, NULL, NULL),
(1179, 'rumbeke', 'ruiterijstraat', 'ruiterijstraat, 45', 'rumbeke', '8800', 1, '', 0, 0, NULL, NULL, NULL),
(1180, 'Kester', 'Patattestraat', '33a', 'Kester', '1755', 1, '', 0, 0, NULL, NULL, NULL),
(1181, 'pittem', 'ter ooigem', '27', 'pittem', '8740', 1, '', 0, 0, NULL, NULL, NULL),
(1182, 'lauwe', 'prins regentstraat', '32', 'lauwe', '8930', 1, '', 0, 0, NULL, NULL, NULL),
(1183, 'Puurs', 'broekstraat', '24', 'Puurs', '2870', 1, '', 0, 0, NULL, NULL, NULL),
(1184, 'Herenthout', 'Itegemsesteenweg', '94', 'Herenthout', '2270', 1, '', 0, 0, NULL, NULL, NULL),
(1185, 'menen', 'parkstraat', '38', 'menen', '8930', 1, '', 0, 0, NULL, NULL, NULL),
(1186, '', 'shimla', 'kasumpti', 'shimla', '171009', 38, 'india', 0, 0, NULL, NULL, NULL),
(1188, '', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, 'india', 0, 0, NULL, NULL, NULL),
(1189, '', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', 'Shimla, Himachal Pradesh, India', '171001', 38, 'india', 0, 0, NULL, NULL, NULL),
(1190, 'Kuurne', 'Renbaanlaan', 'Renbaanlaan, 39', 'Kuurne', '8520', 1, '', 0, 0, NULL, NULL, NULL),
(1191, 'Aartselaar', 'Lindelei', '59', 'Aartselaar', '2630', 1, '', 0, 0, NULL, NULL, NULL),
(1192, 'Toronto, ON, Canada', 'Toronto, ON, Canada', '', NULL, 'A4B-94G', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1194, 'Berendrecht', 'Sint-jan Baptiststraat', 'Sint-jan Baptiststraat, 6', 'Berendrecht', '2040', 1, '', 0, 0, NULL, NULL, NULL),
(1195, 'kuurne', 'roterijstraat', '53', 'kuurne', '8520', 1, '', 0, 0, NULL, NULL, NULL),
(1196, 'kruishoutem', 'wedekensdriesstraat', '8', 'kruishoutem', '9770', 1, '', 0, 0, NULL, NULL, NULL),
(1197, 'Puurs', 'Eeuwfeeststraat', '39', 'Puurs', '2870', 1, '', 0, 0, NULL, NULL, NULL),
(1198, 'Deerlijk', 'Hoogstraat', '123', 'Deerlijk', '8540', 1, '', 0, 0, NULL, NULL, NULL),
(1199, 'Teralfene', 'Azalealaan', '25', 'Teralfene', '1790', 1, '', 0, 0, NULL, NULL, NULL),
(1200, 'Menen', 'Kortrijkstraat', '433', 'Menen', '8930', 1, '', 0, 0, NULL, NULL, NULL),
(1201, 'bree', 'genattestraat', '5', 'bree', '3960', 1, '', 0, 0, NULL, NULL, NULL),
(1202, 'lochristi', 'pachtgoed', '21', 'lochristi', '9080', 1, '', 0, 0, NULL, NULL, NULL),
(1203, 'Sint-Agatha-Berchem', 'Jean-Baptist Vandendrieschstraat', '12', 'Sint-Agatha-Berchem', '1082', 1, '', 0, 0, NULL, NULL, NULL),
(1204, 'Alken', 'Hulzenstraat', 'Hulzenstraat, 82a', 'Alken', '3570', 1, '', 0, 0, NULL, NULL, NULL),
(1205, 'Nijlen', 'Kempensestraat', '2', 'Nijlen', '2560', 1, '', 0, 0, NULL, NULL, NULL),
(1206, 'Pernes les Fontaines', 'Rue de la Condamine', '118', 'Pernes les Fontaines', '84210', 3, '', 0, 0, NULL, NULL, NULL),
(1207, 'Bellem', 'Nieuwstraat', '29', 'Bellem', '9881', 1, '', 0, 0, NULL, NULL, NULL),
(1208, '3 Grand Rue', '3 Grand Rue', 'VELINES', '3 Grand Rue', '24230', 3, '', 0, 0, NULL, NULL, NULL),
(1209, 'reninge', 'Lostraat', '23', 'reninge', '8647', 1, '', 0, 0, NULL, NULL, NULL),
(1210, 'Tielt-Winge', 'Roeselberg', '47', 'Tielt-Winge', '3390', 1, '', 0, 0, NULL, NULL, NULL),
(1211, 'Moorsele', 'Overheulestr', '18', 'Moorsele', '8560', 1, '', 0, 0, NULL, NULL, NULL),
(1212, 'Dendermonde', 'Veldstraat', '69', 'Dendermonde', '9200', 1, '', 0, 0, NULL, NULL, NULL),
(1215, 'Bavikhove', 'Eerste Aardstraat', '4', 'Bavikhove', '8531', 1, '', 0, 0, NULL, NULL, NULL),
(1216, 'Kapelle-op-den-Bos', 'Spoorwegstraat', '6', 'Kapelle-op-den-Bos', '1880', 1, '', 0, 0, NULL, NULL, NULL),
(1217, 'Warang - Ouoran', 'Rue de la Mer', '', 'Warang - Ouoran', '99999', 26, '', 0, 0, NULL, NULL, NULL),
(1218, 'Zonhoven', 'Donkweg', '66b', 'Zonhoven', '3520', 1, '', 0, 0, NULL, NULL, NULL),
(1219, 'erembodegem', 'Brusselbaan', '19', 'erembodegem', '9320', 1, '', 0, 0, NULL, NULL, NULL),
(1220, 'sint niklaas', 'kon. elisabethlaan', '81', NULL, 'B-9100', 1, '', 0, 0, NULL, NULL, NULL),
(1221, 'Bronstraat', '81 bus 102', '', 'Bronstraat', '2400', 1, '', 0, 0, NULL, NULL, NULL),
(1222, 'Oostnieuwkerke', 'Zuidstraat', '4', 'Oostnieuwkerke', '8840', 1, '', 0, 0, NULL, NULL, NULL),
(1223, 'Brugge', 'Albert Serreynstraat', '37', 'Brugge', '8200', 1, '', 0, 0, NULL, NULL, NULL),
(1224, 'Beaumont-du-Ventoux', 'LES VALETTES', '0', 'Beaumont-du-Ventoux', '84340', 3, '', 0, 0, NULL, NULL, NULL),
(1229, 'koekelare', 'Ter Heide', '1', 'koekelare', '8680', 1, '', 0, 0, NULL, NULL, NULL),
(1230, 'Les Brousses, Les Teissonnieres Le Bousquet', 'Les Brousses, Les Teissonnieres Le Bousquet', 'Les Brousses, Les Teissonnieres Le Bousquet', 'Les Brousses, Les Teissonnieres Le Bousquet', '30410', 3, '', 0, 0, NULL, NULL, NULL),
(1233, 'Wingene', 'Koningin Fabiolalaan', '2', 'Wingene', '8750', 1, '', 0, 0, NULL, NULL, NULL),
(1234, 'Recsk', 'Kenyeres ut', '13', 'Recsk', '3244', 34, '', 0, 0, NULL, NULL, NULL),
(1235, 'Borsbeek', 'August van putlei ', '90', NULL, '2150', 1, '', 0, 0, NULL, NULL, NULL),
(1238, 'Izegem', 'Monseigneur Bouckaertstraat', '25', 'Izegem', '8870', 1, '', 0, 0, NULL, NULL, NULL),
(1239, 'Kessel', 'Ringstraat', '9', 'Kessel', '2560', 1, '', 0, 0, NULL, NULL, NULL),
(1240, 'Hoboken', 'Grasmuslaan', '59', 'Hoboken', '2660', 1, '', 0, 0, NULL, NULL, NULL),
(1241, 'Beerse', 'Houtseweg', '81', 'Beerse', '2340', 1, '', 0, 0, NULL, NULL, NULL),
(1242, 'Cressy sur Somme', 'Route de Bourbon', '0', 'Cressy sur Somme', '71760', 3, '', 0, 0, NULL, NULL, NULL),
(1243, 'Overpelt', 'Leukenstraat', '14', 'Overpelt', '3900', 1, '', 0, 0, NULL, NULL, NULL),
(1244, 'Brasschaat', 'Hanendreef', '25', 'Brasschaat', '2830', 1, '', 0, 0, NULL, NULL, NULL),
(1245, 'Arfeuilles', 'Route de Chatel Montagne', '', 'Arfeuilles', '03120', 3, '', 0, 0, NULL, NULL, NULL),
(1246, 'Auw bei Prüm', 'Mühlenweg', '8', 'Auw bei Prüm', '54597', 11, '', 0, 0, NULL, NULL, NULL),
(1247, 'Montaigut-en-Combraille', 'Les Gicons', '2', 'Montaigut-en-Combraille', '63700', 3, '', 0, 0, NULL, NULL, NULL),
(1249, 'Echandelys', 'La Foresterie', '', 'Echandelys', '63980', 3, '', 0, 0, NULL, NULL, NULL),
(1250, '', 'ssss', '333', 'ssss', '1234', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1252, 'ssss', 'ssss', '333', 'ssss', '1234', 3, 'Frankrijk', 0, 0, NULL, NULL, NULL),
(1253, 'saint hilaire', 'le breux', '0', 'saint hilaire', '03440', 3, '', 0, 0, NULL, NULL, NULL),
(1254, 'Poperinge', 'Waaierstraat', '6', 'Poperinge', '8970', 1, '', 0, 0, NULL, NULL, NULL),
(1255, 'Praag', 'Na Bojisti', '2', 'Praag', '120 00', 17, '', 0, 0, NULL, NULL, NULL),
(1256, 'ingelmunster', 'ingelmunsterbosstraat', '17', 'ingelmunster', '8770', 1, '', 0, 0, NULL, NULL, NULL),
(1257, 'leivi', 'via degli ulivi', '39', 'leivi', '16040', 7, '', 0, 0, NULL, NULL, NULL),
(1258, 'Oedelem', 'Den Akker', '48', NULL, '8730', 1, '', 0, 0, NULL, NULL, NULL),
(1259, 'Zomergem', 'Zandstraat', '10', 'Zomergem', '9930', 1, '', 0, 0, NULL, NULL, NULL),
(1260, 'Merchtem', 'De Zippe', '39', 'Merchtem', '1785', 1, '', 0, 0, NULL, NULL, NULL),
(1261, 'Wilderness', 'Lake road', '675', 'Wilderness', '6529', 29, '', 0, 0, NULL, NULL, NULL),
(1262, 'place', 'street', '123', 'place', '110011', 38, '', 0, 0, NULL, NULL, NULL),
(1263, 'Vesdun', 'Les Charpes', '', 'Vesdun', '18360', 3, '', 0, 0, NULL, NULL, NULL),
(1264, 'daumazan sur arize', '4 chemin de loumet', '', 'daumazan sur arize', '09350', 3, '', 0, 0, NULL, NULL, NULL),
(1265, 'Lliber', 'Refugio Marnes pda Marnes', '20115', 'Lliber', '03720', 5, '', 0, 0, NULL, NULL, NULL),
(1266, 'Alfeizerao', 'Rue Principal', '115', 'Alfeizerao', '2460-202', 4, '', 0, 0, NULL, NULL, NULL),
(1267, 'Vosselaar', 'kruisstraat', '68', 'Vosselaar', '2350', 1, '', 0, 0, NULL, NULL, NULL),
(1268, 'Vinkt', 'Lotenhullestraat', '23', 'Vinkt', '9800', 1, '', 0, 0, NULL, NULL, NULL),
(1269, 'OOSTNIEUWKERKE', 'Groenestraat', '20a', 'OOSTNIEUWKERKE', '8840', 1, '', 0, 0, NULL, NULL, NULL),
(1270, 'Sint Niklaas', 'Moerlandstraat', '180', NULL, '9100', 1, '', 0, 0, NULL, NULL, NULL),
(1271, 'Ieper', 'Izegrimstraat', '7', 'Ieper', '8900', 1, '', 0, 0, NULL, NULL, NULL),
(1272, 'diepenbeek', 'kruisstraat', '15', 'diepenbeek', '3590', 1, '', 0, 0, NULL, NULL, NULL),
(1273, 'Antwerpsesteenweg, 29/7', 'Antwerpsesteenweg', '29 bus 7', 'Antwerpsesteenweg, 29/7', '2500 Lier', 1, '', 0, 0, NULL, NULL, NULL),
(1274, 'Bray-Dunes', '15 rue des peupliers app 404', '', 'Bray-Dunes', '59123', 3, '', 0, 0, NULL, NULL, NULL),
(1275, 'Hofstade', 'Kerkhoflei', '22', 'Hofstade', '1981', 1, '', 0, 0, NULL, NULL, NULL),
(1276, 'Kamilari', 'Dymos Faistou', 'Box 129', 'Kamilari', '70200', 12, '', 0, 0, NULL, NULL, NULL),
(1277, 'Geel', 'Holven 47', 'Holven 47', 'Geel', '2440', 1, '', 0, 0, NULL, NULL, NULL),
(1278, 'Belgie', 'Kortrijksesteenweg 321', 'Kortrijksesteenweg 321', 'Belgie', 'Gent', 1, '', 0, 0, NULL, NULL, NULL),
(1279, 'Mauprévoir', 'Lieu dit la Pinière', '', 'Mauprévoir', '86460', 3, '', 0, 0, NULL, NULL, NULL),
(1280, 'Kraainem', '....', '.....', NULL, '......', 1, '', 0, 0, NULL, NULL, NULL),
(1281, 'Wervik', 'begonialaan', '14', 'Wervik', '8940', 1, '', 0, 0, NULL, NULL, NULL),
(1282, 'Moorsele', 'Stijn Streuvelslaan', '20', 'Moorsele', '8560', 1, '', 0, 0, NULL, NULL, NULL),
(1283, 'Herent', 'Tildonksesteenweg', '50', 'Herent', '3020', 1, '', 0, 0, NULL, NULL, NULL),
(1285, '', '', '', '', '1234', 4, 'Portugal', 0, 0, NULL, NULL, NULL),
(1286, 'ontinyent', 'casa cami Garcia de l,Ombria', '16', 'ontinyent', '46870', 5, '', 0, 0, NULL, NULL, NULL),
(1287, 'BUGGENHOUT', 'DIEPMEERSTRAAT', 'DIEPMEERSTRAAT, 44', 'BUGGENHOUT', '9255', 5, '', 0, 0, NULL, NULL, NULL),
(1288, 'Londerzeel', 'Valkenstraat', '7', 'Londerzeel', '1840', 3, '', 0, 0, NULL, NULL, NULL),
(1289, 'Antwerpsesteenweg 136/1', 'Antwerpsesteenweg 136/1', '', 'Antwerpsesteenweg 136/1', '3970', 1, '', 0, 0, NULL, NULL, NULL),
(1290, 'Herfelingen', 'Druimerenstraat', '26', 'Herfelingen', '1540', 1, '', 0, 0, NULL, NULL, NULL),
(1291, 'Lauwz', 'Kloosterhoek', '24', 'Lauwz', '8930', 1, '', 0, 0, NULL, NULL, NULL),
(1292, 'Ruiselede', 'kapellestraat', '32', NULL, '8755', 1, '', 0, 0, NULL, NULL, NULL),
(1293, 'Vliermaal', 'Zammelenstraat', '26', 'Vliermaal', '3724', 1, '', 0, 0, NULL, NULL, NULL),
(1294, 'l van campenhoutstraat', 'l van campenhoutstraat', '32', 'l van campenhoutstraat', '2870', 1, '', 0, 0, NULL, NULL, NULL),
(1295, 'londerzeel', 'Valkenstraat', '7', NULL, '1840', 1, '', 0, 0, NULL, NULL, NULL),
(1296, 'londerzeel', 'valkenstr', '7', 'londerzeel', '1840', 1, '', 0, 0, NULL, NULL, NULL),
(1297, 'Kortenberg', 'Frans mombaersstraat', '186', 'Kortenberg', '3071', 1, '', 0, 0, NULL, NULL, NULL),
(1298, 'Kortenberg', 'Camiel Schuermanslaan', '10', 'Kortenberg', '3070', 1, '', 0, 0, NULL, NULL, NULL),
(1299, 'Veldegem', 'Halfuurdreef', '85', 'Veldegem', '8210', 1, '', 0, 0, NULL, NULL, NULL),
(1300, 'Wommelgem', 'Koralenhoeve', '15', 'Wommelgem', '2160', 1, '', 0, 0, NULL, NULL, NULL),
(1301, 'Veldegem', 'Halfuurdreef', '85', 'Veldegem', '8210', 1, '', 0, 0, NULL, NULL, NULL),
(1302, 'Houthalen', 'Mahonie', '17', 'Houthalen', '3530', 1, '', 0, 0, NULL, NULL, NULL),
(1303, 'Geel', 'Zonnebloemstraat', '9', 'Geel', '2440', 1, '', 0, 0, NULL, NULL, NULL),
(1304, 'Langdorp', 'Diepvenstraat', '44', 'Langdorp', '3201', 1, '', 0, 0, NULL, NULL, NULL),
(1305, 'Ninove', 'Langemuren', '15', 'Ninove', '9400', 1, '', 0, 0, NULL, NULL, NULL),
(1306, 'kolemveldstraat 12', 'kolemveldstraat 12', 'kolemveldstraat 12', 'kolemveldstraat 12', '3370', 1, '', 0, 0, NULL, NULL, NULL),
(1307, 'Sint Eloois vijve', 'Posterijstraat', '11', 'Sint Eloois vijve', '8793', 1, '', 0, 0, NULL, NULL, NULL),
(1308, 'brasschaat', 'guyotdreef', '114', 'brasschaat', '2930', 1, '', 0, 0, NULL, NULL, NULL),
(1309, 'Melsbroek', 'Sellaerstraat', '40a bus 101', 'Melsbroek', '1820', 1, '', 0, 0, NULL, NULL, NULL),
(1310, 'Merchtem', 'Leireken', '167', 'Merchtem', '1785', 1, '', 0, 0, NULL, NULL, NULL),
(1311, 'Hoboken (antw)', 'Scheldevrijstraat 11', 'Scheldevrijstraat 11', NULL, '2660', 1, '', 0, 0, NULL, NULL, NULL),
(1312, 'MACHELEN VLAANDEREN', 'RIJSKWEG', '12G', 'MACHELEN VLAANDEREN', '9870', 1, '', 0, 0, NULL, NULL, NULL),
(1313, 'Alkmaar', 'Sloestraat', '13', 'Alkmaar', '1826CV', 2, '', 0, 0, NULL, NULL, NULL),
(1314, 'cantecroylaan, 15', 'cantecroylaan', '15', 'cantecroylaan, 15', '2640', 1, '', 0, 0, NULL, NULL, NULL),
(1315, 'anthe', 'las coussoulesses', '1', 'anthe', '47370', 3, '', 0, 0, NULL, NULL, NULL),
(1316, 'lokeren', 'Kluisstraat', '12', 'lokeren', '9160', 1, '', 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_helps`
--

CREATE TABLE `user_helps` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_helps`
--

INSERT INTO `user_helps` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(6, 'What is Lorem Ipsum? updated....', '<p><strong style="margin: 0px; padding: 0px; font-family: "Open Sans", Arial, sans-serif; text-align: justify;">Lorem Ipsum</strong><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p><p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBLAHCAwERAAIRAQMRAf/EAKUAAAIDAQEBAQAAAAAAAAAAAAIDAAEEBQYHCAEAAwEBAQEBAAAAAAAAAAAAAAECAwQFBgcQAAEDAgQEBAQDBQUHAwUAAAEAAgMRBCExEgVBUWETcYEiBpGhMhRCUgexwdEjFeFicoIz8JKissIkFkNTY/FzkyUIEQEBAQEAAgEDBAICAQIHAAAAARECEgMhMUEEUWEiE3EygRQFscGRoeFSYnIj/9oADAMBAAIRAxEAPwD9Ste1wwKmwLSNEBCQAnITFdzDSQtOYHDvZTQ0XRzEV52/fWvNdHMRXAvONVvyzrjXLc1rE1zpmGqsmWSOqoazujzTGluYmNCWILVUIQNaotz3CIAMmeAMhVTeYNObvu7B1RO5L+uDXYtfeDmxsbNFqcPreMPks76VTt1ofcm2TMBdIWv/ACnAhZ31WK84h9z7fI4hrqEcHYAo/qo84I77t7x/qsBFKYpf10eUDL7msGVDpA5wOFET1UecYLn3QHHXGW0BppJId4ii0nqT5jZ7hsixrXAySf3nClfklfXT8hXV1czYRQB5zBJB8qhKSQWpG29iYO49sYeANJ1VBRcoZItu3Oe8c+emllAKEEkeCq9SQZXauLaA24Y+KVwrQnj45rGX5XjTt+3vso6RNL2SYuLjj5KeutOTGwWcL2aHNGl2Jx81GmG4tHGEsidoLfpI4InQFBZvY1okeHvbk6im9G0NY3EtadQzU2hAzSMcTzU2mW+EGhz6o0KERPH0jgMMVNphcCBgKEcUtNYZ6MeOVErQoM9NKerilplhjwaV9PEJaEfbxhpOdchwS02OUt1aSzHMGiZFSOu2AO06q4jnRL4MLbp5PqYSeYCVgWLkhhrQVU0Ct3FxOJfQ50wU9G1OjDoyAPGiztNypniFxDjpaUw5O5+7Ng2/ud64DpIml0jI6OLQM9RJDR4VqotOR5Ob9a/bTH6Y2SltaatNT40RlGxp2z9XvZ91M1ktzJaudhWdhDK/4m6gPEovNGvWN3a1niEtvIyWN30yMcHNPgW1CzxWss1295wKWAk3RbgXeVUsMv7mGuLiOuammXJfxt+khw4VU4ZH9SdzGaMN+k+zNHTSdbfmvR1ztDO7pBNPBTcC3dymAR8EyzXRFQcxmrkDm3NwTxWvPKbXIupia4rbmItcS8dWq25TXGusSQtuUVzJ2nFaRNYZmZq0skjFUIhzFQKMeKC1RiTGh7WKCUYigJ2kBfaqMQgasRkCnJA1fbPBATtoGqMSBqdlBaghQemNdO0ANkcBnQEpYNaI9w3JgIbO+js6muXipvMPyrdtW8XNvcmaasoLdJ5lZ98SxfPTv22/fdyduNojHHWaV8FhfXjSd67EXdltxI2QA1pQY5LK/CxQ2rhJqfO59fwgABK9A/tAEjhxx4KNMYDSXHKmSnTV6qam58v7EgtrSRWQaehStNTms04mo4AJaZZLssA35lIEumhDqOIpyCALuNODPippqJAGBoeSRqAAxOZSCiMKg+CWgp0YdnU04BLQhAxFDUpaZPbYw5DqloU+NpYcuhS0ywXUDQFNoNE0UED3zODdALjyoBWqi035/wDf/wCqe531wYNvP2kAPqLHHuHoThTyT4mn18Pmtze3E7tUry89VpIztZy8pkmooDse2/dO57Dei4tH1jOE1u6uh7eNRz5FTedOV932bdbXdtsg3C3GmOdtQ12bSMC0+BXPWsPcK4NFSpMl0Dji7AJaeBEDRk2p5qLVSL7X/wAbVOnj9C2u59wAscangV6/XGOfW+O94PaarPxBv38FMTRLxpMtzdWpaSSCr55ocK9uWaiGinnVdHPLK1ybi4BBxW0iXJuZSSVrIm1zZnFaRFYpeKuEyyNVxFZnsTIh0QTIBiTCu2OSZK7SArtICdlGgZtyMSKI0J2W+KNCuz0RpIIeiNC+wUtMQt+iNIQtyeCWmIWnRLTwYs0aMELPolp4NlqRiAptNtt57yFumORzRnSuFVFkq5a0R7huTTUSuPiovMVOq7G33pmYW3cpaXGgoKAeax75z6LldZjbV0QZGQ8UoSDUrG6sUUBb9ApzHNTaZz2NdnSvJQZMkWmuggCnFGhjfbF5qXI0MslrpNa18EaACQtyBB6INbZS01ONc1NMTbmppow6KQex5IxAaOFVNMuRznVDXAckgWYXn6pEvIYEWzdXqq4JeRjMY0kNAaFGmSYtP4wloYt1ge+xnaw1e5ha3GmJwzU9X4Pn6vzd7/8Aasex3EYdcia6m1umhaKCNoNGY1OLhjTgj1d22z9F+zj48v1eMcV0OcNcUEiAsHFBvtX6VfcO9qsLwNHekEZGekUGPWq5vZ9WvP0e3ZGD05LG1eKdbOIzFFF6XIA25HM/sU3o8D2hyU6ePfWe+XUdA2UgeK+q69cebOnVh9yXnGSviAs76YrzHLvM0uLn18MEp65CvRDr9x4qvEtA+7qKVTkLWOaUq5CtY5XVVRNY5cSVpEs0gKqFWdzVUKkvYmmlGNMgmJPQrslGkghKNAhCRwRoX2SloTsEo0CFv0RoX9ueSNCxbnkjTwTbYpaMMba9EvI8OZak8FN6PDW2h5JeSsELPop8hhgs+iXkeGCy6JeR4MWZ5KfI8ELM8kvI8OZbGnqFQptOQ6CEscDU+Si1UdiK/YWBr2kHmFjeVa0tibI3WDUcCs7cUTLGKVoepRoZsWmtPT80GCRr3DVGynVxSDG63mca4DyRoV2JRgfklprbaPpUuIU2gyO14HEeKi0xC3b+VTaYCzSa0S0KMgpTLoppkPfTDBIEucRwQCLiV7YZHNaHOa1xa1x0gkCoBPAKacfmb33ve5b7ex7ndQRwse3tRmIUa4sNDjU6i3IlHo5k2b8tfduT9HkXZrpcqqCiAFBLCA+v/ozfCbar6wdUvtpRK2n5JRT/AJmrm9/xW/r+j6A9raUxCwaKGhuRPmVFVC5Zjwr8VOGT3H8z8UsD00Fx1X2VjyNbYrjJRYetTbgkKcPRtkc7JLAexkjqA1HJLTNdZOI+rFLyGMr9vnqQMVU6LxJft1x+VVO4XiTJt7wMcDyonOy8Sxt2oZmvgn5l4skts5pIIVzpNhX25OQVanFfbEZhGjBttScgjyGCFk8iuk04lLyPxH9hNQHtmnOiXnB4iG3yk00mvgjzPxGNsm4tKXmPAX9Mk5US/sHisbc7iEeY8RN28peZ+JjdvPJLzPxPZt5wwU3s/Foj28g1opvapyb/AE+prpS8j8RDb+iXmMMbt4U+Z4Z9gEvM8V9gl5jBCxAS8zwYs2hTehg22bT9IqpvR4dHYNH1YDlxUXs8aiA1ullA0KNUU5jjxwRoAYTxS0B7YzrlwS0FvZGMEtMBHIYJUB0VOXxSoA5tMK0UmBxa0UqSkCXych5owyJS8+HRIM7o5M0gS+NxwJKRvnP6w7pu9htUVjt8cjm3rXm6uI2udpjZT0FwFG6q49EvrV8vjHue6bLdwxQxugs4IImW0LswCwOc4/3nvLnHxVemfFv30e6/Mn7OHTFbMAuPBBBQFhAfX/0VszFt24XroyPuJGxslJwLYwSQB0c7Ncn5F+XR6/o+jPLMyVg0Kdo4FTTJfQKaoFRySDswSt4iq+1seO6VmIXnF1OhWfWnHSZb2w/9QnoFntVh8It2HBtepSun8NH3QDfTQDopwav75oA4njVHiNUbx7vpcByR4jSvuJwcXYdE8haMPJzJNeeKAIscW+kYFIM01iXg4eCudFYzx2L2Hgnek4abOEmrsSl5U8NjhgYKCPUlbRjUwse0tEWJ5CoHkpUKMUJYYya5cEBtiifWhiAwzOai1R8YJjqY26hhQhTQU+3ic5pYxoc7NpATlGKkt4NAEkYEmVWjBEtGMzrIg1LMDkQFXkWLbat5I8hhrLZo4Kb0eGCJoCnTXobyRoWGAmgSCiWgoCg8Vyr0SAw1xOIHRTpiADaFxGHJIwlzK1p8UgIXEbM/klYYHXbK4YpYNCbvolg0JuzyRh6W66cUsBZnfzKMAXTvKWABkl5pABdJzSMJ7h4pAOl/NIAcHUSNxPcXuzZ9ggEt/OGvd/pwtxkd4N5dVF6kVzza+a33/wDQNlqlEG2v0gfyy+QAk9QAUXm/YfDzc/68+5HTExWts2KvpYQ8mnU6k/ClsejsP1S/8l2HcLC3jFrvroH9qEyaGPFPU5jzxa2p0rP2c/Hz9F+q/wAvh8d39ssV62GVoZLFDE17AdVDpBxJ44rT02WbP1Hu+ufs5a1YoRVADRBNEFhdTR9yKMvBkbEwNFS6R9SGtHE4JWxUj9Fe09hO07BZWLwBJFEDL/8Acd6n/Mrh762ujmOq+Dos9VhDrXip8lYW6BRelSA+3S08ddkdCQRiF9vrxGmMOBBGamhpZJLzKVh60NfLhipwzgZM8VIMAeg1iNyCEGP5pAbWydUjMa6UJAeuU5koCBpOYQDWCn4R8EjMYXMNWih8Ega2aUGowPQJYemiaY8fklkGmCWYmtcfBLIY2OfkXUPySoG6Ig62vq7iloQRPkFHOwRpj7E7RRpq1LYMB9rJnTFHkMX2H41NCjRgXRnmEaAFpyJQAkDmgKcAaYZJBXhggKNUjUdSQVRyRqLSkFaEtCiwpaYdB5JaFaDyS000dEtCaCptNWg8ktCaDyU6AlhRoA5tEtN4b9RveV7sNi/7OIl7wQLjMNdSoWPfd3I144mbX53v9/nvZLm5u5nyXkpB7jjU4nELTwHnMrz7nGq1YAqmQoppIpGyRkh7TUEGiVhyvq9l+ice+bbb7vYbyWRX0bZmsuItbm6hi0vY7GhwyWU7z4aWa+c+5Pb17sG8XO13hDprd1C9ldLgcQ4V5hXx35RPXOOZTgFaVho4/BBPoX6M2/e90ucdBbBbyODHnGri0aoxzHHosPf9Gvr+r7lowyXFW8Lc3HJTVEvb0UVUIeFCiqdDmkbo2+4bS9/8xkjBwoalfdXnp4ex1oo9teAYpxQ8HZrO2qyNMcFsDp7gr0p/FTbTyNTLe3GJqfFpA+KnaeDEMZBLRgONUtGBDBwCNLBtYa0ojRhrYHHIV8EtGL7RGYRpiEfRGksRpaBiJGgbY0jMDEBYaEgfFDqaTyU2ng2iP8vxKQF6Gn6PmgzA8tGDR4JYAmZ3DBGDU7siMC+7IUsGq1P4oASHHNACWVRoV20tCdpK08Ts9EtGJ2UtGJ2UtPE7KWniGEUS0YHstHFLQhY1LTAWtSATo5JBRI4NJSMJ1flSASHpBWl3NK01GLmSp0YExNRoxw/cftux3Wymgn0sEjHNL34NGFcVz+//AF1t6rlfkr3Ftcm27hLayfXGcaLo9Ps8+dL3+vx6xxzmtnOFAQFAfTdg/UHdNq/S6a3294Ze214LfvnF0UNw1z2ub11NLQeCw653prL8PAv3Pv2ksd0XSTl7pI5neokyU16iTXhVX4Z1sP8Asnjl+v2Yw9wa5rTQO+rqFpjHUFc0w1WV7c2dxHc20roZ4zqjlYS1wI5EKbNOV92/Tv8AUKPf7f7K+LWbtC2pIwEzBm4Dg4fiHmuH2+rx+n0dHHevaOezNc9jWESv5KcPWSUvNcUsPSNLuaRnxbPfZGMj4L7rzjxPGtcO1XoNO26nOoSvcHjXSs9puC8dxr2jngs+u4qcuzBt8kY0/cSYYgLG9fsuRuiYSCJH1DvpBH8FFM1sNnTIV80toELeJw9IAHNGgDoCPpeB8k9IsxSA51T0IIZEaRsdvX6nEeSVp4cLdo/ET5KfI8WIeVUaMGIHdUtGC7ZAFTgjQlWgU1oCw5n5q/FIDaAefwQZmlSE0I0JoKNC9DktNO2UtGIIkaMEIgp0xdtqWgJYloDpKNCwzxS0xCFp5qbTxfaA4FLQAtHJLQEx9EaFdsVS00MLTwS0K7TRwCWgOlo4hLTA4M4uCQA4xcykAF7AcG1SMBk/uowaWZHcGhLBrn71E252y6imeI7cxuM7yMBGBV3yCx9+eFa+m3ymPyx753Pbb3e5ruxj9BL2jWA7U01aCQfxUOaPxPX1zzldH5Nm/u8ZMwseWnChXbK8/qZSimSkBv2m+ltZJCyQNBY4gOAc3WGua12k1Goajp5KO+dVzcYeitJjRzTJZcEBYOCDb9n3O623cIL21fongeHsPhwPQ5FR1zsxUuP0V7Z3+03/AGiLcLf06vRNFWpjkb9TT+0dF5ns58bldXN2Om5qytXhL2BTaeF9sKdN61haQPVQeAX2byjmmJv4q+ACQOjfHXCvwSsDS0sJGePRSDmlo6JAwNbxaPikEAi1UoKo+Qsm3GB0gjqj5AXS2uWseSeUtid62H4ifJGUbEFzFXjTqEeI8hi4i6/BLxPTIpYicKhKyjWj0njVSpCwckEtsVcgjQMxaTQ4FLRiaAjQmjHNLQv080glW8z8EGvU3mUsCdzDAVRg1esD8JSwavvU/Alh6ndP5Qlg1Rkf+UfFLBodT+SBqtb+SR6muRLAvU7mEjMLNMYe78WVOim0wNIeaaqdCkAPY8GhRoAWu5pBRYUtATGUtCdkcUrTwwWg06jkovSsCbfAkNwGCWjCZI2tOIxRoKcxiA5+9bXb7jttzZSktZPG5hc0kEVFAcFl7ePKY09Xfj1LH5Qvdolsd/ks5YBI+J743RvGHEV8s0c+3fXv0dt9O9/Dy+7j/vpjUO1GuoZGvFdfq/1jg/J/3rAVowUUBEAbQEyGTw+KAoBAWEjECg3vv0n90M2zejYXL9NpuOlgJybOPoPn9JXJ+V6952fZv6uvnH29xqvNtdMhT1Np4Vjy4qTdKK5caL728vC8m2K5f5KLyryamXhHD5qfE/I9l83w81PgPI9t+zqfNLxHkc3cRwGCXgfkYNwr+EJeA8l/dMJFYwjxLyEJ2HOMJYNNbK3IRpYeoKOdQR4pgwMdXBtTlQYlSZrYX0rppxoc0tNWog00owtOg7j3AAUbWhKm/Bw6V7RE4N4fiShsjJ3MeXEaqihBVYWn275J36MAeGCm/Bw58b2kCmqvEKZQOTtxEBwqQMfFKfJhYSGa5NIY76SRjhyCKAvu4wCGNqeDijxo1bZnvbUChGaWDV65Esg1dHHiEgnbPMJaeL7fUJaeJpCWhNISCaGpaawxp4JaD7iJ3bbXIBZyqxh0gOVaTXGxjo6OHq5qLVYQWAJ6WBLWpAOkcktMUcDnuoBhxKm9HIdOA0NazJuazlWzd1zcBlWqeFpMhLnE0QRTh0RoKeOiND5V7h/S9zru93Wbcdc07naT2w3SH1A1GvBcPXqvHP1+Hpev8yfTH5933apra5lZK2ksTRqGeNT+4L0vT7JZ8OT8j0/dwiMV0OJNDjwwQA5ZiiANpTJdeHFAStAkEBQaw5AMjlcx7XtNHMIc09RiErFR+o9suvu9ttbo53EMcp8XsDj+1eB38Wx6ENeotVhVf2parGmNx8F+iY+b09hdzKinrQwHqkGhjeimmewHkpqj2tPJSDmgpA5jMqmlckjbYYAI9ZI6k4UHmotVIRLuDwdMXpaOPE+ac4K9NVvOJoj3CBJ+E5EqbMVKc1wt4ySRqOTeJU/U/oxvunul1ElX4p10YhFIwODtRypl8SVndi4u4uBDpZGcKY+KUmi3GZ9xUUB8VUhaV3sc08LW3b5dJc7phVZ9xXJ75ZA8Oa/IYUUzDPjfCWVnxcceqi79lRjuJg95PDgOQWkmJpVWoDRA9gA1/T0zUdHGgMY+pZl1UaeDEKV6PF9kKfIYsRdEr0eL7fRLRiu2EtNbYK+HEpXoYcy3YwgvIpTJRelYXczuJLeCJBWXS3NO0lSTlg0tUmBk75CGkCiKGhsIJAJAJxoovSpBm3Y3EuwU+R4pz8w3BowUmzSuIBNCmTMZaJggzODqg4pBYmL3eqmKQLfK0O0gV5qbTkeF98+7XwWlxZ7daS3V04GLUWFsLXHm91A48fSuH2+6d/G5Hd6PRnzX5l9wz7ib2WK4FLjWe5pINajIEdF6n4/PPjLPow/K9nVuVyBAQNUmHIcT4Lo1xYpxcMmmiZYU8hwx4JkAYeKAutPFAUgLQEGaDE04hI4/S3seb7j2ftEhxebZjXAf3Ks/6V8/+R8d9T93pcT4jryCgxwWF6XITqCnVY29kseW504r9I18zhsZbq01GrlxU08a42EqbTw+N8QfRzwCMSCeCmnGhksbsYtLgeZx+CnDaI7hxIAAa3i2mBSw9aXCABvbBc53Dgp+TZ7jc2W81GNa4NoCeqc40rcJfvMsjCzANJyVf14XkQbjjVPEiZeEZFHietLb17wKmtFPier7pRgNjvXsbpBwOYU3k5V/dFxrWqWBfeJQYmvqalKmcJXUwUYY4rl7Hg1w4pWHK0Pug8VBoeSiQye45MhCQpAWt2BCk2y2uQyrXfELPqKjbDPHJ9Jx5FZ34VDwFOmt2hpALvFTp4uZzI2uc0atIr8UpTxzfvpS+rjUcleJ1vhuYXxu5Nxos+oqM0l8HvId6W8CiQaAO1VLHaqZoJnMjwSa5pgBLiloHDq7jdP1VU2qjqujFAXZgLHVlXhLYmk/BKCuPJfPYSK0HJXidIfuEla6q9DkjBoTdtIxSCNd3PpIJ5VolaZfcNckrQVfzi2D6kNMYq85mvIUXD7vc6eOHhveV7FcRBsj3RsNKu4tB8FyW706fXMfIdwsbS+3C5fDDS1gIDhXF7jXQyueOkk04VK9L19Xnn5PvidV5Hd2QfcuMUemKM0bUCpPGtOuQ4Bd3q3Pn6uD35uT6RypDUrZzESgaSeKcInIdUyRARAQoCBAGwVcKZpKj9S+2GW23e37Gx1kGOCJtCAKODBqHxqvmPb3eurf3etJkLv7h4NWurRRFYz/ANQd+QZUT8Txypvc9sJA2S6BdwoSafBfp3jHy/j1W22uC7TLG+oOLXAosQ69vut0LgSOdVuFW8KLO8TFTs+6nFxcukYNIPBLmZB1dro7QwOlAc6mGDuRWfsXw6zbd/dEYkBfyzCx1eNOuO1jewOEtyfSByB5Kfr/AIP6OJO0l7i40IOI6reM6K1tnSv0t8zwAS66wSNM1gWYtJe3iaKZ2d5AyHGlMU9LHQg226JoInVpmcBRZ3uLnNR8DmOLXgtcMwUaMD2wjRiAAFIGtAzSMwBTpjaTlRTTXXFI1gpA1kcjslNp4MxyMNHNp4qdGGNBPBTaZsdvM/6WEjnRTejxrjtnR0fKdBGIaM1netVjSy8i10xA5lReaepPdQAGg1E8kpKelG6ie4FwOVCOCMGijto3RveAHEn0gqb0cjO+GZjyYwW9E9LCJO641KNC49bXckrRGk2rtAc4gE5KPJWKbZimpzgBwCV6PGqCGKNpLSNTjms7VSGNAB1l1TwCm02K9utbdIJwTkK1x54y5xJOKvUs7oeqNGAMfVLRiqEcVOmjZJGuq12IU1UYfcc7QWT3L6tyZbjDW8jDyXk+76uz1z4fLffO+OtLNxcWtnkbQajU+o1waOJ4VVfj+q9dfLp5kk37R4eyZd/0yOLVSS4L5Y25BjX/AFSOOf0s+C7++p5ftETfH/Ly28yRmfsw/wCjD6W8yeLj1JXd6p8bXne7645Ema2c5ExwA5pwqT14JktARAVnigJWiDdX2xZffb/YWrsWyTsDv8INXfILH39ePFv7NfTzvUj9DiR73dXL5p62NxhgEDQQ10lMarK9HjN9vHyb8UeZ4+PtlOdV+qPBdba98urJw0O1R8Yzkhl365XsNt9z7dPQSHsv5Oy+KVc3XqseihmBAcBgRUHmFKY6VtcBrcFl1GkrpDcHOiayHUK/V4rPx/VetMkrbVjQQHvfUlxGIKifKr8E2sQmL3PFWAVLuRVdXEyadDEKuij9WvJ+OQU2nI3RW7rduozFreLRmVF61UmGRXdkJy8YSkelxGAPNKy4ew4RX0jjpuAWnEmtPkp2foeU+3tHEuZKWSDg4mhqpvYkJuNslGp0ZD28mnFOeyFeS2bZeOAPaOPOgRfZB409m1StaXSOaw8Gk1r8FN9p+CDbbovADdQP4gahL+yH4jdt1w12kBrj0P8AFT5weJMsE0JpIyic6lGAaW16otBjbgtOCmm0NnErh3AcFF+DdOEtEBELQDxrmsbfn5WKP72UBpoAfD9yVsg+TJLWgo6ca+qnyPCDt9w51Q4Fv56p+cHiBtldF1CMODuCV7g8TorCQPBfQjkDipvZzloLZ2jTGygAyqo1WF9u5LhrYHUyxS0Yvtyaq6BQY1KWnipYNZyGoeKXkME+CYsoKYZJeR4T9pcUxIHRLyGCjtJB9ZGHIpXo5Czb3Lq+qgCXkMJksJSCQ75I8x4sp265dU4Dklex4s0lhdDhXwR5wvEp1nc/lKXkfiU62nGYR5DGW+vLTabGfc9wBNtbAExt+qR5NGsHic1j7vb4z923q9V6uPLe6velvtmyR7lcRMbusxlMFpm2Kp0jV0aG5DMrz/x5e7XX1xJc+z4Nc319vW6d66kfNrkL3hxzqfUcOJyXsTmcco7t6s5n0dz3Be/axTB5H3koHeDQAGU+mJo4aRSvkOC5vTx5X9mvs7nPOvAzHU48Scar1ZHkdXWKTNUhllJMhHAYKomgQF0QEKApARBvoH6N7My+9zuuJRWKyhdJ/nf6G/8AMV53/kvZ4+vP1rr/ABefnX3BtrBHi0Y8818/a9AmWCMmuJS1RWhvL5I03xNr6L9U14RzHo0miKWnFGpr23sf3GIrptlfSj7OTBvdxa13DHhVZ9z9GXXL6laWm2ZgagcAcx5Ln666E5jsW8EULSY4wNXILG9aqQq5szOayvOGWGSc7wrNMisbaOEs1V/E7gle7p+LFLLJ3tNvqww0hXPp8lTHQX4idI5h0HM1ql5QZWAh4OCrUtVvdXTCCDXhQ8lNkOV3Nsj0WrrmQ6pCSGspgOpWHd+cactEW4hsobI0Y8QKFReTlS4jAf3Z52wNdi0OcAS0ZpToYxf+Re3oHhjnuuDxeBgEXnqn8Ota3tpfgvsZGl3jQivMLO/H1M7+mFztUstCMfTnVT/YfiKS1tHAB1SeLuKU6oxnftdk8+iQiuVU/wCyl4xoh2izjZR3rceKi+2qnIn7TaH6QW045pf2UeIBZMjJ0SEBHnowxkxP8vSTwqFNhxqZaMpqLRjxdis70rBSRkkULS3lklp4nbBbQmg6JaMFVgFAaBI1AgY6ktCnSNH4ktMBmYPxJaMLdcxZa0gA3UVKCQJGS65b+YHzQAm6b+ZIBN6B+JI1fejmkeoLpp/EpATKeCRlmR5KQR8YcMfilp4+Y/rHuwt4Nu26KpfLOJHOP0jQRTLxXL3PLv8A/Wf+rv8Aw+Pi18O9y+4Lvcyz7l5eY2hlScCKk/vXf+P6Jx9C7610Ntt2bVtrdzlFJ5hpsGEflwdKejD9PN3gp7vlcT9Hmdxu3zvq4kjiuz18Y4vd7Nc6QUY53PALaOasDjmSqSyHGvVUlEBMkBRKApAW1Bx9l/QhtqLXdXuH84PhBP8Aco6g+K8P/wAtbvP/AC9H8Sfxr6dcX8LAdAFeC8d2SMEm7VFCKDoq8DxX9Vj5cEvCnj4gDzX6nrwjWOQRzHI1NaYZC0hCa9Z7e957jtoEReZrate04nA/3TwUdcyose62X9SrCWjLsPgkrTW31Np+1Ydeoa+hbfLZX1sJobhs0Zyew1XL18LjT/TYHYaquPHop86eHjbbRrQ0sAJpiOin+yn4nthgYO3QUCnyoxlOw2b5C84A/hGSr+6l4K/pm12pLpZmAtFaPcB+9L+20eMZbj3HsFpblvfa9xJ9MfqR49WjY8ju/uwTkNswYh+Jxz8lvzx+qL04M+53ErqySOeRlUkq/GJ0sXLic0jbtq3iazuo52H6HA0qRUDgs++dVzXsh77glFSx0ZpkMcVz/wBWNPJog3uK6Z3GSf4mnAhHjg0+LdYWOxkbXxU2B1rfdY3NAqKLG8qla47yOhxUWK0t17Br9RFEZQjt2to66KDqFPjT2ESb3H+YlPxHkyyb7pHoR4l5M532cnNHiPIR3G9I1AHSlkPTLK+ubm4bDU0JxpyU2HK07jHdtIbbNc8AeriplOuNJf3bHEPqCOBRhM8l7cPNS4owBFxPzKAITz1zKkzBNOeJUmJvfdxKWg6OCc0xU2qwzsytOJS08EYphk5Tpray4z1KbTKu7h0ERkmlEcbc3OIA+ajrqSbVSa+Mfqjv9tebnt7IXiR1vOKlp1CjqUNRhXDgVh655Xq/s9D1c3jj5fI2tjkv7eGZwiE0rWyPP4WucAXHoAar1pPj4c17kvy9/wC7vbW6TTPuGGGDboQIbar/AEshj9DBgDwFa8TiuD0fkcz/ACrvm2ZHi962WDbXiOS+gmlLQ8xw6n0qKgF1KVK7/V7vP7XHH7PVn3eduHEimQ4BdMc9jJLTQVUTWSqpCZIAScUBOCAgQBNzQcfZf0126TZdolnuKtuL4teYj+CNtdFep1VXz/8A5D2f2d5Ppy9f8b1Xnn5+70826xVrRcU9ddUjDNusVTgtJ6qeM39UZ+UZqv6jfPGlfouvnTGlGg1p4oI9hT0q0xvoknGuKUgjFJL1Htf3dfbNcdyF2phFHxOyI/isu/XKNe6sf1YBkH3EFI+JYcarn69B+Z1z+qry0C2iAcCdTnY1bw81P/XHmCP9ULtw9cTXO4O8k/6B5nyfqhcuhLY4Gsc4UBrWnVT/ANcf2PJ3G6TyvLnvLiTUkmq2nLO9Fi5ceKeFqxLglQgeTippja5KmY1xBUVRzJnDipptEdw4ZHBTYZzZ381NNtst3uLd3pcS3i0qLFa71pvrZhQu0u5FZ2G6bDrjLnupXIrO02ZzngkVqmSUcQUtMpzXJaAgOqptN0beRhj7Zqa8a5KKqNdha3EM7ZImmQ08KKLTkdm51saKH1UxUatzJrVl08CfAjIhGljkXVm63lLfqbwcnpYFodTJIC7TjiEtAmseOCVqjmO05qaZom5FSpTpDmkAmfqppubufuTadsp99dst9QqNZOXlVRe5ufdpx6+uvpHzv317/wBtuGiKwLrxxDTqo5kcYIq4YgVcemS5uvVe+t+zv/H9XjPl4bZ9uu903KW7ldFFHGwyzTXBJhiAH16c3OjbVwAC38ueZOWntlzXLZuW0W0NInSXb2vLWmVrWtdG0n+Y9oFXOdXIuK6OvX31f0cPPXM+vyK/9x7fuduGzvNnLGSJJYWFzZYz+B7AWjUMg7lnkp9f4/XF+PlXXu5rxk0hdUtrp4lejI4er+jJK6p8FcZ1muDRlOaqM+mcNTTiOGCAUmSwEBs2vbbjcLtlrblnekB0B7tIJArSp4qPZ7JzNrT1+u9XI9z7b9hts5G3e6Fsk7TWK3aasaRxcfxHpkvN/I/M8vjn6PQ9H4nj89fV658zxhVcEjvZpJSeKuQM0jiePirgI1deKsnmQF9u+fMaEA5oTI5gQloiCCrTGEk60xkpE1xE80iaWONM0irRG4pUjmOPNSD2kpA1v/1S0GtrQKbTMY3kptMwNKnTG1pKjTNa0pWmazAqbTNGCnTGCFNpmseQcFNpuztm8FgEMprHwdyWVhuzqBxacDxU6aCQVoVJjaWVSMzTEcsio02/bYrYOGs0Ix8VFqpHb7rGsAYKN4UUKJfLrqDlwSNkmLmsNBVyNImGJkgrN6jwCWnjNI2LuEZYo0sE1kICWmp2jgkZbgCloBiEtMLnvCWhmubsQxPleCWRtLnAZ0GKz9nfjLV887cfnL9RPd13vW6XRee3C2U9mJuIAaNIxzyC0/E9WTyv1rt9n8Z4x4ea6nJoXuNOZK9Ccxx9dV2di9yT2u33m3kVbdwPiY/iwvc1zqf4g3Ssfb6Jep015918PFye89rqHwIW+OTyCXlpqDgUywiR9U4VIJGapOM7zrd0GSaL8gpRNIX0ogFUTJYCA0WVzJbXUVxHg+Jwe3xBqo652YvjrLr7LaXDbu2jnjNWStDx/mFV4Hc8bj3ebs1JGCuJoOaUqmSYhuRqtOQyySBXIWldzoqwtcBoX2rwDWBMj2jh8EJNaCgmiMYIKtMYNUtS0RtywSoaomFLUtTGYJaR7GcVOk0RxVS02qOBToPbCAFNpmMjCm0GiMKdUc2LDJTaaxGVOmNsZU2qwegqdPBNjcVNoNZC4+KVpmtt38sFFpmNgcDkptDq7dPIwdp9dP4SVFpt9HZqdMbdQSpnsyUWm1QOxxUVUdWCfW3ScwoqoIyBpISMDpOIxSBEs2lp058kjc+TVWpQSR69QA44ItMyRj2Oo4YqdNGgUqUtCOcAcEjLM3MBTTZL2KO4t5YSSwSNLC5uY1ClQo6mzFc3Lr81+/fa257Rud0yZh7UlXRTgeh4JzacvEcF1fjdzJK7O/5828vDyRTNHprQcc13yxwdcdRTDJCA5xxJqP3Ivynm2fVpLmSCoz4qfoqzQOwH7005hD3iuGKqItZpy4t5DkqjPqliqEoc0AD8kyLTJYQFtzSOPr3sybX7astQqWhzK9A8gLwfzJ//AEr2/wAb54jr3ejtmgAK5+Pq6McWcOLl1cpsZSxxOKvSxXa6o8h4uI0L7N8+axoonqacxtUFT2MQTRG1GprXGAlpNUbccktJqjj/ALEtS1RxjDHxU6TVHGOSVptEbMQFNoamNFKqLTNayqm08NbDglp4e2AVCi1WHCEUU6eDbbPOQU3o5DY7F9cVF6VjQzb1F6PDmWDBmFN6PGhlmzKim9DDm2sYFKKb0eGC2ZyU+R4Y21apvR4exgAoClp4jowc6qdGCjq3ilptDJmjPDqp0N1pctac8CpqobLIHVoUjJbLprqSoR1yKEBuBzSNmkfXggJCXGQNAxqlabXPDLTmo02Quc0Yo0FOeSlpllyVplvcQotNjvrazvIezdwMniJB0SNDhUZGhUWq5tn0eI91exPar4Xvh29kM0okkfLHqqNDagNZq0DW8gZKevyOubPl0erq36vhG72PYuBERQV0jwGC9n0+zymuf3evKyvio5zmkjSaU4LWVhaTIDQEmtU4i0sjkmkmYjTRVE0AAoEEAY1PVMgSZIKlpkgQBtGKSo+v+042xe3bCowMerzcSV4H5V32dPc/Hn8I6V40OYXsxWHF+W7kS6q5LphUqjycAqIOhyNPHDa1faPmzmtwTScwI0j2FBHMS0mllOCWk1RE0w+SRVpj1YcilqWyInBTaG2JwpTqotPGmFuOKVptkbG4KLTxpjjFMlFp4c1lErVYbFEXHFRaeNscDMFFqsamNjAyUWmMBvJRTMakDGtKk0oQp0xDUeiWgxlQpM0VPFIxNJUmYKcVIW52FAEjL44pGZGCDgkGtsnApKWTUYJaAhjieSVp4MQF3JTp40QRNjdqLhVTaD3OjOZSNjuGQ4kO8ktNhkc0HoptPCXSBTaeEvkU2mzySKbTked96Oljs7QhxaJBJI4A0BawVAPmFh189On0R8L90UO5wsAyFSV7P4n+tP8AK+rgk1a//Gf3LseZWeU+keKqIoCUyZ7g4DxTiaFxTIDcgggyZFMqUgkQDGVJQqPtWwhrtksNAoOxHh/lC+b9/wDvf8voPV/rP8Gz1DTTBTy0YWw6yQTitfLDxT4XtaRWqJ0bPpd81epxwGhfba+YOaKoI1gQRzRxSI5gr5IJpjbklpNUbT5JaTXEFNpNsTQptNsibzUWnjWxtaKdDXCw1UWnG2JmCm1eHMYQptPDmNUWnjQ0UUWmawKdM9jVNpiwCnQJriUrTOa2oqVNproEtNaWhNZS0GNKmmME8EqYwUjTUApMxj2AdVJo6QJGDvkJGn3ZHFTaFi+6qdNDejOuKm0wu3A0oClps0l448VNpkOuzzS0AN1XiptPAOuRzU2nIX3dbg0ZuNB5qLVSOL79kZJYNkaRoga+FlONXhv8Vhz1vTq9Myvgu/S6t66NFP2r3/xp/Bn+Tf5OR+F/PUf2LqefWaXh4qozoTmmTPN9TfHFOJoZAmVCMgggyDAplSUyWkBNNDVCo+27M10O02UTvwQRj/hC+a913u3930Prmcz/AAbLR3hzUxqToaD6VWgNw9jWV48Ucym5/wBwznxW3inXAaa0ovtdfLmtqjSOZ/sEaR7RXFGk0RsrwS0m6GIEZKbUtbIB/FLQe2Ijqp08aoYzhgotPG6JhKm0NsUeCi1UjXExTaeNsQACi1WHAKLTE35KbQa0KbTPYFNoNa5SoWBKVA26QkYi9Sa2lICBS00qFOgbUtPBA0S08QyFTp4HuJWmJstFOmLuVStMqSRTaCjIVNplPkPNTacLM7hxU2nid6qnTxG1cxx5JabK95U2nhLpCFNqsLMzlOnh1lMG3DZJPoiBe6n90VUddfCpHn/d95NJ7ct5JmhkkshNOTdTiB8FHpn83V6p/J8P3R+reZDWo4+NF9H6Z/COX3/7MH/udHfuWzjrPLw8VUZ0txqmlnkNZAOCoqqQ4ITQjJBAecHJlS0yRIzLdodMxpyc4A+Zolforn6vtEc4DAwYBooB0GC+cvL6TAOkccinIYe64Iw2S6mJaalacclXP1HnxW7JgYvrNfNnsFUamnxsNUaTTEw1FErSa4Wc0rSdGBmXyU2hrjjqotPGqKHI0U2hrii8ip08a44cAptORpjjootVh7RRTaZ8ZCm0z2OH8FGgxpCWmawqbTOap08EKqdGDaClpjFUtNeKm01gkJaE1paa9anTwQlolaaG4Cm0wGYJaAmUVU6Y2y1Cm00c85gpaZZlrmptPC3TAKbTwoyAqbTwDnhTpg1qabXZvY+sbjnkjRhU9s5rjpFQoqmRzHHACpU6ZsVnTNwqeCRkbo0Wdq5xdQzVjFPCp/Ys+/ov1za8j7sudWwWTQ6o7rx/uj+1V+PP511eufNfHLuv9XmNeJw8l9Fx/pHD7v8Aash+p/8Ai/ctXLWe4NAD1VRn0UTQKiIZi8ngmlJDgiJoOCCC/wDEmKUmlEBp28A31uDkZWA/7wUd/wCtaer/AGn+X1QXDRXxXheL6PU+64FHgegddDin4DWO5uAWnHBa88s+umH7gfNbeLHyCwL6TXz7VFknpNUaWk1RAUStJpi4VStDoW4Gai0N8DTUKLTbYozmQptPGpkam1WNUbBRRaeGgKdMYStBreim0zWBTaZzQp0YcxTpmtKnTMaQp0xgpaaa0tCjIptNNaWmp0mCVowPcU6eB7qnTCZcUtMPdU6aw9LRi+4eam1WC+4o2im08KdKp08KdMFNp4WZwlaeAdOFNp4A3KnVYZa3LRKCTSinRjZJfGUOa00HNK9DGI3Wl1AfEqarAO3HTliUtORwfc+8kO27X9HfdqHQt0/9Svjny3/Db1z6vO+47ho2KyaMNM9w0/5dIWn4/P8AO/4jefFr5hI4Hcp3dSve5/1jzvZ/tWYnGTxH7FbmrNcn0eaqM+iZDRnkqTQRijfFMgSHGiImq4BBBfkUxSk0ogG2rtNzE7k9p+anr6L4vzH0YyjSTVeTj39JN0Qak4qvBPmgvGmtSjwOexnludRoKK5yjrtm7o58VeMvJqjK97XiVpjJRpNMZCNJqjOFAlaGqFTodK2BwU2iOrbRjAlZ2qkbmUwUWqPaQp0zmuU2gYS0zGqLQY1TaZjXKbVHMcp0Gh+CVpja+qnTNa+iVoFrU6aalNoAXhLTxRelphMim08CZUtPAmQKbTLMoU6eK74GSnTxffStPFGfDNTachbrjFTaYHXGGaWnhT7hTp4S64PNLVYW64SOQJnpxUnge/1xSPE+6czEO8QkeFvvM8UsPGaS96p+KpHD96SFlptrvxOEkv8AxAD/AJVv+LN6rTn6ORv913NlsHDEPfcSCvVzR+5a+jnO7/wvq/V8/a4G7ndhWpr8V7H2jzur80kn1SeX7FbCs1yf5fmqjPoqanbHVOJqqUCCJf8AWfBUipwSCn5HwTFKTSiAKI0kYeRH7UqfP1e0NySM1weD1/Mp8tOKqcpvRZmPNVifILpuFUeJXovuHn18lWFrrR4L1nltMeKNJrjbVGk1xt5KdNrhACm0OjbECii046kLslFptDX0GdFNqjWydVGg+N/wStM9rqqLTMacFNp4MOAU6YmvU2mYJFNpjbIptPDGypaMMbKlp4ISKdGL7inTwJkS08A6RTaeFuk6qdPCzMp08AZ0rTkLdN1U2qwHfS08Qz9VOjENxgi08KdP1Unhbp6DNJWFunSPCjOkYDP1QeAdOlhlmfNGKwl9z1Rh4xy7rbteGOla1xrhXlmrnrpgbdxy0LHhwORBqi84qMvvuXT/AEy3OD4rVuodXOLv3q/w/nyv7q+zgb25w2jaxkBFKfjK7+C6fT/v1/x/6F3fh46Cjppncq1+K9O/SODfmkF3rk8lTGs9z9A8VUZ9FyUIb4hNNCgipPrKqIqBAU/I+CBSU0rQECA9NFNqja7mAfkuWx6HPWxbnow9Lc9PE6Avx6J4Wq1lA13owvR15zVHQURpNcbxhRGhqjdhgptNqhKm026B2VFFojoRSYKLVNDZFGqw9jgVFp40RvU2nhzZKKbTwXc6qdPBtfU5qdPB9xK0IJaKbTF9wwDFwFOqm08Y3e4NraKi5jcA4MdRwwJ5qbVSFR+89hdci3F20yF2n+7XxySu/oMdptwCKg1ByU2ni+91S8hgTNjmptPAmYKdPC3TdVOnhLphzSVhXe6pGAzdVJgMyDxTp+ZSMP3HVI8C6cc0jwl83VBkuuOqMPAGcVRh4DvoxWAdcdUYchb7gc0YeONvW8i1geW4SU9NQS0+YW/q9PlU99+MfPNz3k3Gou1aicauJNelV63r9Xi832e7Xf8AY999xuVtZCRrDM9jHOfm6rhg3yXL+bxnN6dX4vs+z1X6kSMfvbXs+gwxloHAUwC8/wD8f/p/y7rPh5/eZw7aNsZxayX4GQrr9PP8+v8Aj/0Lv6PKRVBm6Fejfs4L92Wv8145hX9mH3Juj6B4pxHYXYtCaaCqElPzTTVJhH5HwQKSmlEBaA7lvJ/Jj/wj9iws+XXzfgZkqlitA56eFoC9PE6HWnha9G2RdmuM9r+vgloaI5OqNDXHIOCnQ2QyqLTbIpApptsUuCztXGqN4Ki02hkg5qbVHtlU2gxshzqptUISKdMxs2CnQ524e4YLK7gtnsc505oHD6QEvk2PfPc9vZCjLhmrQ+rQau1U9OSJzb9Bbj5m/fNzkY6N1zI6MvLw0k5uzXV/Xz+jPzrLJNI1rjroT8E8LSILmRsmoGhGNVV5KV7vav1Dv7exit+yLiZuBfK+hpwXH16Pn6/DedvcbRv7NwsWXDmGFxoHMd+boeIXL1MuNJHNuvfO3Q7gLYPD2V0vkH4SET1dWaex0J/cFhHbNuO6HRv+ihFXHkAs5zarA2u92t5GZIHgtBI64cUWWDFsv4ZdXbeH6cDQ1SxWJ9wkC3XIHHBJUgDcjmkeBNz1Sw8D9xVCsA64xzRh4B1yEYeFOuMU8PCnXGdCnhgdcHHFHiZbrjqn4mVJP1TnIcje5JZLcsYHEUq4ggDzqt/TMrP27nw8Bctb3HZnHCuPivV5eV1Pl3/YMUJ3eW5dgLKCSRppQ63jts/56rj/APIdXwk/+6//AFdn4HG9/wCHpved0Jt00tNRFGxgPPSMFw/hc5w9L2uRuuNlYHICJ1f/AMjl0+n/AG6/z/7M+/8AWPOx/TM48XYLtv2cV+7FX/uCOYP7Fp9nP9y7r6B4pxHYCfQPBNJeKZFvOJTRUQFPOB8ECkppRAWCgOpbP/kM8FnY6Ob8Gdw5JYehdIU8GhL0J1WpAeha8ro1ynMejQ0RvS0NMb/JLQ1xSY9VNpxrilNQotVG6KVZ2qa4pFFqo0NlFFGqMZMOam08NbL1ootPBd8UzStPHP3De/tJI2ltYnE96UmgYAiTTeE9we7Jb0m3hP8A27C7Q92LyOdeC6PX6s+ay67ecMkklQXEAZuK2+iELnBxIOA4lAUXF4BPpYgCc+CNuFa8+KXzT+E+4mbHra2jR+J1KlHjD+Rxb3dtjdH3CG5tbwB8lN9UOeyltv5akuILzxKq8CdNEm73DmNjMnpbXSDjicz0UT1xV9lo4PcW4Wzg+GbS7TpJaBiOqV9PN+pz22NFj7q3C0aezIRSuBxBJU9/j81fPtx0f/N90Ja4yA6hUsAAAWX/AFeV/wBrPee8Nwnt+3JIBQ19IoTTJVz+NzKL7vgp/um8dcMnbK8aAAGk8s8Mk5+PMwv7rutdt73vu60TEGPVi6mNFHX4kz4Vz+R+rqwe8RM46LZ76H8OOCw6/Gz610c+zy+krc33BZPBJk0aTQ6/Ssr6q1kYp/dFjX+VLq0u9ZAJFPgtJ+P194XlDYt/sZj6ZRU88KKb6bFzDH7nbj6pGhvOoopnFOzCzudsRUStpzqE/wCukU3dLWR2hkzHO5BwJVX12fYp1FuuTzSnJ6y3MncYWEkB2Bpy5LTmYnr5eZ3jbYomt+3B1PJw5Ls9Xst+rh93qk+ibFux2oTRSxml0WB0vEaK4EeLq+SX5Hp/syz7NPxPdPXfn7vSe5SDukhD2yBwaQ9v0kFoxFOBXD+L/o9L3fVmv5Q7aLNn4g6Rp8AQf+paeufzv/COr/FxmWo+wuLhzv8A1dDW+VSSuq9/yk/Zz3j+NrjuNLhvjT5Lo+zhv1Dc10IiOy6+gJp+wEyLdmmiogI/JApCaEQFoNrjuGsjaCpsaTo7uila5pYrULuSBoS8ILVawgteia5bMTWPxS0Hsf5JA9kiQaI5DgpobIpVFVG2GagUVcaGTrOqObcKTNZPjmoqjRcKTZ7/AHI21rJOBrLBWmSOZotfM903O6nlcZJHOLzqIrh4Ls44kY9dOeHnP9q0QJshHWhySsMxgLuGs/8ACEqYZnv80QVmfI6vXiqJTpnOAqTUc0YNAXJhYeRxxSNWriUwIEcUgZGyZwoxhcDyBSti5zb9GuHatwewuLNDAaFz8D5DP4LO+3mN+fxu79jotsgMga+4qTybT/mKm+259GnP4835pj7Pb4LjtlxmbT6tQAJ/y8lM76s36L/p45ufUuSe3adMcMbSOYLifiSqnN+9K+P2kAzct0nAijfogbhpaKAeARfXxPm/Uc+32dfE+hgc5sIBBLxk7+xLPlp8yMlw+7edTjiBgAaLTmSMe71Q91zQAWuL+I4D4J4XlRxzznJxZXjTH5pXmK56pmqOOMF8jjTgcEstqviT5rN3WOka+M0eDVrm4EU8FWMtlvw6bN+lZHoNXPOZqFjfRLXTPyMmHRb9IXUlhBbxIwKi+j9Kvn3z7wz+s2Ejvpc35hL+nqH/AGeul3Um13EfbMxYSQcv4hPnzl+hdcevqZrTc3dtIYe08FjI2R1JBNWNAUccWbv6teup8ZWh7WfYQyPNWte+jeZo1RL/ACsaSTxIvNEXt+OMYGad8h54ADNVx8+239In3cyeufu8pPUSgj8y748nv6rujgAnE9lD/TATR9gIIt2aaKiAjzggUlNCICIAqoNZcSP2IC2yOApXBA1C8lIaDV1TGvVh3VNJrXJA1r0aDWPS0NDJeqmm0RzKacaop1FVGlk4wUVRjZxVRVQ5s6mqE+7axhc40aBUlTgeS9w+4XzPdbxEGCg9Q5ro9fr+6OunmJZg52oZlbyM6Bpc5wzKZNDNLCDLywA5qaqQqW5eagGjRkAnIRPccRiUwujjSmJPAJDBxWd1KKxQveMjpaSKpXqT61fPr6v0jTFst8/DSGu4Nccfgovu5jbn8XunO2N8btM0zG5VLfVSvgpnv36Rf/Us+tGdt26JvqnLj1o0/wC7WqX9nV+y/wDr8T60UZ2mNrWiIuk5uBP8AlfO/dXM9c+3yc3crpkXbigGjIa2gUH91TfVLdtaT29SZITcbldCrQ0gDi0AD5qufXEd+7pjDrqQjUTU8StfiMv5VZiui6sbdP8AexJS2H4dfZI7Gdubw1x+qgx+KL3D59N/UQjlB0NmfTjSgA8xVLZ+ipzf1MbbMIxLnc6uJCXkueuKfFBGHSNYKtwcGiv70S2i8SfOEsudYLWsBJyrwVXlnPZv0jQIWtwedL3DBvRT5Npx+pUkTMdR1CnDIHzTlR1xFNs4A2rWlx4uOSflSnpi5LOEAdkOaTmSQMePklO79zvon2ImtSHAB5PHPiqnTPv0obMYVDqBHkP6RsaxhwDh+9Kr55kNMjaHAE1wH70saWx6HahDeWcQkq0MdSQDOo4jxC4fdbz1cd/pk75aPcrduFm5sEjv5bm9qJ4oQDWoNMDgp/FvXl8n+ZZ4/wCHiZWO1DlWpK9SPF7gbk1APROM+y2/QEJAQmks5ppRBKk+lApSaEQEQD225cwODgK40NEtaz17F/bGtA8V4BGj+sJtpqkaUaX9fQXW8zcx8wjSvroND+RTLxr0zXoSY1/VAMD1INa/gjQY2QqTOZLySB7Jsc1NUcLjFRitMbcUU2HKOS+bDE6Q1NMaJeKteW3nfJrt5bHIWx4DQ0/FbccYz661xC91cytULDi+jaVd80G2wW8sQDnMOp3TJRepWk4ozttzO6oowcC8hufFT/ZIuejqrj2y3jdW5uGljTRzYjqPxySvst+kXz+PJ/tf/g0tbs7XahbuLT/pNxJ8XF1Qo/n+rac+ufYY3i2i9DIoxwyHDwyS/pt+6v7+Z8SQQ3Rs51H+WXZGNoH+WtUf1Yqe7S5py1x/mhp41xJ8QnOf2LrrPuW+4vHR9tjqNOdGnw40TnPO6V66zIW1rA0NcauzqR81VqZIPUwtBL3FwxAazD4qV/8AP/yXHE6YlxaWxNxq40KLcPnm9f4MPZABe+jXZHA0p0U/Krn3BrD2ijDj+LE1TzC3fsdbw3szNMULwXfjIIb51U9dcz61pxz1Z8RrGw7lI0a36BTEZ/wWV/J4jX/rd361oj9ru0CszscyAKfDFRfy/wBmk/D+PqUfbk4dp7ocODiKfsVf9qfoX/Vv6mxe2iSe9K0Y4NYKYdc1N/K/SHz+J+tGNghifpZjXMt+rHqp/wCzbGk/G5n0BJsbK1o+nzVT8gX8eESbDC8aWyvYD8VU/Is+zPr8SX7nQe3oGMDXyPeTzNAp6/JtVx+JJPrVv9uQuFAS0Z1qlPyqq/ic0t3tZ+bZnAcKgFVPzJ+iL+F+7PJ7bumv1B4dyOP8Vc/K5Rfwru6S/Y7sZnLiFc/I5K/i9FnabpjquaSFX93NR/1upWra557a67b2OEUvpJOQI+krP3czrnfvGvpt56yz4rrbjbMnY7UMKA+YwXL6urHT7eJ1Hk7uINLhwBXpcXXk+7nGGXFjR4haxydBFNNEJLdxTRSyCmlDkgBk+lBUpNKIJKoMzTgMR4JKxHE8MUxVsnmZ9LiErDndg/un56jXNGK/sqvvJvzHOvBHiP7a7YcmxMa5IDa9IGh6RjEnwSBrZCFJmCUDilqpzTo3l7S5pBDczUYBRepGk9XVMjfqpRwDTxJopvUiufT1QX1o6WFuq7jhhP8AqEGpA8FE9vz8Rtfxfj5rgybfZNe7TMXt/CSA0+JGK3nd/RlfTz+pb/6dEBhrfXM1y5qv5Us4hjN1ggZoiY3Opc1tK165qb67fque2c/SBk3Fz6AuJd+WmPmU5xIL7dC90hxcXAHKpPwTHyFkbHuDZRUjFoqcBzqi39BOd+p7rOV7BiRCM21oSfPFR5xp/XbP2C20A+m0c41wLhRO9fuJ6/8A8TNE5oBC1nWuVOdEtn6ry/osOtoxV/8AMlJ9TY/lil805eZ9fmtccN1LCCyzc0Eg6jUVp04rO9SX6tuebZ/q6Nvse4TBtLfQM6vpQLDr8jmfdvPTb9nQZ7LvCKyTNoeQAosL+dz9oqfj/rT2ezIA09+4Bxyxoov51+0XPx59z4/bvt+3GNHHi4kBRfyfb0vn0cT7LezZYQezECeg/elL7L9auSQDdwZU9mP01LcGjh/iKq+r9afl+i/v7g4fbgD8xxw8kv65+p7Whk8rm4sDRwoAFF5iibh2kanaiMqNBJx6BVzNFDEyN1DoIHXBFtOGP7YyA8EoZbxGRU4FOaMK1EGgphzVBeogVwryCAB0mqmQTkBfdc3DLkqwtKkLjXS4ivGqqESGy8Xmo41VbC+QvqMdXlVOAh1zI0Uz5giqucRN7bo5hcWL30o5rXB3iMQsrz49Ye7HltxA1u6lej6vo8v3xzHDBbuGkOTZUJZ6AeKCz4UQUFijkmkEn0ohUpNKkBYzQDPBCkIdzH7EhiUAzIPggIUwqiCdyqSRB1EgLVxOSR4j7mNhAccTwGKDxbrshmtrf97BSucFO3K8neXUDGjgwCNvkAlOJF+VpJleXkuPUHEqsAm3HaDiMNQpVvp+NErzpzrFG/upPSBqbkKAnJHhIP7LROjneykjwBnQo2H42/Uv7cVOqQnwB/cjS8P3Pjs4SKFriPAjzU3ppPXBCzia4aY9VchhijyP+qfocKRNOAGvBzWiqn6tJ8AEb3EASOb0DRlzJxRpeP7nxQXgcDC1z+4aCR40gnxIyU3rn7tOeevs6NttO4yPEj3MZwdoZUEeLsFh17uZ8Ojj0927XSi9syuZV0rtNc3uA/YFhfyp+jafj/udHs+3QNcHyCR4wADSfm4qL7+q059PMb7HbrcuB7MYa3HHD40osfZ7b+q5xJ9nQlEEOIkjDqY6BXPqsJtXKS3cLUklzXu04a9LiD4Kr66egfuB1ARsdTg52H7E56v1GlTXU0mBdpHSirnmQ2XsNe4mhca1rxWnlgw6Jlu+QxhwL2ZtriPGim2yaPg2KybEyjG41JPDFTe9OTDAKVqwV6/wUqBPeshj1y0YPJPn17fgr1I4t/7o26EhtTM78rf9gF1+v8Tq/s5/Z+Xxz+7NH7pklIbDZOIJ+pzv4Aq7+JJ9ek8/lW/TlvdezljHOYI9XD/aix/rjp8qjpnHOpHQVROT0cUmttKkEZVB/sU9TFRHQupnUonQwGIwzVFhU2FTy54KuSpMV3bPdobK0vOTQaqrxZ9kTvm/GuduFtuz5y6KYtg4NZgQt/X1xJ8z5c/u9fst2X4XZyXWl0cmpwbk85lHcn1i/VevpTiHGpoRTGtVLXGzaru30SwyPaJH4MjJxNBjgsfdxfiw+O59HC3iIRyuHXBdvouxwfk8447xgV0vNsZZCqY9NtnuVqLKaC6txO4tAt3/AElhrnqAqfBZd+u+Usufq39Xv58Lz1N/RhOa1c4SMEyC8HSShNhCaEQDbZncma05cfJK1fr53rHS+3ZUUABHFZ67v6oE2rQcTqT8k30wt1mwfhqOifkzvpkKfaDMVCeovqD9oOaNT/W6Uk8MT6V7lMy3BtfHil8s/GQp96C00o00oKYHxRlPYS6R7mjU4+VcUwprXNBJ9I4koGBL2aqj1fFAtF35KZU5VRg8qZF3ZDpqB1JSvwrna0ts5XUDzqHBo5qPKNZ67TxGIwDG06mmobTiOdcKKd1p459Gi2t7u4aaxggU1ANr8wo66nLTjjrpvh2Tc8O3a0acu5Ro+Cx69/H3rbn0dfaN8Htfc53etzcRR2mvHlksevy+J9G09HV+q5va8EDiJJHHThQZeFUT8u36H/1eTLT29YUBjaNQ4ZnKn4qqe/yevurn8bmfZ0o9ltIGBz2FxP1Nc4VNPBYX39Vtz65F6rNuLYmRln4QCUs6/VckSW+hlADWlp/KMPNKeuw9BO6sbGMfQniaup81XM+RVMa9hHclGk5OaA3ywxRbv0gmjdextODvTXM5qfCnohukFcq05o/qo8otrzL6hSnBKzFQbyGtq7FKGKOFrvV9VOOKV6M2FkIfRpDq8FPVoNbGQScq8VFoFoeXAk4DjxS0Buo3OZSNwYRm+lSnxf1DmXOzw3jGtvaTBowdi018QV0ce+8/6/CO/VOvr8s8Wx7RCKNt2HhjU5eKu/kd37lz6OJ9IfFDaW4PaiazqMVF666+ta88yfQqSO3eNXDlTBVLYeShEbBXS0g8CE9PxLdK9oOHHBPNLQsne41OBTvMEq3vr480SGU9+Ba6uOdVUhM8VtBCQGsaAPpB4K73ajniRqje1xwIIGazsaSo8MoaAAoh2MkzSDUZrXlHUYo7Jov47pw09upwyOBC1vf8cc19M853+jLu8wlmLhll8Fr6ecjm/J62uQ/iumPP6YZHZq45eqW0mqaIaCpWjTzyQIONocQDxNEVXMYjgSOWCpgiAOCZ0Uge3McOiVmq47vN2OyyVj2B4OBxAWVj0+epZqOLKVJ61QdwOocPJPEbAlrXZoTZKHtjmnpeBL+1X11pwpl81Ti+B/8Aa+nOtOGfkj5P4D6KenXq60QSN+2r/M1V4VyR8iYaztaTp0cPpz+alcw2H7Svr/tSuq5ww6sOzpzwp+6qX+Vf4Mh+71Gv+elK/JK4vnyXPq1P0Vp0RMPrXudl+3/o1n9v/paR3tH1d+nr11xr4YUyXke/fO79f/Z6f4+eHw9E3R2W/Tpp1r51XDfq2+QXGrteiva/H29WquGWnH4J8/U3N3Hv9/8Am6e3T8PLhmt/XmfA+XKd9x3HUrnhWlPkuiZifnS5fvdY1ZU/26pzxF8gNrV31aacaVVXCmmWvc1HOnGlP3pd4rnWqXt6fxV4ZUWcaEnt8NVONaKkmD7HQK14cq0U3VTDP/1uk/Xkc6Uol/I/gcf2/Z/l6tNBp00p5Kb9flU+h7fttP8ANrSuOVVIaoft6enVppxpTzWdP5RvbqdOXWlPklT+RnVQVU/AGdOk66U4c6ImEU/6vT9HXNP4Mg6KmuqnBafBxln+kf8At1/DSqvkIP6fQa9dcKV+WSPkwGmo0rTgn8H8ibo0nV51Sp/JLuzoGVOCoizox0+dEx8s1z3MM6cFpxiLrHJ97Q0/y8/mtZ4ovkQfu+56q+av+OI/lrXaa6HXlXzWfeNedPOnGupQ0+QO0acPNOBkv9fYPa1a+NKf7UWvrzfll7d8bjh3X1DVWvHJdnLyvayS6NPGvDJXHL258mmpzWkcnQBorxqmk0duorqp5JKGPtv/AJPkgQbO1X0aq1FK0qpq+WGXTrdT8x/arjHr6hFEEv8Al9UB1LPsfbN16tWNKUy4LPr6u7054/Jru1h9XySaUR7VMNVadElUI7FfxV8k0TNV/K/vfLNBP//Z" data-filename="romantic-manali_1466979022.jpg" style="width: 450px;"><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"><br></span>		        	</p>', '2018-02-09 23:02:40', '2018-02-12 19:35:16');
INSERT INTO `user_helps` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(7, 'Where can I get some?', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p><p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBLAHCAwERAAIRAQMRAf/EAKUAAAIDAQEBAQAAAAAAAAAAAAIDAAEEBQYHCAEAAwEBAQEBAAAAAAAAAAAAAAECAwQFBgcQAAEDAgQEBAQDBQUHAwUAAAEAAgMRBCExEgVBUWETcYEiBpGhMhRCUgexwdEjFeFicoIz8JKissIkFkNTY/FzkyUIEQEBAQEAAgEDBAICAQIHAAAAARECEgMhMUEEUWEiE3EygRQFscGRoeFSYnIj/9oADAMBAAIRAxEAPwD9Ste1wwKmwLSNEBCQAnITFdzDSQtOYHDvZTQ0XRzEV52/fWvNdHMRXAvONVvyzrjXLc1rE1zpmGqsmWSOqoazujzTGluYmNCWILVUIQNaotz3CIAMmeAMhVTeYNObvu7B1RO5L+uDXYtfeDmxsbNFqcPreMPks76VTt1ofcm2TMBdIWv/ACnAhZ31WK84h9z7fI4hrqEcHYAo/qo84I77t7x/qsBFKYpf10eUDL7msGVDpA5wOFET1UecYLn3QHHXGW0BppJId4ii0nqT5jZ7hsixrXAySf3nClfklfXT8hXV1czYRQB5zBJB8qhKSQWpG29iYO49sYeANJ1VBRcoZItu3Oe8c+emllAKEEkeCq9SQZXauLaA24Y+KVwrQnj45rGX5XjTt+3vso6RNL2SYuLjj5KeutOTGwWcL2aHNGl2Jx81GmG4tHGEsidoLfpI4InQFBZvY1okeHvbk6im9G0NY3EtadQzU2hAzSMcTzU2mW+EGhz6o0KERPH0jgMMVNphcCBgKEcUtNYZ6MeOVErQoM9NKerilplhjwaV9PEJaEfbxhpOdchwS02OUt1aSzHMGiZFSOu2AO06q4jnRL4MLbp5PqYSeYCVgWLkhhrQVU0Ct3FxOJfQ50wU9G1OjDoyAPGiztNypniFxDjpaUw5O5+7Ng2/ud64DpIml0jI6OLQM9RJDR4VqotOR5Ob9a/bTH6Y2SltaatNT40RlGxp2z9XvZ91M1ktzJaudhWdhDK/4m6gPEovNGvWN3a1niEtvIyWN30yMcHNPgW1CzxWss1295wKWAk3RbgXeVUsMv7mGuLiOuammXJfxt+khw4VU4ZH9SdzGaMN+k+zNHTSdbfmvR1ztDO7pBNPBTcC3dymAR8EyzXRFQcxmrkDm3NwTxWvPKbXIupia4rbmItcS8dWq25TXGusSQtuUVzJ2nFaRNYZmZq0skjFUIhzFQKMeKC1RiTGh7WKCUYigJ2kBfaqMQgasRkCnJA1fbPBATtoGqMSBqdlBaghQemNdO0ANkcBnQEpYNaI9w3JgIbO+js6muXipvMPyrdtW8XNvcmaasoLdJ5lZ98SxfPTv22/fdyduNojHHWaV8FhfXjSd67EXdltxI2QA1pQY5LK/CxQ2rhJqfO59fwgABK9A/tAEjhxx4KNMYDSXHKmSnTV6qam58v7EgtrSRWQaehStNTms04mo4AJaZZLssA35lIEumhDqOIpyCALuNODPippqJAGBoeSRqAAxOZSCiMKg+CWgp0YdnU04BLQhAxFDUpaZPbYw5DqloU+NpYcuhS0ywXUDQFNoNE0UED3zODdALjyoBWqi035/wDf/wCqe531wYNvP2kAPqLHHuHoThTyT4mn18Pmtze3E7tUry89VpIztZy8pkmooDse2/dO57Dei4tH1jOE1u6uh7eNRz5FTedOV932bdbXdtsg3C3GmOdtQ12bSMC0+BXPWsPcK4NFSpMl0Dji7AJaeBEDRk2p5qLVSL7X/wAbVOnj9C2u59wAscangV6/XGOfW+O94PaarPxBv38FMTRLxpMtzdWpaSSCr55ocK9uWaiGinnVdHPLK1ybi4BBxW0iXJuZSSVrIm1zZnFaRFYpeKuEyyNVxFZnsTIh0QTIBiTCu2OSZK7SArtICdlGgZtyMSKI0J2W+KNCuz0RpIIeiNC+wUtMQt+iNIQtyeCWmIWnRLTwYs0aMELPolp4NlqRiAptNtt57yFumORzRnSuFVFkq5a0R7huTTUSuPiovMVOq7G33pmYW3cpaXGgoKAeax75z6LldZjbV0QZGQ8UoSDUrG6sUUBb9ApzHNTaZz2NdnSvJQZMkWmuggCnFGhjfbF5qXI0MslrpNa18EaACQtyBB6INbZS01ONc1NMTbmppow6KQex5IxAaOFVNMuRznVDXAckgWYXn6pEvIYEWzdXqq4JeRjMY0kNAaFGmSYtP4wloYt1ge+xnaw1e5ha3GmJwzU9X4Pn6vzd7/8Aasex3EYdcia6m1umhaKCNoNGY1OLhjTgj1d22z9F+zj48v1eMcV0OcNcUEiAsHFBvtX6VfcO9qsLwNHekEZGekUGPWq5vZ9WvP0e3ZGD05LG1eKdbOIzFFF6XIA25HM/sU3o8D2hyU6ePfWe+XUdA2UgeK+q69cebOnVh9yXnGSviAs76YrzHLvM0uLn18MEp65CvRDr9x4qvEtA+7qKVTkLWOaUq5CtY5XVVRNY5cSVpEs0gKqFWdzVUKkvYmmlGNMgmJPQrslGkghKNAhCRwRoX2SloTsEo0CFv0RoX9ueSNCxbnkjTwTbYpaMMba9EvI8OZak8FN6PDW2h5JeSsELPop8hhgs+iXkeGCy6JeR4MWZ5KfI8ELM8kvI8OZbGnqFQptOQ6CEscDU+Si1UdiK/YWBr2kHmFjeVa0tibI3WDUcCs7cUTLGKVoepRoZsWmtPT80GCRr3DVGynVxSDG63mca4DyRoV2JRgfklprbaPpUuIU2gyO14HEeKi0xC3b+VTaYCzSa0S0KMgpTLoppkPfTDBIEucRwQCLiV7YZHNaHOa1xa1x0gkCoBPAKacfmb33ve5b7ex7ndQRwse3tRmIUa4sNDjU6i3IlHo5k2b8tfduT9HkXZrpcqqCiAFBLCA+v/ozfCbar6wdUvtpRK2n5JRT/AJmrm9/xW/r+j6A9raUxCwaKGhuRPmVFVC5Zjwr8VOGT3H8z8UsD00Fx1X2VjyNbYrjJRYetTbgkKcPRtkc7JLAexkjqA1HJLTNdZOI+rFLyGMr9vnqQMVU6LxJft1x+VVO4XiTJt7wMcDyonOy8Sxt2oZmvgn5l4skts5pIIVzpNhX25OQVanFfbEZhGjBttScgjyGCFk8iuk04lLyPxH9hNQHtmnOiXnB4iG3yk00mvgjzPxGNsm4tKXmPAX9Mk5US/sHisbc7iEeY8RN28peZ+JjdvPJLzPxPZt5wwU3s/Foj28g1opvapyb/AE+prpS8j8RDb+iXmMMbt4U+Z4Z9gEvM8V9gl5jBCxAS8zwYs2hTehg22bT9IqpvR4dHYNH1YDlxUXs8aiA1ullA0KNUU5jjxwRoAYTxS0B7YzrlwS0FvZGMEtMBHIYJUB0VOXxSoA5tMK0UmBxa0UqSkCXych5owyJS8+HRIM7o5M0gS+NxwJKRvnP6w7pu9htUVjt8cjm3rXm6uI2udpjZT0FwFG6q49EvrV8vjHue6bLdwxQxugs4IImW0LswCwOc4/3nvLnHxVemfFv30e6/Mn7OHTFbMAuPBBBQFhAfX/0VszFt24XroyPuJGxslJwLYwSQB0c7Ncn5F+XR6/o+jPLMyVg0Kdo4FTTJfQKaoFRySDswSt4iq+1seO6VmIXnF1OhWfWnHSZb2w/9QnoFntVh8It2HBtepSun8NH3QDfTQDopwav75oA4njVHiNUbx7vpcByR4jSvuJwcXYdE8haMPJzJNeeKAIscW+kYFIM01iXg4eCudFYzx2L2Hgnek4abOEmrsSl5U8NjhgYKCPUlbRjUwse0tEWJ5CoHkpUKMUJYYya5cEBtiifWhiAwzOai1R8YJjqY26hhQhTQU+3ic5pYxoc7NpATlGKkt4NAEkYEmVWjBEtGMzrIg1LMDkQFXkWLbat5I8hhrLZo4Kb0eGCJoCnTXobyRoWGAmgSCiWgoCg8Vyr0SAw1xOIHRTpiADaFxGHJIwlzK1p8UgIXEbM/klYYHXbK4YpYNCbvolg0JuzyRh6W66cUsBZnfzKMAXTvKWABkl5pABdJzSMJ7h4pAOl/NIAcHUSNxPcXuzZ9ggEt/OGvd/pwtxkd4N5dVF6kVzza+a33/wDQNlqlEG2v0gfyy+QAk9QAUXm/YfDzc/68+5HTExWts2KvpYQ8mnU6k/ClsejsP1S/8l2HcLC3jFrvroH9qEyaGPFPU5jzxa2p0rP2c/Hz9F+q/wAvh8d39ssV62GVoZLFDE17AdVDpBxJ44rT02WbP1Hu+ufs5a1YoRVADRBNEFhdTR9yKMvBkbEwNFS6R9SGtHE4JWxUj9Fe09hO07BZWLwBJFEDL/8Acd6n/Mrh762ujmOq+Dos9VhDrXip8lYW6BRelSA+3S08ddkdCQRiF9vrxGmMOBBGamhpZJLzKVh60NfLhipwzgZM8VIMAeg1iNyCEGP5pAbWydUjMa6UJAeuU5koCBpOYQDWCn4R8EjMYXMNWih8Ega2aUGowPQJYemiaY8fklkGmCWYmtcfBLIY2OfkXUPySoG6Ig62vq7iloQRPkFHOwRpj7E7RRpq1LYMB9rJnTFHkMX2H41NCjRgXRnmEaAFpyJQAkDmgKcAaYZJBXhggKNUjUdSQVRyRqLSkFaEtCiwpaYdB5JaFaDyS000dEtCaCptNWg8ktCaDyU6AlhRoA5tEtN4b9RveV7sNi/7OIl7wQLjMNdSoWPfd3I144mbX53v9/nvZLm5u5nyXkpB7jjU4nELTwHnMrz7nGq1YAqmQoppIpGyRkh7TUEGiVhyvq9l+ice+bbb7vYbyWRX0bZmsuItbm6hi0vY7GhwyWU7z4aWa+c+5Pb17sG8XO13hDprd1C9ldLgcQ4V5hXx35RPXOOZTgFaVho4/BBPoX6M2/e90ucdBbBbyODHnGri0aoxzHHosPf9Gvr+r7lowyXFW8Lc3HJTVEvb0UVUIeFCiqdDmkbo2+4bS9/8xkjBwoalfdXnp4ex1oo9teAYpxQ8HZrO2qyNMcFsDp7gr0p/FTbTyNTLe3GJqfFpA+KnaeDEMZBLRgONUtGBDBwCNLBtYa0ojRhrYHHIV8EtGL7RGYRpiEfRGksRpaBiJGgbY0jMDEBYaEgfFDqaTyU2ng2iP8vxKQF6Gn6PmgzA8tGDR4JYAmZ3DBGDU7siMC+7IUsGq1P4oASHHNACWVRoV20tCdpK08Ts9EtGJ2UtGJ2UtPE7KWniGEUS0YHstHFLQhY1LTAWtSATo5JBRI4NJSMJ1flSASHpBWl3NK01GLmSp0YExNRoxw/cftux3Wymgn0sEjHNL34NGFcVz+//AF1t6rlfkr3Ftcm27hLayfXGcaLo9Ps8+dL3+vx6xxzmtnOFAQFAfTdg/UHdNq/S6a3294Ze214LfvnF0UNw1z2ub11NLQeCw653prL8PAv3Pv2ksd0XSTl7pI5neokyU16iTXhVX4Z1sP8Asnjl+v2Yw9wa5rTQO+rqFpjHUFc0w1WV7c2dxHc20roZ4zqjlYS1wI5EKbNOV92/Tv8AUKPf7f7K+LWbtC2pIwEzBm4Dg4fiHmuH2+rx+n0dHHevaOezNc9jWESv5KcPWSUvNcUsPSNLuaRnxbPfZGMj4L7rzjxPGtcO1XoNO26nOoSvcHjXSs9puC8dxr2jngs+u4qcuzBt8kY0/cSYYgLG9fsuRuiYSCJH1DvpBH8FFM1sNnTIV80toELeJw9IAHNGgDoCPpeB8k9IsxSA51T0IIZEaRsdvX6nEeSVp4cLdo/ET5KfI8WIeVUaMGIHdUtGC7ZAFTgjQlWgU1oCw5n5q/FIDaAefwQZmlSE0I0JoKNC9DktNO2UtGIIkaMEIgp0xdtqWgJYloDpKNCwzxS0xCFp5qbTxfaA4FLQAtHJLQEx9EaFdsVS00MLTwS0K7TRwCWgOlo4hLTA4M4uCQA4xcykAF7AcG1SMBk/uowaWZHcGhLBrn71E252y6imeI7cxuM7yMBGBV3yCx9+eFa+m3ymPyx753Pbb3e5ruxj9BL2jWA7U01aCQfxUOaPxPX1zzldH5Nm/u8ZMwseWnChXbK8/qZSimSkBv2m+ltZJCyQNBY4gOAc3WGua12k1Goajp5KO+dVzcYeitJjRzTJZcEBYOCDb9n3O623cIL21fongeHsPhwPQ5FR1zsxUuP0V7Z3+03/AGiLcLf06vRNFWpjkb9TT+0dF5ns58bldXN2Om5qytXhL2BTaeF9sKdN61haQPVQeAX2byjmmJv4q+ACQOjfHXCvwSsDS0sJGePRSDmlo6JAwNbxaPikEAi1UoKo+Qsm3GB0gjqj5AXS2uWseSeUtid62H4ifJGUbEFzFXjTqEeI8hi4i6/BLxPTIpYicKhKyjWj0njVSpCwckEtsVcgjQMxaTQ4FLRiaAjQmjHNLQv080glW8z8EGvU3mUsCdzDAVRg1esD8JSwavvU/Alh6ndP5Qlg1Rkf+UfFLBodT+SBqtb+SR6muRLAvU7mEjMLNMYe78WVOim0wNIeaaqdCkAPY8GhRoAWu5pBRYUtATGUtCdkcUrTwwWg06jkovSsCbfAkNwGCWjCZI2tOIxRoKcxiA5+9bXb7jttzZSktZPG5hc0kEVFAcFl7ePKY09Xfj1LH5Qvdolsd/ks5YBI+J743RvGHEV8s0c+3fXv0dt9O9/Dy+7j/vpjUO1GuoZGvFdfq/1jg/J/3rAVowUUBEAbQEyGTw+KAoBAWEjECg3vv0n90M2zejYXL9NpuOlgJybOPoPn9JXJ+V6952fZv6uvnH29xqvNtdMhT1Np4Vjy4qTdKK5caL728vC8m2K5f5KLyryamXhHD5qfE/I9l83w81PgPI9t+zqfNLxHkc3cRwGCXgfkYNwr+EJeA8l/dMJFYwjxLyEJ2HOMJYNNbK3IRpYeoKOdQR4pgwMdXBtTlQYlSZrYX0rppxoc0tNWog00owtOg7j3AAUbWhKm/Bw6V7RE4N4fiShsjJ3MeXEaqihBVYWn275J36MAeGCm/Bw58b2kCmqvEKZQOTtxEBwqQMfFKfJhYSGa5NIY76SRjhyCKAvu4wCGNqeDijxo1bZnvbUChGaWDV65Esg1dHHiEgnbPMJaeL7fUJaeJpCWhNISCaGpaawxp4JaD7iJ3bbXIBZyqxh0gOVaTXGxjo6OHq5qLVYQWAJ6WBLWpAOkcktMUcDnuoBhxKm9HIdOA0NazJuazlWzd1zcBlWqeFpMhLnE0QRTh0RoKeOiND5V7h/S9zru93Wbcdc07naT2w3SH1A1GvBcPXqvHP1+Hpev8yfTH5933apra5lZK2ksTRqGeNT+4L0vT7JZ8OT8j0/dwiMV0OJNDjwwQA5ZiiANpTJdeHFAStAkEBQaw5AMjlcx7XtNHMIc09RiErFR+o9suvu9ttbo53EMcp8XsDj+1eB38Wx6ENeotVhVf2parGmNx8F+iY+b09hdzKinrQwHqkGhjeimmewHkpqj2tPJSDmgpA5jMqmlckjbYYAI9ZI6k4UHmotVIRLuDwdMXpaOPE+ac4K9NVvOJoj3CBJ+E5EqbMVKc1wt4ySRqOTeJU/U/oxvunul1ElX4p10YhFIwODtRypl8SVndi4u4uBDpZGcKY+KUmi3GZ9xUUB8VUhaV3sc08LW3b5dJc7phVZ9xXJ75ZA8Oa/IYUUzDPjfCWVnxcceqi79lRjuJg95PDgOQWkmJpVWoDRA9gA1/T0zUdHGgMY+pZl1UaeDEKV6PF9kKfIYsRdEr0eL7fRLRiu2EtNbYK+HEpXoYcy3YwgvIpTJRelYXczuJLeCJBWXS3NO0lSTlg0tUmBk75CGkCiKGhsIJAJAJxoovSpBm3Y3EuwU+R4pz8w3BowUmzSuIBNCmTMZaJggzODqg4pBYmL3eqmKQLfK0O0gV5qbTkeF98+7XwWlxZ7daS3V04GLUWFsLXHm91A48fSuH2+6d/G5Hd6PRnzX5l9wz7ib2WK4FLjWe5pINajIEdF6n4/PPjLPow/K9nVuVyBAQNUmHIcT4Lo1xYpxcMmmiZYU8hwx4JkAYeKAutPFAUgLQEGaDE04hI4/S3seb7j2ftEhxebZjXAf3Ks/6V8/+R8d9T93pcT4jryCgxwWF6XITqCnVY29kseW504r9I18zhsZbq01GrlxU08a42EqbTw+N8QfRzwCMSCeCmnGhksbsYtLgeZx+CnDaI7hxIAAa3i2mBSw9aXCABvbBc53Dgp+TZ7jc2W81GNa4NoCeqc40rcJfvMsjCzANJyVf14XkQbjjVPEiZeEZFHietLb17wKmtFPier7pRgNjvXsbpBwOYU3k5V/dFxrWqWBfeJQYmvqalKmcJXUwUYY4rl7Hg1w4pWHK0Pug8VBoeSiQye45MhCQpAWt2BCk2y2uQyrXfELPqKjbDPHJ9Jx5FZ34VDwFOmt2hpALvFTp4uZzI2uc0atIr8UpTxzfvpS+rjUcleJ1vhuYXxu5Nxos+oqM0l8HvId6W8CiQaAO1VLHaqZoJnMjwSa5pgBLiloHDq7jdP1VU2qjqujFAXZgLHVlXhLYmk/BKCuPJfPYSK0HJXidIfuEla6q9DkjBoTdtIxSCNd3PpIJ5VolaZfcNckrQVfzi2D6kNMYq85mvIUXD7vc6eOHhveV7FcRBsj3RsNKu4tB8FyW706fXMfIdwsbS+3C5fDDS1gIDhXF7jXQyueOkk04VK9L19Xnn5PvidV5Hd2QfcuMUemKM0bUCpPGtOuQ4Bd3q3Pn6uD35uT6RypDUrZzESgaSeKcInIdUyRARAQoCBAGwVcKZpKj9S+2GW23e37Gx1kGOCJtCAKODBqHxqvmPb3eurf3etJkLv7h4NWurRRFYz/ANQd+QZUT8Txypvc9sJA2S6BdwoSafBfp3jHy/j1W22uC7TLG+oOLXAosQ69vut0LgSOdVuFW8KLO8TFTs+6nFxcukYNIPBLmZB1dro7QwOlAc6mGDuRWfsXw6zbd/dEYkBfyzCx1eNOuO1jewOEtyfSByB5Kfr/AIP6OJO0l7i40IOI6reM6K1tnSv0t8zwAS66wSNM1gWYtJe3iaKZ2d5AyHGlMU9LHQg226JoInVpmcBRZ3uLnNR8DmOLXgtcMwUaMD2wjRiAAFIGtAzSMwBTpjaTlRTTXXFI1gpA1kcjslNp4MxyMNHNp4qdGGNBPBTaZsdvM/6WEjnRTejxrjtnR0fKdBGIaM1netVjSy8i10xA5lReaepPdQAGg1E8kpKelG6ie4FwOVCOCMGijto3RveAHEn0gqb0cjO+GZjyYwW9E9LCJO641KNC49bXckrRGk2rtAc4gE5KPJWKbZimpzgBwCV6PGqCGKNpLSNTjms7VSGNAB1l1TwCm02K9utbdIJwTkK1x54y5xJOKvUs7oeqNGAMfVLRiqEcVOmjZJGuq12IU1UYfcc7QWT3L6tyZbjDW8jDyXk+76uz1z4fLffO+OtLNxcWtnkbQajU+o1waOJ4VVfj+q9dfLp5kk37R4eyZd/0yOLVSS4L5Y25BjX/AFSOOf0s+C7++p5ftETfH/Ly28yRmfsw/wCjD6W8yeLj1JXd6p8bXne7645Ema2c5ExwA5pwqT14JktARAVnigJWiDdX2xZffb/YWrsWyTsDv8INXfILH39ePFv7NfTzvUj9DiR73dXL5p62NxhgEDQQ10lMarK9HjN9vHyb8UeZ4+PtlOdV+qPBdba98urJw0O1R8Yzkhl365XsNt9z7dPQSHsv5Oy+KVc3XqseihmBAcBgRUHmFKY6VtcBrcFl1GkrpDcHOiayHUK/V4rPx/VetMkrbVjQQHvfUlxGIKifKr8E2sQmL3PFWAVLuRVdXEyadDEKuij9WvJ+OQU2nI3RW7rduozFreLRmVF61UmGRXdkJy8YSkelxGAPNKy4ew4RX0jjpuAWnEmtPkp2foeU+3tHEuZKWSDg4mhqpvYkJuNslGp0ZD28mnFOeyFeS2bZeOAPaOPOgRfZB409m1StaXSOaw8Gk1r8FN9p+CDbbovADdQP4gahL+yH4jdt1w12kBrj0P8AFT5weJMsE0JpIyic6lGAaW16otBjbgtOCmm0NnErh3AcFF+DdOEtEBELQDxrmsbfn5WKP72UBpoAfD9yVsg+TJLWgo6ca+qnyPCDt9w51Q4Fv56p+cHiBtldF1CMODuCV7g8TorCQPBfQjkDipvZzloLZ2jTGygAyqo1WF9u5LhrYHUyxS0Yvtyaq6BQY1KWnipYNZyGoeKXkME+CYsoKYZJeR4T9pcUxIHRLyGCjtJB9ZGHIpXo5Czb3Lq+qgCXkMJksJSCQ75I8x4sp265dU4Dklex4s0lhdDhXwR5wvEp1nc/lKXkfiU62nGYR5DGW+vLTabGfc9wBNtbAExt+qR5NGsHic1j7vb4z923q9V6uPLe6velvtmyR7lcRMbusxlMFpm2Kp0jV0aG5DMrz/x5e7XX1xJc+z4Nc319vW6d66kfNrkL3hxzqfUcOJyXsTmcco7t6s5n0dz3Be/axTB5H3koHeDQAGU+mJo4aRSvkOC5vTx5X9mvs7nPOvAzHU48Scar1ZHkdXWKTNUhllJMhHAYKomgQF0QEKApARBvoH6N7My+9zuuJRWKyhdJ/nf6G/8AMV53/kvZ4+vP1rr/ABefnX3BtrBHi0Y8818/a9AmWCMmuJS1RWhvL5I03xNr6L9U14RzHo0miKWnFGpr23sf3GIrptlfSj7OTBvdxa13DHhVZ9z9GXXL6laWm2ZgagcAcx5Ln666E5jsW8EULSY4wNXILG9aqQq5szOayvOGWGSc7wrNMisbaOEs1V/E7gle7p+LFLLJ3tNvqww0hXPp8lTHQX4idI5h0HM1ql5QZWAh4OCrUtVvdXTCCDXhQ8lNkOV3Nsj0WrrmQ6pCSGspgOpWHd+cactEW4hsobI0Y8QKFReTlS4jAf3Z52wNdi0OcAS0ZpToYxf+Re3oHhjnuuDxeBgEXnqn8Ota3tpfgvsZGl3jQivMLO/H1M7+mFztUstCMfTnVT/YfiKS1tHAB1SeLuKU6oxnftdk8+iQiuVU/wCyl4xoh2izjZR3rceKi+2qnIn7TaH6QW045pf2UeIBZMjJ0SEBHnowxkxP8vSTwqFNhxqZaMpqLRjxdis70rBSRkkULS3lklp4nbBbQmg6JaMFVgFAaBI1AgY6ktCnSNH4ktMBmYPxJaMLdcxZa0gA3UVKCQJGS65b+YHzQAm6b+ZIBN6B+JI1fejmkeoLpp/EpATKeCRlmR5KQR8YcMfilp4+Y/rHuwt4Nu26KpfLOJHOP0jQRTLxXL3PLv8A/Wf+rv8Aw+Pi18O9y+4Lvcyz7l5eY2hlScCKk/vXf+P6Jx9C7610Ntt2bVtrdzlFJ5hpsGEflwdKejD9PN3gp7vlcT9Hmdxu3zvq4kjiuz18Y4vd7Nc6QUY53PALaOasDjmSqSyHGvVUlEBMkBRKApAW1Bx9l/QhtqLXdXuH84PhBP8Aco6g+K8P/wAtbvP/AC9H8Sfxr6dcX8LAdAFeC8d2SMEm7VFCKDoq8DxX9Vj5cEvCnj4gDzX6nrwjWOQRzHI1NaYZC0hCa9Z7e957jtoEReZrate04nA/3TwUdcyose62X9SrCWjLsPgkrTW31Np+1Ydeoa+hbfLZX1sJobhs0Zyew1XL18LjT/TYHYaquPHop86eHjbbRrQ0sAJpiOin+yn4nthgYO3QUCnyoxlOw2b5C84A/hGSr+6l4K/pm12pLpZmAtFaPcB+9L+20eMZbj3HsFpblvfa9xJ9MfqR49WjY8ju/uwTkNswYh+Jxz8lvzx+qL04M+53ErqySOeRlUkq/GJ0sXLic0jbtq3iazuo52H6HA0qRUDgs++dVzXsh77glFSx0ZpkMcVz/wBWNPJog3uK6Z3GSf4mnAhHjg0+LdYWOxkbXxU2B1rfdY3NAqKLG8qla47yOhxUWK0t17Br9RFEZQjt2to66KDqFPjT2ESb3H+YlPxHkyyb7pHoR4l5M532cnNHiPIR3G9I1AHSlkPTLK+ubm4bDU0JxpyU2HK07jHdtIbbNc8AeriplOuNJf3bHEPqCOBRhM8l7cPNS4owBFxPzKAITz1zKkzBNOeJUmJvfdxKWg6OCc0xU2qwzsytOJS08EYphk5Tpray4z1KbTKu7h0ERkmlEcbc3OIA+ajrqSbVSa+Mfqjv9tebnt7IXiR1vOKlp1CjqUNRhXDgVh655Xq/s9D1c3jj5fI2tjkv7eGZwiE0rWyPP4WucAXHoAar1pPj4c17kvy9/wC7vbW6TTPuGGGDboQIbar/AEshj9DBgDwFa8TiuD0fkcz/ACrvm2ZHi962WDbXiOS+gmlLQ8xw6n0qKgF1KVK7/V7vP7XHH7PVn3eduHEimQ4BdMc9jJLTQVUTWSqpCZIAScUBOCAgQBNzQcfZf0126TZdolnuKtuL4teYj+CNtdFep1VXz/8A5D2f2d5Ppy9f8b1Xnn5+70826xVrRcU9ddUjDNusVTgtJ6qeM39UZ+UZqv6jfPGlfouvnTGlGg1p4oI9hT0q0xvoknGuKUgjFJL1Htf3dfbNcdyF2phFHxOyI/isu/XKNe6sf1YBkH3EFI+JYcarn69B+Z1z+qry0C2iAcCdTnY1bw81P/XHmCP9ULtw9cTXO4O8k/6B5nyfqhcuhLY4Gsc4UBrWnVT/ANcf2PJ3G6TyvLnvLiTUkmq2nLO9Fi5ceKeFqxLglQgeTippja5KmY1xBUVRzJnDipptEdw4ZHBTYZzZ381NNtst3uLd3pcS3i0qLFa71pvrZhQu0u5FZ2G6bDrjLnupXIrO02ZzngkVqmSUcQUtMpzXJaAgOqptN0beRhj7Zqa8a5KKqNdha3EM7ZImmQ08KKLTkdm51saKH1UxUatzJrVl08CfAjIhGljkXVm63lLfqbwcnpYFodTJIC7TjiEtAmseOCVqjmO05qaZom5FSpTpDmkAmfqppubufuTadsp99dst9QqNZOXlVRe5ufdpx6+uvpHzv317/wBtuGiKwLrxxDTqo5kcYIq4YgVcemS5uvVe+t+zv/H9XjPl4bZ9uu903KW7ldFFHGwyzTXBJhiAH16c3OjbVwAC38ueZOWntlzXLZuW0W0NInSXb2vLWmVrWtdG0n+Y9oFXOdXIuK6OvX31f0cPPXM+vyK/9x7fuduGzvNnLGSJJYWFzZYz+B7AWjUMg7lnkp9f4/XF+PlXXu5rxk0hdUtrp4lejI4er+jJK6p8FcZ1muDRlOaqM+mcNTTiOGCAUmSwEBs2vbbjcLtlrblnekB0B7tIJArSp4qPZ7JzNrT1+u9XI9z7b9hts5G3e6Fsk7TWK3aasaRxcfxHpkvN/I/M8vjn6PQ9H4nj89fV658zxhVcEjvZpJSeKuQM0jiePirgI1deKsnmQF9u+fMaEA5oTI5gQloiCCrTGEk60xkpE1xE80iaWONM0irRG4pUjmOPNSD2kpA1v/1S0GtrQKbTMY3kptMwNKnTG1pKjTNa0pWmazAqbTNGCnTGCFNpmseQcFNpuztm8FgEMprHwdyWVhuzqBxacDxU6aCQVoVJjaWVSMzTEcsio02/bYrYOGs0Ix8VFqpHb7rGsAYKN4UUKJfLrqDlwSNkmLmsNBVyNImGJkgrN6jwCWnjNI2LuEZYo0sE1kICWmp2jgkZbgCloBiEtMLnvCWhmubsQxPleCWRtLnAZ0GKz9nfjLV887cfnL9RPd13vW6XRee3C2U9mJuIAaNIxzyC0/E9WTyv1rt9n8Z4x4ea6nJoXuNOZK9Ccxx9dV2di9yT2u33m3kVbdwPiY/iwvc1zqf4g3Ssfb6Jep015918PFye89rqHwIW+OTyCXlpqDgUywiR9U4VIJGapOM7zrd0GSaL8gpRNIX0ogFUTJYCA0WVzJbXUVxHg+Jwe3xBqo652YvjrLr7LaXDbu2jnjNWStDx/mFV4Hc8bj3ebs1JGCuJoOaUqmSYhuRqtOQyySBXIWldzoqwtcBoX2rwDWBMj2jh8EJNaCgmiMYIKtMYNUtS0RtywSoaomFLUtTGYJaR7GcVOk0RxVS02qOBToPbCAFNpmMjCm0GiMKdUc2LDJTaaxGVOmNsZU2qwegqdPBNjcVNoNZC4+KVpmtt38sFFpmNgcDkptDq7dPIwdp9dP4SVFpt9HZqdMbdQSpnsyUWm1QOxxUVUdWCfW3ScwoqoIyBpISMDpOIxSBEs2lp058kjc+TVWpQSR69QA44ItMyRj2Oo4YqdNGgUqUtCOcAcEjLM3MBTTZL2KO4t5YSSwSNLC5uY1ClQo6mzFc3Lr81+/fa257Rud0yZh7UlXRTgeh4JzacvEcF1fjdzJK7O/5828vDyRTNHprQcc13yxwdcdRTDJCA5xxJqP3Ivynm2fVpLmSCoz4qfoqzQOwH7005hD3iuGKqItZpy4t5DkqjPqliqEoc0AD8kyLTJYQFtzSOPr3sybX7astQqWhzK9A8gLwfzJ//AEr2/wAb54jr3ejtmgAK5+Pq6McWcOLl1cpsZSxxOKvSxXa6o8h4uI0L7N8+axoonqacxtUFT2MQTRG1GprXGAlpNUbccktJqjj/ALEtS1RxjDHxU6TVHGOSVptEbMQFNoamNFKqLTNayqm08NbDglp4e2AVCi1WHCEUU6eDbbPOQU3o5DY7F9cVF6VjQzb1F6PDmWDBmFN6PGhlmzKim9DDm2sYFKKb0eGC2ZyU+R4Y21apvR4exgAoClp4jowc6qdGCjq3ilptDJmjPDqp0N1pctac8CpqobLIHVoUjJbLprqSoR1yKEBuBzSNmkfXggJCXGQNAxqlabXPDLTmo02Quc0Yo0FOeSlpllyVplvcQotNjvrazvIezdwMniJB0SNDhUZGhUWq5tn0eI91exPar4Xvh29kM0okkfLHqqNDagNZq0DW8gZKevyOubPl0erq36vhG72PYuBERQV0jwGC9n0+zymuf3evKyvio5zmkjSaU4LWVhaTIDQEmtU4i0sjkmkmYjTRVE0AAoEEAY1PVMgSZIKlpkgQBtGKSo+v+042xe3bCowMerzcSV4H5V32dPc/Hn8I6V40OYXsxWHF+W7kS6q5LphUqjycAqIOhyNPHDa1faPmzmtwTScwI0j2FBHMS0mllOCWk1RE0w+SRVpj1YcilqWyInBTaG2JwpTqotPGmFuOKVptkbG4KLTxpjjFMlFp4c1lErVYbFEXHFRaeNscDMFFqsamNjAyUWmMBvJRTMakDGtKk0oQp0xDUeiWgxlQpM0VPFIxNJUmYKcVIW52FAEjL44pGZGCDgkGtsnApKWTUYJaAhjieSVp4MQF3JTp40QRNjdqLhVTaD3OjOZSNjuGQ4kO8ktNhkc0HoptPCXSBTaeEvkU2mzySKbTked96Oljs7QhxaJBJI4A0BawVAPmFh189On0R8L90UO5wsAyFSV7P4n+tP8AK+rgk1a//Gf3LseZWeU+keKqIoCUyZ7g4DxTiaFxTIDcgggyZFMqUgkQDGVJQqPtWwhrtksNAoOxHh/lC+b9/wDvf8voPV/rP8Gz1DTTBTy0YWw6yQTitfLDxT4XtaRWqJ0bPpd81epxwGhfba+YOaKoI1gQRzRxSI5gr5IJpjbklpNUbT5JaTXEFNpNsTQptNsibzUWnjWxtaKdDXCw1UWnG2JmCm1eHMYQptPDmNUWnjQ0UUWmawKdM9jVNpiwCnQJriUrTOa2oqVNproEtNaWhNZS0GNKmmME8EqYwUjTUApMxj2AdVJo6QJGDvkJGn3ZHFTaFi+6qdNDejOuKm0wu3A0oClps0l448VNpkOuzzS0AN1XiptPAOuRzU2nIX3dbg0ZuNB5qLVSOL79kZJYNkaRoga+FlONXhv8Vhz1vTq9Myvgu/S6t66NFP2r3/xp/Bn+Tf5OR+F/PUf2LqefWaXh4qozoTmmTPN9TfHFOJoZAmVCMgggyDAplSUyWkBNNDVCo+27M10O02UTvwQRj/hC+a913u3930Prmcz/AAbLR3hzUxqToaD6VWgNw9jWV48Ucym5/wBwznxW3inXAaa0ovtdfLmtqjSOZ/sEaR7RXFGk0RsrwS0m6GIEZKbUtbIB/FLQe2Ijqp08aoYzhgotPG6JhKm0NsUeCi1UjXExTaeNsQACi1WHAKLTE35KbQa0KbTPYFNoNa5SoWBKVA26QkYi9Sa2lICBS00qFOgbUtPBA0S08QyFTp4HuJWmJstFOmLuVStMqSRTaCjIVNplPkPNTacLM7hxU2nid6qnTxG1cxx5JabK95U2nhLpCFNqsLMzlOnh1lMG3DZJPoiBe6n90VUddfCpHn/d95NJ7ct5JmhkkshNOTdTiB8FHpn83V6p/J8P3R+reZDWo4+NF9H6Z/COX3/7MH/udHfuWzjrPLw8VUZ0txqmlnkNZAOCoqqQ4ITQjJBAecHJlS0yRIzLdodMxpyc4A+Zolforn6vtEc4DAwYBooB0GC+cvL6TAOkccinIYe64Iw2S6mJaalacclXP1HnxW7JgYvrNfNnsFUamnxsNUaTTEw1FErSa4Wc0rSdGBmXyU2hrjjqotPGqKHI0U2hrii8ip08a44cAptORpjjootVh7RRTaZ8ZCm0z2OH8FGgxpCWmawqbTOap08EKqdGDaClpjFUtNeKm01gkJaE1paa9anTwQlolaaG4Cm0wGYJaAmUVU6Y2y1Cm00c85gpaZZlrmptPC3TAKbTwoyAqbTwDnhTpg1qabXZvY+sbjnkjRhU9s5rjpFQoqmRzHHACpU6ZsVnTNwqeCRkbo0Wdq5xdQzVjFPCp/Ys+/ov1za8j7sudWwWTQ6o7rx/uj+1V+PP511eufNfHLuv9XmNeJw8l9Fx/pHD7v8Aash+p/8Ai/ctXLWe4NAD1VRn0UTQKiIZi8ngmlJDgiJoOCCC/wDEmKUmlEBp28A31uDkZWA/7wUd/wCtaer/AGn+X1QXDRXxXheL6PU+64FHgegddDin4DWO5uAWnHBa88s+umH7gfNbeLHyCwL6TXz7VFknpNUaWk1RAUStJpi4VStDoW4Gai0N8DTUKLTbYozmQptPGpkam1WNUbBRRaeGgKdMYStBreim0zWBTaZzQp0YcxTpmtKnTMaQp0xgpaaa0tCjIptNNaWmp0mCVowPcU6eB7qnTCZcUtMPdU6aw9LRi+4eam1WC+4o2im08KdKp08KdMFNp4WZwlaeAdOFNp4A3KnVYZa3LRKCTSinRjZJfGUOa00HNK9DGI3Wl1AfEqarAO3HTliUtORwfc+8kO27X9HfdqHQt0/9Svjny3/Db1z6vO+47ho2KyaMNM9w0/5dIWn4/P8AO/4jefFr5hI4Hcp3dSve5/1jzvZ/tWYnGTxH7FbmrNcn0eaqM+iZDRnkqTQRijfFMgSHGiImq4BBBfkUxSk0ogG2rtNzE7k9p+anr6L4vzH0YyjSTVeTj39JN0Qak4qvBPmgvGmtSjwOexnludRoKK5yjrtm7o58VeMvJqjK97XiVpjJRpNMZCNJqjOFAlaGqFTodK2BwU2iOrbRjAlZ2qkbmUwUWqPaQp0zmuU2gYS0zGqLQY1TaZjXKbVHMcp0Gh+CVpja+qnTNa+iVoFrU6aalNoAXhLTxRelphMim08CZUtPAmQKbTLMoU6eK74GSnTxffStPFGfDNTachbrjFTaYHXGGaWnhT7hTp4S64PNLVYW64SOQJnpxUnge/1xSPE+6czEO8QkeFvvM8UsPGaS96p+KpHD96SFlptrvxOEkv8AxAD/AJVv+LN6rTn6ORv913NlsHDEPfcSCvVzR+5a+jnO7/wvq/V8/a4G7ndhWpr8V7H2jzur80kn1SeX7FbCs1yf5fmqjPoqanbHVOJqqUCCJf8AWfBUipwSCn5HwTFKTSiAKI0kYeRH7UqfP1e0NySM1weD1/Mp8tOKqcpvRZmPNVifILpuFUeJXovuHn18lWFrrR4L1nltMeKNJrjbVGk1xt5KdNrhACm0OjbECii046kLslFptDX0GdFNqjWydVGg+N/wStM9rqqLTMacFNp4MOAU6YmvU2mYJFNpjbIptPDGypaMMbKlp4ISKdGL7inTwJkS08A6RTaeFuk6qdPCzMp08AZ0rTkLdN1U2qwHfS08Qz9VOjENxgi08KdP1Unhbp6DNJWFunSPCjOkYDP1QeAdOlhlmfNGKwl9z1Rh4xy7rbteGOla1xrhXlmrnrpgbdxy0LHhwORBqi84qMvvuXT/AEy3OD4rVuodXOLv3q/w/nyv7q+zgb25w2jaxkBFKfjK7+C6fT/v1/x/6F3fh46Cjppncq1+K9O/SODfmkF3rk8lTGs9z9A8VUZ9FyUIb4hNNCgipPrKqIqBAU/I+CBSU0rQECA9NFNqja7mAfkuWx6HPWxbnow9Lc9PE6Avx6J4Wq1lA13owvR15zVHQURpNcbxhRGhqjdhgptNqhKm026B2VFFojoRSYKLVNDZFGqw9jgVFp40RvU2nhzZKKbTwXc6qdPBtfU5qdPB9xK0IJaKbTF9wwDFwFOqm08Y3e4NraKi5jcA4MdRwwJ5qbVSFR+89hdci3F20yF2n+7XxySu/oMdptwCKg1ByU2ni+91S8hgTNjmptPAmYKdPC3TdVOnhLphzSVhXe6pGAzdVJgMyDxTp+ZSMP3HVI8C6cc0jwl83VBkuuOqMPAGcVRh4DvoxWAdcdUYchb7gc0YeONvW8i1geW4SU9NQS0+YW/q9PlU99+MfPNz3k3Gou1aicauJNelV63r9Xi832e7Xf8AY999xuVtZCRrDM9jHOfm6rhg3yXL+bxnN6dX4vs+z1X6kSMfvbXs+gwxloHAUwC8/wD8f/p/y7rPh5/eZw7aNsZxayX4GQrr9PP8+v8Aj/0Lv6PKRVBm6Fejfs4L92Wv8145hX9mH3Juj6B4pxHYXYtCaaCqElPzTTVJhH5HwQKSmlEBaA7lvJ/Jj/wj9iws+XXzfgZkqlitA56eFoC9PE6HWnha9G2RdmuM9r+vgloaI5OqNDXHIOCnQ2QyqLTbIpApptsUuCztXGqN4Ki02hkg5qbVHtlU2gxshzqptUISKdMxs2CnQ524e4YLK7gtnsc505oHD6QEvk2PfPc9vZCjLhmrQ+rQau1U9OSJzb9Bbj5m/fNzkY6N1zI6MvLw0k5uzXV/Xz+jPzrLJNI1rjroT8E8LSILmRsmoGhGNVV5KV7vav1Dv7exit+yLiZuBfK+hpwXH16Pn6/DedvcbRv7NwsWXDmGFxoHMd+boeIXL1MuNJHNuvfO3Q7gLYPD2V0vkH4SET1dWaex0J/cFhHbNuO6HRv+ihFXHkAs5zarA2u92t5GZIHgtBI64cUWWDFsv4ZdXbeH6cDQ1SxWJ9wkC3XIHHBJUgDcjmkeBNz1Sw8D9xVCsA64xzRh4B1yEYeFOuMU8PCnXGdCnhgdcHHFHiZbrjqn4mVJP1TnIcje5JZLcsYHEUq4ggDzqt/TMrP27nw8Bctb3HZnHCuPivV5eV1Pl3/YMUJ3eW5dgLKCSRppQ63jts/56rj/APIdXwk/+6//AFdn4HG9/wCHpved0Jt00tNRFGxgPPSMFw/hc5w9L2uRuuNlYHICJ1f/AMjl0+n/AG6/z/7M+/8AWPOx/TM48XYLtv2cV+7FX/uCOYP7Fp9nP9y7r6B4pxHYCfQPBNJeKZFvOJTRUQFPOB8ECkppRAWCgOpbP/kM8FnY6Ob8Gdw5JYehdIU8GhL0J1WpAeha8ro1ynMejQ0RvS0NMb/JLQ1xSY9VNpxrilNQotVG6KVZ2qa4pFFqo0NlFFGqMZMOam08NbL1ootPBd8UzStPHP3De/tJI2ltYnE96UmgYAiTTeE9we7Jb0m3hP8A27C7Q92LyOdeC6PX6s+ay67ecMkklQXEAZuK2+iELnBxIOA4lAUXF4BPpYgCc+CNuFa8+KXzT+E+4mbHra2jR+J1KlHjD+Rxb3dtjdH3CG5tbwB8lN9UOeyltv5akuILzxKq8CdNEm73DmNjMnpbXSDjicz0UT1xV9lo4PcW4Wzg+GbS7TpJaBiOqV9PN+pz22NFj7q3C0aezIRSuBxBJU9/j81fPtx0f/N90Ja4yA6hUsAAAWX/AFeV/wBrPee8Nwnt+3JIBQ19IoTTJVz+NzKL7vgp/um8dcMnbK8aAAGk8s8Mk5+PMwv7rutdt73vu60TEGPVi6mNFHX4kz4Vz+R+rqwe8RM46LZ76H8OOCw6/Gz610c+zy+krc33BZPBJk0aTQ6/Ssr6q1kYp/dFjX+VLq0u9ZAJFPgtJ+P194XlDYt/sZj6ZRU88KKb6bFzDH7nbj6pGhvOoopnFOzCzudsRUStpzqE/wCukU3dLWR2hkzHO5BwJVX12fYp1FuuTzSnJ6y3MncYWEkB2Bpy5LTmYnr5eZ3jbYomt+3B1PJw5Ls9Xst+rh93qk+ibFux2oTRSxml0WB0vEaK4EeLq+SX5Hp/syz7NPxPdPXfn7vSe5SDukhD2yBwaQ9v0kFoxFOBXD+L/o9L3fVmv5Q7aLNn4g6Rp8AQf+paeufzv/COr/FxmWo+wuLhzv8A1dDW+VSSuq9/yk/Zz3j+NrjuNLhvjT5Lo+zhv1Dc10IiOy6+gJp+wEyLdmmiogI/JApCaEQFoNrjuGsjaCpsaTo7uila5pYrULuSBoS8ILVawgteia5bMTWPxS0Hsf5JA9kiQaI5DgpobIpVFVG2GagUVcaGTrOqObcKTNZPjmoqjRcKTZ7/AHI21rJOBrLBWmSOZotfM903O6nlcZJHOLzqIrh4Ls44kY9dOeHnP9q0QJshHWhySsMxgLuGs/8ACEqYZnv80QVmfI6vXiqJTpnOAqTUc0YNAXJhYeRxxSNWriUwIEcUgZGyZwoxhcDyBSti5zb9GuHatwewuLNDAaFz8D5DP4LO+3mN+fxu79jotsgMga+4qTybT/mKm+259GnP4835pj7Pb4LjtlxmbT6tQAJ/y8lM76s36L/p45ufUuSe3adMcMbSOYLifiSqnN+9K+P2kAzct0nAijfogbhpaKAeARfXxPm/Uc+32dfE+hgc5sIBBLxk7+xLPlp8yMlw+7edTjiBgAaLTmSMe71Q91zQAWuL+I4D4J4XlRxzznJxZXjTH5pXmK56pmqOOMF8jjTgcEstqviT5rN3WOka+M0eDVrm4EU8FWMtlvw6bN+lZHoNXPOZqFjfRLXTPyMmHRb9IXUlhBbxIwKi+j9Kvn3z7wz+s2Ejvpc35hL+nqH/AGeul3Um13EfbMxYSQcv4hPnzl+hdcevqZrTc3dtIYe08FjI2R1JBNWNAUccWbv6teup8ZWh7WfYQyPNWte+jeZo1RL/ACsaSTxIvNEXt+OMYGad8h54ADNVx8+239In3cyeufu8pPUSgj8y748nv6rujgAnE9lD/TATR9gIIt2aaKiAjzggUlNCICIAqoNZcSP2IC2yOApXBA1C8lIaDV1TGvVh3VNJrXJA1r0aDWPS0NDJeqmm0RzKacaop1FVGlk4wUVRjZxVRVQ5s6mqE+7axhc40aBUlTgeS9w+4XzPdbxEGCg9Q5ro9fr+6OunmJZg52oZlbyM6Bpc5wzKZNDNLCDLywA5qaqQqW5eagGjRkAnIRPccRiUwujjSmJPAJDBxWd1KKxQveMjpaSKpXqT61fPr6v0jTFst8/DSGu4Nccfgovu5jbn8XunO2N8btM0zG5VLfVSvgpnv36Rf/Us+tGdt26JvqnLj1o0/wC7WqX9nV+y/wDr8T60UZ2mNrWiIuk5uBP8AlfO/dXM9c+3yc3crpkXbigGjIa2gUH91TfVLdtaT29SZITcbldCrQ0gDi0AD5qufXEd+7pjDrqQjUTU8StfiMv5VZiui6sbdP8AexJS2H4dfZI7Gdubw1x+qgx+KL3D59N/UQjlB0NmfTjSgA8xVLZ+ipzf1MbbMIxLnc6uJCXkueuKfFBGHSNYKtwcGiv70S2i8SfOEsudYLWsBJyrwVXlnPZv0jQIWtwedL3DBvRT5Npx+pUkTMdR1CnDIHzTlR1xFNs4A2rWlx4uOSflSnpi5LOEAdkOaTmSQMePklO79zvon2ImtSHAB5PHPiqnTPv0obMYVDqBHkP6RsaxhwDh+9Kr55kNMjaHAE1wH70saWx6HahDeWcQkq0MdSQDOo4jxC4fdbz1cd/pk75aPcrduFm5sEjv5bm9qJ4oQDWoNMDgp/FvXl8n+ZZ4/wCHiZWO1DlWpK9SPF7gbk1APROM+y2/QEJAQmks5ppRBKk+lApSaEQEQD225cwODgK40NEtaz17F/bGtA8V4BGj+sJtpqkaUaX9fQXW8zcx8wjSvroND+RTLxr0zXoSY1/VAMD1INa/gjQY2QqTOZLySB7Jsc1NUcLjFRitMbcUU2HKOS+bDE6Q1NMaJeKteW3nfJrt5bHIWx4DQ0/FbccYz661xC91cytULDi+jaVd80G2wW8sQDnMOp3TJRepWk4ozttzO6oowcC8hufFT/ZIuejqrj2y3jdW5uGljTRzYjqPxySvst+kXz+PJ/tf/g0tbs7XahbuLT/pNxJ8XF1Qo/n+rac+ufYY3i2i9DIoxwyHDwyS/pt+6v7+Z8SQQ3Rs51H+WXZGNoH+WtUf1Yqe7S5py1x/mhp41xJ8QnOf2LrrPuW+4vHR9tjqNOdGnw40TnPO6V66zIW1rA0NcauzqR81VqZIPUwtBL3FwxAazD4qV/8AP/yXHE6YlxaWxNxq40KLcPnm9f4MPZABe+jXZHA0p0U/Krn3BrD2ijDj+LE1TzC3fsdbw3szNMULwXfjIIb51U9dcz61pxz1Z8RrGw7lI0a36BTEZ/wWV/J4jX/rd361oj9ru0CszscyAKfDFRfy/wBmk/D+PqUfbk4dp7ocODiKfsVf9qfoX/Vv6mxe2iSe9K0Y4NYKYdc1N/K/SHz+J+tGNghifpZjXMt+rHqp/wCzbGk/G5n0BJsbK1o+nzVT8gX8eESbDC8aWyvYD8VU/Is+zPr8SX7nQe3oGMDXyPeTzNAp6/JtVx+JJPrVv9uQuFAS0Z1qlPyqq/ic0t3tZ+bZnAcKgFVPzJ+iL+F+7PJ7bumv1B4dyOP8Vc/K5Rfwru6S/Y7sZnLiFc/I5K/i9FnabpjquaSFX93NR/1upWra557a67b2OEUvpJOQI+krP3czrnfvGvpt56yz4rrbjbMnY7UMKA+YwXL6urHT7eJ1Hk7uINLhwBXpcXXk+7nGGXFjR4haxydBFNNEJLdxTRSyCmlDkgBk+lBUpNKIJKoMzTgMR4JKxHE8MUxVsnmZ9LiErDndg/un56jXNGK/sqvvJvzHOvBHiP7a7YcmxMa5IDa9IGh6RjEnwSBrZCFJmCUDilqpzTo3l7S5pBDczUYBRepGk9XVMjfqpRwDTxJopvUiufT1QX1o6WFuq7jhhP8AqEGpA8FE9vz8Rtfxfj5rgybfZNe7TMXt/CSA0+JGK3nd/RlfTz+pb/6dEBhrfXM1y5qv5Us4hjN1ggZoiY3Opc1tK165qb67fque2c/SBk3Fz6AuJd+WmPmU5xIL7dC90hxcXAHKpPwTHyFkbHuDZRUjFoqcBzqi39BOd+p7rOV7BiRCM21oSfPFR5xp/XbP2C20A+m0c41wLhRO9fuJ6/8A8TNE5oBC1nWuVOdEtn6ry/osOtoxV/8AMlJ9TY/lil805eZ9fmtccN1LCCyzc0Eg6jUVp04rO9SX6tuebZ/q6Nvse4TBtLfQM6vpQLDr8jmfdvPTb9nQZ7LvCKyTNoeQAosL+dz9oqfj/rT2ezIA09+4Bxyxoov51+0XPx59z4/bvt+3GNHHi4kBRfyfb0vn0cT7LezZYQezECeg/elL7L9auSQDdwZU9mP01LcGjh/iKq+r9afl+i/v7g4fbgD8xxw8kv65+p7Whk8rm4sDRwoAFF5iibh2kanaiMqNBJx6BVzNFDEyN1DoIHXBFtOGP7YyA8EoZbxGRU4FOaMK1EGgphzVBeogVwryCAB0mqmQTkBfdc3DLkqwtKkLjXS4ivGqqESGy8Xmo41VbC+QvqMdXlVOAh1zI0Uz5giqucRN7bo5hcWL30o5rXB3iMQsrz49Ye7HltxA1u6lej6vo8v3xzHDBbuGkOTZUJZ6AeKCz4UQUFijkmkEn0ohUpNKkBYzQDPBCkIdzH7EhiUAzIPggIUwqiCdyqSRB1EgLVxOSR4j7mNhAccTwGKDxbrshmtrf97BSucFO3K8neXUDGjgwCNvkAlOJF+VpJleXkuPUHEqsAm3HaDiMNQpVvp+NErzpzrFG/upPSBqbkKAnJHhIP7LROjneykjwBnQo2H42/Uv7cVOqQnwB/cjS8P3Pjs4SKFriPAjzU3ppPXBCzia4aY9VchhijyP+qfocKRNOAGvBzWiqn6tJ8AEb3EASOb0DRlzJxRpeP7nxQXgcDC1z+4aCR40gnxIyU3rn7tOeevs6NttO4yPEj3MZwdoZUEeLsFh17uZ8Ojj0927XSi9syuZV0rtNc3uA/YFhfyp+jafj/udHs+3QNcHyCR4wADSfm4qL7+q059PMb7HbrcuB7MYa3HHD40osfZ7b+q5xJ9nQlEEOIkjDqY6BXPqsJtXKS3cLUklzXu04a9LiD4Kr66egfuB1ARsdTg52H7E56v1GlTXU0mBdpHSirnmQ2XsNe4mhca1rxWnlgw6Jlu+QxhwL2ZtriPGim2yaPg2KybEyjG41JPDFTe9OTDAKVqwV6/wUqBPeshj1y0YPJPn17fgr1I4t/7o26EhtTM78rf9gF1+v8Tq/s5/Z+Xxz+7NH7pklIbDZOIJ+pzv4Aq7+JJ9ek8/lW/TlvdezljHOYI9XD/aix/rjp8qjpnHOpHQVROT0cUmttKkEZVB/sU9TFRHQupnUonQwGIwzVFhU2FTy54KuSpMV3bPdobK0vOTQaqrxZ9kTvm/GuduFtuz5y6KYtg4NZgQt/X1xJ8z5c/u9fst2X4XZyXWl0cmpwbk85lHcn1i/VevpTiHGpoRTGtVLXGzaru30SwyPaJH4MjJxNBjgsfdxfiw+O59HC3iIRyuHXBdvouxwfk8447xgV0vNsZZCqY9NtnuVqLKaC6txO4tAt3/AElhrnqAqfBZd+u+Usufq39Xv58Lz1N/RhOa1c4SMEyC8HSShNhCaEQDbZncma05cfJK1fr53rHS+3ZUUABHFZ67v6oE2rQcTqT8k30wt1mwfhqOifkzvpkKfaDMVCeovqD9oOaNT/W6Uk8MT6V7lMy3BtfHil8s/GQp96C00o00oKYHxRlPYS6R7mjU4+VcUwprXNBJ9I4koGBL2aqj1fFAtF35KZU5VRg8qZF3ZDpqB1JSvwrna0ts5XUDzqHBo5qPKNZ67TxGIwDG06mmobTiOdcKKd1p459Gi2t7u4aaxggU1ANr8wo66nLTjjrpvh2Tc8O3a0acu5Ro+Cx69/H3rbn0dfaN8Htfc53etzcRR2mvHlksevy+J9G09HV+q5va8EDiJJHHThQZeFUT8u36H/1eTLT29YUBjaNQ4ZnKn4qqe/yevurn8bmfZ0o9ltIGBz2FxP1Nc4VNPBYX39Vtz65F6rNuLYmRln4QCUs6/VckSW+hlADWlp/KMPNKeuw9BO6sbGMfQniaup81XM+RVMa9hHclGk5OaA3ywxRbv0gmjdextODvTXM5qfCnohukFcq05o/qo8otrzL6hSnBKzFQbyGtq7FKGKOFrvV9VOOKV6M2FkIfRpDq8FPVoNbGQScq8VFoFoeXAk4DjxS0Buo3OZSNwYRm+lSnxf1DmXOzw3jGtvaTBowdi018QV0ce+8/6/CO/VOvr8s8Wx7RCKNt2HhjU5eKu/kd37lz6OJ9IfFDaW4PaiazqMVF666+ta88yfQqSO3eNXDlTBVLYeShEbBXS0g8CE9PxLdK9oOHHBPNLQsne41OBTvMEq3vr480SGU9+Ba6uOdVUhM8VtBCQGsaAPpB4K73ajniRqje1xwIIGazsaSo8MoaAAoh2MkzSDUZrXlHUYo7Jov47pw09upwyOBC1vf8cc19M853+jLu8wlmLhll8Fr6ecjm/J62uQ/iumPP6YZHZq45eqW0mqaIaCpWjTzyQIONocQDxNEVXMYjgSOWCpgiAOCZ0Uge3McOiVmq47vN2OyyVj2B4OBxAWVj0+epZqOLKVJ61QdwOocPJPEbAlrXZoTZKHtjmnpeBL+1X11pwpl81Ti+B/8Aa+nOtOGfkj5P4D6KenXq60QSN+2r/M1V4VyR8iYaztaTp0cPpz+alcw2H7Svr/tSuq5ww6sOzpzwp+6qX+Vf4Mh+71Gv+elK/JK4vnyXPq1P0Vp0RMPrXudl+3/o1n9v/paR3tH1d+nr11xr4YUyXke/fO79f/Z6f4+eHw9E3R2W/Tpp1r51XDfq2+QXGrteiva/H29WquGWnH4J8/U3N3Hv9/8Am6e3T8PLhmt/XmfA+XKd9x3HUrnhWlPkuiZifnS5fvdY1ZU/26pzxF8gNrV31aacaVVXCmmWvc1HOnGlP3pd4rnWqXt6fxV4ZUWcaEnt8NVONaKkmD7HQK14cq0U3VTDP/1uk/Xkc6Uol/I/gcf2/Z/l6tNBp00p5Kb9flU+h7fttP8ANrSuOVVIaoft6enVppxpTzWdP5RvbqdOXWlPklT+RnVQVU/AGdOk66U4c6ImEU/6vT9HXNP4Mg6KmuqnBafBxln+kf8At1/DSqvkIP6fQa9dcKV+WSPkwGmo0rTgn8H8ibo0nV51Sp/JLuzoGVOCoizox0+dEx8s1z3MM6cFpxiLrHJ97Q0/y8/mtZ4ovkQfu+56q+av+OI/lrXaa6HXlXzWfeNedPOnGupQ0+QO0acPNOBkv9fYPa1a+NKf7UWvrzfll7d8bjh3X1DVWvHJdnLyvayS6NPGvDJXHL258mmpzWkcnQBorxqmk0duorqp5JKGPtv/AJPkgQbO1X0aq1FK0qpq+WGXTrdT8x/arjHr6hFEEv8Al9UB1LPsfbN16tWNKUy4LPr6u7054/Jru1h9XySaUR7VMNVadElUI7FfxV8k0TNV/K/vfLNBP//Z" data-filename="romantic-manali_1466979022.jpg" style="width: 450px;"><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span>		        	</p>', '2018-02-09 23:04:06', '2018-02-09 23:04:06');
INSERT INTO `user_helps` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(8, 'Even More Text With More Title Goes Here.........', '<p><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</span></p><p><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</span></p><p><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBLAHCAwERAAIRAQMRAf/EAKUAAAIDAQEBAQAAAAAAAAAAAAIDAAEEBQYHCAEAAwEBAQEBAAAAAAAAAAAAAAECAwQFBgcQAAEDAgQEBAQDBQUHAwUAAAEAAgMRBCExEgVBUWETcYEiBpGhMhRCUgexwdEjFeFicoIz8JKissIkFkNTY/FzkyUIEQEBAQEAAgEDBAICAQIHAAAAARECEgMhMUEEUWEiE3EygRQFscGRoeFSYnIj/9oADAMBAAIRAxEAPwD9Ste1wwKmwLSNEBCQAnITFdzDSQtOYHDvZTQ0XRzEV52/fWvNdHMRXAvONVvyzrjXLc1rE1zpmGqsmWSOqoazujzTGluYmNCWILVUIQNaotz3CIAMmeAMhVTeYNObvu7B1RO5L+uDXYtfeDmxsbNFqcPreMPks76VTt1ofcm2TMBdIWv/ACnAhZ31WK84h9z7fI4hrqEcHYAo/qo84I77t7x/qsBFKYpf10eUDL7msGVDpA5wOFET1UecYLn3QHHXGW0BppJId4ii0nqT5jZ7hsixrXAySf3nClfklfXT8hXV1czYRQB5zBJB8qhKSQWpG29iYO49sYeANJ1VBRcoZItu3Oe8c+emllAKEEkeCq9SQZXauLaA24Y+KVwrQnj45rGX5XjTt+3vso6RNL2SYuLjj5KeutOTGwWcL2aHNGl2Jx81GmG4tHGEsidoLfpI4InQFBZvY1okeHvbk6im9G0NY3EtadQzU2hAzSMcTzU2mW+EGhz6o0KERPH0jgMMVNphcCBgKEcUtNYZ6MeOVErQoM9NKerilplhjwaV9PEJaEfbxhpOdchwS02OUt1aSzHMGiZFSOu2AO06q4jnRL4MLbp5PqYSeYCVgWLkhhrQVU0Ct3FxOJfQ50wU9G1OjDoyAPGiztNypniFxDjpaUw5O5+7Ng2/ud64DpIml0jI6OLQM9RJDR4VqotOR5Ob9a/bTH6Y2SltaatNT40RlGxp2z9XvZ91M1ktzJaudhWdhDK/4m6gPEovNGvWN3a1niEtvIyWN30yMcHNPgW1CzxWss1295wKWAk3RbgXeVUsMv7mGuLiOuammXJfxt+khw4VU4ZH9SdzGaMN+k+zNHTSdbfmvR1ztDO7pBNPBTcC3dymAR8EyzXRFQcxmrkDm3NwTxWvPKbXIupia4rbmItcS8dWq25TXGusSQtuUVzJ2nFaRNYZmZq0skjFUIhzFQKMeKC1RiTGh7WKCUYigJ2kBfaqMQgasRkCnJA1fbPBATtoGqMSBqdlBaghQemNdO0ANkcBnQEpYNaI9w3JgIbO+js6muXipvMPyrdtW8XNvcmaasoLdJ5lZ98SxfPTv22/fdyduNojHHWaV8FhfXjSd67EXdltxI2QA1pQY5LK/CxQ2rhJqfO59fwgABK9A/tAEjhxx4KNMYDSXHKmSnTV6qam58v7EgtrSRWQaehStNTms04mo4AJaZZLssA35lIEumhDqOIpyCALuNODPippqJAGBoeSRqAAxOZSCiMKg+CWgp0YdnU04BLQhAxFDUpaZPbYw5DqloU+NpYcuhS0ywXUDQFNoNE0UED3zODdALjyoBWqi035/wDf/wCqe531wYNvP2kAPqLHHuHoThTyT4mn18Pmtze3E7tUry89VpIztZy8pkmooDse2/dO57Dei4tH1jOE1u6uh7eNRz5FTedOV932bdbXdtsg3C3GmOdtQ12bSMC0+BXPWsPcK4NFSpMl0Dji7AJaeBEDRk2p5qLVSL7X/wAbVOnj9C2u59wAscangV6/XGOfW+O94PaarPxBv38FMTRLxpMtzdWpaSSCr55ocK9uWaiGinnVdHPLK1ybi4BBxW0iXJuZSSVrIm1zZnFaRFYpeKuEyyNVxFZnsTIh0QTIBiTCu2OSZK7SArtICdlGgZtyMSKI0J2W+KNCuz0RpIIeiNC+wUtMQt+iNIQtyeCWmIWnRLTwYs0aMELPolp4NlqRiAptNtt57yFumORzRnSuFVFkq5a0R7huTTUSuPiovMVOq7G33pmYW3cpaXGgoKAeax75z6LldZjbV0QZGQ8UoSDUrG6sUUBb9ApzHNTaZz2NdnSvJQZMkWmuggCnFGhjfbF5qXI0MslrpNa18EaACQtyBB6INbZS01ONc1NMTbmppow6KQex5IxAaOFVNMuRznVDXAckgWYXn6pEvIYEWzdXqq4JeRjMY0kNAaFGmSYtP4wloYt1ge+xnaw1e5ha3GmJwzU9X4Pn6vzd7/8Aasex3EYdcia6m1umhaKCNoNGY1OLhjTgj1d22z9F+zj48v1eMcV0OcNcUEiAsHFBvtX6VfcO9qsLwNHekEZGekUGPWq5vZ9WvP0e3ZGD05LG1eKdbOIzFFF6XIA25HM/sU3o8D2hyU6ePfWe+XUdA2UgeK+q69cebOnVh9yXnGSviAs76YrzHLvM0uLn18MEp65CvRDr9x4qvEtA+7qKVTkLWOaUq5CtY5XVVRNY5cSVpEs0gKqFWdzVUKkvYmmlGNMgmJPQrslGkghKNAhCRwRoX2SloTsEo0CFv0RoX9ueSNCxbnkjTwTbYpaMMba9EvI8OZak8FN6PDW2h5JeSsELPop8hhgs+iXkeGCy6JeR4MWZ5KfI8ELM8kvI8OZbGnqFQptOQ6CEscDU+Si1UdiK/YWBr2kHmFjeVa0tibI3WDUcCs7cUTLGKVoepRoZsWmtPT80GCRr3DVGynVxSDG63mca4DyRoV2JRgfklprbaPpUuIU2gyO14HEeKi0xC3b+VTaYCzSa0S0KMgpTLoppkPfTDBIEucRwQCLiV7YZHNaHOa1xa1x0gkCoBPAKacfmb33ve5b7ex7ndQRwse3tRmIUa4sNDjU6i3IlHo5k2b8tfduT9HkXZrpcqqCiAFBLCA+v/ozfCbar6wdUvtpRK2n5JRT/AJmrm9/xW/r+j6A9raUxCwaKGhuRPmVFVC5Zjwr8VOGT3H8z8UsD00Fx1X2VjyNbYrjJRYetTbgkKcPRtkc7JLAexkjqA1HJLTNdZOI+rFLyGMr9vnqQMVU6LxJft1x+VVO4XiTJt7wMcDyonOy8Sxt2oZmvgn5l4skts5pIIVzpNhX25OQVanFfbEZhGjBttScgjyGCFk8iuk04lLyPxH9hNQHtmnOiXnB4iG3yk00mvgjzPxGNsm4tKXmPAX9Mk5US/sHisbc7iEeY8RN28peZ+JjdvPJLzPxPZt5wwU3s/Foj28g1opvapyb/AE+prpS8j8RDb+iXmMMbt4U+Z4Z9gEvM8V9gl5jBCxAS8zwYs2hTehg22bT9IqpvR4dHYNH1YDlxUXs8aiA1ullA0KNUU5jjxwRoAYTxS0B7YzrlwS0FvZGMEtMBHIYJUB0VOXxSoA5tMK0UmBxa0UqSkCXych5owyJS8+HRIM7o5M0gS+NxwJKRvnP6w7pu9htUVjt8cjm3rXm6uI2udpjZT0FwFG6q49EvrV8vjHue6bLdwxQxugs4IImW0LswCwOc4/3nvLnHxVemfFv30e6/Mn7OHTFbMAuPBBBQFhAfX/0VszFt24XroyPuJGxslJwLYwSQB0c7Ncn5F+XR6/o+jPLMyVg0Kdo4FTTJfQKaoFRySDswSt4iq+1seO6VmIXnF1OhWfWnHSZb2w/9QnoFntVh8It2HBtepSun8NH3QDfTQDopwav75oA4njVHiNUbx7vpcByR4jSvuJwcXYdE8haMPJzJNeeKAIscW+kYFIM01iXg4eCudFYzx2L2Hgnek4abOEmrsSl5U8NjhgYKCPUlbRjUwse0tEWJ5CoHkpUKMUJYYya5cEBtiifWhiAwzOai1R8YJjqY26hhQhTQU+3ic5pYxoc7NpATlGKkt4NAEkYEmVWjBEtGMzrIg1LMDkQFXkWLbat5I8hhrLZo4Kb0eGCJoCnTXobyRoWGAmgSCiWgoCg8Vyr0SAw1xOIHRTpiADaFxGHJIwlzK1p8UgIXEbM/klYYHXbK4YpYNCbvolg0JuzyRh6W66cUsBZnfzKMAXTvKWABkl5pABdJzSMJ7h4pAOl/NIAcHUSNxPcXuzZ9ggEt/OGvd/pwtxkd4N5dVF6kVzza+a33/wDQNlqlEG2v0gfyy+QAk9QAUXm/YfDzc/68+5HTExWts2KvpYQ8mnU6k/ClsejsP1S/8l2HcLC3jFrvroH9qEyaGPFPU5jzxa2p0rP2c/Hz9F+q/wAvh8d39ssV62GVoZLFDE17AdVDpBxJ44rT02WbP1Hu+ufs5a1YoRVADRBNEFhdTR9yKMvBkbEwNFS6R9SGtHE4JWxUj9Fe09hO07BZWLwBJFEDL/8Acd6n/Mrh762ujmOq+Dos9VhDrXip8lYW6BRelSA+3S08ddkdCQRiF9vrxGmMOBBGamhpZJLzKVh60NfLhipwzgZM8VIMAeg1iNyCEGP5pAbWydUjMa6UJAeuU5koCBpOYQDWCn4R8EjMYXMNWih8Ega2aUGowPQJYemiaY8fklkGmCWYmtcfBLIY2OfkXUPySoG6Ig62vq7iloQRPkFHOwRpj7E7RRpq1LYMB9rJnTFHkMX2H41NCjRgXRnmEaAFpyJQAkDmgKcAaYZJBXhggKNUjUdSQVRyRqLSkFaEtCiwpaYdB5JaFaDyS000dEtCaCptNWg8ktCaDyU6AlhRoA5tEtN4b9RveV7sNi/7OIl7wQLjMNdSoWPfd3I144mbX53v9/nvZLm5u5nyXkpB7jjU4nELTwHnMrz7nGq1YAqmQoppIpGyRkh7TUEGiVhyvq9l+ice+bbb7vYbyWRX0bZmsuItbm6hi0vY7GhwyWU7z4aWa+c+5Pb17sG8XO13hDprd1C9ldLgcQ4V5hXx35RPXOOZTgFaVho4/BBPoX6M2/e90ucdBbBbyODHnGri0aoxzHHosPf9Gvr+r7lowyXFW8Lc3HJTVEvb0UVUIeFCiqdDmkbo2+4bS9/8xkjBwoalfdXnp4ex1oo9teAYpxQ8HZrO2qyNMcFsDp7gr0p/FTbTyNTLe3GJqfFpA+KnaeDEMZBLRgONUtGBDBwCNLBtYa0ojRhrYHHIV8EtGL7RGYRpiEfRGksRpaBiJGgbY0jMDEBYaEgfFDqaTyU2ng2iP8vxKQF6Gn6PmgzA8tGDR4JYAmZ3DBGDU7siMC+7IUsGq1P4oASHHNACWVRoV20tCdpK08Ts9EtGJ2UtGJ2UtPE7KWniGEUS0YHstHFLQhY1LTAWtSATo5JBRI4NJSMJ1flSASHpBWl3NK01GLmSp0YExNRoxw/cftux3Wymgn0sEjHNL34NGFcVz+//AF1t6rlfkr3Ftcm27hLayfXGcaLo9Ps8+dL3+vx6xxzmtnOFAQFAfTdg/UHdNq/S6a3294Ze214LfvnF0UNw1z2ub11NLQeCw653prL8PAv3Pv2ksd0XSTl7pI5neokyU16iTXhVX4Z1sP8Asnjl+v2Yw9wa5rTQO+rqFpjHUFc0w1WV7c2dxHc20roZ4zqjlYS1wI5EKbNOV92/Tv8AUKPf7f7K+LWbtC2pIwEzBm4Dg4fiHmuH2+rx+n0dHHevaOezNc9jWESv5KcPWSUvNcUsPSNLuaRnxbPfZGMj4L7rzjxPGtcO1XoNO26nOoSvcHjXSs9puC8dxr2jngs+u4qcuzBt8kY0/cSYYgLG9fsuRuiYSCJH1DvpBH8FFM1sNnTIV80toELeJw9IAHNGgDoCPpeB8k9IsxSA51T0IIZEaRsdvX6nEeSVp4cLdo/ET5KfI8WIeVUaMGIHdUtGC7ZAFTgjQlWgU1oCw5n5q/FIDaAefwQZmlSE0I0JoKNC9DktNO2UtGIIkaMEIgp0xdtqWgJYloDpKNCwzxS0xCFp5qbTxfaA4FLQAtHJLQEx9EaFdsVS00MLTwS0K7TRwCWgOlo4hLTA4M4uCQA4xcykAF7AcG1SMBk/uowaWZHcGhLBrn71E252y6imeI7cxuM7yMBGBV3yCx9+eFa+m3ymPyx753Pbb3e5ruxj9BL2jWA7U01aCQfxUOaPxPX1zzldH5Nm/u8ZMwseWnChXbK8/qZSimSkBv2m+ltZJCyQNBY4gOAc3WGua12k1Goajp5KO+dVzcYeitJjRzTJZcEBYOCDb9n3O623cIL21fongeHsPhwPQ5FR1zsxUuP0V7Z3+03/AGiLcLf06vRNFWpjkb9TT+0dF5ns58bldXN2Om5qytXhL2BTaeF9sKdN61haQPVQeAX2byjmmJv4q+ACQOjfHXCvwSsDS0sJGePRSDmlo6JAwNbxaPikEAi1UoKo+Qsm3GB0gjqj5AXS2uWseSeUtid62H4ifJGUbEFzFXjTqEeI8hi4i6/BLxPTIpYicKhKyjWj0njVSpCwckEtsVcgjQMxaTQ4FLRiaAjQmjHNLQv080glW8z8EGvU3mUsCdzDAVRg1esD8JSwavvU/Alh6ndP5Qlg1Rkf+UfFLBodT+SBqtb+SR6muRLAvU7mEjMLNMYe78WVOim0wNIeaaqdCkAPY8GhRoAWu5pBRYUtATGUtCdkcUrTwwWg06jkovSsCbfAkNwGCWjCZI2tOIxRoKcxiA5+9bXb7jttzZSktZPG5hc0kEVFAcFl7ePKY09Xfj1LH5Qvdolsd/ks5YBI+J743RvGHEV8s0c+3fXv0dt9O9/Dy+7j/vpjUO1GuoZGvFdfq/1jg/J/3rAVowUUBEAbQEyGTw+KAoBAWEjECg3vv0n90M2zejYXL9NpuOlgJybOPoPn9JXJ+V6952fZv6uvnH29xqvNtdMhT1Np4Vjy4qTdKK5caL728vC8m2K5f5KLyryamXhHD5qfE/I9l83w81PgPI9t+zqfNLxHkc3cRwGCXgfkYNwr+EJeA8l/dMJFYwjxLyEJ2HOMJYNNbK3IRpYeoKOdQR4pgwMdXBtTlQYlSZrYX0rppxoc0tNWog00owtOg7j3AAUbWhKm/Bw6V7RE4N4fiShsjJ3MeXEaqihBVYWn275J36MAeGCm/Bw58b2kCmqvEKZQOTtxEBwqQMfFKfJhYSGa5NIY76SRjhyCKAvu4wCGNqeDijxo1bZnvbUChGaWDV65Esg1dHHiEgnbPMJaeL7fUJaeJpCWhNISCaGpaawxp4JaD7iJ3bbXIBZyqxh0gOVaTXGxjo6OHq5qLVYQWAJ6WBLWpAOkcktMUcDnuoBhxKm9HIdOA0NazJuazlWzd1zcBlWqeFpMhLnE0QRTh0RoKeOiND5V7h/S9zru93Wbcdc07naT2w3SH1A1GvBcPXqvHP1+Hpev8yfTH5933apra5lZK2ksTRqGeNT+4L0vT7JZ8OT8j0/dwiMV0OJNDjwwQA5ZiiANpTJdeHFAStAkEBQaw5AMjlcx7XtNHMIc09RiErFR+o9suvu9ttbo53EMcp8XsDj+1eB38Wx6ENeotVhVf2parGmNx8F+iY+b09hdzKinrQwHqkGhjeimmewHkpqj2tPJSDmgpA5jMqmlckjbYYAI9ZI6k4UHmotVIRLuDwdMXpaOPE+ac4K9NVvOJoj3CBJ+E5EqbMVKc1wt4ySRqOTeJU/U/oxvunul1ElX4p10YhFIwODtRypl8SVndi4u4uBDpZGcKY+KUmi3GZ9xUUB8VUhaV3sc08LW3b5dJc7phVZ9xXJ75ZA8Oa/IYUUzDPjfCWVnxcceqi79lRjuJg95PDgOQWkmJpVWoDRA9gA1/T0zUdHGgMY+pZl1UaeDEKV6PF9kKfIYsRdEr0eL7fRLRiu2EtNbYK+HEpXoYcy3YwgvIpTJRelYXczuJLeCJBWXS3NO0lSTlg0tUmBk75CGkCiKGhsIJAJAJxoovSpBm3Y3EuwU+R4pz8w3BowUmzSuIBNCmTMZaJggzODqg4pBYmL3eqmKQLfK0O0gV5qbTkeF98+7XwWlxZ7daS3V04GLUWFsLXHm91A48fSuH2+6d/G5Hd6PRnzX5l9wz7ib2WK4FLjWe5pINajIEdF6n4/PPjLPow/K9nVuVyBAQNUmHIcT4Lo1xYpxcMmmiZYU8hwx4JkAYeKAutPFAUgLQEGaDE04hI4/S3seb7j2ftEhxebZjXAf3Ks/6V8/+R8d9T93pcT4jryCgxwWF6XITqCnVY29kseW504r9I18zhsZbq01GrlxU08a42EqbTw+N8QfRzwCMSCeCmnGhksbsYtLgeZx+CnDaI7hxIAAa3i2mBSw9aXCABvbBc53Dgp+TZ7jc2W81GNa4NoCeqc40rcJfvMsjCzANJyVf14XkQbjjVPEiZeEZFHietLb17wKmtFPier7pRgNjvXsbpBwOYU3k5V/dFxrWqWBfeJQYmvqalKmcJXUwUYY4rl7Hg1w4pWHK0Pug8VBoeSiQye45MhCQpAWt2BCk2y2uQyrXfELPqKjbDPHJ9Jx5FZ34VDwFOmt2hpALvFTp4uZzI2uc0atIr8UpTxzfvpS+rjUcleJ1vhuYXxu5Nxos+oqM0l8HvId6W8CiQaAO1VLHaqZoJnMjwSa5pgBLiloHDq7jdP1VU2qjqujFAXZgLHVlXhLYmk/BKCuPJfPYSK0HJXidIfuEla6q9DkjBoTdtIxSCNd3PpIJ5VolaZfcNckrQVfzi2D6kNMYq85mvIUXD7vc6eOHhveV7FcRBsj3RsNKu4tB8FyW706fXMfIdwsbS+3C5fDDS1gIDhXF7jXQyueOkk04VK9L19Xnn5PvidV5Hd2QfcuMUemKM0bUCpPGtOuQ4Bd3q3Pn6uD35uT6RypDUrZzESgaSeKcInIdUyRARAQoCBAGwVcKZpKj9S+2GW23e37Gx1kGOCJtCAKODBqHxqvmPb3eurf3etJkLv7h4NWurRRFYz/ANQd+QZUT8Txypvc9sJA2S6BdwoSafBfp3jHy/j1W22uC7TLG+oOLXAosQ69vut0LgSOdVuFW8KLO8TFTs+6nFxcukYNIPBLmZB1dro7QwOlAc6mGDuRWfsXw6zbd/dEYkBfyzCx1eNOuO1jewOEtyfSByB5Kfr/AIP6OJO0l7i40IOI6reM6K1tnSv0t8zwAS66wSNM1gWYtJe3iaKZ2d5AyHGlMU9LHQg226JoInVpmcBRZ3uLnNR8DmOLXgtcMwUaMD2wjRiAAFIGtAzSMwBTpjaTlRTTXXFI1gpA1kcjslNp4MxyMNHNp4qdGGNBPBTaZsdvM/6WEjnRTejxrjtnR0fKdBGIaM1netVjSy8i10xA5lReaepPdQAGg1E8kpKelG6ie4FwOVCOCMGijto3RveAHEn0gqb0cjO+GZjyYwW9E9LCJO641KNC49bXckrRGk2rtAc4gE5KPJWKbZimpzgBwCV6PGqCGKNpLSNTjms7VSGNAB1l1TwCm02K9utbdIJwTkK1x54y5xJOKvUs7oeqNGAMfVLRiqEcVOmjZJGuq12IU1UYfcc7QWT3L6tyZbjDW8jDyXk+76uz1z4fLffO+OtLNxcWtnkbQajU+o1waOJ4VVfj+q9dfLp5kk37R4eyZd/0yOLVSS4L5Y25BjX/AFSOOf0s+C7++p5ftETfH/Ly28yRmfsw/wCjD6W8yeLj1JXd6p8bXne7645Ema2c5ExwA5pwqT14JktARAVnigJWiDdX2xZffb/YWrsWyTsDv8INXfILH39ePFv7NfTzvUj9DiR73dXL5p62NxhgEDQQ10lMarK9HjN9vHyb8UeZ4+PtlOdV+qPBdba98urJw0O1R8Yzkhl365XsNt9z7dPQSHsv5Oy+KVc3XqseihmBAcBgRUHmFKY6VtcBrcFl1GkrpDcHOiayHUK/V4rPx/VetMkrbVjQQHvfUlxGIKifKr8E2sQmL3PFWAVLuRVdXEyadDEKuij9WvJ+OQU2nI3RW7rduozFreLRmVF61UmGRXdkJy8YSkelxGAPNKy4ew4RX0jjpuAWnEmtPkp2foeU+3tHEuZKWSDg4mhqpvYkJuNslGp0ZD28mnFOeyFeS2bZeOAPaOPOgRfZB409m1StaXSOaw8Gk1r8FN9p+CDbbovADdQP4gahL+yH4jdt1w12kBrj0P8AFT5weJMsE0JpIyic6lGAaW16otBjbgtOCmm0NnErh3AcFF+DdOEtEBELQDxrmsbfn5WKP72UBpoAfD9yVsg+TJLWgo6ca+qnyPCDt9w51Q4Fv56p+cHiBtldF1CMODuCV7g8TorCQPBfQjkDipvZzloLZ2jTGygAyqo1WF9u5LhrYHUyxS0Yvtyaq6BQY1KWnipYNZyGoeKXkME+CYsoKYZJeR4T9pcUxIHRLyGCjtJB9ZGHIpXo5Czb3Lq+qgCXkMJksJSCQ75I8x4sp265dU4Dklex4s0lhdDhXwR5wvEp1nc/lKXkfiU62nGYR5DGW+vLTabGfc9wBNtbAExt+qR5NGsHic1j7vb4z923q9V6uPLe6velvtmyR7lcRMbusxlMFpm2Kp0jV0aG5DMrz/x5e7XX1xJc+z4Nc319vW6d66kfNrkL3hxzqfUcOJyXsTmcco7t6s5n0dz3Be/axTB5H3koHeDQAGU+mJo4aRSvkOC5vTx5X9mvs7nPOvAzHU48Scar1ZHkdXWKTNUhllJMhHAYKomgQF0QEKApARBvoH6N7My+9zuuJRWKyhdJ/nf6G/8AMV53/kvZ4+vP1rr/ABefnX3BtrBHi0Y8818/a9AmWCMmuJS1RWhvL5I03xNr6L9U14RzHo0miKWnFGpr23sf3GIrptlfSj7OTBvdxa13DHhVZ9z9GXXL6laWm2ZgagcAcx5Ln666E5jsW8EULSY4wNXILG9aqQq5szOayvOGWGSc7wrNMisbaOEs1V/E7gle7p+LFLLJ3tNvqww0hXPp8lTHQX4idI5h0HM1ql5QZWAh4OCrUtVvdXTCCDXhQ8lNkOV3Nsj0WrrmQ6pCSGspgOpWHd+cactEW4hsobI0Y8QKFReTlS4jAf3Z52wNdi0OcAS0ZpToYxf+Re3oHhjnuuDxeBgEXnqn8Ota3tpfgvsZGl3jQivMLO/H1M7+mFztUstCMfTnVT/YfiKS1tHAB1SeLuKU6oxnftdk8+iQiuVU/wCyl4xoh2izjZR3rceKi+2qnIn7TaH6QW045pf2UeIBZMjJ0SEBHnowxkxP8vSTwqFNhxqZaMpqLRjxdis70rBSRkkULS3lklp4nbBbQmg6JaMFVgFAaBI1AgY6ktCnSNH4ktMBmYPxJaMLdcxZa0gA3UVKCQJGS65b+YHzQAm6b+ZIBN6B+JI1fejmkeoLpp/EpATKeCRlmR5KQR8YcMfilp4+Y/rHuwt4Nu26KpfLOJHOP0jQRTLxXL3PLv8A/Wf+rv8Aw+Pi18O9y+4Lvcyz7l5eY2hlScCKk/vXf+P6Jx9C7610Ntt2bVtrdzlFJ5hpsGEflwdKejD9PN3gp7vlcT9Hmdxu3zvq4kjiuz18Y4vd7Nc6QUY53PALaOasDjmSqSyHGvVUlEBMkBRKApAW1Bx9l/QhtqLXdXuH84PhBP8Aco6g+K8P/wAtbvP/AC9H8Sfxr6dcX8LAdAFeC8d2SMEm7VFCKDoq8DxX9Vj5cEvCnj4gDzX6nrwjWOQRzHI1NaYZC0hCa9Z7e957jtoEReZrate04nA/3TwUdcyose62X9SrCWjLsPgkrTW31Np+1Ydeoa+hbfLZX1sJobhs0Zyew1XL18LjT/TYHYaquPHop86eHjbbRrQ0sAJpiOin+yn4nthgYO3QUCnyoxlOw2b5C84A/hGSr+6l4K/pm12pLpZmAtFaPcB+9L+20eMZbj3HsFpblvfa9xJ9MfqR49WjY8ju/uwTkNswYh+Jxz8lvzx+qL04M+53ErqySOeRlUkq/GJ0sXLic0jbtq3iazuo52H6HA0qRUDgs++dVzXsh77glFSx0ZpkMcVz/wBWNPJog3uK6Z3GSf4mnAhHjg0+LdYWOxkbXxU2B1rfdY3NAqKLG8qla47yOhxUWK0t17Br9RFEZQjt2to66KDqFPjT2ESb3H+YlPxHkyyb7pHoR4l5M532cnNHiPIR3G9I1AHSlkPTLK+ubm4bDU0JxpyU2HK07jHdtIbbNc8AeriplOuNJf3bHEPqCOBRhM8l7cPNS4owBFxPzKAITz1zKkzBNOeJUmJvfdxKWg6OCc0xU2qwzsytOJS08EYphk5Tpray4z1KbTKu7h0ERkmlEcbc3OIA+ajrqSbVSa+Mfqjv9tebnt7IXiR1vOKlp1CjqUNRhXDgVh655Xq/s9D1c3jj5fI2tjkv7eGZwiE0rWyPP4WucAXHoAar1pPj4c17kvy9/wC7vbW6TTPuGGGDboQIbar/AEshj9DBgDwFa8TiuD0fkcz/ACrvm2ZHi962WDbXiOS+gmlLQ8xw6n0qKgF1KVK7/V7vP7XHH7PVn3eduHEimQ4BdMc9jJLTQVUTWSqpCZIAScUBOCAgQBNzQcfZf0126TZdolnuKtuL4teYj+CNtdFep1VXz/8A5D2f2d5Ppy9f8b1Xnn5+70826xVrRcU9ddUjDNusVTgtJ6qeM39UZ+UZqv6jfPGlfouvnTGlGg1p4oI9hT0q0xvoknGuKUgjFJL1Htf3dfbNcdyF2phFHxOyI/isu/XKNe6sf1YBkH3EFI+JYcarn69B+Z1z+qry0C2iAcCdTnY1bw81P/XHmCP9ULtw9cTXO4O8k/6B5nyfqhcuhLY4Gsc4UBrWnVT/ANcf2PJ3G6TyvLnvLiTUkmq2nLO9Fi5ceKeFqxLglQgeTippja5KmY1xBUVRzJnDipptEdw4ZHBTYZzZ381NNtst3uLd3pcS3i0qLFa71pvrZhQu0u5FZ2G6bDrjLnupXIrO02ZzngkVqmSUcQUtMpzXJaAgOqptN0beRhj7Zqa8a5KKqNdha3EM7ZImmQ08KKLTkdm51saKH1UxUatzJrVl08CfAjIhGljkXVm63lLfqbwcnpYFodTJIC7TjiEtAmseOCVqjmO05qaZom5FSpTpDmkAmfqppubufuTadsp99dst9QqNZOXlVRe5ufdpx6+uvpHzv317/wBtuGiKwLrxxDTqo5kcYIq4YgVcemS5uvVe+t+zv/H9XjPl4bZ9uu903KW7ldFFHGwyzTXBJhiAH16c3OjbVwAC38ueZOWntlzXLZuW0W0NInSXb2vLWmVrWtdG0n+Y9oFXOdXIuK6OvX31f0cPPXM+vyK/9x7fuduGzvNnLGSJJYWFzZYz+B7AWjUMg7lnkp9f4/XF+PlXXu5rxk0hdUtrp4lejI4er+jJK6p8FcZ1muDRlOaqM+mcNTTiOGCAUmSwEBs2vbbjcLtlrblnekB0B7tIJArSp4qPZ7JzNrT1+u9XI9z7b9hts5G3e6Fsk7TWK3aasaRxcfxHpkvN/I/M8vjn6PQ9H4nj89fV658zxhVcEjvZpJSeKuQM0jiePirgI1deKsnmQF9u+fMaEA5oTI5gQloiCCrTGEk60xkpE1xE80iaWONM0irRG4pUjmOPNSD2kpA1v/1S0GtrQKbTMY3kptMwNKnTG1pKjTNa0pWmazAqbTNGCnTGCFNpmseQcFNpuztm8FgEMprHwdyWVhuzqBxacDxU6aCQVoVJjaWVSMzTEcsio02/bYrYOGs0Ix8VFqpHb7rGsAYKN4UUKJfLrqDlwSNkmLmsNBVyNImGJkgrN6jwCWnjNI2LuEZYo0sE1kICWmp2jgkZbgCloBiEtMLnvCWhmubsQxPleCWRtLnAZ0GKz9nfjLV887cfnL9RPd13vW6XRee3C2U9mJuIAaNIxzyC0/E9WTyv1rt9n8Z4x4ea6nJoXuNOZK9Ccxx9dV2di9yT2u33m3kVbdwPiY/iwvc1zqf4g3Ssfb6Jep015918PFye89rqHwIW+OTyCXlpqDgUywiR9U4VIJGapOM7zrd0GSaL8gpRNIX0ogFUTJYCA0WVzJbXUVxHg+Jwe3xBqo652YvjrLr7LaXDbu2jnjNWStDx/mFV4Hc8bj3ebs1JGCuJoOaUqmSYhuRqtOQyySBXIWldzoqwtcBoX2rwDWBMj2jh8EJNaCgmiMYIKtMYNUtS0RtywSoaomFLUtTGYJaR7GcVOk0RxVS02qOBToPbCAFNpmMjCm0GiMKdUc2LDJTaaxGVOmNsZU2qwegqdPBNjcVNoNZC4+KVpmtt38sFFpmNgcDkptDq7dPIwdp9dP4SVFpt9HZqdMbdQSpnsyUWm1QOxxUVUdWCfW3ScwoqoIyBpISMDpOIxSBEs2lp058kjc+TVWpQSR69QA44ItMyRj2Oo4YqdNGgUqUtCOcAcEjLM3MBTTZL2KO4t5YSSwSNLC5uY1ClQo6mzFc3Lr81+/fa257Rud0yZh7UlXRTgeh4JzacvEcF1fjdzJK7O/5828vDyRTNHprQcc13yxwdcdRTDJCA5xxJqP3Ivynm2fVpLmSCoz4qfoqzQOwH7005hD3iuGKqItZpy4t5DkqjPqliqEoc0AD8kyLTJYQFtzSOPr3sybX7astQqWhzK9A8gLwfzJ//AEr2/wAb54jr3ejtmgAK5+Pq6McWcOLl1cpsZSxxOKvSxXa6o8h4uI0L7N8+axoonqacxtUFT2MQTRG1GprXGAlpNUbccktJqjj/ALEtS1RxjDHxU6TVHGOSVptEbMQFNoamNFKqLTNayqm08NbDglp4e2AVCi1WHCEUU6eDbbPOQU3o5DY7F9cVF6VjQzb1F6PDmWDBmFN6PGhlmzKim9DDm2sYFKKb0eGC2ZyU+R4Y21apvR4exgAoClp4jowc6qdGCjq3ilptDJmjPDqp0N1pctac8CpqobLIHVoUjJbLprqSoR1yKEBuBzSNmkfXggJCXGQNAxqlabXPDLTmo02Quc0Yo0FOeSlpllyVplvcQotNjvrazvIezdwMniJB0SNDhUZGhUWq5tn0eI91exPar4Xvh29kM0okkfLHqqNDagNZq0DW8gZKevyOubPl0erq36vhG72PYuBERQV0jwGC9n0+zymuf3evKyvio5zmkjSaU4LWVhaTIDQEmtU4i0sjkmkmYjTRVE0AAoEEAY1PVMgSZIKlpkgQBtGKSo+v+042xe3bCowMerzcSV4H5V32dPc/Hn8I6V40OYXsxWHF+W7kS6q5LphUqjycAqIOhyNPHDa1faPmzmtwTScwI0j2FBHMS0mllOCWk1RE0w+SRVpj1YcilqWyInBTaG2JwpTqotPGmFuOKVptkbG4KLTxpjjFMlFp4c1lErVYbFEXHFRaeNscDMFFqsamNjAyUWmMBvJRTMakDGtKk0oQp0xDUeiWgxlQpM0VPFIxNJUmYKcVIW52FAEjL44pGZGCDgkGtsnApKWTUYJaAhjieSVp4MQF3JTp40QRNjdqLhVTaD3OjOZSNjuGQ4kO8ktNhkc0HoptPCXSBTaeEvkU2mzySKbTked96Oljs7QhxaJBJI4A0BawVAPmFh189On0R8L90UO5wsAyFSV7P4n+tP8AK+rgk1a//Gf3LseZWeU+keKqIoCUyZ7g4DxTiaFxTIDcgggyZFMqUgkQDGVJQqPtWwhrtksNAoOxHh/lC+b9/wDvf8voPV/rP8Gz1DTTBTy0YWw6yQTitfLDxT4XtaRWqJ0bPpd81epxwGhfba+YOaKoI1gQRzRxSI5gr5IJpjbklpNUbT5JaTXEFNpNsTQptNsibzUWnjWxtaKdDXCw1UWnG2JmCm1eHMYQptPDmNUWnjQ0UUWmawKdM9jVNpiwCnQJriUrTOa2oqVNproEtNaWhNZS0GNKmmME8EqYwUjTUApMxj2AdVJo6QJGDvkJGn3ZHFTaFi+6qdNDejOuKm0wu3A0oClps0l448VNpkOuzzS0AN1XiptPAOuRzU2nIX3dbg0ZuNB5qLVSOL79kZJYNkaRoga+FlONXhv8Vhz1vTq9Myvgu/S6t66NFP2r3/xp/Bn+Tf5OR+F/PUf2LqefWaXh4qozoTmmTPN9TfHFOJoZAmVCMgggyDAplSUyWkBNNDVCo+27M10O02UTvwQRj/hC+a913u3930Prmcz/AAbLR3hzUxqToaD6VWgNw9jWV48Ucym5/wBwznxW3inXAaa0ovtdfLmtqjSOZ/sEaR7RXFGk0RsrwS0m6GIEZKbUtbIB/FLQe2Ijqp08aoYzhgotPG6JhKm0NsUeCi1UjXExTaeNsQACi1WHAKLTE35KbQa0KbTPYFNoNa5SoWBKVA26QkYi9Sa2lICBS00qFOgbUtPBA0S08QyFTp4HuJWmJstFOmLuVStMqSRTaCjIVNplPkPNTacLM7hxU2nid6qnTxG1cxx5JabK95U2nhLpCFNqsLMzlOnh1lMG3DZJPoiBe6n90VUddfCpHn/d95NJ7ct5JmhkkshNOTdTiB8FHpn83V6p/J8P3R+reZDWo4+NF9H6Z/COX3/7MH/udHfuWzjrPLw8VUZ0txqmlnkNZAOCoqqQ4ITQjJBAecHJlS0yRIzLdodMxpyc4A+Zolforn6vtEc4DAwYBooB0GC+cvL6TAOkccinIYe64Iw2S6mJaalacclXP1HnxW7JgYvrNfNnsFUamnxsNUaTTEw1FErSa4Wc0rSdGBmXyU2hrjjqotPGqKHI0U2hrii8ip08a44cAptORpjjootVh7RRTaZ8ZCm0z2OH8FGgxpCWmawqbTOap08EKqdGDaClpjFUtNeKm01gkJaE1paa9anTwQlolaaG4Cm0wGYJaAmUVU6Y2y1Cm00c85gpaZZlrmptPC3TAKbTwoyAqbTwDnhTpg1qabXZvY+sbjnkjRhU9s5rjpFQoqmRzHHACpU6ZsVnTNwqeCRkbo0Wdq5xdQzVjFPCp/Ys+/ov1za8j7sudWwWTQ6o7rx/uj+1V+PP511eufNfHLuv9XmNeJw8l9Fx/pHD7v8Aash+p/8Ai/ctXLWe4NAD1VRn0UTQKiIZi8ngmlJDgiJoOCCC/wDEmKUmlEBp28A31uDkZWA/7wUd/wCtaer/AGn+X1QXDRXxXheL6PU+64FHgegddDin4DWO5uAWnHBa88s+umH7gfNbeLHyCwL6TXz7VFknpNUaWk1RAUStJpi4VStDoW4Gai0N8DTUKLTbYozmQptPGpkam1WNUbBRRaeGgKdMYStBreim0zWBTaZzQp0YcxTpmtKnTMaQp0xgpaaa0tCjIptNNaWmp0mCVowPcU6eB7qnTCZcUtMPdU6aw9LRi+4eam1WC+4o2im08KdKp08KdMFNp4WZwlaeAdOFNp4A3KnVYZa3LRKCTSinRjZJfGUOa00HNK9DGI3Wl1AfEqarAO3HTliUtORwfc+8kO27X9HfdqHQt0/9Svjny3/Db1z6vO+47ho2KyaMNM9w0/5dIWn4/P8AO/4jefFr5hI4Hcp3dSve5/1jzvZ/tWYnGTxH7FbmrNcn0eaqM+iZDRnkqTQRijfFMgSHGiImq4BBBfkUxSk0ogG2rtNzE7k9p+anr6L4vzH0YyjSTVeTj39JN0Qak4qvBPmgvGmtSjwOexnludRoKK5yjrtm7o58VeMvJqjK97XiVpjJRpNMZCNJqjOFAlaGqFTodK2BwU2iOrbRjAlZ2qkbmUwUWqPaQp0zmuU2gYS0zGqLQY1TaZjXKbVHMcp0Gh+CVpja+qnTNa+iVoFrU6aalNoAXhLTxRelphMim08CZUtPAmQKbTLMoU6eK74GSnTxffStPFGfDNTachbrjFTaYHXGGaWnhT7hTp4S64PNLVYW64SOQJnpxUnge/1xSPE+6czEO8QkeFvvM8UsPGaS96p+KpHD96SFlptrvxOEkv8AxAD/AJVv+LN6rTn6ORv913NlsHDEPfcSCvVzR+5a+jnO7/wvq/V8/a4G7ndhWpr8V7H2jzur80kn1SeX7FbCs1yf5fmqjPoqanbHVOJqqUCCJf8AWfBUipwSCn5HwTFKTSiAKI0kYeRH7UqfP1e0NySM1weD1/Mp8tOKqcpvRZmPNVifILpuFUeJXovuHn18lWFrrR4L1nltMeKNJrjbVGk1xt5KdNrhACm0OjbECii046kLslFptDX0GdFNqjWydVGg+N/wStM9rqqLTMacFNp4MOAU6YmvU2mYJFNpjbIptPDGypaMMbKlp4ISKdGL7inTwJkS08A6RTaeFuk6qdPCzMp08AZ0rTkLdN1U2qwHfS08Qz9VOjENxgi08KdP1Unhbp6DNJWFunSPCjOkYDP1QeAdOlhlmfNGKwl9z1Rh4xy7rbteGOla1xrhXlmrnrpgbdxy0LHhwORBqi84qMvvuXT/AEy3OD4rVuodXOLv3q/w/nyv7q+zgb25w2jaxkBFKfjK7+C6fT/v1/x/6F3fh46Cjppncq1+K9O/SODfmkF3rk8lTGs9z9A8VUZ9FyUIb4hNNCgipPrKqIqBAU/I+CBSU0rQECA9NFNqja7mAfkuWx6HPWxbnow9Lc9PE6Avx6J4Wq1lA13owvR15zVHQURpNcbxhRGhqjdhgptNqhKm026B2VFFojoRSYKLVNDZFGqw9jgVFp40RvU2nhzZKKbTwXc6qdPBtfU5qdPB9xK0IJaKbTF9wwDFwFOqm08Y3e4NraKi5jcA4MdRwwJ5qbVSFR+89hdci3F20yF2n+7XxySu/oMdptwCKg1ByU2ni+91S8hgTNjmptPAmYKdPC3TdVOnhLphzSVhXe6pGAzdVJgMyDxTp+ZSMP3HVI8C6cc0jwl83VBkuuOqMPAGcVRh4DvoxWAdcdUYchb7gc0YeONvW8i1geW4SU9NQS0+YW/q9PlU99+MfPNz3k3Gou1aicauJNelV63r9Xi832e7Xf8AY999xuVtZCRrDM9jHOfm6rhg3yXL+bxnN6dX4vs+z1X6kSMfvbXs+gwxloHAUwC8/wD8f/p/y7rPh5/eZw7aNsZxayX4GQrr9PP8+v8Aj/0Lv6PKRVBm6Fejfs4L92Wv8145hX9mH3Juj6B4pxHYXYtCaaCqElPzTTVJhH5HwQKSmlEBaA7lvJ/Jj/wj9iws+XXzfgZkqlitA56eFoC9PE6HWnha9G2RdmuM9r+vgloaI5OqNDXHIOCnQ2QyqLTbIpApptsUuCztXGqN4Ki02hkg5qbVHtlU2gxshzqptUISKdMxs2CnQ524e4YLK7gtnsc505oHD6QEvk2PfPc9vZCjLhmrQ+rQau1U9OSJzb9Bbj5m/fNzkY6N1zI6MvLw0k5uzXV/Xz+jPzrLJNI1rjroT8E8LSILmRsmoGhGNVV5KV7vav1Dv7exit+yLiZuBfK+hpwXH16Pn6/DedvcbRv7NwsWXDmGFxoHMd+boeIXL1MuNJHNuvfO3Q7gLYPD2V0vkH4SET1dWaex0J/cFhHbNuO6HRv+ihFXHkAs5zarA2u92t5GZIHgtBI64cUWWDFsv4ZdXbeH6cDQ1SxWJ9wkC3XIHHBJUgDcjmkeBNz1Sw8D9xVCsA64xzRh4B1yEYeFOuMU8PCnXGdCnhgdcHHFHiZbrjqn4mVJP1TnIcje5JZLcsYHEUq4ggDzqt/TMrP27nw8Bctb3HZnHCuPivV5eV1Pl3/YMUJ3eW5dgLKCSRppQ63jts/56rj/APIdXwk/+6//AFdn4HG9/wCHpved0Jt00tNRFGxgPPSMFw/hc5w9L2uRuuNlYHICJ1f/AMjl0+n/AG6/z/7M+/8AWPOx/TM48XYLtv2cV+7FX/uCOYP7Fp9nP9y7r6B4pxHYCfQPBNJeKZFvOJTRUQFPOB8ECkppRAWCgOpbP/kM8FnY6Ob8Gdw5JYehdIU8GhL0J1WpAeha8ro1ynMejQ0RvS0NMb/JLQ1xSY9VNpxrilNQotVG6KVZ2qa4pFFqo0NlFFGqMZMOam08NbL1ootPBd8UzStPHP3De/tJI2ltYnE96UmgYAiTTeE9we7Jb0m3hP8A27C7Q92LyOdeC6PX6s+ay67ecMkklQXEAZuK2+iELnBxIOA4lAUXF4BPpYgCc+CNuFa8+KXzT+E+4mbHra2jR+J1KlHjD+Rxb3dtjdH3CG5tbwB8lN9UOeyltv5akuILzxKq8CdNEm73DmNjMnpbXSDjicz0UT1xV9lo4PcW4Wzg+GbS7TpJaBiOqV9PN+pz22NFj7q3C0aezIRSuBxBJU9/j81fPtx0f/N90Ja4yA6hUsAAAWX/AFeV/wBrPee8Nwnt+3JIBQ19IoTTJVz+NzKL7vgp/um8dcMnbK8aAAGk8s8Mk5+PMwv7rutdt73vu60TEGPVi6mNFHX4kz4Vz+R+rqwe8RM46LZ76H8OOCw6/Gz610c+zy+krc33BZPBJk0aTQ6/Ssr6q1kYp/dFjX+VLq0u9ZAJFPgtJ+P194XlDYt/sZj6ZRU88KKb6bFzDH7nbj6pGhvOoopnFOzCzudsRUStpzqE/wCukU3dLWR2hkzHO5BwJVX12fYp1FuuTzSnJ6y3MncYWEkB2Bpy5LTmYnr5eZ3jbYomt+3B1PJw5Ls9Xst+rh93qk+ibFux2oTRSxml0WB0vEaK4EeLq+SX5Hp/syz7NPxPdPXfn7vSe5SDukhD2yBwaQ9v0kFoxFOBXD+L/o9L3fVmv5Q7aLNn4g6Rp8AQf+paeufzv/COr/FxmWo+wuLhzv8A1dDW+VSSuq9/yk/Zz3j+NrjuNLhvjT5Lo+zhv1Dc10IiOy6+gJp+wEyLdmmiogI/JApCaEQFoNrjuGsjaCpsaTo7uila5pYrULuSBoS8ILVawgteia5bMTWPxS0Hsf5JA9kiQaI5DgpobIpVFVG2GagUVcaGTrOqObcKTNZPjmoqjRcKTZ7/AHI21rJOBrLBWmSOZotfM903O6nlcZJHOLzqIrh4Ls44kY9dOeHnP9q0QJshHWhySsMxgLuGs/8ACEqYZnv80QVmfI6vXiqJTpnOAqTUc0YNAXJhYeRxxSNWriUwIEcUgZGyZwoxhcDyBSti5zb9GuHatwewuLNDAaFz8D5DP4LO+3mN+fxu79jotsgMga+4qTybT/mKm+259GnP4835pj7Pb4LjtlxmbT6tQAJ/y8lM76s36L/p45ufUuSe3adMcMbSOYLifiSqnN+9K+P2kAzct0nAijfogbhpaKAeARfXxPm/Uc+32dfE+hgc5sIBBLxk7+xLPlp8yMlw+7edTjiBgAaLTmSMe71Q91zQAWuL+I4D4J4XlRxzznJxZXjTH5pXmK56pmqOOMF8jjTgcEstqviT5rN3WOka+M0eDVrm4EU8FWMtlvw6bN+lZHoNXPOZqFjfRLXTPyMmHRb9IXUlhBbxIwKi+j9Kvn3z7wz+s2Ejvpc35hL+nqH/AGeul3Um13EfbMxYSQcv4hPnzl+hdcevqZrTc3dtIYe08FjI2R1JBNWNAUccWbv6teup8ZWh7WfYQyPNWte+jeZo1RL/ACsaSTxIvNEXt+OMYGad8h54ADNVx8+239In3cyeufu8pPUSgj8y748nv6rujgAnE9lD/TATR9gIIt2aaKiAjzggUlNCICIAqoNZcSP2IC2yOApXBA1C8lIaDV1TGvVh3VNJrXJA1r0aDWPS0NDJeqmm0RzKacaop1FVGlk4wUVRjZxVRVQ5s6mqE+7axhc40aBUlTgeS9w+4XzPdbxEGCg9Q5ro9fr+6OunmJZg52oZlbyM6Bpc5wzKZNDNLCDLywA5qaqQqW5eagGjRkAnIRPccRiUwujjSmJPAJDBxWd1KKxQveMjpaSKpXqT61fPr6v0jTFst8/DSGu4Nccfgovu5jbn8XunO2N8btM0zG5VLfVSvgpnv36Rf/Us+tGdt26JvqnLj1o0/wC7WqX9nV+y/wDr8T60UZ2mNrWiIuk5uBP8AlfO/dXM9c+3yc3crpkXbigGjIa2gUH91TfVLdtaT29SZITcbldCrQ0gDi0AD5qufXEd+7pjDrqQjUTU8StfiMv5VZiui6sbdP8AexJS2H4dfZI7Gdubw1x+qgx+KL3D59N/UQjlB0NmfTjSgA8xVLZ+ipzf1MbbMIxLnc6uJCXkueuKfFBGHSNYKtwcGiv70S2i8SfOEsudYLWsBJyrwVXlnPZv0jQIWtwedL3DBvRT5Npx+pUkTMdR1CnDIHzTlR1xFNs4A2rWlx4uOSflSnpi5LOEAdkOaTmSQMePklO79zvon2ImtSHAB5PHPiqnTPv0obMYVDqBHkP6RsaxhwDh+9Kr55kNMjaHAE1wH70saWx6HahDeWcQkq0MdSQDOo4jxC4fdbz1cd/pk75aPcrduFm5sEjv5bm9qJ4oQDWoNMDgp/FvXl8n+ZZ4/wCHiZWO1DlWpK9SPF7gbk1APROM+y2/QEJAQmks5ppRBKk+lApSaEQEQD225cwODgK40NEtaz17F/bGtA8V4BGj+sJtpqkaUaX9fQXW8zcx8wjSvroND+RTLxr0zXoSY1/VAMD1INa/gjQY2QqTOZLySB7Jsc1NUcLjFRitMbcUU2HKOS+bDE6Q1NMaJeKteW3nfJrt5bHIWx4DQ0/FbccYz661xC91cytULDi+jaVd80G2wW8sQDnMOp3TJRepWk4ozttzO6oowcC8hufFT/ZIuejqrj2y3jdW5uGljTRzYjqPxySvst+kXz+PJ/tf/g0tbs7XahbuLT/pNxJ8XF1Qo/n+rac+ufYY3i2i9DIoxwyHDwyS/pt+6v7+Z8SQQ3Rs51H+WXZGNoH+WtUf1Yqe7S5py1x/mhp41xJ8QnOf2LrrPuW+4vHR9tjqNOdGnw40TnPO6V66zIW1rA0NcauzqR81VqZIPUwtBL3FwxAazD4qV/8AP/yXHE6YlxaWxNxq40KLcPnm9f4MPZABe+jXZHA0p0U/Krn3BrD2ijDj+LE1TzC3fsdbw3szNMULwXfjIIb51U9dcz61pxz1Z8RrGw7lI0a36BTEZ/wWV/J4jX/rd361oj9ru0CszscyAKfDFRfy/wBmk/D+PqUfbk4dp7ocODiKfsVf9qfoX/Vv6mxe2iSe9K0Y4NYKYdc1N/K/SHz+J+tGNghifpZjXMt+rHqp/wCzbGk/G5n0BJsbK1o+nzVT8gX8eESbDC8aWyvYD8VU/Is+zPr8SX7nQe3oGMDXyPeTzNAp6/JtVx+JJPrVv9uQuFAS0Z1qlPyqq/ic0t3tZ+bZnAcKgFVPzJ+iL+F+7PJ7bumv1B4dyOP8Vc/K5Rfwru6S/Y7sZnLiFc/I5K/i9FnabpjquaSFX93NR/1upWra557a67b2OEUvpJOQI+krP3czrnfvGvpt56yz4rrbjbMnY7UMKA+YwXL6urHT7eJ1Hk7uINLhwBXpcXXk+7nGGXFjR4haxydBFNNEJLdxTRSyCmlDkgBk+lBUpNKIJKoMzTgMR4JKxHE8MUxVsnmZ9LiErDndg/un56jXNGK/sqvvJvzHOvBHiP7a7YcmxMa5IDa9IGh6RjEnwSBrZCFJmCUDilqpzTo3l7S5pBDczUYBRepGk9XVMjfqpRwDTxJopvUiufT1QX1o6WFuq7jhhP8AqEGpA8FE9vz8Rtfxfj5rgybfZNe7TMXt/CSA0+JGK3nd/RlfTz+pb/6dEBhrfXM1y5qv5Us4hjN1ggZoiY3Opc1tK165qb67fque2c/SBk3Fz6AuJd+WmPmU5xIL7dC90hxcXAHKpPwTHyFkbHuDZRUjFoqcBzqi39BOd+p7rOV7BiRCM21oSfPFR5xp/XbP2C20A+m0c41wLhRO9fuJ6/8A8TNE5oBC1nWuVOdEtn6ry/osOtoxV/8AMlJ9TY/lil805eZ9fmtccN1LCCyzc0Eg6jUVp04rO9SX6tuebZ/q6Nvse4TBtLfQM6vpQLDr8jmfdvPTb9nQZ7LvCKyTNoeQAosL+dz9oqfj/rT2ezIA09+4Bxyxoov51+0XPx59z4/bvt+3GNHHi4kBRfyfb0vn0cT7LezZYQezECeg/elL7L9auSQDdwZU9mP01LcGjh/iKq+r9afl+i/v7g4fbgD8xxw8kv65+p7Whk8rm4sDRwoAFF5iibh2kanaiMqNBJx6BVzNFDEyN1DoIHXBFtOGP7YyA8EoZbxGRU4FOaMK1EGgphzVBeogVwryCAB0mqmQTkBfdc3DLkqwtKkLjXS4ivGqqESGy8Xmo41VbC+QvqMdXlVOAh1zI0Uz5giqucRN7bo5hcWL30o5rXB3iMQsrz49Ye7HltxA1u6lej6vo8v3xzHDBbuGkOTZUJZ6AeKCz4UQUFijkmkEn0ohUpNKkBYzQDPBCkIdzH7EhiUAzIPggIUwqiCdyqSRB1EgLVxOSR4j7mNhAccTwGKDxbrshmtrf97BSucFO3K8neXUDGjgwCNvkAlOJF+VpJleXkuPUHEqsAm3HaDiMNQpVvp+NErzpzrFG/upPSBqbkKAnJHhIP7LROjneykjwBnQo2H42/Uv7cVOqQnwB/cjS8P3Pjs4SKFriPAjzU3ppPXBCzia4aY9VchhijyP+qfocKRNOAGvBzWiqn6tJ8AEb3EASOb0DRlzJxRpeP7nxQXgcDC1z+4aCR40gnxIyU3rn7tOeevs6NttO4yPEj3MZwdoZUEeLsFh17uZ8Ojj0927XSi9syuZV0rtNc3uA/YFhfyp+jafj/udHs+3QNcHyCR4wADSfm4qL7+q059PMb7HbrcuB7MYa3HHD40osfZ7b+q5xJ9nQlEEOIkjDqY6BXPqsJtXKS3cLUklzXu04a9LiD4Kr66egfuB1ARsdTg52H7E56v1GlTXU0mBdpHSirnmQ2XsNe4mhca1rxWnlgw6Jlu+QxhwL2ZtriPGim2yaPg2KybEyjG41JPDFTe9OTDAKVqwV6/wUqBPeshj1y0YPJPn17fgr1I4t/7o26EhtTM78rf9gF1+v8Tq/s5/Z+Xxz+7NH7pklIbDZOIJ+pzv4Aq7+JJ9ek8/lW/TlvdezljHOYI9XD/aix/rjp8qjpnHOpHQVROT0cUmttKkEZVB/sU9TFRHQupnUonQwGIwzVFhU2FTy54KuSpMV3bPdobK0vOTQaqrxZ9kTvm/GuduFtuz5y6KYtg4NZgQt/X1xJ8z5c/u9fst2X4XZyXWl0cmpwbk85lHcn1i/VevpTiHGpoRTGtVLXGzaru30SwyPaJH4MjJxNBjgsfdxfiw+O59HC3iIRyuHXBdvouxwfk8447xgV0vNsZZCqY9NtnuVqLKaC6txO4tAt3/AElhrnqAqfBZd+u+Usufq39Xv58Lz1N/RhOa1c4SMEyC8HSShNhCaEQDbZncma05cfJK1fr53rHS+3ZUUABHFZ67v6oE2rQcTqT8k30wt1mwfhqOifkzvpkKfaDMVCeovqD9oOaNT/W6Uk8MT6V7lMy3BtfHil8s/GQp96C00o00oKYHxRlPYS6R7mjU4+VcUwprXNBJ9I4koGBL2aqj1fFAtF35KZU5VRg8qZF3ZDpqB1JSvwrna0ts5XUDzqHBo5qPKNZ67TxGIwDG06mmobTiOdcKKd1p459Gi2t7u4aaxggU1ANr8wo66nLTjjrpvh2Tc8O3a0acu5Ro+Cx69/H3rbn0dfaN8Htfc53etzcRR2mvHlksevy+J9G09HV+q5va8EDiJJHHThQZeFUT8u36H/1eTLT29YUBjaNQ4ZnKn4qqe/yevurn8bmfZ0o9ltIGBz2FxP1Nc4VNPBYX39Vtz65F6rNuLYmRln4QCUs6/VckSW+hlADWlp/KMPNKeuw9BO6sbGMfQniaup81XM+RVMa9hHclGk5OaA3ywxRbv0gmjdextODvTXM5qfCnohukFcq05o/qo8otrzL6hSnBKzFQbyGtq7FKGKOFrvV9VOOKV6M2FkIfRpDq8FPVoNbGQScq8VFoFoeXAk4DjxS0Buo3OZSNwYRm+lSnxf1DmXOzw3jGtvaTBowdi018QV0ce+8/6/CO/VOvr8s8Wx7RCKNt2HhjU5eKu/kd37lz6OJ9IfFDaW4PaiazqMVF666+ta88yfQqSO3eNXDlTBVLYeShEbBXS0g8CE9PxLdK9oOHHBPNLQsne41OBTvMEq3vr480SGU9+Ba6uOdVUhM8VtBCQGsaAPpB4K73ajniRqje1xwIIGazsaSo8MoaAAoh2MkzSDUZrXlHUYo7Jov47pw09upwyOBC1vf8cc19M853+jLu8wlmLhll8Fr6ecjm/J62uQ/iumPP6YZHZq45eqW0mqaIaCpWjTzyQIONocQDxNEVXMYjgSOWCpgiAOCZ0Uge3McOiVmq47vN2OyyVj2B4OBxAWVj0+epZqOLKVJ61QdwOocPJPEbAlrXZoTZKHtjmnpeBL+1X11pwpl81Ti+B/8Aa+nOtOGfkj5P4D6KenXq60QSN+2r/M1V4VyR8iYaztaTp0cPpz+alcw2H7Svr/tSuq5ww6sOzpzwp+6qX+Vf4Mh+71Gv+elK/JK4vnyXPq1P0Vp0RMPrXudl+3/o1n9v/paR3tH1d+nr11xr4YUyXke/fO79f/Z6f4+eHw9E3R2W/Tpp1r51XDfq2+QXGrteiva/H29WquGWnH4J8/U3N3Hv9/8Am6e3T8PLhmt/XmfA+XKd9x3HUrnhWlPkuiZifnS5fvdY1ZU/26pzxF8gNrV31aacaVVXCmmWvc1HOnGlP3pd4rnWqXt6fxV4ZUWcaEnt8NVONaKkmD7HQK14cq0U3VTDP/1uk/Xkc6Uol/I/gcf2/Z/l6tNBp00p5Kb9flU+h7fttP8ANrSuOVVIaoft6enVppxpTzWdP5RvbqdOXWlPklT+RnVQVU/AGdOk66U4c6ImEU/6vT9HXNP4Mg6KmuqnBafBxln+kf8At1/DSqvkIP6fQa9dcKV+WSPkwGmo0rTgn8H8ibo0nV51Sp/JLuzoGVOCoizox0+dEx8s1z3MM6cFpxiLrHJ97Q0/y8/mtZ4ovkQfu+56q+av+OI/lrXaa6HXlXzWfeNedPOnGupQ0+QO0acPNOBkv9fYPa1a+NKf7UWvrzfll7d8bjh3X1DVWvHJdnLyvayS6NPGvDJXHL258mmpzWkcnQBorxqmk0duorqp5JKGPtv/AJPkgQbO1X0aq1FK0qpq+WGXTrdT8x/arjHr6hFEEv8Al9UB1LPsfbN16tWNKUy4LPr6u7054/Jru1h9XySaUR7VMNVadElUI7FfxV8k0TNV/K/vfLNBP//Z" data-filename="romantic-manali_1466979022.jpg" style="width: 450px;"><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"><br></span></p><p><strong style="margin: 0px; padding: 0px; font-family: "Open Sans", Arial, sans-serif; text-align: justify;">Lorem Ipsum</strong><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: "Open Sans", Arial, sans-serif; text-align: justify;"><br></span>		        	</p>', '2018-02-09 23:05:25', '2018-02-12 19:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `to_user` int(11) NOT NULL,
  `seen` bit(1) NOT NULL DEFAULT b'0',
  `accom_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `price_booking_id` int(11) DEFAULT NULL,
  `booking_payment_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `request_type_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 for approve,0 for delete,3 for contact message,4 for contact message reply,5 for New Booking Request,6 for Booking Approval Request,7 for Booking Payment Request,8 For Booking Request Rejected,9= New Price Quoted,10=Success Wire Transfer,11= new Product Bought,12=Password Reset Request'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `message`, `to_user`, `seen`, `accom_id`, `ticket_id`, `price_booking_id`, `booking_payment_id`, `created_at`, `updated_at`, `request_type_url`) VALUES
(2, 'Your accomodation has been deleted', 2, b'1', 103, NULL, NULL, NULL, '2018-03-27 20:47:26', '2018-03-27 20:47:44', '/accomodations/my-accomodations'),
(3, 'Your accomodation has been deleted', 2, b'1', 105, NULL, NULL, NULL, '2018-03-27 22:44:45', '2018-03-27 23:12:44', '/accomodations/my-accomodations'),
(4, 'Your accomodation has been deleted', 2, b'1', 106, NULL, NULL, NULL, '2018-03-27 23:13:48', '2018-03-27 23:14:04', '/accomodations/my-accomodations'),
(5, 'Your accomodation has been deleted', 2, b'1', 104, NULL, NULL, NULL, '2018-03-27 23:13:53', '2018-03-27 23:13:59', '/accomodations/my-accomodations'),
(6, 'Your accomodation has been deleted', 2, b'1', 102, NULL, NULL, NULL, '2018-03-27 23:14:48', '2018-03-27 23:16:02', '/accomodations/my-accomodations'),
(7, 'Your accomodation has been deleted', 2, b'1', 107, NULL, NULL, NULL, '2018-03-28 17:15:28', '2018-03-28 17:15:45', '/accomodations/my-accomodations'),
(8, 'Your accomodation has been deleted', 2, b'1', 109, NULL, NULL, NULL, '2018-03-29 07:20:21', '2018-03-29 07:20:28', '/accomodations/my-accomodations'),
(9, 'Your accomodation has been deleted', 2, b'1', 110, NULL, NULL, NULL, '2018-03-29 07:31:04', '2018-03-29 07:31:12', '/accomodations/my-accomodations'),
(10, 'Your accomodation has been deleted', 2, b'1', 111, NULL, NULL, NULL, '2018-03-29 07:38:22', '2018-03-29 07:38:44', '/accomodations/my-accomodations'),
(11, 'Your accomodation has been approved', 2, b'1', 112, NULL, NULL, NULL, '2018-03-29 07:40:55', '2018-03-29 07:41:12', '/accomodations/my-accomodations'),
(12, 'Your accomodation has been deleted', 2, b'1', 112, NULL, NULL, NULL, '2018-03-29 07:41:46', '2018-03-29 07:41:52', '/accomodations/my-accomodations'),
(13, 'Your accomodation has been deleted', 2, b'1', 113, NULL, NULL, NULL, '2018-03-29 07:50:49', '2018-03-29 07:50:56', '/accomodations/my-accomodations'),
(14, 'Your accomodation has been deleted', 2, b'1', 118, NULL, NULL, NULL, '2018-04-04 17:30:40', '2018-04-04 17:30:49', '/accomodations/my-accomodations'),
(15, 'Your accomodation has been deleted', 2, b'1', 108, NULL, NULL, NULL, '2018-04-04 17:31:03', '2018-04-04 18:00:50', '/accomodations/my-accomodations'),
(16, 'Your accomodation has been deleted', 2, b'1', 115, NULL, NULL, NULL, '2018-04-04 17:31:07', '2018-04-04 18:00:49', '/accomodations/my-accomodations'),
(17, 'Your accomodation has been deleted', 2, b'1', 116, NULL, NULL, NULL, '2018-04-04 17:31:11', '2018-04-04 18:00:47', '/accomodations/my-accomodations'),
(18, 'Your accomodation has been deleted', 2, b'1', 114, NULL, NULL, NULL, '2018-04-04 17:31:14', '2018-04-04 17:31:33', '/accomodations/my-accomodations'),
(19, 'Your accomodation has been deleted', 2, b'1', 117, NULL, NULL, NULL, '2018-04-04 17:31:19', '2018-04-04 17:31:31', '/accomodations/my-accomodations'),
(20, 'Your accomodation has been deleted', 2, b'1', 119, NULL, NULL, NULL, '2018-04-04 18:46:40', '2018-04-04 18:46:52', '/accomodations/my-accomodations'),
(21, 'Your accomodation has been deleted', 2, b'1', 120, NULL, NULL, NULL, '2018-04-08 08:07:10', '2018-04-08 08:14:04', '/accomodations/my-accomodations'),
(22, 'Your accomodation has been deleted', 2, b'1', 124, NULL, NULL, NULL, '2018-04-08 08:07:18', '2018-04-08 08:14:05', '/accomodations/my-accomodations'),
(23, 'Your accomodation has been deleted', 2, b'1', 121, NULL, NULL, NULL, '2018-04-08 08:07:43', '2018-04-08 08:14:00', '/accomodations/my-accomodations'),
(24, 'Your accomodation has been deleted', 2, b'1', 122, NULL, NULL, NULL, '2018-04-08 08:07:50', '2018-04-08 08:14:02', '/accomodations/my-accomodations'),
(25, 'Your accomodation has been deleted', 2, b'1', 125, NULL, NULL, NULL, '2018-04-08 08:08:22', '2018-04-08 08:13:58', '/accomodations/my-accomodations'),
(26, 'Your accomodation has been deleted', 2, b'1', 123, NULL, NULL, NULL, '2018-04-08 08:08:27', '2018-04-08 08:13:56', '/accomodations/my-accomodations'),
(27, 'Your accomodation has been deleted', 2, b'1', 126, NULL, NULL, NULL, '2018-04-08 08:08:34', '2018-04-08 08:13:53', '/accomodations/my-accomodations'),
(28, 'Your accomodation has been approved', 24, b'0', 128, NULL, NULL, NULL, '2018-04-08 08:29:53', '2018-04-08 08:29:53', '/accomodations/my-accomodations'),
(29, 'Your bank transfer payment has been approved', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 16:11:51', '2018-04-08 16:23:18', '/subscription-licenses'),
(30, 'Your bank transfer payment has been approved', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 16:20:51', '2018-04-08 16:23:14', '/subscription-licenses'),
(31, 'Your bank transfer payment has been approved', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 16:21:22', '2018-04-08 16:22:25', '/subscription-licenses'),
(32, 'Your bank transfer payment has been approved', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 16:23:30', '2018-04-08 16:28:40', '/subscription-licenses'),
(33, 'Your bank transfer payment has been approved', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 17:47:58', '2018-04-08 17:51:09', '/subscription-licenses'),
(34, 'Your bank transfer payment has been approved for Invoice: LBV-LIC-2', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 18:09:30', '2018-04-08 18:10:09', '/subscription-licenses'),
(35, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-1', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 18:37:33', '2018-04-08 18:42:19', '/subscription-licenses'),
(36, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-2', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 18:41:18', '2018-04-08 18:42:21', '/subscription-licenses'),
(37, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-2', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 18:50:07', '2018-04-08 18:59:01', '/subscription-licenses'),
(38, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-1', 2, b'1', 132, NULL, NULL, NULL, '2018-04-08 18:56:52', '2018-04-08 18:58:58', '/subscription-licenses'),
(39, 'Your bank transfer payment has been approved for Invoice: LBV-LIC-3', 2, b'1', 133, NULL, NULL, NULL, '2018-04-08 19:08:14', '2018-04-08 19:10:01', '/subscription-licenses'),
(40, 'Your bank transfer payment has been approved for Invoice: LBV-LIC-4', 2, b'1', 134, NULL, NULL, NULL, '2018-04-08 19:13:37', '2018-04-08 19:18:43', '/subscription-licenses'),
(41, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-4', 2, b'1', 134, NULL, NULL, NULL, '2018-04-08 19:14:34', '2018-04-08 19:18:25', '/subscription-licenses'),
(42, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-5', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 21:09:36', '2018-04-08 21:27:17', '/subscription-licenses'),
(43, 'Your bank transfer payment has been approved for Invoice: LBV-LIC-5', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 21:15:38', '2018-04-08 21:27:20', '/subscription-licenses'),
(44, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-6', 2, b'1', 131, NULL, NULL, NULL, '2018-04-08 21:22:43', '2018-04-08 21:27:38', '/subscription-licenses'),
(45, 'Your bank transfer payment has been approved for Invoice: LBV-LIC-7', 2, b'1', 135, NULL, NULL, NULL, '2018-04-08 21:40:09', '2018-04-08 21:41:51', '/subscription-licenses'),
(46, 'Your bank transfer payment has been cancelled for Invoice: LBV-LIC-8', 2, b'1', 135, NULL, NULL, NULL, '2018-04-08 21:43:52', '2018-04-09 11:21:29', '/subscription-licenses');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodations`
--
ALTER TABLE `accomodations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `accom_description` (`description`(333)),
  ADD KEY `accom_name_2` (`name`);
ALTER TABLE `accomodations` ADD FULLTEXT KEY `search_engine` (`name`,`website`,`description`);
ALTER TABLE `accomodations` ADD FULLTEXT KEY `accom_name` (`name`);

--
-- Indexes for table `accomodation_addresses`
--
ALTER TABLE `accomodation_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addr_street` (`street`);

--
-- Indexes for table `accomodation_prices`
--
ALTER TABLE `accomodation_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accomodation_price_extra_charges`
--
ALTER TABLE `accomodation_price_extra_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accomodation_searchtags`
--
ALTER TABLE `accomodation_searchtags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accomodation_types`
--
ALTER TABLE `accomodation_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `acpr_type_unique` (`type_name`);

--
-- Indexes for table `admin_notifications`
--
ALTER TABLE `admin_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extra_tabs`
--
ALTER TABLE `extra_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_product_licenses`
--
ALTER TABLE `free_product_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_subscription_licenses`
--
ALTER TABLE `free_subscription_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pay_methods`
--
ALTER TABLE `pay_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_units`
--
ALTER TABLE `price_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privacies`
--
ALTER TABLE `privacies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_prices`
--
ALTER TABLE `room_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searching_groups`
--
ALTER TABLE `searching_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchtags`
--
ALTER TABLE `searchtags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchtag_searching_groups`
--
ALTER TABLE `searchtag_searching_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_instances`
--
ALTER TABLE `subscription_instances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_licenses`
--
ALTER TABLE `subscription_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_payments`
--
ALTER TABLE `subscription_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_helps`
--
ALTER TABLE `user_helps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodations`
--
ALTER TABLE `accomodations`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `accomodation_addresses`
--
ALTER TABLE `accomodation_addresses`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `accomodation_prices`
--
ALTER TABLE `accomodation_prices`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `accomodation_price_extra_charges`
--
ALTER TABLE `accomodation_price_extra_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `accomodation_searchtags`
--
ALTER TABLE `accomodation_searchtags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=430;
--
-- AUTO_INCREMENT for table `accomodation_types`
--
ALTER TABLE `accomodation_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `admin_notifications`
--
ALTER TABLE `admin_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `extra_tabs`
--
ALTER TABLE `extra_tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `free_product_licenses`
--
ALTER TABLE `free_product_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `free_subscription_licenses`
--
ALTER TABLE `free_subscription_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pay_methods`
--
ALTER TABLE `pay_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `price_units`
--
ALTER TABLE `price_units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `privacies`
--
ALTER TABLE `privacies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `room_prices`
--
ALTER TABLE `room_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `searching_groups`
--
ALTER TABLE `searching_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `searchtags`
--
ALTER TABLE `searchtags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `searchtag_searching_groups`
--
ALTER TABLE `searchtag_searching_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subscription_instances`
--
ALTER TABLE `subscription_instances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subscription_licenses`
--
ALTER TABLE `subscription_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subscription_payments`
--
ALTER TABLE `subscription_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1317;
--
-- AUTO_INCREMENT for table `user_helps`
--
ALTER TABLE `user_helps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
