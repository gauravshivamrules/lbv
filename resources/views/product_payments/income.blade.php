@extends('layouts.back')
@section('title',_('All Promotion Income'))
@section('content')
<style type="text/css">
 	.ui-datepicker-title {
    	color: black;
	} 
  .fc-time{
     display : none;
  }
  .tooltip-inner {
      white-space:pre-wrap;
  }
  div.dt-buttons {
      float: right !important;
  }
</style> 
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css"> 
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Promotion Income') }} </h2>  
		</div>
		
		<div class="row">
			<form action="" method="post">
			<div class="form-group col-md-2"></div>	
		 	<div class="form-group col-md-4">
		 		<input type="text" class="form-control" name="start_date" placeholder="{{__('Start Date')}}" id="startDate" required> 
		 	</div>
		 	<div class="form-group col-md-4">
		 		<input type="text" class="form-control" name="end_date" placeholder="{{__('End Date')}}" id="endDate" required> 
		 	</div>
		 	<div class="form-group col-md-2">
		 		<button type="button" class="btn btn-primary" id="getResults"> {{__('Get Results')}}  </button>
		 	</div>
		 	</form>
		</div>
		<ul class="nav nav-tabs"> 
			<li class="active"> 
				<a data-toggle="tab" href="#showActive"><span class="label label-success" id="active"></span> {{__('Completed')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showOutstanding"><span class="label label-danger" id="outstanding"></span> {{__('Outstanding')}} </a>  
			</li>
		</ul>  
		<div class="tab-content"> 
			<div id="showActive" class="tab-pane active"> 
				<div class="table-responsive">  
					<div class="row">
					 	<div class="col-md-12"> 
					 		<div class="info-box bg-green">
					 			<span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
					 			<div class="info-box-content">
					 				<span class="info-box-text">{{__('Total Paid')}}</span>
					 				
					 				<span class="info-box-number" id="sum">&euro; {{$SUM?$SUM:00}} </span> 
					 				<div class="progress">
					 					<div class="progress-bar" style="width: 100%"></div>
					 				</div>
					 			</div>
					 		</div>
					 	</div>
					</div>
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Created On'); ?></th>
								<!-- <th><?php echo __('Valid Upto'); ?></th> -->
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
			<div id="showOutstanding" class="tab-pane"> 
				<div class="table-responsive"> 
					<div class="row">
					 	<div class="col-md-12"> 
					 		<div class="info-box bg-green">
					 			<span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
					 			<div class="info-box-content">
					 				<span class="info-box-text">{{__('Total Outstanding')}}</span>
					 				<span class="info-box-number" id="pSum">&euro; {{$SUMPENDING?$SUMPENDING:00}} </span>
					 				<div class="progress">
					 					<div class="progress-bar" style="width: 100%"></div>
					 				</div>
					 			</div>
					 		</div>
					 	</div>
					</div> 
					<table class="table table-striped table-hover dt-responsive display nowrap" id="outstandingTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th> 
								<th><?php echo __('Created On'); ?></th>
								<!-- <th><?php echo __('Valid Upto'); ?></th> -->
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
	<script type="text/javascript" src="/DATATABLE_BUTTON/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/jszip.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/pdfmake.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/vfs_fonts.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/buttons.html5.min.js"></script> 
	<script>
		$(function() {
			$('#startDate').datepicker({
				changeMonth:true,
				changeYear:true,
				dateFormat:"dd-mm-yy",
				onSelect:function(date1) {
			    	$("#endDate").datepicker("option","minDate",$("#startDate").datepicker("getDate")); 
			    	$('#endDate').val($('#startDate').val());  
				}
			});
			$('#endDate').datepicker({
				changeMonth:true,
				changeYear:true,
				dateFormat:"dd-mm-yy" 
			});
			buildTable('activeTable','active');    
			buildTable('outstandingTable','outstanding');   

			$('#getResults').click(function() {
				if($('#startDate').val()) {
					$('#activeTable').DataTable().destroy();
					$('#outstandingTable').DataTable().destroy();
					getResults();
				} else {
					alert("<?php echo __('Please select start date') ?>");
				}
					

			});
		});  
		function buildTable (table,tCount,condition) {
			if(condition) {
				$('#'+table).DataTable({  
					responsive: true, 
				    processing: true,
				    serverSide: true, 
				    ajax: "/promotion-payments/payments-income-ajax?type="+tCount+"&start_date="+$('#startDate').val()+"&end_date="+$('#endDate').val(),
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },
				    dom: 'Blfrtip',
				    lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				    buttons: [
				        {
				            extend: 'copyHtml5',
				            exportOptions: { 
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'excelHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'pdfHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'csvHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        } 
				    ],      
				    columns: [
				        { data: 'product_name', name: 'product_name' }, 
				        { data: 'total_amount', name: 'total_amount' },
				        { data: 'created_at', name: 'created_at' }, 
				        // { data: 'vUpto', name: 'vUpto' },  
				        { data: 'pName', name: 'pName' }, 
				        { data: 'payment_complete', name: 'payment_complete'},    
				        { data: 'invoice', name: 'invoice' }, 
				        { data: 'uFName', name: 'uFName' }, 
				        { data: 'aName', name: 'aName'},    
				        { data: 'action', name: 'action'}    
				    ],
				});	
			} else {
				$('#'+table).DataTable({ 
					responsive: true, 
				    processing: true,
				    serverSide: true, 
				    ajax: "/promotion-payments/payments-income-ajax?type="+tCount,
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },
				    dom: 'Blfrtip',
				    lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				    buttons: [
				        {
				            extend: 'copyHtml5',
				            exportOptions: { 
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'excelHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'pdfHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        },
				        {
				            extend: 'csvHtml5',
				            exportOptions: {
				                columns: [ 0,1,2,3,4,5,6,7]
				            }
				        } 
				    ],      
				    columns: [
				        { data: 'product_name', name: 'product_name' },
				        { data: 'total_amount', name: 'total_amount' },
				        { data: 'created_at', name: 'created_at' }, 
				        // { data: 'vUpto', name: 'vUpto' },  
				        { data: 'pName', name: 'pName' }, 
				        { data: 'payment_complete', name: 'payment_complete'},    
				        { data: 'invoice', name: 'invoice' }, 
				        { data: 'uFName', name: 'uFName' }, 
				        { data: 'aName', name: 'aName'},    
				        { data: 'action', name: 'action'}    
				    ]
				});
			}
			
		} 
		function getResults () {
			$.ajax({
				url:'/promotion-payments/find-sum?start_date='+$('#startDate').val()+"&end_date="+$('#endDate').val(),
				dataType:'json',
				success:function(r) {
					console.log(r);
					if(r) {
						$('#sum').html("&euro;&nbsp;"+r.sum);
						$('#pSum').html("&euro;&nbsp;"+r.pSum);
					} 
				}
			});
		 	buildTable('activeTable','active',1);    
		 	buildTable('outstandingTable','outstanding',1);   
		} 
	</script>
@endsection