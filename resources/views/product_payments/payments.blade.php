@extends('layouts.back')
@section('title',_('Promotion Payments'))
@section('content')
	<style type="text/css">
		div.dt-buttons {
		    float: right !important;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css"> 
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Promotion Payments') }} </h2>  
		</div>
		<ul class="nav nav-tabs"> 
			<li class="active">
				<a data-toggle="tab" href="#showActive"><span class="label label-success" id="active"></span> {{__('Active')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showCancelled"><span class="label label-warning" id="cancelled"></span> {{__('Cancelled')}}</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showExpired"><span class="label label-primary" id="expired"></span> {{_('Expired')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showOutstanding"><span class="label label-info" id="outstanding"></span> {{__('Outstanding')}} </a> 
			</li>
		</ul> 
		<div class="tab-content"> 
			<div id="showActive" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showCancelled" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="cancelledTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			<div id="showExpired" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="expiredTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
			<div id="showOutstanding" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="outstandingTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
	<script type="text/javascript" src="/DATATABLE_BUTTON/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/jszip.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/pdfmake.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/vfs_fonts.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/buttons.html5.min.js"></script>
	<script>
		$(function() {
			buildTable('activeTable','active');  
			buildTable('cancelledTable','cancelled');  
			buildTable('expiredTable','expired');  
			buildTable('outstandingTable','outstanding');   
		}); 
		function buildTable (table,tCount) {
			$('#'+table).DataTable({ 
				responsive: true, 
			    processing: true,
			    serverSide: true,
			    ajax: "/promotion-payments/pro-payments-ajax?type="+tCount,
			    initComplete: function(settings, json) {
			    	$('#'+tCount).html(json.recordsTotal);
			    },
			    dom: 'Blfrtip',
			    lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
			    buttons: [
			        {
			            extend: 'copyHtml5',
			            exportOptions: { 
			                columns: [ 0, 1, 2,3,4,5,6,7,8]
			            }
			        },
			        {
			            extend: 'excelHtml5',
			            exportOptions: {
			                columns: [ 0, 1, 2,3,4,5,6,7,8]
			            }
			        },
			        {
			            extend: 'pdfHtml5',
			            exportOptions: {
			                columns: [ 0, 1, 2,3,4,5,6,7,8]
			            }
			        },
			        {
			            extend: 'csvHtml5',
			            exportOptions: {
			                columns: [ 0, 1, 2,3,4,5,6,7,8]
			            }
			        }
			    ],      
			    columns: [
			        { data: 'product_name', name: 'product_name' },
			        { data: 'total_amount', name: 'total_amount' },
			        { data: 'vFrom', name: 'vFrom' }, 
			        { data: 'vUpto', name: 'vUpto' },  
			        { data: 'pName', name: 'pName' }, 
			        { data: 'payment_complete', name: 'payment_complete'},    
			        { data: 'invoice', name: 'invoice' }, 
			        { data: 'uFName', name: 'uFName' }, 
			        { data: 'aName', name: 'aName'},    
			        { data: 'action', name: 'action'}    
			    ]
			});
		}  
	</script>
@endsection