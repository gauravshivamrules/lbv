@extends('layouts.back')
@section('title',_('Promotion History'))
@section('content') 
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('Accomodation Promotion History') }} </h2>  
		</div>

		@if($accoms->count()>0)
		<div class="form-group">  
			{!! Form::select('accom_id',$accoms,null,['id'=>'accom_id','class'=>'form-control','required'=>true] ) !!} 
		</div>
		@else
		<p> {{__('No accomodations found')}} </p>
		@endif 

		<ul class="nav nav-tabs"> 
			<li class="active">
				<a data-toggle="tab" href="#showActive"><span class="label label-success" id="active"></span> {{__('Active')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showExpired"><span class="label label-warning" id="expired"></span> {{__('Expired')}}</a>
			</li> 
		</ul> 


		<div class="tab-content"> 
			<div id="showActive" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr> 
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<!-- <th><?php echo __('User'); ?></th> -->
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
			<div id="showExpired" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="expiredTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th> 
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
		

			
	</div>
		<script>
			$(function() {
				buildTable('activeTable','active');  
				buildTable('expiredTable','expired');  
				$('#accom_id').change(function() {
					buildTable('activeTable','active');  
					buildTable('expiredTable','expired');   
				});
			}); 

			function buildTable (table,tCount) {
				$('#'+table).DataTable({ 
					responsive: true, 
				    processing: true,
				    serverSide: true,
				    bDestroy:true,
				    ajax: "/promotion-payments/history-ajax?type="+tCount+"&accom_id="+$('#accom_id').val(), 
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [
				        { data: 'product_name', name: 'product_name' },
				        { data: 'total_amount', name: 'total_amount' },
				        { data: 'vFrom', name: 'vFrom' }, 
				        { data: 'vUpto', name: 'vUpto' },  
				        { data: 'pName', name: 'pName' }, 
				        { data: 'payment_complete', name: 'payment_complete'},    
				        { data: 'invoice', name: 'invoice' }, 
				        { data: 'aName', name: 'aName'},    
				        { data: 'action', name: 'action'}    
				    ]
				});
			} 
			function confirmAction(url) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href=url;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection