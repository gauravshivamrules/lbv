@extends('layouts.back') 
@section('title',__('Privacy')) 
@section('content')
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-2"></div>
			<div class="col-md-8"> 
				<form class="form-horizontal" action="{{route('updatePrivacy')}}" method="post">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Privacy'); ?></legend>
						<div class="form-group">
							<div class="col-lg-10">
								<input type="hidden" name="id" value="{{$privacy->id}}"> 
								<textarea class="summernote" name="privacy_content" id="privacy_content">
										{{$privacy->content}}
								</textarea>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary"><?php echo __('Update'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {  
     	 $('.summernote').summernote({
     	 	 height: 250,   //set editable area's height
     	 });
     });
	</script>
@endsection