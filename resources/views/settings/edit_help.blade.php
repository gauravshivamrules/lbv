@extends('layouts.back') 
@section('title',__('Edit Help')) 
@section('content')
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-2"></div>
			<div class="col-md-8"> 
				<form class="form-horizontal" action="{{route('update_help')}}" method="post">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Edit Help'); ?></legend>
						<div class="form-group">
							<div class="col-lg-10">
								<input type="hidden" name="id" value="{{$h->id}}"> 
								<input type="text" name="title" class="form-control" value="{{$h->title}}" required>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10">
								<textarea class="summernote" name="content" id="content">
										{{$h->content}}
								</textarea>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary"><?php echo __('Update'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {  
     	 $('.summernote').summernote({
     	 	 height: 250,   //set editable area's height
     	 });
     });
	</script>
@endsection