@extends('layouts.back')
@section('title',_('User Help'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('User Help'); ?> </h2> 
		</div>

		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#helpsAddModal" style="float: right;"><?php echo __('Add New')  ?></button>
		<table class="table" id="helpsTable">
			<thead>
				<tr>
					<th><?php echo __('#'); ?></th>     
					<th><?php echo __('Title'); ?></th>
					<th><?php echo __('Actions'); ?></th>
				</tr>
			</thead> 
		</table>
	</div>
	<div class="modal fade" id="helpsAddModal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title"><?php echo __('Add Help'); ?></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div> 
		      <form method="post" id="summbernoteHelp" action="{{ route('update_help') }}">  
		      	{{csrf_field()}}
		      <div class="modal-body">
		        <div class="form-group"> 
		        	<label> <?php echo __('Title'); ?> </label> 
		        	<input type="text" class="form-control" name="title"  id="title" required>
		        </div>
		        <div class="form-group"> 
		        	<label> <?php echo __('Content'); ?> </label>
		        	<textarea class="summernoteA" name="content" id="content" required>
		        	</textarea>
		        	<div id="total-caracteres"> </div>
		        </div> 
		      </div>
		      <div class="modal-footer">
		        <button type="sumit" class="btn btn-primary"><?php echo __('Add') ?></button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
		      </div>
		    </form>
		    </div>
		  </div>
	</div> 
	<div class="modal fade" id="helpsViewModal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title"><?php echo __('View Help'); ?></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div> 
		      
		      <div class="modal-body">
		        <div class="form-group" id="titleViewHelp"> 
		        	
		        </div>
		        <br>
		        <hr>
		        <div class="form-group" id="contentViewHelp"> 
		        	
		        </div> 
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
		      </div>
		   
		    </div>
		  </div>
	</div>

	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
	<script>
		$(function() {
			$('.summernoteA').summernote({
				 height: 250, 
			});
			$('.summernoteE').summernote({
				 height: 250, 
			});
		    $(".note-editable").on("keypress", function(){
		        var limiteCaracteres = 255;
		        var caracteres = $(this).text();
		        var totalCaracteres = caracteres.length;

		        //Update value
		        $("#total-caracteres").text(totalCaracteres);

		        //Check and Limit Charaters
		        if(totalCaracteres >= limiteCaracteres){
		            return false;
		        }
		    });
		    $('#helpsTable').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{ route('get_helps') }}",    
		        columns: [
		        	{ data: 'id', name: 'id' }, 
		            { data: 'title', name: 'title' },
		            { data: 'action', name: 'action'}    
		        ]
		    }); 
		});
		function getDataView(id) {
			$.ajax({  
				url:'/settings/view_help?id='+id,
				dataType:'json', 
				success:function(s) {
					$('#titleViewHelp').html(s.title);
					$('#contentViewHelp').html(s.content);
					$('#helpsViewModal').modal('show');
				}
			});
		}
		function confirmDel(id) {
			if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
				window.location.href='/settings/delete/'+id;
			} else {  
				return false;
			} 
		} 
	</script>
@endsection