@extends('layouts.back')
@section('title',_('View Settings'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Settings'); ?> </h2> 
		</div>
		<table class="table" id="usersTable">
			<thead>
				<tr>
					<th><?php echo __('#'); ?></th>     
					<th><?php echo __('Key'); ?></th>
					<th><?php echo __('Value'); ?></th>
					<th><?php echo __('Actions'); ?></th>
				</tr>
			</thead> 
		</table>
	</div>
	<div class="modal fade" id="settingsModal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title"><?php echo __('Update Setting'); ?></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div> 
		      <form method="post" action="{{ route('update_setting') }}"> 
		      	{{csrf_field()}}
		      <div class="modal-body">
		        <div class="form-group"> 
		        	<label> <?php echo __('Key'); ?> </label>
		        	<input type="hidden" name="option_id" id="OPTIONID">
		        	<input type="text" class="form-control" name="option_key" readonly="true" id="OPTIONKEY">
		        </div>
		        <div class="form-group"> 
		        	<label> <?php echo __('Value'); ?> </label>
		        	<input type="text" class="form-control" name="option_value" id="OPTIONVALUE" required>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="sumit" class="btn btn-primary"><?php echo __('Update') ?></button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
		      </div>
		    </form>
		    </div>
		  </div>
	</div>
	<script>
		$(function() {
		    $('#usersTable').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "/settings/getSettings",    
		        columns: [
		        	{ data: 'id', name: 'id' }, 
		            { data: 'option_key', name: 'option_key' },
		            { data: 'option_value', name: 'option_value' }, 
		            { data: 'action', name: 'action'}    
		        ]
		    });
		});
		function getData(id) {
			$.ajax({
				url:'/setting/edit/'+id,
				dataType:'json',
				success:function(s) {
					$('#OPTIONID').val(s.id);
					$('#OPTIONKEY').val(s.option_key);
					$('#OPTIONVALUE').val(s.option_value);
					$('#settingsModal').modal('show');
				}
			});
		}
	</script>
@endsection