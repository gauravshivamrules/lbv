@extends('layouts.back') 
@section('title',__('Add Searchtag')) 
@section('content')
	<div class="well well bs-component">   
		@foreach($errors->all() as $e)
			<li class="error"> {{ $e }} </li>
		@endforeach 
		</ul>
		<div class="row"> 
			<div class="col-md-2"></div>
			<div class="col-md-8"> 
				<form class="form-horizontal" action="{{route('addSearchTags')}}" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add Searchtag'); ?></legend>
						<div class="form-group">
							<div class="col-lg-8">
								{!! Form::select('search_group_id',$searchgroups,null,['class'=>'form-control'] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10">
								<input type="text" name="search_tag" class="form-control" placeholder="{{__('Add Searchtag')}}" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary"><?php echo __('Add'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
@endsection