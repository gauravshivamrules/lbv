@extends('layouts.back') 
@section('title',__('Add Prices')) 
@section('content')
<style type="text/css">
 	.ui-datepicker-title {
    	color: black;
	} 
</style>  
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
<link rel="stylesheet" type="text/css" href="/css/fullcalendar.min.css">
<link rel="stylesheet" type="text/css" href="/css/fullcalendar.print.min.css" media="print">

<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
    40%
  </div>
</div>

<ul>    
@foreach($errors->all() as $e)
  <li class="error"> {{ $e }} </li>
@endforeach 
</ul>
<!-- <div class="alert alert-info alert-dismissible">
 	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
 	<h4><i class="icon fa fa-info"></i> Alert!</h4>
 	{{__('Click on future dates to add prices')}}. <br>
 	{{__('You can add multiple price ranges')}}. 
  {{__('Click next to continue to add images')}}. 
  </div> -->
  <div class="well well bs-component">    
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
					<fieldset>
						<legend><?php echo __('Add Accommodation Prices'); ?></legend>  
             <a href="/accomodations/add_paid_images/{{$accom}}"  class="btn btn-danger" style="float:right;"><?php echo __('Next') ?></a>
            
            <ul class="nav nav-tabs">
              <li class="active">
                <a data-toggle="tab" href="#showCalendar">  {{__('Price Calendar')}} </a>
              </li>
              <li>
                <a data-toggle="tab" id="ExtraPriceLink" href="#showExtraPrice">{{__('Extra Prices')}}</a>
              </li>
            </ul>
            <br>
          <div class="tab-content">
            <div id="showCalendar" class="tab-pane fade in active">
              <p> {{__('Add prices for your accomodation here by clicking on any valid date')}}</p>
              <div id='calendar'></div>
            </div>

            <div id="showExtraPrice" class="tab-pane fade">
                <form action="/accomodations/add_paid_extra_price/{{$accom}}" method="post" accept-charset="utf-8"> 
                      {{csrf_field()}}
                     <p> {{__('If you have any other extra required or optional charges or services to offer, you can add them here')}}. </p>


                      <div class="row"> 
                        <div class="form-group col-md-2">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                            </font>
                          </label>
                          <input name="amount" class="form-control valid" type="number" id="ExtraPriceAmount" required="true">                        
                        </div>
                        <div class="form-group col-md-2">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Cost Type'); ?></font>
                            </font>
                          </label>
                          <select name="is_optional" class="form-control"> 
                              <option value="1">{{__('Optional')}}</option>
                              <option value="0">{{__('Required')}}</option>
                          </select>
                        </div>
                        <div class="form-group col-md-4">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                            </font>
                          </label>
                          <textarea name="description" class="form-control" rows="3" cols="30" required="true"></textarea>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="radio-inline">
                          <input type="radio" name="charge_type" value="perday">{{__('Per Day') }}
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="charge_type" value="fix">{{__('Fix') }}
                        </label>
                        <label class="radio-inline">
                          <input type="checkbox" name="is_per_person" value="1">{{__('Per Person') }}   
                        </label>                                              
                        </div>
                      </div>
                      <div class="form-action"> 
                           <button type="submit"  class="btn btn-primary" data-dismiss="modal"><?php echo __('Add') ?></button>
                      </div> 
                 </form>
            </div>                      
          </div>
        </div>
					</fieldset>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  <?php 
    $sunChecked=$satChecked=$anyChecked='';
    if(isset($CHANGEDAY)) {
        if($CHANGEDAY !='') {
            if($CHANGEDAY=='sunday') {
              $sunChecked='checked';
            } else if($CHANGEDAY=='saturday') {
              $satChecked='checked';
            } else {
              $anyChecked='checked'; 
            } 
        }   
    } 

    // dd($CHANGEDAY);
  ?>
  <!-- MODALS -->
	<div class="modal fade" id="priceModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo __('Add Prices'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          <div class="modal-body" id="homeModalBody">
          		
          		<form action="/accomodations/add_paid_price/{{$accom}}" method="post" accept-charset="utf-8" novalidate="true">  
          			{{csrf_field()}} 
                <input type="hidden" name="accom_type" value="{{$accomType}}" id="accomType">
  					<h4><?php echo __('Change Day'); ?> </h4>              
            <div class="form-group"> 
              <label class="radio-inline">
                <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
              </label>
              <label class="radio-inline">
                <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
              </label>
              <label class="radio-inline">
                <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
              </label>  
              <br>        
              <span id="changeDayErrPerWeek" style="color:red;"> </span>
            </div> 
            @if(!$HASPRICES) 
    					<div class="form-group">
    						<select name="price_unit" id="PriceUnit" class="form-control">
    							<option value="">{{__('Select Price Unit')}}</option>
    							<option value="perperson">{{__('Per Person')}}</option> 
    							<option value="perroom">{{__('Per Room')}}</option>
    						</select>        
    					</div> 
            @endif

  					<div id="PerRoomDiv" > 
  						<div class="row"> 
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
  									</font>
  								</label>
  								<input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoom" required="true">                        
  							</div>
  							<div class="form-group col-md-8">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
  									</font>
  								</label>
  								<textarea name="description_perroom" class="form-control" rows="2" cols="50"></textarea>
  							</div>
  							
  						</div>
  						<div class="row"> 
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
  									</font>
  								</label>
  								<input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoom" required="true">                        
  							</div>
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
  									</font>
  								</label>
  								<input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoom" required="true">                        
  							</div>
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
  									</font>
  								</label>
  								<input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoom" value="1" min="1"  required="true">                        
  							</div>
  						</div>
  						<div class="row"> 
  							<div class="form-group col-md-3">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
  									</font>
  								</label>
  								<input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoom" required="true">                        
  							</div>
  							<div class="form-group col-md-3">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
  									</font>
  								</label>
  								<input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoom" required="true">                        
  							</div> 

  							<div class="form-group col-md-3">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
  									</font>
  								</label>
  								<input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoom" required="true" value="1">                        
  							</div>
  						
  							<div class="form-group col-md-3">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
  									</font> 
  								</label>
  								<input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoom" required="true" value="1">                        
  							</div> 
  						</div>
              <div class="row" id="maxChildAgeDivPerRoom"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                      </font>
                    </label>
                    <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoom" required="true" min="0" value="0">                        
                  </div> 
              </div>
  					</div>

  					<div id="PerPersonDiv" >  
  						<div class="row"> 
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
  									</font>
  								</label>
  								<input name="room_type" class="form-control valid" type="text" id="PriceRoomType" required="true">                        
  							</div>
  							<div class="form-group col-md-8">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
  									</font>
  								</label>
  								<textarea name="description" class="form-control" rows="2" cols="50"></textarea>
  							</div>
  							
  						</div>
  						<div class="row"> 
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
  									</font>
  								</label>
  								<input name="start_date" class="form-control valid" type="text" id="PriceStartDate" required="true">                        
  							</div>
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
  									</font>
  								</label>
  								<input name="end_date" class="form-control valid" type="text" id="PriceEndDate" required="true">                        
  							</div>
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
  									</font>
  								</label>
  								<input name="min_days" class="form-control valid" type="number" id="PriceMinDay" required="true" min="1" value="1">                        
  							</div>
  						</div>

  						<div class="row"> 
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
  									</font>
  								</label>
  								<input name="no_of_rooms" class="form-control valid" type="number" value="1" id="PriceNoOfRoom" required="true" min="1" value="1" >                        
  							</div> 

  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
  									</font>
  								</label>
  								<input name="adults" class="form-control valid" type="number" id="PriceAdult" required="true" min="1" value="1">                        
  							</div>

  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
  									</font>
  								</label>
  								<input name="childs" class="form-control valid" type="number"  id="PriceChilds" required="true" min="0" value="1">                        
  							</div> 



  						</div>

  						<div class="row"> 
                <div class="form-group col-md-4" id="maxChildAgePerPerson">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                    </font>
                  </label>
                  <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAge" required="true" min="0" value="0">                        
                </div> 
  							
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">

  										<font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
  									</font>
  								</label>
  								<input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdult" required="true" min="1" value="1">                        
  							</div>

  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
  									</font>
  								</label>
  							<input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChild" required="true" min="1" value="1">                        
  							</div>
  						
  							
  						</div>

  						<div class="row"> 
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                    </font>
                  </label>
                  <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoom" required="true" min="1" value="1">                        
                </div>
  							<div class="form-group col-md-4">
  								<label class="control-label">
  									<font style="vertical-align: inherit;">
  										<font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
  									</font>
  								</label>
  								<input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoom" required="true" min="1" value="1">                        
  							</div>
  						
  						</div>

              <div class="row"> 
             
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Per Person/Night)'); ?>&euro;</font>
                    </font>
                  </label>
                  <input name="price1" class="form-control valid" type="number" min="1" id="Price1" required="true" >                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('2 Person/Night'); ?>&euro;</font>
                    </font>
                  </label>
                <input name="price2" class="form-control valid" type="number" min="1" id="Price2" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <?php echo __('Family/Night'); ?>&euro;
                    </font>
                  </label>
                  <input name="price3" class="form-control valid" type="number" min="1" id="Price3" required="true">                        
                </div>
              </div>
  					</div>

          	

          </div>
          <div class="modal-footer" >
            <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
            <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>
          <div class="modal-footer" style="display:none;" id="perPersonFooter">
            <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
            <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>

        </form>
        </div>
      </div>
 	</div>
 
 	<div class="modal fade" id="priceModalPerWeek">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <h5 class="modal-title"><?php echo __('Add Prices Per Week'); ?></h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div> 
           <div class="modal-body" id="homeModalBodyPerWeek">	 

           	<form action="/accomodations/add_paid_price/{{$accom}}" method="post" accept-charset="utf-8" > 
   					  {{csrf_field()}}
              <input type="hidden" name="accom_type" value="{{$accomType}}" id="accomType"> 
   					<h4><?php echo __('Change Day'); ?> </h4>              
   					<div class="form-group"> 
   						<label class="radio-inline">
   							<input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
   						</label>
   						<label class="radio-inline">
   							<input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
   						</label>
   						<label class="radio-inline">
   							<input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
   						</label>  
   						<br>        
   						<span id="changeDayErrPerWeek" style="color:red;"> </span>
   					</div> 
   					<div class="row"> 
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
   								</font>
   							</label>
   							<input name="start_date" class="form-control valid" type="text" id="PriceStartDatePerWeek" required="true">                        
   						</div>
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
   								</font>
   							</label>
   							<input name="end_date" class="form-control valid" type="text" id="PriceEndDatePerWeek" required="true">                        
   						</div>
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
   								</font>
   							</label>
   							<input name="min_days" class="form-control valid" type="number" value="7" id="PriceMinDay" required="true" min="1">                        
   						</div>
   					</div>
   					
   					<div class="row"> 
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<font style="vertical-align: inherit;"><?php echo __('Maximum Persons'); ?></font>
   								</font>
   							</label>
   							<input name="max_persons" class="form-control valid" type="number" value="1"  min="1" id="PriceMaxPerson" required="true">                        
   						</div>
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
   								</font>
   							</label>
   							<input name="amount" class="form-control valid" type="number" min="1" id="PriceAmount" required="true">                        
   						</div>
   						<div class="form-group col-md-4">
   							<label class="control-label">
   								<font style="vertical-align: inherit;">
   									<?php echo __('Extra Price (Per Person)'); ?>
   								</font>
   							</label>
   							<input name="extra_price_per_person" class="form-control valid" type="number" min="1" id="PriceExtraPricePerPerson" required="true">                        
   						</div>
   					</div>
           	</div>
           <div class="modal-footer">
           	<button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
            <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
           </div>
       		</form>
        
         </div>
       </div>
  </div> 

	<script type="text/javascript" src="/js/moment.js"></script> 
	<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/js/custom/accomodation_prices.js"></script>  
@endsection