@extends('layouts.back') 
@section('title',__('Add Images')) 
@section('content')
 <div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">
    60%
  </div>
</div>
 <ul>    
 @foreach($errors->all() as $e)
 	<li class="error"> {{ $e }} </li>
 @endforeach 
 </ul>
 <div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="#" method="post" novalidate="true">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add Accommodation Images'); ?></legend> 
						<a href="/accomodations/add_paid_location"  class="btn btn-danger" style="float:right;"><?php echo __('Next') ?></a>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
@endsection