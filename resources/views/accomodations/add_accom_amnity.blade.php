@extends('layouts.back') 
@section('title',__('Add Amnities')) 
@section('content')
 <div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
    100%
  </div>
</div>
 <ul>    
 @foreach($errors->all() as $e)
 	<li class="error"> {{ $e }} </li>
 @endforeach 
 </ul>
 <div class="well well bs-component">    
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/accomodations/add_accom_amnities/{{$accom}}" method="post">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend>{{ __('Add Accommodation Amnities') }} </legend> 
						
						<?php foreach ($searchGroup as $amenity) { ?>
								<div class="row" style="margin: 0;">
									<h3><?php echo $amenity['group_name'] ?> </h3>
									<?php foreach ($amenity['SearchingTags'] as $tag) { 
											if($tag->id) {
									?>

										<div class="col-md-3">
											<div class="checkbox checkbox-info">
												<input name="searchtag_id[]" value="<?php echo $tag->id ?>" id="<?php echo $tag->id ?>" class="styled" type="checkbox">
												<label for="<?php echo $tag->id ?>">
													<?php echo $tag->tag_name ?> 	
												</label>
											</div>
										</div>
									   <?php  }}  ?>
								</div>
						<?php } ?>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" style="float:right;"><?php echo __('Finish'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
@endsection