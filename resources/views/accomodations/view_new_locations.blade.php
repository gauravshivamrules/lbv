@extends('layouts.back')
@section('title',_('New Locations'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	<div class="panel panel-default"> 
		<div class="panel-heading">
			<h2> <?php echo __('All New Locations'); ?> </h2> 
		</div>

		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#showCountries"><span class="label label-success" id="active"></span> {{__('Countries')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showRegions"><span class="label label-warning" id="pending"></span> {{__('Regions')}}</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showDepartments"><span class="label label-primary" id="rejected"></span> {{_('Departments')}} </a>
			</li>
		</ul> 

		<div class="tab-content"> 
			<div id="showCountries" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="countriesTable">
					<thead>
						<tr>
							<th><?php echo __('Country'); ?></th>     
							<th><?php echo __('Region'); ?></th> 
							<th><?php echo __('Department'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody>
						@if($countries)
						@foreach($countries as $l) 
							<tr>
								<td>{{$l['name']}}</td>
								
								<td>{{$l['region'] ? $l['region']['region_name'] : ''  }}</td>
								<td>{{$l['department'] ? $l['department']['department_name'] : ''}}</td> 
								<td>
									<a href="/accomodations/edit-new-locations/{{$l['id']}}/{{$l['region']['id']}}/{{$l['department']['id']}}" class="btn btn-xs btn-primary">
										<i class="glyphicon glyphicon-edit"></i> 
									</a>
								</td> 
							</tr>
						@endforeach
						@endif
					</tbody>
				</table>
				</div>
			</div>
			<div id="showRegions" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="regionsTable">
					<thead>
						<tr>
							<th><?php echo __('Region'); ?></th>     
							<th><?php echo __('Country'); ?></th> 
							<th><?php echo __('Department'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody>
						@if($regions)
						@foreach($regions as $l) 
							<tr>
								<td><span class="label label-danger"> {{$l['region_name']}} </span></td>
								<td>{{$l['country']['name']}}</td>
								<td>{{$l['department']['department_name']}}</td> 
								<td>
									<a href="/accomodations/edit-new-locations/{{$l['country']['id']}}/{{$l['id']}}/{{$l['department']['id']}}" class="btn btn-xs btn-primary">
										<i class="glyphicon glyphicon-edit"></i> 
									</a>
								</td> 
							</tr>
						@endforeach
						@endif
					</tbody>
				</table>
				</div>
			</div>
			<div id="showDepartments" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="departmentsTable">
					<thead>
						<tr>
							<th><?php echo __('Department'); ?></th>     
							<th><?php echo __('Region'); ?></th> 
							<th><?php echo __('Country'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody>
						@if($departments)
						@foreach($departments as $l)  
							<tr>
								<td><span class="label label-danger">{{$l['department_name']}}</span></td>
								<td>{{$l['region']['region_name']}}</td>
								<td>{{$l['country']['name']}}</td>  
								<td>
									<a href="/accomodations/edit-new-locations/{{$l['country']['id']}}/{{$l['region']['id']}}/{{$l['id']}}" class="btn btn-xs btn-primary">
										<i class="glyphicon glyphicon-edit"></i> 
									</a>
								</td> 
							</tr>
						@endforeach
						@endif
					</tbody>
				</table>
				</div>
			</div>
		</div>	
	</div> 
		<script>
			$(function() {
			    $('#countriesTable').DataTable({
			    	responsive: true,
			        /*processing: true,
			        serverSide: true,
			        ajax: "/accomodations/get-new-locations",    
			        columns: [
			            { data: 'country_id', name: 'country_id' }, 
			            { data: 'region_id', name: 'region_id' }, 
			            { data: 'department_id', name: 'department_id' }, 
			            { data: 'action', name: 'action'}    
			        ]*/
			    });
			    $('#regionsTable').DataTable({responsive: true});
			    $('#departmentsTable').DataTable({responsive: true});
			}); 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/accomodations/delete-accomodation/'+id;  
				} else { 
					return false;
				} 
			}  
		</script>
@endsection