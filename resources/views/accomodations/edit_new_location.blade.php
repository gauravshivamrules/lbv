@extends('layouts.back') 
@section('title',__('Edit Location')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/accomodations/edit-new-locations/{{$country}}/{{$region}}/{{$department}}" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Edit Location'); ?></legend> 
						<div class="form-group"> 
							<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
							<div class="col-lg-8">
								<input type="hidden" name="label_id" >
								<input type="hidden" name="payment_type"> 
								<input name="country_name" class="form-control"  type="text"  value="{{$cName->name}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Region'); ?></label>
							<div class="col-lg-8">
								<input name="region_name" class="form-control"  type="text"  value="{{$rName->region_name}}"   required>     
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Department'); ?> </label>
							<div class="col-lg-8">
								<input name="department_name" class="form-control"  type="text"  value="{{$dName->department_name}}" required>                          
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Update & Approve'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
@endsection