@extends('layouts.back') 
@section('title',__('Add Prices')) 
@section('content')
<style type="text/css">
 	.ui-datepicker-title {
    	color: black;
	} 
  .fc-time{
     display : none;
  }
  .tooltip-inner {
      white-space:pre-wrap;
  }
</style>  
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
<link rel="stylesheet" type="text/css" href="/css/fullcalendar.min.css">
<link rel="stylesheet" type="text/css" href="/css/fullcalendar.print.min.css" media="print">

<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
    40%
  </div>
</div>
<ul>    
@foreach($errors->all() as $e)
  <li class="error"> {{ $e }} </li> 
@endforeach 
</ul>
 
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
  <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
  <div class="well well bs-component">    
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
        <?php 
          $sunChecked=$satChecked=$anyChecked='';
          if(isset($CHANGEDAY)) {
              if($CHANGEDAY !='') {
                  if($CHANGEDAY=='sunday') {
                    $sunChecked='checked';
                  } else if($CHANGEDAY=='saturday') {
                    $satChecked='checked';
                  } else {
                    $anyChecked='checked'; 
                  } 
              }   
          } 
        ?>
        <?php if($LABEL) { ?>
            <?php if($LABEL==3) { ?>
                  <!-- RENT -->
                  <fieldset>
                            <legend><?php echo __('Add Accommodation Prices'); ?></legend>  
                             <a href="/accomodations/add_accom_images/{{$accom}}"  class="btn btn-danger" style="float:right;"><?php echo __('Next') ?></a>
                            <ul class="nav nav-tabs">
                              <li class="active">
                                <a data-toggle="tab" href="#showCalendar">  {{__('Price Calendar')}} </a>
                              </li>
                              @if($HASPRICES)
                              <li>
                                <a data-toggle="tab" href="#showPrices">  {{__('Prices')}} </a>
                              </li>
                              @endif
                              <li>
                                <a data-toggle="tab" id="ExtraPriceLink" href="#showExtraPrice">{{__('Extra Prices')}}</a>
                              </li>
                              @if($HASPRICES)
                                @if($PRICEUNIT !='perweek')
                                <li>
                                  <a href="javascript:;" onclick="confirmSwitchAccom({{$accom}})"><?php echo __('Switch Price Unit') ?></a>
                                </li>
                                @endif  
                              @endif
                            </ul>
                            <br>
                      <div class="tab-content">
                        <div id="showCalendar" class="tab-pane fade in active">
                          <p> {{__('Add prices for your accomodation here by clicking on any valid date')}}</p>
                          <div id='calendar'></div>
                        </div>
                        <div id="showPrices" class="tab-pane fade"> 
                          <?php if($allPrices) { ?>
                            <div class="table-responsive">  
                              <!-- PER WEEk -->
                              <?php if($PRICEUNIT !='') { ?>
                                <?php if($PRICEUNIT =='perweek') { ?>
                                      <table class="table table-striped table-hover dt-responsive display nowrap" id="perWeekTable">
                                        <thead>
                                          <tr>
                                            <th><?php echo __('Start'); ?></th>     
                                            <th><?php echo __('End'); ?></th>
                                            <th><?php echo __('Amount'); ?>(&euro;)</th>
                                            <th><?php echo __('Change Day'); ?></th>
                                            <th><?php echo __('Min. Days'); ?></th>
                                            <th><?php echo __('Extra Person Price'); ?></th>
                                            <th><?php echo __('Actions'); ?></th>
                                          </tr>
                                          <tbody>
                                          @foreach($allPrices as $e)
                                            <tr>  
                                                <td> {{date('d-m-Y',strtotime($e['start_date']))}} </td>
                                                <td> {{date('d-m-Y',strtotime($e['end_date']))}} </td>
                                                <td> {{$e['amount']}} </td>
                                                <td> {{$e['changeday']}} </td>
                                                <td> {{$e['min_days']}} </td>
                                                <td> {{$e['extra_price_per_person']}} </td>
                                                <td> 
                                                    <a href="javascript:;" onclick="editPriceTable({{$e['id']}},'{{$PRICEUNIT}}')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>
                                                    <a id="deleteExtraCharge"  onclick="confirmDelTable({{$e['id']}},'{{$PRICEUNIT}}');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a> 
                                                </td>
                                            </tr>
                                          @endforeach
                                            
                                          </tbody>
                                        </thead> 
                                      </table>
                                <?php } else if($PRICEUNIT =='perroom') { ?>
                                      <!-- PER ROOM -->
                                      <table class="table table-striped table-hover dt-responsive display nowrap" id="perRoomTable">
                                        <thead>
                                          <tr>
                                            <th><?php echo __('Start'); ?></th>     
                                            <th><?php echo __('End'); ?></th>
                                            <th><?php echo __('Room'); ?></th>
                                            <th><?php echo __('Amount'); ?>(&euro;)</th>
                                            <th><?php echo __('Change Day'); ?></th>
                                            <th><?php echo __('Adults/Childs'); ?></th>
                                            <th><?php echo __('Actions'); ?></th>
                                          </tr>
                                          <tbody>
                                          @foreach($allPrices as $e)
                                            <tr>  
                                                <td> {{date('d-m-Y',strtotime($e['room_price'][0]['start_date']))}} </td>
                                                <td> {{date('d-m-Y',strtotime($e['room_price'][0]['end_date']))}} </td>
                                                <td> {{$e['room_type']}} </td>
                                                <td> {{$e['room_price'][0]['price']}} </td>
                                                <td> {{ $HASPRICES ? $CHANGEDAY: '' }} </td>
                                                <td> {{$e['adults']}}/{{$e['childs']}} </td>
                                                <td> 
                                                  
                                                    <a href="javascript:;" onclick="editPriceTable({{$e['id']}},'{{$PRICEUNIT}}')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>
                                                    <a id="deleteExtraCharge"  onclick="confirmDelTable({{$e['accom_price_id']}} ,'{{$PRICEUNIT}}',{{$e['id']}},{{$e['room_price'][0]['id']}});" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a> 
                                                </td>
                                            </tr>
                                          @endforeach
                                            
                                          </tbody>
                                        </thead> 
                                      </table>
                                <?php } else { ?>
                                      <!-- PER PERSON -->
                                      <table class="table table-striped table-hover dt-responsive display nowrap" id="perPersonTable">
                                        <thead>
                                          <tr>
                                            <th><?php echo __('Start'); ?></th>     
                                            <th><?php echo __('End'); ?></th>
                                            <th><?php echo __('Room'); ?></th>
                                            <th><?php echo __('Amount'); ?>(&euro;)</th>
                                            <th><?php echo __('Change Day'); ?></th>
                                            <th><?php echo __('Adults/Childs'); ?></th>
                                            <th><?php echo __('Actions'); ?></th>
                                          </tr>
                                          <tbody>
                                          @foreach($allPrices as $e)
                                            <tr>  
                                                <td> {{date('d-m-Y',strtotime($e['room_price'][0]['start_date']))}} </td>
                                                <td> {{date('d-m-Y',strtotime($e['room_price'][0]['end_date']))}} </td>
                                                <td> {{$e['room_type']}} </td>
                                                <td> {{$e['room_price'][0]['price1']}},{{$e['room_price'][0]['price2']}},{{$e['room_price'][0]['price3']}} </td>
                                                <td> {{ $HASPRICES ? $CHANGEDAY: '' }} </td>
                                                <td> {{$e['adults']}}/{{$e['childs']}} </td>
                                                <td> 
                                                    <a href="javascript:;" onclick="editPriceTable({{$e['id']}},'{{$PRICEUNIT}}')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>
                                                      <a id="deleteExtraCharge"  onclick="confirmDelTable({{$e['accom_price_id']}} ,'{{$PRICEUNIT}}',{{$e['id']}},{{$e['room_price'][0]['id']}});" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a> 
                                                </td>
                                            </tr>
                                          @endforeach
                                          </tbody>
                                        </thead> 
                                      </table>
                                <?php } ?> 
                            </div>
                            <?php } ?>
                          <?php } ?>

                        </div> 

                        <div id="showExtraPrice" class="tab-pane fade">
                            @if($extraPrices)
                            <div class="table-responsive">  
                              <table class="table table-striped table-hover dt-responsive display nowrap" id="extraPriceTable">
                              <thead>
                                <tr>
                                  <th><?php echo __('Amount'); ?></th>     
                                  <th><?php echo __('Description'); ?></th>
                                  <th><?php echo __('Is Optional'); ?></th>
                                  <th><?php echo __('Is Per Person'); ?></th>
                                  <th><?php echo __('Created On'); ?></th>
                                  <th><?php echo __('Type'); ?></th>
                                  <th><?php echo __('Actions'); ?></th>
                                </tr>
                                <tbody>
                                @foreach($extraPrices as $e)
                                  <tr>  
                                      <td> {{$e['amount']}} </td>
                                      <td> {{$e['description']}} </td>
                                      
                                      <td> {{ $e['is_optional']  ? __("YES") : __("NO") }} </td>
                                      <td> {{ $e['is_per_person']  ? __("YES") : __("NO") }} </td> 
                                      <td> {{$e['created_at']}} </td>
                                      <td> {{$e['charge_type']}} </td>
                                      <td> 
                                          <a href="javascript:;" onclick="editExtra({{$e['id']}})" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>
                                          <a id="deleteExtraCharge"  onclick="deleteExtraCharge({{$e['id']}});" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a> 
                                      </td>
                                  </tr>
                                @endforeach
                                  
                                </tbody>
                              </thead> 
                            </table>
                            </div>
                            @endif

                            <form action="/accomodations/add_paid_extra_price/{{$accom}}" method="post" accept-charset="utf-8"> 
                                  {{csrf_field()}}
                                 <p> {{__('If you have any other extra required or optional charges or services to offer, you can add them here')}}. </p>


                                  <div class="row"> 
                                    <div class="form-group col-md-2">
                                      <label class="control-label">
                                        <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                                        </font>
                                      </label>
                                      <input name="amount" class="form-control valid" type="number" min="0" id="ExtraPriceAmount" required="true">                        
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="control-label">
                                        <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;"><?php echo __('Cost Type'); ?></font>
                                        </font>
                                      </label>
                                      <select name="is_optional" class="form-control"> 
                                          <option value="1">{{__('Optional')}}</option>
                                          <option value="0">{{__('Required')}}</option>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label class="control-label">
                                        <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                                        </font>
                                      </label>
                                      <textarea name="description" class="form-control" rows="3" cols="30" required="true"></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                       <label class="radio-inline">
                                      <input type="radio" name="charge_type" value="perday">{{__('Per Day') }}
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="charge_type" value="fix">{{__('Fix') }}
                                    </label>
                                    <label class="radio-inline">
                                      <input type="checkbox" name="is_per_person" value="1">{{__('Per Person') }}   
                                    </label>                                              
                                    </div>
                                  </div>
                                  <div class="form-action"> 
                                       <button type="submit"  class="btn btn-primary" data-dismiss="modal"><?php echo __('Add') ?></button>
                                  </div> 
                             </form>
                        </div>
                        <script type="text/javascript">
                          $(function() {
                              $('#extraPriceTable').DataTable({responsive: true});
                              $('#perWeekTable').DataTable({responsive: true});
                              $('#perRoomTable').DataTable({responsive: true});
                              $('#perPersonTable').DataTable({responsive: true});
                          });
                        </script>
                      </div>
                  </fieldset>
            <?php } else { ?>
                  <!-- SALE --> 
                  <form action="/accomodations/add-sale-accom-price/{{$accom}}" method="post" accept-charset="utf-8"> 
                        {{csrf_field()}} 
                        <h4> {{__('Add Accomodation Price')}}</h4>
                        <div class="form-group col-md-2">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                            </font>
                          </label>
                          <input name="amount" class="form-control valid" type="number" min="0"  id="SaleAmount" required="true">                        
                        </div>
                        <div class="form-action"> 
                             <button type="submit"  class="btn btn-primary"><?php echo __('Add') ?></button>
                        </div> 
                   </form>
            <?php } ?>
        <?php } ?>
        </div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

  
  <!-- MODALS -->

  <div class="modal fade" id="priceModalPerWeek">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Add Prices Per Week'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/add_accom_price/{{$accom}}" method="post" accept-charset="utf-8" > 
                {{csrf_field()}}
               <input type="hidden" name="accom_type" value="{{$accomType}}" id="accomType"> 
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
              <div class="row"> 
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                    </font>
                  </label>
                  <input name="start_date" class="form-control valid" type="text" id="PriceStartDatePerWeek" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                    </font>
                  </label>
                  <input name="end_date" class="form-control valid" type="text" id="PriceEndDatePerWeek" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                    </font>
                  </label>
                  <input name="min_days" class="form-control valid" type="number" value="7" id="PriceMinDay" required="true" min="1">                        
                </div>
              </div>
              
              <div class="row"> 
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Maximum Persons'); ?></font>
                    </font>
                  </label>
                  <input name="max_persons" class="form-control valid" type="number" value="0"  min="0" id="PriceMaxPerson" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                    </font>
                  </label>
                  <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmount" required="true" value="1">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <?php echo __('Extra Price (Per Person)'); ?>
                    </font>
                  </label>
                  <input name="extra_price_per_person" class="form-control valid" type="number" min="0" value="0" id="PriceExtraPricePerPerson" required="true">                        
                </div>
              </div>
              </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
             <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
            </div>
            </form>
         
          </div>
        </div>
  </div>  

  <div class="modal fade" id="priceModalNew">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Add Prices'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/add_accom_price/{{$accom}}" method="post" accept-charset="utf-8" > 
                {{csrf_field()}}
               <input type="hidden" name="accom_type" value="{{$accomType}}" id="accomType"> 
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
              <?php if(!$HASPRICES) { ?>
                <div class="form-group">  
                  <select name="price_unit" id="PriceUnit" class="form-control">
                    <option value="">{{__('Select Price Unit')}}</option>
                    <option value="perperson">{{__('Per Person')}}</option> 
                    <option value="perroom">{{__('Per Room')}}</option>
                  </select>        
                </div>
                <div id="PerRoomDiv" style="display:none;" > 
                    
                    <div class="row"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                          </font>
                        </label>
                        <input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoom" required="true">                        
                      </div>
                      <div class="form-group col-md-8">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                          </font>
                        </label>
                        <textarea name="description_perroom" class="form-control" rows="2" cols="50"></textarea>
                      </div>
                      
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                          </font>
                        </label>
                        <input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoom" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                          </font>
                        </label>
                        <input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoom" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                          </font>
                        </label>
                        <input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoom" value="1" min="1"  required="true">                        
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                          </font>
                        </label>
                        <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoom" required="true">                        
                      </div>
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                          </font>
                        </label>
                        <input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoom" required="true">                        
                      </div> 

                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                          </font>
                        </label>
                        <input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoom" required="true" value="1">                        
                      </div>
                    
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                          </font> 
                        </label>
                        <input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoom" required="true" value="0">                        
                      </div> 
                    </div>
                    <div class="row" id="maxChildAgeDivPerRoom"> 
                        <div class="form-group col-md-4">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                            </font>
                          </label>
                          <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoom" required="true" min="0" value="0">                        
                        </div> 
                    </div>
                </div>

                <div id="PerPersonDiv"  style="display:none;">  
                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                        </font>
                      </label>
                      <input name="room_type" class="form-control valid" type="text" id="PriceRoomType" required="true">                        
                    </div>
                    <div class="form-group col-md-8">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                        </font>
                      </label>
                      <textarea name="description" class="form-control" rows="2" cols="50"></textarea>
                    </div>
                    
                  </div>
                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                        </font>
                      </label>
                      <input name="start_date" class="form-control valid" type="text" id="PriceStartDate" required="true">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                        </font>
                      </label>
                      <input name="end_date" class="form-control valid" type="text" id="PriceEndDate" required="true">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                        </font>
                      </label>
                      <input name="min_days" class="form-control valid" type="number" id="PriceMinDay" required="true" min="1" value="1">                        
                    </div>
                  </div>

                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                        </font>
                      </label>
                      <input name="no_of_rooms" class="form-control valid" type="number" value="1" id="PriceNoOfRoom" required="true" min="1" value="1" >                        
                    </div> 

                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                        </font>
                      </label>
                      <input name="adults" class="form-control valid" type="number" id="PriceAdult" required="true" min="1" value="1">                        
                    </div>

                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                        </font>
                      </label>
                      <input name="childs" class="form-control valid" type="number"  id="PriceChilds" required="true" min="0" value="0">                        
                    </div> 

                  </div>

                  <div class="row"> 
                    <div class="form-group col-md-4" id="maxChildAgePerPerson">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                        </font>
                      </label>
                      <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAge" required="true" min="0" value="0">                        
                    </div> 
                    
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">

                          <font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
                        </font>
                      </label>
                      <input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdult" required="true" min="0" value="0">                        
                    </div>

                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
                        </font>
                      </label>
                    <input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChild" required="true" min="0" value="0">                        
                    </div>
                  
                    
                  </div>

                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                        </font>
                      </label>
                      <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoom" required="true" min="0" value="0">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
                        </font>
                      </label>
                      <input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoom" required="true" min="0" value="0">                        
                    </div>
                  
                  </div>

                  <div class="row"> 
                 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Per Person/Night'); ?>&euro;</font>
                        </font>
                      </label>
                      <input name="price1" class="form-control valid" type="number" min="1" id="Price1" required="true" >                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('2 Person/Night'); ?>&euro;</font>
                        </font>
                      </label>
                    <input name="price2" class="form-control valid" type="number" min="1" id="Price2" required="true">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <?php echo __('Family/Night'); ?>&euro;
                        </font>
                      </label>
                      <input name="price3" class="form-control valid" type="number" min="1" id="Price3" required="true">                        
                    </div>
                  </div>
                </div>    
               
              <?php } else { ?>

                  <div class="form-group">  
                      <input name="price_unit"  class="form-control valid" type="hidden" id="PriceUnit" value="{{$PRICEUNIT}}">                        
                  </div>

                  <?php if($PRICEUNIT=='perroom') { ?>
                    
                      <div id="PerRoomDiv"> 
                          
                          
                          <div class="row"> 
                            <div class="form-group col-md-4">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                                </font>
                              </label>
                              <input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoom" required="true">                        
                            </div>
                            <div class="form-group col-md-8">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                                </font>
                              </label>
                              <textarea name="description_perroom" class="form-control" rows="2" cols="50"></textarea>
                            </div>
                            
                          </div>
                          <div class="row"> 
                            <div class="form-group col-md-4">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                                </font>
                              </label>
                              <input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoom" required="true">                        
                            </div>
                            <div class="form-group col-md-4">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                                </font>
                              </label>
                              <input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoom" required="true">                        
                            </div>
                            <div class="form-group col-md-4">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                                </font>
                              </label>
                              <input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoom" value="1" min="1"  required="true">                        
                            </div>
                          </div>
                          <div class="row"> 
                            <div class="form-group col-md-3">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                                </font>
                              </label>
                              <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoom" required="true">                        
                            </div>
                            <div class="form-group col-md-3">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                                </font>
                              </label>
                              <input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoom" required="true">                        
                            </div> 

                            <div class="form-group col-md-3">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                                </font>
                              </label>
                              <input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoom" required="true" value="1">                        
                            </div>
                          
                            <div class="form-group col-md-3">
                              <label class="control-label">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                                </font> 
                              </label>
                              <input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoom" required="true" value="0">                        
                            </div> 
                          </div>
                          <div class="row" id="maxChildAgeDivPerRoom"> 
                              <div class="form-group col-md-4">
                                <label class="control-label">
                                  <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                                  </font>
                                </label>
                                <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoom" required="true" min="0" value="0">                        
                              </div> 
                          </div>
                      </div>
                  <?php } else { ?>
                      <div id="PerPersonDiv">  
                        <div class="row"> 
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                              </font>
                            </label>
                            <input name="room_type" class="form-control valid" type="text" id="PriceRoomType" required="true">                        
                          </div>
                          <div class="form-group col-md-8">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                              </font>
                            </label>
                            <textarea name="description" class="form-control" rows="2" cols="50"></textarea>
                          </div>
                          
                        </div>
                        <div class="row"> 
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                              </font>
                            </label>
                            <input name="start_date" class="form-control valid" type="text" id="PriceStartDate" required="true">                        
                          </div>
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                              </font>
                            </label>
                            <input name="end_date" class="form-control valid" type="text" id="PriceEndDate" required="true">                        
                          </div>
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                              </font>
                            </label>
                            <input name="min_days" class="form-control valid" type="number" id="PriceMinDay" required="true" min="1" value="1">                        
                          </div>
                        </div>

                        <div class="row"> 
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                              </font>
                            </label>
                            <input name="no_of_rooms" class="form-control valid" type="number" value="1" id="PriceNoOfRoom" required="true" min="1" value="1" >                        
                          </div> 

                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                              </font>
                            </label>
                            <input name="adults" class="form-control valid" type="number" id="PriceAdult" required="true" min="1" value="1">                        
                          </div>

                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                              </font>
                            </label>
                            <input name="childs" class="form-control valid" type="number"  id="PriceChilds" required="true" min="0" value="0">                        
                          </div> 

                        </div>

                        <div class="row"> 
                          <div class="form-group col-md-4" id="maxChildAgePerPerson">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                              </font>
                            </label>
                            <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAge" required="true" min="0" value="0">                        
                          </div> 
                          
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">

                                <font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
                              </font>
                            </label>
                            <input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdult" required="true" min="0" value="0">                        
                          </div>

                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
                              </font>
                            </label>
                          <input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChild" required="true" min="0" value="0">                        
                          </div>
                        
                          
                        </div>

                        <div class="row"> 
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                              </font>
                            </label>
                            <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoom" required="true" min="0" value="0">                        
                          </div>
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
                              </font>
                            </label>
                            <input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoom" required="true" min="0" value="0">                        
                          </div>
                        
                        </div>

                        <div class="row"> 
                       
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('Per Person/Night)'); ?>&euro;</font>
                              </font>
                            </label>
                            <input name="price1" class="form-control valid" type="number" min="1" id="Price1" required="true" >                        
                          </div>
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"><?php echo __('2 Person/Night'); ?>&euro;</font>
                              </font>
                            </label>
                          <input name="price2" class="form-control valid" type="number" min="1" id="Price2" required="true">                        
                          </div>
                          <div class="form-group col-md-4">
                            <label class="control-label">
                              <font style="vertical-align: inherit;">
                                <?php echo __('Family/Night'); ?>&euro;
                              </font>
                            </label>
                            <input name="price3" class="form-control valid" type="number" min="1" id="Price3" required="true">                        
                          </div>
                        </div>
                      </div>
                  <?php } ?>
              <?php } ?>


          </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
             <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
            </div>
            </form>
         
          </div>
        </div>
  </div> 

  <div class="modal fade" id="priceModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo __('Add Prices'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          <div class="modal-body" id="homeModalBody">
              
              <form action="/accomodations/add_accom_price/{{$accom}}" method="post" accept-charset="utf-8">  
                {{csrf_field()}} 
                <input type="hidden" name="accom_type" value="{{$accomType}}" id="accomType">
            <h4><?php echo __('Change Day'); ?> </h4>              
            <div class="form-group"> 
              <label class="radio-inline">
                <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
              </label>
              <label class="radio-inline">
                <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
              </label>
              <label class="radio-inline">
                <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
              </label>  
              <br>        
              <span id="changeDayErrPerWeek" style="color:red;"> </span>
            </div> 
          
              <div class="form-group">
                <select name="price_unit" id="PriceUnit" class="form-control">
                  <option value="">{{__('Select Price Unit')}}</option>
                  <option value="perperson">{{__('Per Person')}}</option> 
                  <option value="perroom">{{__('Per Room')}}</option>
                </select>        
              </div> 
          
            
            
              <div id="PerRoomDiv" style="display:none;" > 
                  
                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                        </font>
                      </label>
                      <input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoom" required="true">                        
                    </div>
                    <div class="form-group col-md-8">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                        </font>
                      </label>
                      <textarea name="description_perroom" class="form-control" rows="2" cols="50"></textarea>
                    </div>
                    
                  </div>
                  <div class="row"> 
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                        </font>
                      </label>
                      <input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoom" required="true">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                        </font>
                      </label>
                      <input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoom" required="true">                        
                    </div>
                    <div class="form-group col-md-4">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                        </font>
                      </label>
                      <input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoom" value="1" min="1"  required="true">                        
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="form-group col-md-3">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                        </font>
                      </label>
                      <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoom" required="true">                        
                    </div>
                    <div class="form-group col-md-3">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                        </font>
                      </label>
                      <input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoom" required="true">                        
                    </div> 

                    <div class="form-group col-md-3">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                        </font>
                      </label>
                      <input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoom" required="true" value="1">                        
                    </div>
                  
                    <div class="form-group col-md-3">
                      <label class="control-label">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                        </font> 
                      </label>
                      <input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoom" required="true" value="0">                        
                    </div> 
                  </div>
                  <div class="row" id="maxChildAgeDivPerRoom"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                          </font>
                        </label>
                        <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoom" required="true" min="0" value="0">                        
                      </div> 
                  </div>
              </div>
             

 
              <div id="PerPersonDiv"  style="display:none;">  
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                      </font>
                    </label>
                    <input name="room_type" class="form-control valid" type="text" id="PriceRoomType" required="true">                        
                  </div>
                  <div class="form-group col-md-8">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                      </font>
                    </label>
                    <textarea name="description" class="form-control" rows="2" cols="50"></textarea>
                  </div>
                  
                </div>
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                      </font>
                    </label>
                    <input name="start_date" class="form-control valid" type="text" id="PriceStartDate" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                      </font>
                    </label>
                    <input name="end_date" class="form-control valid" type="text" id="PriceEndDate" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                      </font>
                    </label>
                    <input name="min_days" class="form-control valid" type="number" id="PriceMinDay" required="true" min="1" value="1">                        
                  </div>
                </div>

                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                      </font>
                    </label>
                    <input name="no_of_rooms" class="form-control valid" type="number" value="1" id="PriceNoOfRoom" required="true" min="1" value="1" >                        
                  </div> 

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                      </font>
                    </label>
                    <input name="adults" class="form-control valid" type="number" id="PriceAdult" required="true" min="1" value="1">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                      </font>
                    </label>
                    <input name="childs" class="form-control valid" type="number"  id="PriceChilds" required="true" min="0" value="0">                        
                  </div> 

                </div>

                <div class="row"> 
                  <div class="form-group col-md-4" id="maxChildAgePerPerson">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                      </font>
                    </label>
                    <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAge" required="true" min="0" value="0">                        
                  </div> 
                  
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">

                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
                      </font>
                    </label>
                    <input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdult" required="true" min="0" value="0">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
                      </font>
                    </label>
                  <input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChild" required="true" min="0" value="0">                        
                  </div>
                
                  
                </div>

                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoom" required="true" min="0" value="0">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoom" required="true" min="0" value="0">                        
                  </div>
                
                </div>

                <div class="row"> 
               
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Per Person/Night)'); ?>&euro;</font>
                      </font>
                    </label>
                    <input name="price1" class="form-control valid" type="number" min="1" id="Price1" required="true" >                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('2 Person/Night'); ?>&euro;</font>
                      </font>
                    </label>
                  <input name="price2" class="form-control valid" type="number" min="1" id="Price2" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <?php echo __('Family/Night'); ?>&euro;
                      </font>
                    </label>
                    <input name="price3" class="form-control valid" type="number" min="1" id="Price3" required="true">                        
                  </div>
                </div>
              </div>  

          </div>
          <div class="modal-footer" >
            <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>
            <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>

        </form>
        </div>
      </div>
  </div>

  <div class="modal fade" id="editPricePersonModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Edit Prices'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                {{csrf_field()}} 
               
               <!-- <input type="hidden" name="accomId" value="" id="PriceIdPerWeekEdit">  -->

               <input type="hidden" name="price_id" id="EditPriceIDPerson"> 
               <input type="hidden" name="id" id="EditRoomIDPerson"> 
               <input type="hidden" name="room_price_id" id="EditRoomPriceIDPerson"> 
               
               <input type="hidden" name="price_unit" id="priceUnitWeekEdit" value="{{$PRICEUNIT}}"> 
               
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
           

              <div id="PerPersonDiv">  
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                      </font>
                    </label>
                    <input name="room_type" class="form-control valid" type="text" id="PriceRoomTypeEdit" required="true">                        
                  </div>
                  <div class="form-group col-md-8">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                      </font>
                    </label>
                    <textarea name="description" class="form-control" rows="2" cols="50" id="PriceRoomDescriptionEdit"></textarea>
                  </div>
                  
                </div>
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                      </font>
                    </label>
                    <input name="start_date" class="form-control valid" type="text" id="PriceStartDateEdit" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                      </font>
                    </label>
                    <input name="end_date" class="form-control valid" type="text" id="PriceEndDateEdit" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                      </font>
                    </label>
                    <input name="min_days" class="form-control valid" type="number" id="PriceMinDayEdit" required="true" min="1" value="1">                        
                  </div>
                </div>

                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                      </font>
                    </label>
                    <input name="no_of_rooms" class="form-control valid" type="number"  id="PriceNoOfRoomEdit" required="true" min="1" value="1" >                        
                  </div> 

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                      </font>
                    </label>
                    <input name="adults" class="form-control valid" type="number" id="PriceAdultEdit" required="true" min="1" value="1">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                      </font>
                    </label>
                    <input name="childs" class="form-control valid" type="number"  id="PriceChildsEdit" required="true" min="0" value="0">                        
                  </div> 

                </div>

                <div class="row"> 
                  <div class="form-group col-md-4" id="maxChildAgePerPerson">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                      </font>
                    </label>
                    <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAgeEdit" required="true" min="0" value="0">                        
                  </div> 
                  
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">

                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
                      </font>
                    </label>
                    <input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdultEdit" required="true" min="0" value="0">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
                      </font>
                    </label>
                  <input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChildEdit" required="true" min="0" value="0">                        
                  </div>
                
                  
                </div>

                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoomEdit" required="true" min="0" value="0">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoomEdit" required="true" min="0" value="0">                        
                  </div>
                
                </div>

                <div class="row"> 
               
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Per Person/Night)'); ?>&euro;</font>
                      </font>
                    </label>
                    <input name="price1" class="form-control valid" type="number" min="1" id="Price1Edit" required="true" >                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('2 Person/Night'); ?>&euro;</font>
                      </font>
                    </label>
                  <input name="price2" class="form-control valid" type="number" min="1" id="Price2Edit" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <?php echo __('Family/Night'); ?>&euro;
                      </font>
                    </label>
                    <input name="price3" class="form-control valid" type="number" min="1" id="Price3Edit" required="true">                        
                  </div>
                </div>
              </div>   



            </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>

              <a type="button" id="deletePricePerson" onclick="confirmDel('perperson');"  style="float:right;" class="btn btn-danger effect pull-left">
                <?php echo __('Delete'); ?></a>

             
            </div>
            </form>
         
          </div>
        </div>
  </div> 

  <div class="modal fade" id="editPriceModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Edit Prices Per Week'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>  
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                {{csrf_field()}}
               
               <input type="hidden" name="id" id="EditPriceIDWeek">  
               
               <input type="hidden" name="price_unit" id="priceUnitWeekEdit" value="{{$PRICEUNIT}}"> 
               
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
              <div class="row"> 
                <div class="form-group col-md-4">
                  <label class="control-label"> 
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                    </font>
                  </label>
                  <input name="start_date" class="form-control valid" type="text" id="PriceStartDatePerWeekEdit"  required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                    </font>
                  </label>
                  <input name="end_date" class="form-control valid" type="text" id="PriceEndDatePerWeekEdit" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                    </font>
                  </label>
                  <input name="min_days" class="form-control valid" type="number" value="7" id="PriceMinDayEdit" required="true" min="1">                        
                </div>
              </div>
              
              <div class="row"> 
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Maximum Persons'); ?></font>
                    </font>
                  </label>
                  <input name="max_persons" class="form-control valid" type="number" value="0"  min="0" id="PriceMaxPersonEdit" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                    </font>
                  </label>
                  <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountEdit" required="true">                        
                </div>
                <div class="form-group col-md-4">
                  <label class="control-label">
                    <font style="vertical-align: inherit;">
                      <?php echo __('Extra Price (Per Person)'); ?>
                    </font>
                  </label>
                  <input name="extra_price_per_person" class="form-control valid" type="number" min="0" id="PriceExtraPricePerPersonEdit" required="true">                        
                </div>
              </div>
              </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>

              <button type="button" id="deletePriceWeek" onclick="confirmDel('perweek');"  style="float:right;" class="btn btn-danger effect pull-left">
                <?php echo __('Delete'); ?></button>

             
            </div>
            </form>
         
          </div>
        </div>
  </div> 
 
  <div class="modal fade" id="editPriceModalPerRoom">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Edit Prices'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>  
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                {{csrf_field()}}
               
               <input type="hidden" name="id" id="EditPriceIDPerRoom">  
               <input type="hidden" name="room_id" id="EditRoomIDPerRoom"> 
               <input type="hidden" name="room_price_id" id="EditRoomPriceIDPerRoom"> 
               
               <input type="hidden" name="price_unit" id="priceUnitWeekEdit" value="{{$PRICEUNIT}}"> 
               
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
            
                <div id="PerRoomDiv"> 
                    
                     
                    <div class="row"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                          </font>
                        </label>
                        <input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoomEdit" required="true">                        
                      </div>
                      <div class="form-group col-md-8">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                          </font>
                        </label>
                        <textarea name="description_perroom" class="form-control" rows="2" cols="50" id="PriceRoomDescriptionPerRoomEdit"></textarea>
                      </div>
                      
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                          </font>
                        </label>
                        <input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoomEdit" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                          </font>
                        </label>
                        <input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoomEdit" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                          </font>
                        </label>
                        <input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoomEdit" value="1" min="1"  required="true">                        
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                          </font>
                        </label>
                        <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoomEdit" required="true">                        
                      </div>
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                          </font>
                        </label>
                        <input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoomEdit" required="true">                        
                      </div> 

                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                          </font>
                        </label>
                        <input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoomEdit" required="true" value="1">                        
                      </div>
                    
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                          </font> 
                        </label>
                        <input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoomEdit" required="true" value="0">                        
                      </div> 
                    </div>
                    <div class="row" id="maxChildAgeDivPerRoom"> 
                        <div class="form-group col-md-4">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                            </font>
                          </label>
                          <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoom" required="true" min="0" value="0">                        
                        </div> 
                    </div>
                </div>



            </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>

              <a type="button" id="deletePriceRoom" onclick="confirmDel('perroom');"  style="float:right;" class="btn btn-danger effect pull-left">
                <?php echo __('Delete'); ?></a>

             
            </div>
            </form>
         
          </div>
        </div>
  </div> 

  <div class="modal fade" id="editExtraCharge">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Add Prices Per Week'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
            <div class="modal-body">   
                <form action="/accomodations/edit_extra_charge" method="post" accept-charset="utf-8"> 
                      {{csrf_field()}}
 
                      <div class="row"> 
                        <div class="form-group col-md-2">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                            </font>
                          </label>
                          <input type="hidden" name="id" id="editExtraChargeID"> 
                          <input name="amount" class="form-control valid" type="number" id="ExtraPriceAmountEdit" required="true">                        
                        </div>
                        <div class="form-group col-md-2">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Cost Type'); ?></font>
                            </font>
                          </label>
                          <select name="is_optional" class="form-control" id="ExtraIsOptional"> 
                              <option value="1">{{__('Optional')}}</option>
                              <option value="0">{{__('Required')}}</option>
                          </select>
                        </div>
                        <div class="form-group col-md-4">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                            </font>
                          </label>
                          <textarea name="description" class="form-control" rows="3" cols="30" required="true" id="ExtraDescriptionEdit"></textarea>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="radio-inline">
                          <input type="radio" name="charge_type_edit" value="perday">{{__('Per Day') }}
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="charge_type_edit" value="fix">{{__('Fix') }} 
                        </label>
                        <label class="radio-inline">
                          <input type="checkbox" name="is_per_person" id="PerPersonCheckBox" value="1">{{__('Per Person') }}   
                        </label>                                              
                        </div>
                      </div>
                      <div class="form-action"> 
                           <button type="submit"  class="btn btn-primary"><?php echo __('Update') ?></button>
                      </div> 
                 </form>
            
         
          </div>
        </div>
  </div>
 </div>

  <div class="modal fade" id="editPriceModalTable">
          <div class="modal-dialog" role="document">
            <div class="modal-content"> 
              <div class="modal-header">
                <h5 class="modal-title"><?php echo __('Edit Prices Per Week'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>  
              <div class="modal-body" id="homeModalBodyPerWeek">   
    
                <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                  {{csrf_field()}}
                 
                 <input type="hidden" name="id" id="EditPriceIDWeekTable">  
                 
                 <input type="hidden" name="price_unit" id="priceUnitWeekEditTable" value="{{$PRICEUNIT}}"> 
                 
                <h4><?php echo __('Change Day'); ?> </h4>              
                <div class="form-group"> 
                  <label class="radio-inline">
                    <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                  </label>  
                  <br>        
                  <span id="changeDayErrPerWeek" style="color:red;"> </span>
                </div> 
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label"> 
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                      </font>
                    </label>
                    <input name="start_date" class="form-control valid" type="text" id="PriceStartDatePerWeekEditTable"  required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                      </font>
                    </label>
                    <input name="end_date" class="form-control valid" type="text" id="PriceEndDatePerWeekEditTable" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                      </font>
                    </label>
                    <input name="min_days" class="form-control valid" type="number" value="7" id="PriceMinDayEditTable" required="true" min="1">                        
                  </div>
                </div>
                
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Maximum Persons'); ?></font>
                      </font>
                    </label>
                    <input name="max_persons" class="form-control valid" type="number" value="1"  min="1" id="PriceMaxPersonEditTable" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                      </font>
                    </label>
                    <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountEditTable" required="true">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <?php echo __('Extra Price (Per Person)'); ?>
                      </font>
                    </label>
                    <input name="extra_price_per_person" class="form-control valid" type="number" min="0" value="0" id="PriceExtraPricePerPersonEditTable" required="true">                        
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>
              </div> 
              </form>
           
            </div>
          </div>
    </div> 
  </div> 

  <div class="modal fade" id="editPriceModalPerRoomTable">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Edit Prices'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>  
            <div class="modal-body">   
  
              <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                {{csrf_field()}}
               
               <input type="hidden" name="id" id="EditPriceIDPerRoom">  
               <input type="hidden" name="room_id" id="EditRoomIDPerRoom"> 
               <input type="hidden" name="room_price_id" id="EditRoomPriceIDPerRoom"> 
               
               <input type="hidden" name="price_unit" id="priceUnitWeekEdit" value="{{$PRICEUNIT}}"> 
               
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
            
                <div id="PerRoomDiv"> 
                    <div class="row">  
                      <div class="form-group col-md-4"> 
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                          </font>
                        </label>
                        <input type="hidden" id="PriceIdTable" name="id" >
                        <input type="hidden" id="PriceRoomIdTable" name="room_id" >
                        <input type="hidden" id="RoomPriceIdTable" name="room_price_id" >
                        <input name="room_type_perroom" class="form-control valid" type="text" id="PriceRoomTypePerRoomEditTable" required="true">                        
                      </div>
                      <div class="form-group col-md-8">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                          </font>
                        </label>
                        <textarea name="description_perroom" class="form-control" rows="2" cols="50" id="PriceRoomDescriptionPerRoomEditTable"></textarea>
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
                          </font>
                        </label>
                        <input name="start_date_perroom" class="form-control valid" type="text" id="PriceStartDatePerRoomEditTable" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
                          </font>
                        </label>
                        <input name="end_date_perroom" class="form-control valid" type="text" id="PriceEndDatePerRoomEditTable" required="true">                        
                      </div>
                      <div class="form-group col-md-4">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                          </font>
                        </label>
                        <input name="min_days_perroom" class="form-control valid" type="number" id="PriceMinDayPerRoomEditTable" value="1" min="1"  required="true">                        
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Amount'); ?></font>
                          </font>
                        </label>
                        <input name="amount" class="form-control valid" type="number" min="1" id="PriceAmountPerRoomEditTable" required="true">                        
                      </div>
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                          </font>
                        </label>
                        <input name="no_of_rooms" class="form-control valid" type="number" value="1" min="1" id="PriceNoOfRoomPerRoomEditTable" required="true">                        
                      </div> 

                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                          </font>
                        </label>
                        <input name="adults" class="form-control valid" type="number" min="1" id="PriceAdultPerRoomEditTable" required="true" value="1">                        
                      </div>
                    
                      <div class="form-group col-md-3">
                        <label class="control-label">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                          </font> 
                        </label>
                        <input name="childs" class="form-control valid" type="number" min="0" id="PriceChildsPerRoomEditTable" required="true" value="0">                        
                      </div> 
                    </div>
                    <div class="row" id="maxChildAgeDivPerRoom"> 
                        <div class="form-group col-md-4">
                          <label class="control-label">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                            </font>
                          </label>
                          <input name="max_child_age" class="form-control valid" type="number"  id="PriceMaxChildAgePerRoomTable" required="true" min="0" value="0">                        
                        </div> 
                    </div>
                </div>



            </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>
            </div>
            </form>
         
          </div>
        </div>
  </div> 

  <div class="modal fade" id="editPriceModalPerPersonTable"> 
        <div class="modal-dialog" role="document">
          <div class="modal-content"> 
            <div class="modal-header">
              <h5 class="modal-title"><?php echo __('Edit Prices'); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
            <div class="modal-body" id="homeModalBodyPerWeek">   
  
              <form action="/accomodations/edit_accom_price/{{$accom}}" method="post" accept-charset="utf-8" >   
                {{csrf_field()}} 
                
               <input type="hidden" name="price_id" id="PriceIdEditTable"> 
               <input type="hidden" name="id" id="PriceRoomIdEditTable"> 
               <input type="hidden" name="room_price_id" id="PriceRoomPriceIdEditTable"> 
               
               <input type="hidden" name="price_unit" id="priceUnitWeekEdit" value="{{$PRICEUNIT}}"> 
               
              <h4><?php echo __('Change Day'); ?> </h4>              
              <div class="form-group"> 
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="saturday" <?php echo $satChecked; ?> > {{__('Saturday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="sunday" <?php echo $sunChecked; ?> >{{__('Sunday') }}
                </label>
                <label class="radio-inline">
                  <input type="radio" name="changeday" value="anyday" <?php echo $anyChecked; ?> >{{__('Anyday') }} 
                </label>  
                <br>        
                <span id="changeDayErrPerWeek" style="color:red;"> </span>
              </div> 
           

              <div id="PerPersonDiv">  
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Room Type'); ?></font>
                      </font>
                    </label>
                    <input name="room_type" class="form-control valid" type="text" id="PriceRoomTypeEditTable" required="true">                        
                  </div>
                  <div class="form-group col-md-8">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
                      </font>
                    </label>
                    <textarea name="description" class="form-control" rows="2" cols="50" id="PriceRoomDescriptionEditTable"></textarea>
                  </div>
                  
                </div>
                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('No.Of Room(s)'); ?></font>
                      </font>
                    </label>
                    <input name="no_of_rooms" class="form-control valid" type="number"  id="PriceNoOfRoomEditTable" required="true" min="1" value="1" >                        
                  </div> 

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Adults'); ?></font>
                      </font>
                    </label>
                    <input name="adults" class="form-control valid" type="number" id="PriceAdultEditTable" required="true" min="1" value="1">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Childs'); ?></font>
                      </font>
                    </label>
                    <input name="childs" class="form-control valid" type="number"  id="PriceChildsEditTable" required="true" min="0" value="0">                        
                  </div> 

                </div>

                <div class="row"> 
                  <div class="form-group col-md-4" id="maxChildAgePerPerson">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max Child Age'); ?></font>
                      </font>
                    </label>
                    <input name="max_child_age" class="form-control valid" type="number" id="PriceMaxChildAgeEditTable" required="true" min="0" value="0">                        
                  </div> 
                  
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">

                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Adult)'); ?>&euro;</font>
                      </font>
                    </label>
                    <input name="extra_price_adult" class="form-control valid" type="number" id="PriceExtraPriceAdultEditTable" required="true" min="0" value="0">                        
                  </div>

                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Extra Price (Child)'); ?>&euro;</font>
                      </font>
                    </label>
                  <input name="extra_price_child" class="form-control valid" type="number" id="PriceExtraPriceChildEditTable" required="true" min="0" value="0">                        
                  </div>
                
                  
                </div>

                <div class="row"> 
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Persons Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_person_per_room" class="form-control valid" type="number" id="PriceMaxPersonPerRoomEditTable" required="true" min="0" value="0">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Max. Adults Per Room'); ?></font>
                      </font>
                    </label>
                    <input name="max_adults_per_room" class="form-control valid" type="number" id="PriceMaxAdultsPerRoomEditTable" required="true" min="0" value="0">                        
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;"><?php echo __('Minimum Day(s)'); ?></font>
                      </font>
                    </label>
                    <input name="min_days" class="form-control valid" type="number" id="PriceMinDayEditTable" required="true" min="1" value="1">                        
                  </div>
                
                </div>

                <div class="row" id="appendPrices"> 
                  
                 
                </div>
              </div>   

            </div>
            <div class="modal-footer">
              <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>             
            </div>
            </form>
         
          </div>
        </div>
  </div> 
 

  <script type="text/javascript">

      function confirmDel(type) { 
        if(window.confirm("<?php echo __('Are you sure?') ?>")) {  
          if(type=='perweek') {
            window.location.href='/accomodations/delete_price/perweek/'+$('#deletePriceWeek').attr('price_id');
          } else if(type=='perperson') {
            window.location.href='/accomodations/delete_price/perperson/'+$('#deletePricePerson').attr('price_id')+'/'+$('#deletePricePerson').attr('room_id')+'/'+$('#deletePricePerson').attr('room_price_id'); 
          } else {
            window.location.href='/accomodations/delete_price/perroom/'+$('#deletePriceRoom').attr('price_id')+'/'+$('#deletePriceRoom').attr('room_id')+'/'+$('#deletePriceRoom').attr('room_price_id'); 
          } 
        } else { 
          return false;
        }  
      } 


     

      function confirmDelTable(id,type,priceID,rPriceId) { 
          if(window.confirm("<?php echo __('Are you sure?') ?>")) {  
            if(type=='perweek') {
              window.location.href='/accomodations/delete_price/perweek/'+id;
            } else if(type=='perperson') {
              window.location.href='/accomodations/delete_price/perperson/'+id+'/'+priceID+'/'+rPriceId; 
            } else {
              window.location.href='/accomodations/delete_price/perperson/'+id+'/'+priceID+'/'+rPriceId; 
            } 
          } else { 
            return false;
          }
      }

      function deleteExtraCharge(id) {
        if(window.confirm("<?php echo __('Are you sure?') ?>")) {  
          window.location.href='/accomodations/delete_extra_charge/'+id;
        } else {
          return false;
        }
      }

      function confirmSwitchAccom(id) {
        if(window.confirm("<?php echo __('By switchning price unit,all your prices will be deleted,are you sure you want to proceed?') ?>")) {  
          window.location.href='/accomodations/switch_prices/'+id;
        } else {
          return false;
        }
      } 
  </script>
	<script type="text/javascript" src="/js/moment.js"></script> 
	<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/js/custom/accomodation_prices.js"></script>  
@endsection