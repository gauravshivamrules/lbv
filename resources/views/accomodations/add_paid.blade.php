@extends('layouts.back') 
@section('title',__('Add New Accommodation')) 
@section('content')

<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
    20%
  </div>
</div> 
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li>
@endforeach  
</ul>
<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/accomodations/add_paid" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add New Accommodation'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input type="hidden" name="label_id" value="{{$label}}">
								<input type="hidden" name="payment_type" value="{{$pay}}"> 
								<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{old('name')}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',$accom,null,['class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
							<div class="col-lg-8">
								<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{old('email')}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
							<div class="col-lg-8">
								<input name="website" class="form-control"  type="url" id="AccomodationWebsite" value="{{old('website')}}" placeholder="{{__('https://www.mywebsite.com')}}">                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" value="{{old('number_of_persons')}}" required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								<textarea class="form-control" name="description"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Next'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
@endsection