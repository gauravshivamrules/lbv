@extends('layouts.back') 
@section('title',__('Update Accommodation')) 
@section('content')

<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
    20%
  </div>
</div> 
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component">   
 

	<div class="row"> 
		
		<div id="progressbar"></div> 
		<legend><?php echo __('Update Accommodation'); ?></legend> 
		<form class="form-horizontal" action="/accomodations/edit/{{$accom->id}}" method="post"> 
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>
				<div class="col-md-6">
					
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
						<div class="col-lg-8">
							<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{$accom->name}}"   required>                         
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
						<div class="col-lg-8">
							{!! Form::select('type_id',$accomTypes,$accom->accomodation_type_id,['class'=>'form-control','required'=>true] ) !!} 
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
						<div class="col-lg-8">
							<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{$accom->email}}" required>                         
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
						<div class="col-lg-8">
							<input name="website" class="form-control"  type="text" id="AccomodationWebsite" value="{{$accom->website}}" placeholder="{{__('https://www.mywebsite.com')}}">                    
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('YouTube Url'); ?> </label>
						<div class="col-lg-8">
							<input name="youtube_url" class="form-control"  type="url" id="AccomodationYoutubeUrl" value="{{old('youtube_url')}}" placeholder="{{__('https://www.youtube.com/somevideo')}}">                          
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
						<div class="col-lg-8">
							<textarea class="form-control" name="description">{{$accom->description}}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
						<div class="col-lg-8">
							<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" value="{{$accom->number_of_persons}}"  required>
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Bathrooms'); ?> </label>
						<div class="col-lg-8">
							<input name="bathrooms" class="form-control"  type="number" id="AccomodationBathrooms"  min="0" max="50" value="{{$accom->bathrooms}}"   required>                          
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-lg-4 control-label"><?php echo __('Bedrooms'); ?></label>
						<div class="col-lg-8">
							<input name="bedrooms" class="form-control"  type="number" id="AccomodationBedRooms"  min="0" max="50" value="{{$accom->bedrooms}}"  required>
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Single Beds'); ?> </label>
						<div class="col-lg-8">
							<input name="single_beds" class="form-control"  type="number" id="AccomodationSingleBeds"   min="0" max="50" value="{{$accom->single_beds}}" required>                          
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-lg-4 control-label"><?php echo __('Double Beds'); ?> </label>
						<div class="col-lg-8">
							<input name="double_beds" class="form-control"  type="number" id="AccomodationDoubleBeds"  min="0" max="50" value="{{$accom->double_beds}}" required>                          
						</div>
					</div> 
				</div>   
			</fieldset>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button type="submit" class="btn btn-primary" style="float:right;" id="cont"><?php echo __('Next'); ?></button> 
				</div>
			</div>
		</form>
	</div>




		
</div> 
@endsection