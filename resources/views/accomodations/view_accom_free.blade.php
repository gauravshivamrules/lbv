@extends('layouts.back') 
@section('title',$accom->name ) 
@section('content')

<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-6">
				<form class="form-horizontal"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Current Information'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{$oldInfo->name}}" readonly="true"  required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',$accomTypes,$oldInfo->accomodation_type_id,['class'=>'form-control','required'=>true,'disabled'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
							<div class="col-lg-8">
								<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{$oldInfo->email}}" readonly="true" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
							<div class="col-lg-8">
								<input name="website" class="form-control"  type="url" id="AccomodationWebsite" value="{{$oldInfo->website}}" readonly="true" placeholder="{{__('https://www.mywebsite.com')}}">                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" readonly="true" value="{{$oldInfo->number_of_persons}}" required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Bathrooms'); ?> </label>
							<div class="col-lg-8">
								<input name="bathrooms" class="form-control"  type="number" id="AccomodationBathrooms" readonly="true"  min="0" max="50" value="{{$oldInfo->bathrooms}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Bedrooms'); ?></label>
							<div class="col-lg-8">
								<input name="bedrooms" class="form-control"  type="number" id="AccomodationBedRooms"  readonly="true" min="0" max="50" value="{{$oldInfo->bedrooms}}"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Single Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="single_beds" class="form-control"  type="number" id="AccomodationSingleBeds" readonly="true"   min="0" max="50" value="{{$oldInfo->single_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Double Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="double_beds" class="form-control"  type="number" id="AccomodationDoubleBeds"  readonly="true" min="0" max="50" value="{{$oldInfo->double_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								{{$oldInfo->description}}
							</div>
						</div>
					</fieldset>
				</form>
			</div>   
			<div class="col-md-6">
			

				<form class="form-horizontal" action="/accomodations/approve-edit-accomodation/{{$accom->accom_id}}" method="post" id="editForm">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('New Information'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{$accom->name}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',$accomTypes,$accom->accomodation_type_id,['class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
							<div class="col-lg-8">
								<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{$accom->email}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
							<div class="col-lg-8">
								<input name="website" class="form-control"  type="url" id="AccomodationWebsite" value="{{$accom->website}}" placeholder="{{__('https://www.mywebsite.com')}}">                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" value="{{$accom->number_of_persons}}" required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Bathrooms'); ?> </label>
							<div class="col-lg-8">
								<input name="bathrooms" class="form-control"  type="number" id="AccomodationBathrooms" readonly="true"  min="0" max="50" value="{{$accom->bathrooms}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Bedrooms'); ?></label>
							<div class="col-lg-8">
								<input name="bedrooms" class="form-control"  type="number" id="AccomodationBedRooms"  readonly="true" min="0" max="50" value="{{$accom->bedrooms}}"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Single Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="single_beds" class="form-control"  type="number" id="AccomodationSingleBeds" readonly="true"   min="0" max="50" value="{{$accom->single_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Double Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="double_beds" class="form-control"  type="number" id="AccomodationDoubleBeds"  readonly="true" min="0" max="50" value="{{$accom->double_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								<textarea class="form-control" name="description">{{$accom->description}}</textarea>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit"  class="btn btn-primary"><?php echo __('Update & Approve'); ?></button> &nbsp;
								<a href="javascript:;" onclick="rejectAccom({{$accom->accom_id}})"  class="btn btn-danger"><?php echo __('Reject'); ?></a>  
							</div>
						</div>
						
					</fieldset>
				</form>
				

			</div>
		</div>
</div>  
	
	<script type="text/javascript">
	    function approveAccom(url) {
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/approve-edit-accomodation/'+url; 
	    	} else { 
	    		return false;
	    	} 
	    }
	    function rejectAccom(id) {
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/reject-edit-accomodation/'+id;  
	    	} else { 
	    		return false;
	    	} 
	    }

	    
	</script> 

@endsection