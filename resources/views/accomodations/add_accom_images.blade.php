@extends('layouts.back') 
@section('title',__('Add Images')) 
@section('content')

 <div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">
    60%
  </div>
</div>
 <ul>     
 @foreach($errors->all() as $e)
 	<li class="error"> {{ $e }} </li>
 @endforeach 
 </ul>
 <div class="well well bs-component">   
		<div class="row"> 
			<!-- <div class="col-md-1"></div>    -->
			<div class="col-md-12">
				<form class="dropzone"   id="image-upload" action="/accomodations/add_accom_images/{{$accom}}" method="post" enctype="multipart/form-data"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add Accommodation Images'); ?></legend>  
						<a href="/accomodations/add_accom_location/{{$accom}}"  class="btn btn-danger" style="float:right;"><?php echo __('Next') ?></a><br> 
					</fieldset>
				</form>
				<br>   
			</div> 
		</div>
		<div class="row"> 
			<br>

			@if($galleryImages)
				@if($galleryImages->count()>0)
					<h5>{{__('You can drag and drop images to set display order')}} </h5>	<br>	
				@endif
			@endif
			<div id="sortable" class="col-sm-12">

				@foreach($galleryImages as $i)
				<div  id="{{$i->id}}_{{$i->image}}" class="col-md-2 delIcon">
					<?php if($featuredImage) { ?>
					<?php if($featuredImage->featured_image==$i->image) { ?>
					<span class="notify-badge2">{{__('Featured')}}</span>
					<?php } else { ?>
					<a href="javascript:;" onclick="makeFeatured({{$accom}},'{{$i->image}}')"> <span class="notify-badge">{{__('Make Featured')}}</span></a>
					<?php } ?>
					<?php } else { ?>
					<a href="javascript:;" onclick="makeFeatured({{$accom}},'{{$i->image}}')"> 
						<span class="notify-badge">
							{{__('Make Featured')}}
						</span></a>
						<?php } ?>

						<img src="/images/gallery/{{$accom}}/{{$i->thumb_image}}" class="img-thumbnail" alt="Cinque Terre" width="300" height="100"> 

						<a onclick="deleteImage({{$i->id}})" href="javascript:;">
							<i class="glyphicon glyphicon-remove-sign" style="top:-80px;padding-right: 10px;margin-left: -10px;"></i>
						</a>
					</div>
					@endforeach
				</div>
			</div>
	</div>
	<link rel="stylesheet" type="text/css" href="/css/dropzone.css"> 
	<script type="text/javascript" src="/js/dropzone.js"></script>
	<script type="text/javascript" src="/js/jquery.ui.touch-punch.min.js"></script> 
	<script type="text/javascript">
	    jQuery(document).ready(function() {


	    	$('#sortable').sortable({
	    	  connectWith: '#sortable',
	    	  cursor: 'move',
	    	  // axis: "x",  
	    	  update: function(event, ui) {
	    	    var changedList = this.id;
	    	    var order = $(this).sortable('toArray');
	    	    var positions = order.join(';');
	    	    var arr={id:changedList,positions: positions};
	    	     $.ajax({
	    	     	url: '/accomodations/sort-images',
	    	       	data: arr,
	    	       	type: 'POST',
	    	       	dataType:'json',
	    	       	success:function(r) {
	    	       		if(r.success) {
	    	       			alert('<?php echo __("Images sorted successfully") ?>'); 
	    	       		}
	    	       	}
	    	    });
	    	  }
	    	});

	    	$('#sortable').on('sortupdate',function(){
	    	   console.log('update called'); 
	    	});
	    	
	        Dropzone.options.imageUpload = { 
	        	paramName :'file', 
	            maxFilesize  : 1,
	            acceptedFiles : "image/jpeg,image/png", 
	        };
	    });
	    var accom=<?php echo $accom ?>;  
	    function deleteImage(image) {
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/delete-image/'+image+'/'+accom; 
	    	} else { 
	    		return false;
	    	} 
	    } 
	    function makeFeatured(accom,featured) { 
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/make-featured/'+accom+'/'+featured; 
	    	} else { 
	    		return false;
	    	} 	
	    }
	</script> 

@endsection