@extends('layouts.back')
@section('title',_('My Accomodations'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('My Accomodations'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="accomodationsTable">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>     
						<th><?php echo __('Email'); ?></th>
						<th><?php echo __('Label'); ?></th>
						<th><?php echo __('Payment Type'); ?></th>
						<th><?php echo __('Approved'); ?></th>
						<th><?php echo __('Completed'); ?></th>
						<th><?php echo __('Created On'); ?></th>
						<th><?php echo __('Actions'); ?></th> 
					</tr>
				</thead> 
			</table>
			</div>
	</div> 
		<script>
			$(function() {
			    $('#accomodationsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/accomodations/get_my_accomodations",    
			        columns: [
			            { data: 'name', name: 'name' },
			            { data: 'email', name: 'email' }, 
			            { data: 'label_id', name: 'label_id' }, 
			            { data: 'payment_type', name: 'payment_type' }, 
			            { data: 'approved', name: 'approved' }, 
			            { data: 'completed', name: 'completed' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});
			function confirmDelete(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/accomodations/delete/'+id; 
				} else { 
					return false;
				} 
			} 
		</script>
@endsection