@extends('layouts.back') 
@section('title',__('Add New Location')) 
@section('content')
<ul>    
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li>
@endforeach 
</ul> 
 <div class="well well bs-component"> 
 <div class="panel-heading"> 
 	<h2> {{__('Add New Location') }} </h2>  
 </div>  
		<div class="row"> 
			<div class="col-md-8">  
					<ul class="nav nav-tabs">  
						<li class="active"> 
							<a data-toggle="tab" href="#showCountry"> {{__('Country')}} </a>
						</li>
						<li>
							<a data-toggle="tab" href="#showRegion"> {{__('Region')}} </a>  
						</li>
						<li>
							<a data-toggle="tab" href="#showDepartment"> {{__('Department')}} </a>  
						</li>
					</ul>


					<div class="tab-content"> 
						<div id="showCountry" class="tab-pane active"> 
								<form class="form-horizontal" action="/accomodations/add-new-locations" method="post">  
									<input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
										<fieldset>
											<div class="form-group">
												<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
												<div class="col-lg-8">
													<input name="country_id" class="form-control"  type="text" id="country_id"   required>     
												</div>
											</div> 
											<div class="form-group" id="regionDiv">
												<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
												<div class="col-lg-8">
													<input name="region_id" class="form-control"  type="text"   required>     
												</div>
											</div>
											<div class="form-group" id="departmentDiv">
												<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
												<div class="col-lg-8">
														<input name="department_id" class="form-control"  type="text" >     										
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-10 col-lg-offset-2">
													<button type="submit" class="btn btn-primary" id="cont">{{__('Add')}}</button> 
												</div>
											</div>
										</fieldset>
								</form>
						</div> 
						<div id="showRegion" class="tab-pane"> 
								<form class="form-horizontal" action="/accomodations/add-new-region" method="post">  
									<input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
										<fieldset>
											<div class="form-group">
												<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
												<div class="col-lg-8">
													{!! Form::select('country_id',$countries,null,['class'=>'form-control'] ) !!} 
												</div>
											</div> 
											<div class="form-group" id="regionDiv">
												<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
												<div class="col-lg-8">
													<input name="region_id" class="form-control"  type="text"   required>     
												</div>
											</div>
											<div class="form-group" id="departmentDiv">
												<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
												<div class="col-lg-8">
														<input name="department_id" class="form-control"  type="text">     										
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-10 col-lg-offset-2">
													<button type="submit" class="btn btn-primary" id="cont">{{__('Add')}}</button> 
												</div>
											</div>
										</fieldset>
								
								</form>
						</div>
						<div id="showDepartment" class="tab-pane"> 
								<form class="form-horizontal" action="/accomodations/add-new-department" method="post">  
									<input type="hidden" name="_token" value="{!! csrf_token() !!}">  
										<fieldset>
											<div class="form-group">
												<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
												<div class="col-lg-8">
														{!! Form::select('country_id',$countries,null,['class'=>'form-control','id'=>'countryId'] ) !!}  
												</div>
											</div> 
											<div class="form-group" id="regionDiv">
												<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
												<div class="col-lg-8">
													<select class="form-control" id="region_id" name="region_id" required>

													</select>
												</div>
											</div>
											<div class="form-group" id="departmentDiv">
												<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
												<div class="col-lg-8">
														<input name="department_id" class="form-control"  type="text" id="department_id"  required>     										
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-10 col-lg-offset-2">
													<button type="submit" class="btn btn-primary" id="cont">{{__('Add')}}</button> 
												</div>
											</div>
										</fieldset>
								</form>
						</div>
					</div>
			</div>   
		</div>
</div>  
<script>
	$(document).ready(function() {
		$('#countryId').change(function() {
			getRegions($(this).val());
		});
	});
	function getRegions(country){
	    if(country) {
	      $.ajax({
	        url:'/accomodations/get_regions?country_id='+country,
	        dataType:'json',
	        success:function(r) {
	          if(r) {
	          	console.log(r); 
	            $('#region_id').empty();
	            $('#department_id').empty();
	            $.each(r, function(key, value) {
	                $('#region_id').append($("<option/>", {
	                    value: value.id, 
	                    text: value.region_name
	                }));
	            });
	          }
	        }
	      }); 
	    }
	}
	function getDepartments(region){
	    if(region) {
	      $.ajax({
	        url:'/accomodations/get_departments?region_id='+region,
	        dataType:'json',
	        success:function(r) { 
	          if(r) {
	            $('#department_id').empty();
	            $.each(r, function(key, value) {
	                $('#department_id').append($("<option/>", {
	                    value: value.id,
	                    text: value.department_name
	                }));
	            });
	            findAddressOnMap();
	          }
	          $('#department_id').append($("<option/>", {
	              value: 'other', 
	              text: "Other"
	          }));
	         removeOthers();
	        }
	      }); 
	    }
	}
</script>
@endsection 