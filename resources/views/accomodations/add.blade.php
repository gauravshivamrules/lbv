@extends('layouts.back') 
@section('title',__('Add New Accommodation')) 
@section('content')
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form class="form-horizontal" action="javascript:;">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add New Accommodation'); ?></legend>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Accommodation Type'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('type_id',['3'=>__('For Rent'),'2'=>__('For Sale')],null,['id'=>'lableId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Subscription Type'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('role_id',['1'=>__('Paid'),'2'=>__('No Cure No Pay')],null,['id'=>'paymentTypeId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="button" class="btn btn-primary" id="cont"><?php echo __('Next'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {

			if($('#lableId').val()==2 ) {  
				// $.blockUI({ message: '<h1> Just a moment...</h1>' });
				$("#paymentTypeId option[value='2']").remove(); 
				// $.unblockUI();
			} 
			$('#lableId').change(function() { 
				if($('#lableId').val()==2 ) {
					$("#paymentTypeId option[value='2']").remove(); 		
				} else {
					$('#paymentTypeId').append($("<option></option>").attr("value",2).text('No Cure No Pay')); 
				} 
			});
			$('#cont').click(function() {
				$.blockUI({ css: { 
								border: 'none', 
								padding: '15px', 
								backgroundColor: '#000', 
								'-webkit-border-radius': '10px', 
								'-moz-border-radius': '10px', 
								opacity: .5, 
								color: '#fff' 
					} 
				});
				window.location.href = "/accomodations/add_accom?label="+$('#lableId').val()+'&pay='+$('#paymentTypeId').val(); 
			});
		}); 
	</script>
@endsection