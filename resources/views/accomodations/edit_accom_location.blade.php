@extends('layouts.back') 
@section('title',__('Update Location')) 
@section('content')
 <div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
    80%
  </div>
</div>
 <ul>    
 @foreach($errors->all() as $e)
 	<li class="error"> {{ $e }} </li>
 @endforeach 
 </ul> 
 <div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-6">  
		
					<form class="form-horizontal" action="/accomodations/edit-accom-location/{{$accom}}" method="post">  
						<input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
						<?php if($address) { ?>
						<div> 
							<input type="hidden" name="lat" id="lat" value="{{$address->lattitude}}"> 
							<input type="hidden" name="lng" id="lng" value="{{$address->longitude}}">  
							<fieldset>
								<legend>{{__('Update Accommodation Location')}}</legend>  
								<br>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
									<div class="col-lg-8">
										@if($IsApprovedCountry)
											{{$selected=''}}
											<select name="country_id" class="form-control" id="country_id">
											     @foreach($countries as $k=>$v)
											     @if($address->country_id==$k) 
											     	{{$selected="selected"}}
											     @else
											     	{{$selected=""}}
											     @endif
											     <option value="{{ $k }}" <?php echo $selected ?>>{{ $v }}</option>
											     @endforeach
											     <option value="other">{{ __('Other') }}</option>
											</select>
										@else
											<input type="hidden" name="new_country" value="1">
											<input name="country_id" class="form-control"  type="text" id="country_id"  readonly="true" value="{{$countryName}}">     
										@endif
										
									</div>
								</div>
								<div class="form-group" id="regionDiv">
									<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
									<div class="col-lg-8">
										@if($isApprovedRegion)
											{!! Form::select('region_id',$regions,$address->region_id,['class'=>'form-control','id'=>'region_id'] ) !!} 
										@else
											<input type="hidden" name="new_region" value="1">
											<input name="region_id" class="form-control"  type="text" id="region_id"  readonly="true" value="{{$regionName}}">     
										@endif
									</div>
								</div>
								<div class="form-group" id="departmentDiv">
									<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
									<div class="col-lg-8">
										@if($isApprovedDepartment) 
											{!! Form::select('department_id',$departments,$address->department_id,['class'=>'form-control','id'=>'department_id'] ) !!} 
										@else
											<input type="hidden" name="new_department" value="1">
											<input name="department_id" class="form-control"  type="text" id="department_id"  readonly="true" value="{{$departmentName}}">     
										@endif
										
									</div>
								</div>
								<div class="form-group" id="otherCountry" style="display:none;">
									<label for="name" class="col-lg-4 control-label"> {{__('Country')}}</label>
									<div class="col-lg-8">
										<input name="other_country" class="form-control"  type="text" id="other_country" placeholder="{{__('Enter country name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div>
								<div class="form-group" id="otherRegion" style="display:none;">
									<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
									<div class="col-lg-8">
										<input name="other_region" class="form-control"  type="text" id="other_region" placeholder="{{__('Enter region name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div>
								<div class="form-group" id="otherDepartment" style="display:none;">
									<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
									<div class="col-lg-8">
										<input name="other_department" class="form-control"  type="text" id="other_department" placeholder="{{__('Enter department name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div> 

								<div class="form-group"> 
									<label for="name" class="col-lg-4 control-label">{{__('Street')}} </label>
									<div class="col-lg-8">
										<input name="street" class="form-control"  type="text" id="street" required onchange="findAddressOnMap();" value="{{$address->street}}">                          
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('Street Number')}} </label>
									<div class="col-lg-8">
										<input name="street_number" class="form-control"  type="number" min="0"  id="street_number"  onchange="findAddressOnMap();" value="{{$address->street_number}}">
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('City/Town')}} </label>
									<div class="col-lg-8">
										<input name="city_town" class="form-control"  type="text" id="city_town"  required onchange="findAddressOnMap();" value="{{$address->city}}">
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('Postal Code')}}</label>
									<div class="col-lg-8">
										<input name="postal_code" class="form-control"  type="text" id="postal_code"  required onchange="findAddressOnMap();" value="{{$address->postal_code}}">
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-10 col-lg-offset-2">
										<button type="submit" class="btn btn-primary" id="cont">{{__('Next')}}</button> 
									</div>
								</div>
							</fieldset>
						</div>
						<?php } else { ?>
							<input type="hidden" name="lat" id="lat" > 
							<input type="hidden" name="lng" id="lng" >   
							<fieldset>
								<legend>{{__('Update Accommodation Location')}}</legend> 
								{{__('You can drag and drop the marker for accurate location')}}
								<br>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
									<div class="col-lg-8">
										{!! Form::select('country_id',$countries,null,['class'=>'form-control','id'=>'country_id'] ) !!}  
									</div>
								</div>
								<div class="form-group" id="regionDiv">
									<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
									<div class="col-lg-8">
										<select name="region_id" id="region_id" class="form-control" required><option>{{__('Region')}}</option></select>
									</div>
								</div>
								<div class="form-group"  id="departmentDiv">
									<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
									<div class="col-lg-8">
										<select name="department_id" id="department_id" class="form-control"><option>{{__('Department')}}</option></select> 
									</div>
								</div>
								<div class="form-group" id="otherCountry" style="display:none;">
									<label for="name" class="col-lg-4 control-label"> {{__('Country')}}</label>
									<div class="col-lg-8">
										<input name="other_country" class="form-control"  type="text" id="other_country" placeholder="{{__('Enter country name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div>
								<div class="form-group" id="otherRegion" style="display:none;">
									<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
									<div class="col-lg-8">
										<input name="other_region" class="form-control"  type="text" id="other_region" placeholder="{{__('Enter region name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div>
								<div class="form-group" id="otherDepartment" style="display:none;">
									<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
									<div class="col-lg-8">
										<input name="other_department" class="form-control"  type="text" id="other_department" placeholder="{{__('Enter department name')}}"  onchange="findAddressOnMap();">     
									</div>
								</div> 

								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('Street')}} </label>
									<div class="col-lg-8">
										<input name="street" class="form-control"  type="text" id="street" required onchange="findAddressOnMap();" >                          
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('Street Number')}} </label>
									<div class="col-lg-8">
										<input name="street_number" class="form-control"  type="number" min="0"  id="street_number"  onchange="findAddressOnMap();">
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('City/Town')}} </label>
									<div class="col-lg-8">
										<input name="city_town" class="form-control"  type="text" id="city_town"  required onchange="findAddressOnMap();" >
									</div>
								</div>
								<div class="form-group">
									<label for="name" class="col-lg-4 control-label">{{__('Postal Code')}}</label>
									<div class="col-lg-8">
										<input name="postal_code" class="form-control"  type="text" id="postal_code"  required onchange="findAddressOnMap();" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-10 col-lg-offset-2">
										<button type="submit" class="btn btn-primary" id="cont">{{__('Next')}}</button> 
									</div>
								</div>
							</fieldset>

						<?php } ?>


					</form>

			</div>   
			<div class="col-md-6">
				<div id="map" style="height:450px;"> </div>
			</div>
		</div>
</div>  
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw8GNwHQjD19djzT9xYf84mTz1vnrYV7Y&libraries=places"></script> 
	<script src="/js/custom/accomodation_location2.js"></script> 
	
@endsection 