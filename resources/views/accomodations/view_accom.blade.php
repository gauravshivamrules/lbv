@extends('layouts.back') 
@section('title',$accom->name ) 
@section('content')

 <div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				 
			</div>
			<div class="col-md-1"></div>
		</div> 
		<div class="row"> 
			<fieldset>
				<legend>{{$accom->name}} &nbsp;&nbsp; {{ $accom->accomodation_address ? ucfirst($accom->accomodation_address->city) : ''  }}  {{ $accom->accomodation_address ? ucfirst($accom->accomodation_address->country_id) : ''  }}</legend>  
				<!-- <a href="javascript:;"  class="btn btn-info" style="float:right;"><?php //echo __('Social Share Icons') ?></a> -->

				@if(Auth::user()->role_id==1)
					<a href="/accomodations"  class="btn btn-danger" style="float:right;"><?php echo __('Go Back') ?></a>
					@if(!$accom->approved)
					<a href="javascript:;" onclick="approveAccom({{$accom->id}})" class="btn btn-danger" style="float:right;"><?php echo __('Approve') ?></a>
					@endif
					@if($accom->deletion_request)
					<a href="javascript:;" onclick="deleteAccom({{$accom->id}})" class="btn btn-danger" style="float:right;"><?php echo __('Delete') ?></a>
					@endif
				@else 
					<a href="/accomodations/my-accomodations"  class="btn btn-danger" style="float:right;"><?php echo __('Go Back') ?></a>
				@endif


				<br>  
				@if( $accom->accomodation_address )
				<input type="hidden" id="lat" value="{{$accom->accomodation_address->lattitude ? $accom->accomodation_address->lattitude : ''}}">
				<input type="hidden" id="lng" value="{{$accom->accomodation_address->longitude ? $accom->accomodation_address->longitude : ''}}">
				@endif
			</fieldset>
			<div class="col-md-3">
				@if(Auth::user()->role_id==1)
				<h4> {{__('Advertiser Infomation')}}</h4>
				<p> {{$accom->user->first_name}}  {{$accom->user->last_name}}</p>
				<p> {{$accom->user->user_company}} </p>
				<p> {{$accom->user->street_number}} {{$accom->user->street}} </p>
				<p> {{$accom->user->city}}  {{$accom->user->country_id}} </p>  {{$accom->user->post_code}}
				<p> {{$accom->user->email}}</p>
				<p> {{$accom->user->phone_number}}</p>
				@endif
			</div>
			<div class="col-md-8">


				@if($accom->gallery_images)
				<div id="slider">
					<ul>
						@foreach($accom->gallery_images as $gi)
						<li>
							<img src="/images/gallery/{{$accom->id}}/{{$gi->slider_image}}" >
						</li>
						@endforeach
					</ul>
				</div>
				@endif

				<ul class="nav nav-tabs">
					@if($accom->description)
					<li class="active">
						<a data-toggle="tab" href="#showDescription">  {{__('Description')}} </a>
					</li>
					@endif
					@if($accom->accomodation_prices)
						@if($accom->accomodation_prices->count()>0)
						<li>
							<a data-toggle="tab" href="#showPrice">{{__('Prices')}}</a>
						</li>
						@endif
					@endif
					@if($searchTags) 
					<li>
						<a data-toggle="tab" href="#showAmnities"><?php echo __('Amnities') ?></a>
					</li>
					@endif
					@if($accom->accomodation_address)
						@if($accom->accomodation_address->count()>0)
						<li>
							<a data-toggle="tab" href="#showAddress"><?php echo __('Address') ?></a>
						</li>
						@endif
					@endif
				</ul> 

				<div class="tab-content">
					@if($accom->description)
					<div id="showDescription" class="tab-pane fade in active">
						<p>{{$accom->description}} </p>
					</div>
					@endif
					@if( $accom->accomodation_prices )
					<div id="showPrice" class="tab-pane fade"> 
						<?php if($accom->accomodation_prices->count()>0) { ?>
							<?php if($accom->label_id==3) { ?>
									<?php if($accom->accomodation_prices[0]->price_unit !='perweek') { ?>
										<?php if($accom->accomodation_prices[0]->price_unit =='perroom') { ?>
											<table class="table">
												<thead>
													<tr>
														<th>{{__('Period')}}</th>
														<th>{{__('Price')}} (&euro;)</th>
														<th>{{__('Unit')}}</th>
														<th>{{__('Min.Days')}}</th>
														<th>{{__('Change Day')}}</th>
														<th>{{__('Room Type')}}</th>
													</tr>
												</thead>
												<tbody>
													@foreach($accom->room as $r)
														@foreach($r->room_price as $rp)
													<tr>
														<td>{{__('From')}}  {{ date('d-m-Y',strtotime($rp->start_date)) }} {{__('To')}}  {{  date('d-m-Y',strtotime($rp->end_date)) }}</td>
														<td>{{$rp->price}}</td>
														<td>{{ucfirst($accom->accomodation_prices[0]->price_unit)}}</td>
														<td>{{$accom->accomodation_prices[0]->min_days}}</td>
														<td>{{ucfirst($accom->accomodation_prices[0]->changeday)}}</td>
														<td>{{$r->room_type}}</td>
													</tr>
														@endforeach
													@endforeach
												</tbody>
											</table>

										<?php } else { ?>
										 <table class="table">
										 	<thead>
										 		<tr>
										 			<th>{{__('Period')}}</th>
										 			<th>{{__('Price')}} (&euro;)</th>
										 			<th>{{__('Unit')}}</th>
										 			<th>{{__('Min.Days')}}</th>
										 			<th>{{__('Change Day')}}</th>
										 			<th>{{__('Room Type')}}</th>
										 		</tr>
										 	</thead>
										 	<tbody>
										 		@foreach($accom->room as $r)
										 		@foreach($r->room_price as $rp)
										 		<tr>
										 			<td>{{__('From')}}  {{ date('d-m-Y',strtotime($rp->start_date)) }} {{__('To')}}  {{  date('d-m-Y',strtotime($rp->end_date)) }}</td>
										 			<td>{{$rp->price1}}    {{$rp->price2}}   {{$rp->price1}}</td>
										 			<td>{{ucfirst($accom->accomodation_prices[0]->price_unit)}}</td>
										 			<td>{{$accom->accomodation_prices[0]->min_days}}</td>
										 			<td>{{ucfirst($accom->accomodation_prices[0]->changeday)}}</td>
										 			<td>{{$r->room_type}}</td>
										 		</tr>
										 		@endforeach
										 		@endforeach
										 	</tbody>
										 </table>
										<?php } ?>
									<?php } else { ?>
									<table class="table">
										<thead>
											<tr>
												<th>{{__('Period')}}</th>
												<th>{{__('Price')}} (&euro;)</th>
												<th>{{__('Unit')}}</th>
												<th>{{__('Min.Days')}}</th>
												<th>{{__('Change Day')}}</th>
												<th>{{__('Extra Price Per Person')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($accom->accomodation_prices as $rp)
											<tr>
												<td>{{__('From')}}  {{ date('d-m-Y',strtotime($rp->start_date)) }} {{__('To')}}  
													{{  date('d-m-Y',strtotime($rp->end_date)) }}
												</td>
												<td>{{$rp->amount}}</td>
												<td>{{ucfirst($accom->accomodation_prices[0]->price_unit)}}</td>
												<td>{{$accom->accomodation_prices[0]->min_days}}</td>
												<td>{{ucfirst($accom->accomodation_prices[0]->changeday)}}</td>
												<td>{{$rp->extra_price_per_person}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									<?php } ?>
							<?php } else { ?>
								<table class="table">
									<thead>
										<tr>
											<th>{{__('Price')}} (&euro;)</th>
										</tr>
									</thead>
									<tbody>
										@foreach($accom->accomodation_prices as $rp)
										<tr> 
											<td>{{$rp->amount}}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							<?php } ?>

							
						<?php } ?>

						@if($accom->extra_price)
							@if($accom->extra_price->count()>0)
							<h3> {{__('Extra Costs')}} </h3>
							<table class="table">
								<thead>
									<tr>
										<th>{{__('Description')}}</th>
										<th>{{__('Price')}} (&euro;)</th>
										<th>{{__('Optional')}}</th>
										<th>{{__('Type')}}</th>
										<th>{{__('Fix/Perperson')}}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($accom->extra_price as $e)
									<tr>
										<td> {{$e->description}} </td>
										<td> {{$e->amount}} </td>
										<td> {{$e->is_optional ? __('Yes'):__('No') }} </td>
										<td> {{ucfirst($e->charge_type)}} </td>
										<td> {{$e->is_per_person ? __('Per Person') : __('Fix') }} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						@endif
						@endif
					</div>
					@endif
					@if( $searchTags ) 
					<div id="showAmnities" class="tab-pane fade"> 
						@foreach($searchTags as $s)
							<input name="searchtag_id[]"  disabled="disabled" checked="checked"  class="styled" type="checkbox">{{$s->tag_name}}
						@endforeach
					</div> 
					@endif
					@if( $accom->accomodation_address )
					<div id="showAddress" class="tab-pane fade">
						<span><b>{{__('Website')}}<b>: {{$accom->website ? $accom->website  :'' }} </span><br>
						<span><b>{{__('Street')}}<b>: {{$accom->accomodation_address->street_number}} {{$accom->accomodation_address->street}} </span><br>
						<span><b>{{__('City')}}<b>: {{$accom->accomodation_address->city}}  </span><br>
						<span><b>{{__('Postal Code')}}<b>: {{$accom->accomodation_address->postal_code}}  </span><br>
						<span><b>{{__('Region')}}<b>:  {{$accom->accomodation_address->region_id}}</span><br>
						<span><b>{{__('Country')}}<b>: {{$accom->accomodation_address->country_id}} </span><br>
						<span><b>{{__('Department')}}<b>: {{$accom->accomodation_address->department_id}} </span><br>

						<div id="map" style="height:200px;weidth:200px;"> </div>
					</div>
					@endif
					
				</div>


			</div>
			<div class="col-md-2"> </div>
		</div>
	</div>
	<script type="text/javascript" src="/js/jquery.sudoSlider.min.js"></script>  	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw8GNwHQjD19djzT9xYf84mTz1vnrYV7Y&libraries=places"></script> 
	<script type="text/javascript">
	    jQuery(document).ready(function() {

	    	<?php if($accom->gallery_images->count()>0) { ?>
	    	$("#slider").sudoSlider({
                // Autoheight is on per default.
                 auto:true,
                 touch: true,
                 mouseTouch: true
            });
            <?php } ?>
            if($('#lat').val() && $('#lng').val() ) {
            	initAutocomplete();
            } 
	    });
	    function initAutocomplete() { 
	       var myLatLng = {lat: parseFloat($('#lat').val()), lng: parseFloat($('#lng').val()) };
	       map = new google.maps.Map(document.getElementById('map'), {    
	           zoom: 2,
	           center: myLatLng
	       });
	       var marker = new google.maps.Marker({
	            position: myLatLng,
	            map: map,
	        });
	    }
	    function approveAccom(id) {
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/approve-accomodation/'+id;
	    	} else { 
	    		return false;
	    	} 
	    }
	    function deleteAccom(id) {
	    	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
	    		window.location.href='/accomodations/delete-accomodation/'+id;  
	    	} else { 
	    		return false;
	    	} 
	    }

	    
	</script> 

@endsection