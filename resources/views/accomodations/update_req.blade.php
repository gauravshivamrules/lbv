@extends('layouts.back')
@section('title',_('All Update Requests'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('All Un-approved Update Requests'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="accomodationsTable">
				<thead>
					<tr>
						<th><?php echo __('Accomodation'); ?></th>     
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Created On'); ?></th>
						<th><?php echo __('Actions'); ?></th> 
					</tr>
				</thead> 
			</table>
			</div>
	</div> 
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
		<script>
			$(function() {
			    $('#accomodationsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/accomodations/view-update-ajax",    
			        columns: [
			            { data: 'name', name: 'name' }, 
			            { data: 'status', name: 'status' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			}); 
		</script>
@endsection