@extends('layouts.back')
@section('title',_('All Deletion Requests'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('All Deletion Requests'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="accomodationsTable">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>     
						<th><?php echo __('UserInfo'); ?></th> 
						<th><?php echo __('Type'); ?></th>
						<th><?php echo __('Label'); ?></th>
						<th><?php echo __('Payment Type'); ?></th>
						<th><?php echo __('Approved'); ?></th>
						<th><?php echo __('Completed'); ?></th>
						<th><?php echo __('Created On'); ?></th>
						<th><?php echo __('Actions'); ?></th> 
					</tr>
				</thead> 
			</table>
			</div> 
	</div> 
		<script>
			$(function() {
			    $('#accomodationsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/accomodations/requested-delete",       
			        columns: [
			            { data: 'name', name: 'name' },
			            { data: 'user_id', name: 'user_id' }, 
			            { data: 'accomodation_type_id', name: 'accomodation_type_id' },  
			            { data: 'label_id', name: 'label_id' }, 
			            { data: 'payment_type', name: 'payment_type' }, 
			            { data: 'approved', name: 'approved' }, 
			            { data: 'completed', name: 'completed' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});  
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/accomodations/delete-accomodation/'+id; 
				} else { 
					return false;
				} 
			}  
		</script>
@endsection