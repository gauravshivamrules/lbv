@extends('layouts.back') 
@section('title',__('Select Subscription')) 
@section('content')
<div class="well well bs-component">   
		<div class="row">  
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/subscriptions/choose-payment/{{$accomodation->id}}" method="get" id="paymentForm">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Select Subscription'); ?></legend> 
							<div class="form-group" id="showSubscription">
								<div class="col-lg-10 col-lg-offset-2">
									{!! Form::select('type_id',$subscriptions,null,['id'=>'SubscriptionId','class'=>'form-control','required'=>true] ) !!} 
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary" id="cont">{{__('Continue')}}</button>
									&nbsp; &nbsp; 
									{{__('OR')}} 
									<input type="checkbox" name="free_token" id="freeSubscriptionToken"> {{__('I have a free license coupon')}}
								</div>
							</div>
							<span> </span>
					</fieldset>
				</form>
				<form class="form-horizontal" action="/subscriptions/use-coupon/{{$accomodation->id}}" method="post" style="display:none" id="showCoupon">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Enter Coupon Code'); ?></legend> 
							<div> 
								<div class="form-group" >
									<div class="col-lg-10 col-lg-offset-2">
										<input type="text" name="token" class="form-control" required>  
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<a href="javascript:;" id="backBTN" class="btn btn-primary"> {{__('Back')}} </a>
									&nbsp; &nbsp;  
									<button type="submit" class="btn btn-primary" id="cont">{{__('Continue')}}</button>
								</div>
							</div>
							<span> </span>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
<script type="text/javascript">
	$(document).ready(function() {
		$('#freeSubscriptionToken').change(function() {
	       if($(this).is(":checked")) {
	       		$('#showCoupon').show();
	       		$('#paymentForm').hide(); 
	       } 
		});
		$('#backBTN').click(function() {
			$('#showCoupon').hide();
			$('#paymentForm').show(); 
			$('#freeSubscriptionToken').prop('checked', false); 
		});
	});
</script>
@endsection