@extends('layouts.back') 
@section('title',__('Add Location')) 
@section('content')
 <div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
    80%
  </div>
</div>
 <ul>    
 @foreach($errors->all() as $e)
 	<li class="error"> {{ $e }} </li>
 @endforeach 
 </ul> 
 <div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-6"> 
		
					<form class="form-horizontal" action="/accomodations/add_accom_location/{{$accom}}" method="post">  
						<input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
						<input type="hidden" name="lat" id="lat"> 
						<input type="hidden" name="lng" id="lng">  
						<fieldset>
							<legend>{{__('Add Accommodation Location')}}</legend>  
							<br>
							<div class="form-group">
								<label for="name" class="col-lg-4 control-label"><?php echo __('Country'); ?> </label>
								<div class="col-lg-8">
									<select name="country_id" class="form-control" id="country_id">
									     @foreach($countries as $k=>$v)
									     <option value="{{ $k }}">{{ $v }}</option>
									     @endforeach
									     <option value="other">{{ __('Other') }}</option>
									</select>
								</div>
							</div>
							<div class="form-group" id="regionDiv">
								<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
								<div class="col-lg-8">
									<select name="region_id" id="region_id" class="form-control" required><option>{{__('Region')}}</option></select>
								</div>
							</div>
							<div class="form-group" id="departmentDiv">
								<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
								<div class="col-lg-8">
									<select name="department_id" id="department_id" class="form-control"><option>{{__('Department')}}</option></select> 
								</div>
							</div>

							<div class="form-group" id="otherCountry" style="display:none;">
								<label for="name" class="col-lg-4 control-label"> {{__('Country')}}</label>
								<div class="col-lg-8">
									<input name="other_country" class="form-control"  type="text" id="other_country" placeholder="{{__('Enter country name')}}" required onchange="findAddressOnMap();">     
								</div>
							</div>
							<div class="form-group" id="otherRegion" style="display:none;">
								<label for="content" class="col-lg-4 control-label">{{__('Region')}}</label>
								<div class="col-lg-8">
									<input name="other_region" class="form-control"  type="text" id="other_region" placeholder="{{__('Enter region name')}}" required onchange="findAddressOnMap();">     
								</div>
							</div>
							<div class="form-group" id="otherDepartment" style="display:none;">
								<label for="name" class="col-lg-4 control-label">{{__('Department')}} </label>
								<div class="col-lg-8">
									<input name="other_department" class="form-control"  type="text" id="other_department" placeholder="{{__('Enter department name')}}" required onchange="findAddressOnMap();">     
								</div>
							</div>






							<div class="form-group">
								<label for="name" class="col-lg-4 control-label">{{__('Street')}} </label>
								<div class="col-lg-8">
									<input name="street" class="form-control"  type="text" id="street" required onchange="findAddressOnMap();">                          
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-lg-4 control-label">{{__('Street Number')}} </label>
								<div class="col-lg-8">
									<input name="street_number" class="form-control"  type="number" min="0"  id="street_number"  onchange="findAddressOnMap();">
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-lg-4 control-label">{{__('City/Town')}} </label>
								<div class="col-lg-8">
									<input name="city_town" class="form-control"  type="text" id="city_town"  required onchange="findAddressOnMap();">
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-lg-4 control-label">{{__('Postal Code')}}</label>
								<div class="col-lg-8">
									<input name="postal_code" class="form-control"  type="text" id="postal_code"  required onchange="findAddressOnMap();">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary" id="cont">{{__('Next')}}</button> 
								</div>
							</div>
						</fieldset>
					</form>

			</div>   
			<div class="col-md-6">
				<div id="map" style="height:450px;"> </div>
			</div>
		</div>
</div>  
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw8GNwHQjD19djzT9xYf84mTz1vnrYV7Y&libraries=places"></script> 
	<script src="/js/custom/accomodation_location.js"></script>
@endsection 