@extends('layouts.front') 
@section($accom->name,__(env('SITE_TITLE_BACK'))) 
@section('content') 
<style type="text/css">
  .yellow_star{
    color: yellow;
  }
  .my-5 {
    margin: 7px 0;
  }
  .mtb-4 {
    margin: 20px 0;
}
.owl-dots{
  display: none;
}
.icomoon_font{
	font-size: 48px;
	color: #000;
}
   ul > li {
       position: relative;
   }

   ul ul {
       position: absolute;
       top: 1em;
       left: 0;
       display: none;
   }

   ul > li:hover ul {
       display: block;
   }
   .ui-datepicker-title {
      color: black !important;
   }
   .navbar-fixed-top {
   position: unset;
   background: #59abfd;
   }
   a.navbar-brand.page-scroll img {
   width: 100px;
   margin-top: -21px;
   }
   td {
   font-size: 14px;
   }
   th {
   font-size: 15px;
   }
   form, p, span {
   margin:0;
   padding:0; }
   input { font:12px arial; }
   a {
   color:#0000FF;
   text-decoration:none; }
   a:hover { text-decoration:underline; }
   #wrapper, #loginform {
   margin:0 auto;
   padding-bottom:25px;
   background:#EBF4FB;
   width:504px;
   border:1px solid #ACD8F0; }
   #loginform { padding-top:18px; }
   #loginform p { margin: 5px; }
   #chatbox {
   text-align:left;
   margin:0 auto;
   margin-bottom:25px;
   padding:10px;
   background:#fff;
   height:270px;
   width:430px;
   border:1px solid #ACD8F0;
   overflow:auto; }
   #usermsg {
   width:395px;
   border:1px solid #ACD8F0; }
   #submit { width: 60px; }
   .error { color: #ff0000; }
   #menu { padding:12.5px 25px 12.5px 25px; }
   .welcome { float:left; }
   .logout { float:right; }
   .msgln { margin:0 0 2px 0; }
   .accordion {
       background-color: #eee;
       color: #444;
       cursor: pointer;
       padding: 18px;
       width: 100%;
       border: none;
       text-align: left;
       outline: none;
       font-size: 15px;
       transition: 0.4s;
   }
   .active, .accordion:hover {
       background-color: #ccc; 
   }
   .panel {
       padding: 0 18px;
       /*display: none;*/
       background-color: white;
       overflow: hidden;
   }
  /* header {
       background-image: url(/images/gallery/1/5b92e548d5046.jpg) !important;
       background-attachment: scroll;
       background-position: center center;
       -webkit-background-size: cover;
       -moz-background-size: cover;
       background-size: cover;
       -o-background-size: cover;
       text-align: center;
       color: #fff;
   }*/
   .navbar-fixed-top, .navbar-fixed-bottom {
    position: relative ;
   }
  .bg_img {
    /*background: url(https://lbv.sharmag.in/images/gallery/3/slider_5b92e54c29fba.jpeg);*/
    background: url("<?php echo $featuredImg ?>");
    height: 600px; 
    width: 100%;
    background-position: center;
    background-size: cover;
}
   .mt_20{
    margin-top: 20px; 
   }
  .view_title {
    font-size: 33px;
    color: #fff;
    font-weight: 800;
    margin-top: 12em;
}
   .view_dis{
    color: #fff;
   }
   .btn_view {
       margin-top: 450px;
       padding: 10px 20px;
       color: #000;
   }
   .bg_per {
       background: #59abfd;
       padding: 23px 0;
   }
   /* ----------- Mobile css ----------- */

   /* Portrait and Landscape */
   @media only screen 
     and (min-device-width: 320px) 
     and (max-device-width: 480px) {
   .btn_view {
       margin-top: 20px;
       padding: 10px 20px;
       color: #000;
   }
   .view_title {
       font-size: 33px;
       color: #fff;
       font-weight: 800;
       margin-top: 9em;
   }
   .tb_padding {
    margin-bottom: 20px;
}
   }
   /* ----------- tablet ----------- */

   /* Portrait and Landscape */
   @media only screen 
     and (min-device-width: 768px) 
     and (max-device-width: 1024px){
   .btn_view {
       margin-top: -200px;
       padding: 10px 20px;
       color: #000;
   }
   .tb_padding {
    margin-bottom: 20px;
}
   }

</style>
<link rel="stylesheet" href="/css/front/icomoon.css">
<link rel="stylesheet" href="/css/front/jquery.qtip.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/front/jquery.qtip.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/front/zabuto_calendar.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/front/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
<link rel="stylesheet" type="text/css" href="/css/front/lightgallery.css"> 

<link rel="stylesheet" href="/css/front/starability-all.min.css">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

<script type="text/javascript" src="/js/front/lightgallery.min.js"></script>

<div class="bg_img"> 
  <div class="container">
    <div class="col-md-12 mt_20">

      <a href="{{$refrer}}" class="btn btn-primary"> <i class="fas fa-chevron-left"></i> {{__('Go Back')}} </a>
    </div>
    <div class="col-md-6">
      <h2 class="view_title">{{ucfirst($accom->name)}}</h2>
      <p class="view_dis"> <i class="fas fa-check"></i> {{$accom->accomodation_address->region_id?ucfirst($accom->accomodation_address->region_id):''}},
          {{$accom->accomodation_address->country_id?ucfirst($accom->accomodation_address->country_id):''}}  
      </p>
    </div>
    <?php if(count($accom->gallery_images)>0){ ?>
    <div class="col-md-6 text-right">
      <button type="button" class="btn btn-primary btn_view" id="dynamic"> <i class="fas fa-camera-retro"></i> {{__('View Images')}}  ({{count($accom->gallery_images)}}) </button>
    </div>
    <?php } ?>
  </div>
  
</div>
<div class="bg_per">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xs-12">
        <div class="col-xs-4 tb_padding col-sm-2">
           <!-- <img src="/images/icon/icon11.png" class="icon_width"> -->
           <span class="icon-users icomoon_font"></span>
           <span class="badge_count" title="{{$accom->number_of_persons}} {{__('Persons')}}">{{$accom->number_of_persons}} </span>
        </div>
        <div class="col-xs-4 tb_padding col-sm-2">
           <!-- <img src="/images/icon/icon13.png" class="icon_width"> -->
           <span class="icon-hotel icomoon_font"></span>
           <span class="badge_count" title="{{$accom->bedrooms}} {{__('Bedrooms')}}"> {{$accom->bedrooms}}  </span>
        </div> 
        <div class="col-xs-4 tb_padding col-sm-2">
           <!-- <img src="/images/icon/icon12.png" class="icon_width_46"> -->
            <span class="icon-bathtub icomoon_font"></span>
           <span class="badge_count" title="{{$accom->bathrooms}} {{__('Bathrooms')}}">{{$accom->bathrooms}} </span>
        </div>
        
         <div class="col-xs-4 tb_padding col-sm-2">
           <span class="icon-single-bed-1 icomoon_font"></span> 
           <span class="badge_count" title="{{$accom->single_beds}} {{__('Singlebeds')}}">{{$accom->single_beds}}</span>
        </div>  <div class="col-xs-4 tb_padding col-sm-2">
           <span class="icon-bed icomoon_font"></span>
           <span class="badge_count" title="{{$accom->double_beds}} {{__('Doublebeds')}}">{{$accom->double_beds}}</span>
        </div> 
      </div>
      <div class="col-md-4"> 
        <a href="#"><div id="rateYo"></div></a> 
       </div>
    </div>
  </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script type="text/javascript">
/*$(function () {
  $("#rateYo").rateYo({
    starWidth: "40px",
    rating: 3.5,
    readOnly: true
  });
 
});*/

 $('#dynamic').on('click', function() {
    $.ajax({
      url:"/accomodations/getgal-image?id={{$accom->id}}",
      dataType:'json',
      success:function(r){
        $(this).lightGallery({
            dynamic: true,
            dynamicEl:r
        })
      }
    });
    setTimeout(function(){  $('.lg-close').append("{{__('Close')}}");  }, 1000);
 });
</script>

<div class="container">
   <div class="buy-single-single">
      <div class="col-md-8 single-box">
         
         <div class="buy-sin-single">
            <div class="col-sm-12 buy-sin">
               <h4>Description</h4>
               <p style="text-align:justify;"> {{$accom->description}} </p>
            </div>
            
            <div class="clearfix"> </div>
         </div>
         <div class="video-pre">
            <h4>{{__('Amenities')}}</h4>
            @if($searchTags)
              @foreach($searchTags as $s)
                <div class="col-md-6" style="margin: 5px 0;"> <i class="fas fa-check orange"></i> &nbsp;&nbsp; {{$s->tag_name}}</div>
              @endforeach
            @else
            <p> {{__('No amenities found')}} </p>
            @endif
         </div>
         <div class="clearfix"> </div>
         <!-- Map -->
         <div class="map-buy-single">
            <h4>Neighborhood Info</h4>
            @if( $accom->accomodation_address )
            <div class="row">
               <div class="col-md-6"> 
                  <?php
                     $parsed = parse_url($accom->website);
                     $urlStr='';
                     if (empty($parsed['scheme'])) {
                        $urlStr = 'http://' . ltrim($accom->website, '/');
                     }else{
                        $urlStr=ltrim($accom->website, '/');
                     }
                  ?>

                  @if($accom->payment_type==1)
                    <span><b>{{__('Website')}}<b>:<a href="{{$accom->website?$urlStr:'#'}}" target="_blank"> {{$accom->website ? $urlStr  :'--' }}</a></span><br>
                    <span><b>{{__('Street')}}<b>: {{$accom->accomodation_address->street_number}} {{$accom->accomodation_address->street}} </span><br>
                    <span><b>{{__('City')}}<b>: {{ucfirst($accom->accomodation_address->city)}}  </span><br>
                  @endif
               </div>
               <div class="col-md-6"> 
                  @if($accom->payment_type==1) 
                    <span><b>{{__('Postal Code')}}<b>: {{$accom->accomodation_address->postal_code}}  </span><br>
                  @endif
                    <span><b>{{__('Department')}}<b>: {{$accom->accomodation_address->department_id}} </span><br>
                    <span><b>{{__('Region')}}<b>:  {{$accom->accomodation_address->region_id}}</span><br>
                    <span><b>{{__('Country')}}<b>: {{$accom->accomodation_address->country_id}} </span><br>
               </div>
            </div>
            @endif
            <div class="map-buy-single1">
               <div id="map" style="width: 100%;min-height: 250px;"> </div>
            </div>
         </div>
         @if($accom->youtube_url)
         <div class="video-pre mt-40"> 
            <h4 style="clear: both;">Video Presentation</h4>
            <?php 
               $uURL=$accom->youtube_url;
               $uID=explode('?',$uURL);
               $link=str_replace("v=","",$uID[1]);
               $url='https://www.youtube.com/embed/'.$link;
            ?>
            <iframe width="420" height="315" src="{{$url}}" allow="autoplay; encrypted-media" allowfullscreen></iframe>
         </div>
         @endif
         @if(!$accom->extra_price->isEmpty())
         <div class="video-pre mt-40">
            <h4 style="clear: both;">{{__('Extra Charges')}} </h4>
            <table class="table">
                <thead>
                  <tr>
                    <th>{{__('Amount')}} </th>
                    <th>{{__('Description')}} </th>
                    <th>{{__('Type')}} </th>
                    <th>{{__('Per Person')}} </th>
                    <th>{{__('Optional')}} </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($accom->extra_price as $ep)
                    <tr>
                      <td> &euro;{{$ep->amount}} </td>
                      <td> {{$ep->description}} </td>
                      <td> {{$ep->charge_type}} </td>
                      <td> {{$ep->is_per_person?'Yes':'No'}} </td>
                      <td> {{$ep->is_optional?'Yes':'No'}} </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
         </div>
         @endif
         <div class="video-pre mt-40">
            <h4 style="clear: both;">{{__('AVAILABILITY')}} </h4>
            @if(count($accom->accomodation_prices)>0)
              <p>{{__('Click on dates to view prices')}} </p>
            @endif
              <div id="my-calendar"></div>
         </div>
         <div class="video-pre mt-40">
             <link href="/css/front/owl.carousel.min.css" rel="stylesheet">
          <script src="/js/front//owl.carousel.min.js"></script>

            <section id="testimonial">
              @if($finalRateArr)
                 <div class="center fadeInDown">
                     <h2>{{__('Rating and Reviews')}}</h2>
                 </div>
                 <div class="testimonial-slider owl-carousel">
                   
                    <?php $g=0; ?>
                        @foreach($finalRateArr as $f)
                            <?php 
                                $loc=$hyg=$comf=$serv=$pqual=0;
                                foreach ($f['ratings'] as $key => $r) {
                                  switch ($r->ratecategory_id) {
                                    case '1':
                                      $loc=$r->score;
                                      break;
                                    case '2':
                                      $hyg=$r->score;
                                      break;
                                    case '3':
                                      $comf=$r->score;
                                      break;
                                    case '4':
                                      $serv=$r->score;
                                      break;
                                    case '5':
                                      $pqual=$r->score;
                                      break;
                                  }
                                }
                            ?>
                              <div class="single-slide">
                                  <div class="content">
                                      {{$f['description']}} 
                                      <p>{{__('Locatie')}}: 
                                       <?php 
                                             for($i=0;$i<$loc;$i++) { ?>
                                               <i class="fa fa-star yellow_star" aria-hidden="true"></i>  
                                             <?php }
                                             $noStar=5-$loc;
                                             for ($j=0;$j<$noStar;$j++) { ?>
                                               <i class="fa fa-star" aria-hidden="true"></i>  
                                             <?php } ?>
                                      </p>
                                      <p>{{__('Hygiene')}}:
                                        <?php 
                                          for($i=0;$i<$hyg;$i++) { ?>
                                            <i class="fa fa-star yellow_star" aria-hidden="true"></i>  
                                          <?php }
                                          $noStarH=5-$hyg;
                                          for ($j=0;$j<$noStarH;$j++) { ?>
                                            <i class="fa fa-star" aria-hidden="true"></i>  
                                          <?php } ?>
                                      </p>
                                      <p>{{__('Comfort')}}:
                                          <?php 
                                          for($i=0;$i<$comf;$i++) { ?>
                                            <i class="fa fa-star yellow_star" aria-hidden="true"></i>  
                                          <?php }
                                          $noStarC=5-$comf;
                                          for ($j=0;$j<$noStarC;$j++) { ?>
                                            <i class="fa fa-star" aria-hidden="true"></i>  
                                          <?php } ?>
                                      </p>
                                      <p>{{__('Service')}}:
                                          <?php 
                                          for($i=0;$i<$serv;$i++) { ?>
                                            <i class="fa fa-star yellow_star" aria-hidden="true"></i>  
                                          <?php }
                                          $noStarS=5-$serv;
                                          for ($j=0;$j<$noStarS;$j++) { ?>
                                            <i class="fa fa-star" aria-hidden="true"></i>  
                                          <?php } ?>
                                      </p>
                                      <p>{{__('Prijskwaliteit')}}
                                          <?php 
                                          for($i=0;$i<$pqual;$i++) { ?>
                                            <i class="fa fa-star yellow_star" aria-hidden="true"></i>  
                                          <?php }
                                          $noStarP=5-$pqual;
                                          for ($j=0;$j<$noStarP;$j++) { ?>
                                            <i class="fa fa-star" aria-hidden="true"></i>  
                                          <?php } ?>
                                      </p>
                                      <?php $g++; ?>
                                      <p style="float: right;">{{__('By')}}: {{$f['user']}}   {{__('on')}}: {{$f['created']}} </p>
                                  </div>
                              </div>
                        @endforeach
                 </div>
                 @endif
         </section>
         </div>
         @if($similarAccomArr)
           <div class="video-pre mt-40">
            <h6 style="clear: both;">{{__('Not the one you are looking for? check out other ')}} {{$accom->accomodation_type->type_name}} </h6>
            <hr>
            <br>
             <div class="row">
                @foreach($similarAccomArr as $s)
                  <div class="col-md-6 box_2">
                      <div class="tag">
                        <a href="/accomodations/view/{{$s['slug']}}" class="mask">
                          <img class="img-responsive zoom-img" src="{{$s['image']}}" alt="" onerror="this.src='/images/no-image.png'">
                          @if($s['price'])
                            <span class="four">{{$s['price']}}  </span>
                          @endif
                        </a>
                          <div class="most-1">
                          <h5><a href="/accomodations/view/{{$s['slug']}}">{{$s['name']}} </a></h5> 
                        </div>
                      </div>
                  </div>
                @endforeach
             </div>
           </div>
          @endif
         <script type="text/javascript">
           $('.testimonial-slider').owlCarousel({
                   margin: 30,
                   autoplay:true,
                   autoplayTimeout:2000,
                   navigation:true,
                   navigationText: [
                       "<i class='fa fa-chevron-left'></i>",
                       "<i class='fa fa-chevron-right'></i>"
                    ],
                   responsive: {
                       0: {
                           items: 1
                       },
                       768: {
                           items: 2
                       },
                       992: {
                           items: 2
                       }
                   }
               });
         </script>
      </div>
      <!--MOBILE Modal -->
      @if(count($accom->accomodation_prices)>0)
          <div class="modal fade" id="myModal" role="dialog">
             <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"> 
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"> {{__('Book Now')}} </h4>
                   </div>
                   <div class="modal-body">
                      <div class="sidebar_holder">
                         <div class="side_frst_sec">
                            <div class="row bdr_btm">
                                  @if($accom->accomodation_prices[0]->price_unit=='perweek') 
                                     <div class="col-md-6 font-14">{{__('Price starts')}} {{__(ucfirst($accom->accomodation_prices[0]->price_unit))}} </div>
                                     <div class="col-md-6">
                                        <div class="s_price">
                                           {{__('From')}} €{{$minPrice}}
                                        </div>
                                     </div>
                                  @else
                                     <div class="col-md-6 font-14">Last minute rate per week from <strong> € 553 for </strong> </div>
                                     <div class="col-md-6">
                                        <div class="s_price">
                                           $500
                                        </div>
                                     </div>
                                  @endif 
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-md-12">
                               <div class="sec_pref">
                                  <p class="pref_dt_title">{{__('Please select your preferred dates?')}}</p>
                                  <form class="sidebar_frm">
                                     <input type="text" class="pic_date" placeholder="select your date">
                                     @if($accom->number_of_persons)
                                     <select class="form-control">
                                        <?php  
                                        for($i=1;$i<=$accom->number_of_persons;$i++){
                                           if($i==1){
                                              echo "<option value='$i'>$i Person</option>";
                                           }else{
                                              echo "<option value='$i'>$i Persons</option>";
                                           }
                                        }
                                        ?>
                                     </select>
                                     @endif
                                     <button type="submit" class="book_btn">Book Now</button>
                                  </form>
                                  <p class="nwc_title">Nothing will be charged yet.</p>
                               </div>
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-md-6 col-xs-6 nopad_right">
                               <div class="mark_fav">
                                  <i class="fas fa-heart"></i>
                                  <p>Mark as favorite</p>
                               </div>
                            </div>
                            <div class="col-md-6 col-xs-6 nopad_left">
                               <div class="forward">
                                  <i class="fas fa-reply"></i>
                                  <p>Mark as favorite</p>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   </div>
                </div>
             </div>
          </div>
      @endif
      <div class="modal fade" id="chatModal" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"> {{__('Contact Owner')}} </h4>
               </div>
               <div class="modal-body">
                  <!-- <div class="sidebar_holder"> -->
                  <div class="row">
                     <div class="col-md-3"> </div>
                     <div class="col-md-6">
                        <div id="chatbox"></div>
                     </div>
                  </div>
                  <form name="message" action="#" class="form-horizontal">
                     <div class="row">
                        <div class="col-md-3"> </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <input name="msg" class="form-control" type="text" id="usermsg" size="63" />
                           </div>
                           <button name="submitmsg" class="btn btn-default" type="button"  id="submitmsg"> Send</button>
                        </div>
                        <div class="col-md-3"> </div>
                     </div>
                  </form>
                  <!-- </div> -->
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div id="signInModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"> {{__('Login')}} </h4>
               </div>
               <div class="modal-body">
                  <ul class="nav nav-tabs" id="tabContent">
                     <li class="active"><a href="#login" data-toggle="tab">{{__('Login')}} </a></li>
                     <li><a href="#register" data-toggle="tab">{{__('Register')}} </a></li>
                  </ul>
                  <br>
                  <div class="tab-content">
                     <div class="tab-pane active" id="login">
                        <div class="login-box-body">
                           <p class="login-box-msg"><?php echo __('Sign in to your account') ?></p>
                           <form action="/login-view" method="post">
                              {{csrf_field()}}
                              <div class="form-group has-feedback">
                                 <input type="hidden" name="accom_id" value="{{$accom->id}}">
                                 <input type="email" name="email" class="form-control" placeholder="<?php echo __('Email') ?>" required>
                                 <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                 <span style="color:red">{{$errors->first('email')}}  </span>
                              </div>
                              <div class="form-group has-feedback">
                                 <input type="password" name="password" class="form-control" placeholder="<?php echo __('Password') ?>" required>
                                 <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                 <span style="color:red">{{$errors->first('password')}} </span>
                              </div>
                              <div class="row">
                                 <div class="col-xs-8">
                                 </div>
                                 <div class="col-xs-4">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign In') ?></button>
                                 </div>
                              </div>
                              <div class="social-auth-links text-center"> 
                                  <p>- <?php echo __('Or login with') ?> -</p> 
                                  <a style="font-size: 12px;" href="/auth/facebook" class="btn btn-facebook"><i class="fab fa-facebook"></i></a>
                                  <a style="font-size: 12px;" href="/auth/google" class="btn btn-google"><i class="fab fa-google"></i></a>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="tab-pane" id="register">
                        <form action="{{ route('postCustomerRegisterView') }}" method="post">
                           {{csrf_field()}}
                           <div class="form-group has-feedback">
                              <input type="hidden" name="accom_id" value="{{$accom->id}}">
                              <input type="text" name="first_name" class="form-control" placeholder="<?php echo __('First Name') ?>" required>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                              <span style="color:red">{{$errors->first('first_name')}}  </span>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="text" name="last_name" class="form-control" placeholder="<?php echo __('Last Name') ?>" required>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                              <span style="color:red">{{$errors->first('last_name')}}  </span>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="email" name="email" class="form-control" placeholder="<?php echo __('Email') ?>" required>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                              <span style="color:red">{{$errors->first('email')}}  </span>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="password" name="password" class="form-control" placeholder="<?php echo __('Password') ?>" required>
                              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                              <span style="color:red">{{$errors->first('password')}} </span>
                           </div>
                           <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="tos" required> 
                              <label class="custom-control-label" for="defaultUnchecked">  <a href="/tos" target="_blank">{{__('I accept the terms and conditions')}}</a></label><br>
                              <span style="color:red">{{$errors->first('tos')}}  </span> 
                           </div>
                           <div class="row">
                              <div class="col-xs-8">
                              </div>
                              <div class="col-xs-4">
                                 <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign Up') ?></button>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-8">
                                 <p><?php echo __('Or Signup with') ?></p>
                                 <a style="font-size: 12px;" href="/auth/facebook?role=2" class="btn btn-facebook"><i class="fab fa-facebook"></i></a>
                                 <a style="font-size: 12px;" href="/auth/google?role=2" class="btn btn-google"><i class="fab fa-google"></i></a>
                              </div>
                              
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">

         @if(Auth::check())
            refreshChats();
         @endif
            $("#submitmsg").click(function(){
               if(!$("#usermsg").val()){
                  alert('Please enter message');
                  return false;
               }   
               $.ajax({
                  type:'post',
                  url:'/tickets/contact-advertiser',  
                  data:{text:$("#usermsg").val(),accom_id:"{{$accom->id}}"},
                  success:function(r){ 
                     refreshChats();
                  }
               });
               $("#usermsg").val('');
               return false;
            });
            function refreshChats() {
               $.ajax({
                  url:"/tickets/refresh-chats?accom_id={{$accom->id}}",
                  dataType: 'html',
                  success:function(r){
                     $('#chatbox').html(r);
                  }
               });
             }
             $(document).keypress(function(e) {
                 if(e.which == 13) {
                    e.preventDefault();
                    if(!$("#usermsg").val()){
                       alert('Please enter message');
                       return false;
                    }  
                    $.ajax({
                      type:'post',
                      url:'/tickets/contact-customer',   
                      data:{text:$("#usermsg").val(),accom_id:"{{$accom->id}}"},
                      success:function(r){
                       refreshChats();
                      }
                   });
                    $("#usermsg").val(''); 
                 }
             }); 
      </script>
      <div class="ftr visible-xs">
         <div class="row">
            <div class="col-xs-6">
               <!-- <button type="button" class="book_btn" style="background: #000;">Contact Now</button> -->
               @if(Auth::check()) 
               <a href="/tickets/contact-owner/{{$accom->id}}" class="book_btn" style="background: #000;" id="chatBtnMobile"> {{__('Contact Owner')}} </a> 
               @else
               <button type="button" class="book_btn" style="background: #000;" data-toggle="modal" data-target="#signInModal" id="signInchatBtn">Contact Owner</button> 
               @endif
            </div>
            <div class="col-xs-6">
               <button type="submit" class="book_btn" data-toggle="modal" data-target="#myModal">Book Now</button>
            </div>
         </div>
      </div>
      <!-- WEB BOOKING -->
      @if(count($accom->accomodation_prices)>0)
          <div class="col-md-4">
             <div class="sidebar_holder hidden-xs" style="z-index:1;">
                <div class="side_frst_sec">
                   <div class="row bdr_btm">
                     <div class="col-md-6 font-14">{{__(ucfirst($accom->accomodation_prices[0]->price_unit))}} {{__('prices starting from')}} </div>
                     <div class="col-md-6">
                        <div class="s_price">
                            €<span id="showSPrice"> {{$minPrice}} </span>
                        </div>
                     </div>
                   </div>
                </div>
                <div class="row">
                   <div class="col-md-12">
                      <div class="sec_pref">
                         <p class="pref_dt_title">{{__('Please select your preferred dates')}} </p>
                         <form class="sidebar_frm" method="get" action="/bookings/make-booking">  
                           {{csrf_field()}}

                           <input type="hidden" name="rooms_arr" id="roomsArr">
                            <div class="row"> 
                               <div class="col-md-5">
                                  <input type="text" id="booking_start_date" name="booking_start_date" class="form-control" placeholder="{{__('From')}}" required>
                                 <span style="color:red;">  {{$errors->first('booking_start_date')}}</span>
                               </div>
                              <div class="col-md-1"> <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i></div>
                               <div class="col-md-5"> 
                                  <input type="text" id="booking_end_date" name="booking_end_date" class="form-control" placeholder="{{__('To')}}" required>
                                  <span style="color:red;">  {{$errors->first('booking_end_date')}}</span>
                               </div>
                             </div>
                            <input type="hidden" name="accom_id" value="{{$accom->id}}">
                            <input type="hidden" name="amount" id="booking_amount" value="{{$minPrice}}">
                            <input type="hidden" name="validate_booking_persons" id="validate_booking_persons" value="0">
                            <input type="hidden" name="price_unit" id="price_unit" value="{{$accom->accomodation_prices[0]->price_unit}}">
                            <div class="row"> 
                               <div class="col-md-6">
                                @if($accom->number_of_persons)
                                   <select class="form-control" id="bookingAdults" name="adults" onchange="setMaxPeople();"  required>
                                    <option value="">{{__('Adults')}} </option>
                                      <?php  
                                      for($i=1;$i<=$accom->number_of_persons;$i++){
                                         if($i==1){
                                            echo "<option value='$i'>$i </option>";
                                         }else{
                                            echo "<option value='$i'>$i </option>";
                                         }
                                      }
                                      ?> 
                                   </select>
                                   <span style="color:red;">  {{$errors->first('adults')}}</span>

                                @endif
                               </div>
                               <div class="col-md-6"> 
                                @if($accom->number_of_persons)
                                   <select class="form-control" id="bookingChilds" name="childs" >
                                    <option value="">{{__('Childs')}} </option>
                                   </select>
                                @endif
                               </div>
                             </div>
                            <br>
                            @if($priceUnit!='perweek')
                                <span><a href="javascript:;" class="btn btn-primary" id="showRoomsBtn" onclick="showRoomsDialog();"> {{__('Click here to select rooms')}}</a></span>
                                <div id="roomsDiv">
                                  <div id="dialog" title="{{__('Select Rooms')}}" style="display:none;">
                                      <div id="loadingAjax"><img src="/images/loader.gif"></div>
                                  </div>
                                </div>
                            @endif
                            <textarea class="form-control" name="comments" placeholder="{{__('Comments')}}"></textarea>
                            <div id="extrahiddenfields"></div>
                            <button type="submit" class="book_btn">{{__('Book Now')}} </button>
                         </form>
                         <!-- <p class="nwc_title">Nothing will be charged yet.</p> -->
                      </div>
                   </div>
                </div>
                <div class="row">
                   <div class="col-md-6 col-xs-6 nopad_right">
                      <div class="mark_fav">
                         <i class="fas fa-heart"></i>
                         <p>{{__('Mark as favorite')}} </p>
                      </div>
                   </div>
                   <div class="col-md-6 col-xs-6 nopad_left">
                      <div class="forward">
                         @if(Auth::check())
                         <a href="/tickets/contact-owner/{{$accom->id}}"   id="chatBtn">
                            <i class="fas fa-file-contract"></i>
                            <p>{{__('Contact Owner')}} </p>
                         </a>
                         @else
                         <a href="javascript:;"  data-toggle="modal" data-target="#signInModal" id="signInchatBtn">
                            <i class="fas fa-file-contract"></i>
                             <p>{{__('Contact Owner')}} </p>
                         </a>
                         @endif
                      </div>
                   </div>
                </div>
             </div> 
          </div>
      @endif
      <div class="clearfix"> </div>
      <style type="text/css">
       .ui-tooltip {
           max-width: 350px;
         }
      </style>
   </div>
</div>
<!--//footer-->
<script type="text/javascript" src="/js/front/jquery.qtip.min.js"></script> 
<script type="text/javascript" src="/js/front/zabuto_calendar.min.js"></script> 
<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="/js/front/jquery.flexisel.js"></script>
<script defer src="/js/front/jquery.flexslider.js"></script> 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>

<!-- <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBw8GNwHQjD19djzT9xYf84mTz1vnrYV7Y&libraries=places"></script>  -->
<script type="text/javascript">
  var total=0;
  var roomsArr=[]; 
  $(document).ready(function(){
    zabutoPriceCalendar();
    setTimeout(function(){  getTitle() }, 2000);
    $("#owl-example").owlCarousel({
       items : 2,
       slideSpeed : 300,
    });
    var changeDay="{{$changeDay}}";
    var hideDay=8;
    var addDay=1;
    if(changeDay=='sunday'){
        hideDay=0;
        addDay=7;
    }else if(changeDay=='saturday'){
        hideDay=6;
        addDay=7;
     }
     var minDay="{{$minDay}}";
     var array = "{{json_encode($bookedArrFinal)}}";
     // ["2018-11-20","2018-11-21","2018-11-22"]
     if(changeDay=='sunday' || changeDay=='saturday'){ 
         $("#booking_start_date").datepicker({ 
             dateFormat: 'dd-mm-yy',
             minDate:0,
             onSelect:function(date) {
               var selectedDate=new Date(date);
               var endDate=new Date(selectedDate.getTime() );
               var date2=$('#booking_start_date').datepicker("getDate",'+'+minDay+'d');
               date2.setDate(date2.getDate()+parseInt(minDay)); 
               $('#booking_end_date').datepicker('setDate', date2);
               $('#booking_end_date').datepicker("option","minDate",date2);     
             }, 
             beforeShowDay: function(date) {
               var day=date.getDay();
               // return [day==hideDay,''];
               var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
               return [ array.indexOf(string) == -1 && day==hideDay,'' ];
             } 
         });
         $("#booking_end_date").datepicker({
            minDate:0,
            dateFormat:'dd-mm-yy',
            beforeShowDay: function(date) {
               var day=date.getDay();
               // return [day==hideDay,''];
               var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
               return [ array.indexOf(string) == -1 && day==hideDay,'' ];
             }
         });
      } else{
         $("#booking_start_date").datepicker({  
             dateFormat: 'dd-mm-yy',
             minDate:0,
             onSelect:function(date) {
               var selectedDate=new Date(date);
               var endDate=new Date(selectedDate.getTime() );
               // var date2=$('#booking_start_date').datepicker("getDate",'+'+minDay+'d');
               var date2=$('#booking_start_date').datepicker("getDate");
               date2.setDate(date2.getDate()+parseInt(minDay)); 
               $('#booking_end_date').datepicker('setDate', date2);
               $('#booking_end_date').datepicker("option","minDate",date2);     
             },
             beforeShowDay: function(date){
                  var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                  return [ array.indexOf(string) == -1 ];
             }
         });
         $("#booking_end_date").datepicker({
            minDate:0,
            dateFormat:'dd-mm-yy',
            beforeShowDay: function(date){
               var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
               return [ array.indexOf(string) == -1 ];
            }
         });
      } 
      /*initAutocomplete(); */
      $.ajax({url:'/stastics/accom-visit?accom_id={{$accom->id}}',success:function(){}});
  });
  $(document).on('change', '.select', function(){
      total=0;
      roomsArr=[];
      $("input:checkbox[name=booking_rooms]:checked").each(function(i,v){
          var chunks=v.id.split("_");
          if(chunks){
            var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]).val();
            var price=chunks[2];
            total+=parseInt(noR)*parseInt(chunks[2]);
            pushRoom(chunks[1],noR);
          }
      });
      $('#roomsArr').val(JSON.stringify(roomsArr));
      var days=calculateDays();
      total=parseInt(total)*parseInt(days); 
      $('#ui-id-1').html("{{__('Select Rooms')}}| {{__('Your Total:')}}€"+total);
      $('#showSPrice').html(total);
      $('#booking_amount').val(total);
  });
  $(document).on('change', '.selectperson', function(){
      total=0;
      roomsArr=[];
      $("input:checkbox[name=booking_rooms]:checked").each(function(i,v){
          var chunks=v.id.split("_");
          if(chunks){
            console.log(chunks);
            var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]+'_'+chunks[3]+'_'+chunks[4]).val();
            console.log(noR);

            /*var price=chunks[2];
            total+=parseInt(noR)*parseInt(chunks[2]);
            pushRoom(chunks[1],noR);*/
          }
      });
      $('#roomsArr').val(JSON.stringify(roomsArr));
      var days=calculateDays();
      total=parseInt(total)*parseInt(days); 
      $('#ui-id-1').html("{{__('Select Rooms')}}| {{__('Your Total:')}}€"+total);
      $('#showSPrice').html(total);
      $('#booking_amount').val(total);
  });
  function setRatesForPerson(chunks){
   console.log('chunks');
   console.log(chunks);
   var adults=$('#bookingAdults').val();
   var childs=$('#bookingChilds').val()?$('#bookingChilds').val():0;
   var totalPersons=parseInt(adults)+parseInt(childs);
   var maxAdults=$('#max_adults_per_room_'+chunks[1]).val();
   var maxPersons=$('#max_persons_per_room_'+chunks[1]).val();
   var eAdultPrice=$('#extra_price_adult_'+chunks[1]).val();
   var eChildPrice=$('#extra_price_child_'+chunks[1]).val();
   var noOfRoom=$('#max_no_of_rooms_'+chunks[1]).val();
   var rAdults=$('#adults_'+chunks[1]).val();
   var rChilds=$('#childs_'+chunks[1]).val();
   var price=0;
    if(totalPersons==1){
      price=chunks[2];
    }else if(totalPersons==2){
      price=chunks[3];
    }else if(totalPersons>2){
      if(adults==2 && childs>0 && childs<=2){
        price=chunks[4];
      }else{
        var maxPersonsPerRoom=parseInt(maxPersons)*parseInt(noOfRoom);
        if(totalPersons<=maxPersonsPerRoom){ 
          console.log('1089');
          //if(adults>rAdults){
            console.log('1091');
            if(adults<=maxAdults){
              console.log('1093');
              // var extraAdult=parseInt(maxAdults)-parseInt(rAdults);
              var extraAdult=parseInt(rAdults)-parseInt(adults);
              var extraAdultAmt=parseInt(Math.abs(extraAdult))*parseInt(eAdultPrice);
              console.log('extraAdultAmt');
              console.log(extraAdultAmt);

              price=parseInt(chunks[3])+parseInt(extraAdultAmt);
              console.log('1097');
              console.log(price);
            }
            if(childs>rChilds){
              price+=parseInt(eChildPrice);
            }
          /*}else{
            console.log('1101');
            var extra=parseInt(adults)-parseInt(2);
            var eP=parseInt(chunks[2])*parseInt(extra);
            price=parseInt(chunks[3])+parseInt(eP);
          } */
        }else{
          console.log('1107');
          console.log('total persons more than allowed select more rooms');
        }
      }
    }
    return price;
  }
  function setPerPersonTotal() {
      total=0;
      roomsArr=[];
      $("input:checkbox[name=booking_rooms]:checked").each(function(i,v){
          var chunks=v.id.split("_");
          console.log(chunks); 
          if(chunks){
            var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]+'_'+chunks[3]+'_'+chunks[4]).val();
            console.log(noR); 
            price=setRatesForPerson(chunks);
            console.log('price');
            console.log(price);
            total+=parseInt(noR)*parseInt(price);
            pushRoom(chunks[1],noR);
          }
      });
      $('#roomsArr').val(JSON.stringify(roomsArr));
      var days=calculateDays();
      total=parseInt(total)*parseInt(days); 
      $('#ui-id-1').html("{{__('Select Rooms')}}| {{__('Your Total:')}}€"+total);
      $('#showSPrice').html(total);
      $('#booking_amount').val(total);
  }
  function setNewTotal() {
      total=0;
      roomsArr=[];
      $("input:checkbox[name=booking_rooms]:checked").each(function(i,v){
          var chunks=v.id.split("_");
          if(chunks){
            var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]).val();
            var price=chunks[2];
            total+=parseInt(noR)*parseInt(chunks[2]);
            pushRoom(chunks[1],noR);
          }
      });
      $('#roomsArr').val(JSON.stringify(roomsArr));
      var days=calculateDays();
      total=parseInt(total)*parseInt(days); 
      $('#ui-id-1').html("{{__('Select Rooms')}}| {{__('Your Total:')}}€"+total);
      $('#showSPrice').html(total);
      $('#booking_amount').val(total);
  }
  function showRoomsDialog() {
    if($('#bookingAdults').val() && $('#booking_start_date').val()){
        total=0;
        roomsArr=[];
        //$('#roomsArr').val('');
        $('#loadingAjax').show();
        allotRooms();
        $( "#dialog" ).dialog({
               width: '600px', 
               maxWidth:"800px",
               height: 'auto', 
               resizable: true,
               modal: true, 
               buttons: {
                 "{{__('Select')}}": function() { 
                  if(total>0){
                    if(validateBookingPersons()){
                      $(this).dialog("close");
                    } else{
                      alert("{{__('You need more rooms,please select more')}}");
                   }
                  }else{
                      alert("{{__('Please select rooms')}}");
                   }
                 }, 
                 "{{__('Cancel')}}": function() { 
                    $(this).dialog("close");
                 } 
               },
        }); 
        $('#loadingAjax').hide(); 
        $('.ui-dialog-titlebar-close').text('X');
    }else{
      alert("{{__('Please select dates/persons')}}");
      return false;
    }
  }
  function validateBookingPersons(){
    var adults=$('#bookingAdults').val();
    var childs=$('#bookingChilds').val()?$('#bookingChilds').val():0;
    var totalPersons=parseInt(adults)+parseInt(childs);
    var totalCapacity=0;
    $("input:checkbox[name=booking_rooms]:checked").each(function(i,v){
        if($('#price_unit').val()=='perroom'){
            var chunks=v.id.split("_");
            if(chunks){
              var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]).val();
              var rAdults=$('#adults_'+chunks[1]).val(); 
              var rChilds=$('#childs_'+chunks[1]).val();
              var rTotalPersons=parseInt(rAdults)+parseInt(rChilds);
              totalCapacity+=parseInt(noR)*parseInt(rTotalPersons);
            }          
        }else{
          var chunks=v.id.split("_");
          if(chunks){
            var noR=$('#no_of_room_'+chunks[1]+'_'+chunks[2]+'_'+chunks[3]+'_'+chunks[4]).val();
            var rAdults=$('#adults_'+chunks[1]).val(); 
            var rChilds=$('#childs_'+chunks[1]).val();
            var rTotalPersons=parseInt(rAdults)+parseInt(rChilds);
            totalCapacity+=parseInt(noR)*parseInt(rTotalPersons);
          }  
        } 
    });
    if(totalPersons>=totalCapacity){
      $('#validate_booking_persons').val(0);
      return false;
    }else{
      $('#validate_booking_persons').val(1);
      return true;
    }
  }
  function calculatePerPerson(){
    setPerPersonTotal();
   /* var adults=$('#bookingAdults').val();
    var childs=$('#bookingChilds').val()?$('#bookingChilds').val():0;
    var totalPersons=parseInt(adults)+parseInt(childs);
    var price;
    var family;
    if(totalPersons==1){
        price=price1;
    }else if(totalPersons==2){
        price=price2;
        if(adults==2 && childs>0 && childs<=2){
          price=price3;
        }
    }*/
  }
  function allotRooms(){
    total=0;
    $.ajax({
      url:'/bookings/allot-rooms?accom_id={{$accom->id}}&adults='+$('#bookingAdults').val()+'&childs='+$('#bookingChilds').val()+'&start='+$('#booking_start_date').val()+'&end='+$('#booking_end_date').val(),
      dataType:'json',
      success: function(r){
        $('#dialog').html('');
        if(r.success){
           console.log(r); 
           $.each(r.rooms,function(i,v){
              if($('#price_unit').val()=='perroom'){
                var div="<div class='row'>";
                div+="<div class='col-md-1'><input type='checkbox' class='book-checkbox' onchange='setTotal(this);'  data='"+v.room_price[0].price+"' name='booking_rooms' id='room_"+v.id+"_"+v.room_price[0].price+"'></div>";
                div+="<div class='col-md-3'>";
                div+="<input type='hidden' id='adults_"+v.id+"' value='"+v.adults+"'>";
                div+="<input type='hidden' id='childs_"+v.id+"' value='"+v.childs+"'>";
                div+="<select class='form-control select'   name='no_of_room' id='no_of_room_"+v.id+"_"+v.room_price[0].price+"'>";
                for(var n=1;n<=v.no_of_rooms;n++){
                  div+="<option value='"+n+"'>"+n+"</option>";    
                }
                div+="</select>";
                div+="</div>";
                div+="<div class='col-md-8'>";
                div+="<span>{{__('Type')}}: "+v.room_type+"</span><br>";
                div+="<span>{{__('Adults')}}: "+v.adults+"</span><br>";
                if(v.childs){
                  div+="<span>{{__('Childs')}}: "+v.childs+"</span><br>";
                }
                div+="<span>{{__('Price')}}: €"+v.room_price[0].price+"</span>";
                if(v.max_adults_per_room){
                  div+="<span>{{__('Max Adults Per Room')}}: "+v.max_adults_per_room+"</span><br>";
                }
                if(v.max_persons_per_room){
                  div+="<span>{{__('Max Persons Per Room')}}: "+v.max_persons_per_room+"</span><br>";
                }
                div+="</div>";
                div+="</div><hr>"; 
              }else{
                var div="<div class='row'>";
                div+="<div class='col-md-1'><input type='checkbox' class='book-checkbox' onchange='calculatePerPerson(this);'  name='booking_rooms' id='room_"+v.id+"_"+v.room_price[0].price1+"_"+v.room_price[0].price2+"_"+v.room_price[0].price3+"'></div>";
                div+="<div class='col-md-3'>";
                div+="<input type='hidden' id='extra_price_adult_"+v.id+"' value='"+v.room_price[0].extra_price_adult+"'>";
                div+="<input type='hidden' id='extra_price_child_"+v.id+"' value='"+v.room_price[0].extra_price_child+"'>";
                div+="<input type='hidden' id='max_adults_per_room_"+v.id+"' value='"+v.max_adults_per_room+"'>";
                div+="<input type='hidden' id='max_persons_per_room_"+v.id+"' value='"+v.max_persons_per_room+"'>";
                div+="<input type='hidden' id='max_no_of_rooms_"+v.id+"' value='"+v.no_of_rooms+"'>";
                div+="<input type='hidden' id='adults_"+v.id+"' value='"+v.adults+"'>";
                div+="<input type='hidden' id='childs_"+v.id+"' value='"+v.childs+"'>";
                div+="<select class='form-control selectperson'   name='no_of_room' id='no_of_room_"+v.id+"_"+v.room_price[0].price1+"_"+v.room_price[0].price2+"_"+v.room_price[0].price3+"'>";
                for(var n=1;n<=v.no_of_rooms;n++){
                  div+="<option value='"+n+"'>"+n+"</option>";     
                }
                div+="</select>";
                div+="</div>";
                div+="<div class='col-md-8'>";
                div+="<span>{{__('Type')}}: "+v.room_type+"</span><br>";
                div+="<span>{{__('Adults')}}: "+v.adults+"</span><br>";
                if(v.childs){
                  div+="<span>{{__('Childs')}}: "+v.childs+"</span><br>";
                }
                div+="<span>{{__('Minimum Days')}}: "+v.min_days+"</span><br>";
                div+="<span>{{__('Price/Person')}}: €"+v.room_price[0].price1+"</span><br>";
                div+="<span>{{__('Price/2 Person')}}: €"+v.room_price[0].price2+"</span><br>";
                div+="<span>{{__('Price/Family')}}: €"+v.room_price[0].price3+"</span><br>";
                if(v.room_price[0].extra_price_adult){
                  div+="<span>{{__('Extra Price Adult')}}: €"+v.room_price[0].extra_price_adult+"</span><br>";  
                }
                if(v.room_price[0].extra_price_child){
                  div+="<span>{{__('Extra Price Child')}}: €"+v.room_price[0].extra_price_child+"</span><br>";   
                }
                if(v.max_adults_per_room){
                  div+="<span>{{__('Max Adults Per Room')}}: "+v.max_adults_per_room+"</span><br>";
                }
                if(v.max_persons_per_room){
                  div+="<span>{{__('Max Persons Per Room')}}: "+v.max_persons_per_room+"</span><br>";
                }
                div+="</div>";
                div+="</div><hr>"; 
              }
              $('#dialog').append(div);
           });
        } else {
          $('#dialog').append("<span>{{__('Sorry,no rooms found!')}}</span>");
        }
      }
    });  
  }
  function removeByKey(array, params){
    array.some(function(item, index) {
      if(array[index][params.key] === params.value){
        // found it!
        array.splice(index, 1);
        return true; // stops the loop
      }
      return false;
    });
    return array;
  }
  function pushRoom(id,count){
    roomsArr.push({
        room_id: id, 
        room_count:count
    });
  }
  function removeRoom(id){
    removeByKey(roomsArr,{
      key: 'room_id',
      value: id
    });
  }
  function setTotal(d){
    setNewTotal();
  }
  function calculateDays() {
      var d1 = $('#booking_start_date').datepicker('getDate');
      var d2 = $('#booking_end_date').datepicker('getDate');
      var oneDay = 24*60*60*1000;
      var diff = 0;
      if (d1 && d2) {
        diff = Math.round(Math.abs((d2.getTime() - d1.getTime())/(oneDay)));
        return diff;
      }
  }
  function setMaxPeople() {
    var maxP="{{$accom->number_of_persons?$accom->number_of_persons:0}}";
    var childs=parseInt(maxP)-parseInt($('#bookingAdults').val());
    $('#bookingChilds').empty();
    var option="<option value=''>{{__('Childs')}}</option>";
    $('#bookingChilds').append(option);
    for(var i=1;i<=childs;++i){
      option="<option value="+i+">"+i+"</option>";    
      $('#bookingChilds').append(option);
    }
  } 
  function initAutocomplete() {   
      var myLatLng = {lat: parseFloat("{{$accom->accomodation_address->lattitude}}"), lng: parseFloat("{{$accom->accomodation_address->longitude}}") };
      map = new google.maps.Map(document.getElementById('map'), {    
          zoom: 2,
          center: myLatLng
      });
      var marker = new google.maps.Marker({
           position: myLatLng,
           map: map,
       });
  }
  function approveAccom(id) {
     if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
       window.location.href='/accomodations/approve-accomodation/'+id;
     } else { 
       return false;
     } 
  }
  function deleteAccom(id) {
     if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
       window.location.href='/accomodations/delete-accomodation/'+id;  
     } else { 
       return false;
     } 
  }
  globalQtipDate='';
  function getTitle() { 
      $('.label-success').click(function(event) {
        if($('.in').length){
          $('.in').remove();
        }
         var el = $(this);
         el.popover({
             content: "loading…", // maybe some loading animation like <img src='loading.gif />
             html: true,
             placement: "auto",
             container: 'body',
             trigger: 'click'
         });
         // show this LOADING popover
         el.popover('show');
         // requesting data from unsing url from data-poload attribute
        var ajaxUrl='/bookings/show-prices?accom_id={{$accom->id}}&date='+event.currentTarget.id;
        $.get(ajaxUrl, function (d) {
            var newContent=d;
            el.data('bs.popover').options.content = newContent;
            var a='';
            el.data('bs.popover').options.title ="{{__('Prices')}}"+" "+"{{$priceUnit}}" +" "+a;
            $('.popover-title').show();
            el.popover('show');
        });
      });
      $('#closePop').click(function(){
        $('.in').remove();
      });
  }
  function closePopUp (p) {
    alert(p);
    $('#'+p).hide();
  }  
  function zabutoPriceCalendar() {
     $("#my-calendar").zabuto_calendar({
        language: "en",
        cell_border: true,
        show_previous: false,
        legend: [
          {type: "text", label: "Available"},
          {type: "list", list: ["label label-success", "label label-danger"]},
          {type: "text", label: "Not Available"}   
        ],
        ajax: {url: "/bookings/avability-dates?accom_id={{$accom->id}}"},
        action_nav: function() { setTimeout(function(){ getTitle() }, 2000); }
     });
  }
  function getBookingPrice() {
     console.log('getBookingPrice');
     $.ajax({
        'url':"/bookings/get-booking-price?accom_id={{$accom->id}}&persons="+$('#bookingNoOfPerson').val()+"&start="+$('#booking_start_date').val()+"&end="+$('#booking_end_date').val(),
        success:function(r){
            console.log(r);
        } 
     });
  }
  $(function(){
    $(window).scroll(function(){
      var aTop = $('.sidebar_holder').height();
      if($(this).scrollTop()>=700){
          $('.sidebar_holder').css({'position':'fixed','top': '60px','width': '340px'});
      }else{
        $('.sidebar_holder').removeAttr( 'style' );
      }
    });
  })
</script>
@endsection