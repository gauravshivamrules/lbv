@extends('layouts.back') 
@section('title',__('Update Slider')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach   
</ul>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>
<div class="well well bs-component">    
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<form class="form-horizontal" action="/accomodations/slider-accomodations" method="post">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Update Slider'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Accommodation'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('accom_id',$accoms,$ids,['name'=>'accom_id[]', 'class'=>'form-control','id'=>'accomID','multiple'=>true] ) !!}                      
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Submit'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
<script type="text/javascript">
		$("#accomID").chosen({disable_search_threshold: 10});
</script>
@endsection