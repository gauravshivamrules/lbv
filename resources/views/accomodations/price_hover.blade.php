@if($priceType->price_unit=='perweek')
	<p>
		<span><b> {{__('Price')}} </b>:&euro;{{$prices->first()->amount}} </span><br> 
		<span><b> {{__('PriceUnit')}} </b>:{{ucfirst($prices->first()->price_unit)}} </span><br> 
		<span><b> {{__('Max Persons')}} </b>:{{$prices->first()->max_persons}} </span><br> 
		<span><b> {{__('Min.Days')}} </b>: {{$prices->first()->min_days}}  </span><br> 
		<span><b> {{__('Change Day')}} </b>: {{$changeDay}}   </span><br> 
		@if($prices->first()->extra_price_per_person)
		<span><b> {{__('Extra Price Per Persons')}} </b>: {{$prices->first()->extra_price_per_person}}  </span><br> 
		@endif
	</p>
@elseif($priceType->price_unit=='perroom') 
	@if($prices)
		<ul class="nav nav-tabs">
			<?php $cnt=0; ?>
			@foreach($prices as $p)
				<li class="<?php if($cnt==0){ echo 'active'; } else { echo ''; } ?>"><a data-toggle="tab" href="#con_{{$p->id}}">{{$p->room_type}} </a></li>
				<?php $cnt++; ?>
			@endforeach
		</ul>
		<div class="tab-content">
			<?php $cnt2=0; ?>
			@foreach($prices as $p)
				<div id="con_{{$p->id}}" class="tab-pane <?php if($cnt2==0){ echo 'fade in active'; } else { echo 'fade'; } ?>">
				  <h4>{{$p->room_type}}</h4>
				  <span><b> {{__('Change Day')}} </b>: {{$changeDay}}  </span><br> 
				  <span><b> {{__('Min.Days')}} </b>: {{$p->min_days}}  </span><br> 
				  <span><b> {{__('Price')}} </b>: &euro;{{$p->room_price[0]->price}}  </span><br> 
				  <span><b> {{__('Adults')}}</b>: {{$p->adults}} </span><br>
				  @if($p->childs)
				  	<span><b> {{__('Childs')}} </b>: {{$p->childs}}  </span><br> 
				  	<span><b> {{__('Max Child Age')}} </b>: {{$p->max_child_age}}  </span><br> 
				  @endif
				  <span><b> {{__('No of Rooms')}} </b>: {{$p->no_of_rooms}}  </span><br> 
				  @if($p->max_adults_per_room)
				  	<span><b> {{__('Max Adults Per Room')}} </b>: {{$p->max_adults_per_room}}  </span><br> 
				  @endif 
				  @if($p->max_persons_per_room)
				  	<span><b> {{__('Max Persons Per Room')}} </b>: {{$p->max_persons_per_room}}  </span><br> 
				  @endif 
				  <p>{{$p->description}}</p>
				</div>
			<?php $cnt2++; ?>
			@endforeach
		</div>  
	@endif
@elseif($priceType->price_unit=='perperson')
	<ul class="nav nav-tabs">
		<?php $cnt=0; ?>
		@foreach($prices as $p)
			<li class="<?php if($cnt==0){ echo 'active'; } else { echo ''; } ?>"><a data-toggle="tab" href="#con_{{$p->id}}">{{$p->room_type}} </a></li>
			<?php $cnt++; ?>
		@endforeach
	</ul>
	<div class="tab-content">
		<?php $cnt2=0; ?>
		@foreach($prices as $p)
			<div id="con_{{$p->id}}" class="tab-pane <?php if($cnt2==0){ echo 'fade in active'; } else { echo 'fade'; } ?>">
			  <h4>{{$p->room_type}}</h4>
			  <span><b> {{__('Change Day')}} </b>: {{$changeDay}}  </span><br> 
			  <span><b> {{__('Min.Days')}} </b>: {{$p->min_days}}  </span><br> 
			  <span><b> {{__('1 Person/Night')}} </b>:&euro;{{$p->room_price[0]->price1}} </span><br> 
			  <span><b> {{__('2 Person/Night')}} </b>:&euro;{{$p->room_price[0]->price2}}  </span><br>
			  <span><b> {{__('Family/Night')}} </b>:&euro;{{$p->room_price[0]->price3}}  </span><br>
			  <span><b> {{__('Adults')}}</b>: {{$p->adults}} </span><br>
 			    @if($p->childs)
				  	<span><b> {{__('Childs')}} </b>: {{$p->childs}}  </span><br> 
				  	<span><b> {{__('Max Child Age')}} </b>: {{$p->max_child_age}}  </span><br> 
				@endif
			  <span><b> {{__('No of Rooms')}} </b>: {{$p->no_of_rooms}}  </span><br> 
			  @if($p->max_adults_per_room)
			  	<span><b> {{__('Max Adults Per Room')}} </b>: {{$p->max_adults_per_room}}  </span><br> 
			  @endif 
			  @if($p->max_persons_per_room)
			  	<span><b> {{__('Max Persons Per Room')}} </b>: {{$p->max_persons_per_room}}  </span><br> 
			  @endif
			  @if($p->extra_price_adult)
			  	<span><b> {{__('Extra Price Adult')}} </b>: {{$p->extra_price_adult}}  </span><br> 
			  @endif
			  @if($p->extra_price_child)
			  	<span><b> {{__('Extra Price Child')}} </b>: {{$p->extra_price_child}}  </span><br> 
			  @endif
			  
			  <p>{{$p->description}}</p>
			</div>
		<?php $cnt2++; ?>
		@endforeach
	</div> 




@endif
 


