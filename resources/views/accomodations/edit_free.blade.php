@extends('layouts.back') 
@section('title',__('Update Accommodation')) 
@section('content')

<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
    20%
  </div>
</div> 
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul> 
<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-6">
				<form class="form-horizontal"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Current Information'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{$accom->name}}" readonly="true"  required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',$accomTypes,$accom->accomodation_type_id,['class'=>'form-control','required'=>true,'disabled'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
							<div class="col-lg-8">
								<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{$accom->email}}" readonly="true" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
							<div class="col-lg-8">
								<input name="website" class="form-control"  type="url" id="AccomodationWebsite" value="{{$accom->website}}" readonly="true" placeholder="{{__('https://www.mywebsite.com')}}">                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" readonly="true" value="{{$accom->number_of_persons}}" required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Bathrooms'); ?> </label>
							<div class="col-lg-8">
								<input name="bathrooms" class="form-control"  type="number" id="AccomodationBathrooms" readonly="true"  min="0" max="50" value="{{$accom->bathrooms}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Bedrooms'); ?></label>
							<div class="col-lg-8">
								<input name="bedrooms" class="form-control"  type="number" id="AccomodationBedRooms"  readonly="true" min="0" max="50" value="{{$accom->bedrooms}}"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Single Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="single_beds" class="form-control"  type="number" id="AccomodationSingleBeds" readonly="true"   min="0" max="50" value="{{$accom->single_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Double Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="double_beds" class="form-control"  type="number" id="AccomodationDoubleBeds"  readonly="true" min="0" max="50" value="{{$accom->double_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								{{$accom->description}}
							</div>
						</div>
					</fieldset>
				</form>
			</div>   
			<div class="col-md-6">
				<div id="progressbar"></div> 

				<form class="form-horizontal" action="/accomodations/edit-free/{{$accom->id}}" method="post">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('New Information'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="AccomodationName" value="{{$accom->name}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Category'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',$accomTypes,$accom->accomodation_type_id,['class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Email'); ?> </label>
							<div class="col-lg-8">
								<input name="email" class="form-control"  type="email" id="AccomodationEmail" value="{{$accom->email}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Website'); ?> </label>
							<div class="col-lg-8">
								<input name="website" class="form-control"  type="url" id="AccomodationWebsite" value="{{$accom->website}}" placeholder="{{__('https://www.mywebsite.com')}}">                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" value="{{$accom->number_of_persons}}" required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Max. Persons'); ?> </label>
							<div class="col-lg-8">
								<input name="number_of_persons" class="form-control"  type="number" min="0" max="50" id="AccomodationNumberOfPersons" value="{{$accom->number_of_persons}}"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Bathrooms'); ?> </label>
							<div class="col-lg-8">
								<input name="bathrooms" class="form-control"  type="number" id="AccomodationBathrooms"  min="0" max="50" value="{{$accom->bathrooms}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Bedrooms'); ?></label>
							<div class="col-lg-8">
								<input name="bedrooms" class="form-control"  type="number" id="AccomodationBedRooms"  min="0" max="50" value="{{$accom->bedrooms}}"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Single Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="single_beds" class="form-control"  type="number" id="AccomodationSingleBeds"   min="0" max="50" value="{{$accom->single_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Double Beds'); ?> </label>
							<div class="col-lg-8">
								<input name="double_beds" class="form-control"  type="number" id="AccomodationDoubleBeds"  min="0" max="50" value="{{$accom->double_beds}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								<textarea class="form-control" name="description">{{$accom->description}}</textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<a href="/accomodations/edit-accom-price/{{$accom->id}}?type={{$accom->accomodation_type_id}}" class="btn btn-info">{{__('Skip')}}</a>
							<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Next'); ?></button> 
						</div>
					</div>
				</form>

			</div>
		</div>
</div> 
@endsection