@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
             <img src="{{asset('images/lbv.png')}}">
        @endcomponent 
    @endslot
    {{ __('Hi')}} <br>
	{{__($msg)}}. <br>
    <a href="{{env('SITE_URL')}}/login">{{__('Please Login to your account') }}</a>
    @slot('footer')   
        @component('mail::footer') 
        	{{__('Thanks')}},<br>  
        	{{ env('SITE_TITLE_FRONT') }} 
        @endcomponent
    @endslot
@endcomponent


