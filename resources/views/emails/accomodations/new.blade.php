@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot
    {{ __('Hi')}} 
	<p>{{__($msg) }}.</p>
	 {{__('Accomodation')}}: {{ucfirst($accom->name)}}  {{ucfirst($accom->accomodation_address->country_id)}}
	 {{__('Advertiser') }} : {{ucfirst($accom->user->first_name)}}  {{ucfirst($accom->user->last_name)}} 

    @slot('footer')
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


