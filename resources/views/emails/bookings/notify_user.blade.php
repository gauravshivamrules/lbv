@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot
    {{ __('Hi')}}  {{$user->first_name}}  {{$user->last_name}} <br> 
	{{__($msg)}}.  <br>
    @if(isset($url))
        @component('mail::button', ['url' =>$url])
                {{__('Login')}}
        @endcomponent
    @endif
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent 
    @endslot 
@endcomponent 


