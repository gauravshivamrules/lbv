@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            {{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
    {{ __('Hi')}},{{$user->first_name}} {{$user->last_name}} <br> 
	 {{__($msg)}}  <br>
     
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


