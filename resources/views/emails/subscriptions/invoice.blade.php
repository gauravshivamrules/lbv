@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
             <img src="{{asset('images/lbv.png')}}"> 
        @endcomponent
    @endslot
    {{ __('Hi')}},{{$user->first_name}} {{$user->last_name}} <br> 
	 {{__('Thanks for paying by bank transfer, your payment will me marked paid after approval from admin.Please find enclosed invoice for your payment.')}}  <br>
     
    @slot('footer') 
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


