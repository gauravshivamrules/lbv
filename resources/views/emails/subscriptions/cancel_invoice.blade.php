@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
             <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot 
    {{ __('Hi')}},{{$user->first_name}} {{$user->last_name}} <br> 
	 {{__('Your payment by bank transfer has been cancelled.Please find enclosed invoice for your payment.')}}  <br> 
     
    @slot('footer') 
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


