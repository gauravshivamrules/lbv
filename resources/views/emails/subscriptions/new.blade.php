@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot
    {{ __('Hi')}} <br>
	 {{__('A new subscription bank transfer request has been received for property ')}}: {{ucfirst($accom->name)}}  <br>
     {{__('Country')}}: {{ucfirst($accom->accomodation_address->country_id)}}
     <br>
	 {{__('Advertiser') }} : {{ucfirst($accom->user->first_name)}}  {{ucfirst($accom->user->last_name)}} 

    @slot('footer') 
        @component('mail::footer') 
        	{{__('Thanks')}},<br>
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


