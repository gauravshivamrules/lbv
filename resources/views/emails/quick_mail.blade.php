@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
             <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot
    {{ __('Hi')}} <br>
	{{__('New quick mail request has been received by advertiser')}} {{ucfirst($user->first_name)}} <br>  
    {{__('Sender Name')}}:  {{ucfirst($user->first_name)}}  {{ucfirst($user->last_name)}} <br> 
    {{__('Subject')}}: {{$advSubject}} <br> 
    {{__('Message')}}:  {{$msg}}
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}},<br>  
        	{{ env('SITE_TITLE_FRONT') }} 
        @endcomponent
    @endslot
@endcomponent  


