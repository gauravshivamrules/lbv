@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            <img src="{{asset('images/lbv.png')}}">
        @endcomponent
    @endslot 
    {{ __('Hi')}},{{$user->first_name}} {{$user->last_name}} <br> 
	{{__('Congratulations,you have got a free promotion coupon.You can use this coupon to buy new promotion.')}}  <br>
    <b>{{__('Coupon code')}}:</b> {{$coupon}} <br>
    <b>{{__('Promotion')}}:  {{$product}} </b> <br>
    <b>{{__('Accomodation')}}:</b> {{$accom}}
    @slot('footer') 
        @component('mail::footer') 
        	{{__('Thanks')}},<br> 
        	{{ env('SITE_TITLE_FRONT') }}
        @endcomponent
    @endslot
@endcomponent


