@extends('layouts.back') 
@section('title',__('Buy Promotions')) 
@section('content')

@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
	<div id="cancelPaypalPayment" class="callout callout-danger" style="display:none;"></div>
	<!-- <div class="callout callout-success"> -->
		<h4></h4>
	<!-- </div>  -->
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">

				<p> <?php echo __('You can Buy New Promotion Here')?></p> <br>

				<p> * <?php echo __('Tip of the month, this allows you for 1 month with a large picture at the top of the home page.'); ?></p>
				<p> * <?php echo __('In the picture, this is you and 5 other for 1 month in the middle of the home page.'); ?> </p>
				<p> * <?php echo __('Last minute, this can indicate a last minute for 1 month promotion.'); ?> </p>
				<p> * <?php echo __('News Visitors always keep novelties, with this module you can also find useful in games.'); ?> </p>
				<p> * <?php echo __('Any accommodation that are purchased a promotion comes automatically in the newsletter that month.'); ?> </p> <br>
				<p style="font-weight: bold;"><?php echo __('Caution!'); ?> </p>
				<p> * <?php echo __('For "no cure no pay" advertising is not permitted to refer to the property name, location, or other contact data.If it is determined that there will be contact data is in, it will be modified or removed the promotion without the advertiser have to recover the amount paid.'); ?> 
				</p>

				<div class="panel panel-default">
				    <div class="panel-heading">{{__('Select promotions to buy')}} </div> 
				    <div class="panel-body">
				    	@foreach($category as $c)
				    	<span> <input type="radio" name="category_id"  value="{{$c->id}}"> </span>
				    	<span> {{$c->name}} </span>
				    	&nbsp;&nbsp;
				    	@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-1"></div> 
		</div>
		<div class="row" id="promotionsRow" style="display:none"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10" id="appendPromotions">
				<form action="/promotions/checkout" method="post">   
					{{csrf_field()}}
					<table class="table table-bordered">
						<thead> 
							<tr>
								<th> # </th>
								<th> <?php echo __('Name'); ?> </th>
								<th> <?php echo __('Description'); ?> </th>
								<th> <?php echo __('Amount'); ?> (&euro;) </th>
								<th> <?php echo __('Month'); ?> </th>
								<th> <?php echo __('Year'); ?> </th>
								<th> <?php echo __('Select'); ?> </th>
							</tr>
						</thead>
						<tbody id="promotionsTableBody">
							
						</tbody>
					</table>
					<input type="hidden" name="pro[]" id="hPro">
					<div class="form-actions right">
						<a href="javascript:;" class="btn btn-danger" id="bucket" style="display:none"> </a>
						<a href="javascript:;" class="btn btn-default" id="clearBucket" style="display:none"> {{__('Clear Bucket')}} </a>
						<button type="submit" class="btn green"><?php echo __('Checkout'); ?> </button>
					</div>
				</form>
			</div>
			<div class="col-md-1"></div>   
		</div>
		 
		
</div> 
<script type="text/javascript">
	BUCKET=[];
	COUNTBucket=BUCKET.length;
	$(document).ready(function() {
		//showChecked(BUCKET);
		$('input[name="category_id"]').change(function(){ 
			elemId=$('input[name=category_id]:checked').val();
		    $.ajax({
		    	url:'/promotions/get-promotion?id='+elemId,
		    	dataType:'json',
				success:function(p) {
					$('#promotionsRow').show();
		    		 //p=$.parseJSON(p); 
		    		$('#promotionsTableBody').html('');
		    		if(p.success) {
		    			c=1;  
		    			$.each( p.products, function( key, value ) {
		    				if(value.product_month=="") {
		    					value.product_month=' 4 Weeks';
		    				}
		    				if(value.product_year=="") {
		    					var dteNow = new Date();
		    					var intYear = dteNow.getFullYear();
		    					value.product_year=intYear; 
		    				}
		    				if(elemId==3 || elemId==5) { 
		    					value.product_month='4 weeks';
		    					value.product_year='-----';
		    				} 
		    				var row='<tr><td>'+c+'</td><td> '+value.product_name+'</td><td>'+value.product_description+'</td><td>'+value.product_price+'</td><td>'+value.product_month+'</td><td>'+value.product_year+'</td><td> <input type="checkbox" name="proo[]" id="'+value.id+'" value="'+value.id+'" /> </td></tr>'; 
		    			  // var row='<tr><td>'+c+'</td><td> '+value.product_name+'</td><td>'+value.product_description+'</td><td>'+value.product_price+'</td><td>'+value.product_month+'</td><td>'+value.product_year+'</td><td > <input type="checkbox" name="'+value.id+'" value="'+value.id+'" /> </td></tr>'; 
		    			  $('#promotionsTableBody').append(row); 	
		    			  c++;   
		    			}); 
		    		} else {
		    			var row='<tr><td colspan="7" style="text-align:center;"><?php echo __('No Promotions Found') ?> </td></tr>'; 
		    			$('#promotionsTableBody').append(row);	
		    		}  
		    	}
		    });
			showChecked(BUCKET);
		});
		$(document).on("change", "input[type='checkbox']",function(){
		    if (this.checked) {
		    	console.log($.inArray($(this).val(),BUCKET));
		    	if($.inArray($(this).val(),BUCKET)==-1) {
		    		BUCKET.push($(this).val());
			    	COUNTBucket=BUCKET.length;
			    	$('#bucket').show();
			    	$('#clearBucket').show();
			    	$('#bucket').html(COUNTBucket+ "{{ __('In bucket') }}");
		    	} else {
		    		alert("{{__('You have already added that promotion to your bucket')}}");
		    		$("#"+$(this).val()).prop('checked',false);
		    	}
		    } else {
		    	BUCKET.pop($(this).val());
		    	COUNTBucket=BUCKET.length;
		    	$('#bucket').show();
		    	$('#clearBucket').show();
		    	$('#bucket').html(COUNTBucket+ "{{ __('In bucket') }}");
		    }
		    console.log('BUCKET');
		    console.log(BUCKET);
		    $('#hPro').val(BUCKET);
		    //showChecked(BUCKET); 
		});

		$('#clearBucket').click(function(){
			BUCKET=[];
			COUNTBucket=BUCKET.length;
			$('#bucket').html(COUNTBucket+ "{{ __('In bucket') }}");
		});

		function showChecked(checked){
			if(checked){
				console.log('checked');
				console.log(checked);
				$.each(checked,function(i,v){
					console.log(v);
					$("#"+v).prop('checked',true);
				});
			}
		}

	});

</script>

@endsection