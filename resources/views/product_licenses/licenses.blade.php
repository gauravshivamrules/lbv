@extends('layouts.back')
@section('title',_('Promotion Licenses'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Promotion Licenses') }} </h2> 
		</div>


		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#showTips"><span class="label label-success" id="tips"></span> {{__('Tips')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showSpots"><span class="label label-warning" id="spots"></span> {{__('Spotlights')}}</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showLastMinutes"><span class="label label-primary" id="lastminutes"></span> {{_('Last Minutes')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showNews"><span class="label label-info" id="news"></span> {{__('News')}} </a> 
			</li>
		</ul> 

		<div class="tab-content"> 
			<div id="showTips" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="tipsTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showSpots" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="spotsTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			<div id="showLastMinutes" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="minutesTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			<div id="showNews" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="newsTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

		</div>
		

			
	</div>
		<script>
			$(function() {
				buildTable('tipsTable','tips',1);  
				buildTable('spotsTable','spots',2);  
				buildTable('minutesTable','lastminutes',5);  
				buildTable('newsTable','news',3);   
			    /*$('#tipsTable').DataTable({ 
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/promotions-license/get-all-licenses-ajax?type=1",     
			        columns: [
			            { data: 'product_name', name: 'product_name' },
			            { data: 'product_price', name: 'product_price' },
			            { data: 'vFrom', name: 'vFrom' }, 
			            { data: 'vUpto', name: 'vUpto' }, 
			            { data: 'pName', name: 'pName' }, 
			            { data: 'payment_complete', name: 'payment_complete'},    
			            { data: 'invoice', name: 'invoice' }, 
			            { data: 'aName', name: 'aName'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });*/
			}); 
			function buildTable (table,tCount,type) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/promotions-license/get-all-licenses-ajax?type_id="+type,
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [
				        { data: 'product_name', name: 'product_name' },
				        { data: 'total_amount', name: 'total_amount' },
				        { data: 'vFrom', name: 'vFrom' }, 
				        { data: 'vUpto', name: 'vUpto' },  
				        { data: 'pName', name: 'pName' }, 
				        { data: 'payment_complete', name: 'payment_complete'},    
				        { data: 'invoice', name: 'invoice' }, 
				        { data: 'uFName', name: 'uFName' }, 
				        { data: 'aName', name: 'aName'},    
				        { data: 'action', name: 'action'}    
				    ]
				});
			} 
			function confirmAction(url) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href=url;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection