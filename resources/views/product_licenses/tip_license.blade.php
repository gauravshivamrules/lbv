@extends('layouts.back')
@section('title',_('All Tip Licenses'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Tip Licenses') }} </h2> 
		</div>


		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#showActive"><span class="label label-success" id="active"></span> {{__('Active')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showInactive"><span class="label label-warning" id="inactive"></span> {{__('Inactive')}}</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showExpired"><span class="label label-danger" id="expired"></span> {{_('Expired')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#shosCancelled"><span class="label label-info" id="cancelled"></span> {{__('Cancelled')}} </a> 
			</li>
			<li>
				<a data-toggle="tab" href="#showNewTransfers"><span class="label label-primary" id="newtransfers"></span> {{__('New Transfers')}} </a> 
			</li>
		</ul> 

		<div class="tab-content"> 
			<div id="showActive" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showInactive" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="inActiveTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showExpired" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="expiredTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="shosCancelled" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="cancelledTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showNewTransfers" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="newTransferTable">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>     
								<th><?php echo __('Price'); ?>(&euro;)</th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('User'); ?></th>
								<th><?php echo __('Accomodation'); ?></th>
								<th><?php echo __('Actions'); ?></th>  
							</tr>
						</thead> 
					</table>
				</div>
			</div>

		</div>
		

			
	</div>
		<script>
			$(function() {
				buildTable('activeTable','active',1);  
				buildTable('inActiveTable','inactive',2);  
				buildTable('expiredTable','expired',3);  
				buildTable('cancelledTable','cancelled',4);   
				buildTable('newTransferTable','newtransfers',5);  
			    /*$('#tipsTable').DataTable({ 
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/promotions-license/get-all-licenses-ajax?type=1",     
			        columns: [
			            { data: 'product_name', name: 'product_name' },
			            { data: 'product_price', name: 'product_price' },
			            { data: 'vFrom', name: 'vFrom' }, 
			            { data: 'vUpto', name: 'vUpto' }, 
			            { data: 'pName', name: 'pName' }, 
			            { data: 'payment_complete', name: 'payment_complete'},    
			            { data: 'invoice', name: 'invoice' }, 
			            { data: 'aName', name: 'aName'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });*/
			}); 
			function buildTable (table,tCount,type) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true, 
				    serverSide: true,
				    ajax: "/promotions-license/get-catwise-products?type_id="+type,
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [
				        { data: 'product_name', name: 'product_name' },
				        { data: 'product_price', name: 'product_price' },
				        { data: 'vFrom', name: 'vFrom' }, 
				        { data: 'vUpto', name: 'vUpto' },  
				        { data: 'pName', name: 'pName' }, 
				        { data: 'payment_complete', name: 'payment_complete'},    
				        { data: 'invoice', name: 'invoice' }, 
				        { data: 'uFName', name: 'uFName' }, 
				        { data: 'aName', name: 'aName'},    
				        { data: 'action', name: 'action'}    
				    ] 
				});
			} 
			function confirmAction(url) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href=url;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection