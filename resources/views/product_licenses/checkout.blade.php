@extends('layouts.back') 
@section('title',__('Promotions Checkout')) 
@section('content')

@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
	<div id="cancelPaypalPayment" class="callout callout-danger" style="display:none;"></div>
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">  
				<div class="panel panel-default">
				    <div class="panel-heading">{{__('Promotions Checkout')}} </div> 
				    <div class="panel-body">
				    	<table class="table table-bordered">
				    		<thead> 
				    			<tr>
				    				<th> # </th>
				    				<th> <?php echo __('Name'); ?> </th>
				    				<th> <?php echo __('Description'); ?> </th>
				    				<th> <?php echo __('Amount'); ?> (&euro;) </th>
				    				<th> <?php echo __('Valid upto'); ?> </th>
				    			</tr>
				    		</thead>
				    		<tbody id="promotionsTableBody">
				    			<?php $counter=1;$totalAmount=0;$productID=$productCategory=$productMonth=$productYear=null;
				    					$productIdArr=$productMonthStr=$productYearStr='';
				    			?>
				    			@foreach($products as $p)
				    			<tr>
				    				<td> 
				    					{{$counter++}} 
				    					<?php 
				    						$productIdArr .=$p->id.',';
				    						$productMonthStr .=$p->product_month.',';
				    						$productYearStr .=$p->product_year.',';
				    						$productID=$p->id;
				    						$productCategory=$p->product_category_id;
				    						$productMonth=$p->product_month;
				    						$productYear=$p->product_year;  
				    					?> 
				    				</td> 
				    				<td> {{$p->product_name}} </td>
				    				<td> {{$p->product_description}} </td>
				    				<td> 
				    					{{$p->product_price}} 
				    					<?php  $totalAmount=$totalAmount+$p->product_price ?>
				    				</td>
				    				<td>
				    					@if($p->product_category_id==1 || $p->product_category_id==2)
				    						{{__('1 Month')}}
				    					@else
				    						{{__('4 Weeks')}}
				    					@endif 
				    				</td>
				    			</tr>  
				    			@endforeach
				    			<tr> 
				    				<td colspan="5" style="text-align:right"> <b>{{__('Total amount')}}:</b> &euro;{{$totalAmount}}  </td>
				    			</tr>
				    		</tbody>
				    	</table>
				    	@if($accoms)
				    	<div id="accomDiv"> 
				    		<div class="form-group">
				    			<label for="name" class="col-lg-2 control-label"> {{__('Accommodation')}}</label> 
				    			<div class="col-lg-10">
				    				{!! Form::select('accom_id',$accoms,null,['id'=>'accomID','class'=>'form-control','required'=>true,'title'=>'Accomodation'] ) !!} 
				    			</div>
				    		</div>
				    	</div>
				    	<br> 
				    	<div id="paymentForm"> 

	    			    	<div class="row"> 
	    			    		<div class="col-md-1"></div>   
	    			    		<div class="col-md-10">
	    			    			<fieldset>
	    			    				<legend><?php echo __('Select Payment'); ?></legend> 
	    			    					<div class="col-xs-4"> 
	    			    						<div class="form-group">
	    			    							<div class="col-lg-10 col-lg-offset-2"> 
	    			    								<form method="post" action="/promotion-licenses/pay-pal" id="subscriptionPaypalForm"> 
	    			    									{{csrf_field()}} 
	    			    									<input type="hidden" name="productIds" value="{{rtrim($productIdArr,',')}}">
	    			    									<input type="hidden" name="productMonths" id="productMonth" value="{{rtrim($productMonthStr,',')}}">
	    			    									<input type="hidden" name="productYears" id="productYear" value="{{rtrim($productYearStr,',')}}">
	    			    									<input type="hidden" name="productCategory" id="productCategory" value="{{$productCategory}}">
	    			    									<input type="hidden" name="accom_id" id="accom_id">
	    			    									<input type="hidden" name="cart_id" id="cartId">
	    			    									<input type="hidden" name="amount_paid" id="amountPaid">
	    			    									<input type="hidden" name="txn_id" id="txnId">
	    			    									<input type="hidden" name="payer_email" id="payerEmail">
	    			    									<input type="hidden" name="payer_first_name" id="payerFirstName">
	    			    									<input type="hidden" name="payer_last_name" id="payerLastName">
	    			    									<input type="hidden" name="transaction_json" id="transactionJson">
	    			    								</form>
	    			    								<div id="paypal-button"> </div>
	    			    							</div>
	    			    						</div>	
	    			    					</div> 
	    			    					<div class="col-xs-4">
	    			    						<div class="form-group">
	    			    							<div class="col-lg-10 col-lg-offset-2"> 
	    			    								<form method="post" action="/promotion-licenses/mollie" id="promotionMollieForm" onsubmit="return confirm('Are you sure?');"> 
	    			    									{{csrf_field()}}  
	    			    									<input type="hidden" name="productIds" value="{{rtrim($productIdArr,',')}}">
	    			    									<input type="hidden" name="productMonths" id="productMonth" value="{{rtrim($productMonthStr,',')}}">
	    			    									<input type="hidden" name="productYears" id="productYear" value="{{rtrim($productYearStr,',')}}">
	    			    									<input type="hidden" name="productCategory" id="productCategory" value="{{$productCategory}}">
	    			    									<input type="hidden" name="accom_id" id="accomIdMollie">
	    			    									<input type="hidden" name="amount_paid" id="amountPaid" value="{{$totalAmount}}">
	    			    									<button type="submit" class="btn green"> 
	    			    											{{__('Checkout')}}
	    			    									</button>	  

	    			    								</form>
	    			    							</div> 
	    			    						</div>
	    			    					</div>
	    			    					<div class="col-xs-4"> 
	    			    						<form method="post" action="/promotion-licenses/bank-transfer" onsubmit="return confirm('Are you sure?');">
	    			    							{{csrf_field()}}
	    			    						<div class="form-group">
	    			    							<div class="col-lg-10 col-lg-offset-2" style="margin-top:-15px;">
	    			    								<input type="hidden" name="productIds" value="{{rtrim($productIdArr,',')}}">
	    		    									<input type="hidden" name="productMonths" id="productMonth" value="{{rtrim($productMonthStr,',')}}">
	    		    									<input type="hidden" name="productYears" id="productYear" value="{{rtrim($productYearStr,',')}}">
	    		    									<input type="hidden" name="productCategory" id="productCategory" value="{{$productCategory}}">
	    			    								<input type="hidden" name="accom_id" id="accomIdTransfer" >
	    			    								<input type="hidden" name="amount_paid" id="amountPaid" value="{{$totalAmount}}"> 
	    			    								<!-- <button type="submit"   class="btn btn-primary btn-sm"> 
	    			    									<img src="/images/wire-transfer.png" style="width: 80px;">
	    			    								</button>	 -->
	    			    							</div>
	    			    						</div>
	    			    						</form>
	    			    					</div>
	    			    			</fieldset>

	    			    		</div>
	    			    		<div class="col-md-1"></div>
	    			    	</div>
	    			    	<div class="row"> 
	    			    		<div class="col-md-1"></div>   
	    			    		<div class="col-md-10">
	    			    			<input type="checkbox" name="freeCoupon" id="freeCoupon"> {{__('I have a free promotion coupon')}}
	    			    		</div>
	    			    		<div class="col-md-1"></div>   
	    			    	</div>
				    	</div>
				    	<div class="row"  id="showCoupon" style="display:none">
				    		<div class="col-md-2"></div>   
				    		<div class="col-md-10"> 
				    			<form class="form-horizontal" action="/promotion-licenses/use-free-license" method="post">   
				    				<input type="hidden" name="_token" value="{!! csrf_token() !!}">
				    				<fieldset> 
				    					<legend><?php echo __('Enter Coupon Code'); ?></legend> 
				    						<div> 
				    							<div class="form-group">
				    								<div class="col-lg-10 col-lg-offset-2">
				    									<input type="hidden" name="accomId" id="couponAccomId">   
				    									<input type="text" name="token" class="form-control" required>   
				    								</div>
				    							</div>
				    						</div>  
				    						
				    						<div class="form-group">
				    							
				    							<div class="col-lg-10 col-lg-offset-2">
				    								<a href="javascript:;" id="backBTN" class="btn btn-primary"> {{__('Back')}} </a>
				    								&nbsp; &nbsp;  
				    								<button type="submit" class="btn btn-primary" id="cont">{{__('Continue')}}</button>
				    							</div>
				    						</div>
				    						<span> </span>
				    				</fieldset>
				    			</form>
				    		</div> 
				    	</div>
				    	@endif
					</div>
				</div>
			</div>
			<div class="col-md-1"></div> 
		</div>	
</div> 

<script type="text/javascript">
		$('#accomIdMollie').val($('#accomID').val());
		$('#accomIdTransfer').val($('#accomID').val());
		$('#couponAccomId').val($('#accomID').val());

		$('#accomID').change(function() {
			$('#accomIdMollie').val($('#accomID').val());
			$('#accomIdTransfer').val($('#accomID').val());
			$('#couponAccomId').val($('#accomID').val());
		});

		$('#freeCoupon').change(function() {
	       if($(this).is(":checked")) {
	       		$('#showCoupon').show();
	       		$('#paymentForm').hide(); 
	       } 
		});
		$('#backBTN').click(function() {
			$('#showCoupon').hide(); 
			$('#paymentForm').show();
			$('#freeCoupon').prop('checked', false); 
		});


	
</script>

@endsection