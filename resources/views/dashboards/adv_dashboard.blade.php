@extends('layouts.back')
@section('title',__('Advertiser Dashboard')) 
@section('content')
<link rel="stylesheet" href="/css/morris.css">
<link rel="stylesheet" href="/css/ionicons.min.css">
<link rel="stylesheet" href="/css/jquery-jvectormap.css">
<script src="/js/raphael.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/jquery.knob.min.js"></script>
<script src="/js/dashboard.js"></script> 
<section class="content-header">
   <h1>
      <?php echo __('Dashboard') ?>
      <small> <?php echo __('Control panel') ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="javascript:;"><i class="fa fa-dashboard"></i> <?php echo __('Home') ?></a></li>
      <?php echo __('Dashboard') ?>
   </ol>
</section>
<!-- Small boxes (Stat box) -->
<div class="row">
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
         <div class="inner">
            <h3>00 </h3>
            <p>{{__('Messages')}}</p>
         </div>
         <div class="icon">
            <i class="fa fa-envelope"></i>
         </div>
         <a href="/tickets/adv-message" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
         <div class="inner">
            <h3>00<sup style="font-size: 20px"></sup></h3>
            <p>{{__('My Bookings')}} </p>
         </div>
         <div class="icon"> 
            <i class="ion ion-stats-bars"></i>
         </div>
         <a href="/bookings/adv-bookings" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
         <div class="inner">
            <h3>00</h3>
            <p>{{__('Visitors This Month')}}</p>
         </div>
         <div class="icon">
            <i class="ion ion-pie-graph"></i>
         </div>
         <a href="/social-media/statistics" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
         <div class="inner">
            <h3>{{$accoms}} </h3>
            <p>{{__('My Accomodations')}}</p>
         </div>
         <div class="icon">
            <i class="fa fa-home"></i>
         </div>
         <a href="/accomodations/my-accomodations" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
</div>
<!-- /.row -->
<div class="row">
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
         <div class="inner">
            <h3>{{$subscriptions}} </h3>
            <p>{{__('My Active Subscriptions')}}</p>
         </div>
         <div class="icon">
            <i class="fa fa-newspaper-o"></i>
         </div>
         <a href="/subscription-licenses" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
         <div class="inner">
            <h3>{{$promotions}} <sup style="font-size: 20px"></sup></h3>
            <p>{{__('My Active Promotions')}} </p>
         </div>
         <div class="icon">
            <i class="fa fa-product-hunt"></i>
         </div>
         <a href="/promotions/my-promotions" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
</div>

<!-- Main row -->
<div class="row">
   <!-- Left col -->
   <section class="col-lg-12 connectedSortable ui-sortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div style="cursor: move;">
         <div class="nav-tabs-custom" >
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right ui-sortable-handle">
               <li class="pull-left header"><i class="fa fa-newspaper-o"></i> {{__('Accomodation Visits This Week')}}</li>
            </ul>
         </div>
         <div id="visitsChart" style="height: 250px;"></div>
         <p id="showNoVisitFound" style="display:none;">{{__('No Data Found')}}</p>
      </div>
   </section>
</div>
<div class="row">
  <section class="col-lg-7 connectedSortable ui-sortable">
      <div class="box box-info">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
          <i class="fa fa-envelope"></i>

          <h3 class="box-title">Quick Email To Admin</h3> 
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
              <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form action="/adv-dashboard/quick-mail" method="post"> 
              {{csrf_field()}} 
              <div class="form-group">
                <input type="text" class="form-control" name="subject" placeholder="Subject" required>
               <span class="error"> {{ $errors->first('subject') }}</span> 
              </div>
              <div>
               
            </ul>
            <textarea class="summernoteA" name="message" id="content" required>
            </textarea>
            <span class="error"> {{ $errors->first('message') }}</span>

          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
            <i class="fa fa-arrow-circle-right"></i></button>
          </div>
          </form>
        </div>
  </section>
</div>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<!-- /.row (main row) -->
<script type="text/javascript">
    $('.summernoteA').summernote({
         height: 250, 
    });
   drawVisitsChart();
  /* $(window).resize(function() {
      window.drawSubscriptionsChart.redraw();
      window.drawPromotionsChart.redraw();
      window.drawVisitsChart();
   });*/
   
  function drawSubscriptionsChart() {
     var subData=<?php echo $dataSub ?>;
        if(subData){
           new Morris.Area({
             element: 'subscriptionsChart',
             data:subData,
             xkey: 'year',
             ykeys: ['value'], 
             labels: ['€'],
             resize: true,
             redraw: true
           });
        } else{
           $('#showNoSubFound').show();
        }
  }
  function drawPromotionsChart() {
     var proData=<?php echo $dataPro ?>;
     if(proData){
        new Morris.Line({
          element: 'promotionsChart',
          data: proData,
          xkey: 'year',
          ykeys: ['value'],
          labels: ['€']
        });
     }else{
        $('#showNoPromoFound').show();
     }
  }
  function drawVisitsChart() {
    var visitData=<?php echo $accomVisits ?>;
     if(visitData){
        Morris.Area({
          element: 'visitsChart',
          data:visitData,
          xkey: 'y',
          ykeys: ['v',],
          labels: ['Total Visits']
        });

      }else{
         $('#showNoVisitFound').show();
      }
  } 
</script>
@endsection

