@extends('layouts.back')
@section('title',__('Admin Dashboard')) 
@section('content')
<link rel="stylesheet" href="/css/morris.css">
<link rel="stylesheet" href="/css/ionicons.min.css">
<link rel="stylesheet" href="/css/jquery-jvectormap.css">
<script src="/js/raphael.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/jquery.knob.min.js"></script>
<script src="/js/dashboard.js"></script> 
<section class="content-header">
   <h1>
      <?php echo __('Dashboard') ?>
      <small> <?php echo __('Control panel') ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="javascript:;"><i class="fa fa-dashboard"></i> <?php echo __('Home') ?></a></li>
      <?php echo __('Dashboard') ?>
   </ol>
</section>
<!-- Small boxes (Stat box) -->
<div class="row">
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
         <div class="inner">
            <h3>{{$accoms}}</h3>
            <p>{{__('Accomodations')}}</p>
         </div>
         <div class="icon">
            <i class="fa fa-home"></i>
         </div>
         <a href="/accomodations" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
         <div class="inner">
            <h3>00<sup style="font-size: 20px"></sup></h3>
            <p>{{__('Bookings')}} </p>
         </div>
         <div class="icon">
            <i class="ion ion-stats-bars"></i>
         </div>
         <a href="javascript:;" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
         <div class="inner">
            <h3>{{$users}}</h3>
            <p>{{__('User Registrations')}}</p>
         </div>
         <div class="icon">
            <i class="ion ion-person-add"></i>
         </div>
         <a href="/users" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
         <div class="inner">
            <h3>{{$visitorsThisMonth?$visitorsThisMonth:0}}</h3>
            <p>{{__('Visitors This Month')}}</p>
         </div>
         <div class="icon">
            <i class="ion ion-pie-graph"></i>
         </div>
         <a href="javascript:;" class="small-box-footer">{{__('More info')}} <i class="fa fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
   <!-- Left col -->
   <section class="col-lg-6 connectedSortable ui-sortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div style="cursor: move;">
         <div class="nav-tabs-custom" >
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right ui-sortable-handle">
               <li class="pull-left header"><i class="fa fa-newspaper-o"></i> {{__('Subscriptions')}}</li>
            </ul>
         </div>
         <div id="subscriptionsChart" style="height: 250px;"></div>
         <p id="showNoSubFound" style="display:none;">{{__('No Data Found')}}</p>
      </div>
   </section>
   <!-- /.Left col -->
   <!-- right col (We are only adding the ID to make the widgets sortable)-->
   <section class="col-lg-6 connectedSortable ui-sortable">
      <!-- Map box -->
      <div style="cursor: move;">
         <div class="nav-tabs-custom" >
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right ui-sortable-handle">
               <li class="pull-left header"><i class="fa fa-product-hunt"></i> {{__('Promotins')}}</li>
            </ul>
         </div>
         <div id="promotionsChart" style="height: 250px;"></div>
         <p id="showNoPromoFound" style="display:none;">{{__('No Data Found')}}</p> 
      </div>
   </section>
   <!-- right col -->
</div>
<div class="row">
   <!-- Left col -->
   <section class="col-lg-12 connectedSortable ui-sortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div style="cursor: move;">
         <div class="nav-tabs-custom" >
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right ui-sortable-handle">
               <li class="pull-left header"><i class="fa fa-newspaper-o"></i> {{__('Site Visits This Week')}}</li>
            </ul>
         </div>
         <div id="visitsChart" style="height: 250px;"></div>
         <p id="showNoVisitFound" style="display:none;">{{__('No Data Found')}}</p>
      </div>
   </section>
</div>
<!-- /.row (main row) -->
<script type="text/javascript">
   drawSubscriptionsChart();
   drawPromotionsChart();
   drawVisitsChart();
   function drawVisitsChart() {
      var visitData=<?php echo $siteVisits ?>;
      if(visitData){
        Morris.Area({
          element: 'visitsChart',
          data: visitData ,
          xkey: 'y',
          ykeys: ['v',],
          labels: ['Total Visits'],
        });
      }else{
         $('#showNoVisitFound').show();
      }
   }
   function drawSubscriptionsChart() {
      var subData=<?php echo $dataSub ?>;
         if(subData){
            new Morris.Line({
              element: 'subscriptionsChart',
              data:subData,
              xkey: 'year',
              ykeys: ['value'], 
              labels: ['€'],
              resize: true,
              redraw: true
            });
         } else{
            $('#showNoSubFound').show();
         }
   }
   function drawPromotionsChart() {
      var proData=<?php echo $dataPro ?>;
      if(proData){
         new Morris.Area({
           element: 'promotionsChart',
           data: proData,
           xkey: 'year',
           ykeys: ['value'],
           labels: ['€']
         });
      }else{
         $('#showNoPromoFound').show();
      }
   }
   
</script>
@endsection