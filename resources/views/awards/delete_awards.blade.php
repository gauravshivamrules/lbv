@extends('layouts.back')
@section('title',_('Awards Delete Requests'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<ul>    
		@foreach($errors->all() as $e)
			<li class="error"> {{ $e }} </li>
		@endforeach 
	</ul>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Awards Delete Requests'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="awardsTable">
				<thead>
					<tr>
						<th><?php echo __('Award'); ?></th>  
						<th><?php echo __('Accomodation'); ?></th>  
						<th><?php echo __('User'); ?></th>  
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table>
			</div>
	</div> 
		<script>
			$(function() {
			    $('#awardsTable').DataTable({
			    	responsive: true,
			        processing: true, 
			        serverSide: true,
			        ajax: "/awards/delete-award-ajax",        
			        columns: [
			            { data: 'title', name: 'title' },
			            { data: 'name', name: 'name' }, 
			            { data: 'first_name', name: 'first_name' },  
			            { data: 'action', name: 'action'}     
			        ]
			    });
			}); 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/awards/delete-award/'+id;  
				} else { 
					return false;
				} 
			}  
		</script>
@endsection