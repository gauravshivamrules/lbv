@extends('layouts.back')
@section('title',_('View All Awards'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<ul>    
		@foreach($errors->all() as $e)
			<li class="error"> {{ $e }} </li>
		@endforeach 
	</ul>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('View All Awards'); ?> </h2> 
			<span style="float:right;"> <a href="javascript:;" data-toggle="modal" data-target="#awardsModel" class="btn btn-primary"> {{__('Add New')}} </a> </span><br><br>
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="awardsTable">
				<thead>
					<tr>
						<th><?php echo __('Title'); ?></th>     
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table>
			</div>
	</div>
	<div class="modal fade" id="awardsModel"> 
	      <div class="modal-dialog" role="document">
	        <div class="modal-content"> 
	          <div class="modal-header">
	            <h5 class="modal-title"><?php echo __('Add New Award'); ?></h5>
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div> 
	          <div class="modal-body" id="homeModalBodyPerWeek">   
	
	         <form action="/awards/add" method="post" accept-charset="utf-8"  enctype="multipart/form-data">   
	              {{csrf_field()}} 
	              <div class="row"> 
	              	<div class="col-md-2"> </div>
	              	<div class="col-md-8"> 
	              		<div class="form-group">
	              		  <label class="control-label">
	              		    <font style="vertical-align: inherit;">
	              		      <font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
	              		    </font>
	              		  </label>
	              		  <input name="award_title" class="form-control valid" type="text"  required="true">                        
	              		</div>		
	              	</div>
	              	<div class="col-md-2"> </div>
	              </div>
	              <div class="row"> 
	              	<div class="col-md-2"> </div>
	              	<div class="col-md-8"> 
	              		<div class="form-group">
	              		  <label class="control-label">
	              		    <font style="vertical-align: inherit;">
	              		      <font style="vertical-align: inherit;"><?php echo __('Image'); ?></font>
	              		    </font>
	              		  </label>
	              		  <input name="award_image" class="form-control valid" type="file"  required="true" accept="image/*">                       
	              		</div>		
	              	</div>
	              	<div class="col-md-2"> </div>
	              </div>
	          </div>
	          <div class="modal-footer">
	            <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Add'); ?></button>             
	          </div>
	        </form>
	       
	        </div>
	      </div>
	</div> 
		<script>
			$(function() {
			    $('#awardsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/awards/get-awards",      
			        columns: [
			            { data: 'title', name: 'title' },
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/awards/delete/'+id; 
				} else { 
					return false;
				} 
			}  
		</script>
@endsection