@extends('layouts.back') 
@section('title',__('Add Award')) 
@section('content')
<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
</ul>
	<div class="well well bs-component"> 

		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
					<fieldset>
						<legend><?php echo __('Add Award'); ?></legend> 
						 <form action="/awards/request" method="post" accept-charset="utf-8"  enctype="multipart/form-data">    
						      {{csrf_field()}}  
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group">
						      		  <label class="control-label">
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
						      		    </font>
						      		  </label>
						      		  {!! Form::select('accom_id',$accoms,null,['id'=>'accomId','class'=>'form-control','required'=>true] ) !!} 
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div>
						      </div> 
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Award'); ?></font>
						      		    </font>
						      		  </label>
						      		    {!! Form::select('award_id',$awards,null,['id'=>'awardId','class'=>'form-control','required'=>true] ) !!}  
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div>
						      </div>
						  <div class="modal-footer">
						  	<button type="button" data-toggle="modal" data-target="#awardsModel" class="btn btn-primary btn-effect pull-right"> {{__('Request New Award')}} </button>
						    <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-left" id="addBtn"><?php echo __('Add'); ?></button>             
						  </div>
						</form>
					</fieldset>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<div class="modal fade" id="awardsModel"> 
	      <div class="modal-dialog" role="document">
	        <div class="modal-content"> 
	          <div class="modal-header">
	            <h5 class="modal-title"><?php echo __('Request New Award from admin'); ?></h5>
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div> 
	          <div class="modal-body" id="homeModalBodyPerWeek">   
	
	         <form action="/awards/ask_award" method="post" accept-charset="utf-8"  enctype="multipart/form-data">   
	              {{csrf_field()}} 
	              <div class="row"> 
	              	<div class="col-md-2"> </div>
	              	<div class="col-md-8"> 
	              		<div class="form-group">
	              		  <label class="control-label">
	              		    <font style="vertical-align: inherit;">
	              		      <font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
	              		    </font>
	              		  </label>
	              		  <input name="award_title" class="form-control valid" type="text"  required="true">                        
	              		</div>		
	              	</div>
	              	<div class="col-md-2"> </div>
	              </div>
	              <div class="row"> 
	              	<div class="col-md-2"> </div>
	              	<div class="col-md-8"> 
	              		<div class="form-group">
	              		  <label class="control-label">
	              		    <font style="vertical-align: inherit;">
	              		      <font style="vertical-align: inherit;"><?php echo __('Image'); ?></font>
	              		    </font>
	              		  </label>
	              		  <input name="award_image" class="form-control valid" type="file"  required="true" accept="image/*">                       
	              		</div>		
	              	</div>
	              	<div class="col-md-2"> </div>
	              </div>
	          </div>
	          <div class="modal-footer">
	            <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Submit'); ?></button>             
	          </div>
	        </form>
	       
	        </div>
	      </div>
	</div> 
@endsection