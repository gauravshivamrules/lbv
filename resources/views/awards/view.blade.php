@extends('layouts.back') 
@section('title',__('View Award')) 
@section('content')
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
				
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('View Award'); ?>
							@if(Auth::check())
								@if(Auth::user()->role_id==1)
									<span style="float:right;"><a href="/awards" class="btn btn-primary"> {{__('Back')}} </a> </span>
									@else
									<span style="float:right;"><a href="/awards/my-awards" class="btn btn-primary"> {{__('Back')}} </a> </span>
								@endif
							@endif
						</legend> 

						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Title'); ?> </label>
							<div class="col-lg-8">
								{{$award->title}}
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-8">
								<img src="/images/awards/{{$award->image}}" height="200px;">
							</div>
						</div>
					</fieldset>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
@endsection