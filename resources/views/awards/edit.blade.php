@extends('layouts.back') 
@section('title',__('Update Award')) 
@section('content')
<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
</ul>
	<div class="well well bs-component"> 

		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
				
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Update Award'); ?></legend> 
						
						 <form action="/awards/edit/{{$award->id}}" method="post" accept-charset="utf-8"  enctype="multipart/form-data">   
						      {{csrf_field()}} 
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group">
						      		  <label class="control-label">
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
						      		    </font>
						      		  </label>
						      		  <input name="award_title" class="form-control valid" type="text" value="{{$award->title}}"  required="true">                        
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div>
						      </div> 
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Image'); ?></font>
						      		    </font>
						      		  </label>
						      		  <img src="/images/awards/{{$award->image}}" height="200px;">
						      		  <input name="award_image" class="form-control valid" type="file" accept="image/*">                       
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div>
						      </div>
						  <div class="modal-footer">
						    <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>             
						  </div>
						</form>
					</fieldset>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
@endsection