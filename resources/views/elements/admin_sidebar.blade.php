 <aside class="main-sidebar">
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header"><?php echo __('MAIN NAVIGATION')  ?></li>
          <li class="active">
            <a href="{{ route('adminDashboard') }}"> 
              <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard'); ?></span>
            </a>
          </li> 
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span><?php echo __('Users'); ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="/users/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a>
              </li> 
              <li>
                <a href="/users"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-home"></i>
              <span><?php echo __('Accomodations'); ?></span> 
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">  
              <li><a href="/accomodations"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a></li>
              <li><a href="/accomodations/paid-view"><i class="fa fa-circle-o"></i><?php echo __('Paid'); ?></a></li> 
              <li><a href="/accomodations/no-cure-no-pay"><i class="fa fa-circle-o"></i><?php echo __('No Cure No Pay'); ?></a></li> 
              <li><a href="/accomodations/un-approved-view"><i class="fa fa-circle-o"></i><?php echo __('Un-Approved'); ?></a></li> 
              <li><a href="/accomodations/requested-delete-view"><i class="fa fa-circle-o"></i><?php echo __('Requested Deletion'); ?></a></li>  
              <li><a href="/accomodations/view-all-update"><i class="fa fa-circle-o"></i><?php echo __('Requested Update'); ?></a></li>  
            </ul>
          </li> 
          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-product-hunt"></i> <span>{{__('Promotions')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="/products"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a></li>
                <li>  <a href="/products/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a></li>
                <li>
                  <a href="/promotion-licenses/license">
                    <i class="fa fa-circle-o"></i> <span><?php echo __('Licenses'); ?></span>
                  </a>
                </li>

                <!-- SUB MENU HERE -->
               
                <li>
                  <a href="/promotion-licenses/give-free-license"><i class="fa fa-circle-o"></i><?php echo __('Give Free Coupons '); ?></a>
                </li>
                <li>
                  <a href="/promotion-licenses/all-coupons"><i class="fa fa-circle-o"></i><?php echo __('View Free Coupons'); ?></a>
                </li> 
              </ul> 
          </li>
          <li class="treeview">
            <a href="javascript:;">
              <i class="fa fa-newspaper-o"></i> 
              <span><?php echo __('Subscriptions'); ?></span> 
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="/subscriptions"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a>
              </li>
              <li>
                <a href="/subscriptions/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a>
              </li>
              <li>
                <a href="/subscription-licenses/licenses"><i class="fa fa-circle-o"></i><?php echo __('Licenses'); ?></a> 
              </li>
              <li>
                <a href="/subscription-licenses/give-free-license"><i class="fa fa-circle-o"></i><?php echo __('Give Free Coupons'); ?></a>
              </li>
              <li>
                <a href="/subscription-licenses/all-coupons"><i class="fa fa-circle-o"></i><?php echo __('View Free Coupons'); ?></a>
              </li>
            </ul>
          </li>
          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-euro"></i> <span>{{__('Payments')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="/subscription-payments/payments"><i class="fa fa-circle-o"></i>{{__('Subscriptions')}}</a></li>
                <li> <a href="/promotion-payments/payments"><i class="fa fa-circle-o"></i>{{__('Promotions')}} </a>
                <li> <a href="javascript:;"><i class="fa fa-circle-o"></i>{{__('Bookings')}} </a>

                <li><a href="/subscription-payments/payments-income"><i class="fa fa-circle-o"></i>{{__('Subscriptions Income')}}</a></li>
                <li> <a href="/promotion-payments/payments-income"><i class="fa fa-circle-o"></i>{{__('Promotions Income')}} </a> 
                <li> <a href="javascript:;"><i class="fa fa-circle-o"></i>{{__('Bookings Income')}} </a>

 

                </li>
              </ul> 
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-trophy"></i> 
              <span><?php echo __('Awards'); ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="/awards"><i class="fa fa-circle-o"></i><?php echo __('Manage'); ?></a>
              </li>
              <li>
                <a href="/awards/new-award"><i class="fa fa-circle-o"></i><?php echo __('New Request'); ?></a>
              </li>
              <li>
                <a href="/awards/delete-award"><i class="fa fa-circle-o"></i><?php echo __('Requested Deletion'); ?></a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-bars"></i> 
              <span><?php echo __('Extra Tabs'); ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="/extra-tab/all-tabs"><i class="fa fa-circle-o"></i><?php echo __('New Requests'); ?></a>
              </li>
              <li>
                <a href="/extra-tab/tabs-update"><i class="fa fa-circle-o"></i><?php echo __('Update Requests'); ?></a>
              </li>
            </ul>
          </li>
          <li class="treeview"> 
            <a href="#">
              <i class="fa fa-newspaper-o"></i>
              <span><?php echo __('Newsletter'); ?></span> 
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span> 
            </a>
            <ul class="treeview-menu">  
              <li><a href="/newsletter/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a></li> 
              <li><a href="/newsletter"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a></li>
              <li><a href="/newsletter/subscribers"><i class="fa fa-circle-o"></i><?php echo __('Subscribers'); ?></a></li>
            </ul>
          </li>
          <li class="active">
            <a href="/tickets/message">
              <i class="fa fa-envelope"></i> <span><?php echo __('Messages'); ?></span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-cog"></i> 
              <span><?php echo __('Settings'); ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="/settings"><i class="fa fa-circle-o"></i><?php echo __('View'); ?></a>
              </li>
              <li>
                <a href="/settings/help"><i class="fa fa-circle-o"></i><?php echo __('User Help'); ?></a>
              </li>
              <li>
                <a href="/settings/privacy"><i class="fa fa-circle-o"></i><?php echo __('Privacy'); ?></a>
              </li>
              <li>
                <a href="/settings/tos"><i class="fa fa-circle-o"></i><?php echo __('Terms & Conditions'); ?></a>
              </li>
             
              <li>
                <a href="/accomodations/slider-accomodations"><i class="fa fa-circle-o"></i><?php echo __('Slider Accomodations'); ?></a>
              </li>
            </ul>
          </li>

          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-exchange"></i> <span>{{__('Extras')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li>
                  <a href="/settings/search_tags"><i class="fa fa-circle-o"></i><?php echo __('Add Search Tags'); ?></a>
               </li>
                <li class="treeview">
                  <a href="javascript:;"><i class="fa fa-exchange"></i>{{__('Locations')}}
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: none;">
                    <li><a href="/accomodations/add-new-locations"><i class="fa fa-circle-o"></i>{{__('Add New')}}</a></li>
                    <li>
                       <a href="/accomodations/view-new-location"><i class="fa fa-circle-o"></i><?php echo __('Un-Approved Locations'); ?></a>
                    </li>
                  </ul>
                </li>
              </ul> 
          </li>

          <li>
            <a href="/bookings/admin-bookings">
              <i class="fa fa-book"></i> <span><?php echo __('Bookings'); ?></span>  
            </a>
          </li>  
         <li>
           <a href="/old-products">
             <i class="fa fa-line-chart"></i> <span><?php echo __('Old Invoices'); ?></span>  
           </a>
         </li>  

        </ul>
      </section>


      <!-- /.sidebar -->
 </aside>