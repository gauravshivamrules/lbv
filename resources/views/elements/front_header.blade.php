<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top mobi_nav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/"><img src="/images/lbv.png"></a>
            </div> 

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right right-icons">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                     <li class="dropdown">
                       <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;"> 
                         <img src="/images/flags/belgen.png" style="padding:  4px;margin-top: -3px;">BE 
                       </a>
                       <ul class="dropdown-menu">
                        <li>
                          <a href="javascript:;">
                            <img src="/images/flags/nederlanders.png" style="padding:  4px;margin-top: -3px;">NL
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                        <a class="page-scroll" href="/">{{__('HOME')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/contact-us">{{__('CONTACT')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/advertisers">{{__('ADVERTISERS')}}</a>
                    </li>
                    @if(!Auth::check())
                        <li>
                            <a class="page-scroll" href="/login">{{__('Login')}}</a>
                        </li>
                    @else
                    <li class="dropdown">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"> 
                                     Hi,{{Auth::user()->first_name}}
                                    <span class="caret"></span>
                                  </a>
                                    <ul class="dropdown-menu">
                                        @if(Auth::user()->role_id==2)
                                          <li><a href="/dashboard/cust-dashboard"><i class="fa fa-dashboard"></i>{{__('Dashboard')}}</a></li>
                                        @elseif(Auth::user()->role_id==3)
                                          <li><a href="/dashboard/adv-dashboard"><i class="fa fa-dashboard"></i>{{__('Dashboard')}}</a></li>
                                        @elseif(Auth::user()->role_id==1)
                                          <li><a href="/dashboard/dashboard"><i class="fa fa-dashboard"></i>{{__('Dashboard')}}</a></li>
                                        @endif
                                        <li><a href="/logout"><i class="fa fa-key"></i>{{__('Logout')}}</a></li>
                                    </ul>
                                </li>
                    @endif
                    <!-- <li><a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a></li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        <div class="icon-bar">
  <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> 
  <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> 
  <a href="#" class="google"><i class="fa fa-google"></i></a> 
  <!-- <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
  <a href="#" class="youtube"><i class="fa fa-youtube"></i></a>  -->
</div>
    </nav>