 <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- /.search form --> 
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header"><?php echo __('MAIN NAVIGATION')  ?></li>
          <li class="active">
            <a href="{{ route('advDashboard') }}">
              <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard'); ?></span>
            </a>
          </li>
          <li class="treeview">
            <a href="javascript:;"> 
              <i class="fa fa-home"></i>
              <span><?php echo __('Accomodations'); ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a> 
            <ul class="treeview-menu">
              <li><a href="/accomodations/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a></li> 
              <li><a href="/accomodations/my-accomodations"><i class="fa fa-circle-o"></i><?php echo __('View All'); ?></a></li>
            </ul>
          </li>
          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-newspaper-o"></i> <span>{{__('Subscriptions')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('getAdvertiserLicensesView') }}"><i class="fa fa-circle-o"></i>{{__('My Subscriptions')}}</a></li>
                <li class="treeview">
                  <a href="javascript:;"><i class="fa fa-history"></i>{{__('History')}}
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: none;">
                    <li><a href="/subscription-payments/history"><i class="fa fa-circle-o"></i>{{__('Subscription')}}</a></li>
                    <li>
                      <a href="/promotion-payments/history"><i class="fa fa-circle-o"></i>{{__('Promotion')}}</a>
                    </li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="javascript:;"><i class="fa fa-adjust"></i>{{__('Switch Subscription')}}
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: none;">
                    <li><a href="/subscriptions/switch-to-free"><i class="fa fa-circle-o"></i>{{__('Switch to no cure no pay')}}</a></li>
                    <li>
                      <a href="/subscriptions/switch-to-paid"><i class="fa fa-circle-o"></i>{{__('Switch to paid')}}</a>
                    </li>
                  </ul>
                </li> 
              </ul> 
          </li>
         
          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-product-hunt"></i> <span>{{__('Promotions')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="/promotions/buy"><i class="fa fa-circle-o"></i>{{__('Buy New')}}</a></li>
                <li class="treeview">
                  <a href="javascript:;"><i class="fa fa-product-hunt"></i>{{__('My Promotions')}}
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: none;">
                    <li><a href="/promotions/my-promotions"><i class="fa fa-circle-o"></i>{{__('All')}}</a></li>
                    <li>
                      <a href="/promotions/my-tips"><i class="fa fa-circle-o"></i><?php echo __('Tips'); ?></a>
                    </li>
                    <li>
                      <a href="/promotions/my-spotlights"><i class="fa fa-circle-o"></i><?php echo __('Spotlights'); ?></a>
                    </li>
                    <li>
                      <a href="/promotions/my-lastminutes"><i class="fa fa-circle-o"></i><?php echo __('Last Minute'); ?></a>
                    </li>
                    <li>
                      <a href="/promotions/my-news"><i class="fa fa-circle-o"></i><?php echo __('News'); ?></a>
                    </li>
                  </ul>
                </li>
              </ul> 
          </li>

          <li class="treeview">
              <a href="javascript:;">
                <i class="fa fa-euro"></i> <span>{{__('Payments')}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="/subscription-payments"><i class="fa fa-circle-o"></i>{{__('Subscription')}}</a></li>
                <li> <a href="/promotion-payments"><i class="fa fa-circle-o"></i>{{__('Promotion')}} </a>
                </li>
              </ul> 
          </li>
         <li class="treeview">
           <a href="#">
             <i class="fa fa-trophy"></i> 
             <span><?php echo __('Awards'); ?></span>
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span>
           </a> 
           <ul class="treeview-menu">
             <li>
               <a href="/awards/request"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a>
             </li>
             <li>
               <a href="/awards/my-awards"><i class="fa fa-circle-o"></i><?php echo __('My Awards'); ?></a>
             </li>
           </ul>
         </li>

         <li class="treeview">
           <a href="#">
             <i class="fa fa-bars"></i> 
             <span><?php echo __('Extra Tabs'); ?></span>
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span> 
           </a> 
           <ul class="treeview-menu">
             <li>
               <a href="/extra-tabs/add"><i class="fa fa-circle-o"></i><?php echo __('Add New'); ?></a> 
             </li>
             <li>
               <a href="/extra-tabs"><i class="fa fa-circle-o"></i><?php echo __('My Tabs'); ?></a>
             </li>
           </ul>
         </li>
         <li class="treeview">
           <a href="#">
             <i class="fa fa-book"></i> 
             <span><?php echo __('Bookings'); ?></span>
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i> 
             </span> 
           </a> 
           <ul class="treeview-menu">
            <li>
              <a href="/bookings/invoices"><i class="fa fa-circle-o"></i><?php echo __('Invoices'); ?></a> 
            </li>
             <li>
               <a href="/bookings/adv-bookings"><i class="fa fa-circle-o"></i><?php echo __('Bookings'); ?></a> 
             </li>
             <li>
               <a href="/bookings/no-avaibality"><i class="fa fa-circle-o"></i><?php echo __('Calendar'); ?></a>
             </li>
           </ul>
         </li>
         <li class="active">
           <a href="/tickets/adv-message">
             <i class="fa fa-envelope"></i> <span><?php echo __('Messages'); ?></span>
           </a>
         </li>
         <li class="treeview">
           <a href="#">
             <i class="fa fa-bars"></i> 
             <span><?php echo __('Social Media & Statistics'); ?></span>
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span> 
           </a> 
           <ul class="treeview-menu">
             <li>
               <a href="/social-media/post"><i class="fa fa-circle-o"></i><?php echo __('Post To Social Media'); ?></a> 
             </li>
             <li>
               <a href="/social-media/statistics"><i class="fa fa-circle-o"></i><?php echo __('Statistics'); ?></a>
             </li>
           </ul>
         </li>
        
         <li>
           <a href="/old-products/my-invoices">
             <i class="fa fa-line-chart"></i> <span><?php echo __('Old Invoices'); ?></span>  
           </a>
         </li>




         @if(session()->has('back_id')) 
         <li>
          <a href="/users/switch_back/{{ session('back_id') }}">
            <i class="fa fa-adjust"></i> <span><?php echo __('Switch Back'); ?></span>
          </a>
            
        </li>
        @endif
        </ul>
      </section>
      <!-- /.sidebar -->
 </aside>