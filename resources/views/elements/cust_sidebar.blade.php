 <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- /.search form --> 
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header"><?php echo __('MAIN NAVIGATION')  ?></li>
          <li class="active">
            <a href="{{ route('custDashboard') }}">
              <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard'); ?></span>
            </a>
          </li>
          <li>
            <a href="{{ route('custMessages') }}">
              <i class="fa fa-envelope"></i> <span><?php echo __('Messages'); ?></span>
            </a>
          </li>
          <li>
            <a href="{{ route('myBookings') }}">
              <i class="fa fa-book"></i> <span><?php echo __('Bookings'); ?></span>
            </a>
          </li>
          <!-- {{session('back_id')}} -->
         @if(session()->has('back_id') ) 
         <li>
          <a href="/users/switch_back/{{ session('back_id') }}">
            <i class="fa fa-adjust"></i> <span><?php echo __('Switch Back'); ?></span>
          </a>
        </li>
        @endif
        </ul>
      </section>
      <!-- /.sidebar -->
 </aside>