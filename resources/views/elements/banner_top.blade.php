<div class="banner-bottom-top">
   <div class="container">
      <div class="bottom-header">
         <div class="header-bottom">
            <div class=" bottom-head">
               <a href="#">
                  <div class="buy-media">
                     <i class="buy"> </i>
                     <h6>{{__('Rent')}}</h6>
                  </div>
               </a>
            </div>
            <div class=" bottom-head">
               <a href="#">
                  <div class="buy-media">
                     <i class="rent"> </i>
                    <h6>{{__('Sale')}}</h6> 
                  </div>
               </a>
            </div>
            <div class=" bottom-head">
               <a href="/home/lastminutes">
                  <div class="buy-media">
                     <i class="pg"> </i>
                     <h6>{{__('Last Minutes')}}</h6>
                  </div>
               </a>
            </div>
            <div class=" bottom-head">
               <a href="/home/news">
                  <div class="buy-media">
                     <i class="sell"> </i>
                      <h6>{{__('News')}}</h6>
                  </div>
               </a>
            </div>
            <div class=" bottom-head">
               <a href="#">
                  <div class="buy-media">
                     <i class="deal"> </i>
                     <h6>{{__('Advertisers')}}</h6>
                  </div>
               </a>
            </div> 
            <div class=" bottom-head">
               <a href="#">
                  <div class="buy-media">
                     <i class="sell"> </i>
                     <h6>{{__('Contact Us')}}</h6>
                  </div>
               </a>
            </div>
            <div class="clearfix"> </div>
         </div>
      </div>
   </div>
</div>