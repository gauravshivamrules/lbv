<!--test-->
<div class="content-bottom">
   <div class="container">
      <h3>Testimonials</h3>
      <div class="col-md-6 name-in">
         <div class=" bottom-in">
            <p class="para-in">Duis aute irure dolor in reprehenderit in voluptate
               velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <i class="dolor"> </i>
            <div class="men-grid">
               <a href="#" class="men-top"><img class="img-responsive men-top" src="/images/front/te.jpg" alt=""></a>
               <div class="men">
                  <span>Roger V. Coates</span>
                  <p>Ut enim ad minim</p>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
         <div class=" bottom-in">
            <p class="para-in">Duis aute irure dolor in reprehenderit in voluptate
               velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <i class="dolor"> </i>
            <div class="men-grid">
               <a href="#" class="men-top"><img class="img-responsive " src="/images/front/te2.jpg" alt=""></a>
               <div class="men">
                  <span>Ann K. Perez</span>
                  <p>Ut enim ad minim</p>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <div class="col-md-6  name-on">
         <div class="bottom-in ">
            <p class="para-in">Duis aute irure dolor in reprehenderit in voluptate
               velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <i class="dolor"> </i>
            <div class="men-grid">
               <a href="#" class="men-top"><img class="img-responsive " src="/images/front/te1.jpg" alt=""></a>
               <div class="men">
                  <span>Nancy M. Picker</span>
                  <p>Ut enim ad minim</p>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
   </div>
</div>
<!--//test-->  
<!--partners-->
<div class="content-bottom1">
   <h3>Our Partners</h3>
   <div class="container">
      <ul>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg1.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg2.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg3.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg4.png" alt=""></a></li>
         <div class="clearfix"> </div>
      </ul>
      <ul>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg5.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg6.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg7.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg8.png" alt=""></a></li>
         <li><a href="#"><img class="img-responsive" src="/images/front/lg9.png" alt=""></a></li>
         <div class="clearfix"> </div>
      </ul>
   </div>
</div>
<!--//partners--> 