 <style type="text/css">
        .navbar-nav>.notifications-menu>.dropdown-menu>li .menu>li>a, .navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a, .navbar-nav>.tasks-menu>.dropdown-menu>li .menu>li>a {
    display: block;
     white-space: normal !important; 
    border-bottom: 1px solid #f4f4f4;
} 
 </style>
 <header class="main-header">
      <!-- Logo -->
      <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>BV</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><?php echo env('SITE_TITLE_BACK','LBV')  ?></b></span> 
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu"> 
              <a href="javascript:;" onclick="getHelps()" class="dropdown-toggle" data-toggle="dropdown" title="{{__('Help')}}">
                <i class="glyphicon glyphicon-question-sign"></i>
              </a>
            </li>
            <li class="dropdown messages-menu">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" title="{{__('Message(s)')}}">
                <i class="fa fa-envelope-o"></i>
                @if(count($msgNotification)>0)
                <span class="label label-success"> 
                  {{count($msgNotification)}}
                </span>
                @endif
              </a>
              <ul class="dropdown-menu" >
                <li class="header"><?php echo __('You have'); ?> {{count($msgNotification)}}  <?php echo __('messages'); ?></li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    @foreach($msgNotification as $aN)  
                      @if(Auth::user()->role_id==3)
                        <li><!-- start message -->
                          <a href="javascript:;" onclick="markSeenMsg({{$aN->id}},'/tickets/adv-message')">
                            <div class="pull-left"> 
                              <h5>
                                <small><i class="fa fa-clock-o"></i> {{date('d-m-Y H:i:s',strtotime($aN->created_at))}} </small>
                              </h5> 
                              <p> {{__('You have a new message request')}}  </p>
                            </div>
                          </a>
                        </li>
                      @elseif(Auth::user()->role_id==2)
                        <li><!-- start message -->
                          <a href="javascript:;" onclick="markSeenMsg({{$aN->id}},'/messages/my-messages')">
                            <div class="pull-left"> 
                              <h5>
                                <small><i class="fa fa-clock-o"></i> {{date('d-m-Y H:i:s',strtotime($aN->created_at))}} </small>
                              </h5> 
                              <p> {{__('You have a new message request')}}  </p>
                            </div>
                          </a>
                        </li>
                      @else
                        <li><!-- start message -->
                          <a href="javascript:;" onclick="markSeenMsg({{$aN->id}},'/tickets/message')"> 
                            <div class="pull-left"> 
                              <h5>
                                <small><i class="fa fa-clock-o"></i> {{date('d-m-Y H:i:s',strtotime($aN->created_at))}} </small>
                              </h5> 
                              <p> {{__('You have a new message request')}}  </p>
                            </div>
                          </a>
                        </li>
                      @endif
                    @endforeach

                    <!-- end message -->
                  </ul> 
                </li>
                <!-- <li class="footer"><a href="#">See All Messages</a></li> -->
              </ul>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
            @if(Auth::check())
              @if(Auth::user()->role_id==1)
              <li class="dropdown notifications-menu" >
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" title="{{__('Notifications')}}">
                  <i class="fa fa-bell-o"></i>
                  @if(count($adminNotification)>0)
                  <span class="label label-danger"> 
                    {{count($adminNotification)}}
                  </span>
                  @endif
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> <?php echo __('You have');?> {{count($adminNotification)}} <?php echo __('notification(s)'); ?></li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu"> 
                      @foreach($adminNotification as $aN) 
                      <li>
                        <a onclick="markSeen({{$aN->id}},'{{$aN->request_type_url}}')" href="javascript:;"> 
                          <!-- <i class="fa fa-users text-aqua"></i> -->
                           <p>
                            {{__($aN->message)}}
                         </p>
                         <small><i class="fa fa-clock-o"></i> {{date('d-m-Y h:m a',strtotime($aN->created_at))}}</small>
                        </a>
                      </li>
                      @endforeach

                    </ul>
                  </li>
                 <!--  <li class="footer"><a href="#">View all</a></li> -->
                </ul>
              </li>
              @else 
              <li class="dropdown notifications-menu" >
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" title="{{__('Notifications')}}">
                  <i class="fa fa-bell-o"></i>
                  @if(count($userNotification)>0)
                  <span class="label label-danger">  
                    {{count($userNotification)}}
                  </span>
                  @endif
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> <?php echo __('You have');?> {{count($userNotification)}} <?php echo __('notification(s)'); ?></li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu"> 
                      @foreach($userNotification as $aN) 
                      <li>
                        <a onclick="markSeen({{$aN->id}},'{{$aN->request_type_url}}')" href="javascript:;"> 
                          <!-- <i class="fa fa-users text-aqua"></i> -->
                           <p>   {{__($aN->message)}} </p> 
                         <small><i class="fa fa-clock-o"></i> {{date('d-m-Y h:m a',strtotime($aN->created_at))}}</small>
                        </a>
                      </li>
                      @endforeach

                    </ul>
                  </li>
                 <!--  <li class="footer"><a href="#">View all</a></li> -->
                </ul>
              </li>
              @endif
            @endif
            <!-- Tasks: style can be found in dropdown.less -->
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                  <?php echo __('Hi'); ?>, {{ Auth::user()->first_name}}
                <span class="caret"></span>
              </a>
              <!-- style="background: #222d32;" -->
                <ul class="dropdown-menu">
                  <li><a href="/users/profile/{{ Auth::user()->id}}"><i class="fa fa-user"></i><?php echo __('Profile'); ?></a></li>
                  <li><a href="/users/change_password"><i class="fa fa-lock"></i><?php echo __('Change Password'); ?></a></li>
                  @if(Auth::user()->role_id==3)
                    <li><a href="javascript:;" onclick="findPayment()"><i class="fa fa-key"></i><?php echo __('Logout'); ?></a></li>
                  @else 
                    <li><a href="/logout"><i class="fa fa-key"></i><?php echo __('Logout'); ?></a></li>
                  @endif 
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
          </ul>
        </div>
      </nav>
  </header> 
 <div class="modal fade" id="helpsModalHome">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo __('View Help'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          
          <div class="modal-body" id="homeModalBody">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>
       
        </div>
      </div>
 </div>
 <div class="modal fade" id="helpsModalHomeFull">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="homeModalTitleFull"> </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          
          <div class="modal-body" id="homeModalBodyFull">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>
       
        </div>
      </div>
 </div>
 <div class="modal fade" id="unPaidInvoiceModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo __('Unpaid Invoice'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          <div class="modal-body">
            <p> {{__('You have an unpaid subscription payment,please pay your subscription')}} </p>
            <div id="addLink"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
          </div>
        </div>
      </div>
 </div>
   <script type="text/javascript">
      function findPayment(){ 
        $.ajax({
          url:'/subscription-licenses/find-unpaid-invoice',
          dataType:'json',
          success:function(r) {
            if(r.success){
              $('#addLink').html('');
              $('#addLink').append("<a href='/accomodations/pay/"+r.accom+"' class='btn btn-primary'>{{__('Pay Now')}}</a>");
              $('#addLink').append("&nbsp;<a href='/logout' class='btn btn-danger'>{{__('Logout and pay later')}}  </a>");
              $('#unPaidInvoiceModal').modal('show');
            } else {
              window.location.href='/logout'; 
            }
          }
        });
      }
          function getHelps () {
              $.ajax({
                url:'/settings/getall_helps',
                dataType:'json',
                success:function(s){
                  $('#homeModalBody').html('');
                  $.each(s,function(i,v) {
                    var a="<h4><a href='javascript:;' onclick='showFullHelp("+v.id+")'>"+v.title+"</a></h4><br>";
                    $('#homeModalBody').append(a);
                  });
                  $('#helpsModalHome').modal('show');
                }
              });
          } 
          function showFullHelp(id) {
            $.ajax({
              url:'/settings/get_help_byid?id='+id,
              dataType:'json',
              success:function(s){
                $('#homeModalBodyFull').html('');
                $('#homeModalTitleFull').html(s.title); 
                $('#homeModalBodyFull').html(s.content); 
              }
            });
            $('#helpsModalHomeFull').modal('show');
          }
          function markSeen(id,url) {
            if(id) {
              if(url) {
                $.ajax({
                url:'/notifications/mark-seen?id='+id+'&user={{Auth::user()->role_id}}',  
                success:function(r) {
                  window.location.href=url;
                }
              });  
              }
              
            }
          }
          function markSeenMsg(id,url) {
            if(id) {
              if(url) {
                $.ajax({
                url:'/notifications/mark-seen-msg?id='+id,  
                success:function(r) {
                  window.location.href=url;
                }
              });  
              }
              
            }
          }
    </script>
  