<div class="content-grid">
   <div class="container">
      <h3>{{__('Tip')}}</h3>
      @if($tipAccom)
      <div class="col-md-2 box_2"></div>
      <div class="col-md-8 box_2"> 
         <a href="/accomodations/view/{{$tipAccom->slug}}" class="mask">
            <img class="img-responsive zoom-img" src="/images/gallery/{{$tipAccom->id}}/{{$tipAccom->image}}" alt="">
            <span class="four"> {{$tipAccom->price}}&euro;</span>
         </a> 
         <div class="most-1">
            <h5><a href="/accomodations/view/{{$tipAccom->slug}}"> {{$tipAccom->name}}  </a></h5>
            <p>{{ucfirst($tipAccom->region)}}, {{ucfirst($tipAccom->country)}}</p>
         </div>
      </div>
      <div class="col-md-2 box_2"></div>
      @else
      <h3>{{__('No Tip Found')}} </h3>
      @endif
      <div class="clearfix"> </div>
   </div>
</div>