<div class="services">
   <div class="container">
      <div class="service-top">
         <h3> {{__('Spotlights')}} </h3>
      </div>
      <div class="services-grid">
         <?php if($spotAccoms) { ?>
            @foreach($spotAccoms['accom'] as $sA)
            <div class="col-md-4 box_2">
                  @if($sA->thumb) 
                   <a href="/accomodations/view/{{$sA->slug}}" class="mask">
                        <img class="img-responsive zoom-img" src="/images/gallery/{{$sA->id}}/{{$sA->thumb}}" alt="">
                        <span class="four">{{$sA->price?$sA->price:''}}&euro;</span>
                   </a>
                  @endif
                     <div class="most-1">
                         <h5><a href="/accomodations/view/{{$sA->slug}}">{{$sA->name}} </a></h5> 
                        <p>{{ucfirst($sA->region)}},{{ucfirst($sA->country)}} </p>
                     </div>
            </div>
            @endforeach
         <?php } else { ?>
         <h3> {{__('No Spotlight Found')}} </h3>
         <?php } ?>
   </div>
</div>