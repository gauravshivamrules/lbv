<style type="text/css">
   .rslides {
  position: relative;
  list-style: none;
  overflow: hidden;
  width: 100%;
  padding: 0;
  margin: 0;
  }

.rslides li {
  -webkit-backface-visibility: hidden;
  position: absolute;
  display: none;
  width: 100%;
  left: 0;
  top: 0;
  }

.rslides li:first-child {
  position: relative;
  display: block;
  float: left;
  }
 
.rslides img {
  display: block; 
  max-height: 550px;
  float: left;
  width: 100%;
  border: 0;
  }
</style>
 <script src="/js/front/responsiveslides.min.js"></script>
 <div class=" header-right">
      <div class=" banner">
         @if(isset($tipAccom))
         <div class="slider">
            <div class="callbacks_container">
               <ul class="rslides" id="slider">
                  @foreach($tipAccom['accom'] as $sA)
                    @if($sA->image)
                     <li>
                        <img src="/images/gallery/{{$sA->id}}/{{$sA->image}}" alt="">
                         <div class="caption">
                           <h3><span><a href="/accomodations/view/{{$sA->slug}}">{{$sA->name}}</a></span></h3> 
                           <p>{{ucfirst($sA->region)}},{{ucfirst($sA->country)}}</p> 
                        </div>
                     </li>
                    @endif
                 @endforeach 
               </ul>
            </div>
         </div> 
         @endif
      </div>
</div>
<script type="text/javascript">
      $("#slider").responsiveSlides({
         auto: true,
         speed: 500,
         namespace: "callbacks",
         pager: true, 
      });
</script>