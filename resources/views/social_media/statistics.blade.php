@extends('layouts.back')
@section('title',_('Statistics'))
@section('content')
	<link rel="stylesheet" href="/css/morris.css">
	<link rel="stylesheet" href="/css/ionicons.min.css">
	<link rel="stylesheet" href="/css/jquery-jvectormap.css">
	<script src="/js/raphael.min.js"></script>
	<script src="/js/morris.min.js"></script>
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('Accomodation Statistics') }} </h2> 
			<p> {{__('Check the number of visitors to your accomodations here')}} </p> 
		</div>
		{!! Form::select('accom_id',$accoms,null,['class'=>'form-control','id'=>'accomID'] ) !!}
		<ul class="nav nav-tabs" id="stats" >
			<li class="active" id="activeDefault" onclick="drawGraphDaily('yes');"><a data-toggle="tab" href="#showDaily"> {{__('Daily')}} </a></li>
			<li><a data-toggle="tab" href="#showMonthly" onclick="drawGraphMonthly();">{{__('Monthly')}}</a></li> 
			<li><a data-toggle="tab" href="#showYearly" onclick="drawGraphYearly();"> {{_('Yearly')}} </a></li>
		</ul>  
		<ul class="nav nav-tabs" id="tabs">
			<li class="active"><a data-toggle="tab" href="#showDaily" onclick="drawGraphAll();"> {{__('All Visitors')}} </a> </li>
		</ul>
		<div class="tab-content"> 
			<div id="showDaily" class="tab-pane active">
				<p id="visitsChartNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChart" style="height: 250px;"></div>
			</div>
			<div id="showMonthly" class="tab-pane">
				<p id="visitsChartMonthlyNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChartMonthly" style="height: 250px;"></div>
			</div>
			<div id="showYearly" class="tab-pane">
				<p id="visitsChartYearlyNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChartYearly" style="height: 250px;"></div>
			</div>
			<div id="showCountry" class="tab-pane">
				<p id="visitsChartCountryNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChartCountry" style="height: 250px;"></div>
			</div>
			<div id="showRegion" class="tab-pane">
				<p id="visitsChartRegionNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChartRegion" style="height: 250px;"></div>
			</div>
			<div id="showDepartment" class="tab-pane">
				<p id="visitsChartDepartmentNotFound" style="display:none;text-align:center;">{{__('No Data Found')}} </p>
				<div id="visitsChartDepartment" style="height: 250px;"></div>
			</div>
		</div>				
	</div>
		<script>
		function startBlock(){
			$.blockUI({message:'<h1>Loading...</h1>',css:{backgroundColor:'#000',color:'#fff'}});
		}
		function endBlock(){
			$.unblockUI();	
		}
		$(document).ready(function(){
			drawGraphDaily();
		});
		$('#accomID').change(function(){
			$("#stats>li.active").removeClass("active");
			$('#activeDefault').addClass('active');
			$("#tab-content>.active").removeClass("active");
			$('#showDaily').addClass('active');
			drawGraphDaily();
		});
		function drawGraph(accom){ 
			startBlock();
			$.ajax({
				url:'/social-media/statistics-ajax?accom_id='+accom+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){ 
					drawVisitsChart(r,'visitsChart');
				}
			});
			endBlock();
		} 
		function drawGraphDaily(yes){
			startBlock();
			$.ajax({
				url:'/social-media/daily-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text()+'&yes='+yes, 
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChart');	
				}
			});
			endBlock();
		}
		function drawGraphAll(){
			startBlock();
			$.ajax({
				url:'/social-media/all-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(), 
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChart');	
				}
			});
			endBlock();
		}
		function drawGraphMonthly(){
			startBlock();
			$.ajax({
				url:'/social-media/monthly-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChartMonthly');	
				}
			});
			endBlock();
		}
		function drawGraphYearly(){
			startBlock();
			$.ajax({
				url:'/social-media/yearly-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChartYearly');	
				}
			});
			endBlock();	
		}
		function drawGraphCountry(){
			startBlock();
			$.ajax({
				url:'/social-media/country-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChartCountry');
					$("#tabs>li.active").removeClass("active");
					$('#country').addClass('active');
				}
			});
			endBlock();	
		}
		function drawGraphRegion(){
			startBlock();
			$.ajax({
				url:'/social-media/region-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChartRegion');
					$("#tabs>li.active").removeClass("active");
					$('#region').addClass('active');	
				}
			});
			endBlock();	
		}  
		function drawGraphDepartment(){
			startBlock();
			$.ajax({
				url:'/social-media/department-statistics-ajax?accom_id='+$('#accomID').val()+'&selectedtab='+$('#stats').find('.active').text(),
				dataType:'json',
				success:function(r){
					drawVisitsChart(r,'visitsChartDepartment'); 
					$("#tabs>li.active").removeClass("active");
					$('#department').addClass('active');	
				}
			});
			endBlock();	
		}
		function drawVisitsChart(data,elem){
			$("#"+elem).empty();
			if(data.siteVisits.length>0){
				var tab='<li class="active"><a data-toggle="tab" onclick="drawGraphAll();" href="#showDaily">{{__('All Visitors')}}</a></li>';
				if(data.location.name){
					tab+='<li id="country"><a data-toggle="tab" onclick="drawGraphCountry()" href="#showCountry">'+data.location.name.toUpperCase()+'</a></li>';	
				}
				if(data.location.region_name){
					tab+='<li id="region"><a data-toggle="tab" onclick="drawGraphRegion()" href="#showRegion">'+data.location.region_name.toUpperCase()+'</a></li>';	
				}
				if(data.location.department_name){
					tab+='<li id="department"><a data-toggle="tab" onclick="drawGraphDepartment()" href="#showDepartment">'+data.location.department_name.toUpperCase()+'</a></li>';	
				} 
				$('#tabs').html('');
				$('#tabs').append(tab); 
				$('#visitsChartNotFound').hide();
				Morris.Area({
					element:elem,
			        data:data.siteVisits,
			        xkey: 'y',
			        ykeys: ['v',],
			        labels: ['Total Visits'],
			        redraw:true 
		  		});
			}else{
				$('#visitsChartNotFound').hide();
				$('#visitsChartMonthlyNotFound').hide();
				$('#visitsChartYearlyNotFound').hide(); 
				$('#visitsChartCountryNotFound').hide();
				$('#visitsChartRegionNotFound').hide();
				$('#visitsChartDepartmentNotFound').hide();
				$('#'+elem+'NotFound').show(); 
			}
		}
		</script>
@endsection
