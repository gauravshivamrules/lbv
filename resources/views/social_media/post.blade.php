@extends('layouts.back') 
@section('title',__('Social Media')) 
@section('content')
<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
</ul>
	<div class="well well bs-component"> 

		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
					<fieldset>
						<legend><?php echo __('Post to social media'); ?></legend> 
						 <form action="/social-media/post" method="post" accept-charset="utf-8"  enctype="multipart/form-data">    
						      {{csrf_field()}}  
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group">
						      		  <label class="control-label">
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
						      		    </font>
						      		  </label>
						      		  {!! Form::select('accom_id',$accoms,null,['id'=>'accomId','class'=>'form-control','required'=>true] ) !!} 
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div>
						      </div> 
						      <div class="row"> 
						      	<div class="col-md-2"> </div>
						      	<div class="col-md-8"> 
						      		<div class="form-group"> 
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Social Media'); ?></font>
						      		    </font>
						      		  </label> 
						      		    {!! Form::select('media_id',['fb'=>'Facebook','gplus'=>'Google Plus','tw'=>'Twitter','ln'=>'Linkedin'],null,['class'=>'form-control','required'=>true] ) !!}  
						      		</div>		
						      	</div>
						      	<div class="col-md-2"> </div> 
						      </div> 
						  <div class="modal-footer">
						    <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-left" id="addBtn"><?php echo __('Post'); ?></button>             
						  </div>
						</form>
					</fieldset>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	@if(isset($shareUrl))
		<script type="text/javascript">
			window.open("<?php echo $shareUrl ?>", '_blank');
		</script>
	@endif 
@endsection