@extends('layouts.login')
@section('title','|Log In')
@section('content')

	<div class="login-box">
	  <div class="login-logo">   
	    	<a href="/"><b><?php echo __('Logeren bij') ?></b> <?php echo __('Vlamingen') ?></a>
	  </div>
	  <div class="login-box-body">
	    <p class="login-box-msg"><?php echo __('Sign in to your account') ?></p>
	     <!-- route('login') -->
	    <form action="{{ route('login') }}" method="post">
	    	{{csrf_field()}}
	      <div class="form-group has-feedback">
	        <input type="email" name="email" class="form-control" placeholder="<?php echo __('Email') ?>">
	        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	        <span style="color:red">{{$errors->first('email')}}  </span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" name="password" class="form-control" placeholder="<?php echo __('Password') ?>">
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	         <span style="color:red">{{$errors->first('password')}} </span>
	      </div>
	      <div class="row">
	        <div class="col-xs-8">
	        </div>
	        <div class="col-xs-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign In') ?></button>
	        </div>
	      </div>
	    </form>
	    <div class="social-auth-links text-center"> 
	        <p>- <?php echo __('Or login with') ?> -</p> 
	        <a style="font-size: 12px;" href="/auth/facebook" class="btn btn-facebook"><i class="fab fa-facebook"></i></a>
	        <a style="font-size: 12px;" href="/auth/google" class="btn btn-google"><i class="fab fa-google"></i></a>
	        <a style="font-size: 12px;" href="/auth/twitter" class="btn btn-twitter"><i class="fab fa-twitter"></i></a> 
	        <a style="font-size: 12px;" href="/auth/linkedin" class="btn btn-linkedin"><i class="fab fa-linkedin"></i></a>
	    </div>
	    <a href="/forgot-password"><?php echo __('Forgot password?'); ?></a><br>
	    {{__('Register as')}}<a href="/users/adv-register"> {{__('Advertiser')}} </a> {{__('Or')}} <a href="/users/cust-register"> {{__('Customer')}} </a>
	  </div>
	</div>
@endsection