@extends('layouts.back') 
@section('title',__('Add User'))
@section('content')
	<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
	</ul>
	<div class="well well bs-component">
		<!-- {{ route('postAddUser') }} -->
		<form  action="/users/add" id="personal_info_form" method="post" accept-charset="utf-8">
			{{csrf_field()}}
			<div class="row"> 
				<div class="form-group col-md-4">
					<label class="control-label">
						<font style="vertical-align: inherit;">
							<font style="vertical-align: inherit;"><?php echo __('First Name'); ?></font>
						</font>
					</label>
					<input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="UserId">                              
					<input name="first_name" class="form-control valid"  type="text"  id="UserFirstName" value="{{old('first_name')}}"  required>                        
				</div>
				<div class="form-group col-md-4">
					<label class="control-label">
						<font style="vertical-align: inherit;">
							<font style="vertical-align: inherit;"><?php echo __('Last Name'); ?></font></font>
						</label>
						<input name="last_name" class="form-control" type="text"  id="UserSurName" value="{{old('last_name')}}"  required>                            
				</div>
				<div class="form-group col-md-4">
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Email'); ?></font></font></label>
						<input name="email" class="form-control"  type="email" id="UserEmail" value="{{old('email')}}"  required>                            
				</div>
			</div> 
			<div class="row">
				<div class="form-group col-md-4">
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Password'); ?></font></font></label>
						<input name="password" class="form-control"  type="password"  id="password"  required>                           
				</div>
				<div class="form-group col-md-4">
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Confirm Password'); ?></font></font></label>
						<input type="password" class="form-control" id="confirm_password" name="confirm_password" required>                          
				</div>
				<div class="form-group col-md-4">
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Phone number (13 digits)'); ?></font></font></label>
						<input name="phone_number" class="form-control"  type="tel"  id="UserPhone" value="{{old('phone_number')}}" required>                           
				</div>
				
			</div>
			<div class="row">
				<div class="form-group col-md-4">
						<label class="control-label"><font style="vertical-align: inherit;">
							<font style="vertical-align: inherit;"><?php echo __('Enter your company name'); ?></font></font></label>
							<input name="user_company" class="form-control"  type="text" id="UserUserCompany" value="{{old('user_company')}}"  >                          
				</div>
				<div class="form-group col-md-4">    
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Role') ?></font></font></label>
						{!! Form::select('role_id',['1'=>'Admin','2'=>'Customer',3=>'Advertiser'],null,['class'=>'form-control'] ) !!} 
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-4">    
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Select country') ?></font></font></label>
						{!! Form::select('country_id',$countries,null,['class'=>'form-control'] ) !!} 
				</div>
					<div class="form-group col-md-4">
						<label class="control-label">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
								<?php echo __('City / Town') ?></font></font></label> 
								<input name="city" class="form-control"  type="text" id="UserCity" value="{{old('city')}}" >   
					</div>
					<div class="form-group col-md-4">
						<label class="control-label">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
								<?php echo __('Street') ?></font></font></label>
								<input name="street" class="form-control"  type="text" id="UserStreet" value="{{old('street')}}" >
					</div>
			</div> 
			<div class="row">
				<div class="form-group col-md-4">
					<label class="control-label"><font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;"><?php echo __('Street number') ?></font></font></label>
						<input name="street_number" class="form-control"  type="text" id="UserStreetNumber" value="{{old('street_number')}}" >                                
					</div>
					<div class="form-group col-md-4">
						<label class="control-label"><font style="vertical-align: inherit;">
							<font style="vertical-align: inherit;"><?php echo __('Postal Code') ?></font></font></label>
							<input name="post_code" class="form-control" type="text" id="UserZip" value="{{old('post_code')}}" >                           
						</div>
						<div class="form-group col-md-4">
							<label class="control-label"><font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('VAT number') ?></font></font></label>
								<input name="vat" class="form-control"  type="text" id="UserUserVat" value="{{old('vat')}}" > 
							</div>
			</div>
			<div class="row"> 
				<div class="col-md-12">
					<div class="margiv-top-10">
						<button type="submit" class="btn btn-info btn-effect pull-right">
							<?php echo __('Add') ?>
						</button>
					</div>  
				</div>
			</div>
		</form>   
	</div>
@endsection 