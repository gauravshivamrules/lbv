@extends('layouts.login')  
@section('title',__('Reset Password')) 
@section('content') 
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form class="form-horizontal" action="/users/reset-password/{{$token}}" method="post" > 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Reset Password'); ?></legend>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Password'); ?> </label>
							<div class="col-lg-10">
								<input type="password" class="form-control" id="password" name="password" required>
								<span class="text-danger"> {{$errors->first('password')}} </span>         
							</div>
						</div> 
						<div class="form-group">
							<label for="content" class="col-lg-2 control-label"><?php echo __('Confirm Password'); ?></label>
							<div class="col-lg-10">
								<input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
								<span class="text-danger"> {{$errors->first('confirm_password')}} </span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary"><?php echo __('Reset'); ?></button>  
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
		
	</div>
@endsection