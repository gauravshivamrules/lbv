@extends('layouts.back') 
@section('title',$user->name)
@section('content')
	 
	<div class="well well bs-component">
		<form class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>
				<legend>Edit Profile</legend>
				<div class="form-group">
					<label for="name" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" id="name" name="name" value="{!! $user->name !!}">
						<span class="text-danger"> {{$errors->first('name')}} </span>         
					</div>
				</div>
				<div class="form-group">
					<label for="content" class="col-lg-2 control-label">Email</label>
					<div class="col-lg-10">
						<input type="email" class="form-control" id="email" name="email" value="{!! $user->email !!}">
						<span class="text-danger"> {{$errors->first('email')}} </span>
					</div>
				</div>
				@if($user->image)
					<div style="margin-left: 140px;">
						<h3> Current Image</h3>
						<img src="/images/user_avatars/{{$user->image}}" width="200px" />
					</div> 
				@endif
				<div class="form-group">
					<label for="content" class="col-lg-2 control-label">Image</label>
					<div class="col-lg-10">
						<input type="file" class="form-control" id="file" name="file"> 
						<span class="text-danger"> {{$errors->first('file')}} </span> 
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div> 
			</fieldset>
		</form>
	</div>
@endsection