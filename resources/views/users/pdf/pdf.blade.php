<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
     {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') !!} 
	 {!! Html::style('css/app.css') !!} 
	 {!! Html::style('css/custom.css') !!} 
	 {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') !!}
	 {!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') !!}
  </head>
  <body> 
   <div class="row"> 
   		<span> <h2 style="text-align: center;">Laravel First App</h2> </span>
   		<div class="col-md-3"> </div>
   		<div class="col-md-6"> 
   			<div class="well well bs-component">
   				<div class="content">
   				<div style="float:right;">
            @if($user->image)
              <img src="{{ asset('images/user_avatars/' . $user->image) }}" width="300px;" />   
            @else 
              <img src="{{asset('images/hotel_56d569512fdd7.jpeg')}}" />   
            @endif 
   				</div>	
   					<h2 class="header">{!! $user->name !!}</h2>
            <p> <strong>Email:{!! $user->email !!} </strong> </p>
            <p> <strong>Registered On:{!! date('d-m-Y',strtotime($user->created_at)) !!} </strong> </p>
   					<p> <strong>Role</strong>: {!! $user->role_id  !!}</p> 
   				</div> 
   			</div>
   		</div>
   		<div class="col-md-3"> </div>

   </div>

  </body>
  </html>
