@extends('layouts.back')
@section('title',_('View All Users'))
@section('content')
	<style type="text/css">
		div.dt-buttons {
		    float: right !important;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">  
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Users'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="usersTable">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>     
						<th><?php echo __('Email'); ?></th>
						<th><?php echo __('Phone'); ?></th>
						<th><?php echo __('Role'); ?></th>
						<th><?php echo __('Registered On'); ?></th>
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table>
			</div>
	</div>   
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
	<script type="text/javascript" src="/DATATABLE_BUTTON/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/jszip.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/pdfmake.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/vfs_fonts.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/buttons.html5.min.js"></script> 
		<script>
			$(function() {
			    $('#usersTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/users/getUsers",    
			        dom: 'Blfrtip',
			        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
			        buttons: [
			            {
			                extend: 'copyHtml5',
			                exportOptions: { 
			                    columns: [ 0, 1, 2,3,4 ]
			                }
			            },
			            {
			                extend: 'excelHtml5',
			                exportOptions: {
			                    columns: [ 0, 1, 2,3,4 ]
			                }
			            },
			            {
			                extend: 'pdfHtml5',
			                exportOptions: {
			                    columns: [ 0, 1, 2,3,4 ]
			                }
			            },
			            {
			                extend: 'csvHtml5',
			                exportOptions: {
			                    columns: [ 0, 1, 2,3,4 ]
			                }
			            }
			        ], 
			        columns: [
			            { data: 'first_name', name: 'first_name' },
			            { data: 'email', name: 'email' }, 
			            { data: 'phone_number', name: 'phone_number' }, 
			            { data: 'role_id', name: 'role_id' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'action', name: 'action'}    
			        ],
			    });
			});
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
			function confirmSwitch(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {  
					window.location.href='/users/switch/'+id;
				} else {  
					return false;
				} 
			}
		</script>
@endsection