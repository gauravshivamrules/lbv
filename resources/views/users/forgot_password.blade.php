@extends('layouts.login')
@section('title','|Forgot Password')
@section('content')

	<div class="login-box">
	  <div class="login-logo">
	    	<a href="/"><b><?php echo __('Logeren bij') ?></b> <?php echo __('Vlamingen') ?></a>
	  </div>
	  <div class="login-box-body">
	    <p class="login-box-msg"><?php echo __('Enter your registered email') ?></p>
	     <!-- route('login') -->
	    <form action="/forgot-password" method="post">
	    	{{csrf_field()}} 
	      <div class="form-group has-feedback">
	        <input type="email" name="email" class="form-control" placeholder="<?php echo __('Email') ?>" required>
	        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	        <span style="color:red">{{$errors->first('email')}}  </span> 
	      </div>
	      <div class="row">
	        <div class="col-xs-8">
	        </div>
	        <div class="col-xs-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Send Link') ?></button>
	        </div>
	      </div>
	    </form>
	    <a href="/login" class="btn btn-default"><?php echo __('Back'); ?></a><br>
	  </div>
	</div>
@endsection