@extends('layouts.login')
@section('title',__('Customer Register'))
@section('content')

	<div class="login-box">
	  <div class="login-logo">   
	    	<a href="/"><b><?php echo __('Logeren bij') ?></b> <?php echo __('Vlamingen') ?></a>
	  </div>
	  <div class="login-box-body">
	    <p class="login-box-msg"><?php echo __('Signup as Customer') ?></p>
	    <form action="{{ route('postCustomerRegister') }}" method="post"> 
	    	{{csrf_field()}}

	    	<div class="form-group has-feedback">
	    	  <input type="text" name="first_name" class="form-control" placeholder="<?php echo __('First Name') ?>" required>
	    	  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	    	  <span style="color:red">{{$errors->first('first_name')}}  </span>
	    	</div>
	    	<div class="form-group has-feedback"> 
	    	  <input type="text" name="last_name" class="form-control" placeholder="<?php echo __('Last Name') ?>" required>
	    	  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	    	  <span style="color:red">{{$errors->first('last_name')}}  </span>
	    	</div>
	      <div class="form-group has-feedback">
	        <input type="email" name="email" class="form-control" placeholder="<?php echo __('Email') ?>" required>
	        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	        <span style="color:red">{{$errors->first('email')}}  </span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" name="password" class="form-control" placeholder="<?php echo __('Password') ?>" required>
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	         <span style="color:red">{{$errors->first('password')}} </span>
	      </div>
	      <div class="custom-control custom-checkbox">
	          <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="tos" required> 
	          <label class="custom-control-label" for="defaultUnchecked">  <a href="/tos" target="_blank">{{__('I accept the terms and conditions')}}</a></label><br>
	          <span style="color:red">{{$errors->first('tos')}}  </span> 
	      </div>
	      <div class="row">
	        <div class="col-xs-8">
	        </div>
	        <div class="col-xs-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign Up') ?></button>
	        </div>
	      </div>
	    </form>
	    <div class="social-auth-links text-center"> 
	        <p>- <?php echo __('Or Signup with') ?> -</p>
	        <a style="font-size: 12px;" href="/auth/facebook?role=2" class="btn btn-facebook"><i class="fab fa-facebook"></i></a>
	        <a style="font-size: 12px;" href="/auth/google?role=2" class="btn btn-google"><i class="fab fa-google"></i></a>
	        <a style="font-size: 12px;" href="/auth/twitter?role=2" class="btn btn-twitter"><i class="fab fa-twitter"></i></a> 
	        <a style="font-size: 12px;" href="/auth/linkedin?role=2" class="btn btn-linkedin"><i class="fab fa-linkedin"></i></a> 
	    </div> 
	    <a href="/login" class="btn btn-default"><?php echo __('Back') ?></a>
	  </div>
	</div>
@endsection