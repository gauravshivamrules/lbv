@extends('layouts.login')  
@section('title',__('Write Review')) 
@section('content')
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form class="form-horizontal" action="/bookings/write-review" method="post" > 
					{{csrf_field()}}
					<fieldset>
						<legend><?php echo __('Here you can write your review and and rate your experience of stay'); ?></legend>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Locatie'); ?> </label>
							<div class="col-lg-10">
								<div id="rateYo1"></div>
								<input type="hidden" name="rate[1]" id="location">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Hygiene'); ?> </label>
							<div class="col-lg-10">
								<div id="rateYo2"></div>
								<input type="hidden" name="rate[2]" id="hygiene">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Comfort'); ?> </label>
							<div class="col-lg-10">
								<div id="rateYo3"></div>
								<input type="hidden" name="rate[3]" id="comfort">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Service'); ?> </label>
							<div class="col-lg-10">
								<div id="rateYo4"></div>
								<input type="hidden" name="rate[4]" id="service">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Prijskwaliteit'); ?> </label>
							<div class="col-lg-10">
								<div id="rateYo5"></div>
								<input type="hidden" name="rate[5]" id="pricequality">
							</div>
						</div> 


						<div class="form-group">
							<label for="name" class="col-lg-2 control-label"><?php echo __('Write Review'); ?> </label>
							<div class="col-lg-10">
								<textarea name="review" class="form-control" required></textarea>
								<input type="hidden" name="bookingToken" value="{{$bookingToken}}">
							</div>
						</div> 
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary"><?php echo __('Submit'); ?></button>  
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
	<script src="/js/front/jquery.min.js"></script>  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
	<script type="text/javascript">
		$(function () {
			$("#rateYo1").rateYo({
			  	starWidth: "30px",
			  	fullStar: true,
			  	onChange: function (rating, rateYoInstance) {
			  		console.log(rating);
			  		$('#location').val(rating);
	    		}
    		});
			$("#rateYo2").rateYo({
			  	starWidth: "30px",
			  	fullStar: true,
			  	onChange: function (rating, rateYoInstance) {
			  		console.log(rating);
			  		$('#hygiene').val(rating);
	    		}
    		});
    		$("#rateYo3").rateYo({
			  	starWidth: "30px",
			  	fullStar: true,
			  	onChange: function (rating, rateYoInstance) {
			  		console.log(rating);
			  		$('#comfort').val(rating);
	    		}
    		});
    		$("#rateYo4").rateYo({
			  	starWidth: "30px",
			  	fullStar: true,
			  	onChange: function (rating, rateYoInstance) {
			  		console.log(rating);
			  		$('#service').val(rating);
	    		}
    		});
    		$("#rateYo5").rateYo({
			  	starWidth: "30px",
			  	fullStar: true,
			  	onChange: function (rating, rateYoInstance) {
			  		console.log(rating);
			  		$('#pricequality').val(rating);
	    		}
    		});
		});
	</script
@endsection