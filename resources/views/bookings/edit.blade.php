@extends('layouts.back') 
@section('title',__('Edit Booking')) 
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-3">	
			<h4> {{__('Customer')}} </h4>
			<p> {{$booking->fName}} {{$booking->lName}} </p>
			<p> {{$booking->cEmail}}  </p>
			<p> {{$booking->cPhone}}  </p> 
			</div>
			<div class="col-md-6">
				<form class="form-horizontal" action="/bookings/edit/{{$booking->id}}" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Edit Booking'); ?></legend>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('From'); ?> </label>
							<div class="col-lg-8">
								<input type="text" name="booking_start_date" id="booking_start_date" value="{{$booking->book_from}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('To'); ?></label>
							<div class="col-lg-8">
								<input type="text" name="booking_end_date" id="booking_end_date" value="{{$booking->book_to}}" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Adults'); ?></label>
							<div class="col-lg-8">
								<input type="number" min="0" name="adult" id="adult" value="{{$booking->adult}}" class="form-control">
							</div>
						</div>
						@if($booking->child)
							<div class="form-group">
								<label for="content" class="col-lg-4 control-label"><?php echo __('Childs'); ?></label>
								<div class="col-lg-8">
									<input type="number" min="0" name="child" id="child" value="{{$booking->child}}" class="form-control">
								</div>
							</div>
						@endif
						
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Booking Amount'); ?>(&euro;)</label>
							<div class="col-lg-8">
								<input type="number" min="0" name="booking_amount" id="booking_amount" value="{{$booking->booking_amount}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Extra Charges'); ?>(&euro;)</label>
							<div class="col-lg-8">
								<input type="number"  min="0"name="extra_charges" id="extra_charges" value="{{$booking->extra_charges}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Total Amount'); ?>(&euro;)</label>
							<div class="col-lg-8">
								<input type="number" min="0" name="total_cost" id="total_cost" value="{{$booking->total_cost}}" class="form-control" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Customer Comments'); ?></label>
							<div class="col-lg-8">
								<p style="text-align:justify;"> {{$booking->comments}} </p>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Your Comments'); ?></label>
							<div class="col-lg-8">
								<textarea name="adv_comments" class="form-control">{{$booking->adv_comments}} </textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-6 control-label"><?php echo __('Send notification mail to customer?'); ?></label>
							<div class="col-lg-6">
								<input type="checkbox"  name="notifyAdv"> 
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Update'); ?></button> 
							</div>
						</div>

					</fieldset>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div> 
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
	<script>
		updateAmount();
		$('#booking_amount').change(function(){
			updateAmount();
		});
		$('#extra_charges').change(function(){
			updateAmount(); 
		});
		$("#booking_start_date").datepicker({ 
		    dateFormat: 'dd-mm-yy',
		    minDate:0,
		    onSelect:function(date) {
		      var selectedDate=new Date(date);
		      var endDate=new Date(selectedDate.getTime() );
		      var date2=$('#booking_start_date').datepicker("getDate");
		      // date2.setDate(date2.getDate()+addDay); 
		      $('#booking_end_date').datepicker('setDate', date2);
		      $('#booking_end_date').datepicker("option","minDate",date2);     
		    }
		});
		$("#booking_end_date").datepicker({
		   minDate:0,
		   dateFormat:'dd-mm-yy'
		});
		function updateAmount(){
			var bAmount=$('#booking_amount').val();
			var eAmount=$('#extra_charges').val();
			var tAmount=parseInt(bAmount)+parseInt(eAmount);
			$('#total_cost').val(tAmount); 
		}
	</script>
@endsection