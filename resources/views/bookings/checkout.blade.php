@extends('layouts.back') 
@section('title',__('Bookings Checkout')) 
@section('content')
 
</ul>
<div class="well well bs-component"> 
	<div id="cancelPaypalPayment" class="callout callout-danger" style="display:none;"></div>
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">  
				<div class="panel panel-default">
				    <div class="panel-heading">{{__('Bookings Checkout')}} </div> 
				    <div class="panel-body">
				    	<table class="table table-bordered">
				    		<thead> 
				    			<tr>
				    				<th> <?php echo __('Description'); ?> </th>
				    				<th> <?php echo __('Total Amount'); ?> (&euro;) </th>
				    			</tr>
				    		</thead>
				    		<tbody id="promotionsTableBody">
				    			<tr>
				    				<td> {{__('10% booking commission for invoice') }} {{$payment->invoice_number}} </td>
				    				<td> 
				    					{{$payment->amount}} 
				    				</td>
				    			</tr>  
				    			<tr> 
				    				<td colspan="5" style="text-align:right"> <b>{{__('Total amount')}}:</b> &euro;{{$payment->amount}}   </td>
				    			</tr>
				    		</tbody>
				    	</table>
				    	
				    
				    	<br> 
				    	<div id="paymentForm"> 

	    			    	<div class="row"> 
	    			    		<div class="col-md-1"></div>   
	    			    		<div class="col-md-10">
	    			    			<fieldset>
	    			    				<legend><?php echo __('Select Payment'); ?></legend> 
	    			    					<div class="col-xs-4"> 
	    			    						<div class="form-group">
	    			    							<div class="col-lg-10 col-lg-offset-2"> 
	    			    								
	    			    								
	    			    							</div>
	    			    						</div>	
	    			    					</div>  
	    			    					<div class="col-xs-4">
	    			    						<div class="form-group">
	    			    							<div class="col-lg-10 col-lg-offset-2"> 
	    			    								<form method="post" action="/bookings/mollie" id="promotionMollieForm" onsubmit="return confirm('Are you sure?');"> 
	    			    									{{csrf_field()}}  
	    			    									<input type="hidden" name="booking_commission_id"  value="{{$payment->booking_commission_id}}">
	    			    									<button type="submit" class="btn green"> 
	    			    											{{__('Checkout')}}
	    			    									</button>	  

	    			    								</form>
	    			    							</div> 
	    			    						</div>
	    			    					</div>
	    			    					<div class="col-xs-4"> 
	    			    						
	    			    					</div>
	    			    			</fieldset>

	    			    		</div>
	    			    		<div class="col-md-1"></div>
	    			    	</div>
	    			    	
				    	</div>
				    	
				    	
					</div>
				</div>
			</div>
			<div class="col-md-1"></div> 
		</div>	
</div> 
@endsection