<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
   {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') !!} 
   {!! Html::style('css/app.css') !!} 
   {!! Html::style('css/custom.css') !!} 
   {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') !!}
   {!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') !!}
   <style type="text/css">
      body {color: black;}
   </style>
  </head>
  <body> 
   <div class="row"> 
      <span> <h2 style="text-align: center;">{{env('SITE_TITLE_FRONT')}}</h2> </span>
      <span style="float:right;"> {{__('Date')}}: {{date('d-m-Y',strtotime($licenses->first()->created_at))}}</span>

      <div class="col-md-1"> </div>
      <div class="col-md-10"> 
        <div class="well well bs-component">
          <div class="content">
            <div style="float:left;">  
              <h4> {{ __('Client') }} </h4>
              {{__('Name')}}:{{$licenses->first()->first_name}} {{$licenses->first()->last_name}} <br>
              {{__('Client  Number')}}: {{$licenses->first()->user_id}} <br>
              {{__('Company')}}: {{$licenses->first()->user_company}} <br>
              {{$licenses->first()->street_number}} {{$licenses->first()->street}} <br> 
              {{$licenses->first()->post_code}}  {{$licenses->first()->city}} <br> 
              {{$licenses->first()->countryName}} <br>
              {{$licenses->first()->phone_number}} <br>
              {{$licenses->first()->email}} <br>
              {{__('Btw No.')}}:

            </div>
            <div style="float:right;"> 
               <h4> {{__('Payment Details')}}</h4>

               <?php 
               $status=__('Not Paid'); 
               if($licenses->first()->status==1) {
                  $status=__('Paid');
               } else if($licenses->first()->payment_complete==2){
                  $status=__('Cancelled');
               } else if($licenses->first()->payment_complete==3){
                  $status=__('Free');
               } 
               ?>
               {{__('Status')}}: {{ $status}}<br>
               {{__('Method')}}: {{$licenses->first()->paymentName}} ({{ucfirst($licenses->first()->method)}}) <br> 
               {{__('Invoice Number')}}: {{$licenses->first()->invoice_number}} <br>
            </div> 
          </div>
        </div>
      </div>
      <div class="col-md-1"> </div>
   </div>
  

   <table style="width:100%;margin-top:10px;">
      <tr>
        <th style="text-align:left;"><?php echo __('Description'); ?></th>
        <th style="text-align:right;"><?php echo __('Booking Price') ?>(&euro;) </th>  
        <th style="text-align:right;"><?php echo __('10% commission') ?></th>  
        <th style="text-align:right;"><?php echo __('Total'); ?>(&euro;)</th> 
      </tr>
      @foreach($licenses as $l) 
      <tr style="background:#F1F4F7;">
        <td><h4>{{__('10% booking commission') }}</h4></td>
        <td style="text-align:right;"> {{$l->total_amount}}</td>
        <td style="text-align:right;"> {{$l->amount}}</td> 
        <td style="text-align:right;"> {{$l->amount}}</td> 
      </tr>     
      @endforeach  
   </table>

   <div class="row" style="width:100%;margin-top:8%;">
    <div class="col-md-1"> </div>
    <div class="col-md-10"> 

        <div style="width:35%;float:left;text-align:left;"> 
          <b>{{ env('SITE_TITLE_FRONT') }}</b><br>
          Joeri De Bonnaire<br>
          Lencouet, 47230 Feugarolles, Frankrijk<br> 
          Siret nr. : 53453667700014<br>
          {{ env('SITE_EMAIL') }}
        </div>
        <div style="width:33%;float:right;background:#F1F4F7;padding:10px;"> 
          <table style="width:100%;">
            <tr>
              <td style="text-align:left;"><b><?php echo __('SUBTOTAAL'); ?></b></td>
              <td style="text-align:right;">&euro; {{$licenses->first()->amount}}</td>  
            </tr> 
            <tr>
              <td style="text-align:left;"><b><?php echo __('Total'); ?></b></td>
              <td style="text-align:right;">&euro; {{$licenses->first()->amount}}</td>    
            </tr>
            <tr> 
              <td style="text-align:left;" colspan="2"><b><?php echo __('BTW niet van toepassing article 293 B van CGI'); ?></b></td> 
            </tr>  
          </table>
        </div>
    </div>
    <div class="col-md-1"> </div>
   </div>
  </body>
  </html>
