@extends('layouts.back')
@section('title',_('My Bookings Invoices'))
@section('content')
	<style type="text/css">
		div.dt-buttons {
		    float: right !important;
		}
	</style> 
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">  
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Booking Invoices'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="bookingsTable">
				<thead>
					<tr>
						<th><?php echo __('Invoice#'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Method'); ?></th>
						<th><?php echo __('Booking Amount'); ?>(&euro;)</th>
						<th><?php echo __('Commission'); ?>(&euro;)</th>      
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table> 
			</div>
	</div>   
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script> 
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
		<script>
			$(function() {
			    $('#bookingsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/bookings/invoices-ajax",    
			        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
			        columns: [
			        	{ data: 'invoice_number', name: 'invoice_number'}, 
			        	{ data: 'status', name: 'status'},
			        	{ data: 'method', name: 'method'},      
			            { data: 'total_amount', name: 'total_amount' }, 
			            { data: 'commission', name: 'commission' }, 
			            { data: 'action', name: 'action'}    
			        ],
			    });
			});
		</script>
@endsection