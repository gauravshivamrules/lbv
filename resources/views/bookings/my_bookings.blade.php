@extends('layouts.back')
@section('title',_('My Bookings'))
@section('content')
	<style type="text/css">
		div.dt-buttons {
		    float: right !important;
		}
	</style> 
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">  
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Bookings'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="bookingsTable">
				<thead>
					<tr>
						<th><?php echo __('Accomodation'); ?></th>
						<th><?php echo __('Advertiser'); ?></th>      
						<th><?php echo __('Email'); ?></th>
						<th><?php echo __('From'); ?></th>
						<th><?php echo __('To'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table> 
			</div>
	</div>   
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script> 
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
		<script>
			$(function() {
			    $('#bookingsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/bookings/my-bookings-ajax",    
			        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
			        columns: [
			            { data: 'name', name: 'name' }, 
			            { data: 'fName', name: 'fName' }, 
			            { data: 'email', name: 'email'},
			            { data: 'book_from', name: 'book_from'},      
			            { data: 'book_to', name: 'book_to'}, 
			            { data: 'status', name: 'status'},      
			            { data: 'action', name: 'action'}    
			        ],
			    });
			}); 
		</script>
@endsection