@extends('layouts.back') 
@section('title','View Booking') 
@section('content')
 	<div class="well well bs-component">   
		<div class="row"> 
			<fieldset>
				<legend> {{$booking->name}}  </legend>  
				
			</fieldset>
			<div class="col-md-3">
				@if(Auth::user()->role_id==2)
					<h4> {{__('Advertiser')}} </h4>
					<p> {{$booking->aFname}} {{$booking->aLname}} </p>
					@if($paymentType==1)
						<p> {{$booking->aEmail}}  </p>
						<p> {{$booking->APhone}}  </p>
					@else
						@if($booking->status==0 || $booking->status==3)
							<p>*********</p>
							<p>*********</p>
						@else
							<p> {{$booking->aEmail}}  </p>
							<p> {{$booking->APhone}}  </p>
						@endif
					@endif
				@else
					<h4> {{__('Customer')}} </h4>
					<p> {{$booking->fName}} {{$booking->lName}} </p>
					@if($paymentType==1 )
						<p> {{$booking->cEmail}}  </p>
						<p> {{$booking->cPhone}}  </p>
					@else
						@if($booking->status==0 || $booking->status==3)
							<p>*********</p>
							<p>*********</p>
						@else
							<p> {{$booking->cEmail}}  </p>
							<p> {{$booking->cPhone}}  </p>
						@endif
					@endif
				@endif
			</div>
			<div class="col-md-4"> 
				<h4> {{__('Booking Detail')}} </h4>
				<p> <b>{{__('Status')}}:</b> 
					@if($booking->status==0)
						<span class="label label-warning">{{__('PENDING')}}</span>
					@elseif($booking->status==2)
						<span class="label label-success">{{__('COMPLETED')}}</span>
					@elseif($booking->status==1)
						<span class="label label-success">{{__('ACCEPTED')}}</span>
					@else
						<span class="label label-danger">{{__('REJECTED')}}</span>
					@endif
				</p>

				<p> <b>{{__('From')}}:</b> {{date('d-m-Y',strtotime($booking->book_from))}} <b>{{__('To')}}: </b> {{date('d-m-Y',strtotime($booking->book_to))}}  </p>
				<p> <b>{{__('Adults')}}:</b> {{$booking->adult}}</p>
				@if($booking->child)
					<p> <b>{{__('Childs')}}:</b> {{$booking->child}}</p>
				@endif
				
			</div>
			<div class="col-md-4">
				<h4> {{__('Amount Detail')}} </h4>
				<p> <b>{{__('Booking Amount')}}:</b> &euro;{{$booking->booking_amount}}</p>
				<p> <b>{{__('Extra Charges')}}:</b> &euro;{{$booking->extra_charges?$booking->extra_charges:0.00}}   </p>
				<p> <b>{{__('Total')}}:</b> &euro;{{$booking->total_cost}}</p>
			</div>
		</div>
	</div>
	@if($roomsArr) 
	<div class="row">
		<div class="col-md-12">
		<h4> {{__('Rooms')}} </h4> 
			@foreach($roomsArr as $r)
				<div class="col-md-4">
				<b>{{$r['room_count']}} X</b> <p>{{$r['room_type']}}</p>
				</div>
			@endforeach
		</div>
	</div>
	@endif
	@if(Auth::user()->role_id==3)
	<div class="row">
		<div class="col-md-12">
			<h4> {{__('Customer Comments')}} </h4> 
			<p style="text-align:justigy;"> {{$booking->comments}} </p>
			<br><br>
			<legend> 
				<a href="javascript:;" onclick="submitAction('edit',{{$booking->id}})"  class="btn btn-primary" ><?php echo __('Edit') ?></a> &nbsp;
				<a href="javascript:;" onclick="submitAction('approve',{{$booking->id}})" class="btn btn-warning" ><?php echo __('Approve') ?></a> &nbsp;
				<a href="javascript:;" onclick="submitAction('complete',{{$booking->id}})" class="btn btn-success" ><?php echo __('Complete') ?></a> &nbsp;
				<a href="javascript:;" onclick="submitAction('reject',{{$booking->id}})" class="btn btn-danger" ><?php echo __('Reject') ?></a>
				@if(Auth::user()->role_id==3)
					@if(!is_null($booking->bookStatus))
						@if($booking->bookStatus==0)
							<a href="javascript:;" onclick="submitAction('pay',{{$booking->bookId}})" class="btn btn-success" ><?php echo __('Pay Now') ?></a> 
						@endif
					@endif
				@endif 
			 </legend>  
			
		</div>
	</div>
	@elseif(Auth::user()->role_id==2)
		@if($booking->adv_comments)
			<div class="row">
				<div class="col-md-12">
					<h4> {{__('Owner Comments')}} </h4> 
					<p style="text-align:justigy;"> {{$booking->adv_comments}} </p> 
				</div>
			</div>
		@endif
	@endif
	<script type="text/javascript">
		function submitAction(type,id) {
			if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
				if(type=='edit'){
					window.location.href='/bookings/edit/'+id;  
				}else if(type=='approve'){
					window.location.href='/bookings/approve/'+id;  
				} else if(type=='reject'){
					window.location.href='/bookings/reject/'+id;  
				} else if(type=='complete'){
					window.location.href='/bookings/compelete/'+id;  
				} else if(type=='pay'){
					window.location.href='/bookings/pay/'+id;  
				} 
			} else { 
				return false;
			} 
		}
	</script>
@endsection