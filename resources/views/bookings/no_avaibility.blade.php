@extends('layouts.back') 
@section('title',__('Block dates')) 
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
	<link rel="stylesheet" type="text/css" href="/css/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="/css/fullcalendar.print.min.css" media="print">
	<style type="text/css">
		.fc-time{
		   display : none;
		}
	</style>
	<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-12"> 
					<fieldset>
						<legend><?php echo __('Add Block dates'); ?></legend>
						<div class="form-group">
								<div class="col-lg-3">
									{!! Form::select('accom_id',$accoms,null,['id'=>'accomId','class'=>'form-control','required'=>true] ) !!} 
								</div>
							<div id='appendRooms' style="display:none"> 
								<div class="col-lg-2">
									<select id="roomId" class="form-control"></select>
								</div>
							</div>
							<div id="appendNoOfRooms" style="display:none">
								<div class="col-lg-1">
									<select id="selectNoOfRooms" class="form-control"></select>
								</div>
							</div>	
							<div class="col-lg-2"> 
								<input type="text" class="form-control" name="start" id="start" placeholder="{{__('Start Date')}}">
							</div>
							<div class="col-lg-2"> 
								<input type="text" class="form-control" name="end" id="end" placeholder="{{__('End Date')}}">
							</div>
							<div class="col-lg-1"> 
								<button type="button" onclick="addNoAvability();" class="btn btn-primary"><?php echo __('Add'); ?></button> 
							</div>
						</div> 
					</fieldset>
			</div>
		</div>
		<div class="row">
			<div id='calendar'></div>
		</div> 
	</div>
	<div class="modal fade" id="avabilityEdit">
	      <div class="modal-dialog" role="document">
	        <div class="modal-content"> 
	          <div class="modal-header">
	            <h5 class="modal-title"><?php echo __('Edit Avaibility'); ?></h5>
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div> 
	          <div class="modal-body">   
	            <form action="/bookings/update-noavability" method="post" accept-charset="utf-8" > 
	            {{csrf_field()}}
	            <div class="row"> 
	              <div class="form-group col-md-3"></div>
	              <div class="form-group col-md-6">
	                <label class="control-label">
	                  <font style="vertical-align: inherit;">
	                    <font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
	                  </font>
	                </label>
	                	<input type="hidden" name="id" id="id">
						{!! Form::select('accom_id',$accoms,null,['id'=>'accomIdModal','class'=>'form-control','required'=>true] ) !!} 
	              </div>
	              <div class="form-group col-md-3"></div>
	            </div>
	            <div class="row"> 
	              <div class="form-group col-md-3"></div>
	              <div class="form-group col-md-6">
	                <label class="control-label">
	                  <font style="vertical-align: inherit;">
	                    <font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
	                  </font>
	                </label>
	                <input name="start_date" class="form-control valid" type="text" id="startEdit" required="true">                        
	              </div>
	              <div class="form-group col-md-3"></div>
	            </div>
	            <div class="row">
	            	<div class="form-group col-md-3"></div>
	            	<div class="form-group col-md-6">
	            	  <label class="control-label">
	            	    <font style="vertical-align: inherit;">
	            	      <font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
	            	    </font>
	            	  </label>
	            	  <input name="end_date" class="form-control valid" type="text" id="endEdit" required="true">                        
	            	</div>
	            	<div class="form-group col-md-3"></div>
	            </div>
	            </div>
	          <div class="modal-footer">
	           <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-right" id="addBtn"><?php echo __('Update'); ?></button>&nbsp;&nbsp;
	           <button type="button"  class="btn btn-danger btn-effect pull-right" id="deleteBtn"><?php echo __('Delete'); ?></button>
	           <button type="button" style="float:left;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
	          </div>
	          </form>
	       
	        </div>
	      </div>
	</div>  
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
	<script type="text/javascript" src="/js/moment.js"></script> 
	<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			setTextDates();
			getCalendarEvents();
			setTimeout(function(){ makeNoOfRooms()},500);
			$('#accomId').change(function(){
				getCalendarEvents();
			});
			$('#roomId').change(function(){
				makeNoOfRooms();
			});
			$('#deleteBtn').click(function(){
				if(window.confirm("{{__('Are you sure?')}}")){
					window.location.href="/bookings/delete-noavability/"+$('#id').val();
				}else{
					return false;
				}
			});
		}); 
		function startBlock() {
			$.blockUI({ message: '<h1>Loading...</h1>',css: { backgroundColor: '#000', color: '#fff'}});
		}
		function endBlock() {
			$.unblockUI();	
		}
		function addNoAvability() { 
			if(!$('#start').val() || !$('#end').val()) {
				alert("{{__('Please select both dates')}}");
				return false;
			}
			var roomID,noOfRoom;
			if($('#appendRooms').is(":visible")){
				roomID=$('#roomId').val();
				noOfRoom=$('#selectNoOfRooms').val();
			}
			$.ajax({
				type:'POST',
				data:{accom:$('#accomId').val(),start:$('#start').val(),end:$('#end').val(),roomid:roomID,nor:noOfRoom},
				url:'/bookings/add-noavability',
				success:function(r){
					if(r=='true'){
						getCalendarEvents();
						$.blockUI({ message: '<h1>Saved Successfully</h1>',css: { backgroundColor: '#000', color: '#fff'}});
						endBlock();
					}else{
						$.blockUI({ message: '<h1>ERROR: Try again</h1>',css: { backgroundColor: '#000', color: '#fff'}});
						endBlock();
					}
				}
			});
		}
		function getCalendarEvents() {
			if($('#accomId').val()){
				startBlock();
				$.ajax({
					url:'/bookings/get-accom-noavability/'+$('#accomId').val(),
					success:function(r){
						events=$.parseJSON(r);
						if(events.priceUnit=='perroom' || events.priceUnit=='perperson'){
							var options='';
							$.each(events.rooms,function(i,v){
								options+='<option value="'+v.id+'">'+v.room_type+'</option>';
							});
							$('#roomId').empty();
							$('#roomId').append(options);
							makeNoOfRooms();
							$('#appendRooms').show();
							$('#appendNoOfRooms').show();
						}else{
							$('#appendRooms').hide();
							$('#appendNoOfRooms').hide();
						}
						displayCal(events.noAvability); 
					}
				});
				endBlock();
			}
		}
		function makeNoOfRooms() {
			$.ajax({
				url:'/bookings/get-no-of-rooms/'+$('#roomId').val(),
				success:function(r){
					var options='';
					for(var i=1;i<=r;i++){
						options+='<option value="'+i+'">'+i+'</option>';
					}
					$('#selectNoOfRooms').empty();
					$('#selectNoOfRooms').append(options);
					$('#appendNoOfRooms').show();
				}
			});
		}
		function displayCal(events) {  
			$('#calendar').fullCalendar( 'removeEvents'); 
			$('#calendar').fullCalendar( 'addEventSource', events);  
			$('#calendar').fullCalendar(
				{
					header    : {
					       left  : 'prev,next today',
					       center: 'title',
					       right : 'month,agendaWeek,agendaDay'
					     },
					     buttonText: { 
					       today: 'today',
					       month: 'month',
					       week : 'week',
					       day  : 'day'
					     }, 
					events: events,
					eventRender: function(event, element) {
					    $(element).tooltip({title: event.hover});             
					}, 
					eventClick: function(event) { 
						$('#id').val(event.id); 
						$("#accomIdModal").val(event.accom);
						$('#startEdit').val(event.start_date);
						$('#endEdit').val(event.end_date);  
						$('#avabilityEdit').modal('show');
					}
				}
			);
		}  
		function setTextDates(){
			$("#start").datepicker({
				minDate:0,
				dateFormat:'dd-mm-yy',
				onSelect:function(date) {
					var selectedDate=new Date(date);
					var endDate=new Date(selectedDate.getTime() );
					jQuery('#end').datepicker("option","minDate",
					    $('#start').datepicker("getDate"));       
				} 
			});
			$("#end").datepicker({dateFormat:'dd-mm-yy'});
			$("#startEdit").datepicker({
				minDate:0,
				dateFormat:'dd-mm-yy',
				onSelect:function(date) {
					var selectedDate=new Date(date);
					var endDate=new Date(selectedDate.getTime() );
					jQuery('#endEdit').datepicker("option","minDate",
					    $('#startEdit').datepicker("getDate"));       
				} 
			});
			$("#endEdit").datepicker({dateFormat:'dd-mm-yy'});
		}

	</script>
@endsection