@extends('layouts.front') 
@section('title',__(env('SITE_TITLE_FRONT'))) 
@section('content')
<style type="text/css">
	.error{color: red;}
	header {
	    background-image: url(../css/images/header-advertisers.jpg);
	    background-repeat: no-repeat;
	    background-attachment: scroll;
	    background-position: center center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    background-size: cover;
	    -o-background-size: cover;
	    text-align: center;
	    color: #fff;
	}
</style>
<div class="header-right"> 
	<header>
		<div class="container">
			<div class="intro-text" style="float:left;">
               <a href="/last-minutes" class="page-scroll btn btn-xl"><img src="/images/last-minutes.svg"></a>
           </div>
       </div>
   </header> 
</div>
<div class="service-top">
	<div class="ser-top">
	<h4>  </h4>
	</div> 
</div>
<div class="services-grid row">
	<div class="col-md-6 service-top1">
		<div class="ser-top">
			<h4>{{__('Welcome To')}} {{env('SITE_TITLE_FRONT')}}</h4>
		</div>
		
		<div class="clearfix"> </div>
		<p style="text-align:justify;"> {{__('Through this website you can give your accommodation a place on the internet. The possibilities are very extensive.In the first place, your accommodation will receive a page on this website. On that page are not only the most important data, but also a photo gallery with up to 8 photos, extra tabs, a Google Maps map and reviews. In addition, your accommodation ends up in our search resultsYou can rent out your accommodation with a fixed subscription or with a commission (no cure no pay).Secondly, you can put your accommodation even more in the spotlight to get more visitors through numerous promotional opportunities, such as tip of the month, in the spotlight or a last minute')}} </p>
		</div>
		<div class="col-md-6 service-top1">
			
			<div class="ser-top">
				<h4>{{__('Prices')}}</h4>
			</div>
			<div class="clearfix"> </div>
			<table class="table">
				<thead>
					<tr><th>{{_('Subscription')}} </th>
						<th>{{_('Type')}}</th>
						<th>{{_('Duration')}}</th>
						<th>{{_('Price')}}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $d)
						<tr>
							<td style="text-align:left;" title="{{$d->name}}">{{__($d->name)}}</td>
							<td> {{$d->type_id==2?'Sale':'Rent'}} </td>
							<td>{{$d->duration}} {{$d->duration_type}} </td>
							<td style="text-align:left;">€ {{$d->amount}}</td>
						</tr>
					@endforeach
						<tr> 
							<td colspan="3" style="text-align: left;"> {{__('No Cure No Pay')}}</td>
							<td> {{__('10% of the total rent')}}</td>
						</tr>
						<tr> 
							<td colspan="4">    

								{{__('For more explanation, see our terms and conditions')}} <a style="color: black;font-weight: bold;" href="/terms"> {{__('here')}}</a>

							</td>   
						</tr>
					</tbody>
			</table>
		</div>
		<div class="clearfix"> </div>
		
</div>
@endsection