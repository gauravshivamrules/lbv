@extends('layouts.front') 
@section('title',__(env('SITE_TITLE_FRONT'))) 
@section('content')
<style type="text/css">
	.error{color: red;}
	header {
	    background-image: url(../css/images/header-last-minutes.jpg);
	    background-repeat: no-repeat;
	    background-attachment: scroll;
	    background-position: center center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    background-size: cover;
	    -o-background-size: cover;
	    text-align: center;
	    color: #fff;
	}
</style>
<div class="header-right">
	<header>
		<div class="container">
			<div class="intro-text" style="float:left;">
               <a href="/last-minutes" class="page-scroll btn btn-xl"><img src="/images/last-minutes.svg"></a>
           </div>
       </div>
   </header> 
</div>
<div class="service-top">
	<h3>{{__('Last Minutes')}} </h3>
</div>
@if($minutes)
	@foreach($minutes as $n) 
<div class="services-grid row">
	<div class="col-lg-4 service-top1">
		
			<div class="ser-top">
				<h4><a href="/accomodations/view/{{$n->slug}}">{{$n->name}}</a></h4>
				<h5>{{ucfirst($n->rName)}}, {{ucfirst($n->cName)}}</h5><br>
				<a href="/accomodations/view/{{$n->slug}}"><img class="img-responsive zoom-img" src="/images/gallery/{{$n->id}}/{{$n->featured_image}}" alt=""> </a>
			</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-lg-8 service-top1">
				<!-- <div class=" ser-grid">	
					<a href="javascript:;" class="hi-icon hi-icon-archive glyphicon glyphicon-leaf"> </a>
				</div> -->
				<div class="ser-top">
					<h4>{{$n->title}}</h4>
					<p> {{strip_tags($n->description)}} </p>
					</div>
					<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
		@endforeach
		@else
			<p style="text-align:center;"> {{__('No last minutes found')}} </p>
		@endif

@endsection