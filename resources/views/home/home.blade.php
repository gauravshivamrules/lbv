@extends('layouts.front') 
@section('title',__(env('SITE_TITLE_FRONT'))) 
@section('content')
<style type="text/css">
.corner-ribbon {
	width: 200px;
	background: #e43;
	position: absolute;
	top: 47px !important;
	left: -38px !important;
	text-align: center;
	line-height: 30px;
	letter-spacing: 1px;
	color: #f0f0f0;
	transform: rotate(-45deg);
	-webkit-transform: rotate(-45deg);
}
.search_box{
	position: absolute;
	top: 360px;
	width: 430px;
	right: 0;
}
.srch {
    background: #fed136;
    width: 250px;
    padding-top: 7px;
    color: #000;
    border-radius: 5px 5px 0 0;
}
.btn-xl {
    top: 391px;
}
.search_box {
    position: absolute;
    top: 400px;
    width: 360px;
    right: 0;
}






.home-newsletter .single_news_ltr {
max-width: 650px;
margin: 0 auto;
text-align: center;
position: relative;
z-index: 2; }
.home-newsletter .single_news_ltr h2 {
font-size: 22px;
color: white;
text-transform: uppercase;
margin-bottom: 40px; }
.home-newsletter .single_news_ltr .form-control {
height: 50px;
background: rgba(255, 255, 255, 0.6);
border-color: transparent;
border-radius: 20px 0 0 20px; }
.home-newsletter .single_news_ltr .form-control:focus {
box-shadow: none;
border-color: #243c4f; }
.home-newsletter .single_news_ltr .btn {
min-height: 50px; 
border-radius: 0 20px 20px 0;
background: #243c4f;
color: #fff;
}
.home-newsletter {
    background: #59abfd;
}
/*------------------------------*/

@media only screen and (max-width: 426px) {
	.search_box {
    position: absolute;
    top: 310px;
    width: 324px;
    right: 21px;
}
	.btn-xl {
		top: 160px;
	}
	header {
    height: 430px;
}
header .intro-text {
    margin-left: 60px;
}
}
/*------------------------------*/
</style>
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
<div class="header-right">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="intro-text" style="float:left; text-align: left;">
						<a href="/last-minutes" class="page-scroll btn btn-xl"><img src="/images/last-minutes.svg"></a>
						<!-- <form action="/hms/accommodations" method="GET"> </form> -->
		</div>
	</div>
	<div class="col-lg-6">
		<div class="intro-text" class="flt_right ">
			<a href="/search" class="page-scroll btn btn-xl"><i class="glyphicon glyphicon-search" style="color: #000;"></i>{{__('Search')}} </a> 
		</div>
	</div>
</div>



</div>
<!-- <input type="text"  placeholder="Search your accommodation" style="color:black"> -->
</header> 
</div>
<div class="content-grid">
	<div class="container">
		@if($tipAccom)

		@foreach($tipAccom['accom'] as $sA)
		<div class="col-md-4 box_2">
			<div class="tag">
				<div class="corner-ribbon top-left sticky red shadow">{{__('TIP OF THE MONTH')}} </div>
				<a href="/accomodations/view/{{$sA->slug}}" class="mask">
					<img class="img-responsive zoom-img" src="/images/gallery/{{$sA->id}}/{{$sA->thumb}}" alt="" onerror="this.src='/images/no-image.png'">
					<span class="four">{{$sA->price?$sA->price:''}}&euro;</span>
				</a>
				<div class="most-1">
					<h5><a href="/accomodations/view/{{$sA->slug}}">{{$sA->name}} </a></h5> 
					<p>{{ucfirst($sA->region)}},{{ucfirst($sA->country)}} </p>
				</div>
			</div>
		</div>
		@endforeach
		@else
		<h4 style="text-align:center;color: black;"> {{__('No Tip Found')}} </h4>
		@endif
		<!-- <div class="clearfix"> </div> -->
	</div>
</div>
<div class="services">
	<div class="container">
		<div class="services-grid"> 
			@if($spotAccoms)
			@foreach($spotAccoms['accom'] as $sA)
			<div class="col-md-3 box_2">
				<div class="tag">
					@if($sA->thumb) 
					<div class="corner-ribbon top-left sticky red shadow">{{__('SPOTLIGHT')}} </div>
					<a href="/accomodations/view/{{$sA->slug}}" class="mask">
						<img class="img-responsive zoom-img" src="/images/gallery/{{$sA->id}}/{{$sA->thumb}}" alt="" onerror="this.src='/images/no-image.png'">
						<span class="four">{{$sA->price?$sA->price:''}}&euro;</span>
					</a>
					@endif
					<div class="most-1">
						<h5><a href="/accomodations/view/{{$sA->slug}}">{{$sA->name}} </a></h5> 
						<p>{{ucfirst($sA->region)}},{{ucfirst($sA->country)}} </p> 
					</div>
				</div>
			</div>
			@endforeach
			@else 
			<h3 style="text-align:center;"> {{__('No Spotlight Found')}} </h3>
			@endif
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<section class="home-newsletter">
<div class="container">
<div class="row">
<div class="col-sm-12">
	<div class="single_news_ltr">
		<h2>Subscribe to our Newsletter</h2>
	<div class="input-group">
         <input type="email" class="form-control" placeholder="Enter your email">
         <span class="input-group-btn">
         <button class="btn btn-theme" type="submit">Subscribe</button>
         </span>
          </div>
	</div>
</div>
</div>
</div>
</section>

<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
<script type="text/javascript">
	/*$.ajax({
		url:'/site-visit',
		success:function(r){}
	});*/
	oldSearchVal='';
	//$('#txtSearch').change(function(){
		$("#txtSearch").autocomplete({ 
			source: "/search/home-text-search",
			minLength: 2,
			response: function(event, ui) { 
	            if (ui.content.length === 0) {
	            	var results = $.ui.autocomplete.filter(myarray, request.term);
	                $("#empty-message").text("No results found");
	            } else {
	            	oldSearchVal=$("#txtSearch").val();
	                $("#empty-message").empty();
	            }

			},
			select: function( event, ui ) {
				
				var country='';
				if(ui.item.country_id){
					country=ui.item.country_id;
				}
				if(ui.item.type=='desc'){
					country=oldSearchVal;
				}
				window.location.href='/search?type='+ui.item.type+'&type_id='+ui.item.id+'&term='+country
			}
		}); 
	//});
</script>
@endsection 