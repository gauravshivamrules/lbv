@extends('layouts.front') 
@section('title',__('Privacy policy')) 
@section('content')
<style type="text/css">
	.error{color: red;}
	header {
	    background-image: url(../css/images/header-contact.jpg);
	    background-repeat: no-repeat;
	    background-attachment: scroll;
	    background-position: center center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    background-size: cover;
	    -o-background-size: cover;
	    text-align: center;
	    color: #fff;
	}
</style>
<div class="header-right">
	<header>
		<div class="container">
			<div class="intro-text" style="float:left;">
               <a href="/last-minutes" class="page-scroll btn btn-xl"><img src="/images/last-minutes.svg"></a>
           </div>
       </div>
   </header> 
</div>
	 		<div class="row"> 
			<div class="col-md-2"></div>
			<div class="col-md-8"> 
				<legend><?php echo __('Privacy Policy'); ?></legend>
					<div class="col-lg-10" >
						<p style="text-aligh:justify;">{{strip_tags($privacy->content)}}</p> 					
					</div> 
			</div>
			<div class="col-md-2"></div>
		</div> 
@endsection


