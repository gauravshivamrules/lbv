@extends('layouts.front') 
@section(__('Contact Us'),__(env('SITE_TITLE_FRONT'))) 
@section('content')
<style type="text/css">
	.error{color: red;}
	header {
	    background-image: url(../css/images/header-contact.jpg);
	    background-repeat: no-repeat;
	    background-attachment: scroll;
	    background-position: center center;
	    -webkit-background-size: cover;
	    -moz-background-size: cover;
	    background-size: cover;
	    -o-background-size: cover;
	    text-align: center;
	    color: #fff;
	}
</style>
<div class="header-right">
	<header>
		<div class="container">
			<div class="intro-text" style="float:left;">
               <a href="/last-minutes" class="page-scroll btn btn-xl"><img src="/images/last-minutes.svg"></a>
           </div>
       </div>
   </header> 
</div>
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="service-top">
	<div class="ser-top">
	<h4>  </h4>
	</div> 
</div>
<div class="services-grid row">
	<div class="col-md-6 service-top1">
		<div class="service-top">
			<h3> {{__('Contact Us')}} </h3>
		</div>
		<div class="clearfix"> </div>
		<form class="form-horizontal" action="/contact-us" method="post" novalidate="true">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>  
				
				<div class="form-group">
					<div class="col-lg-10">
						<input type="text" name="name" class="form-control" title="{{__('Full Name')}}" placeholder="{{__('Name')}}" required value="{{old('name')}}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10">
						<input type="email" name="email" class="form-control" title="{{__('Email')}}" placeholder="{{__('Email')}}" required value="{{old('email')}}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10">
						<textarea  name="message" placeholder="{{__('Message...')}}" maxlength="255"  required style="width:  100%; height: 80px;">
							{{old('message')}}
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10">
						<div class="captcha">
							<span>{!! captcha_img() !!}</span>
							<button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
						</div>
						<input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
						@if ($errors->has('captcha'))
						<span class="help-block">
							<strong>{{ $errors->first('captcha') }}</strong> 
						</span>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary"><?php echo __('Send'); ?></button> 
					</div>
				</div>
			</fieldset>
		</form>		
		</div>
		<div class="col-md-6 col-xs-12 service-top1">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11375540.263931021!2d-6.932850165543392!3d45.886682689283276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54a02933785731%3A0x6bfd3f96c747d9f7!2sFrance!5e0!3m2!1sen!2sin!4v1529013145136" 
				width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="clearfix"> </div>
</div>
<script type="text/javascript">
	$(".btn-refresh").click(function(){
	  $.ajax({
	     url:'/refresh-captcha',
	     success:function(data){
	        $(".captcha span").html(data.captcha);
	     }
	  });
	});
</script>
@endsection