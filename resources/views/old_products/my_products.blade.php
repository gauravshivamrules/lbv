@extends('layouts.back')
@section('title',_('My Old Invoices'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	<style type="text/css">
		img {
			height: 35px;
		}
	</style>

	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('My Old Invoices') }} </h2> 
		</div>


		
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Subscription'); ?></th>
								<th><?php echo __('Valid From'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th> 
								<th><?php echo __('Amount'); ?>(&euro;)</th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Transaction Id'); ?></th>
								<th><?php echo __('Invoice No.'); ?></th>
								<th><?php echo __('Order No.'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
		
		

			
	</div>
		<script>
			$(function() {
				buildTable('activeTable','active');   
			}); 
			function buildTable (table,tCount) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/old-products/get-all-my-invoices",
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [
				        { data: 'accomodation_name', name: 'accomodation_name' },
				        { data: 'subscription_name', name: 'subscription_name' },
				        { data: 'valid_from', name: 'valid_from' },
				        { data: 'valid_upto', name: 'valid_upto' },
				        { data: 'amount', name: 'amount' },
				        { data: 'method', name: 'method' },
				        { data: 'transaction_id', name: 'transaction_id' },
				        { data: 'invoice_number', name: 'invoice_number' },
				        { data: 'order_number', name: 'order_number' },
				        { data: 'action', name: 'action'}     
				    ]
				}); 
			} 
			 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href="/extra-tabs/delete/"+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection