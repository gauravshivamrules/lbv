@extends('layouts.back')
@section('title',_('View All Messages'))
@section('content')
	<style type="text/css">
		div.dt-buttons {
		    float: right !important; 
		}
	</style>
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">  
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Messages'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="messageTable">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>     
						<th><?php echo __('Email'); ?></th> 
						<th><?php echo __('Accomodation'); ?></th>
						<th><?php echo __('Sent On'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table> 
			</div>
	</div>   
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
		<script>
			$(function() {
			    $('#messageTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/tickets/message-ajax",     
			        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
			        columns: [
			            { data: 'first_name', name: 'first_name' },
			            { data: 'email', name: 'email' }, 
			            { data: 'name', name: 'name' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'approved', name: 'approved'}, 
			            { data: 'action', name: 'action'}    
			        ],
			    });
			});
			function confirmDelete(id) {
			  
			  if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
			    window.location.href='/tickets/delete/'+id;
			  } else { 
			    return false;
			  } 
			}
			function confirmSwitch(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {  
					window.location.href='/users/switch/'+id;
				} else {  
					return false;
				} 
			}
		</script>
@endsection