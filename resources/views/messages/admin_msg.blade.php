@foreach($returnArr as $a)
<br>
<p>
  <div class="item">
   <p class="message">
    @if(Auth::user()->id==$a['from_user'])
     <a href="javascript:;" class="name">
       <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>{{date('d-m-Y H:i:s',strtotime($a['created_at']))}}</small>
       You
     </a> 
     @else
     <a href="javascript:;" class="name">
       <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date('d-m-Y H:i:s',strtotime($a['created_at']))}}</small>
       {{$a['sender_name']}}
     </a>
     @endif
     {{$a['message']}}<br>
     @if(!$a['approved'])
      <a id="approve" onclick="confirmApprove({{$a['id']}},'{{$a['type']}}');" href="javascript:;" class="btn btn-xs btn-success">{{__('Approve')}} </a> &nbsp; 
      <a id="edit"  onclick="confirmEdit({{$a['id']}},'{{$a['type']}}');" href="javascript:;" class="btn btn-xs btn-primary">{{__('Edit')}} </a> 
     @endif 

   </p>
 </div>
</p>
@endforeach
