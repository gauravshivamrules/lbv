@extends('layouts.back')
@section('title',_('View All Message'))
@section('content')
<style type="text/css">
    form, p, span {
        margin:0;
        padding:0; }
      
    input { font:12px arial; }
      
    a {
        color:#0000FF;
        text-decoration:none; }
      
        a:hover { text-decoration:underline; }
      
    #wrapper, #loginform {
        margin:0 auto;
        padding-bottom:25px;
        background:#EBF4FB;
        width:504px;
        border:1px solid #ACD8F0; }
      
    #loginform { padding-top:18px; }
      
        #loginform p { margin: 5px; }
      
    #chatbox {
        text-align:left;
        margin:0 auto;
        margin-bottom:25px;
        padding:10px;
        background:#fff;
        height:270px;
        width:430px;
        border:1px solid #ACD8F0;
        overflow:auto; }
      
    #usermsg {
        /*width:395px;*/
        border:1px solid #ACD8F0; }
      
    #submit { width: 60px; }
       
    .error { color: #ff0000; }
      
    #menu { padding:12.5px 25px 12.5px 25px; }
      
    .welcome { float:left; }
      
    .logout { float:right; }
      
    .msgln { margin:0 0 2px 0; }
</style>
<div class="row">
   <div class="col-md-10">
       <!-- <div id="chatbox"></div> -->


     <div class="box box-success">
                 <div class="box-header ui-sortable-handle" style="cursor: move;">
                   <i class="fa fa-comments-o"></i>
                   <h3 class="box-title">Chat</h3>
                 </div>
                 <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
                  <div class="box-body chat" id="chatbox" style="width: auto; height: 250px;"> 
                 </div>
                 <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 184.911px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                 <!-- /.chat -->
                 <div class="box-footer">
                    <form name="message" action="#" class="form-horizontal">
                   <div class="input-group">
                     <input name="msg" class="form-control" type="text" id="usermsg"  placeholder="{{__('Type your message here.....')}}" />

                     <div class="input-group-btn">
                       <button type="button" name="submitmsg" class="btn btn-success" id="submitmsg"><i class="fa fa-plus"></i></button>
                     </div>
                   </div>
                    </form>  
                 </div>

               </div>
    </div> 
 </div>
 <script type="text/javascript">
    refreshChats();
    setInterval(function(){ refreshChats() },5000);
    function refreshChats() { 
       $.ajax({
          url:"/tickets/refresh-chats-adv?id={{$id}}", 
          dataType: 'html',
          success:function(r){
             $('#chatbox').html(r);
          }
       });
     }
     $("#submitmsg").click(function(){
        if(!$("#usermsg").val()){
           alert('Please enter message');
           return false;
        }   
        $.ajax({
           type:'post',
           url:'/tickets/contact-advertiser',  
           data:{text:$("#usermsg").val(),id:"{{$id}}"}, 
           success:function(r){
              refreshChats();
           }
        });
        $("#usermsg").val('');
        return false;
     });
     $(document).keypress(function(e) {
         if(e.which == 13) {
          e.preventDefault();
          if(!$("#usermsg").val()){
             alert('Please enter message');
             return false;
          } 
           $.ajax({
              type:'post',
              url:"/tickets/contact-advertiser",  
              data:{text:$("#usermsg").val(),id:"{{$id}}"}, 
              success:function(r){
                 refreshChats();
              }
           });
           $("#usermsg").val('');
         }
     });
 </script>
@endsection