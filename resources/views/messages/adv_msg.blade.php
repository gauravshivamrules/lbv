@foreach($returnArr as $a)
<br>
<p>
	 <div class="item">
       <p class="message">
         @if(Auth::user()->id==$a['from_user'])
            	<a href="javascript:;" class="name">
            	  <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>{{date('d-m-Y H:i:s',strtotime($a['created_at']))}}</small>
            	  You
            	</a>
            @else
            	<a href="javascript:;" class="name">
            	  <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date('d-m-Y H:i:s',strtotime($a['created_at']))}}</small>
            	  {{$a['sender_name']}}
            	</a>
            @endif
         {{$a['message']}} <br>
         @if(!$a['approved']) 
         	<span class="label label-danger"> {{__('wating approval')}} </span>
         @endif
       </p>
     </div>
</p>
@endforeach