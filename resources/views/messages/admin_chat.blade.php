@extends('layouts.back')
@section('title',_('View All Message'))
@section('content')
<style type="text/css">
    form, p, span {
        margin:0;
        padding:0; }
      
    input { font:12px arial; }
      
    a {
        color:#0000FF;
        text-decoration:none; }
      
        a:hover { text-decoration:underline; }
      
    #wrapper, #loginform {
        margin:0 auto;
        padding-bottom:25px;
        background:#EBF4FB;
        width:504px;
        border:1px solid #ACD8F0; }
      
    #loginform { padding-top:18px; }
      
        #loginform p { margin: 5px; }
      
   /* #chatbox {
        text-align:left;
        margin:0 auto;
        margin-bottom:25px;
        padding:10px;
        background:#fff;
        height:270px;
        width:430px;
        border:1px solid #ACD8F0;
        overflow:auto; }*/
      
    #usermsg {
        /*width:395px;*/
        border:1px solid #ACD8F0; }
      
    #submit { width: 60px; }
      
    .error { color: #ff0000; }
      
    #menu { padding:12.5px 25px 12.5px 25px; }
      
    .welcome { float:left; }
      
    .logout { float:right; }
      
    .msgln { margin:0 0 2px 0; }
</style>
<div class="row">
 
   <div class="col-md-10">
       <!-- <div id="chatbox"></div> -->  
       <div class="box box-success">
                   <div class="box-header ui-sortable-handle" style="cursor: move;">
                     <i class="fa fa-comments-o"></i>
                     <h3 class="box-title">Chat</h3>
                   </div>
                   <div class="slimScrollDiv" style="position: relative; overflow: scroll; width: auto; height: 250px;">
                    <div class="box-body chat" id="chatbox" style="width: auto; height: 250px;"> </div>
                   <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 184.911px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                   <!-- /.chat -->
                   <div class="box-footer">
                      <form name="message" action="#" class="form-horizontal">
                     <div class="input-group">
                       <input name="msg" class="form-control" type="text" id="usermsg" placeholder="{{__('Type your message here.....')}}"  />

                       <div class="input-group-btn">
                         <button type="button" name="submitmsg" class="btn btn-success" id="submitmsg"><i class="fa fa-plus"></i></button>
                       </div>
                     </div>
                      </form>  
                   </div>

                 </div>
    </div>
 </div>
 <!-- Modal -->
 <div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">{{__('Edit Message')}} </h4>
       </div>
       <div class="modal-body">
         <textarea id="msgText" rows="6" cols="50">  </textarea> 
       </div>
       <div class="modal-footer">
        
       </div> 
     </div>

   </div>
 </div>
 <script type="text/javascript">
    refreshChats();
    setInterval(function(){ refreshChats() },5000);
    function refreshChats() {
       $.ajax({
          url:"/tickets/refresh-chats-admin?id={{$id}}",    
          dataType: 'html', 
          success:function(r){
             $('#chatbox').html(r);
          }
       });
     }
     $("#submitmsg").click(function(){
        if(!$("#usermsg").val()){
           alert('Please enter message');
           return false;
        }  
        $.ajax({
           type:'post',
           url:'/tickets/contact-admin',  
           data:{text:$("#usermsg").val(),id:"{{$id}}"},
           success:function(r){
              refreshChats();
           }
        });
        $("#usermsg").val(''); 
        return false;  
       
     });
     $(document).keypress(function(e) {
         if(e.which == 13) {
          e.preventDefault();
          if(!$("#usermsg").val()){
             alert('Please enter message');
             return false;
          } 
          $.ajax({
             type:'post',
             url:'/tickets/contact-admin',  
             data:{text:$("#usermsg").val(),id:"{{$id}}"},
             success:function(r){
                refreshChats();
             }
          });
          $("#usermsg").val('');
          return false;  
         } 
     });
     function confirmEdit(id,type) { 
       if(id){
         $('.modal-footer').html('');
         $.ajax({
             url:'/tickets/edit?id='+id+'&type='+type,
             dataType:'json',
             success:function(r){
               if(type=='reply'){
                 $('#msgText').val(r.msg.reply);
                 $('.modal-footer').append("<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>");   
                 $('.modal-footer').append("<button type='button' onclick='updateMessage("+id+",1);' class='btn btn-primary' id='editBTN'>{{__('Edit')}}</button>")
               }else{
                 $('#msgText').val(r.msg.message);  
                 $('.modal-footer').append("<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>");    
                 $('.modal-footer').append("<button type='button' onclick='updateMessage("+id+",2);' class='btn btn-primary' id='editBTN'>{{__('Edit')}}</button>")
               }
               $('#myModal').modal('show'); 
             }
         });
       }
     }
     function updateMessage(id,type) { 
       $.ajax({ 
           type:'post',
           url:'/tickets/update-message',
           data:{text:$('#msgText').val(),id:id,type:type},
           success:function(r){
             if(r=='DONE'){
                $('#myModal').modal('hide');
                refreshChats(); 
             }else{
                alert('Sorry cannot update');
             }
           }
       });
     }
     function confirmApprove(id,type) {
       if(window.confirm("<?php echo __('Are you sure?') ?>")) {    
        if(type=='reply'){
          window.location.href='/tickets/approve-reply/'+id;
        }else{
          window.location.href='/tickets/approve/'+id;
        }
      } else { 
       return false;
     } 
   }
   function confirmDelete(id) {
    if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
      window.location.href='/tickets/delete/'+id;
    } else { 
      return false; 
    } 
  }

     
 </script>
@endsection