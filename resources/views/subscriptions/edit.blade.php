@extends('layouts.back') 
@section('title',__('Update Subscription Type')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/subscriptions/edit/{{$subscription->id}}" method="post">  
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Update Subscription Type'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="SubscriptionName" value="{{$subscription->name}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								<textarea class="form-control" name="description">{{$subscription->description}}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Type'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',['3'=>__('For Rent'),'2'=>__('For Sale')],$subscription->type_id,['id'=>'lableId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Duration'); ?> </label>
							<div class="col-lg-8">
								<input name="duration" class="form-control"  type="number" min="1" id="SubscriptionDuration" value="{{$subscription->duration}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Duration Type'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('duration_type',['Hours'=>__('Hours'),'Days'=>__('Days'),'Week'=>__('Week'),'Month'=>__('Month'),'Year'=>__('Year')],$subscription->duration_type,['id'=>'duration_type','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Amount'); ?> </label> 
							<div class="col-lg-8">
								<input name="amount" class="form-control"  type="number" min="0"  id="SubscriptionAmount" value="{{$subscription->amount}}" required> 
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Update'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
@endsection