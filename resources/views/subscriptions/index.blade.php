@extends('layouts.back')
@section('title',_('All Subscriptions Types'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('All Subscriptions Types'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="subscriptionsTable">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>     
						<th><?php echo __('Type'); ?></th>
						<th><?php echo __('Duration'); ?></th>
						<th><?php echo __('Duration Type'); ?></th>
						<th><?php echo __('Amount'); ?>(&euro;)</th>
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead> 
			</table>
			</div>
	</div>
		<script>
			$(function() {
			    $('#subscriptionsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true, 
			        ajax: "/subscriptions/get-subscriptions",    
			        columns: [
			            { data: 'name', name: 'name' },
			            { data: 'type_id', name: 'type_id' }, 
			            { data: 'duration', name: 'duration' }, 
			            { data: 'duration_type', name: 'duration_type' }, 
			            { data: 'amount', name: 'amount'},    
			            { data: 'action', name: 'action'}    
			        ]
			    }); 
			});
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/subscriptions/delete/'+id; 
				} else { 
					return false;
				} 
			} 
		</script>
@endsection