@extends('layouts.back') 
@section('title',__('Switch Subscription')) 
@section('content')

@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
	<div id="cancelPaypalPayment" class="callout callout-danger" style="display:none;"></div>
	<div class="callout callout-success">
		<h4>{{__('Switch to paid')}}</h4>
	</div>
		<div class="row"> 
			<div class="col-md-1"></div>    
			<div class="col-md-10">
				<form method="post" action="/subscriptions/switch-to-paid" onsubmit="return confirm('Are you sure?');">
					{{csrf_field()}}
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2"> 
							<label class="control-label"><font style="vertical-align: inherit;">
							<font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font></font></label>
								{!! Form::select('accom_id',$accoms,null,['id'=>'accom_id','class'=>'form-control','required'=>true] ) !!} 
						</div>
					</div>
					<br>
					<div class="form-action"> 
						<div class="col-lg-10 col-lg-offset-2"> 
							<button type="submit" class="btn btn-default">{{__('Submit Request')}} </button>	
						</div>
					</div>
				</form>

			</div>
			<div class="col-md-1"></div> 
		</div>
		<br>  
		
</div> 
<script type="text/javascript">
		
		
</script>
@endsection