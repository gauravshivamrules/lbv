@extends('layouts.back') 
@section('title',__('Add New Subscription Type')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/subscriptions/add" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Add New Subscription Type'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Name'); ?> </label>
							<div class="col-lg-8">
								<input name="name" class="form-control"  type="text" id="SubscriptionName" value="{{old('name')}}"   required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Description'); ?> </label>
							<div class="col-lg-8">
								<textarea class="form-control" name="description"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-lg-4 control-label"><?php echo __('Type'); ?></label>
							<div class="col-lg-8">
								{!! Form::select('type_id',['3'=>__('For Rent'),'2'=>__('For Sale')],null,['id'=>'lableId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Duration'); ?> </label>
							<div class="col-lg-8">
								<input name="duration" class="form-control"  type="number" min="1" id="SubscriptionDuration" value="{{old('duration')}}" required>                          
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Duration Type'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('duration_type',['Hours'=>__('Hours'),'Days'=>__('Days'),'Week'=>__('Week'),'Month'=>__('Month'),'Year'=>__('Year')],null,['id'=>'duration_type','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Amount'); ?> </label> 
							<div class="col-lg-8">
								<input name="amount" class="form-control"  type="number" min="0"  id="SubscriptionAmount" value="{{old('amount')}}" required>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Add'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
@endsection