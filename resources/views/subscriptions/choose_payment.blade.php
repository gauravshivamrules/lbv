@extends('layouts.back') 
@section('title',__('Select Payment')) 
@section('content')

@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
	<div id="cancelPaypalPayment" class="callout callout-danger" style="display:none;"></div>
	<div class="callout callout-success">
		<h4>{{__('Subscription Details')}}</h4>
	</div>
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<span> {{__('Subscription')}} : {{$subscription->name}}</span><br>
				<span> {{__('Subscription Type')}} : {{ $subscription->type_id }}</span><br>
				<span> {{__('Description')}} : {{$subscription->description}}</span><br>
				<span> {{__('Amount')}} : &euro;{{$subscription->amount}}</span><br>
				<span>{{__('Duration')}}: {{$subscription->duration.' '.$subscription->duration_type}}</span><br>
				<span> {{__('Start From')}} : {{date('d-m-Y')}}</span><br>
				<span> {{__('Valid Upto')}} : {{date('d-m-Y',strtotime('+ '.$subscription->duration.$subscription->duration_type))}}</span><br>

			</div>
			<div class="col-md-1"></div> 
		</div>
		<br>  
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<fieldset>
					<legend><?php echo __('Select Payment'); ?></legend> 
						<div class="col-xs-4"> 
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2"> 
									<!-- <a href="javascript:;"> 
										<input type="image" alt="PayPal" name="submit" src="https://www.paypalobjects.com/en_US/i/btn/x-click-but01.gif">
									</a> -->
									<form method="post" action="/subscription-licenses/pay-pal/{{$accom}}" id="subscriptionPaypalForm"> 
										{{csrf_field()}} 
										<input type="hidden" name="cart_id" id="cartId">
										<input type="hidden" name="amount_paid" id="amountPaid">
										<input type="hidden" name="txn_id" id="txnId">
										<input type="hidden" name="payer_email" id="payerEmail">
										<input type="hidden" name="payer_first_name" id="payerFirstName">
										<input type="hidden" name="payer_last_name" id="payerLastName">
										<input type="hidden" name="subscription_id" value="{{$subscription->id}}">
										<input type="hidden" name="valid_upto" value="{{date('d-m-Y',strtotime('+ '.$subscription->duration.$subscription->duration_type))}}">
									</form>
									<div id="paypal-button"> </div>
								</div>
							</div>	
						</div> 
						<div class="col-xs-4">
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<!-- <a href="javascript:;"> 
										<a href="/subscription-licenses/mollie/{{$accom}}" class="btn btn-success btn-sm">{{__('Mollie')}}</a> 
									</a> -->

									<form method="post" action="/subscription-licenses/mollie/{{$accom}}" onsubmit="return confirm('Are you sure?');">
										{{csrf_field()}}
									<div class="form-group">
										<div class="col-lg-10 col-lg-offset-2"> 
											<input type="hidden" name="accomodation_id" value="{{$accom}}"> 
											<input type="hidden" name="subscription_id" value="{{$subscription->id}}">
											<input type="hidden" name="valid_upto" value="{{date('d-m-Y',strtotime('+ '.$subscription->duration.$subscription->duration_type))}}">
											<button type="submit" class="btn"> 
													{{__('Pay Now')}}
											</button>	
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-xs-4"> 
							<form method="post" action="/subscriptions/bank-transfer" onsubmit="return confirm('Are you sure?');">
								{{csrf_field()}}
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2"> 
									<input type="hidden" name="amount" value="{{$subscription->amount}}">  
									<input type="hidden" name="accomodation_id" value="{{$accom}}"> 
									<input type="hidden" name="subscription_id" value="{{$subscription->id}}">
									<input type="hidden" name="valid_upto" value="{{date('d-m-Y',strtotime('+ '.$subscription->duration.$subscription->duration_type))}}">
									<!-- <button type="submit"   class="btn btn-primary btn-sm"> 
										<img src="/images/wire-transfer.png" style="width: 80px;">
									</button>	 -->
								</div>
							</div>
							</form>
						</div>
				</fieldset> 
			</div>
			<div class="col-md-1"></div>
		</div>
</div> 
@endsection