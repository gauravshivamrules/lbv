@extends('layouts.back')
@section('title',_('All Extra Tabs'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Extra Tabs') }} </h2> 
		</div>


		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#showTips"><span class="label label-success" id="active"></span> {{__('Approved')}} </a>
			</li>
			<li>
				<a data-toggle="tab" href="#showSpots"><span class="label label-warning" id="pending"></span> {{__('Pending')}}</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showLastMinutes"><span class="label label-danger" id="rejected"></span> {{_('Rejected')}} </a>
			</li>
		</ul> 

		<div class="tab-content"> 
			<div id="showTips" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Title'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showSpots" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="pendingTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Title'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			<div id="showLastMinutes" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="rejectedTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Title'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
		

			
	</div>
		<script>
			$(function() {
				buildTable('activeTable','active',1);  
				buildTable('pendingTable','pending',0);  
				buildTable('rejectedTable','rejected',2);    
			}); 
			function buildTable (table,tCount,type) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/extra-tabs/get-advtabs?type_id="+type,
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [
				        { data: 'name', name: 'name' },
				        { data: 'page_name', name: 'page_name' },
				        { data: 'action', name: 'action'}    
				    ]
				}); 
			} 
			 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href="/extra-tabs/delete/"+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection