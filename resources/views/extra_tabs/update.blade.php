@extends('layouts.back')
@section('title',_('All Update Tab Requests'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Update Extra Tab Requests') }} </h2>  
		</div>
		<div class="tab-content"> 
			<div id="showTips" class="tab-pane active"> 
				<div class="table-responsive">   
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Title'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div> 
	</div> 
		<script>
			$(function() {
				buildTable('activeTable','active',1);  
			}); 
			function buildTable (table,tCount,type) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/extra-tabs/get-all-tabs-update",
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal); 
				    },      
				    columns: [
				        { data: 'name', name: 'name' },
				        { data: 'page_name', name: 'page_name' },
				        { data: 'action', name: 'action'}    
				    ]
				}); 
			}   
			function confirmAction(url) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href=url;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection