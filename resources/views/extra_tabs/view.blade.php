@extends('layouts.back') 
@section('title',__('View Extra Tab')) 
@section('content')
<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
</ul>
	<div class="well well bs-component"> 

		<div class="row"> 
			<div class="col-md-1"> </div>
			<div class="col-md-10"> 
					<fieldset> 
						<legend><?php echo __('View Extra Tab'); ?></legend> 
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group"> 
						      		  <label class="control-label">
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
						      		    </font>
						      		  </label>
						      		 {{$accom}}
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div> 
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
						      		    </font>
						      		  </label>
						      		    {{$tab->page_name}}
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div>
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Content'); ?></font>
						      		    </font>
						      		  </label>
						      		   
						      		    	{{strip_tags($tab->tab_content)}}
						      		   
						      		    <div id="total-caracteres"> </div>
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div>
						      <div class="modal-footer">
						      	@if(Auth::user()->role_id==1) 
						      		@if($tab->status !=1)
						      		  <a onclick="confirmAction('/extra-tabs/approve/{{$tab->id}}')"  class="btn btn-info btn-effect pull-left" id="approveBtn"><?php echo __('Approve'); ?></a>             
						      		@endif

						      		@if($tab->status!=2) 
						      		  <a onclick="confirmAction('/extra-tabs/reject/{{$tab->id}}')" class="btn btn-danger btn-effect pull-left" id="rejectBtn"><?php echo __('Reject'); ?></a>             
						      		@endif
						      	@endif
						      	
						      </div>
					</fieldset>
			</div>
			<div class="col-md-1"> </div>
		</div>
	</div>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
	<script>

		$(function() {

			$('.summernoteA').summernote({
				 height: 250, 
			}); 
			
			
		    $(".note-editable").on("keypress", function(){
		        var limiteCaracteres = 1000;
		        var caracteres = $(this).text();
		        var totalCaracteres = caracteres.length;

		        //Update value
		        $("#total-caracteres").text(totalCaracteres);

		        //Check and Limit Charaters
		        if(totalCaracteres >= limiteCaracteres){
		            return false;
		        }
		    });
		});


		function confirmAction(url) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href=url;
				} else { 
					return false; 
				} 
			} 
		
	</script>
@endsection