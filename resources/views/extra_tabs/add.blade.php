@extends('layouts.back') 
@section('title',__('Add Extra Tab')) 
@section('content')
<ul>    
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
</ul>
	<div class="well well bs-component"> 

		<div class="row"> 
			<div class="col-md-3"></div>
			<div class="col-md-6">
					<fieldset>
						<legend><?php echo __('Add Extra Tab'); ?></legend> 
						 <form action="/extra-tabs/add" method="post" accept-charset="utf-8"  enctype="multipart/form-data">    
						      {{csrf_field()}}  
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group"> 
						      		  <label class="control-label">
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
						      		    </font>
						      		  </label>
						      		  {!! Form::select('accom_id',$accoms,null,['id'=>'accomId','class'=>'form-control','required'=>true] ) !!} 
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div> 
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
						      		    </font>
						      		  </label>
						      		    <input type="text"  name="title" class="form-control" required>
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div>
						      <div class="row"> 
						      	<div class="col-md-1"> </div>
						      	<div class="col-md-10"> 
						      		<div class="form-group">
						      		  <label class="control-label"> 
						      		    <font style="vertical-align: inherit;">
						      		      <font style="vertical-align: inherit;"><?php echo __('Content'); ?></font>
						      		    </font>
						      		  </label>
						      		    <textarea class="summernoteA" name="content" id="content" required>
						      		    </textarea>
						      		    <div id="total-caracteres"> </div>
						      		</div>		
						      	</div>
						      	<div class="col-md-1"> </div>
						      </div>

						  <div class="modal-footer">
						    <button type="submit"  style="float:right;" class="btn btn-info btn-effect pull-left" id="addBtn"><?php echo __('Add'); ?></button>             
						  </div>
						</form>
					</fieldset>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
	<script>
		$(function() {
			$('.summernoteA').summernote({
				 height: 250, 
			});
			$('.summernoteE').summernote({
				 height: 250, 
			});
		    $(".note-editable").on("keypress", function(){
		        var limiteCaracteres = 1000;
		        var caracteres = $(this).text();
		        var totalCaracteres = caracteres.length;

		        //Update value
		        $("#total-caracteres").text(totalCaracteres);

		        //Check and Limit Charaters
		        if(totalCaracteres >= limiteCaracteres){
		            return false;
		        }
		    });
		});
		
	</script>
@endsection