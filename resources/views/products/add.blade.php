@extends('layouts.back') 
@section('title',__('Add Promotion'))
@section('content')
	<ul>   
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
	</ul>
	<div class="well well bs-component">
		<form  action="{{ route('postProductAdd') }}" method="post" accept-charset="utf-8">
			{{csrf_field()}}
			<div class="row"> 
				<div class="col-md-2"> </div>
				<div class="col-md-8"> 
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Promotion Name'); ?></font>
							</font>
						</label>
						<input name="product_name" class="form-control valid"  type="text"  id="PromotionName" value="{{old('product_name')}}"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Promotion Description'); ?></font>
							</font>
						</label>
						<textarea class="form-control" name="product_description" required></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Amount'); ?>(&euro;)</font>
							</font>
						</label>
						<input name="product_price" min="1" class="form-control valid"  type="number"  id="ProductAmount" value="{{old('product_price')}}"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Promotion Type'); ?></font>
							</font>
						</label>
						{!! Form::select('product_category_id',$p,null,['class'=>'form-control'] ) !!} 
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Promotion Month'); ?></font>
							</font>
						</label>
						<?php 
						$months=array(1=>__('January'),2=>__('February'),3=>__('March'),4=>__('April'),5=>__('May'),6=>__('June'),7=>__('July'),8=>__('August'),9=>__('September'),10=>__('October'),11=>__('November'),12=>__('December')); 
						?>
						{!! Form::select('product_month',$months,null,['class'=>'form-control'] ) !!} 
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Promotion Year'); ?></font>
							</font>
						</label>
						<?php 
						$year=array();
						for($i=date('Y');$i<=2050;$i++) {
							$year[$i]=$i;
						}
						?>
						{!! Form::select('product_year',$year,null,['class'=>'form-control'] ) !!} 
					</div>
				</div>
				<div class="col-md-2"> </div>
			</div> 

			<div class="row"> 
				<div class="col-md-4"> </div>
				<div class="col-md-4">
					<div class="margin-top-10">
						<button type="submit" class="btn btn-info btn-effect pull-left">
							<?php echo __('Add') ?>
						</button>
					</div>  
				</div>
				<div class="col-md-4"></div>
			</div>
		</form>   
	</div>
@endsection 