@extends('layouts.back') 
@section('title',__('Update News'))
@section('content') 
	<ul>   
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
	</ul>
	<div class="well well bs-component">
		<legend><?php echo __('Update News'); ?></legend>  
		<form  action="/promotion/edit-news/{{$licenseId}}" method="post" accept-charset="utf-8">
			{{csrf_field()}}
			<div class="row"> 
				<div class="col-md-2"> </div>
				<div class="col-md-8"> 
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
							</font>
						</label>

						<input name="accom_name" class="form-control valid" value="{{$accom}}"  readonly="true" type="text"  id="accom_id"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
							</font>
						</label>
						<input name="news_title" class="form-control valid"  type="text"  id="news_title"  value="{{$news->title}}" required>                        
					</div>
					
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
							</font>
						</label>
						<textarea  name="news_description" class="summernote" required>
								{{$news->description}}
						</textarea>
						{{__('Max Characters ')}}:500
						<div id="total-caracteres"> </div>
					</div> 
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
							</font>
						</label>
						<input name="start_from" class="form-control valid"  readonly="true"  type="text"  id="start_from" value="{{date('d-m-Y',strtotime($news->start_from))}}"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
							</font>
						</label>
						<input name="valid_upto"  readonly="true" class="form-control valid"  type="text"  id="valid_upto"  value="{{date('d-m-Y',strtotime($news->valid_upto))}}" required>    
					</div>
				</div>
				<div class="col-md-2"> </div>
			</div> 

			<div class="row"> 
				<div class="col-md-2"> </div>
				<div class="col-md-10">
					<div class="margin-top-10">
						<button type="submit" class="btn btn-info btn-effect pull-left">
							<?php echo __('Update') ?>
						</button>
					</div>  
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>   
	</div>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> 

	<script type="text/javascript">
		$(document).ready(function() {
				$('.summernote').summernote({
					 height: 250, 
				});
			    $(".note-editable").on("keypress", function(){
			        var limiteCaracteres = 500;
			        var caracteres = $(this).text();
			        var totalCaracteres = caracteres.length;
			        //Update value
			        $("#total-caracteres").text(totalCaracteres);
			        //Check and Limit Charaters
			        if(totalCaracteres >= limiteCaracteres){
			            return false;
			        }
			    });
		});
	</script>
@endsection 