@extends('layouts.back') 
@section('title',__('Add LastMinute'))
@section('content')
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
<style type="text/css">
 	.ui-datepicker-title {
    	color: black;
	} 
  .fc-time{
     display : none;
  }
  .tooltip-inner {
      white-space:pre-wrap;
  }
</style> 
	<ul>   
	@foreach($errors->all() as $e)
		<li class="error"> {{ $e }} </li>
	@endforeach 
	</ul>
	<div class="well well bs-component">
		<legend><?php echo __('Add LastMinute'); ?></legend>  
		<form  action="/promotion/add-lastminute/{{$licenseId}}" method="post" accept-charset="utf-8">
			{{csrf_field()}}
			<div class="row"> 
				<div class="col-md-2"> </div>
				<div class="col-md-8"> 
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Accomodation'); ?></font>
							</font>
						</label>

						<input name="accom_name" class="form-control valid" value="{{$accom}}"  readonly="true" type="text"  id="accom_id"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Title'); ?></font>
							</font>
						</label>
						<input name="news_title" class="form-control valid"  type="text"  id="news_title"  required>                        
					</div>
					
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Description'); ?></font>
							</font>
						</label>
						<textarea  name="news_description" class="summernote" required></textarea>
						{{__('Max Characters ')}}:500
						<div id="total-caracteres"> </div>
					</div> 
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('Start'); ?></font>
							</font>
						</label>
						<input name="start_from" class="form-control valid"  type="text"  id="start_from"  required>                        
					</div>
					<div class="form-group">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"><?php echo __('End'); ?></font>
							</font>
						</label>
						<input name="valid_upto"  readonly="true" class="form-control valid"  type="text"  id="valid_upto"  required>    
					</div>
				</div>
				<div class="col-md-2"> </div>
			</div> 

			<div class="row"> 
				<div class="col-md-2"> </div>
				<div class="col-md-10">
					<div class="margin-top-10">
						<button type="submit" class="btn btn-info btn-effect pull-left">
							<?php echo __('Add') ?>
						</button>
					</div>  
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>   
	</div>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> 

	<script type="text/javascript">
		$(document).ready(function() {
				$('.summernote').summernote({
					 height: 250, 
				});
			    $(".note-editable").on("keypress", function(){
			        var limiteCaracteres = 500;
			        var caracteres = $(this).text();
			        var totalCaracteres = caracteres.length;
			        //Update value
			        $("#total-caracteres").text(totalCaracteres);
			        //Check and Limit Charaters
			        if(totalCaracteres >= limiteCaracteres){
			            return false;
			        }
			    });
		});
		$('#start_from').datepicker({
			changeMonth:true,
			changeYear:true,
			dateFormat:"dd-mm-yy",
			minDate:0,
			onSelect:function(date1) {
		    	var startDate=$("#start_from").datepicker("getDate");
		    	// var endDate=add_months(startDate,1);
		    	var endDate=addDays(startDate,28);
		    	console.log(endDate);
		    	//endDate=convert(endDate);
		    	$("#valid_upto").val(endDate);   
			}
		}); 
		function addDays(date,days) {
			var newdate = new Date(date);
			newdate.setDate(newdate.getDate() + days);
			var dd = newdate.getDate();
			var mm = newdate.getMonth() + 1;
			var y = newdate.getFullYear();
			if(mm<10) {
				mm='0'+mm;
			}
			return  dd+ '-' +mm+ '-' + y;
		}
		function add_months(dt, n)  {
			return new Date(dt.setMonth(dt.getMonth() + n));      
		}
		function convert(str) {
		    var date = new Date(str),
		        mnth = ("0" + (date.getMonth()+1)).slice(-2),
		        day  = ("0" + date.getDate()).slice(-2);
		        hours  = ("0" + date.getHours()).slice(-2);
		        minutes = ("0" + date.getMinutes()).slice(-2);
		    return [ day,mnth,date.getFullYear(),].join("-");
		}

	</script>
@endsection 