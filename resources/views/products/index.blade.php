@extends('layouts.back')
@section('title',_('View All Promotions'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <?php echo __('Promotions'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="productsTable">
					<thead>
						<tr>
							<th><?php echo __('Name'); ?></th>     
							<th><?php echo __('Description'); ?></th>
							<th><?php echo __('Price'); ?>(&euro;)</th>
							<th><?php echo __('Month'); ?></th>
							<th><?php echo __('Year'); ?></th>
							<th><?php echo __('Category'); ?></th>
							<th><?php echo __('Actions'); ?></th>
						</tr>
					</thead> 
				</table>
			</div>
	</div>
		<script>
			$(function() {
			    $('#productsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/products/get_products",     
			        columns: [
			            { data: 'product_name', name: 'product_name' },
			            { data: 'product_description', name: 'product_description' }, 
			            { data: 'product_price', name: 'product_price' }, 
			            { data: 'product_month', name: 'product_month' }, 
			            { data: 'product_year', name: 'product_year'},    
			            { data: 'product_category_id', name: 'product_category_id'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection