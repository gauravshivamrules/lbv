@extends('layouts.back')
@section('title',_('My Promotions'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> {{__('My Promotions') }} </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="productsTable">
					<thead>
						<tr>
							<th><?php echo __('Name'); ?></th>     
							<th><?php echo __('Price'); ?>(&euro;)</th>
							<th><?php echo __('Valid From'); ?></th>
							<th><?php echo __('Valid Upto'); ?></th>
							<th><?php echo __('Payment Method'); ?></th>
							<th><?php echo __('Payment Status'); ?></th>
							<th><?php echo __('Invoice No.'); ?></th>
							<th><?php echo __('Accomodation'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
				</table>
			</div>
	</div>
		<script>
			$(function() {
			    $('#productsTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true,
			        ajax: "/promotions/get-my-promotions",     
			        columns: [
			            { data: 'product_name', name: 'product_name' },
			            { data: 'total_amount', name: 'total_amount' },
			            { data: 'vFrom', name: 'vFrom' }, 
			            { data: 'vUpto', name: 'vUpto' }, 
			            { data: 'pName', name: 'pName' }, 
			            { data: 'payment_complete', name: 'payment_complete'},    
			            { data: 'invoice', name: 'invoice' }, 
			            { data: 'aName', name: 'aName'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection