<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
   {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') !!} 
   {!! Html::style('css/app.css') !!} 
   {!! Html::style('css/custom.css') !!} 
   {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') !!}
   {!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') !!}
   <style type="text/css">
      body {color: black;}
   </style>
  </head>
  <body> 
   <div class="row"> 
      <span style="text-align:center; margin-left:150px;" > 
        
      <img src="{{asset('images/covertwitter.png')}}" height="150px">
    </span>
      <span style="float:right;"> {{__('Date')}}: {{date('d-m-Y',strtotime($licenses->invoiceDate))}}</span>

      <div class="col-md-1"> </div>
      <div class="col-md-10"> 
        <div class="well well bs-component">
          <div class="content">
            <div style="float:left;"> 
              <h4> {{ __('Client') }} </h4>
              {{__('Name')}}:{{$licenses->first_name}} {{$licenses->last_name}} <br>
              {{__('Client  Number')}}: {{$licenses->user_id}} <br>
              {{__('Company')}}: {{$licenses->user_company}} <br>
              {{$licenses->street_number}} {{$licenses->street}} <br> 
              {{$licenses->post_code}}  {{$licenses->city}} <br> 
              {{$licenses->countryName}} <br>
              {{$licenses->phone_number}} <br>
              {{$licenses->email}} <br>
              {{__('Btw No.')}}:

            </div>
            <div style="float:right;"> 
               <h4> {{__('Payment Details')}}</h4>

               <?php 
               $status=__('Not Paid');
               if($licenses->payment_complete==1) {
                  $status=__('Paid');
               } else if($licenses->payment_complete==2){
                  $status=__('Cancelled');
               } else if($licenses->payment_complete==3){
                  $status=__('Free');
               } 
               ?>
               {{__('Status')}}: {{ $status}}<br> 
               {{__('Method')}}: {{$licenses->pName}} ({{ucfirst($licenses->method)}}) <br> 
               {{__('Invoice Number')}}: {{$licenses->invoice_number}} <br>
               {{__('Order Number')}}: {{$licenses->order_number}} <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-1"> </div>
   </div>
  

   <table style="width:100%;margin-top:10px;">
       <tr>
      <th style="text-align:left;"><?php echo __('Description'); ?></th>
      <th style="text-align:right;"><?php echo __('Price') ?>(&euro;) </th>  
      <th style="text-align:right;"><?php echo __('Qty.') ?></th>  
      <th style="text-align:right;"><?php echo __('Total'); ?>(&euro;)</th> 
       </tr>
       <tr style="background:#F1F4F7;">
      <td><h4>{{$licenses->sName}}</h4> 
        {{$licenses->sDesc}}
      </td>
      <td style="text-align:right;"> {{$licenses->amount_paid}}</td>
      <td style="text-align:right;"> 1</td> 
      <td style="text-align:right;"> {{$licenses->amount_paid}}</td>
       </tr>       
   </table>

   <div class="row" style="width:100%;margin-top:8%;">
    <div class="col-md-1"> </div>
    <div class="col-md-10"> 

        <div style="width:35%;float:left;text-align:left;"> 
          <b>{{ env('SITE_TITLE_FRONT') }}</b><br>
          Joeri De Bonnaire<br>
          Lencouet, 47230 Feugarolles, Frankrijk<br> 
          Siret nr. : 53453667700014<br>
          {{ env('SITE_EMAIL') }}
        </div>
        <div style="width:33%;float:right;background:#F1F4F7;padding:10px;">
          <table style="width:100%;">
            <tr>
              <td style="text-align:left;"><b><?php echo __('SUBTOTAAL'); ?></b></td>
              <td style="text-align:right;">&euro; {{$licenses->amount_paid}}</td>  
            </tr> 
            <tr>
              <td style="text-align:left;"><b><?php echo __('Total'); ?></b></td>
              <td style="text-align:right;">&euro; {{$licenses->amount_paid}}</td>    
            </tr>
            <tr> 
              <td style="text-align:left;" colspan="2"><b><?php echo __('BTW niet van toepassing article 293 B van CGI'); ?></b></td> 
            </tr>  
          </table>
        </div>
    </div>
    <div class="col-md-1"> </div>
   </div>
  
        <?php if($licenses->pay_method_id==2) { ?>     
          <div class="row"> 
            <div class="col-md-12"> 
                <b><h4><?php echo __('OPMERKING'); ?> </h4>   </b>
                <p style="text-align: justify;font-style: 8px;">
                <?php echo __('Als u heeft gekozen voor handmatig betalen. Stort het bovenstaande bedrag op rekeningnummer'); ?> 
                <?php echo __('IBAN'); ?>: <?php echo __('FR76 1254 8029 9840 4804 9150 229'); ?>  <br>
                <?php echo __('BIC: AXABFRPP') ?>  <br> 
                <?php echo __('Met vermelding van uw naam en het factuurnummer. U zal een bevestiging per mail krijgen als de betaling is ontvangen. U dient deze factuur binnen de 14 dagen te betalen.'); ?> 
                </p>
                <p style="text-align: justify;font-style: 8px;">  
                <h4><?php echo __('Algemene voorwaarden') ?> </h4>    
                <?php echo __('Bij niet tijdige betaling wordt een schadevergoeding aangerekend van 12%, met een minimum van &euro;50,00. Tevens zal een conventionele intrest verschuldigd zijn van 1% per maand, vanaf de vervaldag en zonder dat een ingebrekestelling nodig is.') ?> <br>

                <?php echo __('Enkel de afdelingen van de rechtbanken of het vredegerecht bevoegd voor de plaats van de uitbatingszetel van onze zaak zijn bevoegd in geval van een geschil.'); ?>      
                </p>
            </div> 
          </div>
        <?php } ?>
    

  </body>
  </html>
