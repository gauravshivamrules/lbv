@extends('layouts.back') 
@section('title',__('Give Subscription Coupon')) 
@section('content')

@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>

<div class="well well bs-component">   
		<div class="row"> 
			<div class="col-md-1"></div>   
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<form class="form-horizontal" action="/subscription-licenses/give-free-license" method="post"> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<legend><?php echo __('Give Subscription Coupon'); ?></legend> 
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Subscription'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('subscription_id',$subscriptions,null,['id'=>'subscriptionId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-4 control-label"><?php echo __('Accomodation'); ?> </label>
							<div class="col-lg-8">
								{!! Form::select('accom_id',$accoms,null,['placeholder' => 'Select Accomodation','id'=>'accomId','class'=>'form-control','required'=>true] ) !!} 
							</div>
						</div>
						
						<div class="form-group" style="display:none;" id="showUser">
							<label for="name" class="col-lg-4 control-label"><?php echo __('User'); ?> </label>
							<div class="col-lg-8">
								<input type="email" class="form-control" name="email" readonly="true" id="userEmail">
								<input type="hidden"  name="userId" id="userId">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary" id="cont"><?php echo __('Send Coupon'); ?></button> 
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script> 
<script type="text/javascript">
	  
	$("#accomId").chosen({   
	    disable_search_threshold: 10,
	    no_results_text: "Oops, nothing found!", 
	    width: "95%"
	}); 

	$('#accomId').change(function() {
		getUser($(this).val());
	});

	function getUser(accom) {
		$.ajax({
			url:'/subscription-licenses/get-user?accom='+accom,
			dataType:'json',
			success:function(r) {
				console.log(r);
				if(r) {
					$('#userEmail').val(r.email);
					$('#userId').val(r.id);
					$('#showUser').show(); 
				}
			}
		}); 
	}
</script> 
@endsection