@extends('layouts.back')
@section('title',_('All Subscription Licenses'))
@section('content')
<style type="text/css">
div.dt-buttons {
	float: right !important;
}
</style>
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">  
<link rel="stylesheet" type="text/css" href="/css/responsive.dataTables.min.css">  
<link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">  
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<div class="panel panel-default">
	<div class="panel-heading"> 
		<h2> <?php echo __('All Subscription Licenses'); ?> </h2>  
	</div> 
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-toggle="tab" href="#showActive"><span class="label label-success" id="active_tag"></span> {{__('Active')}} </a>
		</li>
		<li>
			<a data-toggle="tab" href="#showInactive"><span class="label label-warning" id="inactive_tag"></span> {{__('Inactive')}}</a>
		</li> 
		<li>
			<a data-toggle="tab" href="#showExpired"><span class="label label-danger" id="expired_tag"></span> {{_('Expired')}} </a>
		</li>
		<li>
			<a data-toggle="tab" href="#showBankTransfer"><span class="label label-info" id="new_tag"></span> {{__('New Transfer')}} </a> 
		</li>
	</ul> 
	<div class="tab-content">
		<div id="showActive" class="tab-pane active"> 
			<div class="table-responsive">
				<div class="clear-fix"  id="contactBtn" >	
					<input type="checkbox"  id="bulkActive"/> 
					<button id="trigerActive" class="btn btn-default" ><?php echo __('Send Email To Selected Customers')?></button>
					<span class="help-block"><?php echo __('Mark to send mail to all customers present on table')?> </span>
				</div>  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="activeLicensesTable">
					<thead>
						<tr>
							<th><?php echo __('Accomodation'); ?></th>     
							<th><?php echo __('User'); ?></th>  
							<th><?php echo __('Amount'); ?>(&euro;)</th> 
							<th><?php echo __('Valid Upto'); ?></th>
							<th><?php echo __('Payment Method'); ?></th>
							<th><?php echo __('Payment Status'); ?></th>
							<th><?php echo __('Subscription'); ?></th>
							<th><?php echo __('Invoice'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody></tbody>
				</table>
			</div>
		</div> 
		<div id="showInactive" class="tab-pane fade"> 
			<div class="table-responsive">
				<div class="clear-fix"  id="contactBtn" >	
					<input type="checkbox"  id="bulkInActive"/> 
					<button id="trigerInActive" class="btn btn-default" ><?php echo __('Send Email To Selected Customers')?>
					</button>
					<span class="help-block"><?php echo __('Mark to send mail to all customers present on table')?> </span>
				</div>  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="inActiveLicensesTable">
					<thead>
						<tr>
							<th><?php echo __('Accomodation'); ?></th> 
							<th><?php echo __('User'); ?></th>      
							<th><?php echo __('Amount'); ?>(&euro;)</th> 
							<th><?php echo __('Valid Upto'); ?></th>
							<th><?php echo __('Payment Method'); ?></th>
							<th><?php echo __('Payment Completed'); ?></th>
							<th><?php echo __('Subscription'); ?></th>
							<th><?php echo __('Invoice'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody></tbody>
				</table>
			</div>
		</div>
		<div id="showExpired" class="tab-pane fade"> 
			<div class="table-responsive"> 
				<div class="clear-fix">	
					<input type="checkbox"  id="bulkExpired"/> 
					<button id="trigerExpired" class="btn btn-default" ><?php echo __('Send Email To Selected Customers')?>
					</button>
					<span class="help-block"><?php echo __('Mark to send mail to all customers present on table')?> </span>
				</div> 
				<table class="table table-striped table-hover dt-responsive display nowrap" id="expiredLicensesTable">
					<thead>
						<tr>
							<th><?php echo __('Accomodation'); ?></th>
							<th><?php echo __('User'); ?></th>       
							<th><?php echo __('Amount'); ?>(&euro;)</th> 
							<th><?php echo __('Valid Upto'); ?></th>
							<th><?php echo __('Payment Method'); ?></th>
							<th><?php echo __('Payment Completed'); ?></th>
							<th><?php echo __('Subscription'); ?></th>
							<th><?php echo __('Invoice'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody></tbody>
				</table>
			</div>
		</div>
		<div id="showBankTransfer" class="tab-pane fade"> 
			<div class="table-responsive">  
				<div class="clear-fix"  >	
					<input type="checkbox"  id="bulkTransfer"/> 
					<button id="trigerTransfer" class="btn btn-default" ><?php echo __('Send Email To Selected Customers')?>
					</button>
					<span class="help-block"><?php echo __('Mark to send mail to all customers present on table')?> </span>
				</div>
				<table class="table table-striped table-hover dt-responsive display nowrap" id="bankTransferLicensesTable">
					<thead>
						<tr>
							<th><?php echo __('Accomodation'); ?></th> 
							<th><?php echo __('User'); ?></th>      
							<th><?php echo __('Amount'); ?>(&euro;)</th> 
							<th><?php echo __('Valid Upto'); ?></th>
							<th><?php echo __('Payment Method'); ?></th>
							<th><?php echo __('Payment Completed'); ?></th>
							<th><?php echo __('Subscription'); ?></th>
							<th><?php echo __('Invoice'); ?></th>
							<th><?php echo __('Actions'); ?></th> 
						</tr>
					</thead> 
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div> 

	<div class="modal fade" id="myModalForError">
		<div class="modal-dialog" role="document">
			<div class="modal-content"> 
				<div class="modal-header">
					<h5 class="modal-title"><?php echo __('Send Email To Selected Customers'); ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div> 
				<form class="form-horizontal" id="reportErrorForm" method="post" action="/subscription-licenses/contact-user">
					<div class="modal-body">   
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<fieldset>
							<div class="form-group">
								<label for="name" class="col-lg-2 control-label"><?php echo __('Subject'); ?> </label>
								<div class="col-lg-10">
									<input type="hidden" name="users" id="toUsers">  
									<input name="subject" required="required" class="form-control" type="text" id="subject" required>    
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-lg-2 control-label"><?php echo __('Message'); ?> </label>
								<div class="col-lg-10">
									<textarea id="message" class="summernote" name="message" id="message" required> </textarea>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="modal-footer">
						<button type="submit" style="float:left;" class="btn btn-primary"><?php echo __('Send'); ?></button> 
						<button type="button" style="float:right;" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close') ?></button>
					</div>
				</form>
			</div>
		</div>
	</div> 

	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> 
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.responsive.js"></script> 
	<script type="text/javascript" src="/DATATABLE_BUTTON/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/jszip.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/pdfmake.min.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/vfs_fonts.js"></script>
	<script type="text/javascript" src="/DATATABLE_BUTTON/buttons.html5.min.js"></script> 
	<script>
	jQuery(document).ready(function($) {  
		$('.summernote').summernote({
	     	 	 height: 250,   //set editable area's height
	     	 	});
	});
		$("#bulkActive").on('click', function() { // bulk checked
			var status = this.checked;
			$(".rowActive").each(function() { 
				$(this).prop("checked", status); 
			});
		});  
		$("#bulkInActive").on('click', function() { // bulk checked
			var status = this.checked;
			$(".rowInActive").each(function() {
				$(this).prop("checked", status); 
			});
		}); 
		$("#bulkExpired").on('click', function() { // bulk checked
			var status = this.checked;
			$(".rowExpired").each(function() {
				$(this).prop("checked", status); 
			});
		}); 
		$("#bulkTransfer").on('click', function() { // bulk checked
			var status = this.checked;
			$(".rowTransfer").each(function() {
				$(this).prop("checked", status); 
			});
		}); 


		$('#trigerActive').on("click", function(event) {
			if ($('.rowActive:checked').length > 0) {  
				var ids = [];
				$('.rowActive').each(function() {
					if ($(this).is(':checked')) {
						if (jQuery.inArray($(this).val(), ids)=='-1') {
							ids.push($(this).val());
						} 
					}
				});
				ids_string = ids.toString(); 
				$("#returnMessage").html('');
				$("#subject").val('');
				$("#message").val('');
				$('#toUsers').val(ids_string); 
				$('#myModalForError').modal(); 

			} else {
				alert("<?php echo __('Select Customers') ?>");
			}
		});
		$('#trigerInActive').on("click", function(event) {
			if ($('.rowInActive:checked').length > 0) {  
				var ids = [];
				$('.rowInActive').each(function() {
					if ($(this).is(':checked')) {
						if (jQuery.inArray($(this).val(), ids)=='-1') {
							ids.push($(this).val());
						} 
					}
				});
				ids_string = ids.toString(); 
				$("#returnMessage").html('');
				$("#subject").val('');
				$("#message").val('');
				$('#toUsers').val(ids_string); 
				$('#myModalForError').modal(); 

			} else {
				alert("<?php echo __('Select Customers') ?>");
			}
		});
		$('#trigerExpired').on("click", function(event) {
			if ($('.rowExpired:checked').length > 0) {  
				var ids = [];
				$('.rowExpired').each(function() {
					if ($(this).is(':checked')) {
						if (jQuery.inArray($(this).val(), ids)=='-1') {
							ids.push($(this).val());
						} 
					}
				});
				ids_string = ids.toString(); 
				$("#returnMessage").html('');
				$("#subject").val('');
				$("#message").val('');
				$('#toUsers').val(ids_string); 
				$('#myModalForError').modal(); 

			} else {
				alert("<?php echo __('Select Customers') ?>");
			}
		});
		$('#trigerTransfer').on("click", function(event) {
			if ($('.rowTransfer:checked').length > 0) {  
				var ids = [];
				$('.rowTransfer').each(function() {
					if ($(this).is(':checked')) {
						if (jQuery.inArray($(this).val(), ids)=='-1') {
							ids.push($(this).val());
						} 
					}
				});
				ids_string = ids.toString(); 
				$("#returnMessage").html('');
				$("#subject").val('');
				$("#message").val('');
				$('#toUsers').val(ids_string); 
				$('#myModalForError').modal(); 
			} else {
				alert("<?php echo __('Select Customers') ?>");
			}
		}); 

		$(function() {  
			$(function(){
				var hash = window.location.hash;
				hash && $('ul.nav a[href="' + hash + '"]').tab('show');
			});
			$('#activeLicensesTable').DataTable({ 
				responsive: true,
				processing: true,
				serverSide: true, 
				bPaginate:true,
				bLengthChange: true,
				stateSave: true,
				ajax: "/subscription-licenses/get-all-licenses?type=1",
				dom: 'Blfrtip',
				lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: { 
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				}
				],  
				initComplete: function(settings, json) {
					$('#active_tag').html(json.recordsTotal);
				},   
				columns: [ 
				{ data: 'aName', name: 'aName' },
				{ data: 'first_name', name: 'first_name' },  
				{ data: 'amount_paid', name: 'amount_paid' }, 
				{ data: 'valid_upto', name: 'valid_upto' }, 
				{ data: 'pName', name: 'pName' }, 
				{ data: 'payment_complete', name: 'payment_complete' }, 
				{ data: 'sName', name: 'sName' }, 
				{ data: 'invoice_number', name: 'invoice_number'},    
				{ data: 'action', name: 'action'}    
				]
			});
			$('#inActiveLicensesTable').DataTable({ 
				responsive: true,
				processing: true,
				serverSide: true, 
				bPaginate:true,
				bLengthChange: true,
				initComplete: function(settings, json) {
					$('#inactive_tag').html(json.recordsTotal);
				},   
				ajax: "/subscription-licenses/get-all-licenses?type=2",
				dom: 'Blfrtip',
				lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: { 
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				}
				],  
				columns: [ 
				{ data: 'aName', name: 'aName' }, 
				{ data: 'first_name', name: 'first_name' },  
				{ data: 'amount_paid', name: 'amount_paid' }, 
				{ data: 'valid_upto', name: 'valid_upto' }, 
				{ data: 'pName', name: 'pName' }, 
				{ data: 'payment_complete', name: 'payment_complete' }, 
				{ data: 'sName', name: 'sName' }, 
				{ data: 'invoice_number', name: 'invoice_number'},    
				{ data: 'action', name: 'action'}    
				]
			});
			$('#expiredLicensesTable').DataTable({ 
				responsive: true,
				processing: true,
				serverSide: true, 
				bPaginate:true,
				bLengthChange: true,
				initComplete: function(settings, json) {
					$('#expired_tag').html(json.recordsTotal);
				},   
				ajax: "/subscription-licenses/get-all-licenses?type=3", 
				dom: 'Blfrtip',
				lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: { 
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				}
				],      
				columns: [ 
				{ data: 'aName', name: 'aName' }, 
				{ data: 'first_name', name: 'first_name' },  
				{ data: 'amount_paid', name: 'amount_paid' }, 
				{ data: 'valid_upto', name: 'valid_upto' }, 
				{ data: 'pName', name: 'pName' }, 
				{ data: 'payment_complete', name: 'payment_complete' }, 
				{ data: 'sName', name: 'sName' }, 
				{ data: 'invoice_number', name: 'invoice_number'},    
				{ data: 'action', name: 'action'}    
				]
			});
			$('#bankTransferLicensesTable').DataTable({ 
				responsive: true,
				processing: true,
				serverSide: true, 
				bPaginate:true,
				bLengthChange: true,
				stateSave: true,
				initComplete: function(settings, json) {
					$('#new_tag').html(json.recordsTotal);
				},   
				ajax: "/subscription-licenses/get-all-licenses?type=4",  
				dom: 'Blfrtip',
				lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
				buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: { 
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [ 0, 1, 2,3,4,5,6]
					}
				}
				],     
				columns: [ 
				{ data: 'aName', name: 'aName' }, 
				{ data: 'first_name', name: 'first_name' },  
				{ data: 'amount_paid', name: 'amount_paid' }, 
				{ data: 'valid_upto', name: 'valid_upto' }, 
				{ data: 'pName', name: 'pName' }, 
				{ data: 'payment_complete', name: 'payment_complete' }, 
				{ data: 'sName', name: 'sName' }, 
				{ data: 'invoice_number', name: 'invoice_number'},    
				{ data: 'action', name: 'action'}    
				]
			});
		});  

function confirmAction(url) {
	if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
		window.location.href=url;
	} else { 
		return false;
	} 
} 
</script>
@endsection