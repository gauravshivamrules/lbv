@extends('layouts.back')
@section('title',_('My Subscriptions'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> <?php echo __('My Subscriptions'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="licensesTable">
				<thead>
					<tr>
						<th><?php echo __('Accomodation'); ?></th>     
						<th><?php echo __('Amount'); ?>(&euro;)</th> 
						<th><?php echo __('Valid Upto'); ?></th>
						<th><?php echo __('Payment Method'); ?></th>
						<th><?php echo __('Payment Status'); ?></th>
						<th><?php echo __('Subscription'); ?></th>
						<th><?php echo __('Invoice'); ?></th>
						<th><?php echo __('Actions'); ?></th> 
					</tr>
				</thead> 
			</table>
			</div>
	</div> 
		<script>
			$(function() { 
			    $('#licensesTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true, 
			        bPaginate:true,
			        bLengthChange: true,
			        ajax: "/subscription-licenses/get-licenses",    
			        columns: [ 
			            { data: 'aName', name: 'aName' }, 
			            { data: 'amount_paid', name: 'amount_paid' }, 
			            { data: 'valid_upto', name: 'valid_upto' }, 
			            { data: 'pName', name: 'pName' }, 
			            { data: 'payment_complete', name: 'payment_complete' }, 
			            { data: 'sName', name: 'sName' }, 
			            { data: 'invoice_number', name: 'invoice_number'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			}); 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection