@extends('layouts.back')
@section('title',_('Free Subscription Coupons'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> <?php echo __('Free Subscription Coupons'); ?> </h2> 
		</div>
			<div class="table-responsive">  
				<table class="table table-striped table-hover dt-responsive display nowrap" id="freeLicensesTable">
				<thead>
					<tr>
						<th><?php echo __('User'); ?></th>     
						<th><?php echo __('Accomodation'); ?></th>     
						<th><?php echo __('Subscription'); ?></th> 
						<th><?php echo __('Coupon'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Sent On'); ?></th>
					</tr>
				</thead> 
			</table>
			</div>
	</div> 
		<script>
			$(function() { 
			    $('#freeLicensesTable').DataTable({
			    	responsive: true,
			        processing: true,
			        serverSide: true, 
			        bPaginate:true,
			        bLengthChange: true,
			        ajax: "/subscription-licenses/all-coupons-ajax",    
			        columns: [ 
			            { data: 'uName', name: 'uName' }, 
			            { data: 'aName', name: 'aName' }, 
			            { data: 'sName', name: 'sName' }, 
			            { data: 'token', name: 'token' }, 
			            { data: 'is_closed', name: 'is_closed' }, 
			            { data: 'created_at', name: 'created_at'},    
			        ]
			    });
			}); 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {   
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
		</script>
@endsection