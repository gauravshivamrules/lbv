@extends('layouts.back')
@section('title',_('Subscription Payments'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Subscription Payments') }} </h2> 
		</div>
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#showCompleted">
					<span class="label label-success" id="completed"></span> 
					{{__('Completed')}} 
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#showCancelled">
					<span class="label label-danger" id="cancelled"></span> 
					{{__('Cancelled')}}
				</a>
			</li> 
			<li>
				<a data-toggle="tab" href="#showPending">
					<span class="label label-primary" id="pending"></span> 
					{{_('Pending')}} 
				</a>
			</li>
		</ul> 

		<div class="tab-content"> 

			<div id="showCompleted" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="completedTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Amount'); ?>(&euro;)</th> 
								<th><?php echo __('Created On'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Subscription'); ?></th>
								<th><?php echo __('Invoice'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>

			<div id="showCancelled" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="cancelledTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Amount'); ?>(&euro;)</th> 
								<th><?php echo __('Created On'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Subscription'); ?></th>
								<th><?php echo __('Invoice'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			<div id="showPending" class="tab-pane"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="pendingTable">
						<thead>
							<tr>
								<th><?php echo __('Accomodation'); ?></th>     
								<th><?php echo __('Amount'); ?>(&euro;)</th> 
								<th><?php echo __('Created On'); ?></th>
								<th><?php echo __('Valid Upto'); ?></th>
								<th><?php echo __('Payment Method'); ?></th>
								<th><?php echo __('Payment Status'); ?></th>
								<th><?php echo __('Subscription'); ?></th>
								<th><?php echo __('Invoice'); ?></th>
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>


			
		</div>
		

			
	</div>
		<script>
			$(function() {
				buildTable('completedTable','completed');
				buildTable('cancelledTable','cancelled');  
				buildTable('pendingTable','pending');    
			}); 
			function buildTable (table,tCount) {
				$('#'+table).DataTable({ 
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/subscription-payments/get-payments-ajax?type="+tCount,
				    initComplete: function(settings, json) {
				    	$('#'+tCount).html(json.recordsTotal);
				    },      
				    columns: [ 
					    { data: 'aName', name: 'aName' }, 
					    { data: 'amount_paid', name: 'amount_paid' }, 
					    { data: 'created_at', name: 'created_at' }, 
					    { data: 'valid_upto', name: 'valid_upto' }, 
					    { data: 'pName', name: 'pName' }, 
					    { data: 'payment_complete', name: 'payment_complete' }, 
					    { data: 'sName', name: 'sName' }, 
					    { data: 'invoice_number', name: 'invoice_number'},    
					    { data: 'action', name: 'action'}    
				    ]  
				
				});
			}  
		</script>
@endsection