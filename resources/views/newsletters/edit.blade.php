@extends('layouts.back') 
@section('title',__('Update Newsletter')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
	<form action="/newsletter/edit/{{$newsletter->newsletter_content->id}}" method="post" class="form-horizontal"> 
		<div class="row"> 
			<div class="col-md-10">
				<div id="progressbar"></div> 
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset>
						<?php 
							$monthNum=$newsletter->month;
							$monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
						?>
						<legend><?php echo __('Update Newsletter For ').__($monthName).' '.$newsletter->year; ?></legend>  
							<div class="form-group">
								<div class="col-lg-10">
									<textarea id="message" class="summernote" name="message" id="message">{{$newsletter->newsletter_content->content}} </textarea>
								</div>
							</div>
					</fieldset>  
			</div>
		</div>
		<!-- TIP -->
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Tips Of The Month'); ?></legend>
			@if(isset($finalArr['tipOfTheMonth']))
				@foreach($finalArr['tipOfTheMonth']['accom'] as $a) 
				<div class="form-group col-md-4" style="margin-right: 0px;">
					<img src="/images/gallery/{{$a->id}}/{{$a->thumb}}" class="img-responsive">
					<label class="control-label">
						<font style="vertical-align: inherit;">
							{{$a->name}}	
						</font>
					</label>
					 {{$a->region}}, {{ucfirst($a->country) }}
					 <p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($a->description,200) }}</p>
					 <?php 
							$tips[]=$a->id;
							$tipsStr=implode(',',$tips);
					 ?>
				</div>
				@endforeach
				<input type="hidden" value="{{$tipsStr}}" name="tip"> 
			@else
				<legend><?php echo __('No Tip Found'); ?></legend> 
			@endif
		</div>
		<!-- SPOTLIGHT -->
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Spotlights'); ?></legend> 
			@if($finalArr['spotsOfTheMonth']) 
				<?php  $spots=array(); ?> 
				@foreach($finalArr['spotsOfTheMonth']['accom'] as $sA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$sA->id}}/{{$sA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$sA->name}}	
							</font>
						</label>
						{{$sA->region}}, {{ucfirst($sA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($sA->description,200) }}</p>
						<?php 
							$spots[]=$sA->id;
							$spotsStr=implode(',',$spots);
						?>
					</div>
				@endforeach
				<?php //dd($spots) ?>
				<input type="hidden" value="{{$spotsStr}}" name="spot">
			@else
				<legend><?php echo __('No Spotlight Found'); ?></legend>
			@endif
		</div>

		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('News'); ?></legend>
			@if($finalArr['newsOfTheMonth'])
				<?php  $news=array(); ?> 
				@foreach($finalArr['newsOfTheMonth']['accom'] as $nA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$nA->id}}/{{$nA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$nA->name}}	
							</font>
						</label>
						{{$nA->region}}, {{ucfirst($nA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($nA->description,200) }}</p>
					</div>
					<?php 
						$news[]=$sA->id;
						$newsStr=implode(',',$news); 
					?>
				@endforeach
				<input type="hidden" value="{{$newsStr}}" name="news">
			@else
				<legend><?php echo __('No News Found'); ?></legend>
			@endif
		</div>
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Last Minute'); ?></legend>
			@if($finalArr['lastOfTheMonth'])   
				<?php  $minutes=array(); ?> 	
				@foreach($finalArr['lastOfTheMonth']['accom'] as $lA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$lA->id}}/{{$lA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$lA->name}}	
							</font>
						</label>
						{{$lA->region}}, {{ucfirst($lA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($lA->description,200) }}</p>
					</div>
					<?php 
						$minutes[]=$lA->id;
						$minutesStr=implode(',',$minutes);
					?>
				@endforeach
				<input type="hidden" value="{{$minutesStr}}" name="minutes">
			@else
				<legend><?php echo __('No Last Minute Found'); ?></legend> 
			@endif 
		</div>
		
		<div class="row">  
			<legend ><?php echo __('Add More Accomodations'); ?></legend> 
			<div class="form-group col-md-5">
				{!! Form::select('others[]',$accoms,explode(',',$newsletter->newsletter_content->others),['id'=>'accomId','class'=>'form-control','multiple'=>true] ) !!} 
			</div>
		</div>
		<div class="row"> 
			<div class="form-group col-md-5">
				<button type="submit" class="btn btn-primary">{{__('Update')}}</button>
			</div>
		</div>
	</form>
</div>
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>   
<script type="text/javascript">
	jQuery(document).ready(function($) {  
		$('.summernote').summernote({
	     	height: 200,   //set editable area's height
	    });
	    $("#accomId").chosen({   
	        disable_search_threshold: 10,
	        no_results_text: "Oops, nothing found!", 
	        width: "95%"

	    }); 
	});
</script>
@endsection