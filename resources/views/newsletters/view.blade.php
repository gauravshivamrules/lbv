@extends('layouts.back') 
@section('title',__('View Newsletter')) 
@section('content')
@foreach($errors->all() as $e)
	<li class="error"> {{ $e }} </li> 
@endforeach  
</ul>
<div class="well well bs-component"> 
		<div class="row"> 
			<div class="col-md-10">
				<div id="progressbar"></div> 
				<div style="float:right">  
					<form action="/newsletter/send/{{$newsletter->id}}" method="post" onsubmit="return confirm('Are u sure?')">
						{{csrf_field()}}
						<button type="submit" class="btn btn-xs btn-danger">{{__('Send')}}</button>
					</form>
				</div>
					<fieldset> 
						<legend><?php echo __($newsletter->title) ?></legend> 
							<div class="form-group">
								<div class="col-lg-10">
									{{strip_tags($newsletter->newsletter_content->content)}} 
								</div>
							</div>
					</fieldset>   
			</div>
		</div>
		<br>
		<!-- TIP -->
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Tips Of The Month'); ?></legend>
			@if(isset($finalArr['tipOfTheMonth']))
				@foreach($finalArr['tipOfTheMonth']['accom'] as $a) 
				<div class="form-group col-md-4" style="margin-right: 0px;">
					<img src="/images/gallery/{{$a->id}}/{{$a->thumb}}" class="img-responsive">
					<label class="control-label">
						<font style="vertical-align: inherit;">
							{{$a->name}}	
						</font>
					</label>
					 {{$a->region}}, {{ucfirst($a->country) }}
					 <p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($a->description,200) }}</p>
					 <?php 
							$tips[]=$a->id;
							$tipsStr=implode(',',$tips);
					 ?>
				</div>
				@endforeach
				<input type="hidden" value="{{$tipsStr}}" name="tip"> 
			@else
				<legend><?php echo __('No Tip Found'); ?></legend> 
			@endif
		</div>
		<!-- SPOTLIGHT -->
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Spotlights'); ?></legend> 
			@if($finalArr['spotsOfTheMonth']) 
				<?php  $spots=array(); ?> 
				@foreach($finalArr['spotsOfTheMonth']['accom'] as $sA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$sA->id}}/{{$sA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$sA->name}}	
							</font>
						</label>
						{{$sA->region}}, {{ucfirst($sA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($sA->description,200) }}</p>
						<?php 
							$spots[]=$sA->id;
							$spotsStr=implode(',',$spots);
						?>
					</div>
				@endforeach
				<?php //dd($spots) ?>
				<input type="hidden" value="{{$spotsStr}}" name="spot">
			@else
				<legend><?php echo __('No Spotlight Found'); ?></legend>
			@endif
		</div>

		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('News'); ?></legend>
			@if($finalArr['newsOfTheMonth'])  
				<?php  $news=array(); ?> 
				@foreach($finalArr['newsOfTheMonth']['accom'] as $nA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$nA->id}}/{{$nA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$nA->name}}	
							</font>
						</label>
						{{$nA->region}}, {{ucfirst($nA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($nA->description,200) }}</p>
					</div>
					<?php 
						$news[]=$sA->id;
						$newsStr=implode(',',$news); 
					?>
				@endforeach
				<input type="hidden" value="{{$newsStr}}" name="news">
			@else
				<legend><?php echo __('No News Found'); ?></legend>
			@endif
		</div>
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('Last Minute'); ?></legend>
			@if($finalArr['lastOfTheMonth'])   
				@foreach($finalArr['lastOfTheMonth']['accom'] as $lA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$lA->id}}/{{$lA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$lA->name}}	
							</font>
						</label>
						{{$lA->region}}, {{ucfirst($lA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($lA->description,200) }}</p>
					</div>
				@endforeach
			@else
				<legend><?php echo __('No Last Minute Found'); ?></legend> 
			@endif 
		</div>
		<div class="row"> 
			<legend style="text-align: center;"><?php echo __('More Accomodations'); ?></legend>
			@if($finalArr['others'])   
				@foreach($finalArr['others']['accom'] as $oA)
					<div class="form-group col-md-6">
						<img src="/images/gallery/{{$oA->id}}/{{$oA->thumb}}">
						<label class="control-label">
							<font style="vertical-align: inherit;">
								{{$oA->name}}	
							</font>
						</label>
						{{$oA->region}}, {{ucfirst($oA->country) }}
						<p style="text-align: justify;">{{ \Illuminate\Support\Str::limit($oA->description,200) }}</p>
					</div>
				@endforeach 
			@else
				<legend><?php echo __('No more found'); ?></legend> 
			@endif 
		</div>
</div> 
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>   
<script type="text/javascript">
	jQuery(document).ready(function($) {  
		$('.summernote').summernote({
	     	height: 200,   //set editable area's height
	    });
	    $("#accomId").chosen({   
	        disable_search_threshold: 10,
	        no_results_text: "Oops, nothing found!", 
	        width: "95%"

	    }); 
	});
</script>
@endsection