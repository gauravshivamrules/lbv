@extends('layouts.back')
@section('title',_('All Newsletters'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script> 
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Newsletters') }} </h2>  
		</div>
		<div class="tab-content"> 
			<div id="showTips" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Title'); ?></th>   
								<th><?php echo __('Month'); ?></th>   
								<th><?php echo __('Year'); ?></th>     
								<th><?php echo __('Actions'); ?></th> 
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
	</div>
		<script> 
			$(function() {
				buildTable('activeTable');  
			}); 
			function buildTable (table) {
				$('#'+table).DataTable({  
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/newsletter-ajax", 
				    columns: [
				        { data: 'title', name: 'title' },
				        { data: 'month', name: 'month' },
				        { data: 'year', name: 'year' },
				        { data: 'action', name: 'action'}    
				    ]
				}); 
			} 
			 
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {    
					window.location.href="/newsletter/delete/"+id;
				} else { 
					return false;
				} 
			}
			function confirmSend(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {    
					window.location.href="/newsletter/send/"+id; 
				} else { 
					return false;
				} 
			} 

			
		</script>
@endsection