@extends('layouts.back')
@section('title',_('All Newsletter Subscribers'))
@section('content')
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">  
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.js"></script> 
	
	<div class="panel panel-default">
		<div class="panel-heading"> 
			<h2> {{__('All Newsletter Subscribers') }} </h2>  
		</div>
		<div class="tab-content"> 
			<div id="showTips" class="tab-pane active"> 
				<div class="table-responsive">  
					<table class="table table-striped table-hover dt-responsive display nowrap" id="activeTable">
						<thead>
							<tr>
								<th><?php echo __('Email'); ?></th> 
								<th><?php echo __('Status'); ?></th>   
								<th><?php echo __('Actions'); ?></th>  
							</tr>
						</thead> 
					</table>
				</div>
			</div>
		</div>
	</div>
		<script> 
			$(function() {
				buildTable('activeTable');  
			}); 
			function buildTable (table) {
				$('#'+table).DataTable({  
					responsive: true,
				    processing: true,
				    serverSide: true,
				    ajax: "/newsletter/subscribers-ajax", 
				    columns: [
				        { data: 'email', name: 'email' },
				        { data: 'active', name: 'active' }, 
				        { data: 'action', name: 'action'}    
				    ]
				}); 
			}  
			function confirmDel(id) {
				if(window.confirm("<?php echo __('Are you sure?') ?>")) {    
					window.location.href="/newsletter/delete-subscriber/"+id;
				} else { 
					return false;
				} 
			}
		</script>
@endsection