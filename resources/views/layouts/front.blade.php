<!DOCTYPE html>
<html>
<head>
<title> {{env('SITE_TITLE_FRONT')}}  </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="LBV" />
<link href="/css/front/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/front/agency.min.css" rel="stylesheet">
<link href="/css/front/styles.css?v=1.0" rel="stylesheet">
<link href="/css/front/style.css" rel="stylesheet" type="text/css" media="all" />	
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
<script src="/js/front/jquery.min.js"></script> 

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>  
</head>
<body >
 <!-- Navigation -->
    @include('elements.front_header')
<!--// Navigation-->	
 <!-- header -->
<!--//header-->
<!--content-->
<div class="content">
	@if(session('success'))
	    <div class="alert alert-success"> 
	      {{ session('success') }}
	    </div>
	@endif
	@if(session('error'))
	    <div class="alert alert-danger">  
	      {{ session('error') }}
	    </div>
	@endif 
	@yield('content')
</div>
<!--footer-->
	@include('elements.front_footer')
	<script src="/js/front/scripts.js" async ></script>
    <script src="/js/front/bootstrap.min.js" async ></script>
    <script src="/js/front/jquery.easing.min.js" async ></script>
    <script src="/js/front/agency.min.js" async ></script>
<!--//footer-->
</body>
</html>