<!DOCTYPE html>
  <html> 
  <head>
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') |  <?php echo env('SITE_TITLE_BACK', 'LBV') ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {!! Html::style('css/bootstrap.min.css') !!}     
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') !!}
    {!! Html::style('css/AdminLTE.min.css') !!}
    {!! Html::style('css/_all-skins.min.css') !!} 
    {!! Html::style('css/mycustom.css') !!}
    {!! Html::script('js/jquery.min.js') !!}  
    {!! Html::script('js/jquery-ui.min.js') !!}
    {!! Html::script('js/jquery.blockUI.js') !!} 
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

      @include('elements.back_header') 
    <!-- Left side column. contains the logo and sidebar -->
      @if(Auth::user()->role_id==1)
          @include('elements.admin_sidebar')
      @elseif(Auth::user()->role_id==2)
         @include('elements.cust_sidebar') 
      @else
          @include('elements.adv_sidebar')
      @endif 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @if(session('success'))
          <div class="alert alert-success" style="text-align: center;"> 
            {{ session('success') }}
          </div>
      @endif 
      @if(session('error'))
          <div class="alert alert-danger" style="text-align: center;">  
            {{ session('error') }}
          </div>
      @endif 
      <!-- Main content -->
      <section class="content">
          @yield('content')
      </section>
      <!-- /.content -->
    </div>
  
    <!-- /.content-wrapper -->
    @include('elements.back_footer') 
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
 
  {!! Html::script('js/bootstrap.min.js') !!}
  {!! Html::script('js/adminlte.min.js') !!} 

  <script type="text/javascript">
      // $(document).ajaxStart($.blockUI({message: '<h1>Just a moment...</h1>'}) ).ajaxStop($.unblockUI); 
  </script> 
  </body>
</html>
