<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">   
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>  <?php echo env('SITE_TITLE_FRONT', 'Logeren bij Vlamingen') ?> </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <style type="text/css">
      .alert {
        text-align: center;
      }
  </style>
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="/css/ionicons.min.css" rel="stylesheet">
  <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" media="all" /> 
  <link href="/css/blue.css" rel="stylesheet" type="text/css" media="all" /> 
  <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.1.0/css/all.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
    @if(session('success'))
        <div class="alert alert-success"> 
          {{ session('success') }}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">  
          {{ session('error') }}
        </div>
    @endif 
    @yield('content')
</body>
</html>
