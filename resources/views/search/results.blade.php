<div class="row">
	<div class="col-md-12" id="searchTerms">
		<div class="searchtags">{{$result['accoms']->total()}} {{__('accommodations found')}} </div> 
	</div> 
</div>
@foreach($result['accomsArr'] as $a)
	<div class="row">
		<div class="col-md-6">
			<div class="tag">
				@if(isset($promos['Tips']))
				<?php if(in_array($a->id,(array) $promos['Tips'])) { ?>
					<div class="corner-ribbon top-left-search sticky red shadow">{{__('TIP OF THE MONTH')}} </div>
				<?php } ?>
				@endif
				@if(isset($promos['Spots']))
				<?php if(in_array($a->id,(array)$promos['Spots'])) { ?>
					<div class="corner-ribbon top-left-search sticky red shadow">{{__('SPOTLIGHT')}} </div>
				<?php } ?>
				@endif
				@if(isset($promos['Lastminutes']))
				<?php if(in_array($a->id,(array)$promos['Lastminutes'])) { ?>
					<div class="corner-ribbon top-left-search sticky red shadow">{{__('LAST MINUTE')}} </div>
				<?php } ?>
				@endif
				@if(isset($promos['News']))
				<?php if(in_array($a->id,(array)$promos['News'])) { ?>
					<div class="corner-ribbon top-left-search sticky red shadow">{{__('NEWS')}} </div>
				<?php } ?>
				@endif
				<a href="/accomodations/view/{{$a->slug}}" class="mask">
					@if($a->featured_image)
						<img class="img-responsive zoom-img" src="/images/gallery/{{$a->id}}/{{$a->featured_image}}" alt="{{__('Featured Image')}}" onerror="this.src='/images/no-image.png'">
					@else
						<p style="text-align:center;margin-top: 110px;">{{__('No Image Available')}} </p>
					@endif
				</a>
				<div class="most-1">
					<h4><a href="/accomodations/view/{{$a->slug}}">{{$a->name}} </a></h4>
					<span>{{ucfirst($a->rName)}}, {{ucfirst($a->cName)}} </span>
				</div>
			</div>
		</div>
		<div class="col-md-6 p-0 m-top">
			<div class="text-center">
				<span class="price__amount text-center">€{{$a->amount}} </span>
			</div>
			<div class="price__label">{{ucfirst(__($a->price_unit))}}, vanaf</div>
			<div class="border">
				<div class="row p-t-b">
					<div class="col-xs-4">
						<img src="/images/icon/icon1.png" class="icon_width">
						<span class="icon-users icomoon_font"></span>
						<span class="badge_count" title="{{$a->number_of_persons}} {{__('Persons')}}">{{$a->number_of_persons}} </span>
					</div>
					<div class="col-xs-4">
						<img src="/images/icon/icon2.png" class="icon_width_37">
						<span class="icon-hotel icomoon_font"></span>
						<span class="badge_count" title=" {{$a->bedrooms}} {{__('Bedrooms')}}"> {{$a->bedrooms}} </span>
					</div>
					<div class="col-xs-4">
						<img src="/images/icon/icon3.png" class="icon_width">
						<span class="icon-bathtub icomoon_font"></span>
						<span class="badge_count" title="{{$a->bathrooms}} {{__('Bathrooms')}}">{{$a->bathrooms}}</span>
					</div>				
				</div>
			</div>
		</div>
	</div>
@endforeach
{!! $result['accoms']->render() !!}