<!DOCTYPE html>
<html>
<head>
	<title>{{env('SITE_TITLE_FRONT')}} </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<link href="/css/front/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/css/front/agency.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />
	<link href="/css/front/styles.css" rel="stylesheet">
	<link href="/css/front/style.css" rel="stylesheet" type="text/css" media="all" />	
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"> 
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
	<link rel="stylesheet" href="/css/front/map/screen.css" />
	<!-- <link rel="stylesheet" href="/css/front/map/MarkerCluster.css" /> -->
	<link rel="stylesheet" href="/css/front/map/MarkerCluster.Default.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css">
</head> 
<style type="text/css">
.navbar-fixed-top, .navbar-fixed-bottom {
	position: relative ;
}
</style>
<body class="overflow">
	<!-- Navigation -->
	 @include('elements.front_header')
	<div class="row">
		<div class="col-md-7 col-sm-6 p-0 hidden-xs" style="overflow-x: hidden;">
			<div id="map" style="width: 100%;"></div>
		</div>
		<div class="col-md-5 col-sm-6 p-0 height_over">
			<form  id="searchForm" action="/search/ajax-search" method="post"> 
				<div class="yellow_bg p-20">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<select class="form-control" name="label" id="label" onchange="submitSearchForm();">
									<option value="3">{{__('RENT')}}</option>
									<option value="2">{{__('SALE')}}</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" name="search_start_date" id="search_start_date" placeholder="{{__('From')}}">
							</div>
							<!-- <div class="col-md-1"> <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i></div> -->
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control" name="search_end_date" id="search_end_date" onchange="submitSearchForm();" placeholder="{{__('To')}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::select('country',$countries,null,['class'=>'form-control','placeholder'=>__('Country'),'id'=>'countryID','onchange'=>'submitSearchForm()'] ) !!} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<select class="form-control" id="region" name="region" onchange="submitSearchForm();"><option value="">{{__('Region')}}</option></select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<select class="form-control" name="department" id="department" onchange="submitSearchForm();"><option value="">{{__('Department')}}</option></select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::select('type_id',$types,null,['class'=>'form-control','multiple'=>true,'id'=>'typeID','onchange'=>'submitSearchForm()'] ) !!} 
								<input type="hidden" name="typeId" id="typeId"> 
							</div>
						</div>
						
						<div class="col-md-8">
							<div class="form-group">
								<input type="text" class="form-control" id="keywords" name="keywords" onchange="submitSearchForm();" placeholder="{{__('Keywords....')}}"> 
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<select class="form-control" id="noOfPerson" name="noofperson" onchange="submitSearchForm();"><option value="">{{__('No Of Person')}}</option>
									<?php for($i=1;$i<=20;$i++) { ?>
										@if($i==0)
											<option value="{{$i}}">{{$i}} {{__('Person')}}</option>
										@else
											<option value="{{$i}}">{{$i}} {{__('Persons')}}</option>
										@endif
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<!-- <select class="form-control" id="price" name="price" onchange="submitSearchForm();"><option value="">{{__('Price')}}</option></select> -->
								<label for="amount">Price range:</label>
								<span id="amount"></span>
								<input type="hidden" id="price" name="price"> 
								<div id="slider-range"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<button type="button" class="btn filter" id="moreFilters">
								<b id="moreFiltersBold">+ {{__('More Filters')}}</b>
							</button>
						</div>
					</div>
					<div id="moreFiltersDiv" style="display:none;">
						<?php foreach ($searchGroup as $amenity) { ?>
							<div class="row">
								<div class="col-md-6 flip">+<b> {{$amenity['group_name']}}</b></div>
							</div>
							<div class="show_check" style="display: none;">
								<div class="row">
									<!-- <div class="col-md-4"></div>
									<div class="col-md-8"> -->
										<div class="row">
											<?php foreach ($amenity['SearchingTags'] as $tag) 
												if($tag->id) {
											{  ?>
											<div class="col-md-6">
												<div class="checkbox" style="font-size:12px;" >
													<label>
														<input type="checkbox" value="{{$tag->id}}" id="amnity" onchange="submitSearchForm();" >
														<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
														{{$tag->tag_name}}
													</label>
												</div>
											</div>
											<?php } } ?>
										</div>
								</div>
							</div>
						<?php } ?>
						<input type="hidden" name="amnityArr" type="hidden" id="amnityArr">
					</div>
				</div>
			</form>
			<div id="loadingAjax"><img src="/images/loader.gif"></div>
			<div id="searchResults">
			</div>
		</div>
	</div>
	<script src="/js/front/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>

	<script type="text/javascript">
	function getUrlVars(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}
	function getRegions(country){
	    if(country) {
	    	$('#region').empty();
	    	$('#region').append("<option value=''>Region</option>");
	      $.ajax({
	        url:'/accomodations/get_regions?country_id='+country,
	        dataType:'json',
	        success:function(r) {
	          if(r) {
	            $.each(r, function(key, value) {
	                $('#region').append($("<option/>", {
	                    value: value.id, 
	                    text: value.region_name
	                }));
	            });
	            $("#region").trigger("chosen:updated");
	            $("#department").trigger("chosen:updated"); 
	          }
	        }
	      }); 
	    }
	}
	var urlVars=getUrlVars();
	if( typeof(urlVars['type']) != "undefined" && (urlVars['type'] !== null ) ) {
		switch(urlVars['type']) {
		    case 'country':
		        $('#countryID').val(urlVars['type_id']).trigger("chosen:updated");
		        getRegions(urlVars['type_id']);
		        break;
		    case 'region':
		        $('#countryID').val(urlVars['term']).trigger("chosen:updated");
    			getRegions(urlVars['term']);
		        break;
		    case 'department':
		        $('#countryID').val(urlVars['term']).trigger("chosen:updated");
    			getRegions(urlVars['term']);
		    	break;
		    case 'type': 
		    	$('#typeId').val(urlVars['type_id']);
		    	$('#typeID').val(urlVars['type_id']);
		    	break;
		    case 'amnity':
		    	$("input[type=checkbox][value="+urlVars['type_id']+"]").prop("checked",true);
		    	break; 
		    case 'desc':
		    	$('#keywords').val(decodeURI(urlVars['term'])); 
		    	break; 
		} 
	}
	 
		$('#moreFilters').click(function(){
			if($('#moreFiltersDiv').is(":visible")){ 
				$('#moreFilters').text("+"+"{{__('More Filters')}}");
				$('#moreFiltersDiv').hide();
			}else{
				$('#moreFilters').text('-'+"{{__('Less Filters')}}");
				$('#moreFiltersDiv').show(); 
			}
		});
	</script>
	<script src="/js/custom/search.js?v=3.0"></script>
	<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js"></script>
	<script src="/js/front/leaflet.markercluster-src.js"></script>  

	<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
	<script src="/js/front/scripts.js"></script>
	<script src="/js/front/bootstrap.min.js"></script>
	<script src="/js/front/jquery.easing.min.js" async ></script>
	<script src="/js/front/agency.min.js"></script> 

</body>
</html> 